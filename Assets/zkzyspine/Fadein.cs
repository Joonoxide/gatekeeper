﻿using UnityEngine;
using System.Collections;

public class Fadein : MonoBehaviour 
{
	public float alp = 0;
	static float col = 0.3f;
	void Start()
	{
		//renderer.material.shader = GameController.instance.coloredShader;
		col = gameObject.renderer.material.color.r;
		alp = 0f;
		gameObject.renderer.material.color = new Color(col, col, col, 0f);
	}

	void Update () 
	{
		if (alp >= 0.5f)
		{
			Destroy(this);
			return;
		}
		
		alp += 0.05f*Time.deltaTime;
		gameObject.renderer.material.color = new Color(col, col, col, alp);
		
		//resource management, cleanup when it is no longer visible
		
	}
}
