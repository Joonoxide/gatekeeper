
skeleton.png
format: RGBA8888
filter: Linear,Linear
repeat: none
body
  rotate: false
  xy: 392, 62
  size: 82, 64
  orig: 82, 64
  offset: 0, 0
  index: -1
body_o
  rotate: false
  xy: 304, 58
  size: 86, 68
  orig: 86, 68
  offset: 0, 0
  index: -1
cape
  rotate: false
  xy: 555, 53
  size: 69, 73
  orig: 69, 73
  offset: 0, 0
  index: -1
cape_o
  rotate: true
  xy: 476, 53
  size: 73, 77
  orig: 73, 77
  offset: 0, 0
  index: -1
csd_shadow
  rotate: false
  xy: 309, 42
  size: 78, 14
  orig: 78, 14
  offset: 0, 0
  index: -1
csd_sword_01
  rotate: false
  xy: 662, 91
  size: 56, 35
  orig: 56, 89
  offset: 0, 54
  index: -1
csd_sword_02
  rotate: true
  xy: 213, 58
  size: 68, 89
  orig: 68, 89
  offset: 0, 0
  index: -1
csd_sword_03
  rotate: true
  xy: 2, 57
  size: 69, 209
  orig: 69, 209
  offset: 0, 0
  index: -1
hammer
  rotate: true
  xy: 309, 2
  size: 38, 47
  orig: 38, 47
  offset: 0, 0
  index: -1
hammer_o
  rotate: false
  xy: 2, 4
  size: 42, 51
  orig: 42, 51
  offset: 0, 0
  index: -1
hand_left
  rotate: true
  xy: 46, 25
  size: 30, 57
  orig: 30, 57
  offset: 0, 0
  index: -1
hand_left_o
  rotate: false
  xy: 626, 65
  size: 34, 61
  orig: 34, 61
  offset: 0, 0
  index: -1
hand_right
  rotate: true
  xy: 105, 30
  size: 25, 50
  orig: 25, 50
  offset: 0, 0
  index: -1
hand_right_o
  rotate: false
  xy: 278, 2
  size: 29, 54
  orig: 29, 54
  offset: 0, 0
  index: -1
helm
  rotate: true
  xy: 213, 2
  size: 54, 63
  orig: 54, 63
  offset: 0, 0
  index: -1
helm_o
  rotate: true
  xy: 392, 2
  size: 58, 67
  orig: 58, 67
  offset: 0, 0
  index: -1
leg
  rotate: false
  xy: 889, 92
  size: 31, 34
  orig: 31, 34
  offset: 0, 0
  index: -1
leg_o
  rotate: true
  xy: 814, 91
  size: 35, 38
  orig: 35, 38
  offset: 0, 0
  index: -1
shoulder_left
  rotate: false
  xy: 769, 92
  size: 43, 34
  orig: 43, 34
  offset: 0, 0
  index: -1
shoulder_left_o
  rotate: false
  xy: 720, 88
  size: 47, 38
  orig: 47, 38
  offset: 0, 0
  index: -1
shoulder_right
  rotate: false
  xy: 358, 10
  size: 29, 30
  orig: 29, 30
  offset: 0, 0
  index: -1
shoulder_right_o
  rotate: false
  xy: 854, 92
  size: 33, 34
  orig: 33, 34
  offset: 0, 0
  index: -1
