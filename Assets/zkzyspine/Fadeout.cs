﻿using UnityEngine;
using System.Collections;

public class Fadeout : MonoBehaviour 
{
	public float alp = 1;
	static float col = 0.6f;
	void Start()
	{
		renderer.material.shader = GameController.instance.coloredShader;
		alp = 0.8f;
		gameObject.renderer.material.color = new Color(col, col, col, 0.8f);
	}

	void Update () 
	{
		alp -= 0.4f*Time.deltaTime;
		gameObject.renderer.material.color = new Color(col, col, col, alp);
		
		//resource management, cleanup when it is no longer visible
		if (alp < 0)
		{
			GameObject.Destroy(gameObject);
		}
	}
}
