using UnityEngine;
using System.Collections;

public class OnHover : MonoBehaviour {

	float elapsed; 
	Vector3 originalScale;

	void Start () 
	{
		elapsed = 0f;
		originalScale = transform.localScale;
	}
	
	// Update is called once per frame
	void Update () 
	{

		elapsed += Time.deltaTime;
		float time = elapsed/0.3f;
		if(time > 1)
		{
			time = 1;
		}
		Vector3 targetScale = originalScale * 0.8f;

		transform.localScale = Vector3.Lerp( originalScale, targetScale, time);
		var but = gameObject.GetComponent<UIButton>();
		if(!but.controlIsEnabled)
		{
			Destroy(this);
		}
		if(but.m_ctrlState == UIButton.CONTROL_STATE.OVER || but.m_ctrlState == UIButton.CONTROL_STATE.NORMAL)
		{
			Destroy(this);
		}
	}


	public void OnDestroy()
	{
		transform.localScale = originalScale;
	}
}
