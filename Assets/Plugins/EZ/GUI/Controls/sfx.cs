using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class sfx : MonoBehaviour 
{
	public Dictionary<string, AudioSource> dictionaryAudio;
	public Dictionary<string, AudioSource> dictionaryAudio2;
	public Dictionary<string, AudioSource> dictionaryAudio3;

	public static sfx instance;
	public bool musicOn;
	public bool soundOn;
	public string keyBgm = "";
	public AudioSource click;
	
	private AudioSource currentBGM;

	public void Awake()
	{
		DontDestroyOnLoad(gameObject);
	}

	public static sfx getInstance()
	{
		if(instance == null)
		{
			instance = (GameObject.Instantiate(Resources.Load("sfx") as GameObject, Camera.main.transform.position, Quaternion.identity) as GameObject).GetComponent<sfx>();	
			instance.initialize();	
		}
		return instance;
	}

	void initialize () 
	{
		dictionaryAudio = new Dictionary<string, AudioSource>();
		dictionaryAudio2 = new Dictionary<string, AudioSource>();
		dictionaryAudio3 = new Dictionary<string, AudioSource>();

		/*
		dictionaryAudio.Add("merge", transform.Find("merge").gameObject.GetComponent<AudioSource>());
		dictionaryAudio.Add("hit", transform.Find("hit").gameObject.GetComponent<AudioSource>());
		dictionaryAudio.Add("battlebgm", transform.Find("battlebgm").gameObject.GetComponent<AudioSource>());
		dictionaryAudio.Add("endbgm", transform.Find("endbgm").gameObject.GetComponent<AudioSource>());
		dictionaryAudio.Add("titlebgm", transform.Find("titlebgm").gameObject.GetComponent<AudioSource>());
		dictionaryAudio.Add("allyDefeated", transform.Find("allyDefeated").gameObject.GetComponent<AudioSource>());
		dictionaryAudio.Add("highCombo", transform.Find("highCombo").gameObject.GetComponent<AudioSource>());
		dictionaryAudio.Add("summoned", transform.Find("summoned").gameObject.GetComponent<AudioSource>());
		dictionaryAudio.Add("KoSound", transform.Find("KoSound").gameObject.GetComponent<AudioSource>());
		dictionaryAudio.Add("warningSign", transform.Find("warningSign").gameObject.GetComponent<AudioSource>());
		dictionaryAudio.Add("battleStartSound", transform.Find("battleStartSound").gameObject.GetComponent<AudioSource>());
		*/
		GameObject newGO;
		foreach(Transform child in transform)
		{
			dictionaryAudio.Add(child.gameObject.name, child.gameObject.GetComponent<AudioSource>());
			newGO = Instantiate(child.gameObject) as GameObject;
			dictionaryAudio2.Add(child.gameObject.name, newGO.GetComponent<AudioSource>());
			newGO = Instantiate(child.gameObject) as GameObject;
			dictionaryAudio3.Add(child.gameObject.name, newGO.GetComponent<AudioSource>());
		}

		foreach(KeyValuePair<string, AudioSource> kvp in dictionaryAudio2)
		{
			kvp.Value.transform.parent = transform;
		}
		foreach(KeyValuePair<string, AudioSource> kvp in dictionaryAudio3)
		{
			kvp.Value.transform.parent = transform;
		}
		string music = PlayerPrefs.GetString("IsMusic", "null");
		if(music == "no")
		{
			musicOn = false;
			onMusic(0);
		}
		else
		{
			musicOn = true;
			onMusic(1f);
		}
		string sound = PlayerPrefs.GetString("IsSound", "null");
		if(sound == "no")
		{
			soundOn = false;
			onSound(0);
		}
		else
		{
			soundOn = true;
			onSound(1);
		}
	}

	public void play(string name)
	{
		if(dictionaryAudio[name] != null)
		{
			if(name.Contains("bgm") && !musicOn)
			{
			}
			else if(!soundOn && !name.Contains("bgm"))
			{
				return;
			}
			if(name.Contains("bgm") && name != keyBgm)
			{
				if (dictionaryAudio.ContainsKey(keyBgm))
				{
					//dictionaryAudio[keyBgm].Stop();
					
					StartCoroutine(CrossFadeFromTo(dictionaryAudio[keyBgm], dictionaryAudio[name]));
				}
				
				keyBgm = name;
			}	
			if(name.Contains("bgm"))
			{
				if(!dictionaryAudio[name].isPlaying)
				{
					dictionaryAudio[name].Play();
				}
			}
			else
			{
				if(!dictionaryAudio[name].isPlaying)
				{
					dictionaryAudio[name].Play();
				}
				else if(!dictionaryAudio2[name].isPlaying)
				{
					dictionaryAudio2[name].Play();
				}
				else
				{
					dictionaryAudio3[name].Play();	
				}
			}
		}
	}
	
	IEnumerator CrossFadeFromTo(AudioSource fromAudio, AudioSource toAudio)
	{
		toAudio.volume = 0;
		
		//while 
		while (fromAudio.volume > 0)
		{
			fadeOut(fromAudio);
			fadeIn(toAudio);
			yield return null;
		}
						
		fromAudio.Stop();
		
		yield break;
	}
	
	void fadeIn(AudioSource audio)
	{
		if (audio.volume < 1)
		{
			audio.volume += 0.5f * Time.deltaTime;
		}
	}
	
	void fadeOut(AudioSource audio)
	{
		if (audio.volume > 0)
		{
			audio.volume += -0.5f * Time.deltaTime;
		}
	}

	public void stop(string name)
	{
		dictionaryAudio[name].Stop();
	}

	public void onMusic(float volume)
	{
		dictionaryAudio["titlebgm"].volume = volume;
		dictionaryAudio["battlebgm"].volume = volume;

		if(volume == 0)
		{
			musicOn = false;
		}
		else
		{
			musicOn = true;
		}
	}

	public void onSound(float volume)
	{
		/*
		dictionaryAudio["merge"].volume = volume;
		dictionaryAudio["hit"].volume = volume;
		*/

		if(volume == 0)
		{
			soundOn = false;
		}
		else
		{
			soundOn = true;
		}
	}
}
