﻿using UnityEngine;
using System.Collections;

public class PersistentWorldMap : MonoBehaviour {
	
	public static PersistentWorldMap instance;
	public Transform levelContainer;
	
	void Awake()
	{
		if(instance != null)
		{
			DestroyImmediate(instance.gameObject);
		}
		
		instance = this;
		DontDestroyOnLoad(gameObject);
		levelContainer.gameObject.SetActive(true);
		
		OnLevelWasLoaded();
	}
	
	public void hide()
	{
		gameObject.SetActive(false);
	}
	
	public void show()
	{
		gameObject.SetActive(true);
	}
	
	void OnLevelWasLoaded()
	{
		if(Application.loadedLevelName.Contains("level"))
		{
			Destroy(instance.gameObject);
		}
	}
}
