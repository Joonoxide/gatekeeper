using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Fury.Database;

public class CyclopsWeapon : Weapon
{
	public override void OnAttackComplete(Fury.Behaviors.Unit attacker, Fury.Behaviors.Unit target)
	{
		target.ModifyHealth(-(Damage), attacker, this);
		
		if (UnityEngine.Random.value > 0.6f)
		{
			GameObject popup = Instantiate(GameController.instance.popUpTextGO, attacker.transform.position + new Vector3(0, 12, 1), Quaternion.identity) as GameObject;
			
			PackedSprite sprite = popup.GetComponent<PackedSprite>(); 
			
			sprite.PlayAnim(0, (int)ToonPopOutAnim.Swing);
		}
		
	}
	
}