﻿using UnityEngine;
using System.Collections;

public class LinearProjectile : MonoBehaviour {
	
	public Fury.Behaviors.Unit attacker;
	public Fury.Database.Weapon weapon;
	public Fury.Behaviors.Unit target;
	public delegate void FP(Fury.Behaviors.Unit attacker, Fury.Behaviors.Unit target);
	public FP fp;
	
	private Vector3 currPoint, destPoint;
	private float speed, lifetime;
	private Vector3 vec_movement;
	
	private const float LIFETIME_LIMIT = 10f;
	
	// Use this for initialization
	void Start () {
		
		//startPoint = transform.position;
		lifetime += Time.deltaTime;
		speed = 15f;
	
	}
	
	// Update is called once per frame
	void Update () {
		
		lifetime += Time.deltaTime;
		
		if (lifetime >= LIFETIME_LIMIT)
		{
			//destroy the projectile
			Destroy(gameObject);
			return;
		}
		
		if (target)
		{
			currPoint = transform.position;
			destPoint = target.transform.position + new Vector3(0, 5, 0); //to target the center of the unit
			
			vec_movement = destPoint - currPoint;
			vec_movement.Normalize();
		}
		
		else
		{
			//destroy the projectile
			Destroy(gameObject);
		}
		
		//if the projectile arrives within the next time step i.e. speed * Time.deltaTime
		if ((destPoint - currPoint).magnitude < speed * Time.deltaTime)
		{
			//if target and attacker is still valid, fire event
			if (target && attacker)
			{
				fp(attacker, target);
			}
			
			//destroy the projectile
			Destroy(gameObject);
		}
	
		else
		{
			transform.position += vec_movement.normalized * speed * Time.deltaTime;
		}
	}
}
