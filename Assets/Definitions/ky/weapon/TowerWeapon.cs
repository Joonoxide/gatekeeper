using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;
using Fury.Database;

class TowerWeapon : Fury.Database.Weapon
{
	public override void OnAttackComplete(Fury.Behaviors.Unit attacker, Fury.Behaviors.Unit target)
	{	
		var position = attacker.transform.position;
		position.y += 2;
		position.z -= 5;
		GameObject arrow = GameController.instance.instantiateGO(GameController.instance.arrowGO, position);
		Vector3 euler = new Vector3(0, -90, 0);
		arrow.transform.eulerAngles = euler;
		var projectile = arrow.AddComponent<CurvedProjectile>();
		projectile.weapon = this;
		projectile.attacker = attacker;
		projectile.target = target;
		projectile.fp = new CurvedProjectile.FP(onArrived);
		sfx.instance.play("arrow");
	}

	public void onArrived(Fury.Behaviors.Unit attacker, Fury.Behaviors.Unit target)
	{
		if(target && attacker)
		{
			int damage = (int)(Damage*(1f+0.3f*attacker.agent.level));
			target.ModifyHealth(-damage, attacker, this);
			foreach(KYAgent.AbilityWrapper abilityWrapper in attacker.agent.listAbilityWrapper)
			{
				if(abilityWrapper.abilityType == AbilityType.PassiveAttack)
				{
					int rate = abilityWrapper.dictionaryAbilityData["Val1"];
					int rand = UnityEngine.Random.Range(0, 100);
					if(rand < rate)
					{
						attacker.Order(abilityWrapper.ability, target, Vector3.zero);
						break;
					}
				}
			}
		}
	}

}