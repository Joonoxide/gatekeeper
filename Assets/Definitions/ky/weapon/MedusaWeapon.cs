using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;
using Fury.Database;

class MedusaWeapon : Fury.Database.Weapon
{
	public override void OnAttackComplete(Fury.Behaviors.Unit attacker, Fury.Behaviors.Unit target)
	{	
		var position = attacker.transform.position;
		float facingModifier = 1.0f;
		
		//if the unit is facing -x, we flip the offset so that the projectile spawns on the right side (where the staff is pointing)
		if (attacker.agent.sprite.winding == (int)SpriteRoot.WINDING_ORDER.CCW)
		{
			facingModifier = -1.0f;
		}
		
		position.y += 2.5f;
		position.x -= facingModifier;
		
		GameObject arrow = GameController.instance.instantiateGO(GameController.instance.arrowGO, position);
		Vector3 euler = new Vector3(0, -90, 0);
		arrow.transform.eulerAngles = euler;
		var projectile = arrow.AddComponent<CurvedProjectile>();
		projectile.weapon = this;
		projectile.attacker = attacker;
		projectile.target = target;
		projectile.fp = new CurvedProjectile.FP(onArrived);
		sfx.instance.play("arrow");
	}

	public void onArrived(Fury.Behaviors.Unit attacker, Fury.Behaviors.Unit target)
	{
		if(target && attacker)
		{
			target.ModifyHealth(-Damage, attacker, this);
			foreach(KYAgent.AbilityWrapper abilityWrapper in attacker.agent.listAbilityWrapper)
			{
				if(abilityWrapper.abilityType == AbilityType.PassiveAttack)
				{
					int rate = abilityWrapper.dictionaryAbilityData["Val1"];
					int rand = UnityEngine.Random.Range(0, 100);
					if(rand < rate)
					{
						attacker.Order(abilityWrapper.ability, target, Vector3.zero);
						break;
					}
				}
			}
		}
	}

}