using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Fury.Database;

public class PaladinWeapon : Weapon
{
	public override void OnAttackComplete(Fury.Behaviors.Unit attacker, Fury.Behaviors.Unit target)
	{
		target.ModifyHealth(-(Damage), attacker, this);
		
		//TO DO: hammer (blunt) sfx?
		sfx.instance.play("sword1");
	}
	
}