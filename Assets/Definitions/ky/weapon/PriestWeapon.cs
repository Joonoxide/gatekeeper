﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;
using Fury.Database;


public class PriestWeapon : Weapon
{
	public override void OnAttackComplete(Fury.Behaviors.Unit attacker, Fury.Behaviors.Unit target)
	{
		target.ModifyHealth(-(Damage), attacker, this);
		sfx.instance.play("priestAttack");
	}	
}