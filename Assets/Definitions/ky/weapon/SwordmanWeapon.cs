using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

using Fury.Database;

public class SwordmanWeapon : Weapon
{
	public override void OnAttackComplete(Fury.Behaviors.Unit attacker, Fury.Behaviors.Unit target)
	{
		target.ModifyHealth(-(Damage), attacker, this);
		sfx.instance.play("sword2");
		
		if (UnityEngine.Random.value > 0.7f)
		{
			GameObject popup = Instantiate(GameController.instance.popUpTextGO, attacker.transform.position + new Vector3(0, 7, 1), Quaternion.identity) as GameObject;
			
			PackedSprite sprite = popup.GetComponent<PackedSprite>(); 
			
			sprite.PlayAnim(0, (int)ToonPopOutAnim.Slash);
		}
		
	}	
}