using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

class GolemWeapon : Fury.Database.Weapon
{
	public override void OnAttackComplete(Fury.Behaviors.Unit attacker, Fury.Behaviors.Unit target)
	{	
		/*if (attacker)
		{
			Debug.Log ("bomb end..." + Time.time);
		
			//explode because time ran out
			attacker.ModifyHealth(-99999, target, this);
			
			(attacker.agent as SuicidalAgent).suiciderState = SuicidalAgent.SuiciderState.TimedOut;
		}*/
	}
		
	
	public override void OnAttackBegin(Fury.Behaviors.Unit attacker, Fury.Behaviors.Unit target) 
	{
		Debug.Log ("bomb begin..." + Time.time);
		
		Fury.Database.Status status = GameController.instance.kamikazeBuff_bomb;
		
		if (status != null)
		{
			attacker.AddStatus(status, target);
		}
	}
}