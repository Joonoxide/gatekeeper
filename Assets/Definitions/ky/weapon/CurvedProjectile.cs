using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

public class CurvedProjectile : MonoBehaviour
{
	public Vector3 startPoint;
	float kahyen = -90f;
	float speed;
	public Vector3 dest;
	public Vector3 finalDest;
	public Fury.Behaviors.Unit attacker;
	public Fury.Database.Weapon weapon;
	public Fury.Behaviors.Unit target;
	public delegate void FP(Fury.Behaviors.Unit attacker, Fury.Behaviors.Unit target);
	public FP fp;
	public float vvv;
	public void Start()
	{
		startPoint = transform.position;
		finalDest = target.transform.position;
		vvv = (finalDest-startPoint).magnitude;
		if(finalDest.x-transform.position.x < 0)
		{
			kahyen = -90f;
		}
		else
		{
			kahyen = 90f;
		}
		transform.eulerAngles = new Vector3(270, kahyen, 0);
		if(attacker.agent is WalkableAgent)
		{
			speed = 15f;
		}
		else
		{
			speed = 15f;
		}
		index = 0;
	}

	int index = 0;
	public void Update()
	{
		if(target)
		{
			dest = target.transform.position;
		}
		
		else
		{
			Destroy(gameObject);
			return;
		}

		Vector3 distance = (dest-transform.position);
		distance.y = 0;
		float rate;
		if(index == 0)
		{
			rate = 1-(distance.magnitude)/(vvv);
			dest.y = Mathf.Lerp(0, 18, 1-rate);
			if(rate > 0.5f)
			{
				index = 1;
			}
		}
		else
		{
			rate = 1-(distance.magnitude)/(vvv);
			dest.y = Mathf.Lerp(18, 3, rate);

		}
		distance = (dest-transform.position);

		transform.forward = Vector3.Lerp( transform.forward, distance*speed, Time.deltaTime );
		float yy = distance.y;
		distance.y = 0;
		if (distance.magnitude < speed*Time.deltaTime)
		{
			if(target && attacker)
			{
				fp(attacker, target);
			}
			Destroy(gameObject);
		}
		else
		{		
			distance.y = yy;
			transform.position += distance.normalized*speed*Time.deltaTime;
		}
		
	}
}