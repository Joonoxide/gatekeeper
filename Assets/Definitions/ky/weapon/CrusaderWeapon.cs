using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Fury.Database;

public class CrusaderWeapon : Weapon
{
	public override void OnAttackComplete(Fury.Behaviors.Unit attacker, Fury.Behaviors.Unit target)
	{
		target.ModifyHealth(-(Damage), attacker, this);
		sfx.instance.play("sword1");
	}
	
}