using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Fury.Database;

public class HarvesterWeapon : Weapon
{
	public override void OnAttackComplete(Fury.Behaviors.Unit attacker, Fury.Behaviors.Unit target)
	{
		target.ModifyHealth(-(Damage), attacker, this);
		HarvestingAgent harvestingAgent = attacker.agent as HarvestingAgent;
		//Debug.Log(GameController.instance.arrayPlayer[attacker.Owner.Identifier].harvesterLevel);
		harvestingAgent.bag += target.level+1;
		harvestingAgent.chopCount += 1;
		sfx.instance.play("chopWood");
	}
	
}