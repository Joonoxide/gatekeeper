using System;
using System.Collections.Generic;

using UnityEngine;

class Resource : Fury.Database.Unit
{
	public int bounty;
	
	public override void OnDead(Fury.Behaviors.Unit corpse, Fury.Behaviors.Unit killer)
	{
		
	}
	
	public override void OnCreated (Fury.Behaviors.Unit created)
	{
		if (created.name.Contains("Resource"))
		{
			GameObject popup = Instantiate(GameController.instance.popUpTextGO, created.transform.position + new Vector3(0, 7, 1), Quaternion.identity) as GameObject;
			
			PackedSprite sprite = popup.GetComponent<PackedSprite>(); 
			
			sprite.PlayAnim(0, (int)ToonPopOutAnim.Poof);
		}
			
	}
}