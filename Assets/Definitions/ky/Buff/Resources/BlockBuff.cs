using UnityEngine;
using System.Collections;
using Fury;
using System;

public class BlockBuff : CustomGradedStatus, Fury.Database.Status.IHealthChange {
	
	public Single GetModifier(Fury.Behaviors.Unit target, Fury.Behaviors.Unit changer, Fury.Database.ChangeSource source, Int32 baseAmount)
	{
		//if healing, do nothing
		if (baseAmount > 0)
		{
			return 1.0f;
		}
		
		if (UnityEngine.Random.value >= effectMagnitudeByRank[effectiveRank])
		{
			//block failed
			Debug.Log ("Block failed!");
			return 1.0f;
		}
		
		if (target)
		{
			GameObject popup = Instantiate(GameController.instance.popUpTextGO, target.transform.position + new Vector3(0, 7, 1), Quaternion.identity) as GameObject;
			
			PackedSprite sprite = popup.GetComponent<PackedSprite>(); 
			
			sprite.PlayAnim(0, (int)ToonPopOutAnim.Block);
			
			sfx.instance.play("crusaderBlock" + (UnityEngine.Random.value > 0.5f? "1" : "2"));
			
			return 0f; 
		}
		
		return 1.0f;
		
		
	}
	
	public override System.Object OnStatusAdded(Fury.Behaviors.Unit target, Fury.Behaviors.Unit caster) 
	{ 
		/*if (target && caster)
		{
			PackedSprite indicator = (target.agent as WalkableAgent).statusIndicator;
			
			Debug.Log (indicator);
			if (indicator)
			{
				
				indicator.DoAnim(effectIndex);
				indicator.gameObject.SetActive(true);
			}
			
			Debug.Log(target.name + " has increased defense from " + caster.name + "!");
		}*/
		
		effectiveRank = (int)(target.level / 10);
			
		return null; 
	}
	
	public override void OnStatusRemoved(System.Object tag, Fury.Behaviors.Unit target, Fury.Behaviors.Unit caster) 
	{
		/*if (target && caster)
		{
			PackedSprite indicator = (target.agent as WalkableAgent).statusIndicator;
			
			if (indicator)
			{
				indicator.gameObject.SetActive(false);
			}
			
			Debug.Log(target.name + "'s defense buff has expired! [" + this.name + "]");
		}*/
			
	}
}
