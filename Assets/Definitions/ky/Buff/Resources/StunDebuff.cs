﻿using UnityEngine;
using System.Collections;
using Fury;
using System;

public class StunDebuff : CustomGradedStatus, Fury.Database.Status.IStun {
	
	public Boolean IsStunned(Fury.Behaviors.Unit caster, Fury.Behaviors.Unit target)
	{
		return true;
	}
	
	public override System.Object OnStatusAdded(Fury.Behaviors.Unit target, Fury.Behaviors.Unit caster) 
	{ 
		effectiveRank = (int)(caster.level / 10);
		
		if (target)
		{
			PackedSprite indicator = (target.agent as WalkableAgent).statusIndicator;
			
			if (indicator)
			{
				indicator.gameObject.SetActive(true);
				indicator.PlayAnim(0, effectIndex);
			}
			
			Debug.Log(target.name + " has decreased speed from " + caster.name + "!");
		}
		
		this._Duration = effectMagnitudeByRank[effectiveRank];
		
		return null; 
	}
	
	public override void OnStatusRemoved(System.Object tag, Fury.Behaviors.Unit target, Fury.Behaviors.Unit caster) 
	{
		if (target)
		{
			PackedSprite indicator = (target.agent as WalkableAgent).statusIndicator;
			
			if (indicator)
			{
				indicator.gameObject.SetActive(false);
			}
			
			Debug.Log(target.name + "'s speed debuff has expired! [" + this.name + "]");
		}
			
	}
}
