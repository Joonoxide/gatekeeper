﻿using UnityEngine;
using System.Collections;
using Fury;
using System;

public class StoneDebuff : CustomGradedStatus, Fury.Database.Status.ISpeed, Fury.Database.Status.IASpeed {
	
	public Single GetModifier(Fury.Behaviors.Unit caster, Fury.Behaviors.Unit target)
	{
		return 0f; //completely stops all movement and attack
	}
	
	public override System.Object OnStatusAdded(Fury.Behaviors.Unit target, Fury.Behaviors.Unit caster) 
	{ 
		if (target && caster)
		{
			PackedSprite indicator = (target.agent as WalkableAgent).statusIndicator;
			
			if (indicator)
			{
				indicator.gameObject.SetActive(true);
				indicator.PlayAnim(0, effectIndex);
			}
			
			Debug.Log(target.name + " has decreased speed from " + caster.name + "!");
		}
		
		effectiveRank = (int)(caster.level / 10);
			
		return null; 
	}
	
	public override void OnStatusRemoved(System.Object tag, Fury.Behaviors.Unit target, Fury.Behaviors.Unit caster) 
	{
		if (target && caster)
		{
			PackedSprite indicator = (target.agent as WalkableAgent).statusIndicator;
			
			if (indicator)
			{
				indicator.gameObject.SetActive(false);
			}
			
			Debug.Log(target.name + "'s speed debuff has expired! [" + this.name + "]");
		}
			
	}
}
