using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class LevelBuff : Fury.Database.Status, Fury.Database.Status.IHealth, Fury.Database.Status.IHealthChangeCaused, Fury.Database.Status.IASpeed
{	
	int Fury.Database.Status.IHealth.GetModifier (Fury.Behaviors.Unit from, Fury.Behaviors.Unit target)
	{
		int level = from.agent.level;
		
		if(from.Owner.Identifier == 0 && from.agent is WalkableAgent)
		{
			level += GameController.instance.arrayPlayer[0].defendAuraLevel;
		}
		if(from.agent is HarvestingAgent)
		{
			return (int)(from.Controllers.VitalityController.MaxHealth*(level)*0.5f);
		}
		if(from.Owner.Identifier == 1)
		{
			if(from.agent is TowerAgent)
			{
				return LevelManager.instance.tower1HP;
			}
			else if(from.agent is TowerAgent2)
			{
				return LevelManager.instance.tower2HP;
			}
			else if(from.agent is TowerAgent3)
			{
				return LevelManager.instance.tower3HP;
			}
		}
		
		return (int)(from.Controllers.VitalityController.MaxHealth*(level) * GameSettings.HP_INCREMENT_PER_LEVEL);
		//return (int)(from.Controllers.VitalityController.MaxHealth*(level)*0.2f);
		//return 0;
	}

	float Fury.Database.Status.IHealthChangeCaused.GetModifier(Fury.Behaviors.Unit from,Fury.Behaviors.Unit target, Fury.Database.ChangeSource weapon,System.Int32 baseAmount)
	{
		if(from.agent is KYTowerAgent)
		{
			return 1;
		}
		else if (from.agent is WalkableAgent)
		{
			float level = from.agent.level;
			if(from.Owner.Identifier == 0)
			{
				level += GameController.instance.arrayPlayer[0].damageAuraLevel;
			}
			//return 1+(level*0.2f);
			return 1 + (level * GameSettings.DMG_INCREMENT_PER_LEVEL);
		}	
		return 1;	
	}

	Single Fury.Database.Status.IASpeed.GetModifier(Fury.Behaviors.Unit from, Fury.Behaviors.Unit target)
	{
		float level = from.agent.level;
		if(from.agent is KYTowerAgent)
		{
			return 1+(level*2f);
		}
		else
		{
			return 1;
		}
	}
}