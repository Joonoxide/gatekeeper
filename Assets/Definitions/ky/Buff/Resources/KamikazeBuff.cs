﻿using UnityEngine;
using System.Collections;
using Fury;
using System;

public class KamikazeBuff : CustomGradedStatus {
	
	public override void OnStatusRemoved(System.Object tag, Fury.Behaviors.Unit target, Fury.Behaviors.Unit caster) 
	{
		if (target)
		{
			//explode because time ran out
			target.ModifyHealth(-99999, caster, this);
			
			//play sfx
			sfx.instance.play("explosion");
			
			(target.agent as SuicidalAgent).suiciderState = SuicidalAgent.SuiciderState.TimedOut;
			
			int damage = target.Controllers.WeaponController.Properties.Damage;
			
			//apply damage to surrounding units
			foreach(KYAgent enemy in target.agent.arrayNearbyEnemyAgent)
			{
				if (isWithinRange(target.transform.position, enemy.transform.position, effectMagnitudeByRank[effectiveRank]))
				{
					enemy.unit.ModifyHealth(-damage, target, this);
				}
				
			}
		}
	}
	
	public override System.Object OnStatusAdded(Fury.Behaviors.Unit target, Fury.Behaviors.Unit caster) 
	{ 
		if (target)
		{
			PackedSprite indicator = (target.agent as WalkableAgent).statusIndicator;
			
			if (indicator)
			{
				indicator.gameObject.SetActive(true);
				indicator.PlayAnim(0, effectIndex);
				
			}
			
			Debug.Log(target.name + " has lighted up the fuse!");
		}
		
		effectiveRank = (int)(target.level / 10);
			
		return null; 
	}
	
	
	bool isWithinRange(Vector3 source, Vector3 target, float range)
	{	
		return (target - source).magnitude <= range;
	}
}
