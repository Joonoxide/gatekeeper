using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class TowerLevelBuff : Fury.Database.Status, Fury.Database.Status.IHealthChange
{	
	float Fury.Database.Status.IHealthChange.GetModifier(Fury.Behaviors.Unit target,Fury.Behaviors.Unit from, Fury.Database.ChangeSource weapon,System.Int32 baseAmount)
	{
		if(baseAmount >= 0)
		{
			return 1;
		}
		GameObject door = GameController.instance.door[target.Owner.Identifier].gameObject;
		if(!door.GetComponent<ShakeEffect>())
		{
			door.AddComponent<ShakeEffect>().effect = 0.1f;
		}
		return 1;
	}
}