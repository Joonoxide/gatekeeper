﻿using UnityEngine;
using System.Collections;
using Fury;
using System;

public class DefenseBuff : CustomGradedStatus, Fury.Database.Status.IHealthChange {
	
	public Single GetModifier(Fury.Behaviors.Unit target, Fury.Behaviors.Unit changer, Fury.Database.ChangeSource source, Int32 baseAmount)
	{	
		//if healing, do nothing
		if (baseAmount > 0)
		{
			return 1.0f;
		}
		
		return 1 - effectMagnitudeByRank[effectiveRank];
	}
	
	public override System.Object OnStatusAdded(Fury.Behaviors.Unit target, Fury.Behaviors.Unit caster) 
	{ 
		if (target && caster)
		{
			PackedSprite indicator = (target.agent as WalkableAgent).statusIndicator;
			
			if (indicator)
			{
				indicator.gameObject.SetActive(true);
				indicator.PlayAnim(0, effectIndex);
			}
			
			Debug.Log(target.name + " has increased defense from " + caster.name + "!");
		}
		
		effectiveRank = (int)(caster.level / 10);
			
		return null; 
	}
	
	public override void OnStatusRemoved(System.Object tag, Fury.Behaviors.Unit target, Fury.Behaviors.Unit caster) 
	{
		if (target)
		{
			PackedSprite indicator = (target.agent as WalkableAgent).statusIndicator;
			
			if (indicator)
			{
				indicator.gameObject.SetActive(false);
			}
			
			Debug.Log(target.name + "'s defense buff has expired! [" + this.name + "]");
		}
			
	}
}
