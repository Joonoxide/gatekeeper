﻿using UnityEngine;
using System.Collections;
using Fury;
using System;

public class SlowDebuff : CustomGradedStatus, Fury.Database.Status.ISpeed, Fury.Database.Status.IASpeed {
	
	public Single GetModifier(Fury.Behaviors.Unit caster, Fury.Behaviors.Unit target)
	{
		return 1 - effectMagnitudeByRank[effectiveRank];
	}
	
	public override System.Object OnStatusAdded(Fury.Behaviors.Unit target, Fury.Behaviors.Unit caster) 
	{ 
		effectiveRank = (int)(caster.level / 10);
		
		if (target && caster)
		{
			PackedSprite indicator = (target.agent as WalkableAgent).statusIndicator;
			
			if (indicator)
			{
				indicator.gameObject.SetActive(true);
				indicator.PlayAnim(0, effectIndex);
			}
			
			SkeletonAnimation skeletonAnimation = (target.agent.sprite as KYAgent.SpineSpriteWrapper).sprite;
			skeletonAnimation.timeScale = 1 - effectMagnitudeByRank[effectiveRank];
			
			Debug.Log(target.name + " has decreased speed from " + caster.name + "!");
		}
		
		
			
		return null; 
	}
	
	public override void OnStatusRemoved(System.Object tag, Fury.Behaviors.Unit target, Fury.Behaviors.Unit caster) 
	{
		if (target)
		{
			PackedSprite indicator = (target.agent as WalkableAgent).statusIndicator;
			
			if (indicator)
			{
				indicator.gameObject.SetActive(false);
				
				SkeletonAnimation skeletonAnimation = (target.agent.sprite as KYAgent.SpineSpriteWrapper).sprite;
				skeletonAnimation.timeScale = 1;
			}
			
			Debug.Log(target.name + "'s speed debuff has expired! [" + this.name + "]");
		}
			
	}
}
