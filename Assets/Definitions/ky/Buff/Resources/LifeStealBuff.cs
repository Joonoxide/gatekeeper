﻿using UnityEngine;
using System.Collections;
using Fury;
using System;

public class LifeStealBuff : CustomGradedStatus, Fury.Database.Status.IHealthChangeCaused {
	
	public Single GetModifier(Fury.Behaviors.Unit changer, Fury.Behaviors.Unit target, Fury.Database.ChangeSource source, Int32 baseAmount)
	{
		if (changer && target && (target.agent is WalkableAgent))
		{
			int drainAmount = (int)(Math.Min(target.Controllers.VitalityController.Health, baseAmount) * effectMagnitudeByRank[effectiveRank]) * -1;
		
			changer.ModifyHealth(drainAmount, target, source);
			
			Debug.Log(changer.name + " drained " + drainAmount + "HP from " + target + "!");
		}
		
		return 1.0f; //return 1.0f (no effect) because we only want to know how much damage was dealt and to whom
	}
	
	public override System.Object OnStatusAdded(Fury.Behaviors.Unit target, Fury.Behaviors.Unit caster) 
	{ 
		if (target && caster)
		{
			PackedSprite indicator = (target.agent as WalkableAgent).statusIndicator;
			
			if (indicator)
			{
				
				indicator.gameObject.SetActive(true);
				indicator.PlayAnim(0, effectIndex);
			}
			
			Debug.Log(target.name + " has life drain enabled from " + caster.name + "!");
		}
			
		Debug.Log ("Life drain enabled!");
		
		effectiveRank = (int)(target.level / 10);
		
		return null; 
	}
	
	public override void OnStatusRemoved(System.Object tag, Fury.Behaviors.Unit target, Fury.Behaviors.Unit caster) 
	{
		if (target)
		{
			PackedSprite indicator = (target.agent as WalkableAgent).statusIndicator;
			
			if (indicator)
			{
				indicator.gameObject.SetActive(false);
			}
			
			Debug.Log(target.name + "'s defense buff has expired! [" + this.name + "]");
		}
			
	}
}
