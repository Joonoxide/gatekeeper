using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class PassiveBash : Fury.Database.Ability
{
	public override object OnBeginCast(Fury.Behaviors.Unit caster, Fury.Behaviors.Targetable target, UnityEngine.Vector3 position)
	{
		if(target)
		{
			GameObject stunGO = GameController.instance.instantiateGO(GameController.instance.stunGO);
			stunGO.transform.position = target.transform.position;
			Vector3 newPos = stunGO.transform.localPosition;
			newPos.y += (target as Fury.Behaviors.Unit).Properties.Height*10;
			stunGO.transform.localPosition = newPos;
			stunGO.transform.parent = target.transform;
			stunGO.GetComponent<StunEffect>().duration = 3f;	
			(target as Fury.Behaviors.Unit).AddStatus(GameController.instance.stunBuff, (target as Fury.Behaviors.Unit));
		}
		return null;
	}
	
	public override void OnEndCast(object tag, Fury.Behaviors.Unit caster, Fury.Behaviors.Targetable target, UnityEngine.Vector3 position)
	{
	}
}
