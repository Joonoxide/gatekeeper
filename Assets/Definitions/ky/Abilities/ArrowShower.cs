using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ArrowShower : MonoBehaviour
{
	public float elapsed;
	public Fury.Database.Ability source;
	public Fury.Behaviors.Unit from;
	public Fury.Behaviors.Unit target;
	public Vector3 dest;

	void Start () 
	{
		elapsed = 0;
	}
	
	// Update is called once per frame
	void Update () 
	{
		elapsed += Time.deltaTime;
		if(elapsed > 1)
		{
			if(from)
			{
				KYAgent agent = from.agent;
				Fury.Behaviors.Commander cmdr = Fury.Behaviors.Manager.Instance.Commanders[(from.Owner.Identifier == 0)? 1 : 0];
				//foreach (var unit in cmdr.Units)
				foreach(KeyValuePair<int, Fury.Behaviors.Unit> unitData in cmdr.Units)
				{
					Fury.Behaviors.Unit unit = unitData.Value;
					
					if ((unit.transform.position-dest).magnitude < 5)
					{
						unit.ModifyHealth(-from.Controllers.WeaponController.Properties.Damage*2, from, source);
					}
				}	
			}
			Destroy(gameObject);
		}
	}
}
