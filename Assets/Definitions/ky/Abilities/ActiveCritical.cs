using UnityEngine;
using System.Collections;
using System;

public class ActiveCritical : CustomGradedAbility, AISkillInterface
{
	public override object OnBeginCast(Fury.Behaviors.Unit caster, Fury.Behaviors.Targetable target, UnityEngine.Vector3 position)
	{
		KYAgent agent = caster.agent;
		agent.sprite.DoAnim("critical");
		KYAgent.AbilityWrapper abilityWrapper = agent.abilityForAbilityWrapper(this);
		abilityWrapper.cooldown = abilityWrapper.fixedCooldown;
		abilityWrapper.cooldownLeft = abilityWrapper.cooldown;
		return null;
	}

	public override void OnEndCast(object tag, Fury.Behaviors.Unit caster, Fury.Behaviors.Targetable target, UnityEngine.Vector3 position)
	{
		if(caster && target)
		{
			KYAgent agent = caster.agent;
			int damage = agent.unit.Controllers.WeaponController.Properties.Damage*3;
			(target as Fury.Behaviors.Unit).ModifyHealth(-damage, caster, this);
			/*GameObject newTextGO = GameController.instance.instantiateGO(GameController.instance.textGO);
			SpriteText spriteText = newTextGO.GetComponent<SpriteText>();
			spriteText.text = "!!!";
			spriteText.Color = Color.red;
			Vector3 pos = caster.transform.position;
			pos.y += caster.Properties.Height;
			newTextGO.transform.position = pos;*/
			
			GameObject popup = Instantiate(GameController.instance.popUpTextGO, caster.transform.position + new Vector3(0, 7, 1), Quaternion.identity) as GameObject;
			
			PackedSprite sprite = popup.GetComponent<PackedSprite>(); 
			
			sprite.PlayAnim(0, (int)ToonPopOutAnim.Critical);
		}
	}

	public void aiExecute(KYAgent caster, KYAgent target)
	{
		float chanceToExecute = isFixedChance? baseChance: chanceByRank[(caster.unit.level / 10)];
		float rand = UnityEngine.Random.value;
		
		if (rand > chanceToExecute)
		{
			return;
		}
		
		if(caster.unit.Controllers.WeaponController.State == Fury.Controllers.WeaponController.States.PostDelay)
		{
			caster.unit.Order(this, target.unit, Vector3.zero);
		}
	}
	
	public void aiExecute(KYAgent caster)
	{
		//if(caster.unit.Controllers.WeaponController.State == Fury.Controllers.WeaponController.States.PostDelay)
		//{
			caster.unit.Order(this, null, Vector3.zero);
		//}
	}
}
