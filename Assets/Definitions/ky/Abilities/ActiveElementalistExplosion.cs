using UnityEngine;
using System.Collections;
using System;

public class ActiveElementalistExplosion : CustomGradedAbility, AISkillInterface
{
	public float blastRadius = 10f;
	public int damage = 50;
	
	public override object OnBeginCast(Fury.Behaviors.Unit caster, Fury.Behaviors.Targetable target, UnityEngine.Vector3 position)
	{
		caster.agent.sprite.DoAnim("attack", false);
		return null;
	}

	public override void OnEndCast(object tag, Fury.Behaviors.Unit caster, Fury.Behaviors.Targetable target, UnityEngine.Vector3 position)
	{
		float facingModifier = 1.0f;
		
		//if the unit is facing -x, we flip the offset so that the projectile spawns on the right side (where the staff is pointing)
		if (caster.agent.sprite.winding == (int)SpriteRoot.WINDING_ORDER.CCW)
		{
			facingModifier = -1.0f;
		}
		
		var pos = caster.transform.position;
		pos.x -= facingModifier;
		pos.y += 3.5f;
		GameObject fireball = GameController.instance.instantiateGO(GameController.instance.fireballGO, pos);
		//Vector3 euler = new Vector3(0, -90, 0);
		//arrow.transform.eulerAngles = euler;
		var projectile = fireball.AddComponent<LinearProjectile>();
		projectile.attacker = caster;
		projectile.target = target as Fury.Behaviors.Unit;
		projectile.fp = new LinearProjectile.FP(onArrived);
		sfx.instance.play("fireball"); // TO DO: replace with correct sfx for fireball
	}
	
	public void onArrived(Fury.Behaviors.Unit attacker, Fury.Behaviors.Unit target)
	{
		if(target && attacker)
		{
			target.ModifyHealth(-damage, attacker, this);
			
			//play sfx
			sfx.instance.play("explosion");
			
			foreach (KYAgent agent in target.agent.arrayNearbyAllyAgent)
			{
				if (isWithinRange(target.transform.position, agent.transform.position, blastRadius))
				{
					agent.unit.ModifyHealth(-damage, attacker, this);
				}
			}
			
			GameObject explosion = GameController.instance.instantiateGO(GameController.instance.explosionGO, target.transform.position + new Vector3(1, 6, 1));
			
			GameController.instance.waitAndExecute(2.0f, delegate() {
				explosion.AddComponent<Fadeout>();
			});
		}
	}

	public void aiExecute(KYAgent caster, KYAgent target)
	{
		float chanceToExecute = isFixedChance? baseChance: chanceByRank[(caster.unit.level / 10)];
		float rand = UnityEngine.Random.value;
		
		if (rand > chanceToExecute)
		{
			return;
		}
		
		if(caster.unit.Controllers.WeaponController.State == Fury.Controllers.WeaponController.States.PostDelay)
		{
			caster.unit.Order(this, target.unit, target.transform.position);
		}
		
	}
	
	public void aiExecute(KYAgent caster)
	{
		if(caster.unit.Controllers.WeaponController.State == Fury.Controllers.WeaponController.States.PostDelay)
		{
			caster.unit.Order(this, null, Vector3.zero);
		}
	}
	
	bool isWithinRange(Vector3 source, Vector3 target, float range)
	{	
		return (target - source).magnitude <= range;
	}
}
