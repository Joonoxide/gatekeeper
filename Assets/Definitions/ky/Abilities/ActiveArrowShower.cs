using UnityEngine;
using System.Collections;
using System;

public class ActiveArrowShower : CustomGradedAbility, AISkillInterface
{
	public override object OnBeginCast(Fury.Behaviors.Unit caster, Fury.Behaviors.Targetable target, UnityEngine.Vector3 position)
	{
		KYAgent agent = caster.agent;
		agent.sprite.DoAnim("heavy_attack");
		KYAgent.AbilityWrapper abilityWrapper = agent.abilityForAbilityWrapper(this);
		abilityWrapper.cooldown = abilityWrapper.fixedCooldown;
		abilityWrapper.cooldownLeft = abilityWrapper.cooldown;
		return null;
	}

	public override void OnEndCast(object tag, Fury.Behaviors.Unit caster, Fury.Behaviors.Targetable target, UnityEngine.Vector3 position)
	{
		if(target)
		{
			GameController.instance.waitAndExecute(0f, delegate()
			{
				if(caster && caster!= null && target && target != null)
				{
					GameObject arrowShower = GameController.instance.instantiateGO(GameController.instance.arrowShower);
					arrowShower.transform.position = target.transform.position;
					if(position.x < caster.transform.position.x)
					{
						arrowShower.transform.localScale = new Vector3(-1, 1, 1);
					}

					ArrowShower script = arrowShower.GetComponent<ArrowShower>();
					script.target = target as Fury.Behaviors.Unit;
					script.dest = target.transform.position;
					script.source = this;
					script.from = caster;					
				}
			});

			GameController.instance.waitAndExecute(1f, delegate()
			{
				if(caster && caster!= null && target && target != null)
				{
					GameObject arrowShower = GameController.instance.instantiateGO(GameController.instance.arrowShower);
					arrowShower.transform.position = target.transform.position;
					if(position.x < caster.transform.position.x)
					{
						arrowShower.transform.localScale = new Vector3(-1, 1, 1);
					}

					ArrowShower script = arrowShower.GetComponent<ArrowShower>();
					script.target = target as Fury.Behaviors.Unit;
					script.dest = target.transform.position;
					script.source = this;
					script.from = caster;					
				}
			});
		}
	}

	public void aiExecute(KYAgent caster, KYAgent target)
	{
		float chanceToExecute = isFixedChance? baseChance: chanceByRank[(caster.unit.level / 10)];
		float rand = UnityEngine.Random.value;
		
		if (rand > chanceToExecute)
		{
			return;
		}
		
		if(caster.unit.Controllers.WeaponController.State == Fury.Controllers.WeaponController.States.PostDelay && target is WalkableAgent)
		{
			caster.unit.Order(this, target.unit, target.transform.position);
		}
	}
	
	public void aiExecute(KYAgent caster)
	{
		if(caster.unit.Controllers.WeaponController.State == Fury.Controllers.WeaponController.States.PostDelay)
		{
			caster.unit.Order(this, null, Vector3.zero);
		}
	}
}
