using UnityEngine;
using System.Collections;
using System;

public class ActiveGriffinShout : CustomGradedAbility, AISkillInterface
{
	public override object OnBeginCast(Fury.Behaviors.Unit caster, Fury.Behaviors.Targetable target, UnityEngine.Vector3 position)
	{
		KYAgent agent = caster.gameObject.GetComponent<KYAgent>();
		agent.sprite.DoAnim("shout");
		KYAgent.AbilityWrapper abilityWrapper = agent.abilityForAbilityWrapper(this);
		abilityWrapper.cooldown = abilityWrapper.fixedCooldown;
		abilityWrapper.cooldownLeft = abilityWrapper.cooldown;
		
		GameObject popup = Instantiate(GameController.instance.popUpTextGO, caster.transform.position + new Vector3(0, 10, 1), Quaternion.identity) as GameObject;
			
		PackedSprite sprite = popup.GetComponent<PackedSprite>(); 
		
		sprite.PlayAnim(0, (int)ToonPopOutAnim.Wrrr);
		
		return null;
	}

	public override void OnEndCast(object tag, Fury.Behaviors.Unit caster, Fury.Behaviors.Targetable target, UnityEngine.Vector3 position)
	{
		KYAgent agent = caster.gameObject.GetComponent<KYAgent>();
		int damage = agent.unit.Controllers.WeaponController.Properties.Damage;
		Vector3 pos = agent.transform.position;
		bool isFront = false;
		foreach(KYAgent enemyAgent in agent.arrayNearbyEnemyAgent)
		{
			if(pos.x > enemyAgent.transform.position.x)
			{
				if(agent.sprite.winding == 1)
				{
					isFront = true;
				}
			}
			else if(pos.x < enemyAgent.transform.position.x)
			{
				if(agent.sprite.winding == 0)
				{
					isFront = true;
				}

			}
			if(isFront && (pos-enemyAgent.transform.position).magnitude < 15)
			{
				if(!enemyAgent.gameObject.name.Contains("Crystal"))
				{
					enemyAgent.unit.ModifyHealth(-damage, caster, this);				
				}
			}
		}
	}

	public void aiExecute(KYAgent caster, KYAgent target)
	{
		float chanceToExecute = isFixedChance? baseChance: chanceByRank[(caster.unit.level / 10)];
		float rand = UnityEngine.Random.value;
		
		if (rand > chanceToExecute)
		{
			return;
		}
		
		if(caster.unit.Controllers.WeaponController.State == Fury.Controllers.WeaponController.States.PostDelay && target is WalkableAgent)
		{
			caster.unit.Order(this, null, Vector3.zero);
		}
	}

	public void aiExecute(KYAgent caster)
	{
		if(caster.unit.Controllers.WeaponController.State == Fury.Controllers.WeaponController.States.PostDelay)
		{
			caster.unit.Order(this, null, Vector3.zero);
		}
	}
}
