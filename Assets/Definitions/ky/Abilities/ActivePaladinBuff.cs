using UnityEngine;
using System.Collections;
using System;

public class ActivePaladinBuff : Fury.Database.Ability, AISkillInterface
{
	public Fury.Database.Status status = null;
	
	public override object OnBeginCast(Fury.Behaviors.Unit caster, Fury.Behaviors.Targetable target, UnityEngine.Vector3 position)
	{
		KYAgent agent = caster.gameObject.GetComponent<KYAgent>();
		agent.unit.State = Fury.UnitStates.CastingAbility;
		agent.sprite.DoAnim("attack"); //TO DO: casting animation?
		
		KYAgent.AbilityWrapper abilityWrapper = agent.abilityForAbilityWrapper(this);
		abilityWrapper.cooldown = abilityWrapper.fixedCooldown;
		abilityWrapper.cooldownLeft = abilityWrapper.cooldown;
		//(caster as Fury.Behaviors.Unit).AddStatus(GameController.instance.shieldBuff, (caster as Fury.Behaviors.Unit));
		return null;
	}

	public override void OnEndCast(object tag, Fury.Behaviors.Unit caster, Fury.Behaviors.Targetable target, UnityEngine.Vector3 position)
	{
		if(caster && status != null)
		{
			foreach(KYAgent ally in caster.agent.arrayNearbyAllyAgent)
			{
				if(ally && ally is WalkableAgent)
				{
					ally.unit.AddStatus(status, caster);
				}
			}
			
			//buff self too
			caster.AddStatus(status, caster);
		}
	}

	public void aiExecute(KYAgent caster, KYAgent target)
	{
		//if(caster.unit.Controllers.WeaponController.State == Fury.Controllers.WeaponController.States.PostDelay)
		//{
			caster.unit.Order(this, null, Vector3.zero);
		//}
	}

	public void aiExecute(KYAgent caster)
	{
		//if(caster.unit.Controllers.WeaponController.State == Fury.Controllers.WeaponController.States.PostDelay)
		//{
			caster.unit.Order(this, null, Vector3.zero);
		//}
	}
}
