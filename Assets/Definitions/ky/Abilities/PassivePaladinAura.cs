﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Fury;

public class PassivePaladinAura : Fury.Database.Ability, AISkillInterface {

	public Fury.Database.Status status = null;
	
	private List<KYAgent> lastRecordedAllies = new List<KYAgent>();
	
	public override object OnBeginCast(Fury.Behaviors.Unit caster, Fury.Behaviors.Targetable target, UnityEngine.Vector3 position)
	{
		return null;
	}
	
	public override void OnEndCast(object tag, Fury.Behaviors.Unit caster, Fury.Behaviors.Targetable target, UnityEngine.Vector3 position)
	{
	}
	
	public void aiExecute(KYAgent caster, KYAgent target)
	{
		
	}
	
	public void aiExecute(KYAgent caster)
	{
		//exit early if a status was not assigned
		if (status == null)
		{
			return;
		}
		
		foreach (KYAgent ally in caster.arrayNearbyAllyAgent)
		{		
			if(caster.unit && ally.unit && ally is WalkableAgent)
			{	
				bool hasStatus = false;
				
				foreach (Fury.Controllers.StatusController.StatusInfo info in ally.unit.Controllers.StatusController.Statuses)
				{
					if (info.Properties == status)
					{
						hasStatus = true;
						break;
					}
				}
				
				//if unit does not have the status already
				if (!hasStatus)
				{
					if (isWithinRange(caster.unit, ally.unit))
					{
						ally.unit.AddStatus(status, caster.unit);
					}
					
				}
				
				else
				{
					//if the ally is not within range, remove the status and exit early
					if (!isWithinRange(caster.unit, ally.unit))
					{
						Debug.Log ("removing buff off " + ally.unit.name);
						ally.unit.RemoveStatus(status);
					}
				}
				
				//Debug.Log ("Paladdin casted Defense Buff on " + ally.unit.name + "!");
			}
		}
		
	}
	
	public List<KYAgent> getAffectedAllies()
	{
		return lastRecordedAllies;
	}
	
	bool isWithinRange(Fury.Behaviors.Unit caster, Fury.Behaviors.Unit target)
	{
		float dist = (caster.transform.position - target.transform.position).magnitude;
		return (dist <= this.CastRange);
	}
}
