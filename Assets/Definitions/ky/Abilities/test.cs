﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class test : MonoBehaviour {
	
	public class Unit
	{
		public bool isBookmarked = false;
		public int level = 1;
		public string name;
		
		public Unit()
		{
			
		}
	}
	void Start()
	{
		List<Unit> unitList = new List<Unit>();
		unitList.Add(new Unit() {level = 10, name = "a"});
		unitList.Add(new Unit() {name = "b"});
		unitList.Add(new Unit() {level = 5, name = "c"});
		unitList.Add(new Unit() {isBookmarked = true, name = "d"});
		unitList.Add(new Unit() {name = "e"});
		unitList.Add(new Unit() {name = "f"});
		unitList.Add(new Unit() {name = "g"});
		unitList.Add(new Unit() {name = "h"});
		unitList.Add(new Unit() {isBookmarked = true, level = 11, name = "i"});
		
		//sort the damn thing
		var sortedList = from unit in unitList orderby unit.isBookmarked descending, unit.level ascending select unit;
		
		Debug.Log (string.Join(", ", sortedList.Select(o => o.name.ToString()).ToArray()));
	}
}
