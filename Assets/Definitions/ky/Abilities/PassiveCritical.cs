using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class PassiveCritical : Fury.Database.Ability
{
	public override object OnBeginCast(Fury.Behaviors.Unit caster, Fury.Behaviors.Targetable target, UnityEngine.Vector3 position)
	{
		if(target)
		{
			GameObject newTextGO = GameController.instance.instantiateGO(GameController.instance.textGO);
			SpriteText spriteText = newTextGO.GetComponent<SpriteText>();
			spriteText.text = "CRIT!";
			spriteText.Color = Color.red;
			Vector3 pos = caster.transform.position;
			pos.y += caster.Properties.Height*10;
			newTextGO.transform.position = pos;
			float damage = caster.Controllers.WeaponController.Properties.Damage;
			(target as Fury.Behaviors.Unit).ModifyHealth(-((int)damage * 5), null, this);
		}
		return null;
	}
	
	public override void OnEndCast(object tag, Fury.Behaviors.Unit caster, Fury.Behaviors.Targetable target, UnityEngine.Vector3 position)
	{
	}
}
