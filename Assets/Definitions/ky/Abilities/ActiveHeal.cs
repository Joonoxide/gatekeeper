using UnityEngine;
using System.Collections;
using System;

public class ActiveHeal : CustomGradedAbility, AISkillInterface
{
	[SerializeField]
	private int [] amountHealedByRank;
	
	public override object OnBeginCast(Fury.Behaviors.Unit caster, Fury.Behaviors.Targetable target, UnityEngine.Vector3 position)
	{
		KYAgent agent = caster.agent;
		agent.unit.State = Fury.UnitStates.CastingAbility;
		agent.sprite.DoAnim("cast");
		KYAgent.AbilityWrapper abilityWrapper = agent.abilityForAbilityWrapper(this);
		abilityWrapper.cooldown = abilityWrapper.fixedCooldown;
		abilityWrapper.cooldownLeft = abilityWrapper.cooldown;
		sfx.instance.play("priestHealing");
		return null;
	}

	public override void OnEndCast(object tag, Fury.Behaviors.Unit caster, Fury.Behaviors.Targetable target, UnityEngine.Vector3 position)
	{
		if(caster)
		{
			int effectiveRank = (int)(caster.level / 10);
			
			int heal = amountHealedByRank[effectiveRank];
			Vector3 pos = new Vector3(0, 3, 0);
			foreach(KYAgent ally in caster.agent.arrayNearbyAllyAgent)
			{
				if(ally && ally is WalkableAgent)
				{
					bool isUndead = ally.name.Contains("Skeleton") || ally.name.Contains("Necromancer");
					
					if((caster.transform.position-ally.transform.position).magnitude < 15 && ally.unit.Controllers.VitalityController.HealthPercentage < 1)
					{
						//if the ally is an undead, instant death
						ally.unit.ModifyHealth(isUndead? -9999: heal, caster, this);
						GameObject healGO = GameController.instance.instantiateGO(GameController.instance.healPrefab);
						healGO.transform.parent = ally.transform;
						healGO.transform.localPosition = pos;
					}
				}
			}					
			caster.ModifyHealth(heal, caster, this);
			GameObject newHealGO = GameController.instance.instantiateGO(GameController.instance.healPrefab);
			newHealGO.transform.parent = caster.transform;
			newHealGO.transform.localPosition = pos;
		}
	}

	public void aiExecute(KYAgent caster, KYAgent target)
	{
		float chanceToExecute = isFixedChance? baseChance: chanceByRank[(caster.unit.level / 10)];
		float rand = UnityEngine.Random.value;
		
		if (rand > chanceToExecute)
		{
			return;
		}
		
		//if(caster.unit.Controllers.WeaponController.State == Fury.Controllers.WeaponController.States.PostDelay)
		//{
			caster.unit.Order(this, null, Vector3.zero);
		//}
	}
	
	public void aiExecute(KYAgent caster)
	{
		float chanceToExecute = isFixedChance? baseChance: chanceByRank[(caster.unit.level / 10)];
		float rand = UnityEngine.Random.value;
		
		if (rand > chanceToExecute)
		{
			return;
		}
		
		//if(caster.unit.Controllers.WeaponController.State == Fury.Controllers.WeaponController.States.PostDelay)
		//{
			caster.unit.Order(this, null, caster.transform.position);
		//}
	}
}
