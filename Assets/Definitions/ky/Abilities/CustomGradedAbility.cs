﻿using UnityEngine;
using System.Collections;

public class CustomGradedAbility : Fury.Database.Ability {
	
	[SerializeField]
	protected float [] chanceByRank;
	
	public bool isFixedChance = true;
	
	[SerializeField]
	protected float baseChance = 0f;
}
