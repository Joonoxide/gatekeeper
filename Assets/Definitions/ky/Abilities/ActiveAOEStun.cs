using UnityEngine;
using System.Collections;
using System;

public class ActiveAOEStun : CustomGradedAbility, AISkillInterface
{
	public override object OnBeginCast(Fury.Behaviors.Unit caster, Fury.Behaviors.Targetable target, UnityEngine.Vector3 position)
	{
		KYAgent agent = caster.agent;
		agent.sprite.DoAnim("critical");
		KYAgent.AbilityWrapper abilityWrapper = agent.abilityForAbilityWrapper(this);
		abilityWrapper.cooldown = abilityWrapper.fixedCooldown;
		abilityWrapper.cooldownLeft = abilityWrapper.cooldown;
		return null;
	}

	public override void OnEndCast(object tag, Fury.Behaviors.Unit caster, Fury.Behaviors.Targetable target, UnityEngine.Vector3 position)
	{
		Debug.Log("stun?");
		KYAgent agent = caster.agent;
		int damage = agent.unit.Controllers.WeaponController.Properties.Damage;
		Vector3 pos = agent.transform.position;
		
		CustomGradedStatus status = GameController.instance.stunDebuff_cyclops;
		
		Debug.Log (status);
		
		foreach(KYAgent enemyAgent in agent.arrayNearbyEnemyAgent)
		{
			if(enemyAgent is WalkableAgent && (pos-enemyAgent.transform.position).magnitude < 8)
			{
				(enemyAgent.unit).AddStatus(status, agent.unit);
				
				enemyAgent.unit.ModifyHealth(-damage, caster, this);
				//GameObject stunGO = GameController.instance.instantiateGO(GameController.instance.stunGO);
				//stunGO.transform.position = enemyAgent.transform.position;
				//Vector3 newPos = stunGO.transform.localPosition;
				//newPos.y += enemyAgent.unit.Properties.Height*10;
				//stunGO.transform.localPosition = newPos;
				////stunGO.transform.parent = enemyAgent.transform;
				//stunGO.GetComponent<StunEffect>().duration = 3f;
			}
		}
	}

	public void aiExecute(KYAgent caster, KYAgent target)
	{
		float chanceToExecute = isFixedChance? baseChance: chanceByRank[(caster.unit.level / 10)];
		float rand = UnityEngine.Random.value;
		
		if (rand > chanceToExecute)
		{
			return;
		}
		
		//if(caster.unit.Controllers.WeaponController.State == Fury.Controllers.WeaponController.States.PostDelay)
		//{
			caster.unit.Order(this, null, Vector3.zero);
		//}
	}

	public void aiExecute(KYAgent caster)
	{
		float chanceToExecute = isFixedChance? baseChance: chanceByRank[(caster.unit.level / 10)];
		float rand = UnityEngine.Random.value;
		
		if (rand > chanceToExecute)
		{
			return;
		}
		
		//if(caster.unit.Controllers.WeaponController.State == Fury.Controllers.WeaponController.States.PostDelay)
		//{
			caster.unit.Order(this, null, Vector3.zero);
		//}
	}
}
