using UnityEngine;
using System.Collections;
using System;

public class ActiveRaiseDead : CustomGradedAbility, AISkillInterface
{
	[SerializeField]
	private int [] maxSubSpawnsByRank;
	
	public override object OnBeginCast(Fury.Behaviors.Unit caster, Fury.Behaviors.Targetable target, UnityEngine.Vector3 position)
	{
		sfx.instance.play("necroRaiseDead");
		
		float radius = UnityEngine.Random.Range(3, 6);
		MinionController minionController = TowerAgent2.getInstanceForIndex(caster.Owner.Identifier).minionController;
		
		//minionController.CreateSpawn(Resources.Load ("Skeleton") as Fury.Database.Unit, 1, getValidSpawnSpot(radius, caster), caster.level, false);
		
		minionController.CreateSubSpawn(Resources.Load ("Skeleton") as Fury.Database.Unit, caster, getRandomSpawnSpot(radius, caster), caster.level);
		
		KYAgent agent = caster.agent;
		agent.sprite.DoAnim("summon");
		KYAgent.AbilityWrapper abilityWrapper = agent.abilityForAbilityWrapper(this);
		abilityWrapper.cooldown = abilityWrapper.fixedCooldown;
		abilityWrapper.cooldownLeft = abilityWrapper.cooldown;
		
		return null;
		
	}

	public override void OnEndCast(object tag, Fury.Behaviors.Unit caster, Fury.Behaviors.Targetable target, UnityEngine.Vector3 position)
	{
		
	}

	public void aiExecute(KYAgent caster, KYAgent target)
	{
		if (!canSpawnUnit(caster))
		{
			Debug.Log ("Max no. of controllable summons reached!");
			return;
		}
		
		float chanceToExecute = isFixedChance? baseChance: chanceByRank[(caster.unit.level / 10)];
		float rand = UnityEngine.Random.value;
		
		if (rand > chanceToExecute)
		{
			return;
		}
		
		//if(caster.unit.Controllers.WeaponController.State == Fury.Controllers.WeaponController.States.PostDelay)
		//{
			caster.unit.Order(this, null, Vector3.zero);
		//}
	}
	
	public void aiExecute(KYAgent caster)
	{
		if (!canSpawnUnit(caster))
		{
			Debug.Log ("Max no. of controllable summons reached!");
			return;
		}
		
		float chanceToExecute = isFixedChance? baseChance: chanceByRank[(caster.unit.level / 10)];
		float rand = UnityEngine.Random.value;
		
		if (rand > chanceToExecute)
		{
			return;
		}
		
		//if(caster.unit.Controllers.WeaponController.State == Fury.Controllers.WeaponController.States.PostDelay)
		//{
			caster.unit.Order(this, null, Vector3.zero);
		
		//}
	}
	
	private bool canSpawnUnit(KYAgent caster)
	{
		int rank = caster.unit.level / 10;
		int maxSpawns =  maxSubSpawnsByRank[rank];
		
		return TowerAgent2.instance1.minionController.getSubSpawnCount(caster.unit.Identifier) < maxSpawns;
	}
	
	private Vector3 getRandomSpawnSpot(float radius, Fury.Behaviors.Unit caster)
	{
		Vector3 randomDirection = UnityEngine.Random.insideUnitSphere * radius;
		//NavMeshAgent navAgent = (caster.Controllers.MovementController.Agent as Fury.UnityNavMeshAgent).Agent;
		
		randomDirection += caster.transform.position;
		NavMeshHit hit;
		//navAgent.SamplePathPosition(-1, radius, out hit);
		
		//Debug.Log ((NavMesh.GetNavMeshLayerFromName("Default")).ToString() + " " + (NavMesh.GetNavMeshLayerFromName("EnemyPath")).ToString());
		if (NavMesh.SamplePosition(randomDirection, out hit, radius, 1 << NavMesh.GetNavMeshLayerFromName("Default")))
		{
			return hit.position;
		}
		
		return caster.transform.position;
	}
	
	private Vector3 getValidSpawnSpot(float radius, Fury.Behaviors.Unit caster)
	{
		Vector3 [] pathPoints = (caster.Controllers.MovementController.Agent as Fury.UnityNavMeshAgent).Agent.path.corners;
		float cumulativeDist = 0f;
		
		//if no path is returned, return caster's position
		if (pathPoints.Length == 0)
		{
			return caster.transform.position;
		}
		
		float dist, excessLength;
		Vector3 prevPt = caster.transform.position, nextPt;
		
		for (int i = 0; i < pathPoints.Length; i++)
		{
			nextPt = pathPoints[i];
			Vector3 vec = (nextPt - prevPt);
			dist = vec.magnitude;
			
			//when the current vector's magnitude exceeds the max radius if it were to be added to the total
			if ((cumulativeDist + dist) >= radius)
			{
				excessLength = radius - cumulativeDist;
				
				return prevPt + (excessLength * vec.normalized);
			}
			
			//add to the total
			else
			{
				cumulativeDist += dist;
			}
			
			prevPt = nextPt;
		}
		
		//if the loops finished but the cumulative length is still shorter than the given radius,
		//return the last point in the path as the spawn point
		return prevPt;
	}
}
