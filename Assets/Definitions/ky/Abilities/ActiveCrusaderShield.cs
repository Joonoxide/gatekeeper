using UnityEngine;
using System.Collections;
using System;

public class ActiveCrusaderShield : Fury.Database.Ability, AISkillInterface
{
	public override object OnBeginCast(Fury.Behaviors.Unit caster, Fury.Behaviors.Targetable target, UnityEngine.Vector3 position)
	{
		KYAgent agent = caster.gameObject.GetComponent<KYAgent>();
		agent.sprite.DoAnim("defen");
		SkeletonAnimation sprite = (agent.sprite as KYAgent.SpineSpriteWrapper).sprite;
		KYAgent.AbilityWrapper abilityWrapper = agent.abilityForAbilityWrapper(this);
		abilityWrapper.cooldown = abilityWrapper.fixedCooldown;
		abilityWrapper.cooldownLeft = abilityWrapper.cooldown;
		(caster as Fury.Behaviors.Unit).AddStatus(GameController.instance.shieldBuff, (caster as Fury.Behaviors.Unit));
		return null;
	}

	public override void OnEndCast(object tag, Fury.Behaviors.Unit caster, Fury.Behaviors.Targetable target, UnityEngine.Vector3 position)
	{

	}

	public void aiExecute(KYAgent caster, KYAgent target)
	{
		if(caster.unit.Controllers.WeaponController.State == Fury.Controllers.WeaponController.States.PostDelay)
		{
			caster.unit.Order(this, target.unit, Vector3.zero);
		}
	}

	public void aiExecute(KYAgent caster)
	{
		if(caster.unit.Controllers.WeaponController.State == Fury.Controllers.WeaponController.States.PostDelay)
		{
			caster.unit.Order(this, null, Vector3.zero);
		}
	}
}
