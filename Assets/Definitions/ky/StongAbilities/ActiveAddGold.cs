using UnityEngine;
using System.Collections;
using System;

public class ActiveAddGold : Fury.Database.Ability, AISkillInterface
{
	public override object OnBeginCast(Fury.Behaviors.Unit caster, Fury.Behaviors.Targetable target, UnityEngine.Vector3 position)
	{
		GameController.instance.arrayPlayer[caster.Owner.Identifier].amountGold += 50;
		KYAgent.AbilityWrapper abilityWrapper = caster.agent.abilityForAbilityWrapper(this);
		abilityWrapper.cooldown = abilityWrapper.fixedCooldown;
		abilityWrapper.cooldownLeft = abilityWrapper.cooldown;
		return null;
	}

	public override void OnEndCast(object tag, Fury.Behaviors.Unit caster, Fury.Behaviors.Targetable target, UnityEngine.Vector3 position)
	{

	}

	public void aiExecute(KYAgent caster, KYAgent target)
	{
	}

	public void aiExecute(KYAgent caster)
	{
	}
}
