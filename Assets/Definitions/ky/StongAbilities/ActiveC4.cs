using UnityEngine;
using System.Collections;
using System;

public class ActiveC4 : Fury.Database.Ability, AISkillInterface
{
	public GameObject ArrowPrefab = null;
	public Single Speed = 20f;
	public Single BlastRadius = 10f;
	
	public UnityEngine.GameObject ShootEffect = null;

	public override object OnBeginCast(Fury.Behaviors.Unit caster, Fury.Behaviors.Targetable target, UnityEngine.Vector3 position)
	{
		KYAgent.AbilityWrapper abilityWrapper = caster.agent.abilityForAbilityWrapper(this);
		abilityWrapper.cooldown = abilityWrapper.fixedCooldown;
		abilityWrapper.cooldownLeft = abilityWrapper.cooldown;
		var arrow = GameController.instance.instantiateGO(GameController.instance.bombPrefab, new Vector3(0, 0, 100));
		var projectile = arrow.AddComponent<Bomb>();

		// Create the effect
		var effect = (GameObject)GameObject.Instantiate(ShootEffect, new Vector3(0, 0, 100), Quaternion.identity);
		
		// Position the effect
		var pos = caster.transform.FindChild("turret").position;
		effect.transform.position = pos;
		effect.transform.parent = caster.transform;

		projectile.FlightDuration = Vector3.Distance(caster.transform.position, position) / Speed;
		projectile.StartPoint = pos;
		projectile.WeaponSource = this;
		projectile.AttackingUnit = caster;
		projectile.targetedLoc = position;
		projectile.ySpeed = (5-projectile.FlightDuration)/5*150f;
		return null;
	}

	public override void OnEndCast(object tag, Fury.Behaviors.Unit caster, Fury.Behaviors.Targetable target, UnityEngine.Vector3 position)
	{

	}

	public void aiExecute(KYAgent caster, KYAgent target)
	{
		TowerAgent2 agent = caster as TowerAgent2;
		TowerAgent2 agent2Opponent = TowerAgent2.getInstanceForIndex((caster.unit.Owner.Identifier == 0)? 1 : 0);
		Int32 identifier = -1;
		Fury.Behaviors.Unit unitTarget = null;
		if(agent2Opponent.minionController.listHarvesterIdentifier.Count > 0)
		{
			identifier = agent2Opponent.minionController.listHarvesterIdentifier[0];
		}
		else if(agent2Opponent.minionController.Minions.Count > 0)
		{
			identifier = agent2Opponent.minionController.Minions[0];
		}
		unitTarget = Fury.Behaviors.Manager.Instance.Find<Fury.Behaviors.Unit>(identifier);
		if(unitTarget != null && unitTarget)
		{
			Vector3 pos2 = unitTarget.transform.position;
			Vector3 velocity2 = unitTarget.Controllers.MovementController.Velocity;
			Vector3 pos1 = agent.transform.position;
			pos2 = pos2-pos1;
			float kahyenB = (2*pos2.x*velocity2.x+2*pos2.z*velocity2.z);
			float kahyenA = (velocity2.x*velocity2.x+velocity2.z*velocity2.z-20f*20f);
			float kahyenC = (pos2.x*pos2.x+pos2.z*pos2.z);
			float kahyen = kahyenB*kahyenB-4*kahyenA*kahyenC;

			float ky = 0;
			if(kahyen > 0)
			{
				kahyen = Mathf.Sqrt(kahyen);
				ky = (-kahyenB+kahyen)/(2*kahyenA);
				if(ky < 0 || (-kahyenB-kahyen)/(2*kahyenA) < ky)
				{
					ky = (-kahyenB-kahyen)/(2*kahyenA);
				}
			}
			//OnEndCast(0, agent.unit, null, unitTarget.transform.position+ky*velocity2);
			agent.unit.Order(this, agent.unit, unitTarget.transform.position+ky*velocity2);
		}
	}

	public void aiExecute(KYAgent caster)
	{
		TowerAgent2 agent = caster as TowerAgent2;
		TowerAgent2 agent2Opponent = TowerAgent2.getInstanceForIndex((caster.unit.Owner.Identifier == 0)? 1 : 0);
		Int32 identifier = -1;
		Fury.Behaviors.Unit unitTarget = null;
		if(agent2Opponent.minionController.listHarvesterIdentifier.Count > 0)
		{
			identifier = agent2Opponent.minionController.listHarvesterIdentifier[0];
		}
		else if(agent2Opponent.minionController.Minions.Count > 0)
		{
			identifier = agent2Opponent.minionController.Minions[0];
		}
		unitTarget = Fury.Behaviors.Manager.Instance.Find<Fury.Behaviors.Unit>(identifier);
		if(unitTarget != null && unitTarget)
		{
			Vector3 pos2 = unitTarget.transform.position;
			Vector3 velocity2 = unitTarget.Controllers.MovementController.Velocity;
			Vector3 pos1 = agent.transform.position;
			pos2 = pos2-pos1;
			float kahyenB = (2*pos2.x*velocity2.x+2*pos2.z*velocity2.z);
			float kahyenA = (velocity2.x*velocity2.x+velocity2.z*velocity2.z-20f*20f);
			float kahyenC = (pos2.x*pos2.x+pos2.z*pos2.z);
			float kahyen = kahyenB*kahyenB-4*kahyenA*kahyenC;

			float ky = 0;
			if(kahyen > 0)
			{
				kahyen = Mathf.Sqrt(kahyen);
				ky = (-kahyenB+kahyen)/(2*kahyenA);
				if(ky < 0 || (-kahyenB-kahyen)/(2*kahyenA) < ky)
				{
					ky = (-kahyenB-kahyen)/(2*kahyenA);
				}
			}
			//OnEndCast(0, agent.unit, null, unitTarget.transform.position+ky*velocity2);
			agent.unit.Order(this, agent.unit, unitTarget.transform.position+ky*velocity2);
		}
	}
}
