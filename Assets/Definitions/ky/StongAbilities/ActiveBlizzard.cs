using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class ActiveBlizzard : Fury.Database.Ability, AISkillInterface
{
	public override object OnBeginCast(Fury.Behaviors.Unit caster, Fury.Behaviors.Targetable target, UnityEngine.Vector3 position)
	{
		Fury.Behaviors.Commander cmdr = Fury.Behaviors.Manager.Instance.Commanders[(caster.Owner.Identifier == 0)? 1 : 0];
		//foreach (var unit in cmdr.Units)
		foreach(KeyValuePair<int, Fury.Behaviors.Unit> unitData in cmdr.Units)
		{
			Fury.Behaviors.Unit unit = unitData.Value;
			
			if (unit.gameObject.GetComponent<WalkableAgent>())
			{
				Vector3 pos = Vector3.zero;
				GameObject newFrozenGO = GameController.instance.instantiateGO (GameController.instance.frozenPrefab, Vector3.zero) as GameObject;
				newFrozenGO.transform.parent = unit.transform;
				pos.y = 4;
				Vector3 scale = new Vector3(-0.5f, 0.5f, 1f);
				newFrozenGO.transform.localScale = scale;
				newFrozenGO.transform.localPosition = pos;
				newFrozenGO.GetComponent<FrostEffect>().duration = 5f;
				unit.AddStatus(GameController.instance.stunBuff, caster);
			}
		}
		KYAgent.AbilityWrapper abilityWrapper = caster.agent.abilityForAbilityWrapper(this);
		abilityWrapper.cooldown = abilityWrapper.fixedCooldown;
		abilityWrapper.cooldownLeft = abilityWrapper.cooldown;
		return null;
	}

	public override void OnEndCast(object tag, Fury.Behaviors.Unit caster, Fury.Behaviors.Targetable target, UnityEngine.Vector3 position)
	{

	}

	public void aiExecute(KYAgent caster, KYAgent target)
	{
	}

	public void aiExecute(KYAgent caster)
	{
	}
}
