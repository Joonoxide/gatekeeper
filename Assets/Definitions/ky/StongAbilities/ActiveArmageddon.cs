using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class ActiveArmageddon : Fury.Database.Ability, AISkillInterface
{
	public override object OnBeginCast(Fury.Behaviors.Unit caster, Fury.Behaviors.Targetable target, UnityEngine.Vector3 position)
	{
		Fury.Behaviors.Commander cmdr = Fury.Behaviors.Manager.Instance.Commanders[(caster.Owner.Identifier == 0)? 1 : 0];
		//foreach (var unit in cmdr.Units)
		foreach(KeyValuePair<int, Fury.Behaviors.Unit> unitData in cmdr.Units)
		{
			Fury.Behaviors.Unit unit = unitData.Value;
			
			if (unit.gameObject.GetComponent<WalkableAgent>())
			{
				unit.ModifyHealth(-(999999 * 1), caster, this);
			}
		}
		cmdr = Fury.Behaviors.Manager.Instance.Commanders[(caster.Owner.Identifier == 1)? 1 : 0];
		//foreach (var unit in cmdr.Units)
		foreach(KeyValuePair<int, Fury.Behaviors.Unit> unitData in cmdr.Units)
		{
			Fury.Behaviors.Unit unit = unitData.Value;
			
			if (unit.gameObject.GetComponent<WalkableAgent>())
			{
				unit.ModifyHealth(-(999999 * 1), caster, this);
			}
		}

		foreach(var a in TowerAgent2.instance2.listAbilityWrapper)
		{
			if(a.abilityType == AbilityType.Summon && a.cooldownLeft <= 0)
			{
				a.cooldown = 20f;
				a.cooldownLeft = 20f;
			}
		}
		Camera.main.gameObject.AddComponent<ArmageddonEffect>();

		/*
		KYAgent.AbilityWrapper abilityWrapper = caster.agent.abilityForAbilityWrapper(this);
		abilityWrapper.cooldown = abilityWrapper.fixedCooldown;
		abilityWrapper.cooldownLeft = abilityWrapper.cooldown;
		*/
		return null;
	}

	public override void OnEndCast(object tag, Fury.Behaviors.Unit caster, Fury.Behaviors.Targetable target, UnityEngine.Vector3 position)
	{

	}

	public void aiExecute(KYAgent caster, KYAgent target)
	{
	}

	public void aiExecute(KYAgent caster)
	{
	}
}
