using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using UnityEngine;

public class KYScroll : MonoBehaviour 
{
	public List<GameObject> listChildGO = new List<GameObject>();
	public Rect rectRender;
	public int offsetX;
	public int childCount;
	public int page;
	public GameObject pageGO;
	public float startX;
	public int line = 1;
	public int startY;

	void Awake()
	{
		//rectRender = new Rect(transform.position.x-transform.lossyScale.x/2, transform.position.y-transform.lossyScale.y/2, transform.lossyScale.x, transform.lossyScale.y);
		rectRender = new Rect(startX, transform.position.y, startY, transform.lossyScale.y);
	}

	void Start ()
	{
		page = 0;
		if(childCount == 0)
		{
			childCount = 1;
		}
	}
	
	void Update () 
	{
		if(pageGO != null)
		{
			pageGO.GetComponent<SpriteText>().Text = "Page "+page;
		}
		int i = 0;
		foreach(GameObject childGO in listChildGO)
		{
			if(i/childCount == page)
			{
				childGO.SetActive(true);
			}
			else
			{
				childGO.SetActive(false);
			}
			++i;
		}
		//Debug.Log(listChildGO.Count+":"+getPageCount());
	}

	int getPageCount()
	{
		int pageCount = listChildGO.Count/childCount;
		if(pageCount != 0 && listChildGO.Count%childCount == 0)
		{
			--pageCount;
		}
		return pageCount;
	}

	public void onClickLeft()
	{
		--page;
		if(page < 0)
		{
			page = getPageCount();
		}
	}

	public void onClickRight()
	{
		++page;
		if(page > getPageCount())
		{
			page = 0;
		}
	}
	public void addChild(GameObject childGO)
	{
		listChildGO.Add(childGO);
		int moduloCount = (listChildGO.Count-1)%childCount;
		Vector3 pos = new Vector3(rectRender.x, startY, transform.position.z-10);
		int divY = moduloCount/(childCount/line);
		pos.y = pos.y-divY*30f;
		moduloCount = moduloCount%(childCount/line);
		pos.x += moduloCount*offsetX;
		childGO.transform.position = pos;
	}
}
