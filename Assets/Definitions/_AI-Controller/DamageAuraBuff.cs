using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class DamageAuraBuff : Fury.Database.Status, Fury.Database.Status.IHealthChangeCaused
{
	float Fury.Database.Status.IHealthChangeCaused.GetModifier (Fury.Behaviors.Unit changer, Fury.Behaviors.Unit target, Fury.Database.ChangeSource source, int baseAmount)
	{
		TowerAgent agent = TowerAgent.getInstanceForIndex(changer.Owner.Identifier);
		float level = agent.dictionaryAuraLevel[this];
		return 1+level;;
	}


}
