using UnityEngine;
using System.Collections;

public class InputWrapper
{
	public class TouchWrapper
	{
		public Touch touch;
		public float timeElapsed;
	}

	public TouchWrapper touchWrapper = null;
	public float chargeTime;
	public bool prevLMB = false;
	public bool isMouseRelease = false;
	public bool isTrigger = false;
	public Vector3 screenPos = Vector3.zero;
	public bool isRelease = false;
	public bool isPress = false;
	public Vector3 deltaMove;
	public Vector3 prevScreenPos;
	public Vector3 accumulateMove;

	public InputWrapper()
	{
		touchWrapper = new TouchWrapper();
		touchWrapper.timeElapsed = -1;
	}

	void updateTouch()
	{
		if(SystemInfo.deviceType == DeviceType.Handheld)
		{
			foreach(var touch in Input.touches)
			{
				if(touch.phase == TouchPhase.Began && touchWrapper.timeElapsed == -1)
				{
					touchWrapper.touch = touch;
					touchWrapper.timeElapsed = 0;
					isTrigger = true;
				}
				else if(touch.phase == TouchPhase.Ended && touch.fingerId == touchWrapper.touch.fingerId)
				{
					chargeTime = touchWrapper.timeElapsed;
					touchWrapper.timeElapsed = -1;
					isRelease = true;
				}
			}
			foreach(var touch in Input.touches)
			{
				if(touch.fingerId == touchWrapper.touch.fingerId && touchWrapper.timeElapsed != -1)
				{
					screenPos = touch.position;
				}
			}

			if(touchWrapper.timeElapsed != -1)
			{
				touchWrapper.timeElapsed += Time.deltaTime;
				chargeTime = touchWrapper.timeElapsed;
				isPress = true;
			}
		}
	}
	
	public void update()
	{
		isMouseRelease = false;
		isTrigger = false;
		isRelease = false;
		isPress = false;
		if(SystemInfo.deviceType != DeviceType.Handheld)
		{
			if(Input.GetMouseButtonDown(0))
			{
				chargeTime = 0f;
				prevLMB = true;
				isTrigger = true;
			}
			else if(Input.GetMouseButtonUp(0))
			{

				isMouseRelease = true;
				prevLMB = false;
				isRelease = true;
			}
			else if(prevLMB)
			{
				chargeTime += Time.deltaTime;
				isPress = true;
			}
			screenPos = Input.mousePosition;
		}
		if(isTrigger)
		{
			isPress = true;
		}
		updateTouch();
		deltaMove = screenPos-prevScreenPos;
		accumulateMove += deltaMove;
		prevScreenPos = screenPos;
		if(isRelease)
		{
			accumulateMove = Vector3.zero;
		}
		screenPos.x = Mathf.Clamp(screenPos.x, 0, Screen.width);
		screenPos.y = Mathf.Clamp(screenPos.y, 0, Screen.height);
	}
}
