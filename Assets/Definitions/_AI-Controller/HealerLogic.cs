using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class HealerLogic : MonoBehaviour
{
	private Fury.Behaviors.Unit healer;
	
	public Fury.Database.Status LevelBuff = null;
	public int HealerLevel;
	public string BuffName = "";
	
	public Single D_Health;
	public Single D_PreDelay;
	public Single D_PostDelay;
	public Single D_CoolDown;
	public Single D_Damage;
	
	private PackedSprite sprite;
	
	void Start()
	{
		healer = GetComponent<Fury.Behaviors.Unit>();
		
		//HealerLevel = PlayerPrefs.GetInt(BuffName);
		
		sprite  = gameObject.GetComponentInChildren(typeof(PackedSprite)) as PackedSprite;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (healer != null && healer.Controllers.VitalityController.Health > 0)
		{
			if (Time.timeScale == 0)
				sprite.StopAnim();
			
			#region Sprite related
			if (healer.State == Fury.UnitStates.Idle)
			{
				sprite.DoAnim(0);
			}
			else if (healer.State == Fury.UnitStates.MovingToPosition)
			{
				sprite.DoAnim(1);
				if (healer.Controllers.MovementController.TargetPosition.HasValue)
				{
					if (healer.transform.position.x > healer.Controllers.MovementController.TargetPosition.Value.x)
						sprite.winding = SpriteRoot.WINDING_ORDER.CW;
					else
						sprite.winding = SpriteRoot.WINDING_ORDER.CCW;
				}
			}
			else if (healer.State == Fury.UnitStates.MovingToTarget)
			{
				sprite.DoAnim(1);
				if (healer.Controllers.MovementController.TargetUnit)
				{
					if (healer.transform.position.x > healer.Controllers.MovementController.TargetUnit.transform.position.x)
						sprite.winding = SpriteRoot.WINDING_ORDER.CW;
					else
						sprite.winding = SpriteRoot.WINDING_ORDER.CCW;
				}
			}
			else if (healer.State == Fury.UnitStates.CastingAbility)
			{
				sprite.DoAnim(2);
				if(healer.Controllers.WeaponController.Target)
				{
					if (healer.transform.position.x > healer.Controllers.WeaponController.Target.transform.position.x)
						sprite.winding = SpriteRoot.WINDING_ORDER.CW;
					else
						sprite.winding = SpriteRoot.WINDING_ORDER.CCW;
				}
			}
			
		}
		else 
			sprite.DoAnim(3);
		
		#endregion
		
	}
}
