using UnityEngine;
using System.Collections;

public class kkk : MonoBehaviour {

	// Use this for initialization
	public KYAgent.SpineSpriteWrapper spriteWrapper;

	void Start () 
	{
		spriteWrapper = transform.parent.gameObject.GetComponent<KYAgent>().sprite as KYAgent.SpineSpriteWrapper;
		transform.localScale = new Vector3(0.4f*((spriteWrapper.winding == 0)? 1 : -1), 0.6f, 0.6f);
	}
	
	// Update is called once per frame
	void Update () 
	{
		transform.localScale = new Vector3(0.4f*((spriteWrapper.winding == 0)? 1 : -1), 0.6f, 0.6f);
	
	}
}
