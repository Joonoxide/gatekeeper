using UnityEngine;
using System.Collections;

public interface AISkillInterface
{
	void aiExecute(KYAgent caster, KYAgent target);
	void aiExecute(KYAgent caster);
}
