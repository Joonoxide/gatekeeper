using UnityEngine;
using System.Collections;

public class DemoHud : MonoBehaviour {
	public Fury.Database.Unit unitss;
	public Fury.Database.Unit unitsss;

	void Start () 
	{
		Fury.Behaviors.Manager.Instance.OnMapLoaded += OnMapLoaded;
		unitss = Resources.Load("Shaman") as Fury.Database.Unit;
		unitsss = Resources.Load("Archer") as Fury.Database.Unit;
	}
	
	void OnMapLoaded()
	{
		CreateSpawn(unitss, new Vector3(35, 0, 25), 0);
	}

	public void CreateSpawn(Fury.Database.Unit spawnType, Vector3 position, int commanderFlag)
	{		
		Fury.Behaviors.Commander commander;
		if(commanderFlag == 0)
		{
			commander = Fury.Behaviors.Manager.Instance.Commanders.First (c => c.Index == Fury.CommanderIndices.One);
		}
		else
		{
			commander = Fury.Behaviors.Manager.Instance.Commanders.First (c => c.Index == Fury.CommanderIndices.Two);
		}
			
		Fury.Behaviors.Manager.Instance.CreateUnit(spawnType, commander,
				position, null);
	}

}
