using UnityEngine;
using System.Collections;

public class Player
{
	public float amountGold;
	public float prevAmountGold;
	public float incomeRate;
	public bool isPlayer = false;
	public int towerLevel = 0;
	public int levelBuff = 0;
	public int resourceBountyLevel = 1;
	public int harvesterCount = 2;
	public int damageAuraLevel = 0;
	public int defendAuraLevel = 0;
	public int harvesterLevel = 0;
	public void loadData()
	{
		KYDataManager dataManager = KYDataManager.getInstance();
		int level = -1;
		towerLevel = dataManager.upgradeNameForLevel("Tower Mastery");
		damageAuraLevel = dataManager.upgradeNameForLevel("Damage Aura");
		defendAuraLevel = dataManager.upgradeNameForLevel("Defend Aura");
		harvesterLevel = dataManager.upgradeNameForLevel("Harvester Level");
		harvesterCount = dataManager.upgradeNameForLevel("Harvester Count")+2;
		//Debug.Log(harvesterCount+":"+Network.time);
	}


}
