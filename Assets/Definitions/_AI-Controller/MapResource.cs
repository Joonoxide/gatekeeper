﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class MapResource : MonoBehaviour 
{
	public static MapResource instance;
	public Vector3[] vs;
	public int[] iss;

	void Awake () 
	{
		if(instance != null)
		{
			DestroyImmediate(instance.gameObject);
		}
		/*vs = new Vector3[] { 
		new Vector3(2.78f, 2.98f, 0),//1
		new Vector3(6.1f, 2.6f, 0),//2
		new Vector3(6.65f, 0.174f, 0),//3
		new Vector3(4.8f, 0.121f, 0),//4

		new Vector3(2.71f, 0.31f, 0),//5
		new Vector3(0.23f, 0.23f, 0),//6
		new Vector3(-1.713f, 0.244f, 0),//7

		new Vector3(-2.764f, -0.364f, 0),//8

		new Vector3(-5.73f, -2.1f, 0),//9

		new Vector3(-6.5422f, -5.068542f, 0),//10
		new Vector3(-4.347241f, -5.979044f, 0),//11

		new Vector3(-3.496926f, -7.351483f, 0),//12
		new Vector3(-2.139405f, -6.426577f, 0),//13

		new Vector3(0.6088532f, -5.202298f, 0),//14
		new Vector3(0.8841313f, -3.623362f, 0),//15

		new Vector3(3.066214f, -1.799151f, 0),//16
		new Vector3(4.9f, -1.4f, 0),//17

		new Vector3(6.2f, -2.36f, 0),//18
		new Vector3(5.05f, -3.30f, 0),//19

		new Vector3(2.884429f, -5.068382f, 0),//20
		new Vector3(4.7f, -5.12f, 0),//21

		new Vector3(6.995314f, -5.872684f, 0),//22
		new Vector3(5.915411f, -7.20131f, 0),//23
		new Vector3(4.903116f, -7.20131f, 0),//24

		new Vector3(3.335491f, -9.532505f, 0),//25
		new Vector3(3.48787f, -10.01987f, 0),//26

		new Vector3(5.089697f, -13.45204f, 0),//27
		new Vector3(6.822255f, -13.93183f, 0),//28

		new Vector3(7.77543f, -16.15591f, 0),//29
		new Vector3(8.469366f, -17.15517f, 0),//30

		new Vector3(6.829993f, -19.58596f, 0),//31
		new Vector3(5.303687f, -20.6035f, 0),//32		

		new Vector3(4.087194f, -19.16213f, 0),//33
		new Vector3(1.601403f, -18.72453f, 0),//34

		new Vector3(1.601403f, -16.3865f, 0),//35
		new Vector3(0.788855f, -14.98917f, 0),//36

		new Vector3(-1.754608f, -12.94084f, 0),//37
		new Vector3(-3.705352f, -12.76083f, 0),//38

		new Vector3(-5.157036f, -14.63592f, 0),//39
		new Vector3(-6.561198f, -15.82406f, 0),//40

		new Vector3(-6.57036f, -19.33592f, 0),//39

		};*/
		
		iss = new int[] 
		{
		1, 1, 4, 6, 8, 9, 11, 13, 15, 17, 19, 21, 24, 26, 28, 30, 32, 34, 36, 38, 40, 41, 41, 41, 41 

		};
		
		instance = this;
		DontDestroyOnLoad(gameObject);
		
		Application.LoadLevelAdditive("WorldMap");
		
		OnLevelWasLoaded();

		GameObject go = Resources.Load("sprites") as GameObject;
		(GameObject.Instantiate(go) as GameObject).SetActive(false);
	}
	
	public void OnLevelWasLoaded()
	{	
		if(Application.loadedLevelName.Contains("level"))
		{
			Destroy(gameObject);
		}
		
	}
}
