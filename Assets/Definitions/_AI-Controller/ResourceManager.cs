using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class ResourceManager : MonoBehaviour 
{
	float interval;
	float intervalLeft;
	public List<Int32> listResourceIdentifier = new List<Int32>();
	public static ResourceManager instance = null;
	int harvesterCount;
	public Single Radius = 3;
	public float resourceBounty;
	public List<int> resourceMap = new List<int>();
	public int fallen = 0;
	public GameObject Resource;
	public List<Rect> arrayRect = new List<Rect>();
	void Start () 
	{
		Fury.Behaviors.Manager.Instance.OnUnitDead += OnUnitDead;
		instance = this;
		interval = 60f;
		intervalLeft = 0;
		resourceBounty = 5f;

		Resource = GameObject.Find("Resource");
		foreach(Transform child in Resource.transform)
		{
			resourceMap.Add(0);
			arrayRect.Add(new Rect(child.transform.position.x-child.transform.localScale.x/2, child.transform.position.z-child.transform.localScale.z/2, child.transform.localScale.x, child.transform.localScale.z));
		}
	}
	
	void OnDestroy()
	{
		Fury.Behaviors.Manager.Instance.OnUnitDead -= OnUnitDead;
	}
	
	void spawnResource(int rand)
	{
		int spawnCount = UnityEngine.Random.Range(1, 3);
		
		if (listResourceIdentifier.Count + spawnCount >= GameSettings.RESOURCES_MAX_SPAWN)
		{
			spawnCount = Mathf.Min(spawnCount, Mathf.Abs(GameSettings.RESOURCES_MAX_SPAWN - listResourceIdentifier.Count));
		}
		
		Rect rect = arrayRect[rand];
		float posZ = UnityEngine.Random.Range(rect.y+1, rect.y+rect.height-1);	
		float posX = UnityEngine.Random.Range(rect.x+1, rect.x+rect.width-1);

		Vector3 pos = new Vector3(posX, 0, posZ);
		Debug.Log (Fury.Behaviors.Manager.Instance.Commanders.Count);
		for(int i = 0; i < spawnCount; ++i)
		{
			Fury.Behaviors.Commander commander = Fury.Behaviors.Manager.Instance.Commanders[2];
			Vector3 position = pos + new Vector3(UnityEngine.Random.Range(1, 3) * getRandomSign(), 0, UnityEngine.Random.Range(2, 4) * getRandomSign());
			
			Fury.Behaviors.IUnit createdSpawn = null;
			createdSpawn = Fury.Behaviors.Manager.Instance.CreateUnit(GameController.instance.unitResourceA, commander, position, null);
			listResourceIdentifier.Add(createdSpawn.Identifier);
			++resourceMap[rand];
		}
	}
	
	int getRandomSign()
	{
		return UnityEngine.Random.value < 0.5f? -1:1;
	}

	private void OnUnitDead(Fury.Behaviors.Unit deadUnit, Fury.Behaviors.Unit lastAttacker)
	{
		if (listResourceIdentifier.Exists(i => i == deadUnit.Identifier))
		{
			for(int i = 0; i < arrayRect.Count; ++i)
			{
				if(arrayRect[i].Contains(new Vector2(deadUnit.transform.position.x, deadUnit.transform.position.z)))
				{
					--resourceMap[i];
				}
			}
			listResourceIdentifier.Remove(deadUnit.Identifier);
			(deadUnit as ResourceUnit).playDead();
			
			StartCoroutine(killResource(deadUnit.gameObject));
		}
	}

	private IEnumerator killResource(System.Object gameObject)
	{
		yield return new WaitForSeconds(4f);
		
		if (gameObject != null)
			GameObject.Destroy(gameObject as GameObject);
		
	}

	void Update () 
	{	
		if (GameController.instance.isGameOver)
		{
			return;
		}
		
		intervalLeft -= Time.deltaTime;
		if(intervalLeft <= 0)
		{
			int ccc = 0;
			for(int i = 0; i < resourceMap.Count; ++i)
			{
				Rect rect = arrayRect[i];
				if(resourceMap[i] < rect.width*rect.height/100)
				{
					spawnResource(i);
					++ccc;
				}
			}


			intervalLeft = interval;	
			Fury.Behaviors.Commander commander = Fury.Behaviors.Manager.Instance.Commanders[2];

			//foreach(ResourceUnit resource in commander.Units)
			foreach(KeyValuePair<int, Fury.Behaviors.Unit> unitData in commander.Units)
			{
				ResourceUnit resource = unitData.Value as ResourceUnit;
				
				int rand = UnityEngine.Random.Range(0, 10);
				if(rand < 3)
				{
					if(resource != null)
					{
						resource.upgradeResource();
					}					
				}
			}
		}

		if(listResourceIdentifier.Count == 0)
		{
			intervalLeft = 0;
		}
		if(listResourceIdentifier.Count < 5)
		{
			if(intervalLeft > listResourceIdentifier.Count)
			{
				intervalLeft = listResourceIdentifier.Count;
			}
		}
	}
}
