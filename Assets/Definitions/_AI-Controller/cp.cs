﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

public class cp : MonoBehaviour
{
	public Vector3 startPoint;
	float kahyen = -90f;
	float speed;
	public Vector3 dest;
	public Vector3 finalDest;
	public float firstmag;

	public void Start()
	{
		startPoint = transform.position;
		dest = (finalDest+startPoint)/2;
		dest.z += -15;
		if(finalDest.x-transform.position.x < 0)
		{
			kahyen = -90f;
		}
		else
		{
			kahyen = 90f;
		}
		transform.eulerAngles = new Vector3(270, kahyen, 0);
		speed = 15f;
		firstmag = (transform.position-dest).magnitude;
	}

	int index = 0;
	public void Update()
	{
		Vector3 distance = (dest-transform.position);
		distance.y = 0;
		Vector3 euler = new Vector3(0, kahyen, 0);
		float angle = Mathf.Atan(distance.z/distance.x)*Mathf.Rad2Deg;
		if(distance.x > 0)
		{
			euler.x = angle;
		}
		else
		{
			euler.x = -angle;

		}
		euler.x = 225;
		//transform.eulerAngles = euler;
		if(index == 0)
		{
			float nowx = Mathf.Lerp(270, 360, 1f-(distance.magnitude/firstmag))-transform.eulerAngles.x;
			transform.Rotate(nowx, 0, 0);
		}
		else
		{
			float nowx = Mathf.Lerp(0, 90, 1f-(distance.magnitude/firstmag))-transform.eulerAngles.x;
			transform.Rotate(nowx, 0, 0);
		}
		if (distance.magnitude < speed*Time.deltaTime)
		{
			//Destroy(gameObject);
			if(index == 0)
			{
				dest = finalDest;
				firstmag = (transform.position-dest).magnitude;
				index = 1;
			}
			else if(index == 1)
			{
				transform.position = startPoint;
				dest = (finalDest+startPoint)/2;
				dest.z += -15;
				if(finalDest.x-transform.position.x < 0)
				{
					kahyen = -90f;
				}
				else
				{
					kahyen = 90f;
				}
				index = 0;
				transform.eulerAngles = new Vector3(270, kahyen, 0);
				firstmag = (transform.position-dest).magnitude;
			}
		}
		else
		{		
			transform.position += distance.normalized*speed*Time.deltaTime;
		}
		
	}
}