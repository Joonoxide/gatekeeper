using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;
using Hud = Fury.Hud;

public class SimpleHUD : MonoBehaviour
{
	/// <summary>
	/// This interface can be attached to abilities that affect an area.
	/// </summary>
	public interface IAbilityInfo
	{
		Single Radius { get; }
		Boolean SnapToGrid { get; }
		String Description { get; }
	}
	
	public static SimpleHUD Instance;
	
	public Fury.Behaviors.Unit CrystalTower;
	public Fury.Behaviors.Unit Hero;
	public Fury.Behaviors.Unit CurrentTower;

	public Bounds CameraConstraint = new Bounds(Vector3.zero, Vector3.one * Single.MaxValue);
	public Texture2D IconFrame;
	public Texture2D[] Numbers;
	public GameObject FollowObject;
	public Material MaterialSelect, MaterialHover, MaterialArrow, MaterialArrowSelected;
	public Single MaxDistanceFromGround = 12, MinDistanceFromGround = 7, DesiredDistanceFromGround = 10;
	public LayerMask UnitMask, IgnoreMask;

	public GameObject TargetPrefab, RingPrefab, ArrowPrefab;
	private GameObject HoverObject;
	private GameObject MouseTargetObject;

	public Texture2D Green, Green25, Black50, Black75, Blue, Purple;

	private Dictionary<Fury.Database.Ability, Single> BufferAbilities;
	private List<Fury.Behaviors.Unit> BufferFollowers;
	public Fury.Controllers.SelectionController Selection;

	private Fury.Behaviors.Commander Owner;

    public List<GUITexture> Textures;
	private List<GUIText> Texts;

	private Int32 TexturePosition, TextPosition;
	List<int> touchIndex = new List<int>();
	private GameController GCTRL;
    int blek;
	private void Awake()
	{
		Instance = this;
	}

	private void Initialize()
	{
		GCTRL = GameObject.FindGameObjectWithTag("manager").GetComponent("GameController") as GameController;
		
		foreach (var cmdr in Fury.Behaviors.Manager.Instance.Commanders)
			if (cmdr.IsLocal)
			{
				Owner = cmdr;
				Textures = new List<GUITexture>();
				Texts = new List<GUIText>();

				BufferFollowers = new List<Fury.Behaviors.Unit>();
				BufferAbilities = new Dictionary<Fury.Database.Ability, Single>();

				Selection = new Fury.Controllers.SelectionController();
				Selection.OnUnitDeselected += new Action<Fury.Behaviors.Unit>(OnFollowerDeselected);
				Selection.OnUnitSelected += new Action<Fury.Behaviors.Unit>(OnFollowerSelected);
				//Hud.OnDragComplete += new Fury.General.Action<Rect>(OnDragComplete);

				Hud.UnitMask = UnitMask;

				Green = new Texture2D(1, 1);
				Green.SetPixel(0, 0, new Color(0, 0.5f, 0));
				Green.Apply();

				Green25 = new Texture2D(1, 1);
				Green25.SetPixel(0, 0, new Color(0f, 0.5f, 0f, 0.25f));
				Green25.Apply();

				Black50 = new Texture2D(1, 1);
				Black50.SetPixel(0, 0, new Color(0f, 0f, 0f, 0.5f));
				Black50.Apply();

				Black75 = new Texture2D(1, 1);
				Black75.SetPixel(0, 0, new Color(0f, 0f, 0f, 0.75f));
				Black75.Apply();

				Blue = new Texture2D(1, 1);
				Blue.SetPixel(0, 0, new Color(.25f, .25f, 1f));
				Blue.Apply();

				Purple = new Texture2D(1, 1);
				Purple.SetPixel(0, 0, new Color(1f, .25f, 1f));
				Purple.Apply();

				Fury.Behaviors.Manager.Instance.OnUnitDead += OnFollowerDead;

				MouseTargetObject = (GameObject)GameObject.Instantiate(TargetPrefab);
				MouseTargetObject.name = "!MouseTarget";

				break;
			}
			
			Selection.Add(CrystalTower);
			Selection.Add(Hero);
	}
	
	bool isTouch()
	{
		if(SystemInfo.deviceType == DeviceType.Handheld)
		{
			int i = 0;
			foreach(var touch in Input.touches)
			{
				if(touch.phase == TouchPhase.Began)
				{
					if(!touchIndex.Contains (i))
					{
						touchIndex.Add (i);
						return true;
					}
				}
				++i;
			}
			return false;
		}
		else
		{
			return Hud.TriggerLMB;
		}
	}

	private void Update()
	{
		if (Fury.Behaviors.Manager.Instance.GameState == Fury.GameStates.Playing && Owner == null)
			Initialize();

		if (Owner == null) return;

		// Change the zoom level of the camera
		DesiredDistanceFromGround = Mathf.Clamp(DesiredDistanceFromGround + Math.Sign(Input.GetAxis("Mouse ScrollWheel")) * -1f, 
			MinDistanceFromGround, MaxDistanceFromGround);

		// Move the camera, depending on mode
		if (FollowObject != null)
		{
			Camera.main.transform.position = Vector3.Lerp(Camera.main.transform.position,
			FollowObject.transform.position - Camera.main.transform.forward * DesiredDistanceFromGround, 0.1f);
		}
	}

	private void LateUpdate()
	{
		touchIndex.Clear ();
		if (Owner == null) return;

		Selection.Prune();

		var ray = Camera.main.ScreenPointToRay(Hud.Position);
		Hud.CalculateFocus(ray);

		if (HoverObject == null)
			HoverObject = (GameObject)GameObject.Instantiate(RingPrefab);

		// Create the hover ring over our mouse target
		HoverObject.renderer.enabled = false;
		if (Hud.FocusTarget != null)
			if (Hud.FocusTarget is Fury.Behaviors.Targetable)
				PlaceRing(HoverObject, (Fury.Behaviors.Targetable)Hud.FocusTarget);
			else
				PlaceRing(HoverObject, Hud.FocusTarget.Radius, Color.yellow, Hud.FocusTarget.transform.position);

		// Place rings on the ground for area of effect abilities
		if (Hud.QueuedAbility != null && Hud.QueuedAbility is IAbilityInfo)
		{
			var ability = Hud.QueuedAbility as IAbilityInfo;
			if (ability.Radius > 0)
			{
				var rounded = new Vector3(Mathf.Round(Hud.FocusPoint.x), Hud.FocusPoint.y, Mathf.Round(Hud.FocusPoint.z));
				var abilityTargetPoint = ability.SnapToGrid ? rounded : Hud.FocusPoint;
				var closestCaster = Hud.QueueGetCaster(abilityTargetPoint);

				if (closestCaster != null)
				{
					var canBuild = Hud.QueuedAbility.OnCheckUseOnTarget(closestCaster, null, abilityTargetPoint);
					PlaceRing(HoverObject, ability.Radius, canBuild ? Color.yellow : Color.red, rounded);
				}
			}
		}


		BufferAbilities.Clear();
	
		var invertedMousePos = new Vector2(Hud.Position.x, Screen.height - Hud.Position.y);
		var iconSize = 96;

		// Draw all the abilities that our units can use
		var a = 0;
        var b = 0;
		var c = 0;
		foreach(var kvp0 in BufferAbilities)
		{
			var ability = kvp0.Key;
			if(!ability.name.Contains("Hero") && !ability.name.Contains ("Tower"))
			{
				b++;
			}
		}
		foreach (var kvp0 in BufferAbilities)
		{
			var ability = kvp0.Key;
			UnityEngine.Rect abilityRect;
			// Check if the user wants to use the ability
			if (ability.Name.Contains("Hero"))
            {
				abilityRect = new Rect((b+c)*(iconSize+14)+50+Screen.width/20, Screen.height - 30 - iconSize, iconSize, iconSize);
				c++;
            }
			else if (ability.Name.Contains("Tower"))
			{
				Vector2 screenPos = Camera.mainCamera.WorldToScreenPoint(CurrentTower.transform.position);
                screenPos.y = Camera.main.pixelHeight-screenPos.y;
				abilityRect = new Rect(screenPos.x, screenPos.y, iconSize, iconSize);
                
				if (ability.Name.Contains("Rifle"))
				{
					abilityRect = new Rect(3 * (iconSize + 30) + (screenPos.x - 400), screenPos.y + 40, iconSize, iconSize);
				}
				else if (ability.Name.Contains("Upgrade"))
					abilityRect = new Rect(screenPos.x - 150, screenPos.y - 25, iconSize, iconSize);
				else if (ability.Name.Contains("Sell"))
					abilityRect = new Rect(screenPos.x + 50, screenPos.y - 25, iconSize, iconSize);
				else if(ability.Name.Contains ("Cannon"))
				{
					abilityRect = new Rect(2 * (iconSize + 30) + (screenPos.x - 400), screenPos.y - 40, iconSize, iconSize);
				}
				else if(ability.Name.Contains ("Arcane"))
				{			
					abilityRect = new Rect(4 * (iconSize + 30) + (screenPos.x - 400), screenPos.y - 40, iconSize, iconSize);
				}
            }
			else
            {
				abilityRect = new Rect(a * (iconSize + 14) + Screen.width /20, Screen.height - 30 - iconSize, iconSize, iconSize);
				++a;
            }
			
			var isMouseOver = abilityRect.Contains(invertedMousePos);
			var isQueued = (ability == Hud.QueuedAbility);

			// Draw the icon
			DrawTexture(ability.Icon, abilityRect, Color.gray);

			// Draw the fade if spell is not ready
			var overlayRect = abilityRect;
			overlayRect.y += overlayRect.height * (1 - kvp0.Value);
			overlayRect.height *= kvp0.Value;
			DrawTexture(Black75, overlayRect, new Color(0.5f, 0.5f, 0.5f, 0.5f));

			// Draw the frame
			abilityRect = new Rect(abilityRect.x - 2, abilityRect.y - 2, abilityRect.width + 4, abilityRect.height + 4);
			DrawTexture(IconFrame, abilityRect,
				isQueued ? new Color(0.5f, 0f, 0f) : new Color(0.15f, 0.15f, 0.15f));

			// Draw the hotkey
			if (a < 9)
				DrawTexture(Numbers[a + 1], new Rect(abilityRect.xMax - 16, abilityRect.yMax - 16, 16, 16), Color.gray);

			// Check if the user clicked the ability button or pressed the button			
			if ((isMouseOver && isTouch()))
			{ 
				if(kvp0.Value == 0 && Time.timeScale != 0)
				{
					BufferFollowers.Clear();
					foreach (var kvp in Selection)
						if (kvp.Owner == Owner)
							if (kvp.Properties.Abilities.Contains(ability))
								BufferFollowers.Add(kvp);

					Hud.QueueAbility(ability, BufferFollowers);
				}
				Hud.ConsumeLMB();
			}
		}

		// Commander left clicks his mouse
		if (Time.timeScale != 0 && isTouch ())
		{
			if (Hud.QueuedAbility != null)
			{
				// Cast the ability since there is one queued
				var caster = Hud.QueueGetCaster(Hud.FocusPoint);
				if (caster != null && Hud.QueuedAbility.OnCheckUseOnTarget(caster, Hud.FocusTarget, Hud.FocusPoint))
				{
					caster.Order(Hud.QueuedAbility, Hud.FocusTarget, Hud.FocusPoint);
					Hud.QueueAbility(null, null);
				}
			}
			else if (Hud.FocusTarget != null && Hud.FocusTarget is Fury.Behaviors.Unit)
			{
				if (Hud.FocusTarget.IsTeamOrNeutral(Owner))
				{
					if (Hud.FocusTarget.tag == "tower")
					{
						// Modify the selection since a unit was clicked was a tower
						if (!Hud.Shift)
						{
							if(Selection.Units.Count == 2)
							{
								Selection.Remove (Selection.Units[1]);
							}
							if(Selection.Units.Count == 1)
							{
								Selection.Remove (Selection.Units[0]);
							}
							Selection.Add(Hud.FocusTarget as Fury.Behaviors.Unit);
							CurrentTower = Hud.FocusTarget as Fury.Behaviors.Unit;
						}
						
					}
					else
					{
						if(Selection.Units.Count == 2)
						{
							Selection.Remove (Selection.Units[1]);
						}
						if(Selection.Units.Count == 1)
						{
							Selection.Remove (Selection.Units[0]);

						}
						Selection.Add (CrystalTower);
						Selection.Add(Hero);
					}
				}
				else
				{
					// Order units to a target since user clicked on a unit
					foreach (var follower in Selection)
						if (follower.Owner == Owner)
                        {
                            if(follower == Hero)
                            {
                                if(!Hero.Controllers.StatusController.IsStunned)
                                {
                                    attack(follower, Hud.FocusTarget as Fury.Behaviors.Unit);
                                }
                            }       
                            if(follower.tag == "tower")
                            {
                            }
                        }
				}
					
			} 
			else
			{
				if (Hud.FocusPoint.z < 35 && (Screen.height-invertedMousePos.y) > 100)
                {
					foreach (var follower in Selection)
                    {
						if (follower.Owner == Owner)
                        {
                            if(follower == Hero)
                            {
                                if(!Hero.Controllers.StatusController.IsStunned)
                                {
                                    consumeEngageTarget(follower);
									follower.gameObject.GetComponent<KYController>().movementTarget = Hud.FocusPoint;
                                    follower.Order(Hud.FocusPoint);
                                }
                            }                            
                        }
                        if(follower.tag == "tower" && follower != CrystalTower)
                        {
							Selection.Remove (follower);
							Selection.Add (CrystalTower);
                            break;
                        }
                    }
                }
			}
            Hud.ConsumeLMB();
		}
		// Animate the mouse target texture
		if (MouseTargetObject.active)
		{
			var texOffset = MouseTargetObject.renderer.material.mainTextureOffset + new Vector2(-Time.deltaTime * 2, 0);
			if (texOffset.x < 0) MouseTargetObject.active = false;

			MouseTargetObject.renderer.material.mainTextureOffset = texOffset;
		}

		// End the last call
        
		for (Int32 i = TexturePosition; i < Textures.Count; i++)
			Textures[i].enabled = false;

		for (Int32 i = TextPosition; i < Texts.Count; i++)
			Texts[i].enabled = false;


		// Begin the next call
		TexturePosition = 0;
		TextPosition = 0;

		transform.localPosition = Vector3.zero;
		transform.localScale = Vector3.one;
		transform.localRotation = Quaternion.identity;
		
		//Ability auto-cast
		if(Hud.QueuedAbility != null)
        {
            var caster = Hud.QueueGetCaster(Hud.FocusPoint);
			
            if (caster != null && !Hud.QueuedAbility.RequiresTarget)
            {
                caster.Order (Hud.QueuedAbility, caster, caster.transform.position);

                Hud.QueueAbility(null, null);
            }
        }
		
		if(Selection.Units.Count == 0)
		{
			Selection.Add(CrystalTower);
		}
	}
    
    public static void consumeEngageTarget(Fury.Behaviors.Unit source)
    {
        KYController sourceController = source.gameObject.GetComponent<KYController>();
        if(sourceController.engageUnit)
        {
            KYController targetController = sourceController.engageUnit.gameObject.GetComponent<KYController>();
			sourceController.engageUnit.Order (targetController.transform.position);
		    sourceController.engageUnit = null;
            if(targetController.engageUnit == source)
            {
				if(targetController.nextEngageUnit)
				{
					targetController.gameObject.GetComponent<Fury.Behaviors.Unit>().Order (targetController.nextEngageUnit);
                	targetController.engageUnit = targetController.nextEngageUnit;
                	targetController.nextEngageUnit = null;
				}
            	else
            	{
               		targetController.engageUnit = null;
            	}
			}
			else
			{
				targetController.nextEngageUnit = null;
			}
        }
    }
    
    public static void attack(Fury.Behaviors.Unit source, Fury.Behaviors.Unit target)
    {
        KYController sourceController = source.gameObject.GetComponent<KYController>();
        KYController targetController = target.gameObject.GetComponent<KYController>();
        if(targetController.engageUnit && targetController.nextEngageUnit && targetController.engageUnit != source && targetController.nextEngageUnit != source)
        {
            return;
        }
        else if(targetController.engageUnit && targetController.engageUnit != source)
        {
            consumeEngageTarget(source);
            sourceController.engageUnit = target;
            sourceController.sss = 0;
            targetController.nextEngageUnit = source;
        }
        else
        {
            consumeEngageTarget(source);
            sourceController.engageUnit = target;
            sourceController.sss = 0;
            targetController.engageUnit = source;
        }
    }

	private GUITexture GetTexture()
	{
		if (TexturePosition >= Textures.Count)
		{
			var go = new GameObject();
			go.transform.parent = transform;
			go.transform.localPosition = new Vector3(0.5f, 0.5f, 0);
			go.transform.localScale = new Vector3(0, 0, 1);
			go.transform.localRotation = Quaternion.identity;

			Textures.Add(go.AddComponent<GUITexture>());
		}

		var g = Textures[TexturePosition];
		g.enabled = true;
		g.transform.localPosition = new Vector3(0.5f, 0.5f, TexturePosition);

		TexturePosition++;
		return g;
	}

	private GUIText GetText()
	{
		if (TextPosition >= Texts.Count)
		{
			var go = new GameObject();
			go.transform.parent = transform;
			go.transform.localPosition = new Vector3(0.5f, 0.5f, 0);
			go.transform.localScale = new Vector3(0, 0, 1);
			go.transform.localRotation = Quaternion.identity;

			Texts.Add(go.AddComponent<GUIText>());
		}

		var g = Texts[TextPosition];
		g.enabled = true;
		g.transform.localPosition = new Vector3(0.5f, 0.5f, TextPosition + TexturePosition);

		TextPosition++;
		return g;
	}

	public void DrawTexture(Texture2D tex, Rect r, Color col)
	{
		if (tex == null) return;

		var guiTexture = GetTexture();
		guiTexture.pixelInset = new Rect(r.x - Screen.width * 0.5f, Screen.height * 0.5f - r.height - r.y, r.width, r.height);
		guiTexture.texture = tex;
		guiTexture.color = col;
	}
	
	public void DrawTex(Texture2D tex, Rect r, Color col)
	{
		if (tex == null) return;

		var guiTexture = GetTexture();
		guiTexture.pixelInset = new Rect(r.x , Screen.height * 0.5f , r.width, r.height);
		guiTexture.texture = tex;
		guiTexture.color = col;
	}

	public void DrawText(String str, Font font, Single offsetX, Single offsetY, TextAnchor anchor)
	{
		if (str == null) return;

		var guiText = GetText();
		guiText.text = str;
		guiText.anchor = anchor;
		guiText.alignment = TextAlignment.Center;
		guiText.font = font;
		guiText.pixelOffset = new Vector2(offsetX - Screen.width * 0.5f, Screen.height * 0.5f - offsetY);
	}

	public void DrawBar(Texture2D border, Texture2D fill, Single x, Single y, Single w, Single h, Single p)
	{
		Rect bgRect = new Rect(x, y, w, h);
		Rect barRect = new Rect(x + 1, y + 1, (w - 2) * p, h - 2);

		var col = new Color(0.5f, 0.5f, 0.5f, 0.5f);

		if (border != null) DrawTexture(border, bgRect, col);

		DrawTexture(fill, barRect, col);
	}

	private void PlaceRing(GameObject ring, Single radius, Color col, Vector3 position)
	{
		ring.renderer.enabled = true;
		ring.transform.parent = null;
		ring.renderer.material.color = col;
		ring.transform.localRotation = Quaternion.identity;
		ring.transform.localScale = Vector3.one * radius * .2f;
		ring.transform.localPosition = position + Vector3.up * 0.1f;
	}
	
	//place ring around unit
	private void PlaceRing(GameObject ring, Fury.Behaviors.Targetable follower)
	{
		ring.renderer.enabled = true;
		ring.transform.parent = follower.transform;
		ring.renderer.material.color = follower.IsTeamOrNeutral(Owner) ? new Color(0f, 0.5f, 0f) : new Color(1f, 0f, 0f);
		ring.transform.localPosition = Vector3.up * 0.01f;
		ring.transform.localRotation = Quaternion.AngleAxis(Time.timeSinceLevelLoad * 30, Vector3.up);
		ring.transform.localScale = new Vector3(1f / follower.transform.localScale.x,
			1f / follower.transform.localScale.y,
			1f / follower.transform.localScale.z) * follower.Radius * .2f;
	}
	
	//place arrow above unit
	private void PlaceArrow(GameObject ring, Fury.Behaviors.Targetable follower)
	{ 
		ring.renderer.enabled = true;
		ring.transform.parent = follower.transform;
		//ring.renderer.material.color = follower.IsTeamOrNeutral(Owner) ? new Color(0f, 0.5f, 0f) : new Color(1f, 0f, 0f);
		ring.transform.localPosition = Vector3.up * 0.01f + new Vector3(0,10,0);
		/*ring.transform.localRotation = Quaternion.AngleAxis(Time.timeSinceLevelLoad * 30, Vector3.up);
		ring.transform.localScale = new Vector3(1f / follower.transform.localScale.x,
			1f / follower.transform.localScale.y,
			1f / follower.transform.localScale.z) * follower.Radius * .2f;*/
	}

	/*private void OnDragComplete(Rect rect)
	{
		if (Hud.QueuedAbility != null) return;

		Hud.CalculateUnitsInRect(rect, BufferFollowers);

		if (!Hud.Shift) Selection.Clear();

		foreach (var f in BufferFollowers)
			if (f.Owner == Owner)
				Selection.Add(f);
	}*/

	private void OnFollowerDead(Fury.Behaviors.Unit target, Fury.Behaviors.Unit killer)
	{
		Selection.Remove(target);
	}

	private void OnFollowerSelected(Fury.Behaviors.Unit obj)
	{
		if (obj.name != "Crystal_Spire")
		{
			GameObject ring = (GameObject)GameObject.Instantiate(ArrowPrefab);
			ring.renderer.material = MaterialArrowSelected;
			ring.name = "Selection Ring";
			PlaceArrow(ring, obj);
		}
	}

	private void OnFollowerDeselected(Fury.Behaviors.Unit obj)
	{
		/*
		var ring = obj.transform.FindChild("Selection Ring");
		*/
	}
}