using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using LitJson;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class TitleController : MonoBehaviour 
{
	public class WeeWrapper
	{
		public string created_at;
		public string name;
		public string updated_at;
		public int damage;
		public float ms;
		public int hp;
		public int id;
		public int speed;
		public int cost;
		public int maximum;
		public int trigger;
	}

	public class LevelWrapper
	{
		public string created_at;
		public string updated_at;
		public int id;
		public int levelNum;
		public string unit;
		public int initialGold;
		public float rightX;
		public int harvesterCount;
	}

	List<WeeWrapper> arrayWeeWrapper = new List<WeeWrapper>();
	List<LevelWrapper> arrayLevelWrapper = new List<LevelWrapper>();
	WWW www;
	public InputWrapper inputWrapper;
	//public Transform transformLevelParent;
	public SpriteText text;
	public UIPanel panel;
	public bool isBlocking;
	public Camera overlay
	{
		get
		{
			return maincam.instance.overlaycam_below;
		}
	}
	public float w;
	public float h;
	public GameObject[] unlocks;
	public GameObject[] locks;
	public static bool isFlag;
	public MapAgent cursor;
	public static TitleController instance;
	public static int maxLevel = 22;

	void Awake()
	{
		KYDataManager.getInstance();
		instance = this;
	}

	void Start () 
	{
		panel = GameObject.Find("panel_options").GetComponent<UIPanel>();
		
		inputWrapper = new InputWrapper();
		StartCoroutine(lateStart());
		
	}

	public IEnumerator lateStart()
	{
		yield return null;
		
		Transform transformLevelParent = PersistentWorldMap.instance.levelContainer;
		sfx.getInstance().play("titlebgm");
		Transform child = null;
		flag flag = null;
		
		//create container for platforms
		/*GameObject platformContainer = new GameObject("PlatformContainer");
		platformContainer.transform.parent = transformLevelParent.transform;
		platformContainer.transform.localPosition = Vector3.zero;
		
		GameObject platformPrefab = Resources.Load ("LevelPlatform") as GameObject;
		
		for(int n = 1; n < maxLevel; n++)
		{
			child = transformLevelParent.Find("level"+n);
			
			//make platform
			GameObject platform = Instantiate(platformPrefab) as GameObject;
			platform.transform.position = child.transform.position + new Vector3(0, 0, 1);
			platform.transform.parent = platformContainer.transform;
		}*/
		
		int i = 1;
		for(i = 1; i < maxLevel; i++)
		{	
			child = transformLevelParent.Find("level"+i);
			
			KYData.LevelWrapper levelWrapper = KYDataManager.getInstance().levelNumForWrapper(i);
			if(child != null && levelWrapper != null)
			{
				flag = child.gameObject.GetComponent<flag>();
				flag.star = levelWrapper.star;
				if(flag.star == 0)
				{
					break;
				}
			}
			else if(child != null)
			{
				flag = child.gameObject.GetComponent<flag>();
				flag.star = 0;		
				if(flag.star == 0)
				{
					break;
				}		
			}
			else if(child == null)
			{
				--i;
				child = transformLevelParent.Find("level"+i);
				break;
			}
		}

		if(i == 1)
		{
			//GameObject dialog = GameObject.Instantiate((Resources.Load("dialog") as GameObject)) as GameObject;
			GameObject dialog = GameObject.Find("dialog") as GameObject;
			TextMesh textDialog= dialog.transform.Find("text").gameObject.GetComponent<TextMesh>();
			//textDialog.maxWidth = dialog.GetComponent<PackedSprite>().width-0.5f;
			textDialog.text = Language.Get(GenericLanguageField.MAP_START_HERE.ToString());
			dialog.transform.parent = child;
			dialog.transform.localPosition = new Vector3(0, 2.3f, -3);
			//KYAAgent a = dialog.GetComponent<KYAAgent>();
			//a.aname = "bigsmall";
			//a.isLoop = true;
			//a.Animate(1, true);
			
			//AnimatePosition.Do (dialog, EZAnimation.ANIM_MODE.By, new Vector3(0, 0.2f, 0), EZAnimation.GetInterpolator(EZAnimation.EASING_TYPE.Linear), -0.5f, 0, null, null).pingPong = true;
			
			AnimatePosition.Do (dialog, EZAnimation.ANIM_MODE.FromTo, dialog.transform.localPosition, dialog.transform.localPosition + new Vector3(0, 0.2f, 0), EZAnimation.GetInterpolator(EZAnimation.EASING_TYPE.Linear), -0.5f, 0, null, null).pingPong = true;
		}
		else
		{
			if(isFlag)
			{
				cursor.move(i);
				--i;
				child = transformLevelParent.Find("level"+(i));
				isFlag = false;
			}
			GameObject dialog = GameObject.Find("dialog") as GameObject;
			TextMesh textDialog= dialog.transform.Find("text").gameObject.GetComponent<TextMesh>();
			//textDialog.maxWidth = dialog.GetComponent<PackedSprite>().width-0.5f;
			textDialog.text = Language.Get(GenericLanguageField.MAP_YOURE_HERE.ToString());
			dialog.transform.parent = child;
			dialog.transform.localPosition = new Vector3(0, 2.3f, -3);
			///Vector3 ppp = dialog.transform.position;
			///ppp.z = -3;
			///dialog.transform.position = ppp;
			//KYAAgent a = dialog.GetComponent<KYAAgent>();
			//a.aname = "bigsmall";
			//a.isLoop = true;
			//a.Animate(1, true);	
			
			//AnimatePosition.Do (dialog, EZAnimation.ANIM_MODE.By, new Vector3(0, 0.2f, 0), EZAnimation.GetInterpolator(EZAnimation.EASING_TYPE.Linear), -0.5f, 0, null, null).pingPong = true;
			
			AnimatePosition.Do (dialog, EZAnimation.ANIM_MODE.FromTo, dialog.transform.localPosition, dialog.transform.localPosition + new Vector3(0, 0.2f, 0), EZAnimation.GetInterpolator(EZAnimation.EASING_TYPE.Linear), -0.5f, 0, null, null).pingPong = true;
		}


		//cursor.transform.position = MapResource.instance.vs[MapResource.instance.iss[i]-1] + new Vector3(0, 0, 5);
		
		cursor.transform.position = MapData.instance.getStandingPositionForLevel(Math.Min(maxLevel - 1, i));
		
		/*if(MapResource.instance.iss[i] > 1)
		{
			if(cursor.transform.position.x > MapResource.instance.vs[MapResource.instance.iss[i]-2].x)
			{
				cursor.transform.localScale = new Vector3(0.3f, 0.3f, 1);
			}
			else
			{
				cursor.transform.localScale= new Vector3(-0.3f, 0.3f, 1);
			}
		}*/
		
		h = 9.9f/2-Camera.main.orthographicSize;
		w = 25.6f/2-Camera.main.orthographicSize*Screen.width/Screen.height;
		Vector3 pos = cursor.transform.position;
		if(pos.y > h)
		{
			pos.y = h;
		}
		else if(pos.y < -h-9.9f)
		{
			pos.y = -h-9.9f;
		}

		if(pos.x > w)
		{
			//pos.x = w;
		}
		else if(pos.x < -w)
		{
			//pos.x = -w;
		}
		pos.x = Camera.mainCamera.transform.position.x;
		pos.z = -10;
		Camera.mainCamera.transform.position = pos;
		for(i = i+1; i < maxLevel; ++i)
		{
			child = transformLevelParent.Find("level"+i);
			if(child != null)
			{
				child.gameObject.SetActive(false);
			}
		}

		unlocks = new GameObject[3];
		for(i = 0; i <= 2; ++i)
		{
			unlocks[i] = panel.transform.Find("frame"+(i+1)).gameObject;
		}

		locks = new GameObject[3];
		for(i = 0; i <= 2; ++i)
		{
			locks[i] = panel.transform.Find("fake"+(i+1)).gameObject;
		}
		yield break;
	}

	IEnumerator showLast(int index)
	{
		//child.gameObject.SetActive(false);
		//cursor.transform.position = 
		yield return null;
	}

	IEnumerator co()
	{
		while(true)
		{
			yield return null;			
		}
	}

	IEnumerator setUnit()
	{
		www = new WWW("http://whispering-brook-7166.herokuapp.com/unit/go");
		Camera cam = Camera.mainCamera;
		GameController.hideAllCameras();
		yield return www;
		GameController.showCamera(cam);
		arrayWeeWrapper = JsonMapper.ToObject<List<WeeWrapper>>(www.text);

		List<Fury.Database.Unit> arrayUnit = new List<Fury.Database.Unit>();
		Fury.Database.Unit unit;
		unit = Resources.Load("ResourceA") as Fury.Database.Unit;
		arrayUnit.Add(unit);
		unit = Resources.Load("ResourceB") as Fury.Database.Unit;
		arrayUnit.Add(unit);
		unit = Resources.Load("Griffin") as Fury.Database.Unit;
		arrayUnit.Add(unit);
		unit = Resources.Load("Archer") as Fury.Database.Unit;
		arrayUnit.Add(unit);
		unit = Resources.Load("Crusader") as Fury.Database.Unit;
		arrayUnit.Add(unit);
		unit = Resources.Load("Harvester") as Fury.Database.Unit;
		arrayUnit.Add(unit);
		unit = Resources.Load("Swordman") as Fury.Database.Unit;
		arrayUnit.Add(unit);
		unit = Resources.Load("Priest") as Fury.Database.Unit;
		arrayUnit.Add(unit);
		unit = Resources.Load("Cyclops") as Fury.Database.Unit;
		arrayUnit.Add(unit);
		
		Fury.Database.Unit u;
		
		for(int i = 0; i < arrayUnit.Count; i++)
		{
			u = arrayUnit[i];
			
			foreach(WeeWrapper w in arrayWeeWrapper)
			{
				if(w.name == u.name)
				{
					u._MoveRate = w.ms;
					u._Health = w.hp;
					if(u._Weapon)
					{
						u._Weapon._Damage = w.damage;
					}
					u._cost = w.cost;
					u.maximum = w.maximum;
					u.trigger = w.trigger;
#if UNITY_EDITOR
					EditorUtility.SetDirty(u);
#endif
				}
			}
		}
	}

	public void onClickSet()
	{
		StartCoroutine(setUnit());
	}


	IEnumerator setLevel()
	{
		www = new WWW("http://whispering-brook-7166.herokuapp.com/level/go");
		Camera cam = Camera.mainCamera;
		GameController.hideAllCameras();
		yield return www;
		GameController.showCamera(cam);
		arrayLevelWrapper = JsonMapper.ToObject<List<LevelWrapper>>(www.text);

		List<LevelAsset> arrayLevelAsset = new List<LevelAsset>();
		LevelAsset levelAsset;
		int i = 1;
		while(true)
		{
			levelAsset = Resources.Load("LevelAsset"+i) as LevelAsset;
			if(levelAsset == null)
			{
				break;
			}
			arrayLevelAsset.Add(levelAsset);	
			++i;
		}
		
		foreach(LevelAsset l in arrayLevelAsset)
		{
			foreach(LevelWrapper w in arrayLevelWrapper)
			{
				if(l.levelNum == w.levelNum)
				{
					l.initialGold = w.initialGold;
					l.unit = w.unit;
					l.rightX = w.rightX;
					l.harvesterCount = w.harvesterCount;
#if UNITY_EDITOR
					EditorUtility.SetDirty(l);
#endif
				}
			}
		}
	}

	public void onClickSetLevel()
	{
		StartCoroutine(setLevel());
	}

	public void Update()
	{
		inputWrapper.update();

		if(inputWrapper.isTrigger)
		{
		}
		else if(inputWrapper.isPress && !isBlocking)
		{
			if(Mathf.Abs(inputWrapper.deltaMove.x) > 2 || Mathf.Abs(inputWrapper.deltaMove.y) > 2)
			{
				Vector3 pos = Camera.mainCamera.transform.position;
				//pos.x -= inputWrapper.deltaMove.x/75;
				pos.y -= inputWrapper.deltaMove.y/75;
			
				if(pos.y > h)
				{
					pos.y = h;
				}
				else if(pos.y < -h-11.53125f)
				{
					pos.y = -h-11.53125f;
				}

				if(pos.x > w)
				{
					//pos.x = w;
				}
				else if(pos.x < -w)
				{
					//pos.x = -w;
				}
				
				Camera.mainCamera.transform.position = pos;
			}
		}

		/*
		if(FacebookAndroid.isSessionValid() && !textFB.Text.Contains("logout"))
		{
			textFB.Text = "logout";
			buttonFB.methodToInvoke = "onClickLogout";
		}
		else if(!FacebookAndroid.isSessionValid() && !textFB.Text.Contains("connect"))
		{
			textFB.Text = "connect";
			buttonFB.methodToInvoke = "onClickLogin";
		}
		*/
	}

	public void loadPVE(int level)
	{
		//panel.transform.Find ("description").gameObject.GetComponent<SpriteText>().Text = "Level "+level;
		//panel.StartTransition("Bring in Forward");

		LevelManager.createInstance(level);
		string levelName = "level"+level;
		//string levelName = "debuglevel1";
		//GameObject.Find("levelcamera").SetActive(false);
		GameController.LoadLevel(levelName, TransitionType.CloseOpen);
		//StartCoroutine(asyncLoad(levelName));
		//GameController.LoadLevel(levelName);
	}

	public int lll;
	public void onClickLevel(int level)
	{
		if(isBlocking)
		{
			return;
		}
		
		//MainMenuLanguageManager languageManager = MapResource.instance.GetComponent<MainMenuLanguageManager>();

		GameController.star = 1;
		//panel.transform.Find ("description").gameObject.GetComponent<SpriteText>().Text = "STAGE "+level;
		lll = level;
		panel.transform.Find("but_go").gameObject.GetComponent<UIButton>().delay = level;
		panel.StartTransition("Bring in Forward");
		KYData.LevelWrapper levelWrapper = KYDataManager.getInstance().levelNumForWrapper(level);
		
		//languageManager.updateMissionLevel(level);
		
		int i;
		if(levelWrapper == null)
		{
			for(i = 1; i < 3; ++i)
			{
				unlocks[i].SetActive(false);
				locks[i].SetActive(true);
			}
			locks[0].SetActive(false);
		}
		else
		{
			for(i = 0; i <= levelWrapper.star; ++i)
			{
				if(i == 3)
				{
					break;
				}
				unlocks[i].SetActive(true);
				locks[i].SetActive(false);
			}
			for(i = i; i <= 2; ++i)
			{
				unlocks[i].SetActive(false);
				locks[i].SetActive(true);
			}
		}
		isBlocking = true;
		//SocialDummy.instance.fetchHighScore(level);
		overlay.enabled = true;
		//onClickStar(1);
		
		//go to the newest available
		if (levelWrapper == null)
		{
			onClickStar(1);
		}
		
		else
		{
			onClickStar(Mathf.Min(3, levelWrapper.star + 1));
		}
		
	}

/*
	IEnumerator asyncLoad(string levelName)
	{
		AsyncOperation async = Application.LoadLevelAsync(levelName);
		yield return null;
		Transform t = Camera.mainCamera.gameObject.transform;
		Vector3 pos = t.position;
		pos = new Vector3(1000, 1000, 1000);
		t.position = pos;
		foreach(Transform child in t)
		{
			child.gameObject.SetActive(false);
		}
		GameObject newLoad = GameObject.Instantiate(loading, new Vector3(1000, 1000, 1010), Quaternion.identity) as GameObject;
	}
*/
	public bool isDone(AsyncOperation async)
	{
		Debug.Log(async.isDone);
		return async.isDone;
	}

	/*
	public void loadEVE()
	{
		GameController.LoadLevel("debug2", 1);
	}
	*/

	public void loadUpgrade()
	{
		GameController.LoadLevel("upgrade", TransitionType.Close);
	}

	public void loadShop()
	{
		GameController.LoadLevel("shop", TransitionType.Close);
	}
	
	public void loadFusion()
	{
		GameController.LoadLevel("collection", TransitionType.Close);
	}

	public void loadSetting()
	{
		GameController.LoadLevel("setting", TransitionType.Close);
	}

	public void onClickDelete()
	{
		PlayerPrefs.DeleteAll();
		KYDataManager.getInstance().initialize();
		GameController.LoadLevel("title", TransitionType.NoChange);
	}

	public void loadEquipment()
	{
		GameController.LoadLevel("barracks", TransitionType.Close);
	}

	public void loadFragment()
	{
		FragmentManager.instance.comein();
		overlay.enabled = true;
		isBlocking = true;
	}

	public void loadGatcha()
	{
		GameController.LoadLevel("gotcha", TransitionType.Close);
	}

	public void onClickLogin()
	{
		//FacebookAndroid.loginWithReadPermissions( new string[] { "email", "user_birthday" } );
	}

	public void onClickLogout()
	{
		//FacebookAndroid.logout();
	}

	public void onClickPost()
	{
		var parameters = new Dictionary<string,string>
		{
			{ "link", "http://prime31.com" },
			{ "name", "link name goes here" },
			{ "picture", "http://prime31.com/assets/images/prime31logo.png" },
			{ "caption", "the caption for the image is here" }
		};
		//FacebookAndroid.showDialog( "stream.publish", parameters );
	}

	public void onClickCancel()
	{
		panel.StartTransition("Dismiss Forward");
		isBlocking = false;
		overlay.enabled = false;
	}

	public void onClickGo(int level)
	{
		MapLoader.instance.LoadMap(level);
		//loadPVE(level);
	}

	public void onClickRuby()
	{
		Debug.Log("wut");
	}

	public void onClickXP()
	{
		FragmentManager.instance.comein();
		overlay.enabled = true;
		isBlocking = true;		
	}

	public static string ban = "";
	public static bool loseAnyTower;
	public static bool isDestroyTower;
	public static int killHarvester;
	public static int deadHarvester;
	public static int useGold;

	public static string process(string[] bans)
	{
		string ban = "Win the game\n";
		for(int i = 0; i < bans.Length; ++i)
		{
			if(bans[i] != "")
			{
				ban += "Don't use "+bans[i]+"\n";
			}
		}

		if(loseAnyTower)
		{
			ban += "Don't lose any tower\n";
		}
		if(isDestroyTower)
		{
			ban += "Destroy all enemy towers\n";
		}
		if(killHarvester > 0)
		{
			ban += "Kill more than "+killHarvester+" enemy harvesters\n";
		}
		if(deadHarvester > 0)
		{
			ban += "Don't lose more than "+deadHarvester+" harvesters\n";
		}
		if(useGold > 0)
		{
			ban += "Use more than "+useGold+" gold in battle\n";			
		}
		return ban;
	}

	public void onClickStar(int star)
	{
		GameController.star = star;
		
		MainMenuLanguageManager languageManager = MapResource.instance.GetComponent<MainMenuLanguageManager>();
		
		languageManager.updateMissionObjectives(lll, star);
		
		
		/*string[] bans = LevelManager.getBan(lll, star);
		LevelAsset la = Resources.Load("LevelAsset"+lll) as LevelAsset;
		
		if(star == 1)
		{
			loseAnyTower = la.loseAnyTower1;
			isDestroyTower = la.isDestroyTower1;
			killHarvester =  Mathf.Max(0, la.killHarvester1);
			deadHarvester = Mathf.Max(0, la.deadHarvester1);
			useGold = la.useGold1;
		}
		else if(star == 2)
		{
			loseAnyTower = la.loseAnyTower2;
			isDestroyTower = la.isDestroyTower2;
			killHarvester =  Mathf.Max(0, la.killHarvester2);
			deadHarvester = Mathf.Max(0, la.deadHarvester2);
			useGold = la.useGold2;
		}
		else
		{
			loseAnyTower = la.loseAnyTower3;
			isDestroyTower = la.isDestroyTower3;
			killHarvester =  Mathf.Max(0, la.killHarvester3);
			deadHarvester = Mathf.Max(0, la.deadHarvester3);
			useGold = la.useGold3;
		}

		ban = process(bans);

		panel.transform.Find ("description2").gameObject.GetComponent<SpriteText>().Text = ban;*/

		if(panel.transform.Find("iccon"))
		{
			Destroy(panel.transform.Find("iccon").gameObject);
		}
		GameObject go = (GameObject.Instantiate(unlocks[star-1].transform.Find("icon").gameObject) as GameObject);
		go.name = "iccon";
		go.transform.parent = panel.transform;
		go.transform.localPosition = new Vector3(0, 0.9f, -1);
		go.transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
		KYData.LevelWrapper levelWrapper = KYDataManager.getInstance().levelNumForWrapper(lll);
		if(levelWrapper == null)
		{
			panel.transform.Find("star1").gameObject.SetActive(false);
			panel.transform.Find("star2").gameObject.SetActive(true);	
		}
		else if(levelWrapper.star >= star)
		{
			panel.transform.Find("star1").gameObject.SetActive(true);
			panel.transform.Find("star2").gameObject.SetActive(false);
		}
		else
		{
			panel.transform.Find("star1").gameObject.SetActive(false);
			panel.transform.Find("star2").gameObject.SetActive(true);	
		}
	}

	public void onClickCheck()
	{
		//var isSessionValid = FacebookAndroid.isSessionValid();
		//text.Text = isSessionValid+"";
	}

	public void buyRuby()
	{	
		FragmentManager.instance.comein();
		overlay.enabled = true;
		isBlocking = true;	
	}

	public void buyHarvester()
	{	
		FragmentManager.instance.comein(1);
		overlay.enabled = true;
		isBlocking = true;	
	}

	public void buyXP()
	{
		FragmentManager.instance.comein();
		overlay.enabled = true;
		isBlocking = true;			/*
		FragmentManager.instance.comein();
		overlay.enabled = true;
		isBlocking = true;
		*/
	}
	
	public void onClickStar()
	{
		KYDataManager.getInstance().dataWrapper.starCount = 100;
	}
}
