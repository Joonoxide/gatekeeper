using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class TowerAgent : KYTowerAgent 
{
	public Dictionary<Fury.Database.Status, Single> dictionaryAuraLevel = new Dictionary<Fury.Database.Status, Single>();
	public static TowerAgent instance1;
	public static TowerAgent instance2;
	// Use this for initialization
	public override void Start () 
	{
		base.Start ();
		if(unit.Owner.Identifier == 0)
		{
			instance1 = this;
		}
		else
		{
			instance2 = this;
		}
		HPBar.transform.position = GameController.instance.bottomArcher[unit.Owner.Identifier].transform.position+new Vector3(0, 15, 0);
	}

	public static TowerAgent getInstanceForIndex(int index)
	{
		if(index == 0)
		{
			return instance1;
		}
		else
		{
			return instance2;
		}
	}
		// Update is called once per frame
	public override void Update () 
	{
		base.Update ();
		if (GameController.instance.isEnd == 10)
		{
			unit.Order(transform.position);
			return;
		}
		if (unit != null && unit.Controllers.VitalityController.Health > 0)
		{
			if(!isBah)
			{
				unit.Controllers.VitalityController.Health = unit.Controllers.VitalityController.MaxHealth;
				isBah = true;
			}

			if(HPBar)
			{
				var vitCtrl = unit.Controllers.VitalityController;
				float currentHP = vitCtrl.Health;
				float maxHP = vitCtrl.MaxHealth;
				HPBar.percent = currentHP/maxHP;
			}

			if(unit.State == Fury.UnitStates.AttackingUnit)
			{
				SkeletonAnimation animation = GameController.instance.bottomArcher[unit.Owner.Identifier];
				if (isWeaponAttacking())
				{
					if(animation.state.Animation == null || animation.state.Animation.Name != "attack")
					{
						animation.state.SetAnimation("attack", false); 
					}
				}
				else if(isWeaponCD())
				{
					if(animation.state.Animation == null || animation.state.Animation.Name != "standby")
					{
						animation.state.SetAnimation("standby", false); 
					}
				}
				else //moving toward attack unit.
				{
					//sprite.DoAnim(1);
				}
			}			

			if(unit.State != Fury.UnitStates.AttackingUnit)
			{
				float minDist = 30, dist;
				Fury.Behaviors.Unit nearestUnit = null;
				List<KYAgent> arrayAgent = new List<KYAgent>();
				arrayAgent.AddRange(arrayNearbyEnemyAgent);
				arrayAgent.AddRange(TowerAgent2.getInstanceForIndex(unit.Owner.Identifier).arrayNearbyEnemyAgent);
				foreach(KYAgent agent in arrayAgent)
				{
					WalkableAgent walkableAgent = agent as WalkableAgent;
					dist = (transform.position-agent.transform.position).magnitude;

					if(walkableAgent != null) //not tower.
					{
						if(dist < minDist)
						{
							nearestUnit = agent.unit;
							minDist = dist;
						}
					}
				}

				if(nearestUnit != null && unit.Controllers.WeaponController.Target != nearestUnit)
				{
					unit.Order(nearestUnit);
				}
			}
			else if((unit.Controllers.WeaponController.Target.transform.position-transform.position).magnitude > 30)
			{
				unit.Order(transform.position);
			}
		}
	}
}
