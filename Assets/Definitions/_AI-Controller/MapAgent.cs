﻿using UnityEngine;
using System.Collections;
using System;

public class MapAgent : MonoBehaviour 
{
	SkeletonAnimation skeleton;
	iTween it;
	bool isMoving = false;
	
	float walkingSpeed = 0.8f;

	void Awake()
	{
		skeleton = GetComponent<SkeletonAnimation>();
	}

	void Start () 
	{
		//StartCoroutine(sss());
    }

    public void move(int level)
    {
		//Vector3 [] vs = new Vector3[] {MapResource.instance.vs[level - 1], MapResource.instance.vs[level - 1], MapResource.instance.vs[level - 1]};
    	//Vector3[] vs = new Vector3[MapResource.instance.iss[level]-MapResource.instance.iss[level-1]+1];
    	//Array.Copy(MapResource.instance.vs, MapResource.instance.iss[level-1]-1, vs, 0, MapResource.instance.iss[level]-MapResource.instance.iss[level-1]+1);
    	StartCoroutine(sss(MapData.instance.getPathToNextLevel(level - 1).ToArray(), level));
    }

    public IEnumerator sss(Vector3[] vs, int level)
    {
    	while(MapResource.instance == null)
    	{
    		yield return null;
    	}
    	yield return new WaitForSeconds(1);
    	if(vs.Length <= 1)
    	{
    		yield break;
    	}
		
		float totalDist = MapData.instance.getTotalDistance(vs);
		
   		iTween.MoveTo(gameObject,iTween.Hash("path", vs,"time", totalDist / walkingSpeed, "movetopath", false ,"easetype", "linear", "onComplete", new KYAnimator.handler(delegate(GameObject go)
   		{
			Transform child = PersistentWorldMap.instance.levelContainer.Find("level"+level);
			if(child != null)
			{
				child.gameObject.SetActive(true);
				GameObject dialog = GameObject.Find("dialog") as GameObject;
				TextMesh textDialog= dialog.transform.Find("text").gameObject.GetComponent<TextMesh>();
				//textDialog.maxWidth = dialog.GetComponent<PackedSprite>().width-0.5f;
				textDialog.text = Language.Get(GenericLanguageField.MAP_YOURE_HERE.ToString());
				dialog.transform.parent = child;
				dialog.transform.localPosition = new Vector3(0, 2.3f, -1);
				Vector3 ppp = dialog.transform.position;
				ppp.z = -1;
				dialog.transform.position = ppp;

				child.transform.localScale = Vector3.zero;
				AnimateScale.Do(child.gameObject, EZAnimation.ANIM_MODE.FromTo, new Vector3(0.0f, 0.0f, 1), new Vector3(1, 1, 1), EZAnimation.GetInterpolator(EZAnimation.EASING_TYPE.Linear), 0.3f, 0, null, null);
				//PunchScale.Do(child.gameObject, new Vector3(0, -0.5f, 0), 0.5f, 0.3f, null, null);
			}   		
			isMoving = false;
		}
   		)));
   		it = GetComponent<iTween>();    
   		isMoving = true;
    }
	
	void Update () 
	{
		if(isMoving)
		{	
			if(skeleton.state.Animation == null || skeleton.state.Animation.Name != "move")
			{
		    	skeleton.state.SetAnimation("move", true);		
		    }
			if(it.winding == 1)
			{
				transform.localScale = new Vector3(0.3f, 0.3f, 1);
			}
			else if(it.winding == 2)
			{
				transform.localScale = new Vector3(-0.3f, 0.3f, 1);
			}
			Vector3 pos = transform.position;
			float h = TitleController.instance.h;
			float w = TitleController.instance.w;
			if(pos.y > h)
			{
				pos.y = h;
			}
			else if(pos.y < -h-9.9f)
			{
				pos.y = -h-9.9f;
			}

			if(pos.x > w)
			{
				//pos.x = w;
			}
			else if(pos.x < -w)
			{
				//pos.x = -w;
			}
			
			pos.x = Camera.mainCamera.transform.position.x;
			pos.z = -10;
			Camera.mainCamera.transform.position = pos;
		}
		else
		{
			if(skeleton.state.Animation == null || skeleton.state.Animation.Name != "rest")
			{
				skeleton.state.SetAnimation("rest", true);
			}
		}
		/*
		if (transform.position.x > targetX)
			{sprite.setWinding((int)SpriteRoot.WINDING_ORDER.CW);}
		else
			{sprite.setWinding((int)SpriteRoot.WINDING_ORDER.CCW);}	
		*/	
	}
}
