﻿using UnityEngine;
using System.Collections;

public enum GUIAlignment
{
	TopLeft,
	TopRight,
	BottomLeft,
	BottomRight
}

public class KYPlacementHelper : MonoBehaviour 
{
	public GUIAlignment alignment = GUIAlignment.TopLeft;
	public float width, height;
	public float padding = 2f, biasX = 0, biasY = 0;
	
	public float depthFromCamera = 5f;
	
	public Camera cam;
	
	void Awake () 
	{
		if (cam == null)
		{
			cam = Camera.main;
		}
		
		//transform.position = new Vector3((float)Screen.width/(float)Screen.height*height+offset, transform.position.y, transform.position.z);		
		redraw();
	}
	
	void Start()
	{
		redraw();
	}

	public void update()
	{
		//transform.position = new Vector3((float)Screen.width/(float)Screen.height*height+offset, transform.position.y, transform.position.z);
		redraw();
	}
	
	void redraw()
	{
		if (alignment == GUIAlignment.TopLeft)
		{
			transform.position = cam.ScreenToWorldPoint(new Vector3(0, Screen.height, cam.nearClipPlane)) + new Vector3((width * 0.5f + padding) + biasX, - (height * 0.5f + padding) + biasY, depthFromCamera);
		}
		
		else if (alignment == GUIAlignment.TopRight)
		{
			transform.position = cam.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, cam.nearClipPlane)) + new Vector3(-(width * 0.5f + padding) + biasX, - (height * 0.5f + padding) + biasY, depthFromCamera);
		}
		
		else if (alignment == GUIAlignment.BottomLeft)
		{
			transform.position = cam.ScreenToWorldPoint(new Vector3(0, 0, cam.nearClipPlane)) + new Vector3((width * 0.5f + padding) + biasX, (height * 0.5f + padding) + biasY, depthFromCamera);
		}
		
		else if (alignment == GUIAlignment.BottomRight)
		{
			transform.position = cam.ScreenToWorldPoint(new Vector3(Screen.width, 0, cam.nearClipPlane)) + new Vector3(-(width * 0.5f + padding) + biasX, (height * 0.5f + padding) + biasY, depthFromCamera);
		}
	}
}
