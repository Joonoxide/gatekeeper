using UnityEngine;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

public class WWaveManager : WaveManager 
{
	public static WWaveManager instance;

	// Use this for initialization
	public virtual void Start () 
	{
		base.Start();
	}
	
	public void updateAIPlayerIndex(int playerIndex)
    {
    	//Debug.Log(arraySpawnWrapper.Count);
    	WaveWrapper waveWrapper = arrayWaveWrapper[playerIndex]; 
    	Player player = GameController.instance.arrayPlayer[playerIndex];
		TowerAgent agent = TowerAgent.getInstanceForIndex(playerIndex);
		TowerAgent2 agent2 = TowerAgent2.getInstanceForIndex(playerIndex);
		TowerAgent3 agent3 = TowerAgent3.getInstanceForIndex(playerIndex);
		TowerAgent2 agent2Opponent = TowerAgent2.getInstanceForIndex((playerIndex == 0)? 1 : 0);
		List<KYAgent.SpawnWrapper> array = new List<KYAgent.SpawnWrapper>();
		
		foreach(KYAgent.SpawnWrapper s in arraySpawnWrapper[playerIndex])
		{
			if(s.unit.trigger <= ResourceManager.instance.fallen)
			{
				array.Add(s);
			}
		}

		KYAgent.SpawnWrapper spawnWrapper;
		if(!agent2)
		{
			return;
		}
		if(playerIndex == 1)
		{
			player.amountGold += Time.deltaTime*player.incomeRate;
			//Debug.Log(player.incomeRate+":"+Time.deltaTime+":"+Time.deltaTime*player.incomeRate);
		}
		waveWrapper.intervalLeft -= Time.deltaTime;
		waveWrapper.intervalCheat -= Time.deltaTime;
		waveWrapper.intervalRandyLeft -= Time.deltaTime;
		waveWrapper.time += Time.deltaTime;

		if(waveWrapper.intervalLeft <= 0 && agent2.minionController.Minions.Count < 20)
		{
			waveWrapper.intervalLeft = UnityEngine.Random.Range(10, 20);
			int maxSpend = UnityEngine.Random.Range(3, 7);
			int spend = 0;

			float posX = (playerIndex == 0)? LevelManager.instance.leftX+2f : LevelManager.instance.rightX-2f;
			float posZ = UnityEngine.Random.Range(-25, 25);

			while(spend < maxSpend)
			{
				int index = UnityEngine.Random.Range(1, array.Count);
				spawnWrapper = array[index];
				if(agent2.minionController.getCount(spawnWrapper.unit.name)+spawnWrapper.count < spawnWrapper.unit.maximum)
				{
					//spawnWrapper = spawnNameForSpawnWrapper(array, "Gri");
					spawnWrapper.incrementCount();
				}
				//spawnAbilityWrapper.spawnWrapper.count = 1;
				++spend;
			}
			
			List<KYAgent.SpawnWrapper> unitsToSpawn = new List<KYAgent.SpawnWrapper>();
			Vector3 spawnLocation = new Vector3(posX + 2f, 0, posZ);

			foreach(var abilityWrapper in agent2.listAbilityWrapper)
			{
				if(abilityWrapper.abilityType == AbilityType.Summon)
				{
					spawnWrapper = (abilityWrapper as TowerAgent2.SpawnAbilityWrapper).spawnWrapper;
					
					if(spawnWrapper.count > 0)
					{
						//agent2.minionController.CreateSpawn (spawnWrapper.unit, spawnWrapper.count, new Vector3(posX, 0, posZ), spawnWrapper.level);
						//abilityWrapper.cooldown = spawnWrapper.count*abilityWrapper.fixedCooldown;
						//abilityWrapper.cooldownLeft = abilityWrapper.cooldown;
						//spawnWrapper.resetCount();
						
						for (int i = 0; i < spawnWrapper.count; i++)
						{
							unitsToSpawn.Add(spawnWrapper);
						}
						
						abilityWrapper.cooldown = spawnWrapper.count*abilityWrapper.fixedCooldown;
						abilityWrapper.cooldownLeft = abilityWrapper.cooldown;
						spawnWrapper.resetCount();
					}		
				}
				else if(abilityWrapper.cooldownLeft <= 0)
				{
					(abilityWrapper.ability as AISkillInterface).aiExecute(agent2, null);
				}
			}
			
			MinionController minionController = agent2.minionController;
					
			minionController.SpawnSquad(unitsToSpawn, spawnLocation);
		}
		alarm = false;
    }

	// Update is called once per frame
	void Update () 
	{
		updateAIPlayerIndex(1);
	
	}
}
