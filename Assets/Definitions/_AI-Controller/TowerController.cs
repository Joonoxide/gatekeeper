using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class TowerController : MonoBehaviour {
	
	public MeshRenderer towerBase;
	public MeshRenderer towerLv1;
	public MeshRenderer towerLv2;
	public MeshRenderer towerLv3;
	public MeshRenderer towerLv4;

	public bool renderTB = false;
	public bool renderT1 = false;
	public bool renderT2 = false;
	public bool renderT3 = false;
	public bool renderT4 = false;
	
	public int tLevel = 0;
	public bool isBase = true;
	
	private Single Timer;
	public bool attackEnabled = true; 
	
	private Fury.Behaviors.Unit self;
    Fury.Behaviors.Unit attackTarget = null;
	//private NavMeshAgent NaviAgent;
	
	// Use this for initialization
	void Start () 
	{
		ToggleRenderers(gameObject, false);
		self = GetComponent<Fury.Behaviors.Unit>();
		//StartCoroutine("ModifyNavMeshAgent");
	}
	// Update is called once per frame
	void Update () 
	{
		if (self != null)
		{	
			if (tLevel==0 && isBase)
			{
				renderTB = true;
				renderT1 = false;
				renderT2 = false;
				renderT3 = false;
				renderT4 = false;
			}
			else if (tLevel==1)
			{
				renderTB = false;
				renderT1 = true;
				renderT2 = false;
				renderT3 = false;
				renderT4 = false;
			}
			else if (tLevel==2)
			{
				renderTB = false;
				renderT1 = false;
				renderT2 = true;
				renderT3 = false;
				renderT4 = false;
			}
			else if (tLevel==3)
			{
				renderTB = false;
				renderT1 = false;
				renderT2 = false;
				renderT3 = true;
				renderT4 = false;
			}
			else if (tLevel==4)
			{
				renderTB = false;
				renderT1 = false;
				renderT2 = false;
				renderT3 = false;
				renderT4 = true;
			}
			
			if(renderTB)
			{
				towerBase.enabled = true;
			}
			else if(!renderTB)
			{
				towerBase.enabled = false;
			}
			
			if(renderT1)
			{
				towerLv1.enabled = true;
			}
			else if(!renderT1)
			{
				towerLv1.enabled = false;
			}
			
			if(renderT2)
			{
				towerLv2.enabled = true;
			}
			else if(!renderT2)
			{
				towerLv2.enabled = false;
			}
			
			if(renderT3)
			{
				towerLv3.enabled = true;
			}
			else if(!renderT3)
			{
				towerLv3.enabled = false;
			}
			
			if(renderT4)
			{
				towerLv4.enabled = true;
			}
			else if(!renderT4)
			{
				towerLv4.enabled = false;
			}
		}
		
	}
	
	public static void ToggleRenderers (GameObject goCleanUp, bool enabled)
	{
		foreach(Renderer rend in goCleanUp.GetComponentsInChildren<Renderer>())
		{
			rend.enabled = enabled;
		}
	} 
	
	private IEnumerator ModifyNavMeshAgent()
	{
		yield return new WaitForSeconds(1);

		//NaviAgent = GetComponent("NavMeshAgent") as NavMeshAgent;
		//NaviAgent.obstacleAvoidanceType = ObstacleAvoidanceType.NoObstacleAvoidance;
	}
}