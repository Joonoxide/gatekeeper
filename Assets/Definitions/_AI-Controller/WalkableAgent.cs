using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class WalkableAgent : KYAgent 
{
	public static int isMelee = 0;
	public static int isFlying = 1;
	public static int bounty = 2;
	public static int cost = 3;
	public Vector3 movementTarget;
	public bool isSpacing;
	public float intervalSkill = 1;
	public float fixedIntervalSkill = 1f;
	public List<Vector3> arrayWaypoint = new List<Vector3>();
	public int indexWaypoint;
	public Vector3 target;
	public Fury.UnitStates state;
	public Fury.Controllers.WeaponController.States stat;
	
	[SerializeField]
	private Fury.Behaviors.Targetable currentTarget;
	
	public bool hasSpawnAnimation = false;
	public float spawnAnimationDelay;
	
	private bool flag_spawnAnimPlayed = false;
	private bool flag_spawnAnimInit = false;
	private float spawnAnimationCount = 0.0f;
	
	//use this to change between status icons
	public PackedSprite statusIndicator {private set; get;}

	public override void Start () 
	{
		base.Start ();
		if(isBoss)
		{
			Vector3 scale = transform.localScale;
			scale.x = 1.5f;
			scale.y = 1.5f;
			scale.z = 1.5f;
			transform.localScale = scale;
		}

		indexWaypoint = 0;
		unit.AddStatus(GameController.instance.levelBuff, unit);
		
		//Debug.Log (unit.name + " " + (unit.Controllers.VitalityController as Fury.Controllers.VitalityController).MaxHealth);
		
		movementTarget = transform.position;
		GameObject newRingGO = Instantiate (GameController.instance.solidRingPrefab, Vector3.zero, Quaternion.identity) as GameObject;
		newRingGO.transform.parent = transform;
		Vector3 pos = Vector3.zero+new Vector3(0, 0, -0.3f);
		newRingGO.transform.localPosition = pos;
		newRingGO.transform.localScale = Vector3.one*1.5f*0.3f;
		newRingGO.transform.localScale += new Vector3(0.2f, 0, 0);
		
		PackedSprite ringSprite = newRingGO.GetComponentInChildren<PackedSprite>();
		ringSprite.DoAnim(0);
		ringSprite.PauseAnim();
		ringSprite.SetCurFrame(0);
		
		ringSprite.SetCurFrame((int)(unit.Owner.Identifier == 0? RingType.Ally: RingType.Enemy));
		
		//spawn status indicator
		GameObject indicatorPrefab = GameController.instance.statusIndicatorGO;
		
		if (indicatorPrefab)
		{
			Vector3 indicatorPos = unit.gameObject.transform.position + indicatorPrefab.transform.position + new Vector3(0, (unit.Properties as Fury.Database.Unit).Height, 0);
			GameObject indicator = (Instantiate(indicatorPrefab, indicatorPos, Quaternion.identity) as GameObject);
			
			indicator.transform.parent =  unit.gameObject.transform;
			
			statusIndicator = indicator.GetComponentInChildren<PackedSprite>();
			statusIndicator.gameObject.SetActive(false);
		}
		
		/*
		GameObject newDummy = Instantiate (GameController.instance.dummy, Vector3.zero, Quaternion.identity) as GameObject;
		newDummy.GetComponent<dummy>().follow = transform;
		*/
		
		TowerAgent agent;
		if(unit.Owner.Identifier == 0)
		{
			//newRingGO.renderer.material.color = Color.green;
			movementTarget.x = LevelManager.instance.rightX;
			agent = TowerAgent.getInstanceForIndex(0);
		}
		else
		{
			//newRingGO.renderer.material.color = Color.red;
			movementTarget.x = LevelManager.instance.leftX;
			agent = TowerAgent.getInstanceForIndex(1);
		}
		Dictionary<Fury.Database.Status, Single> dictionaryAuraLevel = agent.dictionaryAuraLevel;
		foreach(var kvp in dictionaryAuraLevel)
		{
			unit.AddStatus(kvp.Key, agent.unit);
		}
		
		string str = "";
		foreach(char c in gameObject.name)
		{
			if(c == '(')
			{
				break;
			}
			str += c;
		}
		gameObject.name = str;
		string abilityName = "Call_"+str;
		HPBar.gameObject.SetActive(false);
		isSpacing = false;
		level += (GameController.instance.arrayPlayer[unit.Owner.Identifier].levelBuff-1);

		intervalSkill = UnityEngine.Random.Range(0.5f, 1.5f);

		indexWaypoint = 0;
		arrayWaypoint.Add(transform.position);
		arrayWaypoint.Add(movementTarget);
		getWaypoint();

		if(unit.Owner.Identifier == 0)
		{
			sprite.setWinding((int)SpriteRoot.WINDING_ORDER.CW);			
		}
		else
		{
			sprite.setWinding((int)SpriteRoot.WINDING_ORDER.CCW);					
		}
	}
	
	public void getWaypoint()
	{
		if((transform.position-arrayWaypoint[indexWaypoint]).magnitude < 3)
		{
			++indexWaypoint;
		}
		movementTarget = arrayWaypoint[indexWaypoint];
	}

	public bool isCD;

	int moveHack = 0;

	public bool isMoving()
	{
		if((prevPosition-transform.position).magnitude < 0.0001f)
		{
			moveHack = 0;
			return false;
		}
		else
		{
			++moveHack;
			if(moveHack > 3)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}

	public override void Update () 
	{
		if (unit && GameController.instance.isGameOver)
		{
			//stand still
			unit.Order(unit.transform.position);
			sprite.DoAnim("standby", true);
			return;
		}
		
		if (unit && hasSpawnAnimation && !flag_spawnAnimPlayed)
		{	
			if (!flag_spawnAnimInit)
			{
				sprite.DoAnim("summon", false);
				flag_spawnAnimInit = true;
			}
			
			spawnAnimationCount += Time.deltaTime;
			
			if (spawnAnimationCount >= spawnAnimationDelay)
			{
				flag_spawnAnimPlayed = true;
				sprite.DoAnim("standby");
			}
			
			return;
		}
		
		base.Update();
		
		if (unit && GameController.instance.isEnd != 10)
		{
			state = unit.State;
			
			currentTarget = unit.Controllers.WeaponController.Target;
			
			if(!isBah)
			{
				unit.Controllers.VitalityController.Health = unit.Controllers.VitalityController.MaxHealth;
				isBah = true;
			}
			
            if(unit.Controllers.VitalityController.HealthPercentage < 1 && unit.Controllers.VitalityController.HealthPercentage > 0 && HPBar)
			{
				HPBar.gameObject.SetActive(true);
				var vitCtrl = unit.Controllers.VitalityController;
				float currentHP = vitCtrl.Health;
				float maxHP = vitCtrl.MaxHealth;
				HPBar.percent = currentHP/maxHP;
			}
			
			if(unit.State == Fury.UnitStates.Idle)
			{
				if(isMoving() && !unit.Controllers.StatusController.IsStunned)
				{
					sprite.DoAnim(1);
				}
				else
				{
					sprite.DoAnim(0);
				}
			}
			else if(unit.State == Fury.UnitStates.MovingToPosition || unit.State == Fury.UnitStates.MovingToTarget)
			{
				if(isMoving())
				{
					sprite.DoAnim (1);		
				}
				else
				{
					sprite.DoAnim(0);
				}
			}
			else if(unit.State == Fury.UnitStates.AttackingUnit || unit.State == Fury.UnitStates.FakeAttacking)
			{
				if(isMoving())
				{
					sprite.DoAnim(1);
				}
				else if (isWeaponAttacking())
				{
					if (unit.name.Contains ("Golem"))
					{
						sprite.DoAnim(0);
					}
					
					else
					{
						sprite.DoAnim(2);
					}
					
				}
				else
				{
					sprite.DoAnim (0);
				}
			}

			if(!isSpacing && !(unit.State == Fury.UnitStates.CastingAbility))
			{
				if((unit.Controllers.MovementController.Agent as Fury.UnityNavMeshAgent).Agent.hasPath)
				{
					float targetX = (unit.Controllers.MovementController.Agent as Fury.UnityNavMeshAgent).Agent.steeringTarget.x;
					if(Mathf.Abs(unit.transform.position.x-targetX) > 0.1f)
					{
						if (unit.transform.position.x > targetX)
							{sprite.setWinding((int)SpriteRoot.WINDING_ORDER.CW);}
						else
							{sprite.setWinding((int)SpriteRoot.WINDING_ORDER.CCW);}			
					}		
					else
					{
						if(unit.Owner.Identifier == 0)
						{
							sprite.setWinding((int)SpriteRoot.WINDING_ORDER.CW);			
						}
						else
						{
							sprite.setWinding((int)SpriteRoot.WINDING_ORDER.CCW);					
						}						
					}
				}
				else if(unit.Controllers.WeaponController.Target != null)
				{
					float targetX = unit.Controllers.WeaponController.Target.transform.position.x;
					if(Mathf.Abs(unit.transform.position.x-targetX) > 0.1f)
					{
						if (unit.transform.position.x > targetX)
							{sprite.setWinding((int)SpriteRoot.WINDING_ORDER.CW);}
						else
							{sprite.setWinding((int)SpriteRoot.WINDING_ORDER.CCW);}			
					}	
					else
					{
						if(unit.Owner.Identifier == 0)
						{
							sprite.setWinding((int)SpriteRoot.WINDING_ORDER.CW);			
						}
						else
						{
							sprite.setWinding((int)SpriteRoot.WINDING_ORDER.CCW);					
						}						
					}	
				}
			}
		 	
		 	target = (unit.Controllers.MovementController.Agent as Fury.UnityNavMeshAgent).Agent.steeringTarget;

			if(unit.State == Fury.UnitStates.CastingAbility || unit.State == Fury.UnitStates.FakeAttacking)
			{
				
			}
			else if(isSpacing)
			{
				if(unit.State == Fury.UnitStates.Idle)
				{
					isSpacing = false;
				}
			}
			else if(unit.State == Fury.UnitStates.Idle && arrayNearbyEnemyAgent.Count == 0)
			{
				if(((unit.transform.position-movementTarget).magnitude) > 15)
				{
					unit.Order (movementTarget);
				}
				else
				{
					TowerAgent2 agentTarget = TowerAgent2.getInstanceForIndex((unit.Owner.Identifier == 0)? 1 : 0);
					if(TowerAgent2.instance2 != null)
					{
						unit.Order(agentTarget.unit);
					}
				}
			}
			else if((unit.State != Fury.UnitStates.AttackingUnit && unit.State != Fury.UnitStates.MovingToTarget && unit.State != Fury.UnitStates.FakeAttacking && !isSpacing))
			{
				float minDist = 99999, dist;
				Fury.Behaviors.Unit nearestUnit = null;
				foreach(KYAgent agent in arrayNearbyEnemyAgent)
				{
					WalkableAgent walkableAgent = agent as WalkableAgent;
					dist = (transform.position-agent.transform.position).magnitude;

					if(walkableAgent != null) //not tower.
					{
						if(dist < minDist)
						{
							nearestUnit = agent.unit;
							minDist = dist;
						}
					}
					else
					{
						if(nearestUnit == null) //tower.
						{
							nearestUnit = agent.unit;
						}
					}
				}

				if(nearestUnit != null && unit.Controllers.WeaponController.Target != nearestUnit)
				{
					unit.Order(nearestUnit);
				}
				else
				{
					if(((unit.transform.position-movementTarget).magnitude) < 15)
					{
						TowerAgent2 agentTarget = TowerAgent2.getInstanceForIndex((unit.Owner.Identifier == 0)? 1 : 0);
						if(TowerAgent2.instance2 != null)
						{
							unit.Order(agentTarget.unit);
						}
					}					
				}
			}
			else if(!(isWeaponAttacking() || isWeaponCD()) && unit.Controllers.WeaponController.Target.gameObject.tag != "harvester" && unit.Controllers.WeaponController.Target.gameObject.tag != "tower")
			{
				float minDist = 99999, dist;
				Fury.Behaviors.Unit nearestUnit = null;
				foreach(KYAgent agent in arrayNearbyEnemyAgent)
				{
					WalkableAgent walkableAgent = agent as WalkableAgent;
					dist = (transform.position-agent.transform.position).magnitude;
					
					if(walkableAgent != null && walkableAgent.gameObject.tag != "harvester") //not tower.
					{
						if(dist < minDist)
						{
							nearestUnit = agent.unit;
							minDist = dist;
						}
					}
				}
				if(nearestUnit != null && unit.Controllers.WeaponController.Target != nearestUnit)
				{
					unit.Order(nearestUnit);
				}
			}
			else if(unit.Controllers.WeaponController.Target.gameObject.tag == "harvester")
			{
				float minDist = 99999, dist;
				Fury.Behaviors.Unit nearestUnit = null;
				foreach(KYAgent agent in arrayNearbyEnemyAgent)
				{
					WalkableAgent walkableAgent = agent as WalkableAgent;
					dist = (transform.position-agent.transform.position).magnitude;
					
					if(walkableAgent != null && walkableAgent.gameObject.tag != "harvester") //not tower.
					{
						if(dist < minDist)
						{
							nearestUnit = agent.unit;
							minDist = dist;
						}
					}
				}
				if(nearestUnit != null && unit.Controllers.WeaponController.Target)
				{
					unit.Order(nearestUnit);
				}
			}
			else if(unit.Controllers.WeaponController.Target.gameObject.tag == "tower")
			{
				float minDist = 99999, dist;
				Fury.Behaviors.Unit nearestUnit = null;
				foreach(KYAgent agent in arrayNearbyEnemyAgent)
				{
					WalkableAgent walkableAgent = agent as WalkableAgent;
					dist = (transform.position-agent.transform.position).magnitude;
					if(walkableAgent != null) //not tower.
					{
						if(dist < minDist)
						{
							nearestUnit = agent.unit;
							minDist = dist;
						}
					}
				}
				if(nearestUnit != null && unit.Controllers.WeaponController.Target)
				{
					unit.Order(nearestUnit);
				}
			}

			stat = unit.Controllers.WeaponController.State;
		}
		
		if (unit.State == Fury.UnitStates.AttackingUnit)
		{
			//if it's attacking tower, check if it's premature or 'stuck'
			if (unit.Controllers.WeaponController.Target.gameObject.tag == "tower")
			{
				float minDist = 99999, dist;
				Fury.Behaviors.Unit nearestUnit = null;
				foreach(KYAgent agent in arrayNearbyEnemyAgent)
				{
					WalkableAgent walkableAgent = agent as WalkableAgent;
					dist = (transform.position-agent.transform.position).magnitude;
					if(walkableAgent != null) //not tower.
					{
						if(dist < minDist)
						{
							nearestUnit = agent.unit;
							minDist = dist;
						}
					}
				}
				
				//has alternative nearby
				if(nearestUnit != null)
				{
					unit.Order(nearestUnit);
				}
			}
		}
		
		if(unit.State == Fury.UnitStates.MovingToPosition || unit.State == Fury.UnitStates.MovingToTarget)
		{
			if (!GameController.instance.isPaused)
			{
				if((prevPosition-transform.position).magnitude < 0.001f)
				{
					++stuckCount;
					if(stuckCount > 30)
					{
						Debug.Log ("stuck...");
						
						/*NavMeshAgent navAgent = (unit.Controllers.MovementController.Agent as Fury.UnityNavMeshAgent).Agent;
						
						Vector3 targetPos = transform.position;
					
						if(unit.Owner.Identifier == 0)
						{
							targetPos.x = LevelManager.instance.rightX;	
						}
						else
						{
							targetPos.x = LevelManager.instance.leftX;
						}*/
						
						isSpacing = false;
						unit.Order(transform.position);
					}
				}
				else
				{
					stuckCount = 0;
				}
			}
			
			/*if((prevPosition-transform.position).magnitude < 0.001f)
			{
				++stuckCount;
				if(stuckCount > 10)
				{
					//stuckCount = 0;
					
					if (unit.name.Contains("Skeleton"))
					{
						(unit.Controllers.MovementController.Agent as Fury.UnityNavMeshAgent).Agent.Warp(transform.position + new Vector3(0, 0, UnityEngine.Random.Range(0.5f, 2)));
					}
					
					else
					{
						unit.Order(transform.position);
					}
				}
			}
			else
			{
				stuckCount = 0;
			}*/
		}
		prevPosition = transform.position;
		
		intervalSkill -= Time.deltaTime;
		
		//handle ability activations (passive and active)
		foreach(AbilityWrapper abilityWrapper in listAbilityWrapper)
		{
			/*if (abilityWrapper.abilityType == AbilityType.Aura)
			{
				(abilityWrapper.ability as AISkillInterface).aiExecute(this);
			}
			
			else */if(intervalSkill <= 0 && abilityWrapper.cooldownLeft <= 0)
			{
				KYAgent target = null;
				if(unit.Controllers.WeaponController.Target)
				{
					target = unit.Controllers.WeaponController.Target.gameObject.GetComponent<KYAgent>();
				}
				
				if (abilityWrapper.abilityType == AbilityType.Active)
				{
					//int rand = UnityEngine.Random.Range(0, 3);
					//if(rand == 0)
					//{
						if(target)
						{
							(abilityWrapper.ability as AISkillInterface).aiExecute(this, target);
						}
						else
						{
							(abilityWrapper.ability as AISkillInterface).aiExecute(this);
						}
					//}
					//else if(rand != 0)
					//{
						abilityWrapper.cooldownLeft = abilityWrapper.cooldown;
					//}
					
					intervalSkill = UnityEngine.Random.Range(0.5f, 1f);
					continue;
				}
			}
			
		}
		
		//legacy code to handle active skills only
		/*if(intervalSkill <= 0)
		{
			KYAgent target = null;
			if(unit.Controllers.WeaponController.Target)
			{
				target = unit.Controllers.WeaponController.Target.gameObject.GetComponent<KYAgent>();
			}
			foreach(AbilityWrapper abilityWrapper in listAbilityWrapper)
			{
				
				if(abilityWrapper.cooldownLeft <= 0 && abilityWrapper.abilityType == AbilityType.Active)
				{
					
					int rand = UnityEngine.Random.Range(0, 3);
					
					if (unit && unit.name.Contains("Paladin"))
							Debug.Log ("paladin skill " + abilityWrapper.ability.name + " " + rand.ToString());
					
					if(rand == 0)
					{
						
						if(target)
						{
							if (unit && unit.name.Contains("Paladin"))
								Debug.Log("casting with target?");
							(abilityWrapper.ability as AISkillInterface).aiExecute(this, target);
						}
						else
						{
							if (unit && unit.name.Contains("Paladin"))
								Debug.Log("casting without target?");
							(abilityWrapper.ability as AISkillInterface).aiExecute(this);
						}
					}
					else if(rand != 0)
					{
						abilityWrapper.cooldownLeft = abilityWrapper.cooldown;
					}
					break;
				}
			}	
			intervalSkill = UnityEngine.Random.Range(0.5f, 3f);
		}
		*/

		if(GameController.instance.isEnd == 10 && unit)
		{
			if((unit.Controllers.MovementController.Agent as Fury.UnityNavMeshAgent).Agent.hasPath)
			{
				float targetX = (unit.Controllers.MovementController.Agent as Fury.UnityNavMeshAgent).Agent.steeringTarget.x;
				if(Mathf.Abs(unit.transform.position.x-targetX) > 0.1f)
				{
					if (unit.transform.position.x > targetX)
						{sprite.setWinding((int)SpriteRoot.WINDING_ORDER.CW);}
					else
						{sprite.setWinding((int)SpriteRoot.WINDING_ORDER.CCW);}			
				}		
			}
			else if(unit.Controllers.WeaponController.Target != null)
			{
				float targetX = unit.Controllers.WeaponController.Target.transform.position.x;
				if(Mathf.Abs(unit.transform.position.x-targetX) > 0.1f)
				{
					if (unit.transform.position.x > targetX)
						{sprite.setWinding((int)SpriteRoot.WINDING_ORDER.CW);}
					else
						{sprite.setWinding((int)SpriteRoot.WINDING_ORDER.CCW);}			
				}		
			}
			if(unit.Owner.Identifier == 1 && TowerAgent2.instance1 == null)
			{
				unit.Order(new Vector3(65, 0, 0));
			}
			else if(unit.Owner.Identifier == 0 && TowerAgent2.instance2 == null)
			{
				unit.Order(new Vector3(-75, 0, 0));				
			}
			else
			{
				unit.gameObject.SetActive(false);
			}
			sprite.DoAnim (1);		

		}

		if(!unit && GameController.instance.isEnd != 10)
		{
			HPBar.gameObject.SetActive(false);
			
			sprite.DoAnim("dead", false);
			
			SkeletonAnimation skeletonAnimation = (sprite as SpineSpriteWrapper).sprite;
			skeletonAnimation.timeScale = 1;
			
			if(skeletonAnimation.state.Time/skeletonAnimation.state.Animation.Duration >= 0.9)
			{
				skeletonAnimation.transform.parent = null;
				skeletonAnimation.gameObject.AddComponent<Fadeout>();
				Destroy(gameObject);
			}
		}
		
		if(!unit && GameController.instance.isEnd == 10)
		{
			Destroy(gameObject);
		}	
	}
}
