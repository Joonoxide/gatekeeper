using UnityEngine;
using System.Collections;

public class KYTowerAgent : KYAgent {

	// Use this for initialization
	public override void Start () 
	{	
		base.Start();
		level = GameController.instance.arrayPlayer[unit.Owner.Identifier].towerLevel;
		
		unit.AddStatus(GameController.instance.levelBuff, unit);
		HPBar.gameObject.SetActive(false);
	}
	
	// Update is called once per frame
	public override void Update () 
	{
		base.Update();
		if(GameController.instance.isEnd == 10)
		{
			return;
			this.enabled = false;
		}
	}
}
