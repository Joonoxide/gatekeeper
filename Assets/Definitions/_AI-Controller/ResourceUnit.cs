using System;
using System.Collections.Generic;

using UnityEngine;
using Fury.Controllers;

public class ResourceUnit : Fury.Behaviors.Unit {

	static String[] arrayGrowAnimationName = { "born", "small_to_mid", "mid_to_big", "big_to_big2", "big2_to_big3", "big3_to_big4" };
	static String[] arrayDamageAnimationName = { "", "small_dmg", "mid_dmg", "big_dmg", "big_dmg2", "big_dmg3", "big_dmg4" };
	static String[] arrayDeadAnimationName = { "", "small_dead", "mid_dead", "big_dead", "big_dead2", "big_dead3", "big_dead4" };
	public SkeletonAnimation sprite;
	public int intervalUpgrade;

	void Start () 
	{
		sprite = transform.Find("Palm").GetComponent<SkeletonAnimation>();
		sprite.gameObject.GetComponent<MeshRenderer>().enabled = true;
		upgradeResource();
		AddStatus(GameController.instance.resourceBuff, this);
	}
	
	public void upgradeResource()
	{
		if(level <= 4)
		{
			sprite.state.SetAnimation(arrayGrowAnimationName[level], false);
			++level;
		}
	}

	public void kenaHit()
	{
		string animName = arrayDamageAnimationName[level];
		if(sprite.state.Animation == null || sprite.state.Animation.Name != animName || sprite.state.Time/sprite.state.Animation.Duration >= 0.9)
		{
			sprite.state.SetAnimation(animName, false); 
		}
	}

	public void playDead()
	{
		sprite.state.SetAnimation(arrayDeadAnimationName[level], false);
	}
}
