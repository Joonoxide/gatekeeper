using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class Vision : MonoBehaviour {

	// Use this for initialization
	void Start () 
	{
		GetComponent<BoxCollider>().isTrigger = true;
		if(transform.parent.transform.parent.gameObject.GetComponent<WalkableAgent>())
		{
			float range = GameSettings.VISION_RANGE_MELEE;
			
			string name = transform.parent.transform.parent.gameObject.name;
			
			if(name.Contains("Archer") || name.Contains("Elementalist") 
				|| name.Contains("Necromancer") || name.Contains("Medusa")
				|| name.Contains("Priest"))
			{
				range = GameSettings.VISION_RANGE_RANGED;
			}
			
			transform.localScale = new Vector3(range, 1f, range);
		}
		else if(transform.parent.transform.parent.gameObject.tag == "tower")
		{
				transform.localScale = new Vector3(40, 1, 15);
		}
		//GetComponent<BoxCollider>().size = new Vector3(1, 1, 1);
	}
	
	// Update is called once per frame
	void LateUpdate () 
	{
		transform.rotation = Quaternion.identity;
	}
	
	void OnTriggerEnter(Collider other)
	{ 	
		if(other.gameObject.name == "Self")
		{
			KYAgent otherAgent = other.transform.parent.transform.parent.gameObject.GetComponent<KYAgent>();
			KYAgent selfAgent = gameObject.transform.parent.transform.parent.GetComponent<KYAgent>();
			
			//if(selfAgent.unit.Identifier < otherAgent.unit.Identifier)
			//{
				if(selfAgent.unit.Owner.Identifier == otherAgent.unit.Owner.Identifier)
				{
					selfAgent.addToNearbyAllyAgent(otherAgent);
				}
				else
				{
					selfAgent.addToNearbyEnemyAgent(otherAgent);
				}
			//}
		}
	}
	
	void OnTriggerExit(Collider other)
	{
		if(other.gameObject.name == "Self")
		{
			KYAgent otherAgent = other.transform.parent.transform.parent.gameObject.GetComponent<KYAgent>();
			KYAgent selfAgent = gameObject.transform.parent.transform.parent.GetComponent<KYAgent>();
				
			if(selfAgent.unit.Owner.Identifier < otherAgent.unit.Owner.Identifier)
			{
				if(selfAgent.unit.Owner.Identifier == otherAgent.unit.Owner.Identifier)
				{				
					selfAgent.removeFromNearbyAllyAgent(otherAgent);
				}
				else
				{
					selfAgent.removeFromNearbyEnemyAgent(otherAgent);
				}
			}
		}
	}
	
	void OnDestroy()
	{
		KYAgent agent = gameObject.transform.parent.transform.parent.GetComponent<KYAgent>();
		foreach(KYAgent otherAgent in agent.arrayNearbyAllyAgent)
		{
			otherAgent.arrayNearbyAllyAgent.Remove (agent);
		}
		foreach(KYAgent otherAgent in agent.arrayNearbyEnemyAgent)
		{
			otherAgent.arrayNearbyEnemyAgent.Remove (agent);
		}
	}
}
