using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using UnityEngine;
using KYData;
using Hud = Fury.Hud;

public class LKYHUD : MonoBehaviour
{	
	public enum ControlState { Summon, Charge, Default = -1 };
	ControlState controlState;
	public InputWrapper inputWrapper; 
	public static LKYHUD instance;
	public GameObject hitGO;
	public GameObject buttonAbilityGO;
	public GameObject buttonSpellGO;
	public List<KYButton> arrayButton = new List<KYButton>();
	public List<GameObject> arraybox = new List<GameObject>();
	public Fury.Database.Ability queuedAbility;
	public KYButton queuedButton;
	public Camera cammy;
	public Camera cammy2;
	public List<KYButton> arrayButton1 = new List<KYButton>();
	public List<KYButton> arrayButton2 = new List<KYButton>();
	public List<KYButton> arrayButton3 = new List<KYButton>();
	public float offsetX = 0;
	public float offsetY = 0;
	public int tab = 0;
	public int isScrolling;
	public PackedSprite bag;
	public int isOut;
	public delegate void Del();
	Del del;
	public bool break1;
	public int useGold;
	
	GameObject sideBar;
	
	private UnitInfo unitInfoSection;
	
	UIScrollList scrollList;
	List <UIListItemContainer> arrayScrollItems;

	public void Awake()
	{
		GameObject.Find("buttonCamera").GetComponent<Camera>().depth = 3;
		GameObject.Find("GUI camera").GetComponent<Camera>().depth = 2;
		GameObject.Find("overlaycamera").GetComponent<Camera>().depth = 1;
	}

	public void Start()
	{
		instance = this;
		inputWrapper = new InputWrapper();
		del = update1;
		StartCoroutine(lateStart());
	}

	KYButton focus = null;
	public KYButton[] arrayB = new KYButton[4];
	
	void OnTroopSelect(int index)
	{
		Debug.Log ("what");
	}
	
	public void update1()
	{
		if(Time.timeScale == 0)
		{
			return;
		}
		inputWrapper.update();

		if(break1)
		{
			return;
		}
		RaycastHit hit;
		Ray ray;
	 	hitGO = null;
	 	KYButton hitButton = null;
		
		if(inputWrapper.isTrigger || inputWrapper.isPress || inputWrapper.isRelease)
		{
   			ray = cammy.ScreenPointToRay(inputWrapper.screenPos);

   			if(Physics.Raycast(ray, out hit, Mathf.Infinity, 1 << 10))
			{

				hitGO = hit.collider.gameObject;
				hitButton = hitGO.GetComponent<KYButton>();
			}	
			Hud.CalculateFocus(ray);
		}

		if(inputWrapper.isRelease)
		{
			if(hitGO != null && hitButton != null)
			{
				if(hitButton.identifier >= 500)
				{
					KYButton refe = arrayB[hitButton.identifier-500];
					if(refe != null)
					{
						arrayButton[refe.identifier].gameObject.renderer.material.color = new Vector4(0.5f, 0.5f, 0.5f, 1f);
						Destroy(refe.gameObject);
						arrayB[hitButton.identifier-500] = null;
						for(int i = hitButton.identifier-500; i < arrayB.Length-1; ++i)
						{
							arrayB[i] = arrayB[i+1];
							
							if (i == arrayB.Length - 2)
							{
								arrayB[i + 1] = null;
							}
						}
						for(int i = 0; i < arrayB.Length; ++i)
						{
							if(arrayB[i] != null)
							{
								Vector3 pos = GameObject.Find("container"+(i+1)).transform.position;
								pos.z -= 5;
								arrayB[i].transform.position = pos;
							}
						}
						TrooperWrapper t = arrayGua[arrayButton[refe.identifier].identifier];
						string nnn = t.name;
						foreach(var butt in arrayButton)
						{
							TrooperWrapper t2 = arrayGua[butt.identifier];
							if(t2.name.Contains(t.name))
							{
								butt.gameObject.renderer.material.color = new Vector4(0.5f, 0.5f, 0.5f, 1f);	
							}					
						}						
					}
				}
				else if(Mathf.Abs(hitGO.renderer.material.color.r-0.5f) < 0.1f)
				{
					bool hasSpace = false;
					int currIndex = 0;
					
					for (int i = 0; i < 4; i++)
					{
						if (arrayB[i] == null)
						{
							currIndex = i;
							hasSpace = true;
							break;
						}
					}
					
					if (!hasSpace)
					{
						return;
					}
					
					GameObject newGO = Instantiate(hitGO) as GameObject;
					//newGO.transform.parent = GameObject.Find("pre-game(Clone)").transform.Find("bar").Find("bar_1");
					newGO.name = "cloner" + (currIndex + 1);
					newGO.transform.localScale = new Vector3(1.9f, 1.9f, 1);
					newGO.transform.eulerAngles = Vector3.zero;
					newGO.transform.position = hitGO.transform.position;
					Vector3 pos = newGO.transform.position;
					pos.z = -3;
					newGO.transform.position = pos;
					TrooperWrapper t = arrayGua[hitButton.identifier];
					
					unitInfoSection.updateInfo(t);
					
					/*foreach(SkeletonAnimation s in sprites)
					{
						//Debug.Log (s.name + " " + t.name);
						if(s.gameObject.name.Contains((t.name.Split('_'))[1].ToLower()))
						{
							s.renderer.enabled = true;
							int rank = (t.level-1)/10;
							string n = s.skeletonDataAsset.name;
							++rank;
							int currrank = System.Int32.Parse((n.Split('_'))[1]);
							if(rank != currrank)
							{
								s.skeletonDataAsset = Resources.Load((n.Split('_'))[0]+"_"+rank) as SkeletonDataAsset;
								s.Clear();
								s.Initialize();
							}
							s.state.SetAnimation("standby", true); 
						}
						else
						{
							s.renderer.enabled = false;
						}
					}*/
					string name = (t.name.Split('_'))[1].ToLower();				
					name = char.ToUpper(name[0])+name.Substring(1);	
					//GameObject.Find("title").GetComponent<SpriteText>().Text = name;
					PackedSprite sprite = newGO.GetComponent<PackedSprite>();
					sprite.DoAnim(0);
					sprite.PauseAnim();
					sprite.SetCurFrame(0);
					sprite.SetCurFrame(t.indexIcon);

					hitGO.renderer.material.color = new Vector4(0.1f, 0.1f, 0.1f, 1f);
					newGO.renderer.material.color = new Vector4(0.5f, 0.5f, 0.5f, 1f);

					string nnn = t.name;
					foreach(var butt in arrayButton)
					{
						TrooperWrapper t2 = arrayGua[butt.identifier];
						
						if(t2.name.Contains(t.name))
						{
							butt.gameObject.renderer.material.color = new Vector4(0.1f, 0.1f, 0.1f, 1f);	
						}					
					}
					StartCoroutine(animate(newGO));
				}
			}
			int count = 0;
			for(int i = 0; i < arrayB.Length; ++i)
			{
				if(arrayB[i] != null)				
				{
					++count;
				}
			}
			if(count > 1)
			{
				batgo.controlIsEnabled = true;
			}
			else
			{
				batgo.controlIsEnabled = false;
			}
		}
	}

	public IEnumerator animate(GameObject go)
	{
		Vector3 dest = Vector3.zero;
		for(int i = 0; i < 4; ++i)
		{
			if(arrayB[i] == null)
			{
				dest = GameObject.Find("container"+(i+1)).transform.position;
				GameObject.Find("container"+(i+1)).AddComponent<KYButton>().identifier = 500+i;
				dest.z -= 5;
				//go.transform.position =dest;
				arrayB[i] = go.GetComponent<KYButton>();
				go.GetComponent<Collider>().enabled = false;
				//arrayB[i].identifier = 500+i;
				
				go.transform.parent = GameObject.Find("pre-game(Clone)").transform.Find("bar").Find("bar_1");
				break;
			}
		}

		while((go.transform.position-dest).magnitude > 1f)
		{
			if(go == null)
			{
				yield break;
			}
			Vector3 pos = go.transform.position+(dest-go.transform.position)*Time.deltaTime*5;
			//pos.x += 0.2f;
			go.transform.position = pos;
			yield return null;
		}
		go.transform.position = dest;
	}

	public void onClickBattle()
	{
		GameObject.Find("but_battle").gameObject.SetActive(false);
		break1 = true;

		for(int i = 0; i < arrayB.Length; ++i)
		{
			if(arrayB[i] != null)
			{
				Vector3 pos = GameObject.Find("container"+(i+1)).transform.position;
				pos.z -= 5;
				arrayB[i].transform.position = pos;
			}
		}
		
		
		GameController.instance.hasBattleBegun = true;
		
		GameController.instance.showHUD();
	}

	public void fast()
	{
		++page;
		reload();
	}

	public void slow()
	{
		--page;
		reload();
	}

	int page = 0;

	public void reload()
	{
		return;
		for(int i = 0; i < arrayButton.Count; ++i)
		{
			var but = arrayButton[i];
			if(i/16 == page)
			{
				but.gameObject.SetActive(true);
				arraybox[i].SetActive(true);
			}
			else
			{
				but.gameObject.SetActive(false);
				arraybox[i].SetActive(false);
			}
		}

		if(page == 0)
		{
			pppp.SetActive(false);
		}
		else
		{
			pppp.SetActive(true);
		}

		if((arrayButton.Count-1)/16 == page)
		{
			nnnn.SetActive(false);
		}
		else
		{
			nnnn.SetActive(true);
		}	
	}

	public List<TrooperWrapper> arrayGua = new List<TrooperWrapper>();
	public List<SkeletonAnimation> sprites = new List<SkeletonAnimation>();
	public UIButton batgo;
	public GameObject pppp;
	public GameObject nnnn;
	public IEnumerator lateStart()	
	{
		GameObject but_pause = (GameObject.Instantiate(Resources.Load("but_pause") as GameObject) as GameObject);
		GameController.instance.pauseGO = but_pause;
		
		KYPlacementHelper ph = but_pause.GetComponent<KYPlacementHelper>();
		
		ph.gameObject.GetComponent<UIButton>().scriptWithMethodToInvoke = GameController.instance;
		gameObject.GetComponent<WaveManager>().enabled = false;
		cammy = GameObject.Find("GUI camera").GetComponent<Camera>();
		
		ph.cam = cammy;
		ph.update();

		GameObject pgo = GameObject.Instantiate(Resources.Load("panel") as GameObject) as GameObject;
		pgo.SetActive(false);
		gameObject.GetComponent<GameController>().enabled = false;
		GameObject newGO = GameObject.Instantiate(Resources.Load("pre-game") as GameObject) as GameObject;
		foreach(Transform child in newGO.transform)
		{
			if(child.gameObject.GetComponent<UIButton>())
			{
				child.gameObject.GetComponent<UIButton>().scriptWithMethodToInvoke = this;
			}
		}
		pppp = GameObject.Find("prev");
		nnnn = GameObject.Find("next");

		buttonAbilityGO = Resources.Load("button_ability") as GameObject;
		GameObject grayFrame = Resources.Load("grayframe") as GameObject;
		//GameObject boxGO = newGO.transform.Find("ppanel").Find("box").gameObject;
		batgo = newGO.transform.Find("but_battle").gameObject.GetComponent<UIButton>();
		batgo.controlIsEnabled = false;
		//SlotWrapper slotWrapper = KYDataManager.getInstance().slotWrapper;
		Vector3 origin = Vector3.zero;
		origin.z = -5;
		KYButton button;
		GameObject newAbilityButton;
		PackedSprite sprite;
		SpriteText text = null;
		int yy = 0;
		
		List <TrooperWrapper> sortedList = SortingHelper.sortBy(KYDataManager.getInstance().arrayTrooperWrapper, SortType.Level);
		
		GameObject holderGO = Resources.Load("EquipmentButton2") as GameObject;
		GameObject holder = null;
		GameObject unitButton = null;
		
		scrollList = newGO.transform.Find("ppanel").FindChild("scrollList").GetComponent<UIScrollList>();
		scrollList.scriptWithMethodToInvoke = this;
		arrayScrollItems = new List<UIListItemContainer>();
		
		unitInfoSection = newGO.transform.Find("ppanel").GetComponentInChildren<UnitInfo>();
		
		int numInRow = 3;
		
		for(int i = 0; i < sortedList.Count; ++i)
		{
			if(true)
			{
				TrooperWrapper trooperWrapper = sortedList[i];
				if(trooperWrapper != null && trooperWrapper.genre != 2 && trooperWrapper.level > 0)
				{
					newAbilityButton = Instantiate(buttonAbilityGO) as GameObject;

					string name = (trooperWrapper.name.Split('_'))[1];
					Fury.Database.Unit unit = Resources.Load(name) as Fury.Database.Unit;

					//GameObject newboxGO = Instantiate(boxGO) as GameObject;
					unitButton = Instantiate(Resources.Load ("EquipmentButton_inGame")) as GameObject;
					
					//check if it's bookmarked, then update lock icon accordingly
					ButtonLockMini miniLockIcon = unitButton.GetComponentInChildren<ButtonLockMini>();
					
					if (trooperWrapper.isBookmarked)
					{
						miniLockIcon.doLock(false);
					}
					
					else
					{
						miniLockIcon.doUnlock(false);
					}
					
					//new row
					if(yy%numInRow == 0)
					{
						holder = Instantiate(holderGO) as GameObject;
						holder.name = (yy/numInRow).ToString();
					}
					
					unitButton.transform.parent = holder.transform;
					unitButton.transform.localPosition = new Vector3((scrollList.viewableArea.x * (0.5f / numInRow) * (yy%numInRow + 1)) 
						- (scrollList.viewableArea.x * 0.5f) 
						+ (yy%numInRow * (scrollList.viewableArea.x * (0.5f / numInRow))), 0, 0);
					

					//newboxGO.transform.parent = newGO.transform.Find("ppanel");

					/*origin = Vector3.zero;
					origin.z = -4;
					origin.x = -28;
					origin.y = -3;
					origin.x += yy%4*31;
					origin.y -= (yy%16)/4*22;
					newboxGO.transform.localPosition = origin;*/
					
					//render level preview
					string level = "";
			
					if (CollectionManager.hulala(trooperWrapper.level) < 10)
					{
						level += "0";
					}
					
					level += CollectionManager.hulala(trooperWrapper.level);
					
					unitButton.transform.Find("cost").gameObject.GetComponent<SpriteText>().Text = level;
					button = unitButton.GetComponent<KYButton>();
					button.identifier = arrayButton.Count;
					
					//unitButton.transform.FindChild("portrait").GetComponent<PackedSprite>().PlayAnim(0, trooperWrapper.indexIcon);
					//unitButton.transform.FindChild("portrait").gameObject.SetActive(false);
					
					newAbilityButton.transform.localScale = new Vector3(1.7f, 1.7f, 1);
					newAbilityButton.layer = 10;
					newAbilityButton.transform.eulerAngles = Vector3.zero;
					newAbilityButton.transform.parent = unitButton.transform;
					//origin.z = -3;
					//origin.x = -33;
					//origin.x += yy%4*31;
					newAbilityButton.transform.localPosition = new Vector3(-5.6f, 0, 2);
					
					sprite = newAbilityButton.GetComponent<PackedSprite>();
					sprite.DoAnim(0);
					sprite.PauseAnim();
					sprite.SetCurFrame(0);
					sprite.SetCurFrame(trooperWrapper.indexIcon);
					
					text = newAbilityButton.transform.Find("KYText_info").gameObject.GetComponent<SpriteText>();
					int cost = trooperWrapper.cost;
					text.Text = "LV"+(trooperWrapper.level)+" $"+cost;
					
					newAbilityButton.GetComponent<SummonUnitButton>().hideAll();
					
					button = newAbilityButton.GetComponent<KYButton>();
					arrayButton.Add(button);
					arraybox.Add(unitButton);
					arrayGua.Add(trooperWrapper);
					button.identifier = arrayButton.Count-1;
					
					//newAbilityButton.GetComponent<BoxCollider>().center = new Vector3(0, 0, 0);
					//newAbilityButton.GetComponent<BoxCollider>().size = new Vector3(20, 15, 1);
					
					

					/*
					sprite = newboxGO.transform.Find("bu").gameObject.GetComponent<PackedSprite>();
					sprite.DoAnim(0);
					sprite.PauseAnim();
					sprite.SetCurFrame(trooperWrapper.level/10);
					*/

					for(int ii = (trooperWrapper.level-1)/10+2; ii <= 3; ++ii)
					{
						unitButton.transform.Find("star"+ii).gameObject.transform.localScale = Vector3.zero;
					}

					if(TitleController.ban.Contains(trooperWrapper.name.Split('_')[1]))
					{
						newAbilityButton.renderer.material.color = new Vector4(0.1f, 0.1f, 0.1f, 1f);		
					}
					
					if(yy%numInRow == numInRow - 1)
					{
						holder.GetComponent<UIListItemContainer>().ScanChildren();
						scrollList.AddItem(holder);
						
						arrayScrollItems.Add(holder.GetComponent<UIListItemContainer>());
					}
					
					
				
					++yy;
				}
			}
		}
		
		if (!arrayScrollItems.Contains(holder.GetComponent<UIListItemContainer>()))
		{
			scrollList.AddItem(holder);
		}
		
		reload();
		//Destroy(boxGO);

		/*foreach(Transform child in GameObject.Find("sprites").transform)
		{
			//Debug.Log ("children of 'sprites'..." + child.name);
			SkeletonAnimation s = child.gameObject.GetComponent<SkeletonAnimation>();
			s.state.SetAnimation("standby", true); 
			sprites.Add(s);
			//s.gameObject.renderer.material.shader = Shader.Find("Sprite/Vertex Colored");
			//s.gameObject.renderer.material.color = new Color(0.4f, 0.4f, 0.4f, 0.8f);
			s.renderer.enabled = false;
		}*/
		newGO.SetActive(false);

		yield return new WaitForSeconds(1.5f);
		gameObject.GetComponent<ResourceManager>().enabled = false;
		newGO.transform.Find("ppanel").gameObject.GetComponent<UIPanel>().StartTransition("Bring in Forward");
		GameObject.Find("overlayscreen2(Clone)").renderer.enabled = true;
		newGO.SetActive(true);
		
		GameObject bar1 = newGO.transform.Find("bar").Find("bar_1").gameObject;
		GameObject bar2 = newGO.transform.Find("bar").Find("bar_2").gameObject;
		GameObject bar = newGO.transform.Find("bar").gameObject;
		
		sideBar = bar;
		
		float delay = 0f, leadTime = 1.2f;
		
		iTween.MoveFrom(bar, iTween.Hash("x", -Screen.width * 0.5f, "time", leadTime, "delay", delay));
		
		for(int i = 1; i <= 4; ++i)
		{
			GameObject container = GameObject.Find("container"+i);
			GameObject newFrame = Instantiate(grayFrame) as GameObject;
			newFrame.transform.parent = container.transform;
			newFrame.transform.localPosition = new Vector3(0, 0, 0);
			newFrame.transform.localScale = new Vector3(1.2f, 1.2f, 1f);
		}
		while(!break1)
		{
			yield return null;
		}
		
		gameObject.GetComponent<ResourceManager>().enabled = true;
		
		TowerAgent.instance1.HPBar.gameObject.SetActive(true);
		TowerAgent.instance2.HPBar.gameObject.SetActive(true);
		TowerAgent2.instance1.HPBar.gameObject.SetActive(true);
		TowerAgent2.instance2.HPBar.gameObject.SetActive(true);		
		TowerAgent3.instance1.HPBar.gameObject.SetActive(true);
		TowerAgent3.instance2.HPBar.gameObject.SetActive(true);
		
   		iTween.MoveTo(bar1,iTween.Hash("path", new Vector3[] { bar1.transform.position, bar1.transform.position+new Vector3(0, 0, 0) },"time", 0.5f ,"easetype", "linear"));		
   		iTween.MoveTo(bar2,iTween.Hash("path", new Vector3[] { bar2.transform.position, bar2.transform.position+new Vector3(-15, 0, 4f) },"time", 0.5f ,"easetype", "linear"));
   		newGO.transform.Find("ppanel").gameObject.GetComponent<UIPanel>().StartTransition("Dismiss Forward");
   		yield return new WaitForSeconds(0.5f);
   		foreach(Transform child in bar2.transform)
   		{
   			if(child.gameObject.name == "cloner")
   			{
   				child.gameObject.SetActive(false);
   			}
   		}
   		float ppp = (float)Screen.width/(float)Screen.height*-100+(32.6f/2);
   		//float ppp2 = (float)Screen.width/(float)Screen.height*-100+(2.8f);
   		iTween.MoveTo(bar,iTween.Hash("path", new Vector3[] { bar.transform.position, new Vector3(ppp, bar.transform.position.y, bar.transform.position.z) },"time", 0.5f ,"easetype", "linear", "islocal", false));
       	yield return new WaitForSeconds(0.8f);
   		foreach(Transform child in bar2.transform)
   		{
   			if(child.gameObject.name == "cloner")
   			{
   				child.gameObject.SetActive(true);
   			}
   		}
       	pgo.SetActive(true);
		arrayButton.Clear();
		gameObject.GetComponent<WaveManager>().enabled = true;
		gameObject.GetComponent<ResourceManager>().enabled = true;
		gameObject.GetComponent<GameController>().enabled = true;
		GameObject.Find("overlayscreen2(Clone)").renderer.enabled = false;	
		newGO.transform.Find("bar").gameObject.transform.parent = null;
		Destroy(newGO);

		buttonSpellGO = Resources.Load("button_spell") as GameObject;

		float x = GameObject.Find("frame_normal").transform.position.x;
		origin = new Vector3(x, 0, -35);
		List<TrooperWrapper> arrayZ = new List<TrooperWrapper>();
		for(int i = 0; i < arrayB.Length; ++i)
		{
			if(arrayB[i] != null)
			{
				arrayZ.Add(arrayGua[arrayB[i].identifier]);
			}
		}
		
		for(int i = 0; i < KYDataManager.getInstance().arrayTrooperWrapper.Count; ++i)
		{
			if(true)
			{
				TrooperWrapper trooperWrapper = KYDataManager.getInstance().arrayTrooperWrapper[i];
				if(trooperWrapper.genre == 2)
				{
					arrayZ.Add(trooperWrapper);
				}
			}
		}

		for(int i = 1; i <= 4; ++i)
		{
			GameObject.Find("container"+i).GetComponent<Collider>().enabled = false;
		}
		
		for(int i = 0; i < arrayZ.Count; ++i)
		{
			if(true)
			{
				TrooperWrapper trooperWrapper = arrayZ[i];
				if(trooperWrapper != null)
				{
					KYAgent.AbilityWrapper abilityWrapper = null;
					foreach(var ability in TowerAgent2.instance1.unit.Properties.Abilities)
					{
						if(ability.name == trooperWrapper.name)
						{
							abilityWrapper = new KYAgent.SpawnAbilityWrapper(ability);
							abilityWrapper.level = trooperWrapper.level;
							(abilityWrapper as KYAgent.SpawnAbilityWrapper).spawnWrapper.level = trooperWrapper.level;
							TowerAgent2.instance1.listAbilityWrapper.Insert (i, abilityWrapper);
						}
					}
					/*
					newAbilityButton = Instantiate(buttonAbilityGO) as GameObject;	
					newAbilityButton.transform.position = origin;
					newAbilityButton.transform.parent = pgo.transform;
					*/
					if(trooperWrapper.name.Contains("Harvester"))
					{
						newAbilityButton = GameObject.Instantiate(arrayB[0].gameObject) as GameObject;
						newAbilityButton.transform.position = arrayB[0].transform.position;
						newAbilityButton.name = "clonerHarvester";
						
						newAbilityButton.transform.parent =  (GameObject.Find("bar").transform.FindChild("bar_1").transform);
						//newAbilityButton.transform.parent =  (GameObject.Find("bar").transform.FindChild("bar_1"));
						
						GameObject newG = GameObject.Instantiate(GameObject.Find("container1")) as GameObject;
						newG.name = "containerHarvester";
						newG.transform.localScale = new Vector3(1.2f, 1.2f, 1);
						newG.transform.position = GameObject.Find("container1").transform.position;
						newG.transform.position += new Vector3(0, -130, 0);
						newAbilityButton.transform.position += new Vector3(0, -130, 0);
						
						newG.transform.parent = newAbilityButton.transform;
						
						sprite = newAbilityButton.GetComponent<PackedSprite>();
						sprite.DoAnim(0);
						sprite.PauseAnim();
						sprite.SetCurFrame(0);
						sprite.SetCurFrame(trooperWrapper.indexIcon);
						Destroy(arrayB[0].gameObject);
						
					}
					else if(i == 0)
					{
						newAbilityButton = GameObject.Instantiate(arrayB[0].gameObject) as GameObject;	
						newAbilityButton.transform.position = arrayB[0].transform.position;
						
						newAbilityButton.name = arrayB[0].gameObject.name;
						newAbilityButton.transform.parent =  (GameObject.Find("bar").transform.FindChild("bar_1").transform);
						
						sprite = newAbilityButton.GetComponent<PackedSprite>();
						sprite.DoAnim(0);
						sprite.PauseAnim();
						sprite.SetCurFrame(0);
						sprite.SetCurFrame(trooperWrapper.indexIcon);
						
						//Destroy(arrayB[0].gameObject);
						//arrayB[0] = newAbilityButton.GetComponent<KYButton>();					
					}
					else
					{
						newAbilityButton = arrayB[i].gameObject;
					}

					text = newAbilityButton.transform.Find("KYText_info").gameObject.GetComponent<SpriteText>();
					int cost = trooperWrapper.cost;
					text.Text = "LV"+(trooperWrapper.level)+" $"+cost;
					button = newAbilityButton.GetComponent<KYButton>();
					arrayButton.Add(button);
					button.identifier = arrayButton.Count-1;
					
					newAbilityButton.transform.GetComponentInChildren<SummonUnitButton>().Init();
					
					if (i < arrayB.Length)
					{
						GameObject.Find("container"+(i+1)).transform.parent = button.transform;
					}
					
					newAbilityButton.GetComponent<Collider>().enabled = true;
					foreach(Transform child in newAbilityButton.transform)
					{
						child.gameObject.layer = 10;
					}
					if(arrayButton1.Count < 4 && !trooperWrapper.name.Contains("Harvester"))
					{
						arrayButton1.Add(button);
					}
					else if(!trooperWrapper.name.Contains("Harvester"))
					{
						arrayButton2.Add(button);
						button.gameObject.SetActive(false);
					}
					/*
					else if(trooperWrapper.genre == 1)
					{
						button.transform.position = new Vector3(x, 0, -35);
					}
					
					else
					{
						origin.z += 12;
						button.transform.position = origin;
						if(arrayButton1.Count < 4)
						{
							arrayButton1.Add(button);
							if(arrayButton1.Count == 4)
							{
								origin.z = -35f;
							}
						}
						else
						{
							arrayButton2.Add(button);
							button.transform.position = origin;
							button.gameObject.SetActive (false);
						}
					}
					*/
				}
			}
		}
		
		GameObject newSpellButton;
		origin = Vector3.zero;
		origin.z = -33;
		origin.x = 52.2f;
		/*
		newSpellButton = Instantiate(buttonSpellGO, origin, Quaternion.identity) as GameObject;	
		sprite = newSpellButton.GetComponent<PackedSprite>();
		sprite.DoAnim(0);
		sprite.PauseAnim();	
		button = newSpellButton.GetComponent<KYButton>();
		arrayButton.Add(button);
		button.identifier = arrayButton.Count-1;


		origin.x -= 8;
		*/
		newSpellButton = Instantiate(buttonSpellGO) as GameObject;	
		newSpellButton.transform.position = origin;
		newSpellButton.gameObject.name = "bag";
		sprite = newSpellButton.GetComponent<PackedSprite>();
		sprite.DoAnim(0);
		sprite.PauseAnim();		
		sprite.SetCurFrame(4);	
		newSpellButton.GetComponent<KYButton>().identifier = 20;
		bag = sprite;
		bag.transform.position = arrayB[0].transform.position+new Vector3(0, 25, 0);
		bag.transform.localScale = new Vector3(2.3f, 2.3f, 3);
		bag.gameObject.SetActive(false);
		GameController.instance.launch.SetActive(false);
		bag.transform.eulerAngles = new Vector3(0, 0, 0);
		//Debug.Log(ppp2);
   		//bag.transform.position = new Vector3(ppp2, origin.y, origin.z);
   		bag.gameObject.layer = 10;
   		foreach(Transform child in bag.transform)
   		{
   			child.gameObject.layer = 10;
   		}
		List<KYData.SpellWrapper> arraySpellWrapper = KYDataManager.getInstance().arraySpellWrapper;

		int prevIndex = arrayButton.Count-1;
		origin.z = 33;
		foreach(SpellWrapper spellWrapper in arraySpellWrapper)
		{
			if(spellWrapper != null)
			{
				origin.x -= 10;
				newSpellButton = Instantiate(buttonSpellGO) as GameObject;	
				newSpellButton.transform.position = origin;
				sprite = newSpellButton.GetComponent<PackedSprite>();
				sprite.DoAnim(0);
				sprite.PauseAnim();		
				sprite.SetCurFrame(spellWrapper.index);
				button = newSpellButton.GetComponent<KYButton>();
				arrayButton.Add(button);
				arrayButton3.Add(button);
				button.identifier = prevIndex+spellWrapper.index;
				//button.text.Text = spellWrapper.count+"";
			}		
		}

		queuedAbility = null;
		controlState = ControlState.Default;
		cammy2 = GameObject.Find("buttonCamera").GetComponent<Camera>();

		del = update2;
		isScrolling = 0;
	}

	public void Update()
	{
		//Debug.Log (GameObject.Find("bar").transform.FindChild("bar_1").transform.FindChild("cloner1"));
		del();
	}

	public void setQueuedButton(KYButton button)
	{
		Vector3 scale;
		if(queuedButton != null)
		{
			scale = queuedButton.overlay.localScale;
			scale.y = 0;
			queuedButton.overlay.localScale = scale;
			queuedButton = null;
		}
		if(button != null)
		{
			
			queuedButton = button;
			scale = queuedButton.overlay.localScale;
			scale.y = 1;
			queuedButton.overlay.localScale = scale;
		}
	}

	public void changeTab()
	{
		if(tab == 0)
		{
			foreach(KYButton button in arrayButton1)
			{
				button.gameObject.SetActive(false);
			}
			foreach(KYButton button in arrayButton2)
			{
				button.gameObject.SetActive(true);
			}
			tab = 1;
		}
		else if(tab == 1)
		{
			foreach(KYButton button in arrayButton1)
			{
				button.gameObject.SetActive(true);
			}
			foreach(KYButton button in arrayButton2)
			{
				button.gameObject.SetActive(false);
			}
			tab = 0;		
		}
	}

	public void update2()
	{
		
		
		inputWrapper.update();
		RaycastHit hit;
		Ray ray;
	 	hitGO = null;
	 	KYButton hitButton = null;

		if(GameController.instance.isEnd == 10)
		{
			return;
		}

		/*
		foreach(KYButton but in arrayButton)
		{
			if(but.text.Text == "")
			{
				but.text.Text = "ULULU";				
			}
		}
		*/
		Vector3 prev = Hud.FocusPoint;
		if(inputWrapper.isTrigger || inputWrapper.isPress || inputWrapper.isRelease)
		{
   			ray = cammy.ScreenPointToRay(inputWrapper.screenPos);

   			if(Physics.Raycast(ray, out hit, Mathf.Infinity, 1 << 10))
			{
				hitGO = hit.collider.gameObject;
				hitButton = hitGO.GetComponent<KYButton>();
			}	
			ray = cammy2.ScreenPointToRay(inputWrapper.screenPos);

   			if(Physics.Raycast(ray, out hit, Mathf.Infinity, 1 << 11))
			{
				hitGO = hit.collider.gameObject;
				hitButton = hitGO.GetComponent<KYButton>();
			}	
			Hud.CalculateFocus(ray);
			Hud.FocusPoint -= new Vector3(0, 0, Hud.FocusPoint.y);
			if(inputWrapper.isTrigger)
			{
				offsetX = 0;
				offsetY = 0;
				prev = Hud.FocusPoint;
			}
		}
		offsetX += Hud.FocusPoint.x-prev.x;
		offsetY += Hud.FocusPoint.z-prev.z;

		TowerAgent2 agent2 = TowerAgent2.instance1;
		if(agent2)
		{
			int k = 0;
			Vector3 scale, pos;
			KYButton button;
			foreach(KYAgent.AbilityWrapper abilityWrapper in agent2.listAbilityWrapper)
			{
				if(k >= arrayButton.Count)
				{
					break;
				}
				button = arrayButton[k];
				if(queuedButton != button)
				{
					scale = button.overlay.localScale;
					pos = Vector3.zero;
					pos.z = -1;
					float cd = (abilityWrapper.cooldown == 0)? 0 : Mathf.Clamp01(abilityWrapper.cooldownLeft/abilityWrapper.cooldown);
					scale.y = cd;
					if(cd != 0)
					{
						//pos.y -= (1-scale.y)*0.7f*8f;
						pos.y -= (1-scale.y)*5f;
					}
					/*
					pos.y += 5;
					pos.z += 5;
					*/
					
					button.overlay.localScale = scale;
					button.overlay.transform.localPosition = pos;
				}
				++k;
			}

			if(isScrolling == 1)
			{
				if(bag.GetCurAnim().GetCurPosition() != 4) bag.SetCurFrame(0);
				foreach(KYButton but in arrayButton3)
				{
					if(but.gameObject.activeSelf)
					{
						pos = but.transform.position;
						pos.x += 2;
						but.transform.position = pos;
						if(pos.x > 15)
						{
							but.gameObject.SetActive(false);
						}
					}
				}					
			}
			else if(isScrolling == 0)
			{
				int originX = 50;
				if(bag.GetCurAnim().GetCurPosition() != 4)
				{
					bag.SetCurFrame(5);
				}
				foreach(KYButton but in arrayButton3)
				{
					originX -= 10;
					if(but.transform.position.x > originX)
					{
						pos = but.transform.position;
						pos.x -= 2;
						but.transform.position = pos;
					}
				}					
			}

			int mmm = 0;

			foreach(var abilityWrapper in agent2.listAbilityWrapper)
			{
				if(abilityWrapper.abilityType == AbilityType.Summon)			
				{
					if((int)GameController.instance.arrayPlayer[0].amountGold >= (abilityWrapper as KYAgent.SpawnAbilityWrapper).spawnWrapper.cost)
					{	
						if(Mathf.Abs(arrayButton[mmm].renderer.material.color.r-0.5f) < 0.1f)
						{
						}
						else
						{
							arrayButton[mmm].renderer.material.color = new Vector4(0.5f, 0.5f, 0.5f, 1f);						
						}
					}
					else
					{

						if(Mathf.Abs(arrayButton[mmm].renderer.material.color.r-0.5f) < 0.1f)
						{
							arrayButton[mmm].renderer.material.color = new Vector4(0.1f, 0.1f, 0.1f, 1f);						
						}				
					}

					if(agent2.minionController.getCount((abilityWrapper as KYAgent.SpawnAbilityWrapper).spawnWrapper.unit.name)+(abilityWrapper as KYAgent.SpawnAbilityWrapper).spawnWrapper.count < (abilityWrapper as KYAgent.SpawnAbilityWrapper).spawnWrapper.unit.maximum)
					{
						if(arrayButton[mmm].max.renderer.enabled)
						{
							Debug.Log ("not max" + arrayButton[mmm].name);
							
							SummonUnitButton summonButton = arrayButton[mmm].transform.GetComponent<SummonUnitButton>()?? arrayButton[mmm].transform.parent.GetComponent<SummonUnitButton>();
							
							summonButton.hideMax();
							
							arrayButton[mmm].max.renderer.enabled = false;				
						}
					}
					else
					{
						//Debug.Log (abilityWrapper.ability.name + " maxed at " + (abilityWrapper as KYAgent.SpawnAbilityWrapper).spawnWrapper.unit.maximum.ToString() +"!");
						if(!arrayButton[mmm].max.renderer.enabled)
						{
							Debug.Log ("max" + arrayButton[mmm].name);
							
							SummonUnitButton summonButton = arrayButton[mmm].transform.GetComponent<SummonUnitButton>()?? arrayButton[mmm].transform.parent.GetComponent<SummonUnitButton>();
							
							summonButton.showMax();
							
							arrayButton[mmm].max.renderer.enabled = true;	

						}
					}

					if((abilityWrapper as KYAgent.SpawnAbilityWrapper).spawnWrapper.unit.Prefab.tag == "harvester")
					{
						if(agent2.minionController.listHarvesterIdentifier.Count+(abilityWrapper as KYAgent.SpawnAbilityWrapper).spawnWrapper.count < GameController.instance.arrayPlayer[0].harvesterCount)
						{
							if(arrayButton[mmm].max.renderer.enabled)
							{
								Debug.Log ("not max" + arrayButton[mmm].name);
								
								SummonUnitButton summonButton = arrayButton[mmm].transform.GetComponent<SummonUnitButton>()?? arrayButton[mmm].transform.parent.GetComponent<SummonUnitButton>();
							
								summonButton.hideMax();
								
								arrayButton[mmm].max.renderer.enabled = false;				
							}
						}
						else
						{

							if(!arrayButton[mmm].max.renderer.enabled)
							{
								Debug.Log ("max" + arrayButton[mmm].name);
								
								SummonUnitButton summonButton = arrayButton[mmm].transform.GetComponent<SummonUnitButton>()?? arrayButton[mmm].transform.parent.GetComponent<SummonUnitButton>();
							
								summonButton.showMax();
								
								arrayButton[mmm].max.renderer.enabled = true;				
							}
						}
					}

					++mmm;
				}
			}

			if(inputWrapper.isTrigger)
			{
			}
	
			else if(inputWrapper.isRelease)
			{
				if(!(Hud.FocusPoint.x < 40) && Mathf.Abs(offsetX) > 2 && Mathf.Abs(offsetY) < Mathf.Abs(offsetX))
				{
					//changeTab();
				}
				else if(hitGO && hitButton)
				{
					Debug.Log ("releasing..." + hitButton.identifier + ", abilityCount: " + agent2.listAbilityWrapper.Count.ToString());
					KYAgent.AbilityWrapper abilityWrapper = null;
					if(hitButton.identifier >= agent2.listAbilityWrapper.Count)
					{
						if(hitButton.identifier == 20)
						{
							if(bag.GetCurAnim().GetCurPosition() == 4)
							{
								int j = 0;
								foreach(KYAgent.AbilityWrapper wrapper in agent2.listAbilityWrapper)
								{
									if(wrapper.abilityType == AbilityType.Summon)
									{
										KYAgent.SpawnWrapper spawnWrapper = (wrapper as KYAgent.SpawnAbilityWrapper).spawnWrapper;
										GameController.instance.arrayPlayer[0].amountGold += spawnWrapper.cost*spawnWrapper.count;
										spawnWrapper.resetCount();
										arrayButton[j].text.Text = "";
									}

									++j;										
								}
								controlState = ControlState.Default;
								bag.gameObject.SetActive(false);
								GameController.instance.launch.SetActive(false);
								if(isOut == 1)
								{
									isScrolling = 0;
									foreach(KYButton but in arrayButton3)
									{
										but.gameObject.SetActive(true);
									}
								}
							}
							else
							{
								if(isScrolling == 0)
								{
									isScrolling = 1;
								}
								else if(isScrolling == 1)
								{
									isScrolling = 0;
									foreach(KYButton but in arrayButton3)
									{
										but.gameObject.SetActive(true);
									}
								}
							}
						}
						return;
					}
					abilityWrapper = agent2.listAbilityWrapper[hitButton.identifier];
					float cd = (abilityWrapper.cooldown == 0)? 0 : Mathf.Clamp01(abilityWrapper.cooldownLeft/abilityWrapper.cooldown);
					if(cd == 0 && Time.timeScale != 0)
					{
						if(abilityWrapper.abilityType == AbilityType.Summon && (int)GameController.instance.arrayPlayer[0].amountGold >= (abilityWrapper as KYAgent.SpawnAbilityWrapper).spawnWrapper.cost)
						{	
							KYAgent.SpawnWrapper spawnWrapper = (abilityWrapper as KYAgent.SpawnAbilityWrapper).spawnWrapper;
							if(spawnWrapper.unit.Prefab.tag == "harvester" && agent2.minionController.listHarvesterIdentifier.Count+spawnWrapper.count >= GameController.instance.arrayPlayer[0].harvesterCount)
							{
								hitButton.text.Text = spawnWrapper.count+"";
								controlState = ControlState.Summon;
								setQueuedButton(null);
							}
							else if(agent2.minionController.getCount(spawnWrapper.unit.name)+spawnWrapper.count < spawnWrapper.unit.maximum)
							{
								spawnWrapper.incrementCount();
								GameController.instance.arrayPlayer[0].amountGold -= spawnWrapper.cost;
								useGold += (int)spawnWrapper.cost;
								bag.gameObject.SetActive(true);
								GameController.instance.launch.gameObject.SetActive(true);
								hitButton.text.Text = spawnWrapper.count+"";
								controlState = ControlState.Summon;
								setQueuedButton(null);
							}

						}
						else if(abilityWrapper.abilityType  == AbilityType.Charge)
						{
							controlState = ControlState.Charge;
							queuedAbility = abilityWrapper.ability;
							setQueuedButton(hitButton);
						}
						else if(abilityWrapper.abilityType  == AbilityType.Active)
						{
							controlState = ControlState.Default;
							bool isBam = false;
							for(int i = 0; i < arrayButton3.Count; ++i)
							{
								if(arrayButton3[i] == hitButton)
								{
									KYDataManager.getInstance().arraySpellWrapper.RemoveAt(i);
									isBam = true;
									continue;
								}
								if(isBam)
								{
									Vector3 pos2 = arrayButton3[i].transform.position;
									pos2.x += 10;
									arrayButton3[i].transform.position = pos2;
								}
							}
							arrayButton3.Remove(hitButton);
							arrayButton.Remove(hitButton);
							Destroy(hitButton.gameObject);
							setQueuedButton(null);
							
							agent2.unit.Order(abilityWrapper.ability, agent2.unit, Vector3.zero);
						}
					}
				}
				else if(controlState == ControlState.Summon && isOutPanel())
				{
					List<KYAgent.SpawnWrapper> unitsToSpawn = new List<KYAgent.SpawnWrapper>();
					Vector3 spawnLocation = new Vector3(LevelManager.instance.leftX-1f, 0, Hud.FocusPoint.z);
					int j = 0;
					
					foreach(var abilityWrapper in agent2.listAbilityWrapper)
					{
						if(abilityWrapper.abilityType == AbilityType.Summon)
						{
							KYAgent.SpawnWrapper spawnWrapper = (abilityWrapper as KYAgent.SpawnAbilityWrapper).spawnWrapper;
							if(spawnWrapper.count > 0)
							{
								//MinionController minionController = agent2.minionController;
								//minionController.CreateSpawn (spawnWrapper.unit, spawnWrapper.count, new Vector3(LevelManager.instance.leftX+2f, 0, Hud.FocusPoint.z), abilityWrapper.level);
								
								for (int i = 0; i < spawnWrapper.count; i++)
								{
									unitsToSpawn.Add(spawnWrapper);
								}
								
								//minionController.ss = false;
								abilityWrapper.cooldown = spawnWrapper.count*abilityWrapper.fixedCooldown;
								abilityWrapper.cooldownLeft = abilityWrapper.cooldown;
								spawnWrapper.resetCount();
							}
							arrayButton[j].text.Text = "";			
							++j;
						}
					}
					
					MinionController minionController = agent2.minionController;
					minionController.ss = false;
					
					minionController.SpawnSquad(unitsToSpawn, spawnLocation);
					
					agent2.minionController.ss = true;
					controlState = ControlState.Default;
					bag.gameObject.SetActive(false);
					GameController.instance.launch.gameObject.SetActive(false);
				}
				else if(controlState == ControlState.Charge && isOutPanel())
				{
					Vector3 direction = (agent2.unit.transform.position-Hud.FocusPoint).normalized;
					Vector3 targetPos = agent2.unit.transform.position+(-direction*inputWrapper.chargeTime*50f);
					if(targetPos.x < LevelManager.instance.rightX)
					{
						targetPos.x = LevelManager.instance.rightX;
					}
					if(targetPos.z < -25)
					{
						targetPos.z = -25;
					}
					if(targetPos.z > 25)
					{
						targetPos.z = 25;
					}
					targetPos.y = agent2.unit.transform.position.y;
					agent2.unit.Order(queuedAbility, agent2.unit, targetPos);
					setQueuedButton(null);
					controlState = ControlState.Default;
				}
			}	
		}
	}


	public bool isOutPanel()
	{
		if(Hud.FocusPoint.x < 38 && Hud.FocusPoint.z < 30 && Hud.FocusPoint.z > -30)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public void hideUnitSideBar()
	{
		iTween.MoveTo(sideBar, iTween.Hash("x", sideBar.transform.position.x - 40, "time", 1f));
	}
}