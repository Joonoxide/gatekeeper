using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class MinionLogic : KYController
{
	//public Single AggroDistance = 3;
	//public Single ChaseDistance = 10;
	
	private Fury.Behaviors.Unit minion;
	
	public Fury.Database.Status LevelBuff = null;
	public int MinionLevel;
	public string BuffName = "";
	
	public Single D_Health;
	public Single D_PreDelay;
	public Single D_PostDelay;
	public Single D_CoolDown;
	public Single D_Damage;
	public bool isBah = false;
	
	void Start()
	{
		minion = GetComponent<Fury.Behaviors.Unit>();
		
		MinionLevel = PlayerPrefs.GetInt(BuffName);
		
		spriteScript  = gameObject.GetComponentInChildren(typeof(PackedSprite)) as PackedSprite;
	}
	
	// Update is called once per frame
	void Update () 
	{
		Debug.Log("minion logic?");
		if (minion != null && minion.Controllers.VitalityController.Health > 0)
		{
			if(!isBah)
			{
				minion.Controllers.VitalityController.Health = minion.Controllers.VitalityController.MaxHealth;
				isBah = true;
			}
			
            if(transform.Find("HPBar"))
			{
				GameObject HPBarGO = transform.Find("HPBar").gameObject;
				var vitCtrl = minion.Controllers.VitalityController;
				float currentHP = vitCtrl.Health;
				float maxHP = vitCtrl.MaxHealth;
				HPBar HPScript = HPBarGO.GetComponent(typeof(HPBar)) as HPBar;
				HPScript.percent = currentHP/maxHP;
			}
			if (Time.timeScale == 0)
				spriteScript.StopAnim();
			
			if (minion.Controllers.MovementController.TargetUnit)
			{
				if (minion.transform.position.x > minion.Controllers.MovementController.TargetUnit.transform.position.x)
					{spriteScript.winding = SpriteRoot.WINDING_ORDER.CW;}
				else
					{spriteScript.winding = SpriteRoot.WINDING_ORDER.CCW;}
			}
			else if (minion.Controllers.MovementController.TargetPosition.HasValue)
			{
				if (minion.transform.position.x > minion.Controllers.MovementController.TargetPosition.Value.x)
					{spriteScript.winding = SpriteRoot.WINDING_ORDER.CW;}
				else
					{spriteScript.winding = SpriteRoot.WINDING_ORDER.CCW;}
			}
            if(engageUnit)
            {
                if (minion.transform.position.x > engageUnit.transform.position.x)
					{spriteScript.winding = SpriteRoot.WINDING_ORDER.CW;}
				else
					{spriteScript.winding = SpriteRoot.WINDING_ORDER.CCW;}
            }
            if(engageUnit)
            {
                Vector3 targetV3;
                KYController targetController = engageUnit.gameObject.GetComponent<KYController>();
                if(sss == 0)
                {
                    minion.Order(transform.position);
                    sss = 1;
                }
                else if(sss == 1)
                {
                    if(targetController.engageUnit == minion)
                    {
                        if(engageUnit.transform.position.x < minion.transform.position.x)
                        {
                            targetV3 = engageUnit.transform.position+new Vector3(3, 0, 0);
                        }
                        else
                        {
                            targetV3 = engageUnit.transform.position-new Vector3(3, 0, 0);
                        }
                        movement = targetV3-transform.position;
                        Vector3 movementSpeed = (movement).normalized*10*Time.deltaTime;
                        if(movement.magnitude > movementSpeed.magnitude)
                        {
                            transform.position += movementSpeed;
                        }
                        else
                        {
                            transform.position += movement;
                        }
                        if((engageUnit.transform.position-transform.position).magnitude < 5)
                        {
                            sss = 2;
                            if(engageUnit.transform.position.x < minion.transform.position.x)
                            {
                            	targetController.spriteScript.winding = SpriteRoot.WINDING_ORDER.CCW;
                            }
                            else
                            {
            	                targetController.spriteScript.winding = SpriteRoot.WINDING_ORDER.CW;
                            }
                            engageUnit.Order(engageUnit.transform.position);
                        }
                    }
                    else
                    {
    					if(targetController.spriteScript.winding == SpriteRoot.WINDING_ORDER.CW)
						{
							targetV3 = engageUnit.transform.position+new Vector3(3, 0, 0);
						}
						else
						{
							targetV3 = engageUnit.transform.position-new Vector3(3, 0, 0);
						}
						movement = targetV3-transform.position;
                        Vector3 movementSpeed = (movement).normalized*10*Time.deltaTime;
                        if(movement.magnitude > movementSpeed.magnitude)
                        {
                            transform.position += movementSpeed;
                        }
                        else
                        {
                            transform.position += movement;
							sss = 3;
                        }
                    }
				}
                else if(sss == 2)
                {
					if(engageUnit.transform.position.x < minion.transform.position.x)
                    {
             	       targetV3 = engageUnit.transform.position+new Vector3(3, 0, 0);
                    }
                    else
                    {
	                    targetV3 = engageUnit.transform.position-new Vector3(3, 0, 0);
                    }
                    movement = targetV3-transform.position;
                    Vector3 movementSpeed = (movement).normalized*10*Time.deltaTime;
                    if(movement.magnitude > movementSpeed.magnitude)
                    {
                        transform.position += movementSpeed;
                    }
                    else
                    {
                        transform.position += movement;
                        sss = 3;
                    }
                }
                else if(sss == 3)
                {
                    if(targetController.engageUnit == minion)
                    {
                        minion.Order(engageUnit);
                        engageUnit.Order(minion);
                        sss = 4;
                    }
                    else
                    {
                        minion.Order(engageUnit);
                        sss = 4;
                    }

                }
                else if(sss == 4)
                {
                }
            }
                        
            if(engageUnit && sss < 3)
            {
                spriteScript.DoAnim(1);
            }
			else if (minion.State == Fury.UnitStates.Idle)
			{
				spriteScript.PlayAnim(0);
			}
			else if (minion.State == Fury.UnitStates.MovingToPosition)
			{
				spriteScript.DoAnim(1);
			}
			else if (minion.State == Fury.UnitStates.MovingToTarget)
			{
				spriteScript.DoAnim(1);
			}
			else if (minion.State == Fury.UnitStates.CastingAbility)
			{
				spriteScript.DoAnim(2);
			}
			else if (minion.State == Fury.UnitStates.AttackingUnit)
			{
				if(minion.Controllers.WeaponController.Target)
				{
					if (minion.Controllers.WeaponController.State == Fury.Controllers.WeaponController.States.Queued)
					{
						spriteScript.DoAnim(2);
					}
				}
			}
		}
		else 
			spriteScript.DoAnim(3);
	}
}


