using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class MinionController : MonoBehaviour
{
	public Fury.Database.Unit SpawnType = null;
	
	public Single staticRange = 10;
	
	public Int32 SpawnNum = 3;
	public Single Radius = 3;
	public Single ChaseDistance = 20f;
	public Single AggroDistance = 15f;
	public Single RespawnFrequency = 10f; 
		
	public Int32 CoinCost = 10;
	public Int32 SlotCost = 1;
	
	public UnityEngine.GameObject SummonEffect = null;
	public UnityEngine.GameObject DeathEffect = null;
	
	public List<Int32> Minions;
	public List<Int32> listHarvesterIdentifier;
	public SpriteText slotText;
	public SpriteText coinText;
	public Vector3 spawnOrigin;
	int count = 0;
	public Dictionary<string, int> dictionaryUnitCount = new Dictionary<string, int>();
	public Dictionary<string, List<KYAgent>> dictionaryUnit = new Dictionary<string, List<KYAgent>>();
	public int identifier;
	
	public static int[] deadHarvester = new int[2];
	public static int[] deadTroopers = new int[2];
	
	private Dictionary<int, List<int>> invokedUnitsByOwner = new Dictionary<int, List<int>>();

	private void Start ()
	{
		Fury.Behaviors.Manager.Instance.OnUnitDead += OnUnitDead;
		Fury.Behaviors.Manager.Instance.OnUnitCreated += OnUnitCreated;
		
		Minions = new List<Int32>();
		listHarvesterIdentifier = new List<Int32>();

		/*dictionaryUnitCount["Swordman"] = 0;
		dictionaryUnitCount["Harvester"] = 0;
		dictionaryUnitCount["Crusader"] = 0;
		dictionaryUnitCount["Archer"] = 0;
		dictionaryUnitCount["Cyclops"] = 0;
		dictionaryUnitCount["Priest"] = 0;
		dictionaryUnitCount["Griffin"] = 0;
		dictionaryUnitCount["Necromancer"] = 0;
		dictionaryUnitCount["Skeleton"] = 0;
		dictionaryUnitCount["Paladdin"] = 0;
		*/
		
		dictionaryUnit["Swordman"] = new List<KYAgent>();
		dictionaryUnit["Harvester"] = new List<KYAgent>();
		dictionaryUnit["Crusader"] = new List<KYAgent>();
		dictionaryUnit["Archer"] = new List<KYAgent>();
		dictionaryUnit["Cyclops"] = new List<KYAgent>();
		dictionaryUnit["Priest"] = new List<KYAgent>();
		dictionaryUnit["Griffin"] = new List<KYAgent>();
		dictionaryUnit["Necromancer"] = new List<KYAgent>();
		dictionaryUnit["Skeleton"] = new List<KYAgent>();
		dictionaryUnit["Paladin"] = new List<KYAgent>();
		dictionaryUnit["Medusa"] = new List<KYAgent>();
		dictionaryUnit["Ghoul"] = new List<KYAgent>();
		dictionaryUnit["Golem"] = new List<KYAgent>();
		dictionaryUnit["Elementalist"] = new List<KYAgent>();

		deadHarvester[0] = 0;
		deadHarvester[1] = 0;
		
		deadTroopers[0] = 0;
		deadTroopers[1] = 0;
	}
	
	void OnDestroy()
	{
		Fury.Behaviors.Manager.Instance.OnUnitDead -= OnUnitDead;
		Fury.Behaviors.Manager.Instance.OnUnitCreated -= OnUnitCreated;
	}
	
	public int getCount(string str)
	{
		//return dictionaryUnitCount[str];
		return dictionaryUnit[str].Count;
	}
	
	public int getSubSpawnCount(int unitIdentifier)
	{
		if (!invokedUnitsByOwner.ContainsKey(unitIdentifier))
		{
			invokedUnitsByOwner[unitIdentifier] = new List<int>();
		}
		
		return invokedUnitsByOwner[unitIdentifier].Count;
	}
	
	public void OnUnitCreated(Fury.Behaviors.Unit createdUnit)
	{
		/*if (statusIndicatorGO == null || !(createdUnit.agent is WalkableAgent))
		{
			return;
		}
		
		Vector3 pos = createdUnit.transform.position + statusIndicatorGO.transform.position;
		GameObject indicator = (Instantiate(statusIndicatorGO, pos, Quaternion.identity) as GameObject);
		
		indicator.transform.parent =  createdUnit.gameObject.transform;
		indicator.SetActive(false);*/
		
		//if the created unit is a walkable agent (regular battle units i.e. not trees) AND it belongs to the right side,
		//add to the count
		if (createdUnit.agent is WalkableAgent && identifier == createdUnit.Owner.Identifier)
		{
			dictionaryUnit[(createdUnit.Properties as Fury.Database.Unit).name].Add(createdUnit.agent);			
		}
			
	}
	
	public void OnUnitDead(Fury.Behaviors.Unit deadUnit, Fury.Behaviors.Unit lastAttacker)
	{
		if(deadUnit.agent is TowerAgent2)
		{
			transform.parent = GameController.instance.transform;
		}
		if (Minions.Exists(i => i == deadUnit.Identifier))
		{
			KYHUD.spaceUnits(deadUnit.transform.position, deadUnit.Owner.Identifier);
			Minions.Remove(deadUnit.Identifier);
			
			deadUnit.State = Fury.UnitStates.Dead;
			++ResourceManager.instance.fallen;

			//--dictionaryUnitCount[deadUnit.Properties.Prefab.name];
			dictionaryUnit[(deadUnit.Properties as Fury.Database.Unit).name].Remove(deadUnit.agent);
			++deadTroopers[deadUnit.Owner.Identifier];
			
			foreach(var entry in invokedUnitsByOwner)
			{
				if ((entry.Value).Contains(deadUnit.Identifier))
				{
					(entry.Value).Remove(deadUnit.Identifier);
					break;
				}
			}
			
			//Spawn 'Dead' pop-up
			if (UnityEngine.Random.value > 0.5f)
			{
				//Spawn 'Dead' pop-up
				GameObject popup = Instantiate(GameController.instance.popUpTextGO, deadUnit.transform.position + new Vector3(0, 3, 1), Quaternion.identity) as GameObject;
				
				PackedSprite sprite = popup.GetComponent<PackedSprite>(); 
				
				sprite.PlayAnim(0, (int)ToonPopOutAnim.Dead);
			}
		}
		else if (listHarvesterIdentifier.Exists(i => i == deadUnit.Identifier))
		{
			KYHUD.spaceUnits(deadUnit.transform.position, deadUnit.Owner.Identifier);
			listHarvesterIdentifier.Remove(deadUnit.Identifier);

			deadUnit.State = Fury.UnitStates.Dead;
			//--dictionaryUnitCount[deadUnit.name];
			dictionaryUnit[(deadUnit.Properties as Fury.Database.Unit).name].Remove(deadUnit.agent);
			if(GameController.instance.isEnd != 10 && lastAttacker != null)
			{
				++deadHarvester[deadUnit.Owner.Identifier];
			}
			
			if (lastAttacker != null && UnityEngine.Random.value > 0.5f)
			{
				//Spawn 'Dead' pop-up
				GameObject popup = Instantiate(GameController.instance.popUpTextGO, deadUnit.transform.position + new Vector3(0, 3, 1), Quaternion.identity) as GameObject;
				
				PackedSprite sprite = popup.GetComponent<PackedSprite>(); 
				
				sprite.PlayAnim(0, (int)ToonPopOutAnim.Dead);
			}
		}
		else if(deadUnit && deadUnit.gameObject.name.Contains("Crystal"))
		{
			if(deadUnit.gameObject.name == "Crystal(Clone)")
			{
				GameController.instance.bottomArcher[deadUnit.Owner.Identifier].timeScale = 0.5f;
				GameController.instance.bottomArcher[deadUnit.Owner.Identifier].state.SetAnimation("broken", false); 		
				sfx.instance.play("towerCollapsed");			
			}		
			else if(deadUnit.gameObject.name == "Crystal3(Clone)")
			{
				GameController.instance.bottomArcher[deadUnit.Owner.Identifier].timeScale = 0.5f;
				GameController.instance.topArcher[deadUnit.Owner.Identifier].state.SetAnimation("broken", false); 	
				sfx.instance.play("towerCollapsed");										
			}
			else if(deadUnit.gameObject.name == "Crystal2(Clone)")
			{
				GameController.instance.door[deadUnit.Owner.Identifier].gameObject.SetActive(false);
				sfx.instance.play("gateCollapsed");			
			}
			deadUnit.gameObject.name = "dead";
			StartCoroutine("CleanSpawn", deadUnit.gameObject);
		}
	}

	private IEnumerator CleanSpawn(System.Object gameObject)
	{
		yield return new WaitForSeconds(RespawnFrequency / 25);
		
		
		if (gameObject != null)
		{
			GameObject.Destroy(gameObject as GameObject);
		}
	}
	
	
	int ii = 0;
	public bool ss = true;
	
	public void SpawnSquad(List<KYAgent.SpawnWrapper> units, Vector3 origin)
	{
		FormationFacing facing = identifier == 0? FormationFacing.Right: FormationFacing.Left;
		
		float spacing = 4f;
		
		//get points
		List<Vector3> spawnPts = FormationHelper.generateFormationCoordinates(units.Count, origin, facing, spacing);
		
		//spawn units with points
		KYAgent agent = transform.parent.gameObject.GetComponent<KYAgent>();
			
		if (agent == null)
		{
			return;
		}
		
		Fury.Behaviors.Commander commander = agent.unit.Owner;
		
		//sort units by their movement speeds
		List<KYAgent.SpawnWrapper> sortedList = units.OrderBy(x => x.unit.MoveRate).ToList();
		
		for (int i = 0; i < sortedList.Count; i++)
		{
			KYAgent.SpawnWrapper spawnWrapper = sortedList[i];
			Fury.Database.Unit spawnType = spawnWrapper.unit;
			
			Fury.Behaviors.IUnit createdSpawn = Fury.Behaviors.Manager.Instance.CreateUnit
				(spawnType, commander, spawnPts[i], null, spawnWrapper.level);
		
			if(spawnType.Prefab.tag == "harvester")
			{
				listHarvesterIdentifier.Add(createdSpawn.Identifier);
			}
			else
			{
				Minions.Add(createdSpawn.Identifier);
			}
			
			if(identifier == 0)
			{
				if(spawnType.name.Contains("Cyclops"))
				{
					sfx.instance.play("cyclopSummon");
				}
				else if(spawnType.name.Contains("Swordman"))
				{
					sfx.instance.play("swordmanSummon");
				}
				else if(spawnType.name.Contains("Priest"))
				{
					sfx.instance.play("priestSummon");
				}
				else if(spawnType.name.Contains("Griffin"))
				{
					sfx.instance.play("gryphynSummon");
				}
				else if(spawnType.name.Contains("Crusader"))
				{
					sfx.instance.play("crusaderSummon");
				}
				else if(spawnType.name.Contains("Archer"))
				{
					sfx.instance.play("archerSummon");
				}
				
				else if (spawnType.name.Contains("Ghoul"))
				{
					sfx.instance.play("ghoulSummon");
				}
				
				else if (spawnType.name.Contains("Medusa"))
				{
					sfx.instance.play("medusaSummon");
				}
				
				else if (spawnType.name.Contains("Necromancer"))
				{
					sfx.instance.play("necroSummon");
				}
				
				else if (spawnType.name.Contains("Paladin"))
				{
					sfx.instance.play("paladinSummon");
				}
				
				else if (spawnType.name.Contains("Elementalist"))
				{
					sfx.instance.play("elementalistSummon");
				}
				
				else if (spawnType.name.Contains("Golem"))
				{
					sfx.instance.play("golemSummon");
				}
				
				else if (spawnType.name.Contains("Skeleton"))
				{
					//TO DO: sfx
					
				}
			}
		}
		
	}
	
	public void CreateSubSpawn(Fury.Database.Unit spawnType, Fury.Behaviors.Unit invoker, Vector3 origin, int level)
	{
		if (!invokedUnitsByOwner.ContainsKey(invoker.Identifier))
		{
			invokedUnitsByOwner[invoker.Identifier] = new List<int>();
		}
		
		KYAgent agent = transform.parent.gameObject.GetComponent<KYAgent>();
			
		if (agent == null)
		{
			return;
		}
		
		Fury.Behaviors.Commander commander = agent.unit.Owner;
		Fury.Behaviors.IUnit createdSpawn = Fury.Behaviors.Manager.Instance.CreateUnit(spawnType, commander, origin, null, level);
		
		Minions.Add(createdSpawn.Identifier);
		invokedUnitsByOwner[invoker.Identifier].Add(createdSpawn.Identifier);
		
		if(identifier == 0)
		{
			if (spawnType.name.Contains("Skeleton"))
			{
				//TO DO: sfx
				
			}
		}
	}
	
	public void CreateSpawn(Fury.Database.Unit spawnType, int count, Vector3 origin, int level, bool randomisePosition = true)
	{	
		/*float x = origin.x;
		if(count > 1 && !spawnType.name.Contains("Skeleton"))
		{
			origin.z -= count/2*2;
		}*/
		for(int i = 0; i < count; ++i)
		{
			Vector3 position = origin;
			
			/*if (randomisePosition)
			{
				origin.z += 2;
				if(origin.z > 25)
				{
					origin.z = 25;
				}
				if(origin.z < -25)
				{
					origin.z = -25;
				}
	
				origin.x = x+UnityEngine.Random.Range(-1, 1);
				
				if(position.x > 0)
				{
					position.x -= 2;
				}
				else
				{
					position.x += 2;
				}
			}*/
			
			KYAgent agent = transform.parent.gameObject.GetComponent<KYAgent>();
			
			if (agent == null)
			{
				return;
			}
			
			Fury.Behaviors.Commander commander = agent.unit.Owner;
			Fury.Behaviors.IUnit createdSpawn = Fury.Behaviors.Manager.Instance.CreateUnit(spawnType, commander, position, null, level);
			
			if(spawnType.Prefab.tag == "harvester")
			{
				listHarvesterIdentifier.Add(createdSpawn.Identifier);
			}
			else
			{
				Minions.Add(createdSpawn.Identifier);
			}

			//++dictionaryUnitCount[spawnType.name];
			
			if(identifier == 0 && ss)
			{
				if(spawnType.name.Contains("Cyclops"))
				{
					sfx.instance.play("cyclopSummon");
				}
				else if(spawnType.name.Contains("Swordman"))
				{
					sfx.instance.play("swordmanSummon");
				}
				else if(spawnType.name.Contains("Priest"))
				{
					sfx.instance.play("priestSummon");
				}
				else if(spawnType.name.Contains("Griffin"))
				{
					sfx.instance.play("gryphynSummon");
				}
				else if(spawnType.name.Contains("Crusader"))
				{
					sfx.instance.play("crusaderSummon");
				}
				else if(spawnType.name.Contains("Archer"))
				{
					sfx.instance.play("archerSummon");
				}
				
				else if (spawnType.name.Contains("Necromancer"))
				{
					//TO DO: sfx
				}
				
				else if (spawnType.name.Contains("Skeleton"))
				{
					//TO DO: sfx
					
				}
			}
		}
	}

	private void OnDrawGizmos()
	{
		Gizmos.color = new Color(0.8f, 0.8f, 0.8f); 
		Gizmos.DrawWireCube(transform.position, new Vector3(Radius * 2, 1, Radius * 2));
	}
	
	bool Between(float value, float left, float right)
	{
	   return value > left && value < right; 
	}
	
	private void removeStatuses(Fury.Behaviors.Unit unit)
	{
		if (unit.agent is WalkableAgent)
		{
			foreach (WalkableAgent.AbilityWrapper abilityWrapper in (unit.agent as WalkableAgent).listAbilityWrapper)
			{
				if (abilityWrapper.abilityType == AbilityType.Aura)
				{
					
				}
			}
		}
	}
}