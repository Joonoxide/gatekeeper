using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public enum AbilityType { Summon, Charge, PassiveAttack, Active, Aura };

public class KYAgent : MonoBehaviour
{	
	public class AbilityWrapper
	{
		public AbilityWrapper() { }

		public AbilityWrapper(Fury.Database.Ability ability)
		{
			Dictionary<string, int> dictionary = GameController.instance.dictionaryAbilityData[ability.name];
			this.ability = ability;
			this.cost = dictionary["GoldCost"];
			this.fixedCooldown = dictionary["Cooldown"];
			this.cooldown = dictionary["CooldownLeft"];
			this.cooldownLeft = dictionary["CooldownLeft"];
			this.abilityType = (AbilityType)dictionary["AbilityType"];

			dictionaryAbilityData = new Dictionary<string, int>();
			string valName = "Val";
			foreach(var kvp in dictionary)
			{
				if(kvp.Key.Contains(valName))
				{
					dictionaryAbilityData.Add(kvp.Key, kvp.Value);
				}
			}
		}
		public Fury.Database.Ability ability;
		public AbilityType abilityType;
		public float cooldown;
		public float cooldownLeft;
		public float cost;
		public float fixedCooldown;
		public int level = 1;
		public Dictionary<string, int> dictionaryAbilityData;
	}
	
	public class SpawnWrapper
	{
		public Fury.Database.Unit unit;
		public int count;
		public void incrementCount() { ++count; }
		public void resetCount() { count = 0; }
		public int level;
		
		public int cost
		{
			get
			{
				return Mathf.RoundToInt(unit.cost * (1 + (0.5f * (int)((level - 1)/10))));
			}
		}
	}

	public class SpawnAbilityWrapper : AbilityWrapper
	{
		public SpawnAbilityWrapper() { }

		public SpawnAbilityWrapper(Fury.Database.Ability ability) : base(ability)
		{
			string path2 = KYHUD.abilityNameForUnitName(ability.name);
			Fury.Database.Unit spawnUnit = Resources.Load (path2) as Fury.Database.Unit;
			SpawnWrapper spawnWrapper = new SpawnWrapper();
			this.spawnWrapper = spawnWrapper;
			this.spawnWrapper.unit = spawnUnit;
			this.spawnWrapper.resetCount();
		}

		public SpawnAbilityWrapper(KYAgent.AbilityWrapper abilityWrapper)
		{
			ability = abilityWrapper.ability;
			cooldown = abilityWrapper.cooldown;
			cooldownLeft = abilityWrapper.cooldownLeft;
			fixedCooldown = abilityWrapper.fixedCooldown;
			cost = abilityWrapper.cost;
		}
		public SpawnWrapper spawnWrapper;
	}

	public class SpriteWrapper
	{
		public int winding;

		public virtual void DoAnim(int index, bool isLoop = true) { }
		public virtual void DoAnim(string animName, bool isLoop = true) { }
		public virtual void setWinding(int winding) { }
		public virtual string GetActiveAnim() {return "null";}
	}

	public class PackedSpriteWrapper : SpriteWrapper
	{
		public PackedSprite sprite;

		public PackedSpriteWrapper(PackedSprite sprite) { this.sprite = sprite; }

		public override void DoAnim(int index, bool isLoop = true) { sprite.DoAnim(index); }

		public override void setWinding(int winding) { sprite.winding = (SpriteRoot.WINDING_ORDER)winding; }
	}

	public class SpineSpriteWrapper : SpriteWrapper
	{
		public SkeletonAnimation sprite;
		static Vector3 v3CW = new Vector3(1, 1, 1);
		static Vector3 v3CCW = new Vector3(-1, 1, 1);
		static String[] arrayAnimationName = {"standby", "move", "attack", "dead", "critical"};

		public SpineSpriteWrapper(SkeletonAnimation sprite) { this.sprite = sprite; }

		public override void DoAnim(int index, bool isLoop = true)
		{ 
			if(sprite.state.Animation == null || sprite.state.Animation.Name != arrayAnimationName[index])
			{
				sprite.state.SetAnimation(arrayAnimationName[index], isLoop); 
			}
		}
		
		public override void DoAnim(string animName, bool isLoop = true)
		{ 
			if(sprite.state.Animation == null || sprite.state.Animation.Name != animName)
			{
				sprite.state.SetAnimation(animName, isLoop); 
			}
		}
		
		public override void setWinding(int winding) 
		{
			sprite.transform.localScale = ((winding == 0)? v3CW : v3CCW); 
			this.winding = winding;
		}
		
		public override string GetActiveAnim()
		{
			if (sprite.state.Animation != null)
			{
				return sprite.state.Animation.Name;
			}
			
			return "null";
		}
	}
	public Fury.Behaviors.Unit unit;
	public bool isBah = false;
	public SpriteWrapper sprite = null;	
	public List<KYAgent> arrayNearbyAllyAgent = new List<KYAgent>();
	public List<KYAgent> arrayNearbyEnemyAgent = new List<KYAgent>();
	public float bah = 0f;
	public Fury.Database.ChangeSource DamageSource;
	public float decisionInterval = 0.1f;
	public float decisionLeft = 1.0f;
	public List<KYAgent.AbilityWrapper> listAbilityWrapper = new List<KYAgent.AbilityWrapper>();
	public Vector3 prevPosition;
	public int stuckCount;
	public int level = 1;
	public bool isBoss;
	public HPBar HPBar;
	
	public virtual void Start()
	{
		if(level == 0)
		{
			level = 1;
		}
		unit = GetComponent<Fury.Behaviors.Unit>();
		PackedSprite packedSprite = GetComponentInChildren<PackedSprite>();
		if(packedSprite != null && packedSprite.enabled)
		{
			sprite  = new PackedSpriteWrapper(packedSprite);
		}
		else
		{
			SkeletonAnimation spineSprite = GetComponentInChildren<SkeletonAnimation>();
			if(spineSprite != null)
			{
				sprite = new SpineSpriteWrapper(spineSprite);
			}
		}
		decisionLeft = decisionInterval;
		
		AbilityWrapper abilityWrapper = null;
		/*
		if(GameController.instance.arrayPlayer[unit.Owner.Identifier].isPlayer)
		{
			foreach(int identifier in dataManager.slotWrapper.arrayTrooperIdentifier)
			{
				if(identifier != 0)
				{
					trooperWrapper = dataManager.identifierForTrooperWrapper(identifier);
					if(trooperWrapper != null)
					{
						foreach(var ability in unit.Properties.Abilities)
						{
							if(ability.name == trooperWrapper.name)
							{
								abilityWrapper = new SpawnAbilityWrapper(ability);
								abilityWrapper.level = trooperWrapper.level;
								listAbilityWrapper.Add (abilityWrapper);
							}
						}
					}
				}
			}
		}
		*/
		foreach(var ability in unit.Properties.Abilities)
		{
			//Debug.Log (unit.name + " " + ability.name);
			Dictionary<string, int> dictionary = GameController.instance.dictionaryAbilityData[ability.name];
			AbilityType abilityType = (AbilityType)dictionary["AbilityType"];
			if(abilityType == AbilityType.Summon)
			{
				if(!GameController.instance.arrayPlayer[unit.Owner.Identifier].isPlayer)
				{
					abilityWrapper = new SpawnAbilityWrapper(ability);
					listAbilityWrapper.Add (abilityWrapper);
				}
			}
			else
			{
				abilityWrapper = new AbilityWrapper(ability);
				listAbilityWrapper.Add (abilityWrapper);
			}
		}

		HPBar = transform.Find("HPBar").gameObject.GetComponent<HPBar>();
	}
	
	// Update is called once per frame
	public virtual void Update () 
	{
		foreach(var abilityWrapper in listAbilityWrapper)
		{
			abilityWrapper.cooldownLeft -= Time.deltaTime;
		}
	}
	
	public AbilityWrapper abilityForAbilityWrapper(Fury.Database.Ability ability)
	{
		foreach(var abilityWrapper in listAbilityWrapper)
		{
			if(abilityWrapper.ability == ability)
			{
				return abilityWrapper;
			}
		}
		return null;
	}

	public void addToNearbyAllyAgent(KYAgent agent)
	{
		if (!arrayNearbyAllyAgent.Contains(agent))
		{
			arrayNearbyAllyAgent.Add (agent);
		}
		
		//arrayNearbyAllyAgent.Remove (agent);
		
		if (!agent.arrayNearbyAllyAgent.Contains(this))
		{
			agent.arrayNearbyAllyAgent.Add (this);
		}
		
		//agent.arrayNearbyAllyAgent.Remove (this);
		
	}
	
	public void removeFromNearbyAllyAgent(KYAgent agent)
	{
		arrayNearbyAllyAgent.Remove (agent);
		
		agent.arrayNearbyAllyAgent.Remove (agent);
	}
	
	public void addToNearbyEnemyAgent(KYAgent agent)
	{
		arrayNearbyEnemyAgent.Remove (agent);
		arrayNearbyEnemyAgent.Add (agent);
		
		agent.arrayNearbyEnemyAgent.Remove (this);
		agent.arrayNearbyEnemyAgent.Add (this);
	}
	
	public void removeFromNearbyEnemyAgent(KYAgent agent)
	{
		arrayNearbyEnemyAgent.Remove (agent);
	
		agent.arrayNearbyEnemyAgent.Remove (this);
	}

	public bool isWeaponAttacking()
	{
		if (unit.Controllers.WeaponController.State == Fury.Controllers.WeaponController.States.PreDelay)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public AbilityWrapper abilityNameForAbilityWrapper(string abilityName)
	{
		foreach(AbilityWrapper abilityWrapper in listAbilityWrapper)
		{
			if(abilityWrapper.ability.Name.Contains(abilityName))
			{
				return abilityWrapper;
			}
		}
		return null;
	}

	public bool isWeaponCD()
	{
		if(unit.Controllers.MovementController.isMoving())
		{
			return false;
		}
		else
		{
			return true;
		}
	}
}


