﻿using UnityEngine;
using System.Collections;

public class expbar1 : MonoBehaviour {

	public float curr;
	public float target;
	public int cl;
	public int tl;
	float speed = 0.5f;
	public System.Action onFinish;
	public System.Action<int> onLevelup;
	public System.Action<float, int> update;
	void Start () {
	
	}
	
 	bool isStart = false;
	void Update () 
	{
		if(isStart)
		{
			gameObject.renderer.material.SetFloat("_Cutoff", Mathf.Clamp(1-curr, 0.02f, 1f));
			if(curr < target || cl < tl) 
			{
				curr += speed*Time.deltaTime;
				if(curr >= 1 && (target < 1 || cl < tl))
				{
					curr = 0;
					++cl;
					onLevelup(cl);
				}
				else
				{
					update(curr, cl);
				}
			}
			else
			{
				onFinish();
				curr = target;
				isStart = false;
			}
		}
	}

	public void start()
	{
		isStart = true;
	}
}
