﻿using UnityEngine;
using System.Collections;

public class SuicidalAgent : WalkableAgent {
	
	public enum SuiciderState
	{
		Moving,
		Blocked,
		TimedOut,
		Dead
	}
	
	public SuiciderState suiciderState = SuiciderState.Moving;
	
	private TowerAgent2 targetTower;
	
	// Use this for initialization
	public override void Start () {
		base.Start ();
		
		targetTower = TowerAgent2.getInstanceForIndex((unit.Owner.Identifier == 0)? 1 : 0);
	}
	
	// Update is called once per frame
	public override void Update () {
		
		if (unit && GameController.instance.isGameOver)
		{
			//stand still
			unit.Order(unit.transform.position);
			sprite.DoAnim("standby", true);
			return;
		}
		
		if (unit && GameController.instance.isEnd != 10)
		{
			state = unit.State;
			
			if(!isBah)
			{
				unit.Controllers.VitalityController.Health = unit.Controllers.VitalityController.MaxHealth;
				isBah = true;
			}
			
            if(unit.Controllers.VitalityController.HealthPercentage < 1 && unit.Controllers.VitalityController.HealthPercentage > 0 && HPBar)
			{
				HPBar.gameObject.SetActive(true);
				var vitCtrl = unit.Controllers.VitalityController;
				float currentHP = vitCtrl.Health;
				float maxHP = vitCtrl.MaxHealth;
				HPBar.percent = currentHP/maxHP;
			}
			
			if(unit.State == Fury.UnitStates.Idle)
			{
				if(isMoving() && !unit.Controllers.StatusController.IsStunned)
				{
					sprite.DoAnim(1);
				}
				else
				{
					sprite.DoAnim(0);
				}
			}
			else if(unit.State == Fury.UnitStates.MovingToPosition || unit.State == Fury.UnitStates.MovingToTarget)
			{
				if(isMoving())
				{
					sprite.DoAnim (1);		
				}
				else
				{
					sprite.DoAnim(0);
				}
			}
			else if(unit.State == Fury.UnitStates.AttackingUnit || unit.State == Fury.UnitStates.FakeAttacking)
			{
				if(isMoving())
				{
					sprite.DoAnim(1);
				}
				else
				{
					sprite.DoAnim (0);
				}
			}

			if(!isSpacing && !(unit.State == Fury.UnitStates.CastingAbility))
			{
				if((unit.Controllers.MovementController.Agent as Fury.UnityNavMeshAgent).Agent.hasPath)
				{
					float targetX = (unit.Controllers.MovementController.Agent as Fury.UnityNavMeshAgent).Agent.steeringTarget.x;
					if(Mathf.Abs(unit.transform.position.x-targetX) > 0.1f)
					{
						if (unit.transform.position.x > targetX)
							{sprite.setWinding((int)SpriteRoot.WINDING_ORDER.CW);}
						else
							{sprite.setWinding((int)SpriteRoot.WINDING_ORDER.CCW);}			
					}		
					else
					{
						if(unit.Owner.Identifier == 0)
						{
							sprite.setWinding((int)SpriteRoot.WINDING_ORDER.CW);			
						}
						else
						{
							sprite.setWinding((int)SpriteRoot.WINDING_ORDER.CCW);					
						}						
					}
				}
				else if(unit.Controllers.WeaponController.Target != null)
				{
					float targetX = unit.Controllers.WeaponController.Target.transform.position.x;
					if(Mathf.Abs(unit.transform.position.x-targetX) > 0.1f)
					{
						if (unit.transform.position.x > targetX)
							{sprite.setWinding((int)SpriteRoot.WINDING_ORDER.CW);}
						else
							{sprite.setWinding((int)SpriteRoot.WINDING_ORDER.CCW);}			
					}	
					else
					{
						if(unit.Owner.Identifier == 0)
						{
							sprite.setWinding((int)SpriteRoot.WINDING_ORDER.CW);			
						}
						else
						{
							sprite.setWinding((int)SpriteRoot.WINDING_ORDER.CCW);					
						}						
					}	
				}
			}
		 	
		 	target = (unit.Controllers.MovementController.Agent as Fury.UnityNavMeshAgent).Agent.steeringTarget;

			if(unit.State == Fury.UnitStates.CastingAbility || unit.State == Fury.UnitStates.FakeAttacking)
			{
				
			}
			else if(isSpacing)
			{
				if(unit.State == Fury.UnitStates.Idle)
				{
					isSpacing = false;
				}
			}
			else if(unit.State == Fury.UnitStates.Idle && arrayNearbyEnemyAgent.Count == 0)
			{
				if(((unit.transform.position-movementTarget).magnitude) > 15)
				{
					unit.Order (movementTarget);
				}
				else
				{
					TowerAgent2 agentTarget = TowerAgent2.getInstanceForIndex((unit.Owner.Identifier == 0)? 1 : 0);
					if(TowerAgent2.instance2 != null)
					{
						unit.Order(agentTarget.unit);
					}
				}
			}
			else if((unit.State != Fury.UnitStates.AttackingUnit && unit.State != Fury.UnitStates.MovingToTarget && unit.State != Fury.UnitStates.FakeAttacking && !isSpacing))
			{
				float minDist = 99999, dist;
				Fury.Behaviors.Unit nearestUnit = null;
				foreach(KYAgent agent in arrayNearbyEnemyAgent)
				{
					WalkableAgent walkableAgent = agent as WalkableAgent;
					dist = (transform.position-agent.transform.position).magnitude;

					if(walkableAgent != null) //not tower.
					{
						if(dist < minDist)
						{
							nearestUnit = agent.unit;
							minDist = dist;
						}
					}
					else
					{
						if(nearestUnit == null) //tower.
						{
							nearestUnit = agent.unit;
						}
					}
				}

				if(nearestUnit != null && unit.Controllers.WeaponController.Target != nearestUnit)
				{
					unit.Order(nearestUnit);
				}
				else
				{
					if(((unit.transform.position-movementTarget).magnitude) < 15)
					{
						TowerAgent2 agentTarget = TowerAgent2.getInstanceForIndex((unit.Owner.Identifier == 0)? 1 : 0);
						if(TowerAgent2.instance2 != null)
						{
							unit.Order(agentTarget.unit);
						}
					}					
				}
			}
			else if(!(isWeaponAttacking() || isWeaponCD()) && unit.Controllers.WeaponController.Target.gameObject.tag != "harvester" && unit.Controllers.WeaponController.Target.gameObject.tag != "tower")
			{
				float minDist = 99999, dist;
				Fury.Behaviors.Unit nearestUnit = null;
				foreach(KYAgent agent in arrayNearbyEnemyAgent)
				{
					WalkableAgent walkableAgent = agent as WalkableAgent;
					dist = (transform.position-agent.transform.position).magnitude;
					
					if(walkableAgent != null && walkableAgent.gameObject.tag != "harvester") //not tower.
					{
						if(dist < minDist)
						{
							nearestUnit = agent.unit;
							minDist = dist;
						}
					}
				}
				if(nearestUnit != null && unit.Controllers.WeaponController.Target != nearestUnit)
				{
					unit.Order(nearestUnit);
				}
			}
			else if(unit.Controllers.WeaponController.Target.gameObject.tag == "harvester")
			{
				float minDist = 99999, dist;
				Fury.Behaviors.Unit nearestUnit = null;
				foreach(KYAgent agent in arrayNearbyEnemyAgent)
				{
					WalkableAgent walkableAgent = agent as WalkableAgent;
					dist = (transform.position-agent.transform.position).magnitude;
					
					if(walkableAgent != null && walkableAgent.gameObject.tag != "harvester") //not tower.
					{
						if(dist < minDist)
						{
							nearestUnit = agent.unit;
							minDist = dist;
						}
					}
				}
				if(nearestUnit != null && unit.Controllers.WeaponController.Target)
				{
					unit.Order(nearestUnit);
				}
			}
			else if(unit.Controllers.WeaponController.Target.gameObject.tag == "tower")
			{
				float minDist = 99999, dist;
				Fury.Behaviors.Unit nearestUnit = null;
				foreach(KYAgent agent in arrayNearbyEnemyAgent)
				{
					WalkableAgent walkableAgent = agent as WalkableAgent;
					dist = (transform.position-agent.transform.position).magnitude;
					if(walkableAgent != null) //not tower.
					{
						if(dist < minDist)
						{
							nearestUnit = agent.unit;
							minDist = dist;
						}
					}
				}
				if(nearestUnit != null && unit.Controllers.WeaponController.Target)
				{
					unit.Order(nearestUnit);
				}
			}

			stat = unit.Controllers.WeaponController.State;
		}
		if(unit.State == Fury.UnitStates.MovingToPosition || unit.State == Fury.UnitStates.MovingToTarget)
		{
			if((prevPosition-transform.position).magnitude < 0.001f)
			{
				++stuckCount;
				if(stuckCount > 10)
				{
					//stuckCount = 0;
					
					if (unit.name.Contains("Skeleton"))
					{
						(unit.Controllers.MovementController.Agent as Fury.UnityNavMeshAgent).Agent.Warp(transform.position + new Vector3(0, 0, UnityEngine.Random.Range(0.5f, 2)));
					}
					
					else
					{
						unit.Order(transform.position);
					}
				}
			}
			else
			{
				stuckCount = 0;
			}
		}
		prevPosition = transform.position;
		
		intervalSkill -= Time.deltaTime;
		
		//handle ability activations (passive and active)
		foreach(AbilityWrapper abilityWrapper in listAbilityWrapper)
		{
			/*if (abilityWrapper.abilityType == AbilityType.Aura)
			{
				(abilityWrapper.ability as AISkillInterface).aiExecute(this);
			}
			
			else */if(intervalSkill <= 0 && abilityWrapper.cooldownLeft <= 0)
			{
				KYAgent target = null;
				if(unit.Controllers.WeaponController.Target)
				{
					target = unit.Controllers.WeaponController.Target.gameObject.GetComponent<KYAgent>();
				}
				
				if (abilityWrapper.abilityType == AbilityType.Active)
				{
					int rand = UnityEngine.Random.Range(0, 3);
					
					if(rand == 0)
					{
						
						if(target)
						{
							(abilityWrapper.ability as AISkillInterface).aiExecute(this, target);
						}
						else
						{
							(abilityWrapper.ability as AISkillInterface).aiExecute(this);
						}
					}
					else if(rand != 0)
					{
						abilityWrapper.cooldownLeft = abilityWrapper.cooldown;
					}
					
					intervalSkill = UnityEngine.Random.Range(0.5f, 3f);
					continue;
				}
			}
			
		}

		if(GameController.instance.isEnd == 10 && unit)
		{
			if((unit.Controllers.MovementController.Agent as Fury.UnityNavMeshAgent).Agent.hasPath)
			{
				float targetX = (unit.Controllers.MovementController.Agent as Fury.UnityNavMeshAgent).Agent.steeringTarget.x;
				if(Mathf.Abs(unit.transform.position.x-targetX) > 0.1f)
				{
					if (unit.transform.position.x > targetX)
						{sprite.setWinding((int)SpriteRoot.WINDING_ORDER.CW);}
					else
						{sprite.setWinding((int)SpriteRoot.WINDING_ORDER.CCW);}			
				}		
			}
			else if(unit.Controllers.WeaponController.Target != null)
			{
				float targetX = unit.Controllers.WeaponController.Target.transform.position.x;
				if(Mathf.Abs(unit.transform.position.x-targetX) > 0.1f)
				{
					if (unit.transform.position.x > targetX)
						{sprite.setWinding((int)SpriteRoot.WINDING_ORDER.CW);}
					else
						{sprite.setWinding((int)SpriteRoot.WINDING_ORDER.CCW);}			
				}		
			}
			if(unit.Owner.Identifier == 1 && TowerAgent2.instance1 == null)
			{
				unit.Order(new Vector3(65, 0, 0));
			}
			else if(unit.Owner.Identifier == 0 && TowerAgent2.instance2 == null)
			{
				unit.Order(new Vector3(-75, 0, 0));				
			}
			else
			{
				unit.gameObject.SetActive(false);
			}
			sprite.DoAnim (1);		

		}

		if(!unit && GameController.instance.isEnd != 10)
		{
			HPBar.gameObject.SetActive(false);
			
			if (suiciderState == SuiciderState.TimedOut)
			{
				sprite.DoAnim("explosion", false);
			}
			
			else
			{
				sprite.DoAnim("dead", false);
			}
			
			
			SkeletonAnimation skeletonAnimation = (sprite as SpineSpriteWrapper).sprite;
			if(skeletonAnimation.state.Time/skeletonAnimation.state.Animation.Duration >= 0.9)
			{
				skeletonAnimation.transform.parent = null;
				skeletonAnimation.gameObject.AddComponent<Fadeout>();
				Destroy(gameObject);
			}
		}
		
		if(!unit && GameController.instance.isEnd == 10)
		{
			Destroy(gameObject);
		}	
	
	}
}
