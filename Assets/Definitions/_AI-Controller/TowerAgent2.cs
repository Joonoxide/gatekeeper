using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class TowerAgent2 : KYTowerAgent 
{	
	public MinionController minionController;
	public static TowerAgent2 instance1;
	public static TowerAgent2 instance2;
	public float animTime;
	public bool isAnimateHit;
	
	private Dictionary<string, List<KYAgent>> statusCarrierDict = new Dictionary<string, List<KYAgent>>();
	private float passiveAOERange = 10f, passiveAOEUpCloseRange = 2f;

	// Use this for initialization
	public override void Start () 
	{
		base.Start ();
		
		if(unit.Owner.Identifier == 0)
		{
			
			instance1 = this;
		}
		else
		{
			instance2 = this;
		}
		minionController = transform.Find ("SummonZone").GetComponent<MinionController>();
		minionController.identifier = unit.Owner.Identifier;
		
		unit.AddStatus(GameController.instance.towerLevelBuff, unit);
		WaveManager.instance.loadSpawnWrapper(unit.Owner.Identifier);
		HPBar.transform.position = GameController.instance.door[unit.Owner.Identifier].transform.position+new Vector3(0, 8, 0);
	}
	
	public static TowerAgent2 getInstanceForIndex(int index)
	{
		if(index == 0)
		{
			return instance1;
		}
		else
		{
			return instance2;
		}
	}

	public void doHitAnim()
	{
		if(!isAnimateHit)
		{
			animTime = 0.01f;
			isAnimateHit = true;
		}
	}

	public void animateHit()
	{
		if(isAnimateHit)
		{
			animTime -= Time.deltaTime;
			UIStateToggleBtn door = GameController.instance.door[unit.Owner.Identifier];
			if(animTime < 0)
			{
				if(door.StateNum == 1)
				{
					door.SetToggleState(3);
					animTime = 0.2f;
				}
				else if(door.StateNum == 3)
				{
					door.SetToggleState(1);
					isAnimateHit = false;
				}
			}
		}
	}

	public override void Update () 
	{
		base.Update();
		animateHit();
		if (unit != null && unit.Controllers.VitalityController.Health > 0)
		{
			if(!isBah)
			{
				unit.Controllers.VitalityController.Health = unit.Controllers.VitalityController.MaxHealth;
				isBah = true;
			}
			
			if(HPBar)
			{
				var vitCtrl = unit.Controllers.VitalityController;
				float currentHP = vitCtrl.Health;
				float maxHP = vitCtrl.MaxHealth;
				HPBar.percent = currentHP/maxHP;
			}
			
			handleUnitPassiveSkills("Paladin", false, false);
			handleUnitPassiveSkills("Medusa", true);
			handleUnitPassiveSkills("Ghoul", false, true);
			handleUnitPassiveSkills("Crusader", false, true);

			/*
			if(unit.State != Fury.UnitStates.AttackingUnit)
			{
				float minDist = 99999, dist;
				Fury.Behaviors.Unit nearestUnit = null;
				foreach(KYAgent agent in arrayNearbyEnemyAgent)
				{
					WalkableAgent walkableAgent = agent as WalkableAgent;
					dist = (transform.position-agent.transform.position).magnitude;

					if(walkableAgent != null) //not tower.
					{
						if(dist < minDist)
						{
							nearestUnit = agent.unit;
							minDist = dist;
						}
					}
				}

				if(nearestUnit != null && unit.Controllers.WeaponController.Target != nearestUnit)
				{
					unit.Order(nearestUnit);
				}
			}
			else if((unit.Controllers.WeaponController.Target.transform.position-transform.position).magnitude > 25)
			{
				unit.Order(transform.position);
			}
			*/

		}

		if(instance1 == null && GameController.instance.isEnd != 10)
		{
			GameController.instance.isEnd = 2;
		}
		else if(instance2 == null && GameController.instance.isEnd != 10)
		{
			GameController.instance.isEnd = 1;;
		}
	}
	
	void handleUnitPassiveSkills(string unitType, bool isDebuff = false, bool isSelfOnly = false)
	{
		Dictionary<string, List<KYAgent>> dictUnit = minionController.dictionaryUnit;
		CustomGradedStatus status;
		
		//get status by unit type
		status = getStatusByType(unitType);
		
		if (!statusCarrierDict.ContainsKey(status.name))
		{
			statusCarrierDict[status.name] = new List<KYAgent>();
		}
		
		if (isSelfOnly)
		{
			foreach (KYAgent self in dictUnit[unitType])
			{
				if (!(self.unit))
				{
					continue;
				}
				
				if (!statusCarrierDict[status.name].Contains(self))
				{
					self.unit.AddStatus(status, this.unit);
					statusCarrierDict[status.name].Add(self);
				}
			}
			
			return;
		}
		
		List <KYAgent> nearbyWalkableAgents;
		nearbyWalkableAgents = collectAllNearbyWalkableAgents(dictUnit[unitType], isDebuff);
		
		//if there are caster(s) on the field, apply status to unbuffed units
		if (nearbyWalkableAgents.Count > 0)
		{
			if (unitType.Equals("Medusa"))
			{
				Debug.Log ("has units...");
			}
			
			foreach(KYAgent ally in nearbyWalkableAgents)
			{
				if (!(ally.unit))
				{
					continue;
				}
				
				bool withinRange = isWithinRangeMultiple(ally, dictUnit[unitType]/*.ConvertAll<Vector3>(x => x.transform.position)*/, passiveAOERange);
				
				//if it is a status carrier, check if it has walked out of range
				if (statusCarrierDict[status.name].Contains(ally))
				{
					//if the unit has walked out of range
					if (!withinRange)
					{
						ally.unit.RemoveStatus(status);
						statusCarrierDict[status.name].Remove(ally);
					}
				}
				
				else
				{
					if (withinRange)
					{
						ally.unit.AddStatus(status, dictUnit[unitType][0].unit);
						statusCarrierDict[status.name].Add(ally);
					}
				}
			}
		}
		
		//if there are no caster(s) on the field
		else
		{
			List <KYAgent> carriers = new List<KYAgent>(statusCarrierDict[status.name]);
			
			foreach(KYAgent carrier in carriers)
			{
				carrier.unit.RemoveStatus(status);
				statusCarrierDict[status.name].Remove(carrier);
			}
		}
		
	}
	
	List<KYAgent> collectAllNearbyWalkableAgents(List<KYAgent> sources, bool isDebuff)
	{
		List<KYAgent> result = new List<KYAgent>();
		
		foreach(KYAgent agent in sources)
		{
			foreach(KYAgent target in isDebuff? agent.arrayNearbyEnemyAgent: agent.arrayNearbyAllyAgent)
			{
				if (!result.Contains(target) && target is WalkableAgent)
				{
					result.Add(target);
				}
			}
		}
		
		return result;
	}
			
	CustomGradedStatus getStatusByType(string unitType)
	{
		CustomGradedStatus status = null;
				
		if (unitType.Equals("Paladin"))
		{
			status = GameController.instance.defenseBuff_paladin;	
		}
		
		else if (unitType.Equals("Medusa"))
		{
			status = GameController.instance.slowDebuff_medusa;
		}
		
		else if (unitType.Equals("Ghoul"))
		{
			status = GameController.instance.lifeStealBuff_ghoul;
		}
		
		else if (unitType.Equals("Golem"))
		{
			status = GameController.instance.kamikazeBuff_bomb;
		}
		
		else if (unitType.Equals("Crusader"))
		{
			status = GameController.instance.blockBuff_Crusader;
		}
				
		return status;
	}
	
	CustomGradedStatus getSecondaryStatusByType(string unitType)
	{
		CustomGradedStatus status = null;
				
		if (unitType.Equals("Medusa"))
		{
			status = GameController.instance.stoneDebuff_medusa;
		}
				
		return status;
	}
			
	CustomGradedStatus getExistingStatus(CustomGradedStatus status, Fury.Behaviors.Unit unit)
	{
		foreach (Fury.Controllers.StatusController.StatusInfo info in unit.Controllers.StatusController.Statuses)
		{
			if (info.Properties == status)
			{
				return status as CustomGradedStatus;
			}
		}
				
		return null;
	}
	
	bool isWithinRangeMultiple(Vector3 target, List<Vector3> refPoints, float range)
	{
		float dist;
		
		foreach(Vector3 pt in refPoints)
		{
			dist = (target - pt).magnitude;
			
			if (dist <= range)
			{
				return true;
			}
		}
		
		return false;
	}
	
	bool isWithinRangeMultiple(KYAgent target, List<KYAgent> refPoints, float range)
	{
		float dist;
		
		foreach (KYAgent source in refPoints)
		{
			dist = (target.transform.position - source.transform.position).magnitude;
			
			if (dist <= range && target != source)
			{
				return true;
			}
		}
		
		return false;
	}
			
	bool isWithinRangeSingular(KYAgent target, KYAgent refPoint, float range)
	{
		return (target.transform.position - refPoint.transform.position).magnitude <= range;
	}
}
