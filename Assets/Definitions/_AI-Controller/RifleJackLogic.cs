using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class RifleJackLogic : MonoBehaviour {
	
	private Transform turret;
	
	public Single AggroDistance = 3;
	public Single ChaseDistance = 10;
	
	private Fury.Behaviors.Unit aggressor;
	
	private Fury.Behaviors.Unit minion;
	
	private PackedSprite sprite;
	
	void Start()
	{
		minion = GetComponent<Fury.Behaviors.Unit>();
		
		sprite  = gameObject.GetComponentInChildren(typeof(PackedSprite)) as PackedSprite;
		
		turret = minion.transform.FindChild("-turret");
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (minion != null && minion.Controllers.VitalityController.Health > 0)
		{
			if (minion.State == Fury.UnitStates.Idle)
			{
				sprite.PlayAnim(0);
			}
			else if (minion.State == Fury.UnitStates.MovingToPosition)
			{
				sprite.DoAnim(1);
				if (minion.Controllers.MovementController.TargetPosition.HasValue)
				{
					if (minion.transform.position.x > minion.Controllers.MovementController.TargetPosition.Value.x)
					{
						sprite.winding = SpriteRoot.WINDING_ORDER.CW;
						turret.transform.position = new Vector3( -2, 1, 2);
					}
					else
					{
						sprite.winding = SpriteRoot.WINDING_ORDER.CCW;
						turret.transform.position = new Vector3( 2, 1, 2);
					}
				}
			}
			else if (minion.State == Fury.UnitStates.MovingToTarget)
			{
				sprite.DoAnim(1);
				if (minion.Controllers.MovementController.TargetUnit)
				{
					if (minion.transform.position.x > minion.Controllers.MovementController.TargetUnit.transform.position.x)
					{
						sprite.winding = SpriteRoot.WINDING_ORDER.CW;
						turret.transform.position = new Vector3( -2, 1, 2);
					}
					else
					{
						sprite.winding = SpriteRoot.WINDING_ORDER.CCW;
						turret.transform.position = new Vector3( 2, 1, 2);
					}
				}
			}
			else if (minion.State == Fury.UnitStates.AttackingUnit)
			{
				if (minion.Controllers.WeaponController.State == Fury.Controllers.WeaponController.States.PreDelay)
				{
					sprite.PlayAnim(2);
				}
				if(minion.Controllers.WeaponController.Target)
				{
					if (minion.transform.position.x > minion.Controllers.WeaponController.Target.transform.position.x)
					{
						sprite.winding = SpriteRoot.WINDING_ORDER.CW;
						turret.transform.position = minion.transform.position + new Vector3( -2, 1, 2);
					}
					else
					{
						sprite.winding = SpriteRoot.WINDING_ORDER.CCW;
						turret.transform.position = minion.transform.position + new Vector3( 2, 1, 2);
					}
				}
			}
		}
		else 
			sprite.DoAnim(3);
	}
}
