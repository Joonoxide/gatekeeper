using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using KYData;

using UnityEngine;

public enum RingType
{
	Ally = 0,
	Enemy
}

public enum ToonPopOutAnim
{
	Critical = 0,
	Block,
	Slash,
	Poof,
	Wrrr,
	Swing,
	Dead
}

public enum Allegiance
{
	Player = 0,
	Enemy
}

public class GameController : MonoBehaviour 
{

	public static float timeScale = 2;
	
	public UIPanel WinPanel, LosePanel;
		
	public Dictionary<string, Dictionary<string, int>> dictionaryAbilityData = new Dictionary<string, Dictionary<string, int>>();
	public GameObject textGO;
	public GameObject popUpTextGO;
	public GameObject labelGO;
	public SpriteText textGold1;
	public SpriteText textGold2;
	public Player[] arrayPlayer = new Player[2];
	public Fury.Database.Unit unitResourceA;
	public Fury.Database.Unit unitResourceB;
	//public Fury.Database.Unit unitss;
	//public Fury.Database.Unit units;
	//public Fury.Database.Unit unitssss;
	//public Fury.Database.Unit unitsssss;
	//public Fury.Database.Unit unitssssss;
	//public Fury.Database.Unit unitsssssss;
	public Fury.Database.Ability source;
	public static GameController instance;
	int _isEnd;
	public int isEnd
	{
		get
		{
			return _isEnd;
		}
		set
		{
			_isEnd = value;
		}
	}
	public Fury.Database.Unit unitsss;
	public GameObject stunGO;
	public GameObject arrowGO;
	public GameObject fireballGO;
	public GameObject soulballGO;
	public GameObject explosionGO;
	public GameObject goodTextGO;
	public GameObject statusIndicatorGO;
	public Fury.Database.Status stunBuff;
	public Fury.Database.Status shieldBuff;
	public Fury.Database.Status levelBuff;
	public Fury.Database.Status towerLevelBuff;
	public CustomGradedStatus stunDebuff_cyclops; //cyclops
	public CustomGradedStatus defenseBuff_paladin; //paladin
	public CustomGradedStatus slowDebuff_medusa; //medusa
	public CustomGradedStatus stoneDebuff_medusa; //medusa
	public CustomGradedStatus lifeStealBuff_ghoul; //ghoul
	public CustomGradedStatus kamikazeBuff_bomb; //bomb
	public CustomGradedStatus blockBuff_Crusader; //crusader
	public GameObject ringPrefab, solidRingPrefab;
	public GameObject bombPrefab;
	public GameObject frozenPrefab;
	public GameObject healPrefab;
	public List<Fury.Database.Unit> arrayUnit;
	public UIStateToggleBtn[] bottomTower;
	public UIStateToggleBtn[] topTower;
	public UIStateToggleBtn[] door;
	public SkeletonAnimation[] bottomArcher;
	public SkeletonAnimation[] topArcher;
	public GameObject dummy;
	public GameObject cameraOverlay;
	public GameObject arrowShower;
	public Fury.Database.Status resourceBuff;
	public UIStateToggleBtn btnBGM;
	public UIStateToggleBtn btnSFX;
	public static int star = 1;
	public GameObject pauseGO, textGoldGO;
	//public AtlasAsset[] atlass;
	public Shader coloredShader;
	
	public GameObject winEmblemGO, loseEmblemGO;
	public GameObject btn_clickAnywhere;
	
	public GameObject resultDataGO;
	
	private SkeletonAnimation emblemSkeleton;
	private GameObject clickAnywhereScreen;
	
	private Camera GUICam;
	private GameObject speeder;
	
	public bool isGameOver = false, hasBattleBegun = false, isPaused = false;

	private void Awake()
	{
		coloredShader = Shader.Find("Sprite/Vertex Colored");
		Screen.sleepTimeout = SleepTimeout.NeverSleep;

		if(LevelManager.instance == null)
		{
			string levelName = Application.loadedLevelName;
			int l = Int32.Parse(levelName.Substring(5, levelName.Length-5));
			//int l = Int32.Parse((levelName.Substring(levelName.Length - 1, 1)));
			//LevelManager.createInstance((l == 21)? l : l);
			LevelManager.createInstance(l);
		}
		
		levelBuff = Resources.Load("LevelBuff") as Fury.Database.Status;
		resourceBuff = Resources.Load("ResourceBuff") as Fury.Database.Status;
		towerLevelBuff = Resources.Load("TowerLevelBuff") as Fury.Database.Status;
		
		stunDebuff_cyclops = Resources.Load ("StunDebuff") as CustomGradedStatus;
		defenseBuff_paladin = Resources.Load ("DefenseBuff") as CustomGradedStatus;
		slowDebuff_medusa = Resources.Load ("SlowDebuff") as CustomGradedStatus;
		stoneDebuff_medusa = Resources.Load ("StoneDebuff") as CustomGradedStatus;
		lifeStealBuff_ghoul = Resources.Load ("LifeStealBuff") as CustomGradedStatus;
		kamikazeBuff_bomb = Resources.Load ("KamikazeBuff") as CustomGradedStatus;
		blockBuff_Crusader = Resources.Load("BlockBuff") as CustomGradedStatus;
		isEnd = 0;
		instance = this;
		unitResourceA = Resources.Load("ResourceA") as Fury.Database.Unit;
		unitResourceB = Resources.Load("ResourceB") as Fury.Database.Unit;
		//unitssss = Resources.Load("Griffin") as Fury.Database.Unit;
		//unitsss = Resources.Load("Archer") as Fury.Database.Unit;
		//unitss = Resources.Load("Crusader") as Fury.Database.Unit;
		//units = Resources.Load("Harvester") as Fury.Database.Unit;
		//unitsssss = Resources.Load("Swordman") as Fury.Database.Unit;
		//unitsssssss = Resources.Load("Priest") as Fury.Database.Unit;
		//unitssssss = Resources.Load("Cyclops") as Fury.Database.Unit;
		bombPrefab = Resources.Load("ammo") as GameObject;
		healPrefab = Resources.Load("heal") as GameObject;
		//arrowGO = Resources.Load("-arrow") as GameObject;
		arrowGO = Resources.Load("-arrowSprite") as GameObject;
		fireballGO = Resources.Load("-fireball") as GameObject;
		soulballGO = Resources.Load("-soulball") as GameObject;
		explosionGO = Resources.Load("-explosion") as GameObject;
		arrowShower = Resources.Load("arrowshower") as GameObject;
		statusIndicatorGO = Resources.Load ("StatusIndicator") as GameObject;
		solidRingPrefab = Resources.Load("-solidRing") as GameObject;
		//frozenPrefab = Resources.Load("-frost") as GameObject;
		
		winEmblemGO = Resources.Load ("WinEmblem") as GameObject;
		loseEmblemGO = Resources.Load ("LoseEmblem") as GameObject;
		btn_clickAnywhere = Resources.Load ("btn_clickAnywhere") as GameObject;
		
		resultDataGO = Resources.Load ("ResultData") as GameObject;

		GUICam = GameObject.Find("GUI camera").GetComponent<Camera>();
		
		#region Unit population
		
		arrayUnit = new List<Fury.Database.Unit>();
		Fury.Database.Unit unit;
		unit = Resources.Load("ResourceA") as Fury.Database.Unit;
		arrayUnit.Add(unit);
		unit = Resources.Load("ResourceB") as Fury.Database.Unit;
		arrayUnit.Add(unit);
		unit = Resources.Load("Griffin") as Fury.Database.Unit;
		arrayUnit.Add(unit);
		unit = Resources.Load("Archer") as Fury.Database.Unit;
		arrayUnit.Add(unit);
		unit = Resources.Load("Crusader") as Fury.Database.Unit;
		arrayUnit.Add(unit);
		unit = Resources.Load("Harvester") as Fury.Database.Unit;
		arrayUnit.Add(unit);
		unit = Resources.Load("Swordman") as Fury.Database.Unit;
		arrayUnit.Add(unit);
		unit = Resources.Load("Priest") as Fury.Database.Unit;
		arrayUnit.Add(unit);
		unit = Resources.Load("Cyclops") as Fury.Database.Unit;
		arrayUnit.Add(unit);
		unit = Resources.Load ("Necromancer") as Fury.Database.Unit;
		arrayUnit.Add(unit);
		unit = Resources.Load ("Skeleton") as Fury.Database.Unit;
		arrayUnit.Add(unit);
		unit = Resources.Load ("Paladin") as Fury.Database.Unit;
		arrayUnit.Add(unit);
		unit = Resources.Load ("Medusa") as Fury.Database.Unit;
		arrayUnit.Add(unit);
		unit = Resources.Load ("Ghoul") as Fury.Database.Unit;
		arrayUnit.Add(unit);
		unit = Resources.Load ("Golem") as Fury.Database.Unit;
		arrayUnit.Add(unit);
		unit = Resources.Load ("Elementalist") as Fury.Database.Unit;
		arrayUnit.Add(unit);
		
		#endregion
		
		#region Dictionary population
		Dictionary<string, int> dictionaryData;
		//ability
		/*
		ability types:
			0 : summon;
			1 : charge;
			2 : passiveAttack;
			3 : active;
			4 : aura;
		*/
		dictionaryData = new Dictionary<string, int>();
		dictionaryData.Add ("GoldCost", 5);
		dictionaryData.Add ("Cooldown", 3);
		dictionaryData.Add ("AbilityType", 0);
		dictionaryData.Add ("CooldownLeft", 0);
		dictionaryAbilityData.Add ("Call_Swordman", dictionaryData);
		
		dictionaryData = new Dictionary<string, int>();
		dictionaryData.Add ("GoldCost", 5);
		dictionaryData.Add ("Cooldown", 3);
		dictionaryData.Add ("AbilityType", 0);
		dictionaryData.Add ("CooldownLeft", 0);
		dictionaryAbilityData.Add ("Call_RifleJack", dictionaryData);
		
		dictionaryData = new Dictionary<string, int>();
		dictionaryData.Add ("GoldCost", 5);
		dictionaryData.Add ("Cooldown", 3);
		dictionaryData.Add ("AbilityType", 0);
		dictionaryData.Add ("CooldownLeft", 0);
		dictionaryAbilityData.Add ("Call_Archer", dictionaryData);

		dictionaryData = new Dictionary<string, int>();
		dictionaryData.Add ("GoldCost", 7);
		dictionaryData.Add ("Cooldown", 5);
		dictionaryData.Add ("AbilityType", 0);
		dictionaryData.Add ("CooldownLeft", 0);
		dictionaryAbilityData.Add ("Call_Pryss", dictionaryData);
		
		dictionaryData = new Dictionary<string, int>();
		dictionaryData.Add ("GoldCost", 10);
		dictionaryData.Add ("Cooldown", 5);
		dictionaryData.Add ("AbilityType", 0);
		dictionaryData.Add ("CooldownLeft", 0);
		dictionaryAbilityData.Add ("Call_Cyclops", dictionaryData);
		
		dictionaryData = new Dictionary<string, int>();
		dictionaryData.Add ("GoldCost", 7);
		dictionaryData.Add ("Cooldown", 5);
		dictionaryData.Add ("AbilityType", 0);
		dictionaryData.Add ("CooldownLeft", 0);
		dictionaryAbilityData.Add ("Call_Griffin", dictionaryData);

		dictionaryData = new Dictionary<string, int>();
		dictionaryData.Add ("GoldCost", 7);
		dictionaryData.Add ("Cooldown", 5);
		dictionaryData.Add ("AbilityType", 0);
		dictionaryData.Add ("CooldownLeft", 0);
		dictionaryAbilityData.Add ("Call_Crusader", dictionaryData);

		dictionaryData = new Dictionary<string, int>();
		dictionaryData.Add ("GoldCost", 3);
		dictionaryData.Add ("Cooldown", 5);
		dictionaryData.Add ("AbilityType", 0);
		dictionaryData.Add ("CooldownLeft", 0);
		dictionaryAbilityData.Add ("Call_Harvester", dictionaryData);

		dictionaryData = new Dictionary<string, int>();
		dictionaryData.Add ("GoldCost", 7);
		dictionaryData.Add ("Cooldown", 5);
		dictionaryData.Add ("AbilityType", 0);
		dictionaryData.Add ("CooldownLeft", 0);
		dictionaryAbilityData.Add ("Call_Priest", dictionaryData);
		
		dictionaryData = new Dictionary<string, int>();
		dictionaryData.Add ("GoldCost", 15);
		dictionaryData.Add ("Cooldown", 5);
		dictionaryData.Add ("AbilityType", 0);
		dictionaryData.Add ("CooldownLeft", 0);
		dictionaryAbilityData.Add ("Call_Necromancer", dictionaryData);
		
		dictionaryData = new Dictionary<string, int>();
		dictionaryData.Add ("GoldCost", 20);
		dictionaryData.Add ("Cooldown", 5);
		dictionaryData.Add ("AbilityType", 0);
		dictionaryData.Add ("CooldownLeft", 0);
		dictionaryAbilityData.Add ("Call_Paladin", dictionaryData);
		
		dictionaryData = new Dictionary<string, int>();
		dictionaryData.Add ("GoldCost", 10);
		dictionaryData.Add ("Cooldown", 5);
		dictionaryData.Add ("AbilityType", 0);
		dictionaryData.Add ("CooldownLeft", 0);
		dictionaryAbilityData.Add ("Call_Medusa", dictionaryData);
		
		dictionaryData = new Dictionary<string, int>();
		dictionaryData.Add ("GoldCost", 10);
		dictionaryData.Add ("Cooldown", 5);
		dictionaryData.Add ("AbilityType", 0);
		dictionaryData.Add ("CooldownLeft", 0);
		dictionaryAbilityData.Add ("Call_Ghoul", dictionaryData);
		
		dictionaryData = new Dictionary<string, int>();
		dictionaryData.Add ("GoldCost", 10);
		dictionaryData.Add ("Cooldown", 5);
		dictionaryData.Add ("AbilityType", 0);
		dictionaryData.Add ("CooldownLeft", 0);
		dictionaryAbilityData.Add ("Call_Golem", dictionaryData);
		
		dictionaryData = new Dictionary<string, int>();
		dictionaryData.Add ("GoldCost", 10);
		dictionaryData.Add ("Cooldown", 5);
		dictionaryData.Add ("AbilityType", 0);
		dictionaryData.Add ("CooldownLeft", 0);
		dictionaryAbilityData.Add ("Call_Elementalist", dictionaryData);

		dictionaryData = new Dictionary<string, int>();
		dictionaryData.Add ("GoldCost", 0);
		dictionaryData.Add ("Cooldown", 0);
		dictionaryData.Add ("AbilityType", 1);
		dictionaryData.Add ("CooldownLeft", 50);
		dictionaryAbilityData.Add ("ActiveC4", dictionaryData);

		dictionaryData = new Dictionary<string, int>();
		dictionaryData.Add ("GoldCost", 0);
		dictionaryData.Add ("Cooldown", 0);
		dictionaryData.Add ("AbilityType", 3);
		dictionaryData.Add ("CooldownLeft", 0);
		dictionaryAbilityData.Add ("ActiveArmageddon", dictionaryData);

		dictionaryData = new Dictionary<string, int>();
		dictionaryData.Add ("GoldCost", 0);
		dictionaryData.Add ("Cooldown", 0);
		dictionaryData.Add ("AbilityType", 3);
		dictionaryData.Add ("CooldownLeft", 0);
		dictionaryAbilityData.Add ("ActiveAddGold", dictionaryData);

		dictionaryData = new Dictionary<string, int>();
		dictionaryData.Add ("GoldCost", 0);
		dictionaryData.Add ("Cooldown", 0);
		dictionaryData.Add ("AbilityType", 3);
		dictionaryData.Add ("CooldownLeft", 0);
		dictionaryAbilityData.Add ("ActiveBlizzard", dictionaryData);

		dictionaryData = new Dictionary<string, int>();
		dictionaryData.Add ("GoldCost", 0);
		dictionaryData.Add ("Cooldown", 0);
		dictionaryData.Add ("AbilityType", 2);
		dictionaryData.Add ("CooldownLeft", 0);
		dictionaryData.Add ("Val1", 1);
		dictionaryAbilityData.Add ("PassiveBash", dictionaryData);

		dictionaryData = new Dictionary<string, int>();
		dictionaryData.Add ("GoldCost", 0);
		dictionaryData.Add ("Cooldown", 0);
		dictionaryData.Add ("AbilityType", 2);
		dictionaryData.Add ("CooldownLeft", 0);
		dictionaryData.Add ("Val1", 30);
		dictionaryAbilityData.Add ("PassiveCritical", dictionaryData);

		dictionaryData = new Dictionary<string, int>();
		dictionaryData.Add ("GoldCost", 0);
		dictionaryData.Add ("Cooldown", 7);
		dictionaryData.Add ("AbilityType", 3);
		dictionaryData.Add ("CooldownLeft", 0);
		dictionaryAbilityData.Add ("ActiveAOEStun", dictionaryData);

		dictionaryData = new Dictionary<string, int>();
		dictionaryData.Add ("GoldCost", 0);
		dictionaryData.Add ("Cooldown", 5);
		dictionaryData.Add ("AbilityType", 3);
		dictionaryData.Add ("CooldownLeft", 0);
		dictionaryAbilityData.Add ("ActiveCritical", dictionaryData);

		dictionaryData = new Dictionary<string, int>();
		dictionaryData.Add ("GoldCost", 0);
		dictionaryData.Add ("Cooldown", 10);
		dictionaryData.Add ("AbilityType", 3);
		dictionaryData.Add ("CooldownLeft", 0);
		dictionaryAbilityData.Add ("ActiveHeal", dictionaryData);

		dictionaryData = new Dictionary<string, int>();
		dictionaryData.Add ("GoldCost", 0);
		dictionaryData.Add ("Cooldown", 10);
		dictionaryData.Add ("AbilityType", 3);
		dictionaryData.Add ("CooldownLeft", 0);
		dictionaryAbilityData.Add ("ActiveArrowShower", dictionaryData);

		dictionaryData = new Dictionary<string, int>();
		dictionaryData.Add ("GoldCost", 0);
		dictionaryData.Add ("Cooldown", 10);
		dictionaryData.Add ("AbilityType", 3);
		dictionaryData.Add ("CooldownLeft", 0);
		dictionaryAbilityData.Add ("ActiveGriffinShout", dictionaryData);

		dictionaryData = new Dictionary<string, int>();
		dictionaryData.Add ("GoldCost", 0);
		dictionaryData.Add ("Cooldown", 0);
		dictionaryData.Add ("AbilityType", 3);
		dictionaryData.Add ("CooldownLeft", 0);
		dictionaryAbilityData.Add ("ActiveCrusaderShield", dictionaryData);
		
		dictionaryData = new Dictionary<string, int>();
		dictionaryData.Add ("GoldCost", 0);
		dictionaryData.Add ("Cooldown", 10);
		dictionaryData.Add ("AbilityType", 3);
		dictionaryData.Add ("CooldownLeft", 0);
		dictionaryAbilityData.Add ("ActiveElementalistExplosion", dictionaryData);
		
		dictionaryData = new Dictionary<string, int>();
		dictionaryData.Add ("GoldCost", 0);
		dictionaryData.Add ("Cooldown", 5);
		dictionaryData.Add ("AbilityType", 3);
		dictionaryData.Add ("CooldownLeft", 5);
		dictionaryAbilityData.Add ("ActiveRaiseDead", dictionaryData);
		
		#endregion
		
		textGO = Resources.Load ("Text") as GameObject;
		goodTextGO = Resources.Load("GoodText") as GameObject;
		popUpTextGO = Resources.Load ("PopUpText") as GameObject;
		
		textGoldGO = GameObject.Instantiate(Resources.Load("labelPlayerGold")) as GameObject;
		
		KYPlacementHelper placementHelper = textGoldGO.GetComponent<KYPlacementHelper>();
		
		placementHelper.cam = GUICam;
		placementHelper.update();
		
		textGold1 = textGoldGO.GetComponentInChildren<SpriteText>();
		//textGold1 = GameObject.Find ("labelCoin1").GetComponent<SpriteText>();
		textGold1.Text = "0";
		
		
		
		/*
		textGold2 = GameObject.Find ("labelCoin2").GetComponent<SpriteText>();
		textGold2.text = "0";
		textGold2.gameObject.SetActive(false);
		*/

		arrayPlayer[0] = new Player();
		arrayPlayer[0].amountGold = LevelManager.instance.userInitialGold;
		arrayPlayer[0].prevAmountGold = 0f;
		arrayPlayer[0].incomeRate = 0.2f;
		arrayPlayer[0].loadData();

		arrayPlayer[1] = new Player();
		arrayPlayer[1].amountGold = LevelManager.instance.initialGold;
		arrayPlayer[1].prevAmountGold = 0f;
		arrayPlayer[1].incomeRate = 0.3f;
		arrayPlayer[1].towerLevel = LevelManager.instance.enemyLevel;

		//pauseGO = GameObject.Find("but_pause(Clone)");

		speeder = loadResource("speeder");
		foreach(Transform child in speeder.transform)
		{
			child.gameObject.GetComponent<UIButton>().scriptWithMethodToInvoke = this;
		}

		GameObject.Find("level_art").transform.eulerAngles = new Vector3(90, 315, 0);
		GameObject.Find("level_art").transform.localPosition = new Vector3(-5, -110, -100);
		
		hideHUD();
		
		StartCoroutine(loadAll());
		
		GA.API.Design.NewEvent(CustomDesignEvent.getLevelStart(LevelManager.instance.level));
	}
	
	public void fast()
	{
		Time.timeScale = Time.timeScale+1f;
	}

	public void slow()
	{
		Time.timeScale = Time.timeScale-1f;
	}
	public GameObject launch;
	public IEnumerator loadAll()
	{
		int k = 0;
		int level = GameController.instance.arrayPlayer[0].towerLevel;
		if(level > 4)
		{
			level = 4;
		}
		GameObject castle = loadResource("castle1");
		PackedSprite sprite;
		SkeletonAnimation skeleton;
		string name;
		string[] levels = { "wood", "stone", "brick", "finebrick", "gold" };
		/*
		bottomTower[k] = castle.transform.Find("bottom_tower").gameObject.GetComponent<UIStateToggleBtn>();
		topTower[k] = castle.transform.Find("top_tower").gameObject.GetComponent<UIStateToggleBtn>();
		door[k] = castle.transform.Find("door").gameObject.GetComponent<UIStateToggleBtn>();
		bottomArcher[k] = castle.transform.Find("bottom_archer").Find("sprite_archer").gameObject.GetComponent<SkeletonAnimation>();
		topArcher[k] = castle.transform.Find("top_archer").Find("sprite_archer").gameObject.GetComponent<SkeletonAnimation>();
		*/
		topArcher[k] = castle.transform.Find("top_tower").gameObject.GetComponent<SkeletonAnimation>();
		bottomArcher[k] = castle.transform.Find("bottom_tower").gameObject.GetComponent<SkeletonAnimation>();
		door[k] = castle.transform.Find("door").gameObject.GetComponent<UIStateToggleBtn>();
		launch = castle.transform.Find("launch").gameObject;
		launch.SetActive(false);
		sprite = castle.transform.Find("bottom_wall").gameObject.GetComponent<PackedSprite>();
		sprite.DoAnim(0);
		sprite.PauseAnim();
		sprite.SetCurFrame(level);
		sprite = castle.transform.Find("top_wall").gameObject.GetComponent<PackedSprite>();
		sprite.DoAnim(0);
		sprite.PauseAnim();
		sprite.SetCurFrame(level);
		castle = loadResource("castle2");

		name = "tower"+levels[level]+"skeleton";
		skeleton = topArcher[k];
		skeleton.skeletonDataAsset = Resources.Load(name) as SkeletonDataAsset;
		skeleton.Clear();
		skeleton.Initialize();

		skeleton = bottomArcher[k];
		skeleton.skeletonDataAsset = Resources.Load(name) as SkeletonDataAsset;
		skeleton.Clear();
		skeleton.Initialize();

		k = 1;

		/*
		bottomTower[k] = castle.transform.Find("bottom_tower").gameObject.GetComponent<UIStateToggleBtn>();
		topTower[k] = castle.transform.Find("top_tower").gameObject.GetComponent<UIStateToggleBtn>();
		door[k] = castle.transform.Find("door").gameObject.GetComponent<UIStateToggleBtn>();
		bottomArcher[k] = castle.transform.Find("bottom_archer").Find("sprite_archer").gameObject.GetComponent<SkeletonAnimation>();
		topArcher[k] = castle.transform.Find("top_archer").Find("sprite_archer").gameObject.GetComponent<SkeletonAnimation>();
		*/
		topArcher[k] = castle.transform.Find("top_tower").gameObject.GetComponent<SkeletonAnimation>();
		bottomArcher[k] = castle.transform.Find("bottom_tower").gameObject.GetComponent<SkeletonAnimation>();
		door[k] = castle.transform.Find("door").gameObject.GetComponent<UIStateToggleBtn>();
		level = LevelManager.instance.enemyLevel-1;
		if(level > 4)
		{
			level = 4;
		}
		sprite = castle.transform.Find("bottom_wall").gameObject.GetComponent<PackedSprite>();
		sprite.DoAnim(0);
		sprite.PauseAnim();
		sprite.SetCurFrame(level);
		sprite = castle.transform.Find("top_wall").gameObject.GetComponent<PackedSprite>();
		sprite.DoAnim(0);
		sprite.PauseAnim();
		sprite.SetCurFrame(level);

		name = "tower"+levels[level]+"skeleton";
		skeleton = topArcher[k];
		skeleton.skeletonDataAsset = Resources.Load(name) as SkeletonDataAsset;
		skeleton.Clear();
		skeleton.Initialize();

		skeleton = bottomArcher[k];
		skeleton.skeletonDataAsset = Resources.Load(name) as SkeletonDataAsset;
		skeleton.Clear();
		skeleton.Initialize();

		Vector3 castlePos = castle.transform.position;
		castlePos.x += -35-(-35);
		castle.transform.position = castlePos;
		yield return null;
	}

	public GameObject loadResource(string name)
	{
		GameObject newGO = GameObject.Instantiate(Resources.Load(name) as GameObject) as GameObject;
		return newGO;
	}

	public static void hideAllCameras()
	{
		if(GameObject.Find("Main Camera"))
		{
			GameObject.Find("Main Camera").GetComponent<Camera>().enabled = false;
		}
		else
		{
			if(Camera.main != null)
			{
				Camera.main.enabled = false;
			}
		}
	}

	public static void showAllCameras()
	{
		foreach(Camera cam in Camera.allCameras)
		{
			cam.gameObject.SetActive(true);
		}
	}

	public static void showCamera(Camera cam)
	{
		cam.enabled = true;
	}

	public static void LoadLevel(string name, TransitionType type)
	{
		/*if(name.Contains("level"))
		{
			if(MapResource.instance != null)
			{
				Destroy(MapResource.instance.gameObject);
			}
		}
		if(type == 0)
		{
			//hideAllCameras();
			//foreach(var cammy in Camera.allCameras)
			//{
				//cammy.enabled = false;
			//}
			//GameObject loadcam = GameObject.Instantiate(Resources.Load("loadcamera") as GameObject) as GameObject;
			//loadcam.GetComponent<loadcamera>().load(name);
			
		}*/
		
		PersistentTransitionCamera.instance.transitionToScene(name, type);
	}
	
	public static void LoadMap(Fury.Database.Map map)
	{
		PersistentTransitionCamera.instance.transitionIntoLevel(map);
	}

	public Fury.Database.Unit nameForUnit(string name)
	{
		for(int i = 0; i < arrayUnit.Count; i++)
		{
			if(name.Contains(arrayUnit[i].name))
			{
				return arrayUnit[i];
			}
		}
		return null;
	}

	public GameObject instantiateGO(GameObject go)
	{
		GameObject newGO = GameObject.Instantiate(go, Vector3.zero, Quaternion.identity) as GameObject;
		return newGO;
	}

	public GameObject instantiateGO(GameObject go, Vector3 pos)
	{
		GameObject newGO = GameObject.Instantiate(go, pos, Quaternion.identity) as GameObject;
		return newGO;
	}
	
	public SpriteText createLabel()
	{
		GameObject newLabel = Instantiate (labelGO, Vector3.zero, Quaternion.identity) as GameObject;
		return newLabel.GetComponent<SpriteText>();
	}
	// Update is called once per frame
	private void Update()
	{
		if (isGameOver)
		{
			//reset timescale to default
			Time.timeScale = timeScale;
			return;
		}
		
		Player player = arrayPlayer[0];
		if((int)(player.prevAmountGold)-(int)(player.amountGold) != 0)
		{	
			textGold1.Text = (int)player.amountGold+"";
			player.prevAmountGold = player.amountGold;
		}
		player.amountGold += Time.deltaTime*player.incomeRate;
		
		
		player = arrayPlayer[1];
		if((int)(player.prevAmountGold)-(int)(player.amountGold) != 0)
		{
			//textGold2.Text = (int)player.amountGold+"";
			player.prevAmountGold = player.amountGold;
		}

		if(isEnd == 1 && Time.timeScale != 0)
		{
			//win
			isGameOver = true;
			isEnd = 10;
			bool isDestroyTower = true;
			int numTowersDestroyed = 0, numTowersLost = 0;
			
			hideHUD();
			LKYHUD.instance.hideUnitSideBar();
			
			//fire level complete event (with the level) to GA
			GA.API.Design.NewEvent(CustomDesignEvent.getLevelComplete(LevelManager.instance.level));
			
			//check for destroyed enemy towers
			if(TowerAgent.instance2 != null)
			{
				TowerAgent.instance2.unit.ModifyHealth(-9999999, null, TowerAgent2.instance1.unit.Controllers.WeaponController.Properties);
				isDestroyTower = false;
			}
			
			else
			{
				numTowersDestroyed++;
			}
			
			if(TowerAgent3.instance2 != null)
			{
				TowerAgent3.instance2.unit.ModifyHealth(-9999999, null, TowerAgent2.instance1.unit.Controllers.WeaponController.Properties);
				isDestroyTower = false;
			}
			
			else
			{
				numTowersDestroyed++;
			}
			
			//check for destroyed player towers
			if(TowerAgent.instance1 == null)
			{
				numTowersLost++;
			}
			
			if(TowerAgent3.instance1 == null)
			{
				numTowersLost++;
			}
			
			WaveManager.instance.enabled = false;
			
			GameObject overlayScreen = GameObject.Find("overlayscreen2(Clone)");
			overlayScreen.renderer.enabled = true;
			
			iTween.FadeFrom(overlayScreen, 0, 0.6f);
			
			waitAndExecute(1, delegate()
			{
				bool isNewStar = false, isVirgin = false;
				
				//GameObject.Find("overlayscreen2(Clone)").renderer.enabled = true;
				
		      	//WinPanel.StartTransition("Bring in Forward");
				
				//WinPanel.transform.position = new Vector3(GUICam.ScreenToWorldPoint(new Vector3(Screen.width * 0.5f, Screen.height * 0.5f, GUICam.nearClipPlane)).x, 
					//WinPanel.transform.position.y,
					//WinPanel.transform.position.z);
				
				GameObject emblem = Instantiate(winEmblemGO, GUICam.ScreenToWorldPoint(new Vector3(Screen.width * 0.5f, Screen.height * 0.5f, GUICam.nearClipPlane) + new Vector3(0, 0, 5)), Quaternion.identity) as GameObject;
				
				emblemSkeleton = emblem.GetComponent<SkeletonAnimation>();
				emblemSkeleton.state.SetAnimation("victory", false);
				
				waitAndExecute(2.0f, delegate() 
				{
					emblemSkeleton.state.SetAnimation("rotate", true);
					
					clickAnywhereScreen = Instantiate(btn_clickAnywhere, emblemSkeleton.transform.position + new Vector3(0, 0, -1), Quaternion.identity) as GameObject;
					clickAnywhereScreen.GetComponent<UIButton3D>().scriptWithMethodToInvoke = this;
				});
				
				//----update result data---
				
				Debug.Log ("win");
				ResultData resultData = (Instantiate(resultDataGO) as GameObject).GetComponent<ResultData>();
				
				var levelWrapper = KYDataManager.getInstance().levelNumForWrapper(LevelManager.instance.level);
				
				if(levelWrapper == null || levelWrapper.star == 0)
		      	{
		      		TitleController.isFlag = true;
					//resultData.isNewStar = true;
					resultData.isVirginLevel = true;
		      	}
				
				/*else
				{
					resultData.isNewStar = star > levelWrapper.star;
				}*/
				
				
				resultData.isWin = true;
				resultData.level = LevelManager.instance.level;
				
				resultData.starCount = (levelWrapper == null)? 0: levelWrapper.star;
				resultData.attemptedStarCount = star;
				resultData.enemyTowerLevel = arrayPlayer[1].towerLevel;
				
				//update special level conditions data
				resultData.towersDestroyed = numTowersDestroyed;
				resultData.towersLost = numTowersLost;
				
				resultData.remainingGold = (int)arrayPlayer[0].amountGold;
				resultData.spentGold = LKYHUD.instance.useGold;
				
				resultData.harvestersLost = MinionController.deadHarvester[0];
				resultData.harvestersKilled = MinionController.deadHarvester[1];
				
				resultData.troopsLost = MinionController.deadTroopers[0];
				resultData.troopsKilled = MinionController.deadTroopers[1];
				
				/*
		      	var levelWrapper = KYDataManager.getInstance().levelNumForWrapper(LevelManager.instance.level);
		      	if(levelWrapper == null || levelWrapper.star == 0)
		      	{
		      		TitleController.isFlag = true;
					isNewStar = true;
					isVirgin = true;
					overlayScreen.transform.position = overlayScreen.transform.position + new Vector3(0, 0, -4f);
		      	}
				
				else
				{
					isNewStar = star > levelWrapper.star;
				}
				
		      	int newStar = star;
		      	int nn = star-1;

		      	if(TitleController.loseAnyTower)
		      	{
		      		if(TowerAgent.instance1 == null)
		      		{
		      			newStar = nn;
		      		}
		      		if(TowerAgent3.instance1 == null)
		      		{
		      			newStar = nn;
		      		}
		      	}
		      	if(TitleController.isDestroyTower && !isDestroyTower)
		      	{
		      		newStar = nn;
		      	}
		      	if(TitleController.useGold > 0 && LKYHUD.instance.useGold <= TitleController.useGold)
		      	{
		      		newStar =  nn;
		      	}
		      	if(TitleController.deadHarvester >= 0)
		      	{
		      		if(MinionController.deadHarvester[0] > TitleController.deadHarvester)
		      		{
		      			newStar = nn;
		      		}
		      	}
		      	if(TitleController.killHarvester > 0)
		      	{
		      		if(MinionController.deadHarvester[1] < TitleController.killHarvester)
		      		{
		      			newStar = nn;
		      		}
				}
				
		      	KYDataManager.getInstance().addLevel(LevelManager.instance.level, newStar);
		      	int xpCount;// = KYDataManager.getInstance().levelNumForWrapper(LevelManager.instance.level).count;
		      	//if(xpCount == 0)
		      	//{
				
				xpCount = getRewardXP(isNewStar);
				
				Debug.Log ("You got rewarded " + getRewardFragments(isNewStar) + " fragments!");
				
				if (isVirgin)
				{
					Debug.Log ("won " + LevelManager.instance.level + " for the first time! checking if a new character is unlocked...");
					
					LevelRewardWrapper reward = getRewardWrapper(LevelManager.instance.level);
					
					//has at least one unlockable unit
					if (reward.unlockableCharacters.Count > 0)
					{
						foreach (string name in reward.unlockableCharacters)
						{
							Debug.Log ("Unlocked " + name + "!");
						}
					}
				}
				
				KYDataManager.getInstance().dataWrapper.xpCount += xpCount;
				
		      		//xpCount = LevelManager.instance.exp;
		      	//}
		      //	WinPanel.transform.Find("GoodText").gameObject.GetComponent<SpriteText>().Text = "You've gained "+(xpCount) + " XP";
		       //	WinPanel.transform.Find("GoodText2").gameObject.GetComponent<SpriteText>().Text = "Congratulation!";
		      	//KYDataManager.getInstance().dataWrapper.xpCount += xpCount+(int)((float)xpCount*UnityEngine.Random.Range(0.01f, 0.03f));
				//WinPanel.transform.Find("lose").gameObject.SetActive(false);
			//	WinPanel.transform.Find("victory").gameObject.GetComponent<AnimateText>().doit();
				KYAAgent aag = null;
				for(int i = 1; i <= newStar; ++i)
				{
					aag = WinPanel.transform.Find("starcollector").Find("star"+(i)).gameObject.GetComponent<KYAAgent>();
					aag.aname = "smallbig";
					aag.transform.localScale = Vector3.zero;
					aag.Animate((i-1)*0.8f, false);					
				}				
				for(int i = newStar+1; i <= 3; ++i)
				{
					WinPanel.transform.Find("starcollector").Find("star"+(i)).gameObject.SetActive(false);
				}
		      	//xpCount = (int)(xpCount*0.95f);
		      	KYDataManager.getInstance().levelNumForWrapper(LevelManager.instance.level).count = xpCount;*/
			});
		}
		else if(isEnd == 2 && Time.timeScale != 0)
		{
			//lose (tower destroyed)
			isGameOver = true;
			isEnd = 10;
			int numTowersDestroyed = 0, numTowersLost = 0;
			
			hideHUD();
			LKYHUD.instance.hideUnitSideBar();
			
			GA.API.Design.NewEvent(CustomDesignEvent.getLevelFail(LevelManager.instance.level));
			
			/*if(TowerAgent.instance1 != null)
			{
				TowerAgent.instance1.unit.ModifyHealth(-9999999, null, TowerAgent2.instance2.unit.Controllers.WeaponController.Properties);
			}
			if(TowerAgent3.instance1 != null)
			{
				TowerAgent3.instance1.unit.ModifyHealth(-9999999, null, TowerAgent2.instance2.unit.Controllers.WeaponController.Properties);
			}*/
			
			//check for destroyed enemy towers
			//check for destroyed enemy towers
			if(TowerAgent.instance1 != null)
			{
				TowerAgent.instance1.unit.ModifyHealth(-9999999, null, TowerAgent2.instance2.unit.Controllers.WeaponController.Properties);
				//isDestroyTower = false;
			}
			
			else
			{
				numTowersDestroyed++;
			}
			
			if(TowerAgent3.instance1 != null)
			{
				TowerAgent3.instance1.unit.ModifyHealth(-9999999, null, TowerAgent2.instance2.unit.Controllers.WeaponController.Properties);
				//isDestroyTower = false;
			}
			
			else
			{
				numTowersDestroyed++;
			}
			
			//check for destroyed player towers
			if(TowerAgent.instance1 == null)
			{
				numTowersLost++;
			}
			
			if(TowerAgent3.instance1 == null)
			{
				numTowersLost++;
			}
			
			WaveManager.instance.enabled = false;
			
			GameObject overlayScreen = GameObject.Find("overlayscreen2(Clone)");
			overlayScreen.renderer.enabled = true;
			
			iTween.FadeFrom(overlayScreen, 0, 0.6f);
			
			waitAndExecute(1, delegate()
			{
				GameObject emblem = Instantiate(loseEmblemGO, GUICam.ScreenToWorldPoint(new Vector3(Screen.width * 0.5f, Screen.height * 0.5f, GUICam.nearClipPlane) + new Vector3(0, 0, 5)), Quaternion.identity) as GameObject;
				
				GameObject shadow = emblem.transform.FindChild("shadow").gameObject;
				
				iTween.ScaleFrom(shadow, iTween.Hash ("x", 0, "y", 0, "delay", 0.5f, "time", 0.5f));
				
				emblemSkeleton = emblem.GetComponent<SkeletonAnimation>();
				emblemSkeleton.state.SetAnimation("defeat", false);
				
				waitAndExecute(2.0f, delegate() {
					
					clickAnywhereScreen = Instantiate(btn_clickAnywhere, emblemSkeleton.transform.position + new Vector3(0, 0, -1), Quaternion.identity) as GameObject;
					clickAnywhereScreen.GetComponent<UIButton3D>().scriptWithMethodToInvoke = this;
					
				});
				
				Debug.Log ("lost all towers");
				ResultData resultData = (Instantiate(resultDataGO) as GameObject).GetComponent<ResultData>();
				
				var levelWrapper = KYDataManager.getInstance().levelNumForWrapper(LevelManager.instance.level);
				
				resultData.isWin = false;
				resultData.level = LevelManager.instance.level;
				resultData.starCount = (levelWrapper == null)? 0: levelWrapper.star;
				resultData.attemptedStarCount = star;
				resultData.enemyTowerLevel = arrayPlayer[1].towerLevel;
				
				//update special level conditions data
				resultData.towersDestroyed = numTowersDestroyed;
				resultData.towersLost = numTowersLost;
				
				resultData.remainingGold = (int)arrayPlayer[0].amountGold;
				resultData.spentGold = LKYHUD.instance.useGold;
				
				resultData.harvestersLost = MinionController.deadHarvester[0];
				resultData.harvestersKilled = MinionController.deadHarvester[1];
				
				resultData.troopsLost = MinionController.deadTroopers[0];
				resultData.troopsKilled = MinionController.deadTroopers[1];
				
				//GameObject.Find("overlayscreen2(Clone)").renderer.enabled = true;
				//WinPanel.StartTransition ("Bring in Forward");
				
				//WinPanel.transform.position = new Vector3(GUICam.ScreenToWorldPoint(new Vector3(Screen.width * 0.5f, Screen.height * 0.5f, GUICam.nearClipPlane)).x, 
				//	WinPanel.transform.position.y,
				//	WinPanel.transform.position.z);
				
				//WinPanel.transform.Find("victory").gameObject.SetActive(false);
				//WinPanel.transform.Find("lose").gameObject.GetComponent<AnimateText>().doit();
			   //	WinPanel.transform.Find("box_A").gameObject.SetActive(false);
				//KYDataManager.getInstance().dataWrapper.xpCount += 10;
				/*KYDataManager.getInstance().addLevel(LevelManager.instance.level, 0);
				WinPanel.transform.Find("starcollector").gameObject.SetActive(false);
				WinPanel.transform.Find("star4").gameObject.SetActive(false);
				WinPanel.transform.Find("star5").gameObject.SetActive(false);
				WinPanel.transform.Find("star6").gameObject.SetActive(false);*/
			});
		}
		else if(isEnd == 3 && Time.timeScale != 0)
		{
			//lose (abort/quit)
			isGameOver = true;
			isEnd = 10;
			int numTowersDestroyed = 0, numTowersLost = 0;
			
			hideHUD();
			LKYHUD.instance.hideUnitSideBar();
			
			GA.API.Design.NewEvent(CustomDesignEvent.getLevelAbort(LevelManager.instance.level));
			
			/*if(TowerAgent.instance1 != null)
			{
				TowerAgent.instance1.unit.ModifyHealth(-9999999, null, TowerAgent2.instance2.unit.Controllers.WeaponController.Properties);
			}
			if(TowerAgent3.instance1 != null)
			{
				TowerAgent3.instance1.unit.ModifyHealth(-9999999, null, TowerAgent2.instance2.unit.Controllers.WeaponController.Properties);
			}*/
			
			//check for destroyed enemy towers
			if(TowerAgent.instance1 != null)
			{
				TowerAgent.instance1.unit.ModifyHealth(-9999999, null, TowerAgent2.instance2.unit.Controllers.WeaponController.Properties);
				//isDestroyTower = false;
			}
			
			else
			{
				numTowersDestroyed++;
			}
			
			if(TowerAgent3.instance1 != null)
			{
				TowerAgent3.instance1.unit.ModifyHealth(-9999999, null, TowerAgent2.instance2.unit.Controllers.WeaponController.Properties);
				//isDestroyTower = false;
			}
			
			else
			{
				numTowersDestroyed++;
			}
			
			//check for destroyed player towers
			if(TowerAgent.instance1 == null)
			{
				numTowersLost++;
			}
			
			if(TowerAgent3.instance1 == null)
			{
				numTowersLost++;
			}
			
			WaveManager.instance.enabled = false;
			
			GameObject overlayScreen = GameObject.Find("overlayscreen2(Clone)");
			overlayScreen.renderer.enabled = true;
			
			iTween.FadeFrom(overlayScreen, 0, 0.6f);
			
			waitAndExecute(1, delegate()
			{
				
				GameObject emblem = Instantiate(loseEmblemGO, GUICam.ScreenToWorldPoint(new Vector3(Screen.width * 0.5f, Screen.height * 0.5f, GUICam.nearClipPlane) + new Vector3(0, 0, 5)), Quaternion.identity) as GameObject;
				
				GameObject shadow = emblem.transform.FindChild("shadow").gameObject;
				
				iTween.ScaleFrom(shadow, iTween.Hash ("x", 0, "y", 0, "delay", 0.5f, "time", 0.5f));
				
				emblemSkeleton = emblem.GetComponent<SkeletonAnimation>();
				emblemSkeleton.state.SetAnimation("defeat", false);
				
				waitAndExecute(2.0f, delegate() {
					
					clickAnywhereScreen = Instantiate(btn_clickAnywhere, emblemSkeleton.transform.position + new Vector3(0, 0, -1), Quaternion.identity) as GameObject;
					clickAnywhereScreen.GetComponent<UIButton3D>().scriptWithMethodToInvoke = this;
					
				});
				
				//WinPanel.StartTransition ("Bring in Forward");
				
				//WinPanel.transform.position = new Vector3(GUICam.ScreenToWorldPoint(new Vector3(Screen.width * 0.5f, Screen.height * 0.5f, GUICam.nearClipPlane)).x, 
				//	WinPanel.transform.position.y,
				//	WinPanel.transform.position.z);
				
				//WinPanel.transform.Find("victory").gameObject.SetActive(false);
				//WinPanel.transform.Find("lose").gameObject.GetComponent<AnimateText>().doit();
				//WinPanel.transform.Find("GoodText").gameObject.GetComponent<SpriteText>().Text = "You've gained 10 experience.";
				//WinPanel.transform.Find("GoodText2").gameObject.GetComponent<SpriteText>().Text = "Sorry! Try harder.";
			   //	WinPanel.transform.Find("box_A").gameObject.SetActive(false);
				//KYDataManager.getInstance().dataWrapper.xpCount += 10;
				//KYDataManager.getInstance().addLevel(LevelManager.instance.level, 0);
				//WinPanel.transform.Find("starcollector").gameObject.SetActive(false);
				//WinPanel.transform.Find("star4").gameObject.SetActive(false);
				//WinPanel.transform.Find("star5").gameObject.SetActive(false);
				//WinPanel.transform.Find("star6").gameObject.SetActive(false);
				
				Debug.Log("aborted");
				ResultData resultData = (Instantiate(resultDataGO) as GameObject).GetComponent<ResultData>();
				
				var levelWrapper = KYDataManager.getInstance().levelNumForWrapper(LevelManager.instance.level);
				
				resultData.isWin = false;
				resultData.level = LevelManager.instance.level;
				resultData.starCount = (levelWrapper == null)? 0: levelWrapper.star;
				resultData.attemptedStarCount = star;
				resultData.enemyTowerLevel = arrayPlayer[1].towerLevel;
				
				//update special level conditions data
				resultData.towersDestroyed = numTowersDestroyed;
				resultData.towersLost = numTowersLost;
				
				resultData.remainingGold = (int)arrayPlayer[0].amountGold;
				resultData.spentGold = LKYHUD.instance.useGold;
				
				resultData.harvestersLost = MinionController.deadHarvester[0];
				resultData.harvestersKilled = MinionController.deadHarvester[1];
				
				resultData.troopsLost = MinionController.deadTroopers[0];
				resultData.troopsKilled = MinionController.deadTroopers[1];
			});
		}
	}
	
	int getRewardXP(bool isNewStar)
	{
		Debug.Log ("getting star no. " + star);
		return (int)(UnityEngine.Random.Range(GameSettings.XP_JAGGED_LOW_MOD, GameSettings.XP_JAGGED_HIGH_MOD) * 
			Math.Max((int)GameSettings.XP_MIN_BY_STAR[star - 1], (int)(
			(arrayPlayer[0].amountGold *  arrayPlayer[1].towerLevel) + (GameSettings.XP_BASE_STAR_MOD * arrayPlayer[1].towerLevel * (isNewStar? star: 0))))
			);
	}
	
	int getRewardFragments(bool isNewStar)
	{
		LevelRewardWrapper reward = getRewardWrapper(LevelManager.instance.level);
		
		if (isNewStar && reward != null)
		{
			return reward.fragments[star - 1];
		}
		
		return 0;
	}
	
	LevelRewardWrapper getRewardWrapper(int level)
	{
		foreach (LevelRewardWrapper reward in KYDataManager.arrayLevelRewardWrapper)
		{
			if (reward.level == level)
			{
				return reward;
			}
		}
		
		return null;
	}
	
	void Start()
	{
		//Network.Disconnect();
		sfx.getInstance().play("battlebgm"); 
		/*WinPanel = (GameObject.Instantiate(Resources.Load("panel_victory") as GameObject) as GameObject).GetComponent<UIPanel>();
		WinPanel.transform.position -= new Vector3(-500, 0, 0);
		foreach(Transform child in WinPanel.transform)
		{
			if(child.gameObject.GetComponent<UIButton>())
			{
				child.gameObject.GetComponent<UIButton>().scriptWithMethodToInvoke = this;
			}
		}*/
		LosePanel = (GameObject.Instantiate(Resources.Load("panel_options") as GameObject) as GameObject).GetComponent<UIPanel>();
		foreach(Transform child in LosePanel.transform)
		{
			if(child.gameObject.GetComponent<UIButton>())
			{
				child.gameObject.GetComponent<UIButton>().scriptWithMethodToInvoke = this;
			}
		}
		btnBGM = LosePanel.transform.Find("toggle_bgm").gameObject.GetComponent<UIStateToggleBtn>();
		btnBGM.scriptWithMethodToInvoke = this;
		btnSFX = LosePanel.transform.Find("toggle_sfx").gameObject.GetComponent<UIStateToggleBtn>();
		btnSFX.scriptWithMethodToInvoke = this;
		string music = PlayerPrefs.GetString("IsMusic", "null");
		if(music == "no")
		{
			btnBGM.SetToggleState(1);
		}
		else
		{
			btnBGM.SetToggleState(0);
		}
		string sound = PlayerPrefs.GetString("IsSound", "null");
		if(sound == "no")
		{
			btnSFX.SetToggleState(1);
		}
		else
		{
			btnSFX.SetToggleState(0);
		}

		GameObject.Instantiate(Resources.Load("overlayscreen2") as GameObject);

	}

	void Pausing ()
	{
		if(isEnd == 10)
		{
			return;
		}
		Fury.Hud.ConsumeLMB();
		if(Time.timeScale == 0)
		{
			isPaused = false;
			if(LKYHUD.instance.break1) GameObject.Find("overlayscreen2(Clone)").renderer.enabled = false;
        	LosePanel.StartTransition("Dismiss Forward");
	    	Time.timeScale = timeScale;
			showHUD();
		}
        else 
        {
			isPaused = true;
			GameObject.Find("overlayscreen2(Clone)").renderer.enabled = true;
        	LosePanel.StartTransition ("Bring in Forward");
       		Time.timeScale = 0;
			hideHUD();
        }
	}
	
	public void waitAndExecute(float seconds, System.Action callback)
	{
		StartCoroutine(execute(seconds, callback));
	}

	public IEnumerator execute(float seconds, System.Action callback)
	{
		yield return new WaitForSeconds(seconds);
		callback();
	}

	public static IEnumerator execute2(float seconds, System.Action callback)
	{
		yield return new WaitForSeconds(seconds);
		callback();
	}
	
	public void hideHUD()
	{
		textGoldGO.SetActive(false);
		//pauseGO = GameObject.Find("but_pause(Clone)");
		
		if (pauseGO)
		{
			pauseGO.SetActive(false);
		}
		
		speeder.SetActive(false);
	}
	
	public void showHUD()
	{
		
		//pauseGO = GameObject.Find("but_pause(Clone)");
		
		if (pauseGO)
		{
			pauseGO.SetActive(true);
		}
		
		if (hasBattleBegun)
		{
			//speeder.SetActive(true);
			
			textGoldGO.SetActive(true);
		
			iTween.MoveFrom(textGoldGO, iTween.Hash ("position", (textGoldGO.transform.position - new Vector3(100, 0, 0)), "delay", 1, "time", 0.3f));
		}
		
	}

	public void ToggleBGM()
	{
		if(btnBGM.StateNum == 1)
		{
			PlayerPrefs.SetString("IsMusic", "yes");
			btnBGM.SetToggleState(0);
			sfx.instance.onMusic(1);
		}
		else
		{
			PlayerPrefs.SetString("IsMusic", "no");
			btnBGM.SetToggleState(1);
			sfx.instance.onMusic(0f);
		}
	}

	public void ToggleSFX()
	{
		if(btnSFX.StateNum == 1)
		{
			PlayerPrefs.SetString("IsSound", "yes");
			btnSFX.SetToggleState(0);
			sfx.instance.onSound(1f);
		}
		else
		{
			PlayerPrefs.SetString("IsSound", "no");
			btnSFX.SetToggleState(1);
			sfx.instance.onSound(0f);
		}	
	}
	
	public void OnClickGoToReport()
	{
		//disable and destroy button to prevent repeated invocation of this function
		clickAnywhereScreen.GetComponent<UIButton3D>().enabled = false;
		DestroyImmediate(clickAnywhereScreen);
		
		//go to result screen
		GameController.LoadLevel("ResultScreen", TransitionType.Close);
	}

	public void Quit ()
	{
        Time.timeScale = timeScale;
        if(GameObject.Find("buttonCamera"))
		{
			GameObject.Find("buttonCamera").GetComponent<Camera>().enabled = false;
		}
		if(GameObject.Find("GUI camera"))
		{
			GameObject.Find("GUI camera").GetComponent<Camera>().enabled = false;
		}
  		GameController.LoadLevel("title", TransitionType.CloseOpen);
	}
	
	public void Restart()
	{
		Time.timeScale = timeScale;
		//GameController.LoadLevel(Application.loadedLevelName, TransitionType.CloseOpen);
		
		MapLoader.instance.ReloadMap();
	}

	public void Close()
	{	
		Fury.Hud.ConsumeLMB();
		if(Time.timeScale == 0)
            Time.timeScale = timeScale;
        LosePanel.StartTransition("Dismiss Forward");
		isEnd = 3;
		if(enabled == false)
		{
			Quit();
		}
	}

	private void OnApplicationQuit()
	{
		PlayerPrefs.Save();
	}
}
