using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class SpireController : MonoBehaviour 
{
	private NavMeshAgent NaviAgent;
	
	// Use this for initialization
	void Start () 
	{
		//StartCoroutine("ModifyNavMeshAgent");
	}
	
	// Update is called once per frame
	void Update () 
	{}
	
	private IEnumerator ModifyNavMeshAgent()
	{
		yield return new WaitForSeconds(5);

		NaviAgent = GetComponent("NavMeshAgent") as NavMeshAgent;
		NaviAgent.obstacleAvoidanceType = ObstacleAvoidanceType.NoObstacleAvoidance;
	}
}
