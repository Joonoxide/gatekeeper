using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class PassiveLightOfHeaven : Fury.Database.Ability
{
	public override object OnBeginCast(Fury.Behaviors.Unit caster, Fury.Behaviors.Targetable target, UnityEngine.Vector3 position)
	{
		TowerAgent3 agent = caster.gameObject.GetComponent<TowerAgent3>();
		if(!agent.dictionaryAbilityLevel.ContainsKey(this))
		{
			agent.dictionaryAbilityLevel.Add(this, 0);
			agent.dictionaryAbilityCooldown.Add (this, CastCooldown);
		}
		return null;
	}

	public override void OnEndCast(object tag, Fury.Behaviors.Unit caster, Fury.Behaviors.Targetable target, UnityEngine.Vector3 position)
	{
		foreach (var cmdr in Fury.Behaviors.Manager.Instance.Commanders)
		{
			if(cmdr != caster.Owner)
			{
				//foreach (var unit in cmdr.Units)
				foreach(KeyValuePair<int, Fury.Behaviors.Unit> unitData in cmdr.Units)
				{
					Fury.Behaviors.Unit unit = unitData.Value;
					
					if (unit.gameObject.GetComponent<WalkableAgent>())
					{
						unit.ModifyHealth(-30, unit, this);
						if (Effect != null)
						{
							var effect = (UnityEngine.GameObject)UnityEngine.GameObject.Instantiate(Effect);
							effect.transform.position = unit.transform.position;
						}
						break;
					}
				}
			}
		}
	}
}
