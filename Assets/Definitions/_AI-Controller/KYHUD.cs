using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using UnityEngine;
using Hud = Fury.Hud;

public class KYHUD : MonoBehaviour
{
	/// <summary>
	/// This interface can be attached to abilities that affect an area.
	/// </summary>
	public interface IAbilityInfo
	{
		Single Radius { get; }
		Boolean SnapToGrid { get; }
		String Description { get; }
	}
	
	public class TouchWrapper
	{
		public Touch touch;
		public float timeElapsed;
	}

	public enum ControlState { Summon, Charge, Default = -1 };
	ControlState controlState;
	public Fury.Database.Ability queuedAbility;
	public static KYHUD Instance;
	
	public Fury.Behaviors.Unit CrystalTower;
	public Fury.Behaviors.Unit Hero;
	public Fury.Behaviors.Unit Hero2;
	
	public Bounds CameraConstraint = new Bounds(Vector3.zero, Vector3.one * Single.MaxValue);
	public Texture2D IconFrame;
	public Texture2D[] Numbers;
	public Texture2D panel;
	public GameObject FollowObject;
	public Material MaterialSelect, MaterialHover, MaterialArrow, MaterialArrowSelected;
	public Single MaxDistanceFromGround = 12, MinDistanceFromGround = 7, DesiredDistanceFromGround = 10;
	public LayerMask UnitMask, IgnoreMask;

	public GameObject TargetPrefab, RingPrefab, ArrowPrefab;
	private GameObject HoverObject;
	private GameObject MouseTargetObject;

	public Texture2D Green, Green25, Black50, Black75, Blue, Purple;

	public Fury.Controllers.SelectionController Selection;
	public KYAgent Selection1;

	private Fury.Behaviors.Commander Owner;

    public List<GUITexture> Textures;
	public List<GUIText> Texts;
	public TouchWrapper[] arrayTouchWrapper = new TouchWrapper[5];
	public TouchWrapper newTouchWrapper = null;
	public TouchWrapper finishedTouchWrapper = null;
	private Int32 TexturePosition, TextPosition;
	public float chargeTime;
	public bool prevLMB = false;
	public bool isMouseRelease = false;
	int tab  = 0;
	List<int> touchIndex = new List<int>();

	private void Awake()
	{
		Instance = this;
	}
	
	void Start()
	{
		controlState = ControlState.Default;
		for(int i = 0; i < 5; ++i)
		{
			arrayTouchWrapper[i] = new TouchWrapper();
			arrayTouchWrapper[i].timeElapsed = -1f;
		}
	}

	bool newTouch(Touch touch)
	{
		foreach(var touchWrapper in arrayTouchWrapper)
		{
			if(touchWrapper.touch.phase == TouchPhase.Ended || touchWrapper.timeElapsed == -1)
			{
				touchWrapper.touch = touch;
				touchWrapper.timeElapsed = 0f;
				newTouchWrapper = touchWrapper;
				return true;
			}
		}
		return false;
	}

	bool finishTouch(Touch touch)
	{
		foreach(var touchWrapper in arrayTouchWrapper)
		{
			if(touchWrapper.touch.fingerId == touch.fingerId)
			{
				touchWrapper.touch = touch;
				finishedTouchWrapper = touchWrapper;
				chargeTime = touchWrapper.timeElapsed;
				return true;
			}
		}
		return false;
	}

	void updateTouch()
	{
		if(SystemInfo.deviceType == DeviceType.Handheld)
		{
			newTouchWrapper = null;
			finishedTouchWrapper = null;
			foreach(var touch in Input.touches)
			{
				if(touch.phase == TouchPhase.Began)
				{
					newTouch(touch);
				}
				else if(touch.phase == TouchPhase.Ended)
				{
					finishTouch(touch);
				}
			}

			foreach(var touchWrapper in arrayTouchWrapper)
			{
				if(touchWrapper.touch.phase != TouchPhase.Ended && touchWrapper.timeElapsed != -1)
				{
					touchWrapper.timeElapsed += Time.deltaTime;
				}
			}
		}

	}
	
	bool isRelease()
	{
		if(SystemInfo.deviceType == DeviceType.Handheld)
		{
			if(finishedTouchWrapper == null)
			{
				return false;
			}
			else
			{
				return true;
			}
		}
		else
		{
			return isMouseRelease;
		}

	}
	bool isTouch()
	{
		if(SystemInfo.deviceType == DeviceType.Handheld)
		{
			if(newTouchWrapper != null)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return Hud.TriggerLMB;
		}
	}
	
	private void Initialize()
	{
		foreach (var cmdr in Fury.Behaviors.Manager.Instance.Commanders)
			if (cmdr.IsLocal)
			{
				Owner = cmdr;
				Textures = new List<GUITexture>();
				Texts = new List<GUIText>();

				//Hud.OnDragComplete += new Fury.General.Action<Rect>(OnDragComplete);

				Hud.UnitMask = UnitMask;

				Green = new Texture2D(1, 1);
				Green.SetPixel(0, 0, new Color(0, 0.5f, 0));
				Green.Apply();

				Green25 = new Texture2D(1, 1);
				Green25.SetPixel(0, 0, new Color(0f, 0.5f, 0f, 0.25f));
				Green25.Apply();

				Black50 = new Texture2D(1, 1);
				Black50.SetPixel(0, 0, new Color(0f, 0f, 0f, 0.5f));
				Black50.Apply();

				Black75 = new Texture2D(1, 1);
				Black75.SetPixel(0, 0, new Color(0f, 0f, 0f, 0.75f));
				Black75.Apply();

				Blue = new Texture2D(1, 1);
				Blue.SetPixel(0, 0, new Color(.25f, .25f, 1f));
				Blue.Apply();

				Purple = new Texture2D(1, 1);
				Purple.SetPixel(0, 0, new Color(1f, .25f, 1f));
				Purple.Apply();

				MouseTargetObject = (GameObject)GameObject.Instantiate(TargetPrefab);
				MouseTargetObject.name = "!MouseTarget";

				break;
			}			
	}

	private void Update()
	{
		if (Fury.Behaviors.Manager.Instance.GameState == Fury.GameStates.Playing && Owner == null)
		{
			Initialize();
		}
		if (Owner == null) 
		{
			return;
		}
		DesiredDistanceFromGround = Mathf.Clamp(DesiredDistanceFromGround + Math.Sign(Input.GetAxis("Mouse ScrollWheel")) * -1f, 
			MinDistanceFromGround, MaxDistanceFromGround);

		if (FollowObject != null)
		{
			Camera.main.transform.position = Vector3.Lerp(Camera.main.transform.position,
			FollowObject.transform.position - Camera.main.transform.forward * DesiredDistanceFromGround, 0.1f);
		}

		isMouseRelease = false;
		if(SystemInfo.deviceType != DeviceType.Handheld)
		{
			if(Input.GetMouseButtonDown(0))
			{
				chargeTime = 0f;
				prevLMB = true;
			}
			else if(Input.GetMouseButtonUp(0))
			{

				isMouseRelease = true;
				prevLMB = false;
			}
			else if(prevLMB)
			{
				chargeTime += Time.deltaTime;
			}
		}

	}

	private void LateUpdate()
	{
		if (Owner == null)
		{
			return;
		}
		if(Selection1 == null)
		{
			if(TowerAgent2.getInstanceForIndex(0))
			{
				Selection1 = TowerAgent2.getInstanceForIndex(0);
			}
			else if(TowerAgent.getInstanceForIndex(0))
			{
				Selection1 = TowerAgent.getInstanceForIndex(0);
			}
			else if(TowerAgent3.getInstanceForIndex(0))
			{
				Selection1 = TowerAgent3.getInstanceForIndex(0);
			}
			else
			{
				Selection1 = null;
			}
		}
		
		Selection.Clear ();
		if(Selection1 != null)
		{
			Selection.Add (Selection1.unit);
		}
		else
		{
			Selection.Remove (Selection1.unit);
		}

		Selection.Prune ();
		touchIndex.Clear ();
		updateTouch();
		Vector3 touchPosition;
		if(SystemInfo.deviceType == DeviceType.Handheld)
		{
			if(newTouchWrapper != null)
			{
				touchPosition = newTouchWrapper.touch.position;
				var ray = Camera.main.ScreenPointToRay(touchPosition);
				Hud.CalculateFocus(ray);
			}
			else
			{
				touchPosition = new Vector3(-100, -100, -100);
			}
		}
		else
		{		
			touchPosition = Hud.Position;
			var ray = Camera.main.ScreenPointToRay(touchPosition);
			Hud.CalculateFocus(ray);
		}

		if (HoverObject == null)
			HoverObject = (GameObject)GameObject.Instantiate(RingPrefab);

		HoverObject.renderer.enabled = false;
		
		var invertedMousePos = new Vector2(Hud.Position.x, Screen.height - Hud.Position.y);
		var iconSize = 64;
		
		int a = 0;
		Rect rectPanel1 = new Rect(0, -250, 150, Screen.height+300);
		DrawTexture (panel, rectPanel1, Color.grey);
		KYAgent.SpawnWrapper spawnWrapper = null;
		Fury.Database.Ability ability;

		foreach (var abilityWrapper in Selection1.listAbilityWrapper)
		{
			
			ability = abilityWrapper.ability;
			UnityEngine.Rect abilityRect;
			
			abilityRect = new Rect(10, 20+a*(iconSize+40), iconSize, iconSize);
			
			var isMouseOver = abilityRect.Contains(invertedMousePos);
			
			if(abilityWrapper.abilityType == AbilityType.Summon)
			{
				
				spawnWrapper = (abilityWrapper as KYAgent.SpawnAbilityWrapper).spawnWrapper;
				
				DrawTexture(ability.Icon, abilityRect, Color.gray);
				DrawText ("Lv "+abilityWrapper.level+"      "+spawnWrapper.cost+"", abilityRect.x, abilityRect.y+iconSize, 15);
				DrawText (spawnWrapper.count+"", abilityRect.x+10, abilityRect.y+10);
			}
			else
			{
				DrawTexture(ability.Icon, abilityRect, Color.gray);
			}
			
			float cd = (abilityWrapper.cooldown == 0)? 0 : Mathf.Clamp01(abilityWrapper.cooldownLeft/abilityWrapper.cooldown);
			var overlayRect = abilityRect;
			overlayRect.y += overlayRect.height * (1-cd);
			overlayRect.height *= cd;
			DrawTexture(Black75, overlayRect, new Color(0.5f, 0.5f, 0.5f, 0.5f));

			abilityRect = new Rect(abilityRect.x - 2, abilityRect.y - 2, abilityRect.width + 4, abilityRect.height + 4);
			//DrawTexture(IconFrame, abilityRect, isQueued ? new Color(0.5f, 0f, 0f) : new Color(0.15f, 0.15f, 0.15f));
			
			if(ability.name.Contains ("Aura"))
			{
				TowerAgent agent = TowerAgent.getInstanceForIndex(0);
				float level = (agent.dictionaryAuraLevel.ContainsKey((ability as PassiveAura).auraBuff))? agent.dictionaryAuraLevel[(ability as PassiveAura).auraBuff] : 0;
				
				DrawTexture(Numbers[(int)level], new Rect(abilityRect.xMax - 32, abilityRect.yMax - 32, 32, 32), Color.grey);
			}
		
			if (isMouseOver && isTouch())
			{
				if(cd == 0 && Time.timeScale != 0 && (int)GameController.instance.arrayPlayer[0].amountGold >= spawnWrapper.cost)
				{
					if(abilityWrapper.abilityType == AbilityType.Summon)
					{	
						if(spawnWrapper.unit.Prefab.tag == "harvester" && (Selection1 as TowerAgent2).minionController.listHarvesterIdentifier.Count+spawnWrapper.count >= 3)
						{
						}
						else
						{
							spawnWrapper.incrementCount();
							GameController.instance.arrayPlayer[0].amountGold -= spawnWrapper.cost;
						}
						controlState = ControlState.Summon;
					}
					else if(abilityWrapper.abilityType  == AbilityType.Charge)
					{
						controlState = ControlState.Charge;
						queuedAbility = abilityWrapper.ability;
					}
				}
				Hud.ConsumeLMB();
			}
			++a;
		}
		if (Time.timeScale != 0)
		{
			if(controlState == ControlState.Default && isTouch() && !rectPanel1.Contains(invertedMousePos))
			{
				if(Hud.FocusTarget != null && Hud.FocusTarget is Fury.Behaviors.Unit && Hud.FocusTarget.gameObject.tag == "tower")
				{
					Fury.Behaviors.Unit u = Hud.FocusTarget as Fury.Behaviors.Unit;
					if(u.Owner.Identifier == 0)
					{
						Selection1 = u.agent;
						tab = 0;
					}
				}
				else
				{
					//spaceUnits(Hud.FocusPoint, 0);
				}
			} 
			else if(controlState == ControlState.Summon && isTouch())
			{
				if(!rectPanel1.Contains(invertedMousePos))
				{
					foreach(var abilityWrapper in Selection1.listAbilityWrapper)
					{
						spawnWrapper = (abilityWrapper as KYAgent.SpawnAbilityWrapper).spawnWrapper;
						if(spawnWrapper.count > 0)
						{
							MinionController minionController = (Selection1 as TowerAgent2).minionController;
							//minionController.CreateSpawn (spawnWrapper.unit, spawnWrapper.count, new Vector3(30, 0, Hud.FocusPoint.z));
							abilityWrapper.cooldown = spawnWrapper.count*abilityWrapper.fixedCooldown;
							abilityWrapper.cooldownLeft = abilityWrapper.cooldown;
							spawnWrapper.resetCount();
						}				
					}
					controlState = ControlState.Default;
				}
			}
			else if(controlState == ControlState.Charge && isRelease())
			{
				if(!rectPanel1.Contains(invertedMousePos))
				{
					Vector3 direction = (Selection1.unit.transform.position-Hud.FocusPoint).normalized;
					Vector3 targetPos = Selection1.unit.transform.position+(-direction*chargeTime*50f);
					targetPos.y = Selection1.unit.transform.position.y;
					Selection1.unit.Order(queuedAbility, Selection1.unit, targetPos);
					controlState = ControlState.Default;
				}
			}
            Hud.ConsumeLMB();
		}
		// Animate the mouse target texture
		if (MouseTargetObject.active)
		{
			var texOffset = MouseTargetObject.renderer.material.mainTextureOffset + new Vector2(-Time.deltaTime * 2, 0);
			if (texOffset.x < 0) MouseTargetObject.SetActive (false);

			MouseTargetObject.renderer.material.mainTextureOffset = texOffset;
		}

		// End the last call
        
		for (Int32 i = TexturePosition; i < Textures.Count; i++)
		{
			Textures[i].enabled = false;
			Textures[i].gameObject.GetComponent<GUIText>().enabled = false;
		}

		for (Int32 i = TextPosition; i < Texts.Count; i++)
			Texts[i].enabled = false;


		// Begin the next call
		TexturePosition = 0;
		TextPosition = 0;

		transform.localPosition = Vector3.zero;
		transform.localScale = Vector3.one;
		transform.localRotation = Quaternion.identity;
	}

    public static void spaceUnits(Vector3 position, Int32 commanderIdentifier)
    {
    	int groupCount = 6;
    	List<Fury.Behaviors.Unit> listSpacingUnit = new List<Fury.Behaviors.Unit>();
		Fury.Behaviors.Commander commander = Fury.Behaviors.Manager.Instance.Commanders[(commanderIdentifier == 0)? 0 : 1];
		/*
		foreach (Fury.Behaviors.Unit unit in commander.Units)
		{
			if((unit.agent is WalkableAgent || unit.agent is KYTowerAgent) && (unit.transform.position-position).magnitude < 6)
			{
				Debug.Log("SS");
				return;
				//groupCount = 3;
			}
		}	
		*/
		commander = Fury.Behaviors.Manager.Instance.Commanders[(commanderIdentifier == 1)? 0 : 1];
		//foreach (Fury.Behaviors.Unit unit in commander.Units)
		foreach (KeyValuePair<int, Fury.Behaviors.Unit> unitData in commander.Units)
		{		
			Fury.Behaviors.Unit unit = unitData.Value;
			
			WalkableAgent walkableAgent = unit.gameObject.GetComponent<WalkableAgent>();
			if(walkableAgent != null && walkableAgent.gameObject.tag != "harvester" && (unit.transform.position-position).magnitude < 15 && walkableAgent.unit.State != Fury.UnitStates.CastingAbility)
			{
				listSpacingUnit.Add(unit);
				walkableAgent.isSpacing = true;
			}
			if(listSpacingUnit.Count == groupCount)
			{
				listSpacingUnit[0].Owner.OrderGroupToPosition(listSpacingUnit, position);		
				listSpacingUnit.Clear();
			}
		}
		if(listSpacingUnit.Count > 0)
		{
			listSpacingUnit[0].Owner.OrderGroupToPosition(listSpacingUnit, position);		
		}
    }

	public static string abilityNameForUnitName(string abilityName)
	{
		return stringFromChar(abilityName, '_');
	}

	public static string stringFromChar(string str, char delimiter)
	{
		string str2 = "";
		bool isStart = false;
		foreach(char c in str)
		{
			if(isStart)
			{
				str2 += c;
			}
			if(c == delimiter)
			{
				isStart = true;
			}
		}
		return str2;
	}
	
    public static void consumeEngageTarget(Fury.Behaviors.Unit source)
    {
        KYController sourceController = source.gameObject.GetComponent<KYController>();
        if(sourceController.engageUnit)
        {
            KYController targetController = sourceController.engageUnit.gameObject.GetComponent<KYController>();
			sourceController.engageUnit.Order (targetController.transform.position);
		    sourceController.engageUnit = null;
            if(targetController.engageUnit == source)
            {
				if(targetController.nextEngageUnit)
				{
					targetController.gameObject.GetComponent<Fury.Behaviors.Unit>().Order (targetController.nextEngageUnit);
                	targetController.engageUnit = targetController.nextEngageUnit;
                	targetController.nextEngageUnit = null;
				}
            	else
            	{
               		targetController.engageUnit = null;
            	}
			}
			else
			{
				targetController.nextEngageUnit = null;
			}
        }
    }
    
    public static void attack(Fury.Behaviors.Unit source, Fury.Behaviors.Unit target)
    {
        KYController sourceController = source.gameObject.GetComponent<KYController>();
        KYController targetController = target.gameObject.GetComponent<KYController>();
        if(targetController.engageUnit && targetController.nextEngageUnit && targetController.engageUnit != source && targetController.nextEngageUnit != source)
        {
            return;
        }
        else if(targetController.engageUnit && targetController.engageUnit != source)
        {
            consumeEngageTarget(source);
            sourceController.engageUnit = target;
            sourceController.sss = 0;
            targetController.nextEngageUnit = source;
        }
        else
        {
            consumeEngageTarget(source);
            sourceController.engageUnit = target;
            sourceController.sss = 0;
            targetController.engageUnit = source;
        }
    }

	private GUITexture GetTexture()
	{
		if (TexturePosition >= Textures.Count)
		{
			var go = new GameObject();
			go.transform.parent = transform;
			go.transform.localPosition = new Vector3(0.5f, 0.5f, 0);
			go.transform.localScale = new Vector3(0, 0, 1);
			go.transform.localRotation = Quaternion.identity;

			Textures.Add(go.AddComponent<GUITexture>());
			go.AddComponent<GUIText>();
		}

		var g = Textures[TexturePosition];
		g.enabled = true;
		g.transform.localPosition = new Vector3(0.5f, 0.5f, TexturePosition);

		TexturePosition++;
		return g;
	}

	private GUIText GetText()
	{
		if (TextPosition >= Texts.Count)
		{
			var go = new GameObject();
			go.transform.parent = transform;
			go.transform.localPosition = new Vector3(0.5f, 0.5f, 0);
			go.transform.localScale = new Vector3(0, 0, 1);
			go.transform.localRotation = Quaternion.identity;

			Texts.Add(go.AddComponent<GUIText>());
		}

		var g = Texts[TextPosition];
		g.enabled = true;
		g.transform.localPosition = new Vector3(0.5f, 0.5f, TextPosition + TexturePosition);

		TextPosition++;
		return g;
	}

	public void DrawTexture(Texture2D tex, Rect r, Color col)
	{
		if (tex == null) return;

		var guiTexture = GetTexture();
		guiTexture.pixelInset = new Rect(r.x - Screen.width * 0.5f, Screen.height * 0.5f - r.height - r.y, r.width, r.height);
		guiTexture.texture = tex;
		guiTexture.color = col;
		GUIText text = guiTexture.gameObject.GetComponent<GUIText>();
		text.enabled = false;	
	}
	
	public void DrawTexture(Texture2D tex, Rect r, Color col, string str)
	{
		if (tex == null) return;

		var guiTexture = GetTexture();
		guiTexture.pixelInset = new Rect(r.x - Screen.width * 0.5f, Screen.height * 0.5f - r.height - r.y, r.width, r.height);
		guiTexture.texture = tex;
		guiTexture.color = col;
		GUIText text = guiTexture.gameObject.GetComponent<GUIText>();
		text.enabled = true;
		text.text = str;
		text.pixelOffset = new Vector2(r.x - Screen.width * 0.5f, Screen.height * 0.5f - r.height - r.y);
	}

	public void DrawText(String str, Single offsetX, Single offsetY)
	{
		if (str == null) return;

		var guiText = GetText();
		guiText.text = str;
		guiText.fontSize = 30;
		//guiText.anchor = anchor;
		guiText.alignment = TextAlignment.Center;
		//guiText.font = font;
		guiText.pixelOffset = new Vector2(offsetX - Screen.width * 0.5f, Screen.height * 0.5f - offsetY);
	}

	public void DrawText(String str, Single offsetX, Single offsetY, int fontSize)
	{
		if (str == null) return;

		var guiText = GetText();
		guiText.text = str;
		guiText.fontSize = fontSize;
		//guiText.anchor = anchor;
		guiText.alignment = TextAlignment.Center;
		//guiText.font = font;
		guiText.pixelOffset = new Vector2(offsetX - Screen.width * 0.5f, Screen.height * 0.5f - offsetY);
	}

	public void DrawBar(Texture2D border, Texture2D fill, Single x, Single y, Single w, Single h, Single p)
	{
		Rect bgRect = new Rect(x, y, w, h);
		Rect barRect = new Rect(x + 1, y + 1, (w - 2) * p, h - 2);

		var col = new Color(0.5f, 0.5f, 0.5f, 0.5f);

		if (border != null) DrawTexture(border, bgRect, col);

		DrawTexture(fill, barRect, col);
	}

	private void PlaceRing(GameObject ring, Single radius, Color col, Vector3 position)
	{
		ring.renderer.enabled = true;
		ring.transform.parent = null;
		ring.renderer.material.color = col;
		ring.transform.localRotation = Quaternion.identity;
		ring.transform.localScale = Vector3.one * radius * .2f;
		ring.transform.localPosition = position;
	}
	
	//place ring around unit
	private void PlaceRing(GameObject ring, Fury.Behaviors.Targetable follower)
	{
		ring.renderer.enabled = true;
		ring.transform.parent = follower.transform;
		ring.renderer.material.color = follower.IsTeamOrNeutral(Owner) ? new Color(0f, 0.5f, 0f) : new Color(1f, 0f, 0f);
		ring.transform.localPosition = Vector3.up * 0.01f;
		ring.transform.localRotation = Quaternion.AngleAxis(Time.timeSinceLevelLoad * 30, Vector3.up);
		ring.transform.localScale = new Vector3(1f / follower.transform.localScale.x,
			1f / follower.transform.localScale.y,
			1f / follower.transform.localScale.z) * follower.Radius * .2f;
	}
	
	//place arrow above unit
	private void PlaceArrow(GameObject ring, Fury.Behaviors.Targetable follower)
	{ 
		ring.renderer.enabled = true;
		ring.transform.parent = follower.transform;
		//ring.renderer.material.color = follower.IsTeamOrNeutral(Owner) ? new Color(0f, 0.5f, 0f) : new Color(1f, 0f, 0f);
		ring.transform.localPosition = Vector3.up * 5 + new Vector3(0,10,0);
		/*ring.transform.localRotation = Quaternion.AngleAxis(Time.timeSinceLevelLoad * 30, Vector3.up);
		ring.transform.localScale = new Vector3(1f / follower.transform.localScale.x,
			1f / follower.transform.localScale.y,
			1f / follower.transform.localScale.z) * follower.Radius * .2f;*/
	}

	/*private void OnDragComplete(Rect rect)
	{
		if (Hud.QueuedAbility != null) return;

		Hud.CalculateUnitsInRect(rect, BufferFollowers);

		if (!Hud.Shift) Selection.Clear();

		foreach (var f in BufferFollowers)
			if (f.Owner == Owner)
				Selection.Add(f);
	}*/

	private void OnFollowerDead(Fury.Behaviors.Unit target, Fury.Behaviors.Unit killer)
	{
		Selection.Remove(target);
	}

	private void OnFollowerSelected(Fury.Behaviors.Unit obj)
	{
		if (obj.name != "Crystal_Spire")
		{
			GameObject ring = (GameObject)GameObject.Instantiate(ArrowPrefab);
			ring.renderer.material = MaterialArrowSelected;
			ring.name = "Selection Ring";
			PlaceArrow(ring, obj);
		}
	}

	private void OnFollowerDeselected(Fury.Behaviors.Unit obj)
	{
		var ring = obj.transform.FindChild("Selection Ring");
		if (ring != null) GameObject.Destroy(ring.gameObject);
	}
}