﻿using UnityEngine;
using System.Collections;

public class loadcamera : MonoBehaviour 
{
	public void load(string name)
	{
		StartCoroutine(loadingLevel(name));
	}

	public IEnumerator loadingLevel(string name)
	{
		//yield return new WaitForSeconds(3);
		Application.LoadLevel(name);		
		yield return null;
	}
}
