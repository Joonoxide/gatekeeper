using UnityEngine;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
 
public class WaveManager : MonoBehaviour
{
	public class WaveWrapper
	{
		public float interval;
		public float intervalLeft;
		public float intervalCheat;
		public float intervalRandyLeft;
		public float time;
	}
	
	public WaveWrapper[] arrayWaveWrapper = new WaveWrapper[2];
	public int denominator = 10;
	public bool alarm = false;
	public List<KYAgent.SpawnWrapper>[] arraySpawnWrapper = new List<KYAgent.SpawnWrapper>[2];
	public static WaveManager instance;

    public virtual void Start ()
    {
    	instance = this;
    	GameController.instance.arrayPlayer[0].isPlayer = true;
    	GameController.instance.arrayPlayer[0].loadData();

		arrayWaveWrapper[1] = new WaveWrapper();
		arrayWaveWrapper[1].interval = 2;
		arrayWaveWrapper[1].intervalLeft = 0;
		arrayWaveWrapper[1].intervalRandyLeft = 15;
    }

    public void loadSpawnWrapper(int index)
    {
    	List<KYAgent.SpawnWrapper> array = new List<KYAgent.SpawnWrapper>();
    	TowerAgent2 agent2 = TowerAgent2.getInstanceForIndex(index);
    	if(!agent2 || agent2 == null || agent2.minionController == null)
    	{
    		return;
    	}
    	KYAgent.SpawnWrapper spawnWrapper;
    	foreach(KYAgent.AbilityWrapper abilityWrapper in agent2.listAbilityWrapper)
		{
			if(abilityWrapper is KYAgent.SpawnAbilityWrapper)
			{
				spawnWrapper = (abilityWrapper as KYAgent.SpawnAbilityWrapper).spawnWrapper;
				int level = LevelManager.instance.nameForLevel(spawnWrapper.unit.name);
				if(level > 0)
				{
					spawnWrapper.level = level;
					if(spawnWrapper.unit.name == "Harvester")
					{
						array.Insert(0, spawnWrapper);
					}
					else
					{
						array.Add(spawnWrapper);
					}
				}
			}
		}
		if(spawnNameForSpawnWrapper(array, "Harvester") == null)
		{
			array.Insert(0, new KYAgent.SpawnWrapper());
		}
		arraySpawnWrapper[index] = array;
    }

    public KYAgent.SpawnWrapper spawnNameForSpawnWrapper(List<KYAgent.SpawnWrapper> array, string name)
    {
    	foreach(KYAgent.SpawnWrapper spawnWrapper in array)
    	{
    		if(spawnWrapper.unit != null && spawnWrapper.unit.name.Contains(name))
    		{
    			return spawnWrapper;
    		}
    	}
    	return null;
    }

    public void updateAIPlayerIndex(int playerIndex)
    {
    	//Debug.Log(arraySpawnWrapper.Count);
    	WaveWrapper waveWrapper = arrayWaveWrapper[playerIndex]; 
    	Player player = GameController.instance.arrayPlayer[playerIndex];
		TowerAgent agent = TowerAgent.getInstanceForIndex(playerIndex);
		TowerAgent2 agent2 = TowerAgent2.getInstanceForIndex(playerIndex);
		TowerAgent3 agent3 = TowerAgent3.getInstanceForIndex(playerIndex);
		TowerAgent2 agent2Opponent = TowerAgent2.getInstanceForIndex((playerIndex == 0)? 1 : 0);
		List<KYAgent.SpawnWrapper> array = new List<KYAgent.SpawnWrapper>();
		
		string str = "";
		foreach(KYAgent.SpawnWrapper s in arraySpawnWrapper[playerIndex])
		{
			if((s.unit.trigger <= ResourceManager.instance.fallen || arraySpawnWrapper[playerIndex].Count <= 3) && agent2.minionController.getCount(s.unit.name) < s.unit.maximum)
			{
				array.Add(s);
			}
			else
			{
				str += s.unit.name+":"+s.unit.trigger+":"+ResourceManager.instance.fallen;
			}
		}

		KYAgent.SpawnWrapper spawnWrapper;
		if(!agent2)
		{
			return;
		}
		if(playerIndex == 1)
		{
			player.amountGold += Time.deltaTime*player.incomeRate;
			//Debug.Log(player.incomeRate+":"+Time.deltaTime+":"+Time.deltaTime*player.incomeRate);
			
		}
		waveWrapper.intervalLeft -= Time.deltaTime;
		//waveWrapper.intervalCheat -= Time.deltaTime;
		waveWrapper.intervalRandyLeft -= Time.deltaTime;
		waveWrapper.time += Time.deltaTime;

		/*
		if(player.incomeRate < 0.4f)
		{
			int rand = UnityEngine.Random.Range(0, 5000);
			if(rand == 1)
			{
				player.incomeRate += 0.05f;
			}	
			Debug.Log(player.incomeRate+":"+rand);	
		}
		*/
		int index;
		int posZ = 0;
		if(agent2.arrayNearbyEnemyAgent.Count > 0)
		{
			posZ = (int)TowerAgent2.instance2.transform.position.z;
			if(agent2.unit.Controllers.VitalityController.HealthPercentage < 0.5f)
			{
				alarm = true;
			}
		}
		else if(agent3 != null && agent3.arrayNearbyEnemyAgent.Count > 0)
		{
			posZ = (int)agent3.transform.position.z;
			if(agent3.unit.Controllers.VitalityController.HealthPercentage < 0.5f)
			{
				alarm = true;
			}
		}
		else if(agent != null && agent.arrayNearbyEnemyAgent.Count > 0)
		{
			posZ = (int)agent.transform.position.z;
			if(agent.unit.Controllers.VitalityController.HealthPercentage < 0.5f)
			{
				alarm = true;
			}
		}
		/*
		if(alarm && waveWrapper.intervalCheat <= 0)
		{
			spawnWrapper = spawnNameForSpawnWrapper(array, "Swordman");
			if(spawnWrapper != null)
			{
				//int star = UnityEngine.Random.Range(1, 4);
				int star = 1;
				agent2.minionController.CreateSpawn(spawnWrapper.unit, star, new Vector3(LevelManager.instance.rightX-2f, 0, posZ), 107);
			}
			waveWrapper.intervalCheat = 5000;
		}
		*/


		if(waveWrapper.intervalRandyLeft <= 0 && agent2.minionController.listHarvesterIdentifier.Count == LevelManager.instance.harvesterCount && array.Count > 1)
		{
			float posX = (playerIndex == 0)? LevelManager.instance.leftX-1f : LevelManager.instance.rightX+1f;
			posZ = UnityEngine.Random.Range(-25, 25);
			
			List<KYAgent.SpawnWrapper> unitsToSpawn = new List<KYAgent.SpawnWrapper>();
			Vector3 spawnLocation = new Vector3(posX + 2f, 0, posZ);
			
			index = UnityEngine.Random.Range(1, array.Count);
			spawnWrapper = array[index];
			if(spawnWrapper.cost < player.amountGold && !spawnWrapper.unit.name.Contains("Priest") && !spawnWrapper.unit.name.Contains("Crusader"))
			{
				spawnWrapper.incrementCount();
				player.amountGold -= spawnWrapper.cost;
				
				//agent2.minionController.CreateSpawn (spawnWrapper.unit, spawnWrapper.count, new Vector3(posX, 0, posZ), spawnWrapper.level);
				
				for (int i = 0; i < spawnWrapper.count; i++)
				{
					unitsToSpawn.Add(spawnWrapper);
				}
				
				agent2.minionController.SpawnSquad(unitsToSpawn, spawnLocation);
				
				spawnWrapper.resetCount();
				waveWrapper.intervalRandyLeft = 15;
			}
			else
			{
				waveWrapper.intervalRandyLeft = 30;
			}
		}



		if(waveWrapper.intervalLeft <= 0)
		{
			waveWrapper.intervalLeft = waveWrapper.interval;
			if(player.amountGold > 5f)
			{
				int ky = UnityEngine.Random.Range(0, denominator);
				float posX = (playerIndex == 0)? LevelManager.instance.leftX+2f : LevelManager.instance.rightX-2f;
				int count = -1;
				bool isHarvesting = false;
				if(spawnNameForSpawnWrapper(array, "Harvester") != null && spawnNameForSpawnWrapper(array, "Harvester").count+agent2.minionController.listHarvesterIdentifier.Count == 0 && ResourceManager.instance.listResourceIdentifier.Count >= 1 && player.amountGold >= 10)
				{
					spawnWrapper = spawnNameForSpawnWrapper(array, "Harvester");
					spawnWrapper.count = LevelManager.instance.harvesterCount;
					player.amountGold -= spawnWrapper.cost*2;
					if(player.amountGold < 0)
					{
						player.amountGold = 0;
					}
					spawnWrapper = spawnNameForSpawnWrapper(array, "Crusader");
					if(spawnWrapper != null)
					{
						spawnWrapper.count = 1;
						player.amountGold -= spawnWrapper.cost;	
						if(player.amountGold < 0)
						{
							player.amountGold = 0;
						}
					}
					else
					{
						spawnWrapper = spawnNameForSpawnWrapper(array, "Swordman");
						if(spawnWrapper != null)
						{
							spawnWrapper.count = 1;
							player.amountGold -= spawnWrapper.cost;	
							if(player.amountGold < 0)
							{
								player.amountGold = 0;
							}									
						}
					}

					Fury.Behaviors.Manager manager = Fury.Behaviors.Manager.Instance;
					Fury.Behaviors.Unit unitResource = null, nearestUnit = null;
					float minDist = 99999, dist;
					foreach(Int32 identifier in ResourceManager.instance.listResourceIdentifier)
					{
						unitResource = manager.Find<Fury.Behaviors.Unit>(identifier);
						if(unitResource)
						{
							dist = Mathf.Abs(unitResource.transform.position.x-agent2.transform.position.x);
							if(dist < minDist)
							{
								nearestUnit = unitResource;
								minDist = dist;
							}
						}
					}
					isHarvesting = true;
					posZ = (int)nearestUnit.transform.position.z;

				}
				else if(agent2 != null && agent2.arrayNearbyEnemyAgent.Count > 0)
				{
					posZ = (int)TowerAgent2.instance2.transform.position.z;
				}
				else if(agent3 != null && agent3.arrayNearbyEnemyAgent.Count > 0)
				{
					posZ = (int)agent3.transform.position.z;
				}
				else if(agent != null && agent.arrayNearbyEnemyAgent.Count > 0)
				{
					posZ = (int)agent.transform.position.z;
				}
				else
				{
					posZ = UnityEngine.Random.Range(-25, 25);
				}
				//spawning.
				List<KYAgent.SpawnWrapper> array2 = new List<KYAgent.SpawnWrapper>(array);
				if(count == 0 || player.amountGold > 15f || alarm)
				{
					while(true && array2.Count > 1)
					{
						if(isHarvesting)
						{
							break;
						}
						index = UnityEngine.Random.Range(1, array2.Count);
						spawnWrapper = array2[index];
						int reservedGold = 0;
						if(count >= 0)
						{
							reservedGold = (3-count)*5;
						}
						if(spawnWrapper.cost < player.amountGold && (reservedGold < player.amountGold || alarm))
						{
							spawnWrapper.incrementCount();
							//spawnAbilityWrapper.spawnWrapper.count = 1;
							player.amountGold -= spawnWrapper.cost;
							if(agent2.minionController.getCount(spawnWrapper.unit.name)+spawnWrapper.count >= spawnWrapper.unit.maximum)
							{
								array2.Remove(spawnWrapper);
							}

						}
						else
						{
							break;
						}
					}
				}
				
				List<KYAgent.SpawnWrapper> unitsToSpawn = new List<KYAgent.SpawnWrapper>();
				Vector3 spawnLocation = new Vector3(posX + 2f, 0, posZ);
				
				foreach(var abilityWrapper in agent2.listAbilityWrapper)
				{
					if(abilityWrapper.abilityType == AbilityType.Summon)
					{
						spawnWrapper = (abilityWrapper as TowerAgent2.SpawnAbilityWrapper).spawnWrapper;
						/*
						if(spawnWrapper.unit.name.Contains("Griffin"))
						{
							spawnWrapper.count = 2;
						}
						*/
						if(spawnWrapper.count > 0  )
						{							
							//agent2.minionController.CreateSpawn (spawnWrapper.unit, spawnWrapper.count, new Vector3(posX, 0, posZ), spawnWrapper.level);
							for (int i = 0; i < spawnWrapper.count; i++)
							{
								unitsToSpawn.Add(spawnWrapper);
							}
							
							abilityWrapper.cooldown = spawnWrapper.count*abilityWrapper.fixedCooldown;
							abilityWrapper.cooldownLeft = abilityWrapper.cooldown;
							spawnWrapper.resetCount();
						}		
					}
					else if(abilityWrapper.cooldownLeft <= 0)
					{
						(abilityWrapper.ability as AISkillInterface).aiExecute(agent2, null);
					}
				}
				
				MinionController minionController = agent2.minionController;
					
				minionController.SpawnSquad(unitsToSpawn, spawnLocation);
				
			}
		}
		alarm = false;
    }

	private void Update()
	{
		updateAIPlayerIndex(1);
	}	
}