using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class GuardController : MonoBehaviour 
{
	public Fury.Database.Unit SpawnType = null;
	
	public Int32 SpawnNum = 3;
	public Single Radius = 10;
	public Single ChaseDistance = 20f;
	public Single AggroDistance = 10f;
	public Single RespawnFrequency = 10f; 
	
	private Single DecisionTimer = 1;
	public Single DecisionFrequency = 1f;
	
	public UnityEngine.GameObject SummonEffect = null;
	public UnityEngine.GameObject DeathEffect = null;
	
	public List<Int32> Guards = new List<Int32>();
	
	private void Start ()
	{
		//Fury.Behaviors.Manager.Instance.OnMapLoaded += OnMapLoaded;
		Fury.Behaviors.Manager.Instance.OnUnitDead += new Fury.General.DUnitDead(OnUnitDead);
		StartCoroutine("InitialSpawn");
		
	}

	private void Update()
	{	
		if (Guards == null) return;

		DecisionTimer -= Time.deltaTime;

		if (DecisionTimer < 0)
		{
			foreach (var id in Guards)
			{
				var guard = Fury.Behaviors.Manager.Instance.Find<Fury.Behaviors.Unit>(id);

				if (guard != null)
				{
					if (!guard.gameObject.GetComponent<KYController>().engageUnit && guard.State == Fury.UnitStates.Idle)
					{
						foreach (var commander in Fury.Behaviors.Manager.Instance.Commanders)
                        {
							if (commander != guard.Owner)
							{
								Fury.Behaviors.Unit blek = null;
								//foreach (var target in commander.Units)
								foreach(KeyValuePair<int, Fury.Behaviors.Unit> unitData in commander.Units)
								{
									Fury.Behaviors.Unit target = unitData.Value;
									
									if (Vector3.Distance(target.transform.position, guard.transform.position) < AggroDistance)
									{
                                        if(!target.gameObject.GetComponent<KYController>().engageUnit)
                                        {
                                            SimpleHUD.attack(guard, target);
											blek = null;
                                            break;
                                        }
										else if(!target.gameObject.GetComponent<KYController>().nextEngageUnit)
										{
											blek = target;
										}
									}
								}
								if(blek)
								{
									SimpleHUD.attack (guard, blek);
								}
							}
                        }
					}

					if (!guard.gameObject.GetComponent<KYController>().engageUnit && Vector3.Distance(guard.transform.position, transform.position) > ChaseDistance)
					{
						guard.Order(GetRandomPosition());
					}
				}
			}

			DecisionTimer = DecisionFrequency;
		}
	}

	private void OnUnitDead(Fury.Behaviors.Unit deadUnit, Fury.Behaviors.Unit lastAttacker)
	{
		if (Guards.Exists(i => i == deadUnit.Identifier))
		{
			Guards.Remove(deadUnit.Identifier);
			
            SimpleHUD.consumeEngageTarget(deadUnit);
			var deadAnim = (GameObject)
							GameObject.Instantiate(DeathEffect);
							var effPos = deadUnit.transform.position;
			deadAnim.transform.position = effPos;
			
			StartCoroutine("QueueSpawn");
			StartCoroutine("CleanSpawn", deadUnit.gameObject);
		}
	}

	/*private void OnMapLoaded()
	{
		Guards = new List<Int32>();
	}*/
	
	private IEnumerator InitialSpawn()
	{
		while (Guards.Count < SpawnNum)
		{
			yield return new WaitForSeconds(RespawnFrequency);
			CreateSpawn();
		}
	}
	
	private IEnumerator CleanSpawn(System.Object gameObject)
	{
		yield return new WaitForSeconds(RespawnFrequency / 25);
		
		if (gameObject != null)
			GameObject.Destroy(gameObject as GameObject);
	}

	private IEnumerator QueueSpawn()
	{
		yield return new WaitForSeconds(RespawnFrequency);

		CreateSpawn();
	}

	public void CreateSpawn()
	{
			var position = GetRandomPosition();
			
			var createdSpawn = Fury.Behaviors.Manager.Instance.CreateUnit(SpawnType,
				Fury.Behaviors.Manager.Instance.Commanders.First(c => c.Index == Fury.CommanderIndices.One),
				position, null);
			
			Guards.Add(createdSpawn.Identifier);
			
			if (SummonEffect != null)
			{
				// Create the effect
				var effect = (GameObject)
					GameObject.Instantiate(SummonEffect);
				
				effect.transform.position = position;
			}
	}

	public Vector3 GetRandomPosition()
	{
		return transform.position + new Vector3(UnityEngine.Random.Range(-Radius, Radius), 0, UnityEngine.Random.Range(-Radius, Radius));
	}
	
	private void OnDrawGizmos()
	{
		Gizmos.color = new Color(0.8f, 0.8f, 0.8f); 
		Gizmos.DrawWireCube(transform.position, new Vector3(Radius * 2, 1, Radius * 2));
	}
	
	public void DestroySelf()
	{
		if (Guards != null)
		{
			foreach (var id in Guards) 
			{
				var guard = Fury.Behaviors.Manager.Instance.Find<Fury.Behaviors.Unit>(id);
				
				//Guards.Remove(guard.Identifier);
				
				if (guard.gameObject != null)
					GameObject.Destroy(guard.gameObject as GameObject);
			}
		}
	}
}
