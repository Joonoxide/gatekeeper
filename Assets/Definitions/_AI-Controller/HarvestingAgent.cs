using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class HarvestingAgent : WalkableAgent 
{
	public enum HarvesterState { Seeking, OTWG , Harvesting, Finishing, OTWB, Dead };
	public HarvesterState harvesterState = HarvesterState.Seeking;
	private float minXDistance = 5f;
	public int bag = 0;
	public int chopCount = 0;
	// Use this for initialization
	public override void Start () 
	{
		base.Start ();
		level = GameController.instance.arrayPlayer[unit.Owner.Identifier].harvesterLevel;
		unit.AddStatus(GameController.instance.levelBuff, unit);	
	}
	
	// Update is called once per frame
	public override void Update () 
	{
		if (unit && GameController.instance.isEnd != 10)
		{
			if(!isBah)
			{
				unit.Controllers.VitalityController.Health = unit.Controllers.VitalityController.MaxHealth;
				isBah = true;
			}
			
            if(unit.Controllers.VitalityController.HealthPercentage < 1 && unit.Controllers.VitalityController.Health > 0 && HPBar)
			{
				HPBar.gameObject.SetActive(true);
				var vitCtrl = unit.Controllers.VitalityController;
				float currentHP = vitCtrl.Health;
				float maxHP = vitCtrl.MaxHealth;
				HPBar.percent = currentHP/maxHP;
			}

			if(unit.State == Fury.UnitStates.Idle)
			{
				sprite.DoAnim(0);
			}
			if(unit.State == Fury.UnitStates.MovingToPosition || unit.State == Fury.UnitStates.MovingToTarget)
			{

				if(harvesterState == HarvesterState.OTWB && isMoving())
				{
					(sprite as SpineSpriteWrapper).DoAnim("move_wood");
				}
				else if(isMoving())
				{
					sprite.DoAnim (1);
				}
				else
				{
					sprite.DoAnim(0);
				}
			}
			else if(unit.State == Fury.UnitStates.AttackingUnit)
			{
				if (isMoving())
				{
					sprite.DoAnim(1);
				}
				else if(isWeaponAttacking())
				{
					sprite.DoAnim (2);
				}
				else
				{
					sprite.DoAnim(0);
				}
			}
			
			if (unit.Controllers.MovementController.TargetUnit)
			{
				if (unit.transform.position.x > unit.Controllers.MovementController.TargetUnit.transform.position.x)
					{sprite.setWinding((int)SpriteRoot.WINDING_ORDER.CW);}
				else
					{sprite.setWinding((int)SpriteRoot.WINDING_ORDER.CCW);}
			}
			else if (unit.Controllers.MovementController.TargetPosition.HasValue)
			{
				if (unit.transform.position.x > unit.Controllers.MovementController.TargetPosition.Value.x)
					{sprite.setWinding((int)SpriteRoot.WINDING_ORDER.CW);}
				else
					{sprite.setWinding((int)SpriteRoot.WINDING_ORDER.CCW);}
			}
			//logic.
			if(harvesterState == HarvesterState.Seeking && !isSpacing)
			{
				if(unit.Controllers.WeaponController.State != Fury.Controllers.WeaponController.States.PreDelay && unit.Controllers.WeaponController.State != Fury.Controllers.WeaponController.States.PostDelay)
				{
					Fury.Behaviors.Manager manager = Fury.Behaviors.Manager.Instance;
					Fury.Behaviors.Unit unitResource = null, nearestUnit = null;
					float minDist = 99999, dist;
					foreach(Int32 identifier in ResourceManager.instance.listResourceIdentifier)
					{
						unitResource = manager.Find<Fury.Behaviors.Unit>(identifier);
						if(unitResource)
						{
							dist = (unitResource.transform.position-transform.position).magnitude;
							if(dist < minDist)
							{
								nearestUnit = unitResource;
								minDist = dist;
							}
						}

					}
					if(nearestUnit != null && nearestUnit != unit.Controllers.WeaponController.Target)
					{
						unit.Order(nearestUnit);
					}	
					else if(nearestUnit == null && unit.State == Fury.UnitStates.Idle )
					{
						movementTarget = transform.position;
						if(unit.Owner.Identifier == 0)
						{
							//movementTarget.x = LevelManager.instance.rightX;
						}
						else
						{
							//movementTarget.x = LevelManager.instance.leftX;
						}

						unit.Order(movementTarget);
					}

				}
				else
				{
					harvesterState = HarvesterState.Harvesting;
				}
			}
			else if(harvesterState == HarvesterState.Harvesting)
			{
				if(chopCount == 3)
				{
					harvesterState = HarvesterState.Finishing;
				}
				else if(unit.State == Fury.UnitStates.Idle)
				{
					harvesterState = HarvesterState.Seeking;
				}
			}
			else if(harvesterState == HarvesterState.Finishing)
			{
				if(unit.Owner.Identifier == 0)
				{
					movementTarget = TowerAgent2.instance1.transform.position;
					movementTarget.x = LevelManager.instance.leftX+5;
				}
				else
				{
					movementTarget = TowerAgent2.instance2.transform.position;
					movementTarget.x = LevelManager.instance.rightX-5;
				}
				unit.Order (movementTarget);
				harvesterState = HarvesterState.OTWB;
			}
			else if(harvesterState == HarvesterState.OTWB)
			{
				if(unit.State == Fury.UnitStates.Idle)
				{
					harvesterState = HarvesterState.Finishing;
				}
				if((unit.transform.position-movementTarget).magnitude < 3)
				{
					unit.ModifyHealth(-999999999, null, GameController.instance.source);
					Vector3 effPos = transform.position;
					effPos.y = 5;
					GameObject newTextGO = Instantiate (GameController.instance.textGO) as GameObject;
					newTextGO.transform.position = effPos;
					newTextGO.GetComponent<SpriteText>().text = "+"+bag;

					GameController.instance.arrayPlayer[unit.Owner.Identifier].amountGold += bag;
					harvesterState = HarvesterState.Dead;
					transform.Find("sprite_harvester").GetComponent<MeshRenderer>().enabled = false;
				}
			}
			if(unit.State == Fury.UnitStates.MovingToPosition || unit.State == Fury.UnitStates.MovingToTarget)
			{
				if(prevPosition == transform.position)
				{
					++stuckCount;
					if(stuckCount > 20)
					{
						unit.Order(transform.position);
					}
				}
				else
				{
					stuckCount = 0;
				}
			}
			else
			{
				stuckCount = 0;
			}
			prevPosition = transform.position;
		}
		if(GameController.instance.isEnd == 10)
		{
			//unit.Order(transform.position);
			gameObject.SetActive(false);
		}
		if(!unit)
		{
			HPBar.gameObject.SetActive(false);
			sprite.DoAnim("dead", false);
			SkeletonAnimation skeletonAnimation = (sprite as SpineSpriteWrapper).sprite;
			if(skeletonAnimation.state.Time > 1)
			{
				skeletonAnimation.transform.parent = null;
				skeletonAnimation.gameObject.AddComponent<Fadeout>();
				Destroy(gameObject);
			}
		}
	}
}
