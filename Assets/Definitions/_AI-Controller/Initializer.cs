using UnityEngine;
using System.Collections;
using System;
public class Initializer : MonoBehaviour {

	public Fury.Database.Unit crystal;
	public Fury.Database.Unit crystal3;
	public Fury.Database.Unit crystal2;
	static public Initializer instance;
	void Start () 
	{
		instance = this;
		
		Fury.Behaviors.Manager.Instance.OnMapLoaded += OnMapLoaded;
		
		//OnMapLoaded();
		
		Debug.Log (Fury.Behaviors.Manager.Instance.Commanders.Count);
	}
	
	void OnMapLoaded()
	{
		float leftX = LevelManager.instance.leftX+6;
		float rightX = -35-6;
		rightX += LevelManager.instance.rightX-(-35);
		leftX -= 1;
		rightX += 1;

		CreateSpawn(crystal, new Vector3(leftX, 0, 25), 0);
		CreateSpawn(crystal, new Vector3(rightX, 0, 25), 1);
		CreateSpawn(crystal3, new Vector3(leftX, 0, -25), 0);
		CreateSpawn(crystal3, new Vector3(rightX, 0, -25), 1);
		CreateSpawn(crystal2, new Vector3(leftX, 0, 2.5f), 0);
		CreateSpawn(crystal2, new Vector3(rightX, 0, 2.5f), 1);
		
		//Fury.Behaviors.Manager.Instance.CreateAICommander((Fury.CommanderIndices)2, new byte[1]);
		gameObject.AddComponent<ResourceManager>();
		if(LevelManager.instance.type == 0)
		{
			gameObject.AddComponent<WaveManager>();
		}
		else if(LevelManager.instance.type == 1)
		{
			gameObject.AddComponent<WWaveManager>();
		}
		gameObject.AddComponent<LKYHUD>();
		
		Fury.Behaviors.Manager.Instance.OnMapLoaded -= OnMapLoaded;
		Destroy (this);
	}
	
	public void CreateSpawn(Fury.Database.Unit spawnType, Vector3 position, int commanderFlag)
	{		
		Fury.Behaviors.Commander commander;
		if(commanderFlag == 0)
		{
			commander = Fury.Behaviors.Manager.Instance.Commanders.First (c => c.Index == Fury.CommanderIndices.One);
		}
		else
		{
			commander = Fury.Behaviors.Manager.Instance.Commanders.First (c => c.Index == Fury.CommanderIndices.Two);
		}
			
		Debug.Log (Fury.Behaviors.Manager.Instance.CreateUnit(spawnType, commander,
				position, null));
	}
}
