	

    Shader "HPBar"
    {
            Properties
            {
                    _Green("Green", float) = 1
                    _Red("Red", float) = 0
            }
            SubShader
            {
                    Blend SrcAlpha OneMinusSrcAlpha
                    Pass
                    {
                            CGPROGRAM
                            #pragma target 3.0
                            // Upgrade NOTE: excluded shader from OpenGL ES 2.0 because it does not contain a surface program or both vertex and fragment programs.
                            #pragma vertex vert
                            #pragma fragment frag
                            #include "UnityCG.cginc"
     
                            struct v2f
                            {
                            float4 pos : SV_POSITION;
                                    float3 color : COLOR0;
                                    float4 texCoord  : TEXCOORD0;
                            };
                   
                            struct a2v
                            {
                                    float4 vertex   : POSITION;
                                    float4 color    : COLOR;
                                    float4 texcoord : TEXCOORD0;
                            };
                           
                            float _Green;
                           
                            v2f vert (a2v In)
                            {
                            v2f o;
                            o.pos = mul (UNITY_MATRIX_MVP, In.vertex);
                                    o.texCoord  = In.texcoord ;
                            return o;
                            }
                           
                            struct output
                            {
                                    float4 col : COLOR;
                            };
                           
                            output frag (float4 color : COLOR, float4 texcoord : TEXCOORD0)
                            {
                                    output o2;
                                    if(1-texcoord[0] < _Green)
                                    {
                                            o2.col = float4(0, 1, 0, 1);
                                    }
                                    else
                                    {
                                            o2.col = float4(1, 0, 0, 1);
                                    }
                                    return o2;
                                           
                            }
                            ENDCG
                    }
            }
    }

