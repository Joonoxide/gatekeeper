using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LevelAsset : Fury.Database.Definition 
{
	public int levelNum;
	public int userInitialGold;
	public int initialGold;
	public float leftX;
	public float rightX;
	public string unit;
	public int harvesterCount;
	public int type;
	public string ban1 = "";
	public string ban2 = "";
	public string ban3 = "";
	public bool loseAnyTower1 = false;
	public bool loseAnyTower2 = false;
	public bool loseAnyTower3 = false;
	public bool isDestroyTower1 = false;
	public bool isDestroyTower2 = false;
	public bool isDestroyTower3 = false;
	public int killHarvester1 = -1;
	public int killHarvester2 = -1;
	public int killHarvester3 = -1;
	public int deadHarvester1 = -1;
	public int deadHarvester2 = -1;
	public int deadHarvester3 = -1;
	public int enemyLevel = 1;
	public int useGold1 = 0;
	public int useGold2 = 0;
	public int useGold3 = 0;
	
	[SerializeField]
	private float playerSummonAreaWidth = 0;
	
	[SerializeField]
	private float enemySummonAreaWidth = 0;
	
	//public int tower1HP = 1000;
	//public int tower2HP = 1000;
	//public int tower3HP = 1000;
	//public int exp = 100;
	
	public float getSummonAreaWidth(Allegiance side)
	{
		if (side.Equals(Allegiance.Player))
		{
			return playerSummonAreaWidth;
		}
		
		else
		{
			return enemySummonAreaWidth;
		}
	}
	
	public void setSummonAreaWidth(Allegiance side, float val)
	{
		if (side.Equals(Allegiance.Player))
		{
			playerSummonAreaWidth = val;
		}
		
		else
		{
			enemySummonAreaWidth = val;
		}
	}
}
