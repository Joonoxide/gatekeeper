﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(LevelAsset))]
public class LevelAssetCustomEditor : Editor {

	public override void OnInspectorGUI ()
	{
		LevelAsset activeAsset = target as LevelAsset;
		
		DrawDefaultInspector();
		
		EditorGUILayout.Space();
		
		EditorGUILayout.BeginVertical();
		
		EditorGUILayout.LabelField("Player Settings");
		
		EditorGUILayout.BeginHorizontal();
		
		EditorGUILayout.LabelField("Bounds reference", GUILayout.Width(100));
		Object objectToUpdatePlayer = EditorGUILayout.ObjectField(null, typeof(GameObject)); 
		
		EditorGUILayout.EndHorizontal();
		EditorGUILayout.HelpBox("Drag and drop an object to automatically update the summon area bounds", MessageType.Info);
		
		if (objectToUpdatePlayer)
		{
			activeAsset.setSummonAreaWidth(Allegiance.Player, (objectToUpdatePlayer as GameObject).renderer.bounds.size.x);
		}
		
		EditorGUILayout.LabelField("Width", activeAsset.getSummonAreaWidth(Allegiance.Player).ToString());
		
		EditorGUILayout.Space();
		
		EditorGUILayout.LabelField("Enemy Settings");
		
		EditorGUILayout.BeginHorizontal();
		
		EditorGUILayout.LabelField("Bounds reference", GUILayout.Width(100));
		Object objectToUpdateEnemy = EditorGUILayout.ObjectField(null, typeof(GameObject)); 
		
		EditorGUILayout.EndHorizontal();
		EditorGUILayout.HelpBox("Drag and drop an object to automatically update the summon area bounds", MessageType.Info);
		
		if (objectToUpdateEnemy)
		{
			activeAsset.setSummonAreaWidth(Allegiance.Enemy, (objectToUpdateEnemy as GameObject).renderer.bounds.size.x);
		}
		
		EditorGUILayout.LabelField("Width", activeAsset.getSummonAreaWidth(Allegiance.Enemy).ToString());
		
		EditorGUILayout.EndVertical();
		
		EditorUtility.SetDirty(activeAsset);
	}
}
