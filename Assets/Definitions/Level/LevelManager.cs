using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class LevelManager 
{
	public class SpawnWrapper
	{
		public string name;
		public int level;
	}

	public float leftX;
	public float rightX;
	public float center;
	public List<SpawnWrapper> arraySpawnWrapper;
	public int initialGold;
	public int userInitialGold;
	public static LevelManager instance;
	public int harvesterCount;
	public int level; 
	public int type; 
	public int enemyLevel;
	public int tower1HP;
	public int tower2HP;
	public int tower3HP;
	public int exp;
	
	private float [] summonZonesWidth = new float[2];

	public LevelManager(int index)
	{
		LevelAsset levelAsset = Resources.Load("LevelAsset"+index) as LevelAsset;
		level = index;
		leftX = 31;
		rightX = -40.5f;

		center = (leftX+rightX)/2;
		initialGold = levelAsset.initialGold;
		userInitialGold = levelAsset.userInitialGold;
		harvesterCount = levelAsset.harvesterCount;
		type = levelAsset.type;

		string[] strs = levelAsset.unit.Split(',');
		LevelManager.SpawnWrapper spawn;
		arraySpawnWrapper = new List<LevelManager.SpawnWrapper>();
		foreach(string str in strs)	
		{
			if(!str.Contains("-"))
			{
				break;
			}
			string[] ss = str.Split('-');
			spawn = new LevelManager.SpawnWrapper();
			spawn.name = ss[0];
			spawn.level = Convert.ToInt16(ss[1]);
			arraySpawnWrapper.Add(spawn);
		}
		enemyLevel = levelAsset.enemyLevel;
		
		tower1HP = getSupportTowerHP(enemyLevel);
		tower2HP = getGateHP(enemyLevel);
		tower3HP = getSupportTowerHP(enemyLevel);
		
		summonZonesWidth[(int)Allegiance.Player] = levelAsset.getSummonAreaWidth(Allegiance.Player);
		summonZonesWidth[(int)Allegiance.Enemy] = levelAsset.getSummonAreaWidth(Allegiance.Enemy);
	}

	public static string[] getBan(int level, int index)
	{
		LevelAsset levelAsset = Resources.Load("LevelAsset"+level) as LevelAsset;	
		string str;
		if(index == 1)
		{
			str = levelAsset.ban1;
		}
		else if(index == 2)
		{
			str = levelAsset.ban2;
		}
		else 
		{
			str = levelAsset.ban3;
		}
		string[] strs = str.Split(',');
		return strs;
	}

	public static void createInstance(int index)
	{
		instance = new LevelManager(index);
	}

	public int nameForLevel(string name)
	{
		foreach(SpawnWrapper spawn in arraySpawnWrapper)
		{
			if(spawn.name == name)
			{
				return spawn.level;
			}
		}
		return 0;
	}
	
	int getGateHP(int level)
	{
		return (2 + (2 * level)) * GameSettings.TOWER_HP_BASE_MAIN;
	}
	
	int getSupportTowerHP(int level)
	{
		return (2 + (2 * level)) * GameSettings.TOWER_HP_BASE_SUPPORT;
	}
	
	public float getSummonZoneWidth(Allegiance side)
	{
		return summonZonesWidth[(int)side];
	}
}
