using UnityEngine;
using System.Collections;

public class HPBar : MonoBehaviour {
    GameObject greenGO;
    GameObject redGO;
    public float percent = 0;
    float oldPercent = 0;

    void Awake()
    {
        greenGO = transform.Find("Green").gameObject;
        redGO = transform.Find("Red").gameObject;  
        greenGO.renderer.enabled = false;
        redGO.renderer.enabled = false;    
    }
    void Start ()
    {
        greenGO.renderer.enabled = true;
        redGO.renderer.enabled = true;    
        Vector3 pos = greenGO.transform.localPosition;
        pos.y -= 1f;
        greenGO.transform.localPosition = pos;
        greenGO.renderer.material.color = Color.green;
        redGO.renderer.material.color = Color.red;
        redGO.transform.localScale = new Vector3(1, 1, 1);
        greenGO.transform.localScale = new Vector3(percent, 1, 1);
        greenGO.transform.localPosition = new Vector3(-0.5f+percent/2.0f, 1,  -1);
        greenGO.transform.localScale = new Vector3(1, 1, 1);
    }
    
    // Update is called once per frame

    void OnEnable()
    {
        transform.up = Vector3.up;
        Vector3 eulerAngles = transform.localEulerAngles;
        eulerAngles.x = -45;
        transform.localEulerAngles = eulerAngles;
    }

    void Update () 
    {
        if(percent != oldPercent)
        {
            greenGO.transform.localScale = new Vector3(percent, 1, 1);
            greenGO.transform.localPosition = new Vector3(-0.5f+percent/2.0f, 1,  0);
        }
        oldPercent = percent;
    }
}
