﻿using UnityEngine;
using System.Collections;
using System;

public class PushNotificationHandler : MonoBehaviour {
	
#if UNITY_IPHONE
	
	LocalNotification notification_lastPlayed_24H, notification_everyday_10PM, notification_Sunday_any;
	bool isCleared = false;
	
	string alert1, alert2, alert3;
	
	void Awake()
	{
		DontDestroyOnLoad(this);
	}
	
	// Use this for initialization
	void Start () 
	{	
		clearPreviousSchedules();
		updateAlertBodies();
	}
	
	void OnApplicationPause(bool isPaused)
	{
		if (isPaused)
		{
			Debug.Log ("Paused!");
			
			if (isCleared)
			{
				applyDynamicNotifications();
				applyStaticNotifications();
			}
			
		}
		
		else
		{
			Debug.Log ("Unpaused!");
			clearPreviousSchedules();
		}
	}
	
	void OnApplicationQuit()
	{
		clearPreviousSchedules();
		
		if (isCleared)
		{
			applyStaticNotifications();
			applyDynamicNotifications();
		}
	}
	
	void updateAlertBodies()
	{
		alert1 = Language.Get(PushLanguageField.PUSH_NOTIFICATION_1.ToString());
		alert2 = Language.Get(PushLanguageField.PUSH_NOTIFICATION_2.ToString());
		alert3 = Language.Get(PushLanguageField.PUSH_NOTIFICATION_3.ToString());
	}
	
	void clearPreviousSchedules()
	{
		LocalNotification [] notifications = NotificationServices.scheduledLocalNotifications;
		
		/*for (int i = 0; i < notifications.Length; i++)
		{
			Debug.Log ("Clearing..." + notifications[i].alertBody + " with sound of " + notifications[i].soundName);
		}*/
		
		NotificationServices.ClearLocalNotifications();
		NotificationServices.CancelAllLocalNotifications();
		
		isCleared = true;
	}
	
	void applyStaticNotifications()
	{
		//push at 10 pm everyday
		notification_everyday_10PM = new LocalNotification();
		notification_everyday_10PM.soundName = LocalNotification.defaultSoundName;
		notification_everyday_10PM.alertBody = alert1;
		notification_everyday_10PM.repeatInterval = CalendarUnit.Day;
		
		notification_everyday_10PM.fireDate = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day, 22, 0, 0, DateTimeKind.Local); // testing, need to change back to 10pm:  22H
		
		//already passed for the day
		if (DateTime.Now.Hour >= 22)
		{
			//fire tomorrow, same time
			notification_everyday_10PM.fireDate.AddDays(1);
		}
		
		//push on sunday
		notification_Sunday_any = new LocalNotification();
		notification_Sunday_any.soundName = LocalNotification.defaultSoundName;
		notification_Sunday_any.alertBody = alert2;
		notification_Sunday_any.repeatInterval = CalendarUnit.Week;
		
		DateTime time_2PM = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day, 14, 0, 0, DateTimeKind.Local);
		
		//already passed for the day
		if (DateTime.Now.Hour >= 14)
		{
			//fire tomorrow, same time
			time_2PM.AddDays(1);
		}
		
		notification_Sunday_any.fireDate = Next(time_2PM, DayOfWeek.Sunday);
		
		NotificationServices.ScheduleLocalNotification(notification_everyday_10PM);
		NotificationServices.ScheduleLocalNotification(notification_Sunday_any);
	}
	
	void applyDynamicNotifications()
	{
		//push every 24 hours from the time of last play
		notification_lastPlayed_24H = new LocalNotification();
		notification_lastPlayed_24H.soundName = LocalNotification.defaultSoundName;
		notification_lastPlayed_24H.alertBody = alert3;
		notification_lastPlayed_24H.repeatInterval = CalendarUnit.Day;
		
		notification_lastPlayed_24H.fireDate = System.DateTime.Now.AddHours(24.0);
		
		NotificationServices.ScheduleLocalNotification(notification_lastPlayed_24H);
	}
	
	DateTime Next(DateTime from, DayOfWeek dayOfWeek)
    {
        int start = (int)from.DayOfWeek;
        int target = (int)dayOfWeek;
        if (target <= start)
            target += 7;
        return from.AddDays(target - start);
	}
	
	/*void Update()
	{
		if (NotificationServices.localNotificationCount > 0)
		{
			Debug.Log (NotificationServices.localNotifications[0].alertBody);
			NotificationServices.ClearLocalNotifications();
		}
	}*/
#endif
}
