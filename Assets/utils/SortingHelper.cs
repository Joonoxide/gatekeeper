﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using KYData;

public enum SortType
{
	Level,
	Cost,
	Type
}

class TrooperTypeComparer: IComparer<TrooperWrapper>
{
	public int Compare(TrooperWrapper t1, TrooperWrapper t2)
	{
		//place bookmarked units first
		int compareBookmark = t2.isBookmarked.CompareTo(t1.isBookmarked);
		
		//if both are the same
		if (compareBookmark == 0)
		{
			//if not bookmarked
			if (!t1.isBookmarked)
			{
				return t1.typeIndex.CompareTo(t2.typeIndex);
			}
		}
		
		return compareBookmark;
	}
}

class TrooperLevelComparer: IComparer<TrooperWrapper>
{
	public int Compare(TrooperWrapper t1, TrooperWrapper t2)
	{
		int compareBookmark = t2.isBookmarked.CompareTo(t1.isBookmarked);
		
		//if both are the same
		if (compareBookmark == 0)
		{
			//if not bookmarked
			if (!t1.isBookmarked)
			{
				return t2.level.CompareTo(t1.level);
			}
		}
		
		return compareBookmark;
	}
}

class TrooperCostComparer: IComparer<TrooperWrapper>
{
	public int Compare(TrooperWrapper t1, TrooperWrapper t2)
	{
		int compareBookmark = t2.isBookmarked.CompareTo(t1.isBookmarked);
		
		//if both are the same
		if (compareBookmark == 0)
		{
			//if not bookmarked
			if (!t1.isBookmarked)
			{
				return t1.cost.CompareTo(t2.cost);
			}
		}
		
		return compareBookmark;
	}
}

public static class SortingHelper {
	
	/// <summary>
	/// Sorts TrooperWrapper list according to a given SortType. Bookmarked units are always first
	/// </summary>
	/// <returns>
	/// The sorted list
	/// </returns>
	/// <param name='unitList'>
	/// Unit list.
	/// </param>
	/// <param name='sortType'>
	/// Sort type.
	/// </param>
	public static List<TrooperWrapper> sortBy(List<TrooperWrapper> unitList, SortType sortType)
	{	
		if (sortType == SortType.Level)
		{
			unitList.Sort(new TrooperLevelComparer());
			
			return unitList;
			//return unitList.OrderByDescending(unit => unit.isBookmarked).ThenBy(unit => unit.level).ToList();
		}
		
		else if (sortType == SortType.Type)
		{
			unitList.Sort(new TrooperTypeComparer());
			
			return unitList;
			
			//return unitList.OrderByDescending(unit => unit.isBookmarked).ThenBy(unit => unit.typeIndex).ToList();
		}
		
		else
		{
			unitList.Sort(new TrooperCostComparer());
			
			return unitList;
			
			//return unitList.OrderByDescending(unit => unit.isBookmarked).ThenBy(unit => unit.cost).ToList();
		}
		
	}
	
}
