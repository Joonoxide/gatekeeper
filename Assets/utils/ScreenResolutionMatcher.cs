﻿using UnityEngine;
using System.Collections;

public class ScreenResolutionMatcher : MonoBehaviour {
	
	[SerializeField]
	private Sprite vignetteMask;
	
	private Camera cam;
	
	void Awake () 
	{
		cam = GetComponent<Camera>();
		
		cam.orthographicSize = Screen.height * 0.5f;
		
		vignetteMask.width = Screen.width;
		vignetteMask.height = Screen.height;
		
		DontDestroyOnLoad(this.gameObject);
	}
	
	void OnLevelWasLoaded()
	{
		if (Application.loadedLevelName.Contains("level"))
		{
			//cam.gameObject.SetActive(false);
			vignetteMask.renderer.enabled = false;
		}
		
		else
		{
			//cam.gameObject.SetActive(true);
			vignetteMask.renderer.enabled = true;
		}
	}
	
}
