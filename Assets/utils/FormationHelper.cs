﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum FormationFacing
{
	Left,
	Right
}

public static class FormationHelper {
	
	public static List<Vector3> generateFormationCoordinates(int numberOfUnits, Vector3 origin, FormationFacing facing, float spacingBetweenUnits)
	{
		List<Vector3> result = new List<Vector3>();
		
		float spacing = spacingBetweenUnits;
		
		int count = numberOfUnits;
		
		int maxRows = getMaxRow(count, spacing, facing == FormationFacing.Right? Allegiance.Player: Allegiance.Enemy);
		
		int activeColumn = 0;
		
		int extraUnits = count % maxRows;
		
		float offsetToCenterY = (maxRows - 1) * spacing * 0.5f;
		
		for (int i = 0; i < count; i++)
		{
			int yPos = i % maxRows;
			
			if (i >= maxRows * (activeColumn+1))
			{
				activeColumn++;
			}
			
			if (i >= count - extraUnits)
			{
				int numExtraSlots = maxRows - extraUnits;
				float offsetY = numExtraSlots * spacing * 0.5f;
				
				Vector3 pos = new Vector3(activeColumn * spacing * (facing == FormationFacing.Right? -1: 1), 0, yPos * spacing + offsetY - offsetToCenterY);
				pos += origin;
				result.Add(pos);
				//units[i].transform.position = pos;
			}
			
			else
			{
				Vector3 pos = new Vector3(activeColumn * spacing * (facing == FormationFacing.Right? -1: 1), 0, yPos * spacing - offsetToCenterY);
				pos += origin;
				result.Add(pos);
				//units[i].transform.position = pos;
			}
		}
		
		return result;
	}
	
	private static int getMaxRow(int count, float spacing, Allegiance side)
	{
		int div = 3;
		
		if (count < 4)
		{
			div = 2;
		}
		
		else if (count < 10)
		{
			div = 3;
		}
		
		else if (count < 15)
		{
			div = 4;
		}
		
		else if (count < 20)
		{
			div = 5;
		}
		
		else if (count < 30)
		{
			div = 7;
		}
		
		else
		{
			div = 9;
		}
		
		
		//check for potential overrides (from narrow spawn zones)
		int defaultCol = (count / div) + Mathf.Clamp(count % div, 0, 1);
		float maxPermissibleWidth = LevelManager.instance.getSummonZoneWidth(side);
		
		//run only if a width is available (0 means it has no limits)
		if (maxPermissibleWidth > 0)
		{
			float maxPersmissibleCol = 1 + (int)(maxPermissibleWidth / spacing);
		
			//if default cols exceed the max permissible cols,
			//override default value
			if (defaultCol > maxPersmissibleCol)
			{
				div = Mathf.CeilToInt(count / maxPersmissibleCol);
			}
		}
		
		return div;
	}
	
}
