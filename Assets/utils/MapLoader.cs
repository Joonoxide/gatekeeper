﻿using UnityEngine;
using System.Collections;

public class MapLoader : MonoBehaviour {
	
	public static MapLoader instance {private set; get;}
	
	private float loadTimer = 0f;
	private static int currentLoadedLevel = 0;
	private const float LOAD_TIMEOUT = 5f;
	
	// Use this for initialization
	void Awake () 
	{
		if (instance != null)
		{
			Debug.LogWarning("Only one MapLoader can be initialized at a time. Self destructing!");
			GameObject.Destroy(gameObject);
			return;
		}
		
		instance = this;
		
		GameObject.DontDestroyOnLoad(instance.gameObject);
		
	}
	
	public void UnloadMap()
	{
		Debug.Log ("unloading level " + currentLoadedLevel + "...");
		
		Fury.Behaviors.Manager.Instance.Disconnect();
		
		loadTimer = 0;
	}
	
	public void LoadMap(int level)
	{
		Debug.Log ("loading level " + level + "...");
		
		LevelManager.createInstance(level);
		
		if (Fury.Behaviors.Manager.Instance.GameState == Fury.GameStates.Menu)
		{
			loadTimer = 0;
			currentLoadedLevel = Mathf.Max(1, Mathf.Min(level, Fury.Behaviors.Manager.Instance.AvailableMaps.Length));
			
			Fury.Behaviors.Manager.Instance.Host(7000, Fury.Behaviors.Manager.Instance.AvailableMaps[level - 1], "FuryTest.MapHosting", "", false, null);
		
			StartCoroutine(loadHandler());
		}
		
	}
	
	public void ReloadMap()
	{
		Debug.Log ("reloading level " + currentLoadedLevel + "...");
		
		Fury.Behaviors.Manager.Instance.Disconnect();
		
		loadTimer = 0;
		
		StartCoroutine(rehostHandler());
	}
	
	IEnumerator loadHandler()
	{
		//map hosted successfully
		if (Fury.Behaviors.Manager.Instance.GameState == Fury.GameStates.Lobby)
		{
			Debug.Log ("Map successfully hosted...");
			PopulateRequiredCommanders();
			
			Debug.Log (Fury.Behaviors.Manager.Instance.Commanders.Count + " " + Fury.Behaviors.Manager.Instance.Commanders[1]);
			
			GameController.LoadMap(Fury.Behaviors.Manager.Instance.Map);
			
			yield break;
		}
		
		//attempt to wait until the game is hosted
		else
		{
			loadTimer += Time.deltaTime;
			
			//abort if too long has passed and still unsuccessful
			if (loadTimer >= LOAD_TIMEOUT)
			{
				Debug.LogWarning("[MapLoader] Timed out! Map failed to load!");
				yield break;
			}
			
			//continue waiting 
			else
			{
				yield return new WaitForSeconds(0.5f);
			}
			
		}
	}
	
	IEnumerator rehostHandler()
	{
		//map disconnected successfully
		if (Fury.Behaviors.Manager.Instance.GameState == Fury.GameStates.Menu)
		{
			Debug.Log ("Map successfully disconnected...");
			
			Debug.Log ("Rehosting map...");
			
			Fury.Behaviors.Manager.Instance.Host(7000, Fury.Behaviors.Manager.Instance.AvailableMaps[currentLoadedLevel - 1], "FuryTest.MapHosting", "", false, null);
			
			StartCoroutine(loadHandler());
			
			yield break;
		}
		
		//attempt to wait until the game is hosted
		else
		{
			loadTimer += Time.deltaTime;
			
			//abort if too long has passed and still unsuccessful
			if (loadTimer >= LOAD_TIMEOUT)
			{
				Debug.LogWarning("[MapLoader] Timed out! Map failed to disconnect!");
				yield break;
			}
			
			//continue waiting 
			else
			{
				yield return new WaitForSeconds(0.5f);
			}
			
		}
	}
	
	void PopulateRequiredCommanders()
	{
		Fury.Behaviors.Manager.Instance.CreateAICommander(Fury.CommanderIndices.Two, new byte[1]);
		Fury.Behaviors.Manager.Instance.CreateAICommander(Fury.CommanderIndices.Three, new byte[1]);
	}
}
