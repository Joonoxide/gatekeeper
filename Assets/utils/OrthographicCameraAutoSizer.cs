﻿using UnityEngine;
using System.Collections;

public class OrthographicCameraAutoSizer : MonoBehaviour {
	
	//this class is made specifically to adjust the size of the camera 
	//to always fit the map from side to side
	public float mapWidth = 0f;
	private Camera cam;
	
	void Awake()
	{
		cam = GetComponent<Camera>();
		
		float aspectRatio = (float)Screen.width / (float)Screen.height;
		
		cam.orthographicSize = (mapWidth / aspectRatio) * 0.5f;
	}
}
