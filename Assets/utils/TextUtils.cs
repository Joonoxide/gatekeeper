﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class TextUtils {
	
	public static int GetDisplayLineCount(string input)
	{
		return input.Split('\n').Length;
	}
	
	public static string ResolveTextBySize(string input, float charSize, float width)
	{
		string result = "";
		int numPerLine = Mathf.FloorToInt(width / charSize);
		
#if LANGUAGE_JA
		
		char [] characters = input.ToCharArray();
		List<string> lines = new List<string>();
		string currentLine = "";
		string exceptionBuffer = "";
		int indexFirstException = -1;
		
		for (int i = 0; i < characters.Length; i++)
		{
			if (isContinuous(characters[i]))
			{
				if (exceptionBuffer.Length == 0)
				{
					indexFirstException = currentLine.Length;
				}
				
				exceptionBuffer += characters[i];
			}
			
			else
			{
				if (currentLine.Length + exceptionBuffer.Length >= numPerLine)
				{
					if (exceptionBuffer.Length > 0)
					{	
						int excessLength = Mathf.Abs(numPerLine - (currentLine.Length + exceptionBuffer.Length));
						
						if (excessLength > 0)
						{
							if (!char.IsDigit(exceptionBuffer[0]))
							{
								currentLine = currentLine.Insert(indexFirstException, exceptionBuffer[0].ToString());
								exceptionBuffer = exceptionBuffer.Remove(0, 1);
							}
							
							lines.Add(currentLine);
							currentLine = exceptionBuffer;
							exceptionBuffer = "";
						}
						
						else
						{
							currentLine = currentLine.Insert(indexFirstException, exceptionBuffer);
							
							lines.Add(currentLine);
							currentLine = "";
							exceptionBuffer = "";
						}
						
					}
					
					else
					{
						lines.Add(currentLine);
						currentLine = "";
						exceptionBuffer = "";
					}
					
				}
				
				else
				{
					if (exceptionBuffer.Length > 0)
					{
						currentLine = currentLine.Insert(indexFirstException, exceptionBuffer);
						exceptionBuffer = "";
					}
					
				}
			
				currentLine += characters[i];
			}
			
		}
		
		if (currentLine.Length + exceptionBuffer.Length >= numPerLine)
		{
			if (exceptionBuffer.Length > 0)
			{	
				int excessLength = Mathf.Abs(numPerLine - (currentLine.Length + exceptionBuffer.Length));
				
				if (excessLength > 0)
				{
					if (!char.IsDigit(exceptionBuffer[0]))
					{
						currentLine = currentLine.Insert(indexFirstException, exceptionBuffer[0].ToString());
						exceptionBuffer = exceptionBuffer.Remove(0, 1);
					}
					
					lines.Add(currentLine);
					currentLine = exceptionBuffer;
				}
				
				else
				{
					currentLine = currentLine.Insert(indexFirstException, exceptionBuffer);
					
					lines.Add(currentLine);
					currentLine = "";
				}
				
			}
			
			else
			{
				lines.Add(currentLine);
				currentLine = "";
			}
			
		}
		
		else
		{
			if (exceptionBuffer.Length > 0)
			{
				currentLine = currentLine.Insert(indexFirstException, exceptionBuffer);
				exceptionBuffer = "";
			}
		}
		
		if (currentLine.Length > 0)
		{
			lines.Add(currentLine);
		}
		
		result = string.Join("\n", lines.ToArray());
		
#endif
		
#if LANGUAGE_EN
		//Split string by char " "    
		string[] words = input.Split(' ');
		
		List<string> lines = new List<string>();
		string currentWord = "";
		string currentLine = "";
		
		for (int i = 0; i < words.Length; i++)
		{
			currentWord = words[i];
			
			//if the current line length exceeds the maximum permissible width
			//or the next line will exceed
			if (currentLine.Length > numPerLine ||
				currentLine.Length + currentWord.Length > numPerLine
				)
			{
				lines.Add(currentLine);
				currentLine = "";
			}
			
			if (currentLine.Length > 0)
			{
				currentLine += " " + currentWord;
			}
			
			else
			{
				currentLine += currentWord;
			}
		}
		
		if (currentLine.Length > 0)
		{
			lines.Add(currentLine);
		}
		
		result = string.Join("\n", lines.ToArray());
	 
#endif
		
		return result;
	}
	
	static bool isContinuous(char character)
	{
		//if not digit, fullstop, comma, or %
		if (
			!(char.IsDigit(character)) &&
			!((int)character).Equals(12290) &&
			!((int)character).Equals(12289) &&
			!((int)character).Equals(65285) &&
			/*!((int)character).Equals(12289) &&*/
			!((int)character).Equals(65281) &&
			!((int)character).Equals(65288) &&
			!((int)character).Equals(65289) &&
			!(character.Equals('%')) &&
			!(character.Equals('m'))
			)
		{
			return false;
		}
		
		else
		{
			return true;
		}
	}
	
}
