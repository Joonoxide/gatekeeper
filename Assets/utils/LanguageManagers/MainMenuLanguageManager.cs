﻿using UnityEngine;
using System.Collections;
using KYData;

public class MainMenuLanguageManager : LanguageManager {
	
	[SerializeField]
	private TextMesh label_harvester, label_shop, label_summon, label_powerup, label_barracks, label_upgrade;
	
	[SerializeField]
	private TextMesh label_restore;
	
	[SerializeField]
	private TextMesh harvester_3rd, harvester_4th, harvester_5th, harvester_6th, harvester_7th, harvester_all;
	
	[SerializeField]
	private TextMesh frag_1, frag_6, frag_12, frag_30, frag_60, frag_85;
	
	[SerializeField]
	private TextMesh mission_header, mission_conditionHeader, mission_objectives, label_battle;
	
	private string format_stage, format_unitBan, format_harvesterLost, format_harvesterKilled, format_goldSpent;
	
	// Use this for initialization
	void Start () 
	{
		//initialise generic formatted strings
		format_stage = Language.Get(MissionLanguageField.MISSION_STAGE.ToString());
		format_unitBan = Language.Get(MissionLanguageField.MISSION_OBJECTIVE_BAN.ToString());
		format_harvesterLost = Language.Get (MissionLanguageField.MISSION_OBJECTIVE_HARVESTER_LOST.ToString());
		format_harvesterKilled = Language.Get(MissionLanguageField.MISSION_OBJECTIVE_HARVESTER_KILL.ToString());
		format_goldSpent = Language.Get (MissionLanguageField.MISSION_OBJECTIVE_GOLD_SPENT.ToString());
		
		//gui buttons (bottom panel)
		label_harvester.text = Language.Get (GenericLanguageField.MAIN_MENU_NAV_HARVESTER.ToString()).ToUpper();
		label_shop.text = Language.Get (GenericLanguageField.MAIN_MENU_NAV_SHOP.ToString()).ToUpper();
		label_summon.text = Language.Get (GenericLanguageField.MAIN_MENU_NAV_SUMMON.ToString()).ToUpper();
		label_powerup.text = Language.Get (GenericLanguageField.MAIN_MENU_NAV_POWERUP.ToString()).ToUpper();
		label_barracks.text = Language.Get (GenericLanguageField.MAIN_MENU_NAV_BARRACKS.ToString()).ToUpper();
		label_upgrade.text = Language.Get (GenericLanguageField.MAIN_MENU_NAV_UPGRADE.ToString()).ToUpper();
		label_restore.text = Language.Get(ShopLanguageField.SHOP_RESTORE_PURCHASES.ToString());
		
		float maxWidth = 1f;
									
#if LANGUAGE_JA
		maxWidth = 0.5f;
#endif
		
		//harvester purchase list
		harvester_3rd.text = Language.Get(ShopLanguageField.HARVESTER_THIRD.ToString());
		harvester_4th.text = Language.Get(ShopLanguageField.HARVESTER_FOURTH.ToString());
		harvester_5th.text = Language.Get(ShopLanguageField.HARVESTER_FIFTH.ToString());
		harvester_6th.text = Language.Get(ShopLanguageField.HARVESTER_SIXTH.ToString());
		harvester_7th.text = Language.Get(ShopLanguageField.HARVESTER_SEVENTH.ToString());
		harvester_all.text = Language.Get(ShopLanguageField.HARVESTER_ALL.ToString());
		
		harvester_3rd.text = TextUtils.ResolveTextBySize(harvester_3rd.text, harvester_3rd.characterSize, maxWidth);
		harvester_4th.text = TextUtils.ResolveTextBySize(harvester_4th.text, harvester_4th.characterSize, maxWidth);
		harvester_5th.text = TextUtils.ResolveTextBySize(harvester_5th.text, harvester_5th.characterSize, maxWidth);
		harvester_6th.text = TextUtils.ResolveTextBySize(harvester_6th.text, harvester_6th.characterSize, maxWidth);
		harvester_7th.text = TextUtils.ResolveTextBySize(harvester_7th.text, harvester_7th.characterSize, maxWidth);
		harvester_all.text = TextUtils.ResolveTextBySize(harvester_all.text, harvester_all.characterSize, maxWidth);
		
		//fragment + xp purchase list
		
		string frag_singular = Language.Get(ShopLanguageField.SHOP_FRAGMENT_SINGULAR.ToString());
		string frag_plural = Language.Get (ShopLanguageField.SHOP_FRAGMENT_PLURAL.ToString());
		
		frag_1.text = string.Format(frag_singular, 1);
		frag_6.text = string.Format(frag_plural, 6);
		frag_12.text = string.Format(frag_plural, 12);
		frag_30.text = string.Format(frag_plural, 30);
		frag_60.text = string.Format(frag_plural, 60);
		frag_85.text = string.Format(frag_plural, 85);
		
		//mission
		mission_conditionHeader.text = "- " + Language.Get(MissionLanguageField.MISSION_TO_EARN_STAR.ToString()) + " -";
		
		label_battle.text = Language.Get(MissionLanguageField.MISSION_BATTLE.ToString()).ToUpper();
	}
	
	public void updateMissionLevel(int level)
	{
		mission_header.text = string.Format(format_stage, level);
	}
	
	public void updateMissionObjectives(int level, int starSelected)
	{	
		mission_header.text = string.Format(format_stage, level);
		
		string [] bansRaw = LevelManager.getBan(level, starSelected);
		
		LevelAsset data = Resources.Load("LevelAsset"+level) as LevelAsset;
		
		////////////
		//Update objectives
		
		bool loseAnyTower, isDestroyTower;
		int killHarvester, deadHarvester, useGold;
		
		if(starSelected == 1)
		{
			loseAnyTower = data.loseAnyTower1;
			isDestroyTower = data.isDestroyTower1;
			killHarvester =  Mathf.Max(0, data.killHarvester1);
			deadHarvester = Mathf.Max(0, data.deadHarvester1);
			useGold = data.useGold1;
		}
		else if(starSelected == 2)
		{
			loseAnyTower = data.loseAnyTower2;
			isDestroyTower = data.isDestroyTower2;
			killHarvester =  Mathf.Max(0, data.killHarvester2);
			deadHarvester = Mathf.Max(0, data.deadHarvester2);
			useGold = data.useGold2;
		}
		else
		{
			loseAnyTower = data.loseAnyTower3;
			isDestroyTower = data.isDestroyTower3;
			killHarvester =  Mathf.Max(0, data.killHarvester3);
			deadHarvester = Mathf.Max(0, data.deadHarvester3);
			useGold = data.useGold3;
		}
		
		//Win the game
		string ban = Language.Get(MissionLanguageField.MISSION_OBJECTIVE_WIN.ToString()) + "\n";
		
		//Disallowed units
		if (bansRaw.Length > 0)
		{
			string buffer = "";
			
			for (int i = 0; i < bansRaw.Length; i++)
			{	
				if (bansRaw[i] == "")
				{
					continue;
				}
				
				//not last unit
				if (i < bansRaw.Length - 1)
				{
					buffer += Language.Get("UNIT_NAME_" + bansRaw[i].ToUpper());
					
					if (bansRaw.Length > 2)
					{
						buffer += ", ";
					}
				}
				
				else
				{
					if (bansRaw.Length == 1)
					{
						buffer += Language.Get("UNIT_NAME_" + bansRaw[i].ToUpper());
					}
					
					else
					{
						buffer += " & " + Language.Get("UNIT_NAME_" + bansRaw[i].ToUpper());
					}
					
				}
			}
			
			if (buffer.Length > 0)
			{
				ban += string.Format(format_unitBan, buffer) + "\n";
			}
			
		}
		
		if(loseAnyTower)
		{
			ban += Language.Get (MissionLanguageField.MISSION_OBJECTIVE_TOWER_LOST.ToString()) + "\n";
		}
		
		if(isDestroyTower)
		{
			ban += Language.Get (MissionLanguageField.MISSION_OBJECTIVE_TOWER_DESTROY.ToString()) + "\n";
		}
		
		if(killHarvester > 0)
		{
			ban += string.Format(format_harvesterKilled, killHarvester) + "\n";
		}
		
		if(deadHarvester > 0)
		{
			ban += string.Format(format_harvesterLost, deadHarvester) + "\n";
		}
		
		if(useGold > 0)
		{
			ban += string.Format(format_goldSpent, useGold);			
		}
		
		mission_objectives.text = ban;
	}
}
