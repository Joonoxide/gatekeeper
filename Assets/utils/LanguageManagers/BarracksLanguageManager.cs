﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using KYData;

public class BarracksLanguageManager : LanguageManager {

	[SerializeField]
	private TextMesh title, label_nextLevel, label_gotoFusion, unitName, unitDescription, unitAbility;
	
	void Start () 
	{
		title.text = Language.Get(GenericLanguageField.MAIN_MENU_NAV_BARRACKS.ToString()).ToUpper();
		label_nextLevel.text = Language.Get(GenericLanguageField.BARRACKS_NEXT_LEVEL.ToString());
		label_gotoFusion.text = Language.Get (GenericLanguageField.MAIN_MENU_NAV_POWERUP.ToString()).ToUpper();
	}
	
	public void updateFields(TrooperWrapper trooperWrapper)
	{
		string name = (trooperWrapper.name.Split('_'))[1];
		
		/////////////////////
		///Update unit name and sizing accompanying background
		
		unitName.text = Language.Get("UNIT_NAME_" + name.ToUpper());
		
		//scale the background behind the name to fit the length of the name
		Transform bg_name = unitName.transform.FindChild("bg_name");
		
		if (bg_name)
		{
			Vector3 bgScale = bg_name.localScale;
			bgScale.x = Mathf.Max(2.5f, unitName.renderer.bounds.size.x + 0.3f); //cap the narrowest at 2.5f (tested in scene)
			
			bg_name.localScale = bgScale;
		}
		
		
		////////////////////
		///Update unit description
		
		float maxWidth = 5f;
		float offsetY = 0.4f;
		
#if LANGUAGE_JA
		maxWidth = 2.5f;
		offsetY = 0.45f;
#endif
		
		string description = Language.Get("UNIT_DESCRIPTION_" + name.ToUpper());
		
		unitDescription.text = TextUtils.ResolveTextBySize(description, unitDescription.characterSize, maxWidth);
		
		///////////////////
		///Update unit ability and offset position based on length of description (above)
		
		string ability = Language.Get ("UNIT_ABILITY_" + name.ToUpper());
		
		//replace format placeholders with appropriate values
		UnitVariableAbilityWrapper variableAbilityData = KYDataManager.instance.getVariableAbilityWrapper(trooperWrapper);
		List<int> variables = variableAbilityData.getVariableByRank((int)(trooperWrapper.level - 1) / 10);
		
		List<object> vars = new List<object>();
		
		for (int i = 0; i < variables.Count; i++)
		{
			vars.Add(variables[i]);
		}
		
		ability = string.Format(ability, vars.ToArray());
		
		unitAbility.text = TextUtils.ResolveTextBySize(ability, unitDescription.characterSize, maxWidth);
		
		Vector3 abilityPos = unitAbility.transform.localPosition;
		abilityPos.y = -TextUtils.GetDisplayLineCount(unitDescription.text) * offsetY;
		
		unitAbility.transform.localPosition = abilityPos;
	}
}
