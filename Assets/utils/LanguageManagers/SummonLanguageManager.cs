﻿using UnityEngine;
using System.Collections;

public class SummonLanguageManager : LanguageManager {
	
	[SerializeField]
	private TextMesh title, label_gotoPowerUp, label_gotoShop, label_summonOptionHeader, label_pandoraBox, fragment_desc_big, fragment_desc_small, xp_desc_big, xp_desc_small, error_moreGeneric;
	
	[SerializeField]
	private SpriteText label_frag_free, label_frag_1, label_frag_6, label_xp_1, label_xp_6;
	
	void Start () 
	{
		////settings for main gui
		float maxWidth = 2.18f;
		float lineSpacing = 1.0f;
		float offsetY = 0.35f;
		
#if LANGUAGE_JA
		maxWidth = 1.3f;
		offsetY = 0.4f;
		lineSpacing = 1.0f;
#endif
		
		title.text = Language.Get (GenericLanguageField.MAIN_MENU_NAV_SUMMON.ToString()).ToUpper();
		label_gotoPowerUp.text = Language.Get(GenericLanguageField.MAIN_MENU_NAV_POWERUP.ToString()).ToUpper();
		label_gotoShop.text = Language.Get (GenericLanguageField.MAIN_MENU_NAV_SHOP.ToString()).ToUpper();
		
		label_summonOptionHeader.text = Language.Get (GenericLanguageField.MAIN_MENU_NAV_SUMMON.ToString()).ToUpper();
		
		label_pandoraBox.text = Language.Get (SummonLanguageField.SUMMON_PANDORAS_BOX.ToString());
		
		string desc_frag = Language.Get (SummonLanguageField.SUMMON_FRAGMENT_DESCRIPTION_BIG.ToString());
		string desc_xp = Language.Get (SummonLanguageField.SUMMON_XP_DESCRIPTION_BIG.ToString());
		
		fragment_desc_big.lineSpacing = lineSpacing;
		xp_desc_big.lineSpacing = lineSpacing;
		
		fragment_desc_big.text = TextUtils.ResolveTextBySize(desc_frag, fragment_desc_big.characterSize, maxWidth);
		xp_desc_big.text = TextUtils.ResolveTextBySize(desc_xp, xp_desc_big.characterSize, maxWidth);
			
		desc_frag = Language.Get(SummonLanguageField.SUMMON_FRAGMENT_DESCRIPTION_SMALL.ToString());
		desc_xp = Language.Get (SummonLanguageField.SUMMON_XP_DESCRIPTION_SMALL.ToString());
		
		fragment_desc_small.text = TextUtils.ResolveTextBySize(desc_frag, fragment_desc_small.characterSize, maxWidth);
		xp_desc_small.text = TextUtils.ResolveTextBySize(desc_xp, xp_desc_small.characterSize, maxWidth);
		
		Vector3 abilityPos = fragment_desc_big.transform.localPosition;
		abilityPos.y += -TextUtils.GetDisplayLineCount(fragment_desc_big.text) * offsetY;
		
		fragment_desc_small.transform.localPosition = abilityPos;
		
		abilityPos = xp_desc_big.transform.localPosition;
		abilityPos.y += -TextUtils.GetDisplayLineCount(xp_desc_big.text) * offsetY;
		
		xp_desc_small.transform.localPosition = abilityPos;
		
		////settings for error panel
		maxWidth = 2f;
									
#if LANGUAGE_JA
		maxWidth = 1f;
#endif
		string error = Language.Get(ErrorLanguageField.ERROR_GET_MORE_GENERIC.ToString());
		
		error_moreGeneric.text = TextUtils.ResolveTextBySize(error, error_moreGeneric.characterSize, maxWidth);
		
		////settings for buttons
		maxWidth = 2.51f;
									
#if LANGUAGE_JA
		maxWidth = 0;
#endif
		
		string format_plural, format_singular;
		
		format_plural = Language.Get(SummonLanguageField.SUMMON_TIMES_PLURAL.ToString()).ToUpper();
		format_singular = Language.Get(SummonLanguageField.SUMMON_TIMES_SINGULAR.ToString()).ToUpper();
		
		label_frag_free.maxWidth = maxWidth;
		label_frag_free.Text = Language.Get(SummonLanguageField.SUMMON_FREE_DAILY.ToString()).ToUpper();
		
		label_frag_free.CalcSize();
		
		label_frag_1.Text = string.Format(format_singular, 1);
		label_frag_6.Text = string.Format(format_plural, 6);
		
		label_xp_1.Text = string.Format(format_singular, 1);
		label_xp_6.Text = string.Format(format_plural, 6);
		
	}
}
