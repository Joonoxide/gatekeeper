﻿using UnityEngine;
using System.Collections;

public class TitleLanguageManager : LanguageManager {

	[SerializeField]
	private TextMesh title;
	
	[SerializeField]
	SkeletonAnimation logo;
	
	void Start () 
	{
		title.text = Language.Get(GenericLanguageField.TITLE_TAP_TO_START.ToString());
		
		logo.useAnimationName = true;
		
#if LANGUAGE_EN
		logo.animationName = "rotate";
#elif LANGUAGE_JA
		logo.animationName = "rotate_jp";
#endif
	}
}
