﻿using UnityEngine;
using System.Collections;

public class SettingsLanguageManager : LanguageManager {
	
	[SerializeField]
	private TextMesh title, header_credits;
	
	void Start () 
	{	
		title.text = Language.Get(GenericLanguageField.SETTINGS_TITLE.ToString()).ToUpper();
		header_credits.text = Language.Get(GenericLanguageField.SETTINGS_MUSIC_SOUND.ToString());
	}
}
