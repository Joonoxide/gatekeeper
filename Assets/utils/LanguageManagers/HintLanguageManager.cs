﻿using UnityEngine;
using System.Collections;
using System;

public class HintLanguageManager : LanguageManager {

	[SerializeField]
	private TextMesh hint;
	
	public void generateRandomHint()
	{
		float maxWidth = 80f;
									
#if LANGUAGE_JA
		maxWidth = 40f;
#endif
		string [] hints = Enum.GetNames(typeof(HintLanguageField));
		
		int rand = UnityEngine.Random.Range(0, hints.Length);
		
		string hint_temp = Language.Get(hints[rand]);
		
		hint.text = TextUtils.ResolveTextBySize(hint_temp, hint.characterSize, maxWidth);
	}
}
