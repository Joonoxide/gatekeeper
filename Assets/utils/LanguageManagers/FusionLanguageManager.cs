﻿using UnityEngine;
using System.Collections;
using KYData;

public class FusionLanguageManager : LanguageManager {

	[SerializeField]
	private TextMesh title, label_fuse, unitName, fuseType, label_progress, label_type, label_cost, label_result;
	[SerializeField]
	private TextMesh error_moreXP;
	
	void Start () 
	{
		title.text = Language.Get (GenericLanguageField.MAIN_MENU_NAV_POWERUP.ToString()).ToUpper();
		label_fuse.text = Language.Get(FusionLanguageField.POWERUP_FUSE.ToString()).ToUpper();
		
		label_progress.text = Language.Get(FusionLanguageField.POWERUP_PROGRESS.ToString());
		label_type.text = Language.Get(FusionLanguageField.POWERUP_TYPE.ToString());
		label_cost.text = Language.Get (FusionLanguageField.POWERUP_COST_XP.ToString());
		label_result.text = Language.Get (FusionLanguageField.POWERUP_RESULT.ToString());
		
		float maxWidth = 2f;
									
#if LANGUAGE_JA
		maxWidth = 1f;
#endif
		
		string error = Language.Get(ErrorLanguageField.ERROR_GET_MORE_XP.ToString());
		
		error_moreXP.text = TextUtils.ResolveTextBySize(error, error_moreXP.characterSize, maxWidth);
	}
	
	public void updateFields(TrooperWrapper trooperWrapper)
	{
		string name = (trooperWrapper.name.Split('_'))[1];
		
		/////////////////////
		///Update unit name and sizing accompanying background
		
		unitName.text = Language.Get("UNIT_NAME_" + name.ToUpper());
		
		//scale the background behind the name to fit the length of the name
		Transform bg_name = unitName.transform.FindChild("bg_name");
		
		if (bg_name)
		{
			Vector3 bgScale = bg_name.localScale;
			bgScale.x = Mathf.Max(2.5f, unitName.renderer.bounds.size.x + 0.3f); //cap the narrowest at 2.5f (tested in scene)
			
			bg_name.localScale = bgScale;
		}
		
		///////////////////
		///Update fusion type according to level and availability
		
		//maximum level
		if(trooperWrapper.level == 30)
		{
			fuseType.text = "-";
		}
		
		else
		{
			//can be evolved
			if(trooperWrapper.level%10 == 0)
			{
				fuseType.text = Language.Get(FusionLanguageField.POWERUP_TYPE_EVOLUTION.ToString()).ToUpper();
			}
			
			else
			{
				fuseType.text = Language.Get (FusionLanguageField.POWERUP_TYPE_FUSION.ToString()).ToUpper();
			}
		}
	}
	
	public void clearFields()
	{
		unitName.gameObject.SetActive(false);
	}
	
}
