﻿using UnityEngine;
using System.Collections;

public class ResultLanguageManager : LanguageManager {

	[SerializeField]
	private TextMesh title, label_rematch, label_continue;
	
	[SerializeField]
	private TextMesh remainingGold, goldSpent, troopsKilled, troopsLost, harvestersKilled, harvestersLost, towersDestroyed, towersLost;
	
	[SerializeField]
	private TextMesh battleXP, clearedReward, characterUnlocked, characterAvailable;
	
	void Start () 
	{
		title.text = Language.Get(ResultLanguageField.RESULT_TITLE.ToString()).ToUpper();
		label_rematch.text = Language.Get(ResultLanguageField.RESULT_REMATCH.ToString()).ToUpper();
		label_continue.text = Language.Get(ResultLanguageField.RESULT_CONTINUE.ToString()).ToUpper();
	
		//stats
		remainingGold.text = Language.Get(ResultLanguageField.RESULT_REMAINING_GOLD.ToString()).ToUpper();
		goldSpent.text = Language.Get (ResultLanguageField.RESULT_GOLD_SPENT.ToString()).ToUpper();
		troopsKilled.text = Language.Get(ResultLanguageField.RESULT_TROOPS_KILLED.ToString()).ToUpper();
		troopsLost.text = Language.Get(ResultLanguageField.RESULT_TROOPS_LOST.ToString()).ToUpper();
		harvestersKilled.text = Language.Get(ResultLanguageField.RESULT_HARVESTERS_KILLED.ToString()).ToUpper();
		harvestersLost.text = Language.Get(ResultLanguageField.RESULT_HARVESTERS_LOST.ToString()).ToUpper();
		towersDestroyed.text = Language.Get(ResultLanguageField.RESULT_TOWERS_DESTROYED.ToString()).ToUpper();
		towersLost.text = Language.Get(ResultLanguageField.RESULT_TOWERS_LOST.ToString()).ToUpper();
		
		//banner
		battleXP.text = Language.Get (ResultLanguageField.RESULT_BATTLE_EXPERIENCE.ToString());
		clearedReward.text = Language.Get(ResultLanguageField.RESULT_CLEARED_REWARD.ToString());
		characterUnlocked.text = Language.Get(ResultLanguageField.RESULT_UNLOCKED_CHARACTER.ToString());
		characterAvailable.text = Language.Get(ResultLanguageField.RESULT_UNLOCKED_CHARACTER_SUMMON.ToString());
	}
}
