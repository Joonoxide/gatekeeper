﻿using UnityEngine;
using System.Collections;
using KYData;

public class UnitSelectionLanguageManager : LanguageManager {

	[SerializeField]
	private TextMesh title, instructions, label_battle;
	
	void Start () 
	{
		title.text = Language.Get (MissionLanguageField.MISSION_CHOOSE_YOUR_ARMY.ToString()).ToUpper();
		instructions.text = Language.Get(MissionLanguageField.MISSION_SELECT_UNIT.ToString());
		
		label_battle.text = Language.Get (MissionLanguageField.MISSION_BATTLE.ToString()).ToUpper();
	}
}
