﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using KYData;

public class UpgradeLanguageManager : LanguageManager {

	[SerializeField]
	private TextMesh title, label_upgrade, label_reset, skillName, skillDescription;
	
	void Start () 
	{
		title.text = Language.Get(GenericLanguageField.MAIN_MENU_NAV_UPGRADE.ToString()).ToUpper();
		label_upgrade.text = Language.Get(UpgradeLanguageField.UPGRADE_LEARN.ToString()).ToUpper();
		label_reset.text = Language.Get (UpgradeLanguageField.UPGRADE_RESET.ToString()).ToUpper();
	}
	
	public void updateFields(UpgradeWrapper upgradeWrapper, int level)
	{
		float maxWidth = 2.4f;
		
#if LANGUAGE_JA
		maxWidth = 1.3f;
#endif
		
		string name = Language.Get(upgradeWrapper.titleLanguageField.ToString());
		
		name += "\n(Lvl " + level + ")";
		
		skillName.text = name;
		
		string description = Language.Get(upgradeWrapper.descriptionLanguageField.ToString());
		
		skillDescription.text = TextUtils.ResolveTextBySize(description, skillDescription.characterSize, maxWidth);
	}
}
