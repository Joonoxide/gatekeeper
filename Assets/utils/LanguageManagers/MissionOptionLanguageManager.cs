﻿using UnityEngine;
using System.Collections;

public class MissionOptionLanguageManager : LanguageManager {

	[SerializeField]
	private TextMesh title, label_resume, label_retry, label_quit;
	
	void Start () 
	{
		title.text = Language.Get (MissionLanguageField.MISSION_OPTION_TITLE.ToString()).ToUpper();
		label_resume.text = Language.Get(MissionLanguageField.MISSION_OPTION_RESUME.ToString()).ToUpper();
		label_retry.text = Language.Get(MissionLanguageField.MISSION_OPTION_RETRY.ToString()).ToUpper();
		label_quit.text = Language.Get (MissionLanguageField.MISSION_OPTION_QUIT.ToString()).ToUpper();
	}
}
