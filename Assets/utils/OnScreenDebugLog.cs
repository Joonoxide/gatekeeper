﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class OnScreenDebugLog : MonoBehaviour {
	
	static List<string> output = new List<string>();
	
	public int maxLines = 10;
	
	private Texture2D bgTex;
	
	// Use this for initialization
	void Awake () 
	{
		//create background texture for textbox
		bgTex = new Texture2D(1, 1);
		bgTex.SetPixel(1, 1, new Color(0, 0, 0, 0.7f));
		bgTex.Apply();
		
		Application.RegisterLogCallback(HandleLog);
		
		DontDestroyOnLoad(gameObject);
	}
	
	void OnDestroy () 
	{
		Application.RegisterLogCallback(null);
	}
	
	// Update is called once per frame
	void HandleLog (string logString, string stackTrace, LogType type) 
	{	
		if (output.Count >= maxLines)
		{
			for(int i = 0; i < output.Count - 1; i++)
			{
				output[i] = output[i + 1];
			}
			
			output.RemoveAt(output.Count - 1);
		}
		
		output.Add(logString);
	}
	
	void OnGUI()
	{
		GUI.Box(new Rect (10, 10, Screen.width-10, Screen.height * 0.4f), bgTex);
		GUI.Label (new Rect (10, 10, Screen.width-10, Screen.height * 0.4f), string.Join("\n", output.ToArray()));
	}
}
