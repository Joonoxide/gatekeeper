using UnityEngine;
using System.Collections;

public class StunEffect : MonoBehaviour {

	public float duration;

	void Update () 
	{
		if(duration <= 0)
		{
			Destroy(gameObject);
			return;
		}
		duration -= Time.deltaTime;
		Vector3 rotation = transform.eulerAngles;
		rotation.y += 5;
		transform.eulerAngles = rotation;
	}
}
