using UnityEngine;
using System.Collections;

public class FrostEffect : MonoBehaviour {

	public float duration;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(duration <= 0)
		{
			Destroy(gameObject);
			return;
		}
		duration -= Time.deltaTime;
		transform.rotation = Quaternion.identity;
	}
}
