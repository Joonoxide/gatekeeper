using UnityEngine;
using System.Collections;

public class ShakeEffect : MonoBehaviour 
{
	public float duration = 5;
	public int bbb = 0;
	public Vector3 original;
	public float effect = 0.5f;
	// Use this for initialization
	void Start () 
	{
		original = transform.position;
		duration = 1;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(Time.timeScale == 0)
		{
			return;
		}
		if(duration <= 0)
		{
			Destroy(this);
			transform.position = original;
			return;
		}
		duration -= Time.deltaTime;

		Vector3 pos = transform.position;
		if(bbb < 3)
		{
			pos.x += effect;
			bbb += 1;
		}
		else if(bbb < 6)
		{
			pos.x -= effect;
			bbb += 1;
		}
		else if(bbb == 6)
		{
			bbb = 0;
		}
		transform.position = pos;
	}
}
