﻿using UnityEngine;
using System.Collections;

public class SummonUnitButton : MonoBehaviour {
	
	[SerializeField]
	private GameObject max, cooldownScreen, label;
	
	// Use this for initialization
	void Start () 
	{
		
	}
	
	public void Init()
	{
		showCooldown();
		showLabel();
	}
	
	public void hideAll()
	{
		hideMax();
		hideCooldown();
		hideLabel();
	}
	
	public void showAll()
	{
		showMax();
		showCooldown();
		showLabel();
	}
	
	public void showMax()
	{
		//max.SetActive(true);
		max.transform.localScale = new Vector3(1, 1, 1);
	}
	
	public void hideMax()
	{
		//max.SetActive(false);
		max.transform.localScale = new Vector3(0, 0, 0);
	}
	
	public void showLabel()
	{
		label.transform.localScale = new Vector3(1, 1, 1);
	}
	
	public void hideLabel()
	{
		label.transform.localScale = new Vector3(0, 0, 0);
	}
	
	public void showCooldown()
	{
		//cooldownScreen.SetActive(true);
		cooldownScreen.transform.localScale = new Vector3(1, 0, 1);
	}
	
	public void hideCooldown()
	{
		//cooldownScreen.SetActive(false);
		cooldownScreen.transform.localScale = new Vector3(0, 0, 0);
	}
}
