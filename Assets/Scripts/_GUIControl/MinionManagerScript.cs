using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class MinionManagerScript : MonoBehaviour 
{
	[System.Serializable]
	public class MinionInfo
	{
        public UIStateToggleBtn Button = null;
		
		public string Name = "";
		
		public string Info = "";
		
		public string PlayPrefName = "";
		
		public int Level = 0;
		
		public int ExpBuyCost = 100;
		
		public int GemBuyCost = 5;
		
		public int IndexNo;
		
		public bool CanBuy = false;
    }
	
	public List<MinionInfo> Minion = new List<MinionInfo> ();
	private MinionInfo SelectedButton;
	public SpriteText InfoPanel;
	public UIStateToggleBtn InfoIcon;
	public UIButton LearnButton;

	// Use this for initialization
	void Start () 
	{
		for (int i = 0; i < Minion.Count ; i++) 
		{
			Minion[i].Level = PlayerPrefs.GetInt(Minion[i].PlayPrefName);
		}
	}
	
	// Update is called once per frame
	void Update () {}
	
	void SelectMinion00 ()
	{
		SelectedButton = Minion[0];
		InfoPanel.Text = "Level " + Minion[0].Level + "\n" + Minion[0].Info;
		InfoIcon.SetState(1);
		RefreshButton ();
	}
	
	void SelectMinion01 ()
	{
		SelectedButton = Minion[1];
		InfoPanel.Text = "Level " + Minion[1].Level + "\n" + Minion[1].Info;
		InfoIcon.SetState(2);
		RefreshButton ();
	}
	
	void SelectMinion02 ()
	{
		SelectedButton = Minion[2];
		InfoPanel.Text = "Level " + Minion[2].Level + "\n" + Minion[2].Info;
		InfoIcon.SetState(3);
		RefreshButton ();
	}
	
	void SelectMinion03 ()
	{
		SelectedButton = Minion[3]; 
		InfoPanel.Text = "Level " + Minion[3].Level + "\n" + Minion[3].Info;
		InfoIcon.SetState(4);
		RefreshButton ();
	}
	
	void RefreshButton ()
	{
		for (int i = 0; i < Minion.Count ; i++) 
		{
			if (Minion[i].Button != SelectedButton.Button)
				Minion[i].Button.SetToggleState(0);
		}
	}
	
	void TrainMinion ()
	{
		if (AbilityController.PlayerExp >= SelectedButton.ExpBuyCost && AbilityController.PlayerGem >= SelectedButton.GemBuyCost)
		{
			SelectedButton.Button.SetState(0);
			AbilityController.PlayerExp -= SelectedButton.ExpBuyCost;
			AbilityController.PlayerGem -= SelectedButton.GemBuyCost;
			Minion[SelectedButton.IndexNo].Level++;
			PlayerPrefs.SetInt(Minion[SelectedButton.IndexNo].PlayPrefName, Minion[SelectedButton.IndexNo].Level);
			PlayerPrefs.Save();
			InfoPanel.Text = "Level " + Minion[SelectedButton.IndexNo].Level + "\n" + Minion[SelectedButton.IndexNo].Info;
		}
	}
}
