using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class ItemManagerScript : MonoBehaviour 
{
	[System.Serializable]
	public class ItemInfo
	{
		public UIActionBtn ItemPic = null;
		
        public UIButton Button = null;
		
		public string Name = "";
		
		public string Info = "";
		
		public string PlayPrefName = "";
	
		public int Amount = 0;
		
		public int CashCost = 100;
		
		public bool CanBuy = false;
		
		public int IndexNo;
    }
	
	public List<ItemInfo> Item = new List<ItemInfo> ();
	
	public ItemInfo SelectedItem;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void BuyItem01 ()
	{
		SelectedItem = Item[0];
		ItemBrought();
	}
	void BuyItem02 ()
	{
		SelectedItem = Item[1];
		ItemBrought();
	}
	void BuyItem03 ()
	{
		SelectedItem = Item[2];
		ItemBrought();
	}
	void BuyItem04 ()
	{
		SelectedItem = Item[3];
		ItemBrought();
	}
	void BuyItem05 ()
	{
		SelectedItem = Item[4];
		ItemBrought();
	}
	void BuyItem06 ()
	{
		SelectedItem = Item[5];
		ItemBrought();
	}
	void BuyItem07 ()
	{
		SelectedItem = Item[6];
		ItemBrought();
	}
	void BuyItem08 ()
	{
		SelectedItem = Item[7];
		ItemBrought();
	}
	
	void ItemBrought ()
	{
		Debug.Log(SelectedItem.Name);
		if(AbilityController.UserCash >= SelectedItem.CashCost)
		{
			if (SelectedItem.Name == "EXP_Duplicator")
			{
				AbilityController.PlayerGem -= SelectedItem.CashCost;
				SelectedItem.CanBuy = false;
				PlayerPrefsX.SetBool("EXP_Duplicator", true);
			}
			else if (SelectedItem.Name == "GEM_Duplicator")
			{
				AbilityController.UserCash -= SelectedItem.CashCost;
				SelectedItem.CanBuy = false;
				PlayerPrefsX.SetBool("GEM_Duplicator", true);
			}
			else if (SelectedItem.Name.Contains("Gem"))
			{
				AbilityController.UserCash -= SelectedItem.CashCost;
				AbilityController.PlayerGem += SelectedItem.Amount;
			}
			else if (SelectedItem.Name.Contains("Exp"))
			{
				AbilityController.UserCash -= SelectedItem.CashCost;
				AbilityController.PlayerExp += SelectedItem.Amount;
			}
		}
	}
}
