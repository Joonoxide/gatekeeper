using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class MapManager : MonoBehaviour 
{
	[System.Serializable]
	public class MapInfo
	{
        public UIButton Button = null;
		
		public Texture MapPreview = null;
		
		public string Name = "";
		
		public string Info = "";
		
		public int Gem = 3;
		
		public int IndexNo;
    }
	
	public List<MapInfo> Map = new List<MapInfo> ();
	public SpriteText InfoPanel;
	public SpriteText GemReward;
	public GUITexture MapPreview;
	
	// Use this for initialization
	void Start () 
	{
		InfoPanel.Text = Map[0].Info;
		GemReward.Text = "Reward for this battle : " + Map[0].Gem;
	}
	
	// Update is called once per frame
	void Update () 
	{}
}
