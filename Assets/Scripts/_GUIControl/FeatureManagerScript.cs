using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class FeatureManagerScript : MonoBehaviour 
{
	[System.Serializable]
	public class FeatureInfo
	{
        public UIStateToggleBtn Button = null;
		
		public string Name = "";
		
		public string Info = "";
		
		public string PlayPrefName = "";
		
		public int Modifier = 0;
		
		public int Amount = 0;
		
		public int ExpBuyCost = 100;
		
		public int GemBuyCost = 5;
		
		public int IndexNo;
    }
	
	public List<FeatureInfo> Feature = new List<FeatureInfo> ();
		
	public SpriteText InfoPanel;
	public UIStateToggleBtn InfoIcon;
	public UIButton LearnButton;
	public UIButton LearnAura;
	public UIButton UpgradeFeat;
	
	public FeatureInfo SelectedButton;
	
	// Use this for initialization
	void Start () 
	{
		for (int i = 0; i < Feature.Count ; i++) 
		{
			Feature[i].Modifier = PlayerPrefs.GetInt(Feature[i].PlayPrefName);
		}
		
		LearnButton.Hide(true);
		LearnAura.Hide(true);
		UpgradeFeat.Hide(true);
	}
	
	void RefreshButton ()
	{
		for (int i = 0; i < Feature.Count ; i++) 
		{
			if (Feature[i].Button != SelectedButton.Button)
				Feature[i].Button.SetToggleState(0);
		}
	}
	
	void SelFeat00 ()
	{
		SelectedButton = Feature[0];
		InfoPanel.Text = "Bonus : " + Feature[SelectedButton.IndexNo].Modifier + "\n" + Feature[0].Info;
		InfoIcon.SetState(13);
		LearnButton.Hide(true);
		LearnAura.Hide(true);
		UpgradeFeat.Hide(false);
		
		RefreshButton();
	}
	
	void SelFeat01 ()
	{
		SelectedButton = Feature[1];
		InfoPanel.Text = "Bonus : " + Feature[SelectedButton.IndexNo].Modifier + "\n" + Feature[1].Info;
		InfoIcon.SetState(14);
		LearnButton.Hide(true);
		LearnAura.Hide(true);
		UpgradeFeat.Hide(false);
		
		RefreshButton();
	}
	
	void SelFeat02 ()
	{
		SelectedButton = Feature[2];
		InfoPanel.Text = "Bonus : " + Feature[SelectedButton.IndexNo].Modifier + "\n" + Feature[2].Info;
		InfoIcon.SetState(15);
		LearnButton.Hide(true);
		LearnAura.Hide(true);
		UpgradeFeat.Hide(false);
		
		RefreshButton();
	}
	
	void SelFeat03 ()
	{
		SelectedButton = Feature[3];
		InfoPanel.Text = "Bonus : " + Feature[SelectedButton.IndexNo].Modifier + "\n" + Feature[3].Info;
		InfoIcon.SetState(16);
		LearnButton.Hide(true);
		LearnAura.Hide(true);
		UpgradeFeat.Hide(false);
		
		RefreshButton();
	}
	
	void SelFeat04 ()
	{
		SelectedButton = Feature[4];
		InfoPanel.Text = "Bonus : " + Feature[SelectedButton.IndexNo].Modifier + "\n" + Feature[4].Info;
		InfoIcon.SetState(17);
		LearnButton.Hide(true);
		LearnAura.Hide(true);
		UpgradeFeat.Hide(false);
		
		RefreshButton();
	}
	
	void SelFeat05 ()
	{
		SelectedButton = Feature[5];
		InfoPanel.Text = "Bonus : " + Feature[SelectedButton.IndexNo].Modifier+ "\n" + Feature[5].Info;
		InfoIcon.SetState(18);
		LearnButton.Hide(true);
		LearnAura.Hide(true);
		UpgradeFeat.Hide(false);
		
		RefreshButton();
	}
	
	void SelFeat06 ()
	{
		SelectedButton = Feature[6];
		InfoPanel.Text = "Bonus : " + Feature[SelectedButton.IndexNo].Modifier+ "\n" + Feature[6].Info;
		InfoIcon.SetState(19);
		LearnButton.Hide(true);
		LearnAura.Hide(true);
		UpgradeFeat.Hide(false);
		
		RefreshButton();
	}
	
	void UpgradeFeature ()
	{
		//var ExpCost = SelectedButton.ExpBuyCost * 1;
		if (AbilityController.PlayerExp >= SelectedButton.ExpBuyCost && AbilityController.PlayerGem >= SelectedButton.GemBuyCost)
		{
			AbilityController.PlayerExp -= SelectedButton.ExpBuyCost;
			AbilityController.PlayerGem -= SelectedButton.GemBuyCost;
			Feature[SelectedButton.IndexNo].Modifier += Feature[SelectedButton.IndexNo].Amount;
			InfoPanel.Text = "Bonus : " + Feature[SelectedButton.IndexNo].Modifier+ "\n" + Feature[SelectedButton.IndexNo].Info;
			AbilityController.SlotCap = (Feature[0].Modifier * 3) + 2;
			
			PlayerPrefs.SetInt("Player_Gem", AbilityController.PlayerGem);
			PlayerPrefs.SetInt("Player_Exp", AbilityController.PlayerExp);
			PlayerPrefs.SetInt(Feature[SelectedButton.IndexNo].PlayPrefName, Feature[SelectedButton.IndexNo].Modifier);
			PlayerPrefs.Save();
			
		}
	}
	
	/*private void OnApplicationQuit()
	{
		for (int i = 0; i < Feature.Count ; i++) 
		{
			if (i == 0)
			{
				PlayerPrefs.SetInt(Feature[i].PlayPrefName, Feature[i].Modifier);
				AbilityController.SlotCap = (Feature[i].Modifier * 3) + 2;
			}
			else
				PlayerPrefs.SetFloat(Feature[i].PlayPrefName, Feature[i].Modifier);
		}
		
		PlayerPrefs.Save();
	}*/
}
