using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class GateManager : MonoBehaviour 
{
	public GameObject WoodGate;
	
	// Use this for initialization
	void Awake () 
	{
	}
	
	void GateClose ()
	{
		Debug.Log("Close Gate!");
		WoodGate.animation.Play("gate_close");
	}
	
	void GateOpen ()
	{
		WoodGate.animation.Play("gate_open");
	}
	
	void Test ()
	{
		Debug.Log("Test Test");
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
