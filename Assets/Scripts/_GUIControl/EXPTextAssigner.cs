using UnityEngine;
using System.Collections;

public class EXPTextAssigner : MonoBehaviour 
{
	public GameObject box_exp;
	
	private SpriteText myText;
		
	// Use this for initialization
	void Start () 
	{
		myText = gameObject.GetComponent("SpriteText") as SpriteText;
		myText.transform.position = box_exp.transform.position + new Vector3(-20, 0, -1);
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		myText.Text = AbilityController.PlayerExp.ToString();
	}
}
