using UnityEngine;
using System.Collections;

public class GEMTextAssigner : MonoBehaviour {
	
	public GameObject box_gem;
	
	private SpriteText myText;
		
	// Use this for initialization
	void Start () 
	{
		myText = gameObject.GetComponent("SpriteText") as SpriteText;
		myText.transform.position = box_gem.transform.position + new Vector3(-20, 0, -1);
	}
	
	// Update is called once per frame
	void Update () 
	{
		myText.Text = AbilityController.PlayerGem.ToString();
	}
}
