using UnityEngine;
using System.Collections;

public static class CButtonImageChanger
{
	public static void copy(UIButton from, UIButton to)
	{
		// Before I copy it, I had make appear each buttons.
		// If the button is hidden, You will get some errors when running in Web Player.
		// However in the editor, there is no errors.
	
		// Show a button.
		bool hidden_from = from.IsHidden();
		from.Hide(false);
		
		// Show a button.
		bool hidden_to = to.IsHidden();
		to.Hide(false);
		
		// Copy from -> to.
		to.Copy(from);
		
		// Restore.
		from.Hide(hidden_from);
		to.Hide(hidden_to);
	}
} 