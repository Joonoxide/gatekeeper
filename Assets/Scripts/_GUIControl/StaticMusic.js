#pragma strict

private static var instance:StaticMusic;

public static function GetInstance() : StaticMusic 
{
	return instance;
}
 
function Awake() 
{
	if (instance != null && instance != this) 
	{
		Destroy(this.gameObject);
		return;
	} 
	else 
	{
		instance = this;
	}
	DontDestroyOnLoad(this.gameObject);
}