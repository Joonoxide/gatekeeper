using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class AuraManager : MonoBehaviour 
{
	[System.Serializable]
	public class AuraInfo
	{
        public UIButton Button = null;
		
		public string Name = "";
		
		public string Info = "";
		
		public string PlayPrefName = "";
		
		public int Level = 0;
		
		public int ExpBuyCost = 100;
		
		public int GemBuyCost = 5;
		
		public int IndexNo;
		
		public bool CanBuy = false;
    }
	
	public List<AuraInfo> Auras = new List<AuraInfo> ();
	private AuraInfo SelectedButton;
	public SpriteText InfoPanel;
	public UIStateToggleBtn InfoIcon;
	public UIButton LearnButton;
	public UIButton LearnAura;
	public UIButton UpgradeFeat;
	
	// Use this for initialization
	void Start () 
	{
		for (int i = 0; i < Auras.Count ; i++) 
		{
			Auras[i].Level = PlayerPrefs.GetInt(Auras[i].PlayPrefName);
			
			//Debug.Log(Auras[i].Name + " : " + Auras[i].Level);
			if (i == 1 | i == 3 | i == 5)
				if (Auras[i-1].Level > 1)
					Auras[i].CanBuy = true;
			
			if (!Auras[i].CanBuy)
				Auras[i].Button.Hide(true);
			
		}
		
		LearnButton.Hide(true);
		LearnAura.Hide(true);
	}
	
	void SelectAuras00 ()
	{
		SelectedButton = Auras[0];
		InfoPanel.Text = "Level " + Auras[SelectedButton.IndexNo].Level + "\n" + Auras[0].Info;
		InfoIcon.SetState(7);
		UpgradeFeat.Hide(true);
		LearnButton.Hide(true);
		LearnAura.Hide(false);
	}
	
	void SelectAuras01 ()
	{
		SelectedButton = Auras[1];
		InfoPanel.Text = "Level " + Auras[SelectedButton.IndexNo].Level + "\n" + Auras[1].Info;
		InfoIcon.SetState(8);
		UpgradeFeat.Hide(true);
		LearnButton.Hide(true);
		LearnAura.Hide(false);
	}
	
	void SelectAuras02 ()
	{
		SelectedButton = Auras[2];
		InfoPanel.Text = "Level " + Auras[SelectedButton.IndexNo].Level + "\n" + Auras[2].Info;
		InfoIcon.SetState(9);
		UpgradeFeat.Hide(true);
		LearnButton.Hide(true);
		LearnAura.Hide(false);
	}
	
	void SelectAuras03 ()
	{
		SelectedButton = Auras[3];
		InfoPanel.Text = "Level " + Auras[SelectedButton.IndexNo].Level + "\n" + Auras[3].Info;
		InfoIcon.SetState(10);
		UpgradeFeat.Hide(true);
		LearnButton.Hide(true);
		LearnAura.Hide(false);
	}
	
	void SelectAuras04 ()
	{
		SelectedButton = Auras[4];
		InfoPanel.Text = "Level " + Auras[SelectedButton.IndexNo].Level + "\n" + Auras[4].Info;
		InfoIcon.SetState(11);
		UpgradeFeat.Hide(true);
		LearnButton.Hide(true);
		LearnAura.Hide(false);
	}
	
	void SelectAuras05 ()
	{
		SelectedButton = Auras[5];
		InfoPanel.Text = "Level " + Auras[SelectedButton.IndexNo].Level+ "\n" + Auras[5].Info;
		InfoIcon.SetState(12);
		UpgradeFeat.Hide(true);
		LearnButton.Hide(true);
		LearnAura.Hide(false);
	}
	
	void UpgradeAura ()
	{
		if (AbilityController.PlayerExp >= SelectedButton.ExpBuyCost && AbilityController.PlayerGem >= SelectedButton.GemBuyCost)
		{
			AbilityController.PlayerExp -= SelectedButton.ExpBuyCost;
			AbilityController.PlayerGem -= SelectedButton.GemBuyCost;
			Auras[SelectedButton.IndexNo].Level++;
			InfoPanel.Text = "Level " + Auras[SelectedButton.IndexNo].Level+ "\n" + Auras[SelectedButton.IndexNo].Info;
			
			if ((SelectedButton.IndexNo == 0 | SelectedButton.IndexNo == 2 | SelectedButton.IndexNo == 4) && !Auras[SelectedButton.IndexNo + 1].CanBuy)
			{
				Auras[SelectedButton.IndexNo + 1].CanBuy = true;
				Auras[SelectedButton.IndexNo + 1].Button.Hide(false);
			}
		}
	}
	
	private void OnApplicationQuit()
	{
		PlayerPrefs.SetInt("Aura_Divine", Auras[0].Level);
		PlayerPrefs.SetInt("Aura_Regen", Auras[1].Level);
		PlayerPrefs.SetInt("Aura_Berserk", Auras[2].Level);
		PlayerPrefs.SetInt("Aura_Radiance", Auras[3].Level);
		PlayerPrefs.SetInt("Aura_Farsight", Auras[4].Level);
		PlayerPrefs.SetInt("Aura_Blood", Auras[5].Level);
		//PlayerPrefs.DeleteAll();
		PlayerPrefs.Save();
	}
}
