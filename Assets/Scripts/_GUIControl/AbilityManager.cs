using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class AbilityManager : MonoBehaviour 
{
	[System.Serializable]
	public class AbilityInfo
	{
        public UIStateToggleBtn Button = null;
		
		public string Name = "";
		
		public string Info = "";
		
		public string PlayerPrefName = "";
		
		public int Level;
		
		public int ExpBuyCost = 100;
		
		public int GemBuyCost = 5;
		
		public int IndexNo;
		
		public bool CanBuy = false;
		
		public bool CanUse = false;
    }
	
	public List<AbilityInfo> Ability = new List<AbilityInfo> ();
	private AbilityInfo SelectedButton;
	public SpriteText InfoPanel;
	public UIStateToggleBtn InfoIcon;
	public UIButton LearnButton;
	public UIButton LearnAura;
	public UIButton UpgradeFeat;
	
	// Use this for initialization
	void Start () 
	{
		for (int i = 0; i < Ability.Count ; i++) 
		{
			Ability[i].CanUse = PlayerPrefsX.GetBool("Abilities_"+i);
			Ability[i].CanBuy = PlayerPrefsX.GetBool("LearnedAbility_"+i);
			Ability[i].Level = PlayerPrefs.GetInt(Ability[i].PlayerPrefName);
			
			//if (!Ability[i].CanBuy)
				//Ability[i].Button.Hide(true);
		}
		
		LearnButton.Hide(true);
		LearnAura.Hide(true);
		UpgradeFeat.Hide(true);
	}
	
	void RefreshButton ()
	{
		for (int i = 0; i < Ability.Count ; i++) 
		{
			if (Ability[i].Button != SelectedButton.Button)
				Ability[i].Button.SetToggleState(0);
		}
	}
	
	void SelectAbility00 ()
	{
		SelectedButton = Ability[0];
		InfoPanel.Text = "Level " + Ability[0].Level + "\n" + Ability[0].Info;
		InfoIcon.SetState(1);
		LearnButton.Hide(false);
		LearnAura.Hide(true);
		UpgradeFeat.Hide(true);
		RefreshButton();
	}
	
	void SelectAbility01 ()
	{
		SelectedButton = Ability[1];
		InfoPanel.Text = "Level " + Ability[1].Level + "\n" + Ability[1].Info;
		InfoIcon.SetState(5);
		LearnButton.Hide(false);
		LearnAura.Hide(true);
		UpgradeFeat.Hide(true);
		RefreshButton();
	}
	
	void SelectAbility02 ()
	{
		SelectedButton = Ability[2];
		InfoPanel.Text = "Level " + Ability[2].Level + "\n" + Ability[2].Info;
		InfoIcon.SetState(3);
		LearnButton.Hide(false);
		LearnAura.Hide(true);
		UpgradeFeat.Hide(true);
		RefreshButton();
	}
	
	void LearnAbility ()
	{
		if (AbilityController.PlayerExp >= SelectedButton.ExpBuyCost && AbilityController.PlayerGem >= SelectedButton.GemBuyCost)
		{
			SelectedButton.Level++;
			AbilityController.PlayerExp -= SelectedButton.ExpBuyCost;
			AbilityController.PlayerGem -= SelectedButton.GemBuyCost;
			
			InfoPanel.Text = "Level " + SelectedButton.Level + "\n" + SelectedButton.Info;
			
			PlayerPrefs.SetInt(SelectedButton.PlayerPrefName, SelectedButton.Level);
			PlayerPrefs.SetInt("Player_Gem", AbilityController.PlayerGem);
			PlayerPrefs.SetInt("Player_Exp", AbilityController.PlayerExp);
			
			PlayerPrefs.Save();
		}
	}
	
	/* Original LearnAbility
	 void LearnAbility ()
	{
		if (AbilityController.PlayerExp >= SelectedButton.ExpBuyCost && AbilityController.PlayerGem >= SelectedButton.GemBuyCost)
		{
			AbilityController.PlayerExp -= SelectedButton.ExpBuyCost;
			AbilityController.PlayerGem -= SelectedButton.GemBuyCost;
			PlayerPrefs.SetInt("Player_Gem", AbilityController.PlayerGem);
			PlayerPrefs.SetInt("Player_Exp", AbilityController.PlayerExp);
			if (!Ability[SelectedButton.IndexNo + 1].CanBuy)
			{
				var unlockNo = SelectedButton.IndexNo + 1;
				Ability[unlockNo].CanBuy = true;
				PlayerPrefsX.SetBool("LearnedAbility_"+unlockNo, true);
				
				Ability[SelectedButton.IndexNo + 1].Button.Hide(false);
			}
			PlayerPrefs.Save();
		}
	}
	 */
	/*private void OnApplicationQuit()
	{
		for (int i = 0; i < Ability.Count ; i++) 
		{
			PlayerPrefsX.SetBool("LearnedAbility_"+i, Ability[i].CanBuy);
			PlayerPrefs.SetInt(Ability[i].PlayerPrefName, Ability[i].Level);
		}
		
		PlayerPrefs.Save();
	}*/
}
