using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class AbilityController : MonoBehaviour 
{
	[System.Serializable]
	public class ItemInfo
	{
        public Texture Icon = null;
		
		public string Name = "";
		
		public int BuyCost = 20;
		
		public int Amount = 100;
		
		public bool CanBuy = true;
    }
	
	public List<ItemInfo> ItemForSale = new List<ItemInfo> ();
	
	public static int DivAuraLv, RegenAuraLv, BloodAuraLv, BerserkLv, RadianceLv, FarsightLv; 
	public static int PlayerExp, PlayerGem, SlotCap, SlotCapLevel, UserCash;
	public static int GobbLv, RiflejackLv, PryssLv, BrickBaneLv;
	//public static int COALv, COHLv, HODLv;
	
	// Use this for initialization
	void Start ()
	{		
		/*for (int i = 0; i < ItemForSale.Count ; i++) 
		{
			if (ItemForSale[i].Name == "EXP_Duplicator")
				ItemForSale[i].CanBuy = !PlayerPrefsX.GetBool("EXP_Duplicator");
			else if (ItemForSale[i].Name == "GEM_Duplicator")
				ItemForSale[i].CanBuy = !PlayerPrefsX.GetBool("GEM_Duplicator");
		}
		*/
		
		UserCash = PlayerPrefs.GetInt("User_Cash");
		PlayerExp = PlayerPrefs.GetInt("Player_Exp");
		PlayerGem = PlayerPrefs.GetInt("Player_Gem");
		SlotCapLevel = PlayerPrefs.GetInt("SlotCap_Level");
		SlotCap = (SlotCapLevel * 3) + 2;
		
		DivAuraLv = PlayerPrefs.GetInt("Aura_Divine");
		RegenAuraLv = PlayerPrefs.GetInt("Aura_Regen");
		BerserkLv = PlayerPrefs.GetInt("Aura_Berserk");
		RadianceLv = PlayerPrefs.GetInt("Aura_Radiance");
		FarsightLv = PlayerPrefs.GetInt("Aura_Farsight");
		BloodAuraLv = PlayerPrefs.GetInt("Aura_Blood");
		
		GobbLv = PlayerPrefs.GetInt("Gobb_Level");
		RiflejackLv = PlayerPrefs.GetInt("Riflejack_Level");
		PryssLv = PlayerPrefs.GetInt("Pryss_Level");
		BrickBaneLv = PlayerPrefs.GetInt("BrickBane_Level");
		
		/*COALv = PlayerPrefs.GetInt("COA_Level");
		COHLv = PlayerPrefs.GetInt("COH_Level");
		HODLv = PlayerPrefs.GetInt("HOD_Level");*/
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void ItemBrought (string itemName, int itemNumber)
	{
		Debug.Log(ItemForSale[itemNumber].Name);
		if (itemName == "EXP_Duplicator")
		{
			UserCash -= ItemForSale[itemNumber].BuyCost;
			ItemForSale[itemNumber].CanBuy = false;
			PlayerPrefsX.SetBool("EXP_Duplicator", true);
		}
		else if (itemName == "GEM_Duplicator")
		{
			UserCash -= ItemForSale[itemNumber].BuyCost;
			ItemForSale[itemNumber].CanBuy = false;
			PlayerPrefsX.SetBool("GEM_Duplicator", true);
		}
		else if (itemName.Contains("GEM"))
		{
			UserCash -= ItemForSale[itemNumber].BuyCost;
			PlayerGem += ItemForSale[itemNumber].Amount;
		}
		else if (itemName.Contains("Exp"))
		{
			Debug.Log(ItemForSale[itemNumber].Name);
			UserCash -= ItemForSale[itemNumber].BuyCost;
			PlayerExp += ItemForSale[itemNumber].Amount;
		}
	}
	
	private void OnApplicationQuit()
	{
		PlayerPrefs.SetInt("Player_Exp", PlayerExp);
		PlayerPrefs.SetInt("Player_Gem", PlayerGem);
		PlayerPrefs.SetInt("User_Cash", UserCash);
		
		//PlayerPrefs.DeleteAll();
		
		PlayerPrefs.Save();
	}
}
