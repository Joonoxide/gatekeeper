using UnityEngine;
using System.Collections;

public class MainMenuGUI : MonoBehaviour {
	
	void Awake () 
	{
		PlayerPrefsX.GetBool("UnlockSlot01");
		PlayerPrefsX.GetBool("UnlockSlot02");
		
		#region Bonus
		if (!PlayerPrefs.HasKey("SlotCap_Level"))
		{
			PlayerPrefs.SetInt("SlotCap_Level", 1);
		}
		if (!PlayerPrefs.HasKey("Bonus_HP"))
		{
			PlayerPrefs.SetInt("Bonus_HP", 0);
		}
		if (!PlayerPrefs.HasKey("Bonus_Damage"))
		{
			PlayerPrefs.SetInt("Bonus_Damage", 0);
		}
		if (!PlayerPrefs.HasKey("Revive_Modifier"))
		{
			PlayerPrefs.SetInt("Revive_Modifier", 0);
		}
		if (!PlayerPrefs.HasKey("Summon_Modifier"))
		{
			PlayerPrefs.SetInt("Summon_Modifier", 0);
		}
		if (!PlayerPrefs.HasKey("Bonus_EXP"))
		{
			PlayerPrefs.SetInt("Bonus_EXP", 0);
		}
		if (!PlayerPrefs.HasKey("Bonus_Gold"))
		{
			PlayerPrefs.SetInt("Bonus_Gold", 0);
		}
		#endregion
		
		PlayerPrefsX.GetBool("EXP_Duplicator", false);
		PlayerPrefsX.GetBool("GEM_Duplicator", false);
		
		
		#region Minions
		
		if (!PlayerPrefs.HasKey("Gobb_Level"))
		{
			PlayerPrefs.SetInt("Gobb_Level", 0);
		}
		
		if (!PlayerPrefs.HasKey("Riflejack_Level"))
		{
			PlayerPrefs.SetInt("Riflejack_Level", 0);
		}
		
		if (!PlayerPrefs.HasKey("Pryss_Level"))
		{
			PlayerPrefs.SetInt("Pryss_Level", 0);
		}
		
		if (!PlayerPrefs.HasKey("BrickBane_Level"))
		{
			PlayerPrefs.SetInt("BrickBane_Level", 0);
		}
		
		#endregion
		
		#region HeroAbilities
		for (int i = 0; i < 18; i++) 
		{	
			PlayerPrefsX.GetBool("Abilities_"+i);
			PlayerPrefsX.GetBool("LearnedAbility_"+i);
			
			if (i > 9)
				PlayerPrefsX.SetBool("Abilities_"+i, true);
			else
				PlayerPrefsX.SetBool("Abilities_"+i, false);
			
			/*if (i < 5 | i > 9 | i == 8 )
				PlayerPrefsX.SetBool("Abilities_"+i, true);*/
			
			PlayerPrefs.Save();
		}
		
		//Level of Individual Ability
		if (!PlayerPrefs.HasKey("COA_Level"))
		{
			PlayerPrefs.SetInt("COA_Level", 0);
		}
		if (!PlayerPrefs.HasKey("COH_Level"))
		{
			PlayerPrefs.SetInt("COH_Level", 0);
		}
		if (!PlayerPrefs.HasKey("HOD_Level"))
		{
			PlayerPrefs.SetInt("HOD_Level", 0);
		}
		#endregion
		
		#region Auras
		for (int i = 0; i < 6; i++) 
		{	
			PlayerPrefsX.GetBool("OnAura"+i);
			
			PlayerPrefs.Save();
		}
		
		if (!PlayerPrefs.HasKey("Aura_Divine"))
		{
			PlayerPrefs.SetInt("Aura_Divine", 1);
		}
		
		if (!PlayerPrefs.HasKey("Aura_Regen"))
		{
			PlayerPrefs.SetInt("Aura_Regen", 1);
		}
		
		if (!PlayerPrefs.HasKey("Aura_Berserk"))
		{
			PlayerPrefs.SetInt("Aura_Berserk", 1);
		}
		
		if (!PlayerPrefs.HasKey("Aura_Radiance"))
		{
			PlayerPrefs.SetInt("Aura_Radiance", 1);
		}
		
		if (!PlayerPrefs.HasKey("Aura_Farsight"))
		{
			PlayerPrefs.SetInt("Aura_Farsight", 1);
		}
		
		if (!PlayerPrefs.HasKey("Aura_Blood"))
		{
			PlayerPrefs.SetInt("Aura_Blood", 1);
		}
		
		#endregion
		
		if (!PlayerPrefs.HasKey("Hero_Level"))
		{
			PlayerPrefs.SetInt("Hero_Level", 1);
		}
		
		if (!PlayerPrefs.HasKey("Player_Gem"))
		{
			PlayerPrefs.SetInt("Player_Gem", 1000);
		}
		
		if (!PlayerPrefs.HasKey("Player_Coin"))
		{
			PlayerPrefs.SetInt("Player_Coin", 5000);
		}
		
		if (!PlayerPrefs.HasKey("Player_Exp"))
		{
			PlayerPrefs.SetInt("Player_Exp", 1000);
		}
		
		if (!PlayerPrefs.HasKey("User_Cash"))
		{
			PlayerPrefs.SetInt("User_Cash", 500);
		}
	}
}
