using UnityEngine;
using System.Collections;

public class PlayerPrefsX
{
	public static void SetBool(string name, bool booleanValue) 
	{
		PlayerPrefs.SetInt(name, booleanValue ? 1 : 0);
	}
 
	public static bool GetBool(string name)  
	{
	    return PlayerPrefs.GetInt(name) == 1 ? true : false;
	}
 
	public static bool GetBool(string name, bool defaultValue)
	{
	    if(PlayerPrefs.HasKey(name)) 
		{
	        return GetBool(name);
	    }
 
	    return defaultValue;
	}
	
	public static void StorePurchase(string id, string uniqueIdentifier)
	{
		PlayerPrefs.SetString(id, uniqueIdentifier); 
	}
	
	public static void RemovePurchase(string id, string uniqueIdentifier)
	{
		if (CheckIfPurchaseValid(id, uniqueIdentifier))
		{
			PlayerPrefs.DeleteKey(id);
		}
		
	}
	
	public static bool CheckIfPurchaseValid(string id, string uniqueIdentifier)
	{
		if (PlayerPrefs.HasKey(id))
		{
			return PlayerPrefs.GetString(id).Equals(uniqueIdentifier);
		}
		
		return false;
		
	}
}
