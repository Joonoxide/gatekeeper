﻿using UnityEngine;
using System.Collections;

public class FlurryAnalyticsManager : MonoBehaviour {
	
#if LANGUAGE_EN
	private string key = "MP32DBFCPKNTFX3BDZ9Z";
#elif LANGUAGE_JA
	private string key = "Y9N36XBQWQGCY738DVCB";
#endif
	
	private NerdFlurry mNerdFlurry;
	
	void Awake()
	{
		DontDestroyOnLoad(gameObject);
	}
	
	// Use this for initialization
	void Start () 
	{
		mNerdFlurry = GetComponent<NerdFlurry>();
		mNerdFlurry.StartSession(key);
	}
	
	void OnApplicationQuit()
	{
		mNerdFlurry.EndSession();
	}
}
