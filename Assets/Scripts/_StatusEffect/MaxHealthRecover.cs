using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class MaxHealthRecover : Fury.Database.Status, Fury.Database.Status.IPeriodic
{
	public Single Period = 1.0f;
	public UnityEngine.GameObject EffectPrefab = null;
	
	#region IPeriodic implementation
	object Fury.Database.Status.IPeriodic.OnUpdate (object tag, Fury.Behaviors.Unit unit, Fury.Behaviors.Unit from)
	{
		var DifferInHealth = unit.Controllers.VitalityController.MaxHealth - unit.Controllers.VitalityController.Health;
		
		if (EffectPrefab != null)
        {
            var effect = (UnityEngine.GameObject)UnityEngine.GameObject.Instantiate(EffectPrefab);
            effect.transform.position = unit.collider.bounds.center;
            effect.transform.parent = unit.transform;
        }
		
		if (unit.Owner == 0)
			unit.ModifyHealth(DifferInHealth, from, this);
		
        return null;
	}

	float Fury.Database.Status.IPeriodic.Period { get { return Period; } }
	#endregion	

}
