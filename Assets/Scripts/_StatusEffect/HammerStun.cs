using System;
using System.Collections.Generic;

using UnityEngine;

public class HammerStun : Fury.Database.Status, Fury.Database.Status.IPeriodic, Fury.Database.Status.IStun
{
	public Single Period = 1f;
    public UnityEngine.GameObject EffectPrefab = null;
 
    float Fury.Database.Status.IPeriodic.Period { get { return Period; } }
 
    object Fury.Database.Status.IPeriodic.OnUpdate(object tag, Fury.Behaviors.Unit unit, Fury.Behaviors.Unit from)
    {
        if (EffectPrefab != null)
        {
            var effect = (UnityEngine.GameObject)UnityEngine.GameObject.Instantiate(EffectPrefab);
            effect.transform.position = unit.collider.bounds.center;
            effect.transform.parent = unit.transform;
        }
        return null;
    }

	#region IStun implementation
	bool Fury.Database.Status.IStun.IsStunned (Fury.Behaviors.Unit from, Fury.Behaviors.Unit target)
	{
		return true;
	}
	#endregion
}
