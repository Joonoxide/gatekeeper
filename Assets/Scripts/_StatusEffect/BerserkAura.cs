using System;
using System.Collections.Generic;

using UnityEngine;

public class BerserkAura : Fury.Database.Status, Fury.Database.Status.ISpeed, Fury.Database.Status.IPeriodic
{
	public float haste;
	public Single Period = 1.0f;
	public UnityEngine.GameObject EffectPrefab = null;
	
	#region IPeriodic implementation
	object Fury.Database.Status.IPeriodic.OnUpdate (object tag, Fury.Behaviors.Unit unit, Fury.Behaviors.Unit from)
	{
		if (EffectPrefab != null)
        {
            var effect = (UnityEngine.GameObject)UnityEngine.GameObject.Instantiate(EffectPrefab);
            effect.transform.position = unit.collider.bounds.center;
            effect.transform.parent = unit.transform;
        }
		
		if (unit.Owner == 0 && unit.Properties._Weapon)
			unit.Properties._Weapon._Cooldown = unit.Properties._Weapon._PreDelay + unit.Properties._Weapon._PostDelay;
				
        return null;
	}

	float Fury.Database.Status.IPeriodic.Period { get { return Period; } }
	#endregion	
	
	#region ISpeed implementation
	float Fury.Database.Status.ISpeed.GetModifier (Fury.Behaviors.Unit from, Fury.Behaviors.Unit target)
	{
		if (target != null)
		{
			if (target.Owner == 0 && target.Properties._Weapon)
			{
				haste = 2;
				target.Properties._Weapon._Cooldown = 0;
			}
			else
			{
				haste = 0;
			}
		}
		
		return haste;
	}
	#endregion
}
