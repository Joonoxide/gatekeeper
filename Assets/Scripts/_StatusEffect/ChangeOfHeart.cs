using System;
using System.Collections.Generic;

using UnityEngine;

public class ChangeOfHeart : Fury.Database.Status, Fury.Database.Status.IPeriodic
{
	public Single Period = 1f;
    public UnityEngine.GameObject EffectPrefab = null;
 
    float Fury.Database.Status.IPeriodic.Period { get { return Period; } }
	
	public object OnUpdate (object tag, Fury.Behaviors.Unit unit, Fury.Behaviors.Unit from)
	{
		if (EffectPrefab != null)
        {
            var effect = (UnityEngine.GameObject)UnityEngine.GameObject.Instantiate(EffectPrefab);
            effect.transform.position = unit.collider.bounds.center;
            effect.transform.parent = unit.transform;
        }
	
		unit.ChangeOwner(Fury.CommanderIndices.Two);
		
        return null;
	}
}
