﻿using UnityEngine;
using System.Collections;

public static class EventManager {

	public delegate void ItemEvent();
	public static event ItemEvent HarvesterChanged, FragmentChanged;
	
	public static void TriggerHarvesterChanged()
	{
		if (HarvesterChanged != null)
		{
			HarvesterChanged();
		}
	}
	
	public static void TriggerFragmentChanged()
	{
		if (FragmentChanged != null)
		{
			FragmentChanged();
		}
	}
}
