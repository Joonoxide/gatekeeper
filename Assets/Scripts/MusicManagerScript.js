var BGM : AudioClip; //Pick an audio track to play.
var SFX : AudioClip;
var BGMButton : UIStateToggleBtn;

function Awake ()
{
	Debug.Log(transform.parent);
	if (BGM != null)
	{
		var go = GameObject.Find("menu_2");
		go.audio.clip = BGM;
		//go.audio.Play();
	}
	if (BGMButton != null)
	{
		if (go.audio.isPlaying)
			BGMButton.SetToggleState("on");
		else
			BGMButton.SetToggleState("off");
	}
}

function Start ()
{
	//BGMButton = GameObject.Find("toggle_bgm");
	
}

function ToggleBGM ()
{
	//Fury.Hud.ConsumeLMB();
	if (BGM != null)
	{
		var go = GameObject.Find("menu_2");
		go.audio.clip = BGM;
		
		if (go.audio.isPlaying)
			go.audio.Pause(); 
		else
			go.audio.Play(); 
	}
}

function ToggleSFX()
{
	//Fury.Hud.ConsumeLMB();
	if (SFX != null)
	{
		var sfxgo = GameObject.Find("ambient");
		sfxgo.audio.clip = SFX;
		
		if (sfxgo.audio.isPlaying)
			sfxgo.audio.Pause(); 
		else
			sfxgo.audio.Play(); 
	}
}
