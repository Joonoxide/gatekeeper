﻿using UnityEngine;
using System.Collections;

public static class Randomiser {
	
	public static string createRandomToken () {
	
		string str = "";
		
		for (int i = 0; i < Random.Range(11, 25); i++)
			str += (char) Random.Range(0, 234);
		
		return (str);
	}
}
