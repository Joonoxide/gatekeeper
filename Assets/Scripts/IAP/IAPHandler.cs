﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class IAPHandler : MonoBehaviour {
	
	/*
	WARNING: If isDebug is true, purchases will be deleted 
	and any made thereafter will go through but will be
	consumed immediately without any effect to the game
	*/
	public bool isDebug = false;
	
	//TO DO: encrypt public key
	private string publicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAr9KMX4WkMH9Nrn//pQhbGs70gOHdnvTpmvJDfMILSfGUNj8GJpkKUDlA2eWD/M+q73rNPtxc2Da9WtSYyMy6cPNXN1kuHRUW55NpFfKko+6o5ifG4G/BDHSYI50+GY4OSWUl36hgWLpZMOWKqEXQakyLkKFWsOKfE1+SxKnKnd3hDHPV9F1k0YEg4MEvySLnDWML2qQrr8tKgpJTxDFOwXlJV2Df0Vr7912bGHOAM0zFWmCgGnrEZD2nnyayKGx/C0qRKt1uhB817Ffl1E3T0D6TPkjLriHOVkcoZtBfoaMhpq6bpq5EX0HQA2WkYgTIalDKouIZ4nsF53VfmgEBiwIDAQAB";
	
#if UNITY_IPHONE
	private List<StoreKitProduct> _productList;
	
#endif
	
#if UNITY_IPHONE || UNITY_ANDROID
	void Start()
	{
		DontDestroyOnLoad(this);
		
		init();
	}
#endif
	
#if UNITY_IPHONE
	void init()
	{
		//if billing is possible, initialize IAP engine
		if (StoreKitBinding.canMakePayments())
		{
			Debug.Log("[IAPHandler] Billing option is possible on this device!");
			
			addCoreListeners();
			//verifyPurchases();
			StoreKitBinding.requestProductData(SKUData.fetchSKUList());
		}
		
		//if billing is not possible, don't do anything
		else
		{
			Debug.Log("[IAPHandler] Billing option is not possible on this device!");
		}
	}
	
	void productListReceivedEvent(List<StoreKitProduct> availableProducts)
	{
		//store the product list locally for ease of access later
		_productList = availableProducts;
		
		//Debug.Log ("Available products:\n" + string.Join("\n", _productList.Select(o => o.title).ToArray()));
	}
	
	void productListRequestFailed(string error)
	{
		Debug.LogWarning("[IAPHandler] Unable to fetch product list (" + error +")");
	}
	
	void purchaseSuccessful(StoreKitTransaction product)
	{
		//Debug.Log ("Purchased " + product.quantity + " of " + product.productIdentifier + " [" + product.transactionIdentifier + "]!");
		handleProductEffect(product);
	}
	
	void transactionsRestorationSuccessful()
	{
		Debug.Log ("restoring product...");
	}
	
	void purchaseCancelled(string error)
	{
		Debug.LogWarning("[IAPHandler] Purchase cancelled (" + error +")");
	}
	
	void purchaseFailed(string error)
	{
		Debug.LogWarning("[IAPHandler] Purchase failed (" + error +")");
	}
	
	void receiptValidationSuccessful()
	{
		Debug.Log ("[IAPHandler] Receipt validation successful!");
	}
	
	void receiptValidationFailed(string error)
	{
		Debug.LogWarning("[IAPHandler] Receipt validation failed (" + error + ")");
		
		//revert possible fake purchases
		PlayerPrefs.DeleteAll();
		
		KYDataManager.getInstance().initialize();
	}
	
	void transactionRestorationFailed(string error)
	{
		Debug.LogWarning("[IAPHandler] Transaction restoration failed (" + error + ")");
	}
	
	void addCoreListeners()
	{
		StoreKitManager.productListReceivedEvent += productListReceivedEvent;
		StoreKitManager.productListRequestFailedEvent += productListRequestFailed;
		StoreKitManager.purchaseSuccessfulEvent += purchaseSuccessful;
		StoreKitManager.purchaseCancelledEvent += purchaseCancelled;
		StoreKitManager.purchaseFailedEvent += purchaseFailed;
		
		StoreKitManager.restoreTransactionsFinishedEvent += transactionsRestorationSuccessful;
		StoreKitManager.restoreTransactionsFailedEvent += transactionRestorationFailed;
	}
	
	void removeCoreListeners()
	{
		StoreKitManager.productListReceivedEvent -= productListReceivedEvent;
		StoreKitManager.productListRequestFailedEvent -= productListRequestFailed;
		StoreKitManager.purchaseSuccessfulEvent -= purchaseSuccessful;
		StoreKitManager.purchaseCancelledEvent -= purchaseCancelled;
		StoreKitManager.purchaseFailedEvent -= purchaseFailed;
		
		StoreKitManager.restoreTransactionsFinishedEvent -= transactionsRestorationSuccessful;
		StoreKitManager.restoreTransactionsFailedEvent -= transactionRestorationFailed;
	}
	
	void OnDestroy()
	{
		removeCoreListeners();
	}
	
	void verifyPurchases()
	{
		//TO DO: check harvester count with purchased items
		List<StoreKitTransaction> transactionList = StoreKitBinding.getAllSavedTransactions();
		if( transactionList.Count > 0 )
			StoreKitBinding.validateReceipt( transactionList[0].base64EncodedTransactionReceipt, true );
	}
	
	void handleProductEffect(StoreKitTransaction product)
	{		
		Debug.Log ("handling purchase effect...");
		string id_harvester = "harvester";
		string id_fragment = "fragment";
		
		//handle effect of harvester purchases
		if (product.productIdentifier.Contains(id_harvester))
		{
			Debug.Log ("handling harvester effect...");
			if (product.productIdentifier.Contains("all"))
			{
				//Add 5 harvesters (max)
				if (KYDataManager.getInstance().upgradeNameForLevel("Harvester Count") != 5)
				{
					GA.API.Design.NewEvent(CustomDesignEvent.getPurchaseHarvester(QuantityHarvester._Max));
					GA.API.Business.NewEvent(CustomBusinessEvent.getPurchaseHarvester(QuantityHarvester._Max), 
							CustomBusinessEvent.CURRENCY, 
							CustomBusinessEvent.fetchPriceHarvester(QuantityHarvester._Max));
					
					KYDataManager.getInstance().addUpgrade(5, 5);
				}
				
				else
				{
					Debug.Log ("MAXED HARVESTERS!");
				}
			}
			
			else
			{
				int count;
			
				if (int.TryParse(product.productIdentifier.Substring(product.productIdentifier.LastIndexOf(id_harvester) + id_harvester.Length), out count))
				{
					if (count > KYDataManager.getInstance().upgradeNameForLevel("Harvester Count"))
					{
						//Add difference in harvesters
						GA.API.Design.NewEvent(CustomDesignEvent.getPurchaseHarvester(CustomDesignEvent.getHarvesterQuantityFromID(product.productIdentifier)));
						GA.API.Business.NewEvent(CustomBusinessEvent.getPurchaseHarvester(CustomDesignEvent.getHarvesterQuantityFromID(product.productIdentifier)), 
							CustomBusinessEvent.CURRENCY, 
							CustomBusinessEvent.fetchPriceHarvester(CustomDesignEvent.getHarvesterQuantityFromID(product.productIdentifier)));
						
						KYDataManager.getInstance().addUpgrade(5, count);
					}
					
				}
			
				else
				{
					Debug.Log("Invalid count!");
				}
			}
			
			EventManager.TriggerHarvesterChanged();
		}
		
		//handle effect of fragment purchases
		else if (product.productIdentifier.Contains(id_fragment))
		{
			Debug.Log ("handling fragment effect...");
			int count;
				
			if (int.TryParse(product.productIdentifier.Substring(product.productIdentifier.LastIndexOf(id_fragment) + id_fragment.Length), out count))
			{
				Debug.Log ("Successfully added " + count.ToString() + " fragments!");
				
				GA.API.Design.NewEvent(CustomDesignEvent.getPurchaseFragment(CustomDesignEvent.getFragmentQuantityFromID(product.productIdentifier)));
				GA.API.Business.NewEvent(CustomBusinessEvent.getPurchaseFragment(CustomDesignEvent.getFragmentQuantityFromID(product.productIdentifier)), 
						CustomBusinessEvent.CURRENCY, 
						CustomBusinessEvent.fetchPriceFragment(CustomDesignEvent.getFragmentQuantityFromID(product.productIdentifier)));
				
				KYDataManager.getInstance().dataWrapper.rubyCount += count;
				
				EventManager.TriggerFragmentChanged();
			}
			
			else
			{
				Debug.Log ("Invalid count!");
			}
		}
	}
	
#endif
	
#if UNITY_ANDROID
	void init()
	{
		addInitListeners();
		
		GoogleIAB.init(publicKey);
	}
	
	void startCore()
	{
		addCoreListeners();
		
		Debug.Log ("Core started!");
		
		Debug.Log ("Sending query...");
		
		GoogleIAB.queryInventory(SKUData.fetchSKUList());
	}
	
	void queryInventorySucceeded(List<GooglePurchase> purchases, List<GoogleSkuInfo> skus)
	{
		Debug.Log ("Query inventory successful");
		
		List <string> IDs = new List<string>();
		
		foreach(GooglePurchase purchase in purchases)
		{
			Debug.Log (purchase.productId + ", time: " + purchase.purchaseTime.ToString());		
			IDs.Add (purchase.productId);
		}
		
		GoogleIAB.consumeProducts(IDs.ToArray());
		
	}
	
	void queryInventoryFailed(string error)
	{
		Debug.Log (error);
	}
	
	void purchaseSucceeded(GooglePurchase product)
	{
		Debug.Log ("Purchased " + product.productId + " [" + product.developerPayload + "]!");
		
		//check if the purchase is valid for the current session by 
		//comparing given and stored token
		//if it EQUALS, purchase is valid
		if (SKUData.transactionToken.Equals(product.developerPayload))
		{
			//GoogleIAB.consumeProduct(product.productId);
			PlayerPrefsX.StorePurchase(product.productId, product.developerPayload);
			handleConsumptionNeed(product);
		}
		
		else
		{
			Debug.Log ("Invalid transaction!");
		}
	}
	
	void purchaseFailed(string error)
	{
		Debug.Log (error);
	}
	
	void handleConsumptionNeed(GooglePurchase product)
	{
		if (isDebug || checkIfConsumable(product.productId))
		{
			GoogleIAB.consumeProduct(product.productId);
		}
		
		else
		{
			if (!PlayerPrefsX.CheckIfPurchaseValid(product.productId, product.developerPayload))
			{
				Debug.Log ("Mismatched product uniqueID!");
				return;
			}
			
			string id_harv = "harvester";
			
			if (product.productId.Contains(id_harv))
			{
				
				if (product.productId.Contains("all"))
				{
					//Add 5 harvesters (max)
					if (KYDataManager.getInstance().upgradeNameForLevel("Harvester Count") != 5)
					{
						GA.API.Design.NewEvent(CustomDesignEvent.getPurchaseHarvester(QuantityHarvester._Max));
						GA.API.Business.NewEvent(CustomBusinessEvent.getPurchaseHarvester(QuantityHarvester._Max), 
								CustomBusinessEvent.CURRENCY, 
								CustomBusinessEvent.fetchPriceHarvester(QuantityHarvester._Max));
						
						KYDataManager.getInstance().addUpgrade(5, 5);
					}
					
					else
					{
						Debug.Log ("MAXED HARVESTERS!");
					}
				}
				
				else
				{
					int count;
				
					if (int.TryParse(product.productId.Substring(product.productId.LastIndexOf(id_harv) + id_harv.Length), out count))
					{
						if (count > KYDataManager.getInstance().upgradeNameForLevel("Harvester Count"))
						{
							//Add difference in harvesters
							GA.API.Design.NewEvent(CustomDesignEvent.getPurchaseHarvester(CustomDesignEvent.getHarvesterQuantityFromID(product.productId)));
							GA.API.Business.NewEvent(CustomBusinessEvent.getPurchaseHarvester(CustomDesignEvent.getHarvesterQuantityFromID(product.productId)), 
								CustomBusinessEvent.CURRENCY, 
								CustomBusinessEvent.fetchPriceHarvester(CustomDesignEvent.getHarvesterQuantityFromID(product.productId)));
							
							KYDataManager.getInstance().addUpgrade(5, count);
						}
						
					}
				
					else
					{
						Debug.Log("Invalid count!");
					}
				}
				
				EventManager.TriggerHarvesterChanged();
			}
			
			
		}
	}
	
	bool checkIfConsumable(string id)
	{
		foreach (string identifier in SKUData.ID_PARTIAL_CONSUMABLES)
		{
			if (id.Contains(identifier))
			{
				return true;
			}
		}
		
		return false;
	}
	
	void consumeProductSucceeded(GooglePurchase product)
	{
		if (isDebug || !PlayerPrefsX.CheckIfPurchaseValid(product.productId, product.developerPayload))
		{
			return;
		}
		
		foreach (string identifier in SKUData.ID_PARTIAL_CONSUMABLES)
		{
			if (product.productId.Contains(identifier))
			{
				if (identifier.Equals("fragment"))
				{
					int count;
				
					if (int.TryParse(product.productId.Substring(product.productId.LastIndexOf(identifier) + identifier.Length), out count))
					{
						Debug.Log ("Successfully added " + count.ToString() + " fragments!");
						
						GA.API.Design.NewEvent(CustomDesignEvent.getPurchaseFragment(CustomDesignEvent.getFragmentQuantityFromID(product.productId)));
						GA.API.Business.NewEvent(CustomBusinessEvent.getPurchaseFragment(CustomDesignEvent.getFragmentQuantityFromID(product.productId)), 
								CustomBusinessEvent.CURRENCY, 
								CustomBusinessEvent.fetchPriceFragment(CustomDesignEvent.getFragmentQuantityFromID(product.productId)));
						
						PlayerPrefsX.RemovePurchase(product.productId, product.developerPayload);
						KYDataManager.getInstance().dataWrapper.rubyCount += count;
						
						EventManager.TriggerFragmentChanged();
					}
					
					else
					{
						Debug.Log ("Invalid count!");
					}
				}
				
			}
		}
	}
	
	void consumeProductFailed(string error)
	{
		Debug.Log (error);
	}
	
	void billingSupported()
	{
		Debug.Log ("Billing is supported on this device!");
		
		removeInitListeners();
		
		startCore();
	}
	
	void billingNotSupported(string error)
	{
		Debug.Log ("Billing not supported on this device!");
		
		removeInitListeners();
	}
	
	void addInitListeners()
	{
		GoogleIABManager.billingSupportedEvent += billingSupported;
		GoogleIABManager.billingNotSupportedEvent += billingNotSupported;
	}
	
	void removeInitListeners()
	{
		GoogleIABManager.billingSupportedEvent -= billingSupported;
		GoogleIABManager.billingNotSupportedEvent -= billingNotSupported;
	}
	
	void addCoreListeners()
	{
		GoogleIABManager.purchaseSucceededEvent += purchaseSucceeded;
		GoogleIABManager.purchaseFailedEvent += purchaseFailed;
		
		GoogleIABManager.consumePurchaseSucceededEvent += consumeProductSucceeded;
		GoogleIABManager.consumePurchaseFailedEvent += consumeProductFailed;
		
		GoogleIABManager.queryInventorySucceededEvent += queryInventorySucceeded;
		GoogleIABManager.queryInventoryFailedEvent += queryInventoryFailed;
	}
	
	void removeCoreListeners()
	{
		GoogleIABManager.purchaseSucceededEvent -= purchaseSucceeded;
		GoogleIABManager.purchaseFailedEvent -= purchaseFailed;
		
		GoogleIABManager.consumePurchaseSucceededEvent -= consumeProductSucceeded;
		GoogleIABManager.consumePurchaseFailedEvent -= consumeProductFailed;
		
		GoogleIABManager.queryInventorySucceededEvent -= queryInventorySucceeded;
		GoogleIABManager.queryInventoryFailedEvent -= queryInventoryFailed;
	}
	
	void OnDestroy()
	{
		removeCoreListeners();
	}
	
#endif
}
