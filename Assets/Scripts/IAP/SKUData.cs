﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class SKUData {
	
	//harvesters
	public const string ID_harvester1 = "SKUID_HARV1";
	public const string ID_harvester2 = "SKUID_HARV2";
	public const string ID_harvester3 = "SKUID_HARV3";
	public const string ID_harvester4 = "SKUID_HARV4";
	public const string ID_harvester5 = "SKUID_HARV5";
	public const string ID_harvesterAll = "SKUID_HARVALL";
	
	//fragments
	public const string ID_fragment1 = "SKUID_FRAG1";
	public const string ID_fragment6 = "SKUID_FRAG6";
	public const string ID_fragment12 = "SKUID_FRAG12";
	public const string ID_fragment30 = "SKUID_FRAG30";
	public const string ID_fragment60 = "SKUID_FRAG60";
	public const string ID_fragment85 = "SKUID_FRAG85";
	
	public static string [] ID_PARTIAL_CONSUMABLES = new string []
	{
		"fragment"
	};
	
	//transaction
	public static string transactionToken { get; private set;}
	
	private static readonly Dictionary<string, string> lookup_SKU = new Dictionary<string, string>()
	{
#if LANGUAGE_EN
		{ID_harvester1, "net.topfuncoolgames.gatekeeper.harvester1"}, //3rd harvester
		{ID_harvester2, "net.topfuncoolgames.gatekeeper.harvester2"}, //4th harvester
		{ID_harvester3, "net.topfuncoolgames.gatekeeper.harvester3"}, //5th harvester
		{ID_harvester4, "net.topfuncoolgames.gatekeeper.harvester4"}, //6th harvester
		{ID_harvester5, "net.topfuncoolgames.gatekeeper.harvester5"}, //7th harvester
		{ID_harvesterAll, "net.topfuncoolgames.gatekeeper.allharvesters"}, //all harvesters
		{ID_fragment1, "net.topfuncoolgames.gatekeeper.fragment1"}, //1 fragment
		{ID_fragment6, "net.topfuncoolgames.gatekeeper.fragment6"}, //6 fragments
		{ID_fragment12, "net.topfuncoolgames.gatekeeper.fragment12"}, //12 fragments
		{ID_fragment30, "net.topfuncoolgames.gatekeeper.fragment30"}, //30 fragments
		{ID_fragment60, "net.topfuncoolgames.gatekeeper.fragment60"}, //60 fragments
		{ID_fragment85, "net.topfuncoolgames.gatekeeper.fragment85"}, //85 fragments
#endif
		
#if LANGUAGE_JA
		{ID_harvester1, "net.facv.gatekeeper.harvester1"}, //3rd harvester
		{ID_harvester2, "net.facv.gatekeeper.harvester2"}, //4th harvester
		{ID_harvester3, "net.facv.gatekeeper.harvester3"}, //5th harvester
		{ID_harvester4, "net.facv.gatekeeper.harvester4"}, //6th harvester
		{ID_harvester5, "net.facv.gatekeeper.harvester5"}, //7th harvester
		{ID_harvesterAll, "net.facv.gatekeeper.allharvesters"}, //all harvesters
		{ID_fragment1, "net.facv.gatekeeper.fragment1"}, //1 fragment
		{ID_fragment6, "net.facv.gatekeeper.fragment6"}, //6 fragments
		{ID_fragment12, "net.facv.gatekeeper.fragment12"}, //12 fragments
		{ID_fragment30, "net.facv.gatekeeper.fragment30"}, //30 fragments
		{ID_fragment60, "net.facv.gatekeeper.fragment60"}, //60 fragments
		{ID_fragment85, "net.facv.gatekeeper.fragment85"}, //85 fragments
#endif
	};
	
	//feed in id from this class to obtain associated sku
	public static string getSKU(string id)
	{
		string sku;
		
		if (!lookup_SKU.TryGetValue(id, out sku))
		{
			sku = "null";
		}
		
		return sku;
		
	}
	
	public static string [] fetchSKUList()
	{
		string [] list = new string [lookup_SKU.Count];
		
		lookup_SKU.Values.CopyTo(list, 0);
		
		return list;
	}
	
	public static string createTransactionToken()
	{
		transactionToken = System.Guid.NewGuid().ToString();
		
		return transactionToken;
	}
	
	public static void clearToken()
	{
		transactionToken = "null";
	}
	
}
