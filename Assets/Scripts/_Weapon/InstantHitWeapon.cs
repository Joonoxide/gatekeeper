using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;
using Fury.Database;
// using Fury.Behaviors;

using Unit = Fury.Behaviors.Unit;
using Target = Fury.Behaviors.Targetable;

public class InstantHitWeapon : Weapon
{
	public UnityEngine.GameObject HitEffect = null;
	
	public override void OnAttackComplete(Fury.Behaviors.Unit attacker, Fury.Behaviors.Unit target)
	{		
		// Create hit effect
		var effect = (GameObject)
			GameObject.Instantiate(HitEffect);
		
		// Position the effect
		var position = target.transform.position;
		effect.transform.position = position;
		effect.transform.parent = target.transform;

		// Reduce the target's health
		target.ModifyHealth(-(Damage / 3), 
			attacker, 
			this);
	}
}