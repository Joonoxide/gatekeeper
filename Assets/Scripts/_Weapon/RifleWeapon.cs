using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;
using Fury.Database;

class RifleWeapon : Fury.Database.Weapon
{

	public override void OnAttackComplete(Fury.Behaviors.Unit attacker, Fury.Behaviors.Unit target)
	{	
		var position = attacker.transform.FindChild("-turret").position;
		GameObject arrow = GameController.instance.instantiateGO(GameController.instance.arrowGO, position);
		arrow.transform.localScale = new Vector3(0.02f, 0.2f, 0.2f);
		Vector3 euler = new Vector3(0, -90, 0);
		arrow.transform.eulerAngles = euler;
		var projectile = arrow.AddComponent<CurvedProjectile>();
		projectile.weapon = this;
		projectile.attacker = attacker;
		projectile.target = target;
		projectile.fp = new CurvedProjectile.FP(onArrived);
	}

	public void onArrived(Fury.Behaviors.Unit attacker, Fury.Behaviors.Unit target)
	{
		if(target && attacker)
		{
			target.ModifyHealth(-(Damage * 1), attacker, this);
			foreach(KYAgent.AbilityWrapper abilityWrapper in attacker.agent.listAbilityWrapper)
			{
				if(abilityWrapper.abilityType == AbilityType.PassiveAttack)
				{
					int rate = abilityWrapper.dictionaryAbilityData["Val1"];
					int rand = UnityEngine.Random.Range(0, 100);
					if(rand < rate)
					{
						attacker.Order(abilityWrapper.ability, target, Vector3.zero);
					}
				}
			}
		}
	}
}