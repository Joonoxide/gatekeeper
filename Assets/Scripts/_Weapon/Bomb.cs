using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

class Bomb : MonoBehaviour
{
	public Vector3 StartPoint;
	public Fury.Behaviors.Unit AttackingUnit;
	public Fury.Database.Ability WeaponSource;
	public Single FlightDuration;
	private Single Radius = 5f;
	private Single TimeElapsed;
	public int multiplier;
	public Vector3 targetedLoc;
	public Vector3 halfWay;
	bool isUp;
	public UnityEngine.GameObject HitEffect = Resources.Load("siege_hit") as UnityEngine.GameObject;
	public float sda;
	public float ySpeed = 70f;
	
	public void Start()
	{
		isUp = true;
		sda = 0.5f;
		halfWay = StartPoint+(targetedLoc-StartPoint)*sda;
		ySpeed = 40f;
	}

	public void Update()
	{
		TimeElapsed += Time.deltaTime;

		float age;
		Vector3 pos;
		if(isUp)
		{
			age = Mathf.Clamp01(TimeElapsed / (FlightDuration*sda));
			pos = Vector3.Lerp(StartPoint, halfWay, age);
			float speed = ySpeed*(1-age);
			pos.y = gameObject.transform.position.y+speed*Time.deltaTime;
			if(age > 0.99f)
			{
				isUp = false;
				TimeElapsed = 0;
			}
			if(pos.y >= 35)
			{
				pos.y = 35;
			}
		}
		else
		{
			age = Mathf.Clamp01(TimeElapsed / (FlightDuration*(1-sda)));
			pos = Vector3.Lerp(halfWay, targetedLoc, age);
			float speed = ySpeed-(ySpeed*(1-age));
			pos.y = gameObject.transform.position.y-speed*Time.deltaTime;
			if (age > 0.99f)
			{
				foreach (var cmdr in Fury.Behaviors.Manager.Instance.Commanders)
				{
					if(cmdr != AttackingUnit.Owner)
					{
						//foreach (var unit in cmdr.Units)
						foreach(KeyValuePair<int, Fury.Behaviors.Unit> unitData in cmdr.Units)
						{
							Fury.Behaviors.Unit unit = unitData.Value;
							
							if (unit.agent is WalkableAgent)
							{
								float distance = Vector3.Distance(unit.transform.position, targetedLoc);
								if(distance < Radius)
								{
									int kyRand = UnityEngine.Random.Range(0, 2);
									if(kyRand == 0)
									{
										unit.ModifyHealth(-(150 * 1), AttackingUnit, WeaponSource);
									}
									else
									{
										unit.ModifyHealth (-(200 * 1), AttackingUnit, WeaponSource);
									}
								}
								else if(distance < Radius*2)
								{
									unit.ModifyHealth(-(80 * 1), AttackingUnit, WeaponSource);
								}
							}
						}
					}
				}
				var bombHit = (GameObject)GameObject.Instantiate(HitEffect);
				var hitPosition = targetedLoc;
				bombHit.transform.position = hitPosition;
				GameObject.Destroy(gameObject);
			}
		}
		gameObject.transform.position = pos;
		
	}
}