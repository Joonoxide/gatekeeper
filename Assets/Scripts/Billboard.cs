using UnityEngine;
 
public class Billboard : MonoBehaviour
{
	// To make the object z axis look at some point:
    //public Vector3 myLookAtTarget;
	
    void Update () 
	{
    	transform.up = Vector3.up;
    	Vector3 eulerAngles = transform.localEulerAngles;
    	eulerAngles.x = -45;
    	transform.localEulerAngles = eulerAngles;
    }
}