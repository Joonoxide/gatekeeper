using System;
using System.Collections.Generic;

using UnityEngine;

public class WordsOfHonor : Fury.Database.Ability 
{
	public Fury.Database.Status StatusEffect = null;
	
	public override bool OnCheckUseOnTarget(Fury.Behaviors.Unit caster, Fury.Behaviors.Targetable target, UnityEngine.Vector3 position)
	{
		return true;
	}

	public override object OnBeginCast(Fury.Behaviors.Unit caster, Fury.Behaviors.Targetable target, UnityEngine.Vector3 position)
	{
		return null;
	}

	public override void OnEndCast(object tag, Fury.Behaviors.Unit caster, Fury.Behaviors.Targetable target, UnityEngine.Vector3 position)
	{
		(target as Fury.Behaviors.Unit).ChangeOwner(Fury.CommanderIndices.One);
			
		if (StatusEffect != null)
			(target as Fury.Behaviors.Unit).AddStatus(StatusEffect, caster);
		
		if (Effect != null)
		{
			var effect = (UnityEngine.GameObject)UnityEngine.GameObject.Instantiate(Effect);
			effect.transform.position = target.transform.position;
		}
	}
}
