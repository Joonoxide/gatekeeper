using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class CallofAngels : Fury.Database.Ability 
{
	public float Amount = 0.5f;
	public float AOE = 10f;

	public override bool OnCheckUseOnTarget(Fury.Behaviors.Unit caster, Fury.Behaviors.Targetable target, UnityEngine.Vector3 position)
	{
		return true;
	}

	public override object OnBeginCast(Fury.Behaviors.Unit caster, Fury.Behaviors.Targetable target, UnityEngine.Vector3 position)
	{
		return null;
	}

	public override void OnEndCast(object tag, Fury.Behaviors.Unit caster, Fury.Behaviors.Targetable target, UnityEngine.Vector3 position)
	{
		foreach (var cmdr in Fury.Behaviors.Manager.Instance.Commanders)
				//foreach (var unit in cmdr.Units)
				foreach(KeyValuePair<int, Fury.Behaviors.Unit> unitData in cmdr.Units)
				{
					Fury.Behaviors.Unit unit = unitData.Value;
			
					if (unit.Owner == caster.Owner)
					{
						// Check if unit is inside the radius
						if (Vector3.Distance(unit.transform.position, caster.transform.position) < AOE)
						{
							Debug.Log((Mathf.CeilToInt(unit.Properties.Health * Amount)));
							// heal the unit
							unit.ModifyHealth(Mathf.CeilToInt(unit.Properties.Health * Amount), caster, this);
							
							if (Effect != null)
							{
								var effect = (UnityEngine.GameObject)UnityEngine.GameObject.Instantiate(Effect);
								effect.transform.position = unit.transform.position;
							}
						}
					}
				}
		
	}
}
