using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

class Healing : Fury.Database.Ability
{
	public Int32 Amount = 50;

	public override bool OnCheckUseOnTarget(Fury.Behaviors.Unit caster, Fury.Behaviors.Targetable target, UnityEngine.Vector3 position)
	{
		return true;
	}

	public override object OnBeginCast(Fury.Behaviors.Unit caster, Fury.Behaviors.Targetable target, UnityEngine.Vector3 position)
	{
		//caster.Animator.CrossFade("cast-begin", 1000, CastTime, UnityEngine.WrapMode.ClampForever);
		return null;
	}

	public override void OnEndCast(object tag, Fury.Behaviors.Unit caster, Fury.Behaviors.Targetable target, UnityEngine.Vector3 position)
	{
		(target as Fury.Behaviors.Unit).ModifyHealth(Amount, caster, this);

		if (Effect != null)
		{
			var effect = (UnityEngine.GameObject)UnityEngine.GameObject.Instantiate(Effect);
			effect.transform.position = target.transform.position;
		}
	}
}