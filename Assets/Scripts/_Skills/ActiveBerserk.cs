using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ActiveBerserk : Fury.Database.Ability 
{
	public Fury.Database.Status berserkBuff;
	// Use this for initialization
	public override object OnBeginCast(Fury.Behaviors.Unit caster, Fury.Behaviors.Targetable target, UnityEngine.Vector3 position)
	{
		return null;
	}

	public override void OnEndCast(object tag, Fury.Behaviors.Unit caster, Fury.Behaviors.Targetable target, UnityEngine.Vector3 position)
	{
		foreach (var cmdr in Fury.Behaviors.Manager.Instance.Commanders)
		{
			//foreach (var unit in cmdr.Units)
			foreach(KeyValuePair<int, Fury.Behaviors.Unit> unitData in cmdr.Units)
			{
				Fury.Behaviors.Unit unit = unitData.Value;
				
				if (unit.Owner == caster.Owner && unit.gameObject.GetComponent<WalkableAgent>())
				{
					unit.AddStatus(berserkBuff, unit);
				}
			}
		}
	}
}
