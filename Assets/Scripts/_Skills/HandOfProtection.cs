using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class HandOfProtection : Fury.Database.Ability  
{
	public Fury.Database.Status StatusEffect = null;
	
	public override bool OnCheckUseOnTarget(Fury.Behaviors.Unit caster, Fury.Behaviors.Targetable target, UnityEngine.Vector3 position)
	{
		return true;
	}

	public override object OnBeginCast(Fury.Behaviors.Unit caster, Fury.Behaviors.Targetable target, UnityEngine.Vector3 position)
	{
		return null;
	}

	public override void OnEndCast(object tag, Fury.Behaviors.Unit caster, Fury.Behaviors.Targetable target, UnityEngine.Vector3 position)
	{
		foreach (var cmdr in Fury.Behaviors.Manager.Instance.Commanders)
				foreach(KeyValuePair<int, Fury.Behaviors.Unit> unitData in cmdr.Units)
				{
					Fury.Behaviors.Unit unit = unitData.Value;
			
					if (unit.Owner == caster.Owner && unit.tag != "tower")
					{
						if (StatusEffect != null)
							(target as Fury.Behaviors.Unit).AddStatus(StatusEffect, caster);
						
						if (Effect != null)
						{
							var effect = (UnityEngine.GameObject)UnityEngine.GameObject.Instantiate(Effect);
							effect.transform.position = unit.transform.position;
						}
					}
				}
		
	}
}
