﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;

namespace Fury
{
	internal class QueuedRPC
	{
		private static Dictionary<NetworkPlayer, List<QueuedRPC>> QueuedRPCs;

		static QueuedRPC()
		{
			QueuedRPCs = new Dictionary<NetworkPlayer, List<QueuedRPC>>();
		}

		private NetworkView View;
		private Single Delay;
		private String Method;
		private NetworkPlayer Target;
		private System.Object[] Args;
		private General.Action Action;

		private QueuedRPC(NetworkView view, float delay, string method, NetworkPlayer target, object[] args, General.Action action)
		{
			View = view;
			Delay = delay;
			Method = method;
			Target = target;
			Args = args;
			Action = action;
		}

		internal static void Update(Single timeStep)
		{
			foreach (var kvp in QueuedRPCs)
				for (Int32 i = 0; i < kvp.Value.Count; i++)
				{
					var rpc = kvp.Value[i];
					rpc.Delay -= timeStep;

					if (rpc.Delay <= 0 && i == 0)
					{
						if (rpc.Target == Network.player) rpc.Action();
						else rpc.View.RPC(rpc.Method, rpc.Target, rpc.Args);

						kvp.Value.RemoveAt(i--);
					}
				}
		}

		internal static void Clear()
		{
			QueuedRPCs.Clear();
		}

		internal static void Queue(NetworkView view, String method, NetworkMessageInfo info, RPCPermissions perm, General.Action callback, params System.Object[] args)
		{
			Int32 maxDelay = 0;
			foreach (var cmdr in Behaviors.Manager.Instance.Commanders)
				maxDelay = Math.Max(Network.GetAveragePing(cmdr.networkView.owner), 0);

			// Is this message from a remote user?
			if (info.timestamp > 0)
			{
				// A remote owner is trying to send a command through the server
				if (Network.isServer && perm == RPCPermissions.Owner && view.owner == info.sender)
				{
					foreach (var cmdr in Behaviors.Manager.Instance.Commanders)
						if (!cmdr.IsAI)
							QueuedRPC._Queue(view, maxDelay - Network.GetAveragePing(cmdr.networkView.owner), method, cmdr.networkView.owner, args, callback);
				}
				else
				{
					callback();
				}

				return;
			}

			if (Network.isServer)
			{
				// Server can execute any RPC
				foreach (var cmdr in Behaviors.Manager.Instance.Commanders)
					if (!cmdr.IsAI)
						QueuedRPC._Queue(view, maxDelay - Network.GetAveragePing(cmdr.networkView.owner), method, cmdr.networkView.owner, args, callback);
			}
			else if (perm == RPCPermissions.Owner && view.owner == Network.player)
			{
				// Non-server owner must first send request to server
				view.RPC(method, RPCMode.Server, args);
			}
		}
		
		private static void _Queue(NetworkView view, Int32 delay, String method, NetworkPlayer target, System.Object[] args, General.Action action)
		{
			var queuedRPC = new QueuedRPC(view, delay / 1000f, method, target, args, action);

			List<QueuedRPC> list;
			if (!QueuedRPCs.TryGetValue(target, out list))
			{
				list = new List<QueuedRPC>();
				QueuedRPCs.Add(target, list);
			}

			list.Add(queuedRPC);
		}
	}
}