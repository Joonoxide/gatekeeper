using System;
using System.Collections.Generic;

using UnityEngine;
using Fury.Controllers;

namespace Fury.Behaviors
{
	/// <summary>
	/// The manager handles all the core logic of the engine. It is necessary in order for the 
	/// engine to run properly. This class follows a singleton patern. Use Manager.Instance to
	/// access the current active manager.
	/// </summary>
	[RequireComponent(typeof(NetworkView))]
	[AddComponentMenu("Fury/Manager")]
	public sealed class Manager : MonoBehaviour
	{
		/// <summary>
		/// An empty network message, use this to signify calls originating on the local machine.
		/// </summary>
		public static NetworkMessageInfo Empty { get; private set; }

		/// <summary>
		/// The current instance of the manager. 
		/// </summary>
		public static Manager Instance { get; private set; }

		/// <summary>
		/// An array of empty bytes. Due to the idiosyncracies of the Unity networking
		/// layer, an 'empty' array is an array with 1 element, as opposed to an array
		/// with zero elements.
		/// </summary>
		internal static Byte[] EmptyArray { get; private set; }

		/// <summary>
		/// Static constructor sets up shared resources.
		/// </summary>
		static Manager()
		{
			Empty = new NetworkMessageInfo();
			EmptyArray = new Byte[1];
		}

		/// <summary>
		/// A counter used to generate unique identifiers for deposits and units.
		/// </summary>
		internal Int32 NewBodyID;
		
		/// <summary>
		/// A list of maps that can be dynamically loaded by the manager.
		/// </summary>
		public Fury.Database.Map[] AvailableMaps;

		/// <summary>
		/// The type of the navigation mesh agent to use.
		/// </summary>
		public Type NavMeshAgentType
		{
			get
			{
				return (typeof(INavMeshAgent).IsAssignableFrom(_NavMeshAgentType)) ?
					_NavMeshAgentType : typeof(UnityNavMeshAgent);
			}

			set
			{
				if (typeof(INavMeshAgent).IsAssignableFrom(value))
					_NavMeshAgentType = value;
				else
					Debug.LogWarning("Nav mesh agents must implement Fury.INavMeshAgent.");
			}
		}
		private Type _NavMeshAgentType = null;

		/// <summary>
		/// The list of definitions currently available to be used in play.
		/// </summary>
		public General.ReadOnlyDictionary<Int32, Database.Definition> AvailableDefinitions;

		/// <summary>
		/// List of commanders in the current map.
		/// </summary>
		public General.ReadOnlyList<Commander> Commanders { get; private set; }

		/// <summary>
		/// List of deposits in the current map.
		/// </summary>
		public General.ReadOnlyDictionary<Int32, Deposit> Deposits { get; private set; }

		/// <summary>
		/// The current state of the game.
		/// </summary>
		public GameStates GameState { get; private set; }

		/// <summary>
		/// The current hosted map.
		/// </summary>
		public Fury.Database.Map Map { get; private set; }

		private Byte[] LocalHailData;

		#region /// Trigger events
		/// <summary>
		/// Called when all the players have loaded into the map.
		/// </summary>
		public event General.Action OnMapLoaded;

		/// <summary>
		/// Called when the host has hosted the map.
		/// </summary>
		public event General.Action OnMapHosted;

		/// <summary>
		/// Called when a commander has joined the table.
		/// </summary>
		public event Action<Behaviors.Commander> OnCommanderJoined;

		/// <summary>
		/// Called when a commander has left the table.
		/// </summary>
		public event Action<Behaviors.Commander> OnCommanderLeft;

		/// <summary>
		/// Called by the engine when a unit is created.
		/// </summary>
		public event Action<Behaviors.Unit> OnUnitCreated;

		/// <summary>
		/// Called by the engine when a deposit is created.
		/// </summary>
		public event Action<Behaviors.Deposit> OnDepositCreated;

		/// <summary>
		/// Called by the framework when a deposit is removed.
		/// </summary>
		public event Action<Behaviors.Deposit> OnDepositRemoved;

		/// <summary>
		/// Called by the engine when a unit dies.
		/// </summary>
		public event General.DUnitDead OnUnitDead;

		/// <summary>
		/// Called by the framework when a commander orders a unit.
		/// </summary>
		public event General.DOrderGiven OnOrderGiven;

		/// <summary>
		/// Called by the framework when a unit is afflicted with a status. The first unit is the
		/// unit that affected unit, the second unit is one that applied the status.
		/// </summary>
		public event General.DStatusAdded OnStatusAdded;

		/// <summary>
		/// Called by the framework when a status on a unit is removed.
		/// </summary>
		public event General.DStatusRemoved OnStatusRemoved;

		/// <summary>
		/// Called by the framework when a unit's energy changes.
		/// </summary>
		public event General.DEnergyAmountChanged OnEnergyAmountChanged;

		/// <summary>
		/// Called by the framework when a unit enters a trigger. This event only fires for units that have
		/// RegisterForTriggers enabled under properties.
		/// </summary>
		public event General.DUnitEnterTrigger OnUnitEnterTrigger;

		/// <summary>
		/// Called by the framework when a new token stack is created.
		/// </summary>
		public event General.Action<Behaviors.Stack> OnTokenStackCreated;

		/// <summary>
		/// Called by the framework when a token stack is removed.
		/// </summary>
		public event General.Action<Behaviors.Stack> OnTokenStackRemoved;

		/// <summary>
		/// Called by the engine when an entity's tokens are changed.
		/// </summary>
		public event General.DTokenStackStateChanged OnTokenStackStateChanged;

		/// <summary>
		/// Called by the framework when one unit damages another with an ability. The first unit is
		/// the unit that took damage, the second unit is the unit that dealt damage.
		/// </summary>
		public event General.DHealthAmountChanged OnHealthAmountChanged;
		#endregion

		/// <summary>
		/// Sets up the manager.
		/// </summary>
		[System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
		private void Awake()
		{
			if (Instance != null)
			{
				UnityEngine.Debug.LogWarning("Only one manager can be initialized at a time. Self destructing!");
				GameObject.Destroy(gameObject);
				return;
			}

			Application.runInBackground = true;

			Instance = this;
			Commanders = new General.ReadOnlyList<Commander>();
			Deposits = new General.ReadOnlyDictionary<Int32, Deposit>();

			GameState = GameStates.Menu;

			GameObject.DontDestroyOnLoad(Instance.gameObject);
		}

		/// <summary>
		/// Updates the core logic of the manager.
		/// </summary>
		[System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
		private void Update()
		{
			Timer.Update();
			QueuedRPC.Update(Time.deltaTime);

			switch (GameState)
			{
				case GameStates.Playing:
					Hud.Update();

					if (!(Network.isClient || Network.isServer))
						Disconnect();
					break;

				case GameStates.Lobby:
					if (!(Network.isClient || Network.isServer))
						Disconnect();
					break;
			}
		}

		/// <summary>
		/// Fired when the user connects to a server.
		/// </summary>
		[System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
		private void OnConnectedToServer()
		{
			GameState = GameStates.Lobby;
			gameObject.networkView.RPC("Handshake", RPCMode.Server, Network.AllocateViewID(), LocalHailData);
		}

		/// <summary>
		/// Fired when the user is disconnected from the server.
		/// </summary>
		[System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
		private void OnDisconnectedFromServer()
		{
			Cleanup();
		}

		/// <summary>
		/// Fired when the user initializes a server.
		/// </summary>
		[System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
		private void OnServerInitialized()
		{
			var localIdx = GetEmptyIndex(true);

			CreateCommander(Network.AllocateViewID(), true, false, (Int32)localIdx.Value, LocalHailData);
			GameState = GameStates.Lobby;

			if (OnMapHosted != null)
				OnMapHosted();
		}

		/// <summary>
		/// Fired when a user disconnects from the server.
		/// </summary>
		/// <param name="player">The user that disconnected.</param>
		[System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
		private void OnPlayerDisconnected(NetworkPlayer player)
		{
			var cmdr = Commanders.First(c => c.gameObject.networkView.networkView.owner == player);
			if (cmdr == null) return;

			networkView.RPC("RemoveCommander", RPCMode.All, cmdr.Identifier);
		}

		[System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
		private void OnPlayerConnected(NetworkPlayer player)
		{
			if (GameState != GameStates.Lobby)
				Network.CloseConnection(player, true);
		}

		/// <summary>
		/// Host a game.
		/// </summary>
		/// <param name="port">The port to listen for connections on.</param>
		/// <param name="map">The map to host.</param>
		/// <param name="gameType">The 'type' of the game. This is a unique identifier used to differentiate applications.</param>
		/// <param name="comment">The 'comment' of the game.</param>
		/// <param name="register">True if this game should be registered with the master server, false otherwise.</param>
		/// <param name="cmdrTag">Data associated with the host commander. This is the commander's tag.</param>
		public void Host(Int32 port, Database.Map map, String gameType, String comment, Boolean register, Byte[] cmdrTag)
		{
			if (map == null)
			{
				Debug.LogWarning("You must specify a valid map to host a game.");
				return;
			}

			if (!Application.CanStreamedLevelBeLoaded(map.Scene))
			{
				Debug.LogWarning("Did you forget to add the map's associated scene to the build settings?");
				return;
			}

			if (gameType == null) gameType = "Fury." + map.Scene;

			var useNat = true;
			Debug.Log("Hosting " + map.Name + ", using NAT Punchthrough: " + useNat);

			Map = map;
			LocalHailData = (cmdrTag == null || cmdrTag.Length == 0) ? Manager.EmptyArray : cmdrTag;

			Network.InitializeSecurity();
			Network.InitializeServer(31, port, useNat && register);

			if (register) MasterServer.RegisterHost(gameType, map.Name, comment == null ? "" : comment);
		}

		/// <summary>
		/// Join a game.
		/// </summary>
		/// <param name="game">The game to join.</param>
		/// <param name="data">Additional data that will be associated with the local commander.</param>
		public void Join(HostData game, Byte[] data)
		{
			Debug.Log("Joining " + String.Join(".", game.ip) + " on port " + game.port + ".");

			// TODO: FIX Map = Fury.Database.Definition.Find<Fury.Database.Map>(game.gameName);
			LocalHailData = (data == null || data.Length == 0) ? Manager.EmptyArray : data;
			Network.Connect(game);
		}

		/// <summary>
		/// Join a game.
		/// </summary>
		/// <param name="ip">The ip address of the host.</param>
		/// <param name="port">The port the host is listening on.</param>
		/// <param name="tagData">The tag data for the local commander.</param>
		public void Join(String ip, Int32 port, Byte[] tagData)
		{
			Debug.Log("Joining " + ip + " on port " + port + ".");
			LocalHailData = (tagData == null || tagData.Length == 0) ? Manager.EmptyArray : tagData;
			Network.Connect(ip, port);
		}

		/// <summary>
		/// Disconnect from any game during any stage.
		/// </summary>
		public void Disconnect()
		{
			Network.Disconnect();
		}

		private void Cleanup()
		{
			GameState = GameStates.Menu;

			for (Int32 c = 0; c < Commanders.Count; c++)
			{
				var cmdr = Commanders[c];
				foreach (var kvp in cmdr.Units.Values)
					if (kvp != null && !kvp.IsDestroyed)
						GameObject.Destroy(kvp.gameObject);

				GameObject.Destroy(cmdr.gameObject);
			}

			Timer.Clear();
			QueuedRPC.Clear();
			Deposits.Clear();
			Commanders.Clear();
		}

		internal void Startup()
		{
			AvailableDefinitions = new General.ReadOnlyDictionary<Int32, Database.Definition>();

			var defs = (Database.Definition[])UnityEngine.Object.FindObjectsOfTypeIncludingAssets(typeof(Database.Definition));
			var counter = 0;
			foreach (var def in defs)
			{
				if (def is Database.Unit)
					counter++;

				try
				{
					AvailableDefinitions.Add(def.DefinitionID, def);
				}
				catch
				{
					Debug.LogError(def.Name + " could not be added, because name conflicts with " + AvailableDefinitions[def.DefinitionID].Name);
				}
			}  

#if TRIAL
			/*if (counter > 5)  
			{
				Debug.LogWarning("The free version of the Fury Framework is limited to 5 distinct unit types.");
				GameObject.Destroy(this);
			}
			else if (AvailableDefinitions.Count > 30)
			{
				Debug.LogWarning("The free version of the Fury Framework is limited to 30 distinct definitions.");
				GameObject.Destroy(this);
			}*/
#endif

			// Initialize all the preplaced units
			foreach (var unit in (Unit[])GameObject.FindSceneObjectsOfType(typeof(Unit)))
			{
				var cmdr = Manager.Instance.Commanders.First(c => c.Index == unit.DefaultOwner);

				if (cmdr == null)
				{
					// Destroy the unit since no commander exists at that index
					GameObject.Destroy(unit.gameObject);
					Debug.LogWarning("Destroyed an entity because Commander (" + unit.DefaultOwner + ") does not exist.");
				}
				else if (unit.Properties == null)
				{
					GameObject.Destroy(unit.gameObject);
					Debug.LogWarning("Destroyed an entity because its properties were not set.");
				}
				else
				{
					// Create the unit
					var id = NewBodyID++;
					unit.Initialize(unit.Properties, cmdr, id, null);
					cmdr.Units.Add(id, unit);
				}
			}

			// Initialize all the preplaced deposits
			foreach (var deposit in (Deposit[])GameObject.FindSceneObjectsOfType(typeof(Deposit)))
			{
				if (deposit.Properties == null)
				{
					GameObject.Destroy(deposit.gameObject);
					Debug.LogWarning("Destroyed a deposit  because its properties were not set.");
				}
				else
				{
					var id = NewBodyID++;
					deposit.Initialize(deposit.Properties, id, null);

					Manager.Instance.Deposits.Add(id, deposit);
				}
			}

			GameState = GameStates.Playing;

			if (OnMapLoaded != null)
				OnMapLoaded();
		}

		private CommanderIndices? GetEmptyIndex(Boolean humansOnly)
		{
			var available = new List<Database.Commander>();
			foreach (var cmdr in Map.Commanders)
				if ((cmdr.CommanderType == CommanderTypes.Both || cmdr.CommanderType == CommanderTypes.Player))
					available.Add(cmdr);

			for (Int32 i = 0; i < Commanders.Count; i++)
				for (Int32 a = 0; a < available.Count; a++)
					if (available[a].DefaultIndex == Commanders[i].Index)
						available.RemoveAt(a--);

			foreach (var cmdr in available)
				return cmdr.DefaultIndex;

			return null;
		}

		/// <summary>
		/// Find the unit with the specified identifier.
		/// </summary>
		/// <param name="id">The identifier of the unit.</param>
		/// <returns>A unit if one is found, null otherwise.</returns>
		public T Find<T>(Int32 id) where T : class, IEntity
		{
			if (Deposits.ContainsKey(id))
				return Deposits[id] as T;

			for (Int32 c = 0; c < Commanders.Count; c++)
			{
				var cmdr = Commanders[c];

				if (cmdr.Identifier == id)
					return cmdr as T;

				if (cmdr.Units.ContainsKey(id))
					return cmdr.Units[id] as T;
			}

			return null;
		}

		/// <summary>
		/// Create an AI commander.
		/// </summary>
		/// <param name="idx">The index on which to create the AI commander.</param>
		/// <param name="tagData">The tag data of the AI commander.</param>
		/// <returns>True if the commander was created, false otherwise.</returns>
		public Boolean CreateAICommander(CommanderIndices idx, Byte[] tagData)
		{
			if (!Network.isServer) return false;

			// Check if the slot is open
			var occupied = Commanders.First(c => c.Index == (CommanderIndices)idx);
			if (occupied != null) return false;

			// Check if the slot allows an AI
			var allowAI = false;
			foreach (var cmdr in Map.Commanders)
				if ((cmdr.DefaultIndex == (CommanderIndices)idx) && (cmdr.CommanderType == CommanderTypes.Computer || cmdr.CommanderType == CommanderTypes.Both))
					allowAI = true;

			if (!allowAI) return false;

			Debug.Log("Creating AI Commander.");

			networkView.RPC("CreateCommander", RPCMode.All, Network.AllocateViewID(), false, true, (Int32)idx,
				tagData == null || tagData.Length == 0 ? EmptyArray : tagData);

			return true;
		}

		/// <summary>
		/// Starts the game.
		/// </summary>
		/// <returns>True if the game could be started, false otherwise.</returns>
		public Boolean StartGame()
		{
			return _StartGame(false, Empty);
		}

		[RPC, System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
		internal Boolean _StartGame(Boolean debugLaunch, NetworkMessageInfo info)
		{
			// If the map doesn't exist, nothing is going to be happening
			if (Map == null)
			{
				Debug.LogWarning("A proper map wasn't found.");
				return false;
			}

			// Check if we have all the required slots filled
			foreach (var c0 in Map.Commanders)
				if (c0 != null && c0.IsRequired)
				{
					var cmdr = Commanders.First(c => c.Index == c0.DefaultIndex);
					if (cmdr == null)
					{
						Debug.LogWarning("This game cannot start without Commander " + c0.DefaultIndex + ".");
						return false;
					}
				}
			
			QueuedRPC.Queue(networkView, "_StartGame", info, RPCPermissions.Server,
				delegate
				{
					Debug.Log("Loading map...");

					if (Network.isServer) MasterServer.UnregisterHost();

					// Load up the appropriate level
					GameState = GameStates.Loading;
					NewBodyID = 100;

					if (debugLaunch)
						foreach (var cmdr in Commanders)
							cmdr.GameTime = 0;
					else
						Application.LoadLevel(Map.Scene);
				}, debugLaunch);

			return true;
		}

		/// <summary>
		/// Receieved from remote clients connecting to a server.
		/// </summary>
		/// <param name="tag">Optional byte data that is synchronized at the start of the game.</param>
		/// <param name="id">A network view id instantiated by the client designated for the client's commander.</param>
		/// <param name="info">Network message info.</param>
		[RPC, System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
		private void Handshake(NetworkViewID id, Byte[] tag, NetworkMessageInfo info)
		{
			// Get an empty index
			var idx = GetEmptyIndex(true);

			// Deny access
			if (idx == null)
			{
				Debug.LogWarning(info.sender + " tried to connect, but no slots were available.");
				Network.CloseConnection(info.sender, true);
			}

			// Tell the player the map we're using 
			gameObject.networkView.RPC("SetMap", info.sender, Map.DefinitionID);

			// Create the new commander on the server
			CreateCommander(id, false, false, (Int32)idx.Value, tag);
			var newCmdr = Commanders[Commanders.Count - 1];

			// Create the new commander on the remote
			gameObject.networkView.RPC("CreateCommander", info.sender, id, true, newCmdr.IsAI, (Int32)idx.Value, tag);

			for (Int32 c = 0; c < Commanders.Count; c++)
			{
				var cmdr = Commanders[c];
				if (cmdr.networkView.viewID != id)
				{
					// Tell the newly connected commander of all the old ones
					gameObject.networkView.RPC("CreateCommander", info.sender,
						cmdr.networkView.viewID, false, cmdr.IsAI, (Int32)cmdr.Index, cmdr.Controllers.TagController.Tag);

					// Tell the old commanders, exclusing host, of the newly connected commander
					if (!cmdr.IsLocal)
						gameObject.networkView.RPC("CreateCommander", cmdr.networkView.viewID.owner,
							newCmdr.gameObject.networkView.viewID, false, newCmdr.IsAI, (Int32)newCmdr.Index, cmdr.Controllers.TagController.Tag);
				}
			}
		}

		[RPC, System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
		private void SetMap(Int32 mapID)
		{
			if (!Network.isServer)
				foreach (var map in AvailableMaps)
				{
					if (map.DefinitionID == mapID)
					{
						Map = map;
						Debug.Log("Setting map: " + map.Name);
					}
				}
		}

		/// <summary>
		/// Create a player commander.
		/// </summary>
		/// <param name="viewID">The commander's network view identifier.</param>
		/// <param name="local">True if the commander is played by the local user.</param>
		/// <param name="ai">True if the commander is artifical.</param>
		/// <param name="idx">The index on which to create the commander.</param>
		/// <param name="tag">Optional string data that is synchronized at the start of the game.</param>
		[RPC, System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
		private void CreateCommander(NetworkViewID viewID, Boolean local, Boolean ai, Int32 idx, Byte[] tag)
		{
			if (ai)
			{
				// Check if the AI slot is open
				var occupied = Commanders.First(c => c.Index == (CommanderIndices)idx);
				if (occupied != null) return;

				// Get the commander's AI script
				Database.Commander cmdrInfo = null;
				foreach (var c in Map.Commanders)
					if ((c.DefaultIndex == (CommanderIndices)idx) && (c.CommanderType == CommanderTypes.Computer || c.CommanderType == CommanderTypes.Both))
						cmdrInfo = c;

				if (cmdrInfo == null) return;

				local = false;
			}

			var obj = new GameObject("Commander", typeof(NetworkView));
			obj.networkView.viewID = viewID;
			obj.networkView.stateSynchronization = NetworkStateSynchronization.Off;
			obj.networkView.observed = null;

			var cmdr = obj.AddComponent<Commander>();

			var team = 0;
			foreach (var c in Map.Commanders)
				if (c.DefaultIndex == (CommanderIndices)idx)
					team = c.DefaultTeam;

			obj.transform.parent = gameObject.transform;
			cmdr.Initialize((CommanderIndices)idx, local, ai, team, tag);

			Commanders.Add(cmdr);

			if (OnCommanderJoined != null)
				OnCommanderJoined(cmdr);
		}

		/// <summary>
		/// Remove a commander. This function should not be directly called, only through synchronized RPCs.
		/// </summary>
		/// <param name="id">The commander's identifier.</param>
		/// <param name="info">Network message info.</param>
		[RPC, System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
		private void RemoveCommander(Int32 id, NetworkMessageInfo info)
		{
			var cmdr = Find<Commander>(id);

			if (cmdr != null)
			{
				if (OnCommanderLeft != null)
					OnCommanderLeft(cmdr);

				foreach (var kvp in cmdr.Units.Values)
					Manager.Instance.KillUnit(kvp, Empty);

				GameObject.Destroy(cmdr.gameObject);
				Commanders.Remove(cmdr);
			}
		}

		/// <summary>
		/// Create a deposit.
		/// </summary>
		/// <param name="depType">The type of the deposit.</param>
		/// <param name="pos">The position of the deposit.</param>
		/// <param name="tag">User-defined data assigned to the deposit.</param>
		/// <returns>A deposit stub, that can be used to perform buffered actions before the deposit is created.</returns>
		public IDeposit CreateDeposit(Database.Deposit depType, Vector3 pos, Byte[] tag)
		{
			if (depType == null) return null;
			var id = NewBodyID++;
			_CreateDeposit(id, depType, pos, tag, Empty);
			return new DepositStub(id, depType);
		}

		/// <summary>
		/// Create a deposit.
		/// </summary>
		/// <param name="depId">The unique id of the deposit.</param>
		/// <param name="typeID">The type of deposit.</param>
		/// <param name="pos">The position of the deposit.</param>
		/// <param name="tag">User-defined data assigned to the deposit.</param>
		/// <param name="info">Network message info. Use Manager.Empty if calling directly.</param>
		[RPC, System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
		private void _CreateDeposit(Int32 depId, Int32 typeID, Vector3 pos, Byte[] tag, NetworkMessageInfo info)
		{
			if (GameState != GameStates.Playing) return;

			var properties = AvailableDefinitions[typeID] as Fury.Database.Deposit;

			if (properties == null) return;

			if (properties.Prefab == null)
			{
				Debug.LogWarning("The prefab (" + properties.Prefab + ") for the entity (" + properties.Name + ") does not exist.");
				return;
			}

			if (tag == null || tag.Length == 0) tag = Manager.EmptyArray;

			QueuedRPC.Queue(networkView, "_CreateDeposit", info, RPCPermissions.Server,
				delegate
				{
					var prefab = (GameObject)GameObject.Instantiate(properties.Prefab);

					var deposit = prefab.GetComponent<Deposit>();
					if (deposit == null) deposit = prefab.AddComponent<Deposit>();

					// Initialize the starting state
					deposit.Initialize(properties, depId, tag);
					prefab.transform.position = pos;

					Deposits.Add(depId, deposit);
				}, depId, typeID, pos, tag);
		}

		/// <summary>
		/// Create a new unit.
		/// </summary>
		/// <param name="unitType">The type of unit to create.</param>
		/// <param name="cmdr">The commander to create the unit for.</param>
		/// <param name="pos">The position of the unit.</param>
		/// <param name="tag">The data tag of the unit.</param>
		/// <returns>A unit stub, which can be used to perform buffered actions before the unit is actually created.</returns>
		public IUnit CreateUnit(Database.Unit unitType, Commander cmdr, Vector3 pos, Byte[] tag)
		{
			if (unitType == null || cmdr == null) return null;
			var id = NewBodyID++;
			_CreateUnit(id, unitType, cmdr, pos, tag, Empty);
			return new UnitStub(id, unitType, cmdr);
		}

		public IUnit CreateUnit(Database.Unit unitType, Commander cmdr, Vector3 pos, Byte[] tag, int level)
		{
			if (unitType == null || cmdr == null) return null;
			var id = NewBodyID++;
			_CreateUnit(id, unitType, cmdr, pos, tag, Empty, level);
			return new UnitStub(id, unitType, cmdr);
		}
		/// <summary>
		/// Create a unit for this commander.
		/// </summary>
		/// <param name="unitId">The id of the unit.</param>
		/// <param name="typeID">The type of the unit.</param>
		/// <param name="cmdrID">The identifier of the commander.</param>
		/// <param name="tag"></param>
		/// <param name="pos">The position to create the unit at.</param>
		/// <param name="info">Network information, pass in Manager.Empty if this is a local call.</param>
		[RPC, System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
		private void _CreateUnit(Int32 unitId, Int32 typeID, Int32 cmdrID, Vector3 pos, Byte[] tag, NetworkMessageInfo info)
		{
			if (GameState != GameStates.Playing) return;
			
			var cmdr = Find<Commander>(cmdrID);
			var properties = AvailableDefinitions[typeID] as Fury.Database.Unit;
			
			Debug.Log ("creating unit... cmdr " + cmdr + " prop " + properties);
			
			if (properties == null || cmdr == null) return;

			if (properties.Prefab == null)
			{
				Debug.LogWarning("The prefab for the entity (" + properties.Name + ") does not exist.");
				return;
			}

			if (tag == null || tag.Length == 0) tag = Manager.EmptyArray;

			QueuedRPC.Queue(networkView, "_CreateUnit", info, RPCPermissions.Server,
				delegate
				{
					var prefab = (GameObject)GameObject.Instantiate(properties.Prefab);
					var unit = prefab.GetComponent<Unit>();
					if (unit == null) 
					{
						if(prefab.name.Contains("source"))
						{
							unit = prefab.AddComponent<ResourceUnit>();
						}
						else
						{
							unit = prefab.AddComponent<Unit>();
						}
					}

					if (properties.RegisterForTriggers)
					{
						var body = prefab.GetComponent<Rigidbody>();
						if (rigidbody == null) body = prefab.AddComponent<Rigidbody>();
						body.isKinematic = true;
					}

					// Initialize the starting state
					unit.Initialize(properties, cmdr, unitId, tag);
					prefab.transform.position = pos;

					cmdr.Units.Add(unitId, unit);
				}, unitId, typeID, cmdrID, pos, tag);
		}

		private void _CreateUnit(Int32 unitId, Int32 typeID, Int32 cmdrID, Vector3 pos, Byte[] tag, NetworkMessageInfo info, int level)
		{
			if (GameState != GameStates.Playing) return;

			var cmdr = Find<Commander>(cmdrID);
			var properties = AvailableDefinitions[typeID] as Fury.Database.Unit;
			if (properties == null || cmdr == null) return;

			if (properties.Prefab == null)
			{
				Debug.LogWarning("The prefab for the entity (" + properties.Name + ") does not exist.");
				return;
			}

			if (tag == null || tag.Length == 0) tag = Manager.EmptyArray;

			QueuedRPC.Queue(networkView, "_CreateUnit", info, RPCPermissions.Server,
				delegate
				{
					GameObject prefab;
					int rank = ((level-1)%100)/10;
					if(rank >= 2)
					{
						prefab = (GameObject)GameObject.Instantiate(properties.Prefab3);
					}
					else if(rank >= 1)
					{
						prefab = (GameObject)GameObject.Instantiate(properties.Prefab2);
					}
					else
					{
						prefab = (GameObject)GameObject.Instantiate(properties.Prefab);
					}
					var unit = prefab.GetComponent<Unit>();
					if (unit == null) unit = prefab.AddComponent<Unit>();

					/*if(cmdrID == 0)
					{
						level += 2;
					}*/
					unit.level = level;
					KYAgent a = prefab.GetComponent<KYAgent>();
					if(level > 100)
					{
						a.isBoss = true;
						a.level = level-100;
					}
					else
					{
						a.level = level;
					}
					unit.agent = prefab.GetComponent<KYAgent>();
					if (properties.RegisterForTriggers)
					{
						var body = prefab.GetComponent<Rigidbody>();
						if (rigidbody == null) body = prefab.AddComponent<Rigidbody>();
						body.isKinematic = true;
					}

					// Initialize the starting state
					unit.Initialize(properties, cmdr, unitId, tag);
					prefab.transform.position = pos;

					cmdr.Units.Add(unitId, unit);
				}, unitId, typeID, cmdrID, pos, tag);
		}

		/// <summary>
		/// Set a tag.
		/// </summary>
		/// <param name="targetID">The identifier of the entity.</param>
		/// <param name="tag">The user-defined tag data.</param>
		/// <param name="info">Network message info. Use Manager.Empty if calling directly.</param>
		[RPC, System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
		internal void SetTag(Int32 targetID, Byte[] tag, NetworkMessageInfo info)
		{
			if (GameState != GameStates.Playing) return;

			if (tag == null || tag.Length == 0) tag = Manager.EmptyArray;

			var target = Manager.Instance.Find<IEntity>(targetID);
			var targetTagController = target.Controllers.TagController;

			if (target == null || targetTagController == null) return;

			QueuedRPC.Queue(networkView, "SetTag", info, RPCPermissions.Server,
				delegate
				{
					targetTagController.Tag = tag;
				}, targetID, tag);
		}

		/// <summary>
		/// Remove a deposit.
		/// </summary>
		/// <param name="depositID">The identifier of the deposit.</param>
		/// <param name="info">Network message info. Use Manager.Empty if calling directly.</param>
		[RPC, System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
		internal void RemoveDeposit(Int32 depositID, NetworkMessageInfo info)
		{
			if (GameState != GameStates.Playing) return;

			var deposit = Find<Deposit>(depositID);
			if (deposit == null) return;

			QueuedRPC.Queue(networkView, "RemoveDeposit", info, RPCPermissions.Server,
				delegate
				{
					Deposits.Remove(deposit.Identifier);
					deposit.Properties.OnRemoved(deposit);
					if (OnDepositRemoved != null)
						OnDepositRemoved(deposit);

					GameObject.Destroy(deposit);
					deposit.IsDestroyed = true;
				}, depositID);
		}

		/// <summary>
		/// Kill a unit for this commander.
		/// </summary>
		/// <param name="targetID">The identifier of the unit.</param>
		/// <param name="info">Network information, pass in Manager.Empty if this is a local call.</param>
		[RPC, System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
		internal void KillUnit(Int32 targetID, NetworkMessageInfo info)
		{
			if (GameState != GameStates.Playing) return;

			var target = Find<Unit>(targetID);
			if (target == null) return;

			QueuedRPC.Queue(networkView, "KillUnit", info, RPCPermissions.Server,
				delegate
				{
					if (target.Animator != null)
						target.Animator.ChangeState(AnimationStates.Death, 100, Single.MaxValue);

					foreach (Controllers.Controller controller in target.Controllers)
						controller.Dispose();

					target.Owner.Units.Remove(target);
					target.Properties.OnDead(target, target.Controllers.VitalityController.LastAttacker);
					if (OnUnitDead != null)
						OnUnitDead(target, target.Controllers.VitalityController.LastAttacker);
					GameObject.Destroy(target);
					target.IsDestroyed = true;
				}, targetID);
		}

		/// <summary>
		/// Change a unit's owning commander.
		/// </summary>
		/// <param name="targetID">The unit whose commander will change.</param>
		/// <param name="newOwnerID">The new commander.</param>
		/// <param name="info">Network information, pass in Manager.Empty if this is a local call.</param>
		[RPC, System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
		internal void ChangeOwner(Int32 targetID, Int32 newOwnerID, NetworkMessageInfo info)
		{
			if (GameState != GameStates.Playing) return;

			var target = Manager.Instance.Find<Unit>(targetID);
			var newOwner = Manager.Instance.Find<Commander>(newOwnerID);

			if (target == null || newOwner == null) return;

			QueuedRPC.Queue(networkView, "ChangeOwner", info, RPCPermissions.Server,
				delegate
				{
					target.Owner.Units.Remove(target);
					newOwner.Units.Add(target, target);
					target.Owner = newOwner;
				}, targetID, newOwnerID);
		}

		[RPC, System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
		internal void CreateStack(Int32 stackId, Int32 entityId, Int32 tokenId, Byte[] state, NetworkMessageInfo info)
		{
			if (GameState != GameStates.Playing) return;

			if (state == null || state.Length == 0) state = Manager.EmptyArray;
			var token = AvailableDefinitions[tokenId] as Fury.Database.Token;
			if (token == null) return;

			QueuedRPC.Queue(networkView, "CreateStack", info, RPCPermissions.Server,
				delegate
				{
					var target = Manager.Instance.Find<IEntity>(entityId);
					if (target == null) { return; }

					var controller = target.Controllers.TokenController;
					if (controller == null) { return; }

					var stack = new Stack(stackId, target, token, state);
					controller.Stacks.Add(stack);

					stack.Token.OnTokenStackCreated(stack);
					if (OnTokenStackCreated != null)
						OnTokenStackCreated(stack);
				}, stackId, entityId, tokenId, state);
		}

		[RPC, System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
		internal void RemoveStack(Int32 entityID, Int32 stackID, NetworkMessageInfo info)
		{
			var target = Manager.Instance.Find<IEntity>(entityID);
			if (target == null) return;
			var controller = target.Controllers.TokenController;
			if (controller == null) return;
			var stack = controller.Stacks.First(f => f.Identifier == stackID);
			if (stack == null) return;

			QueuedRPC.Queue(networkView, "RemoveStack", info, RPCPermissions.Server,
				delegate
				{
					controller.Stacks.Remove(stack);

					stack.Token.OnTokenStackRemoved(stack);
					if (OnTokenStackRemoved != null)
						OnTokenStackRemoved(stack);
				}, entityID, stackID);
		}

		/// <summary>
		/// Modify an entity's tokens.
		/// </summary>
		[RPC, System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
		internal void ModifyStackState(Int32 entityID, Int32 stackID, Int32 index, Int32 val, NetworkMessageInfo info)
		{
			if (GameState != GameStates.Playing) return;

			QueuedRPC.Queue(networkView, "ModifyStackState", info, RPCPermissions.Server,
				delegate
				{
					var target = Manager.Instance.Find<IEntity>(entityID);
					if (target == null) return;
					var controller = target.Controllers.TokenController;
					if (controller == null) return;
					var stack = controller.Stacks.First(f => f.Identifier == stackID);
					if (stack == null) return;

					var old = controller.SetStackState(stackID, (byte)index, (byte)val);

					stack.Token.OnTokenStackStateChanged(stack, (byte)index, (byte)old, (byte)val);
					if (OnTokenStackStateChanged != null)
						OnTokenStackStateChanged(stack, (byte)index, (byte)old, (byte)val);
				}, entityID, stackID, index, val);
		}

		[RPC, System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
		internal void ModifyHealth(Int32 targetID, Int32 attackerID, Int32 amt, Int32 sourceID, NetworkMessageInfo info)
		{
			if (GameState != GameStates.Playing) return;

			var target = Find<Unit>(targetID);
			var attacker = Find<Unit>(attackerID);
			var source = AvailableDefinitions[sourceID] as Database.ChangeSource;

			if (target == null || source == null) return;

			QueuedRPC.Queue(networkView, "ModifyHealth", info, RPCPermissions.Server,
				delegate
				{
					var modifier = 1f;

					foreach (var si in target.Controllers.StatusController.Statuses)
						if (si.Properties is Database.Status.IHealthChange)
							modifier *= ((Database.Status.IHealthChange)si.Properties).GetModifier(target, attacker, source, amt);

					if (attacker != null)
						foreach (var si in attacker.Controllers.StatusController.Statuses)
							if (si.Properties is Database.Status.IHealthChangeCaused)
								modifier *= ((Database.Status.IHealthChangeCaused)si.Properties).GetModifier(attacker, target, source, amt);

					amt = (Int32)(amt * modifier);

					var targetVitality = target.Controllers.VitalityController;
					var old = targetVitality.Health;
					targetVitality.Health += (Int32)amt;
					var delta = targetVitality.Health - old;
					if (delta < 0) targetVitality.LastAttacker = attacker;
					else if (delta > 0) targetVitality.LastHealer = attacker;

					if (OnHealthAmountChanged != null)
						OnHealthAmountChanged(target, attacker, source, delta);

					// Kill off the unit if its health falls below zero
					if (targetVitality.Health == 0) { KillUnit(target, Manager.Empty); }
				}, 
				targetID, attackerID, amt, sourceID);
		}

		/// <summary>
		/// Modify the energy of a unit.
		/// </summary>
		/// <param name="unitID">The unit whose energy will be modified.</param>
		/// <param name="energyID">The type of energy to be modified.</param>
		/// <param name="mod">The amount to be modified.</param>
		/// <param name="info">Networking info, use Manager.Empty if calling directly.</param>
		[RPC, System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
		internal void ModifyEnergy(Int32 unitID, Int32 energyID, Int32 mod, NetworkMessageInfo info)
		{
			if (GameState != GameStates.Playing) return;

			var unit = Find<Unit>(unitID);
			var energy = AvailableDefinitions[energyID] as Database.Energy; // Fury.Database.Definition.Find<Database.Energy>(energyID);

			if (unit == null || energy == null) return;

			var abilController = unit.Controllers.VitalityController;

			if (abilController == null) return;

			var gen = abilController.Generators[energy];

			if (gen == null) return;

			QueuedRPC.Queue(networkView, "ModifyEnergy", info, RPCPermissions.Server,
				delegate
				{
					var old = gen.Amount;
					gen.Amount += mod;
					var diff = old - gen.Amount;

					if (OnEnergyAmountChanged != null)
						OnEnergyAmountChanged(unit, energy, diff);
				}, unitID, energyID, mod);
		}

		/// <summary>
		/// Add a status effect to a target.
		/// </summary>
		/// <param name="targetID">The target to apply the status effect.</param>
		/// <param name="fromID">The caster applying the effect.</param>
		/// <param name="statusID">The status effect.</param>
		/// <param name="info">Networking info, use Manager.Empty if calling directly.</param>
		[RPC, System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
		internal void AddStatus(Int32 targetID, Int32 fromID, Int32 statusID, NetworkMessageInfo info)
		{
			if (GameState != GameStates.Playing) return;

			var status = AvailableDefinitions[statusID] as Database.Status;
			if (status == null) return;

			QueuedRPC.Queue(networkView, "AddStatus", info, RPCPermissions.Server,
				delegate
				{
					var target = Find<Unit>(targetID);
					var from = Find<Unit>(fromID);
					if (target == null) return;

					target.Controllers.StatusController.Add(status, from);
					target.Controllers.StatusController.Update(false);

					if (OnStatusAdded != null)
						OnStatusAdded(target, from, status);
				}, targetID, fromID, statusID);
		}

		/// <summary>
		/// Remove a status effect from a target.
		/// </summary>
		/// <param name="targetID">The target from which to remove the effect.</param>
		/// <param name="statusID">The status effect.</param>
		/// <param name="info">Networking info, use Manager.Empty if calling directly.</param>
		[RPC, System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
		internal void RemoveStatus(Int32 targetID, Int32 statusID, NetworkMessageInfo info)
		{
			if (GameState != GameStates.Playing) return;

			var target = Find<Unit>(targetID);
			var status = AvailableDefinitions[statusID] as Database.Status;
			if (target == null || status == null) return;

			QueuedRPC.Queue(networkView, "RemoveStatus", info, RPCPermissions.Server,
				delegate
				{
					target.Controllers.StatusController.Remove(status);
				}, targetID, statusID);
		}

		internal void RelayUnitCreated(Unit unit)
		{
			unit.Properties.OnCreated(unit);
			if (OnUnitCreated != null)
				OnUnitCreated(unit);
		}

		internal void RelayDepositCreated(Deposit deposit)
		{
			deposit.Properties.OnCreated(deposit);
			if (OnDepositCreated != null)
				OnDepositCreated(deposit);
		}

		internal void RelayOrderGiven(Unit unit, OrderTypes order)
		{
			if (OnOrderGiven != null)
				OnOrderGiven(unit, order);
		}

		internal void RelayStatusRemoved(Database.Status status, Unit target, Unit from, System.Object tag)
		{
			status.OnStatusRemoved(tag, target, from);
			if (OnStatusRemoved != null)
				OnStatusRemoved(target, from, status);
		}

		internal void RelayOnTriggerEnter(Unit unit, Collider other)
		{
			if (OnUnitEnterTrigger != null)
				OnUnitEnterTrigger(unit, other);
		}
	}
}