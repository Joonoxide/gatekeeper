﻿using System;
using System.Collections.Generic;

using System.Text;

using UnityEngine;

namespace Fury.Behaviors
{
	/// <summary>
	/// A deposit is an entity that does not belong to any player, cannot cast abilities 
	/// or have health, but can have tokens. This type of entity is ideal for 'resources'
	/// like trees, minerals, gold, etc. It can also be used for chests containing items.
	/// </summary>
	[AddComponentMenu("Fury/Deposit")]
	[ExecuteInEditMode]
	public sealed class Deposit : Targetable, IDeposit
	{
		/// <summary>
		/// The properties of the deposit.
		/// </summary>
		public Database.Deposit Properties { get { return _Properties; } }
		[SerializeField, System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
		internal Database.Deposit _Properties;

		/// <summary>
		/// The radius of the deposit.
		/// </summary>
		public override Single Radius { get { return Properties.Radius; } }

		internal void Initialize(Database.Deposit properties, Int32 id, Byte[] tag)
		{
			_Properties = properties;
			Identifier = id;

			Controllers = new Controllers.ControllerCollection();
			Controllers.Add(new Controllers.TokenController());
			Controllers.Add(new Controllers.TagController(tag));
		}

		[System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
		private void Start()
		{
			CheckUnique();
		}

		[System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
		private void Update()
		{
			if (State == UnitStates.UnloadedOrEditor)
			{
				// Set the collider for the unit
				if (Properties != null)
					CheckCollider(Properties.Radius, Properties.Height);

				// Clamp the unit to the surface
				CheckClamp();

				return;
			}
			else if (State == UnitStates.Initializing)
			{
				CheckCollider(Properties.Radius, Properties.Height);

				foreach (var controller in Controllers)
					controller.Initialize();

				State = UnitStates.Idle;

				Manager.Instance.RelayDepositCreated(this);
			}
		}

		/// <summary>
		/// Deposits are neutral towards all commanders.
		/// </summary>
		/// <param name="other">A commander.</param>
		/// <returns>Always returns true, since deposits are always neutral towards everyone.</returns>
		public override Boolean IsTeamOrNeutral(Commander other)
		{
			return true;
		}

		/// <summary>
		/// Remove this deposit from the game.
		/// </summary>
		public override void Kill()
		{
			Manager.Instance.RemoveDeposit(this, Manager.Empty);
		}
	}
}