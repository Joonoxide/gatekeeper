﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;

namespace Fury.Behaviors
{
	/// <summary>
	/// An entity that has a unique identifier and a list of implemented controllers.
	/// </summary>
	public interface IEntity
	{
		/// <summary>
		/// A unique identifier.
		/// </summary>
		Int32 Identifier { get; }

		/// <summary>
		/// A list of controllers implemented by the entity.
		/// </summary>
		Controllers.ControllerCollection Controllers { get; }

		/// <summary>
		/// Set a synchronized tag for this entity. Calling this method directly generates
		/// garbage and network traffic. Do not call every frame!
		/// </summary>
		/// <param name="tag">The byte data.</param>
		void SetTag(Byte[] tag);
	}
}