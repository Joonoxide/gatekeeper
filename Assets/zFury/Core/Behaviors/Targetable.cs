﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

using System.Text;

using UnityEngine;

namespace Fury.Behaviors
{
	/// <summary>
	/// The base class for MonoBehaviours that also implement ITargetable.
	/// </summary>
	public abstract class Targetable : MonoBehaviour, IEntity
	{
		/// <summary>
		/// A unique identifier.
		/// </summary>
		public Int32 Identifier { get; protected set; }

		/// <summary>
		/// A list of controllers implemented by the entity.
		/// </summary>
		public Controllers.ControllerCollection Controllers { get; protected set; }

		/// <summary>
		/// The current state of the targetable entity.
		/// </summary>
		public UnitStates State { get { return _State; } set { _State = value; } }
		[NonSerialized]
		private UnitStates _State = UnitStates.UnloadedOrEditor;

		/// <summary>
		/// The radius of the entity.
		/// </summary>
		public abstract Single Radius { get; }

		/// <summary>
		/// This is a user-defined tag that is NOT synchronized amongst clients. 
		/// </summary>
		public System.Object UserTag { get; set; }

		/// <summary>
		/// True if the entity should be clamped to the surface during design time.
		/// </summary>
		public Boolean IsClampedToSurface = true;

		/// <summary>
		/// Get the diplomacy status of the entity towards a commander.
		/// </summary>
		/// <param name="other">A commander.</param>
		/// <returns>True if the targetable entity is neutral or teammate of the commander.</returns>
		public abstract Boolean IsTeamOrNeutral(Commander other);

		/// <summary>
		/// True if the entity has been destroyed, false otherwise.
		/// </summary>
		public Boolean IsDestroyed { get; internal set; }

		/// <summary>
		/// Internal constructor prevents external inheritence.
		/// </summary>
		internal Targetable() : base() { }

		internal void CheckUnique()
		{
			if (!Application.isPlaying)
			{
				var targetables = gameObject.GetComponents<Fury.Behaviors.Targetable>();
				for (Int32 i = 1; i < targetables.Length; i++)
				{
					GameObject.DestroyImmediate(targetables[i]);
					Debug.LogWarning("Cannot support more than one Unit or Deposit on a GameObject.");
				}
			}
		}

		internal void CheckCollider(Single radius, Single height)
		{
			var capCollider = collider as CapsuleCollider;
			if (capCollider == null)
			{
				GameObject.DestroyImmediate(collider);
				capCollider = gameObject.AddComponent<CapsuleCollider>();
			}

			if (capCollider.isTrigger == true)
				capCollider.isTrigger = false;

			var tol = 0.01f;

			capCollider.height = height;
			capCollider.radius = radius;
			if (Vector3.Distance(capCollider.center, new Vector3(0, height / 2, 0)) > tol)
				capCollider.center = new Vector3(0, height / 2, 0);

			if (capCollider.direction != 1)
				capCollider.direction = 1;
		}

		internal void CheckClamp()
		{
			if (!Application.isPlaying && IsClampedToSurface)
			{
				Ray ray = new Ray(transform.position + Vector3.up * 10000, Vector3.down);

				var hits = Physics.RaycastAll(ray);
				var minDist = Single.MaxValue;
				var newPos = new Nullable<Vector3>();

				for (Int32 i = 0; i < hits.Length; i++)
				{
					var hit = hits[i];

					if (hit.distance < minDist && hit.collider.gameObject.isStatic)
					{
						newPos = hit.point;
						minDist = hit.distance;
					}
				}

				if (newPos != null)
				{
					var dist = Vector3.Distance(transform.position, newPos.Value);
					if (dist > 0.01f)
						transform.position = newPos.Value;
				}
			}
		}

		/// <summary>
		/// Remove the targetable entity from the game.
		/// </summary>
		public abstract void Kill();

		/// <summary>
		/// Create a new stack of tokens.
		/// </summary>
		/// <param name="token">The type of the token.</param>
		/// <param name="state">The initial state of the tokens.</param>
		public void CreateTokenStack(Database.Token token, Byte[] state)
		{
			if (token == null) return;

			Manager.Instance.CreateStack(Manager.Instance.NewBodyID++, this, token, state, Manager.Empty);
		}

		/// <summary>
		/// Remove a stack from this entity.
		/// </summary>
		/// <param name="stack">The stack to remove.</param>
		public void RemoveTokenStack(Behaviors.Stack stack)
		{
			if (stack == null) return;

			Manager.Instance.RemoveStack(this, stack.Identifier, Manager.Empty);
		}

		/// <summary>
		/// Modify the state of a stack.
		/// </summary>
		/// <param name="stack">The stack to modify.</param>
		/// <param name="index">The index of the state.</param>
		/// <param name="val">The new state.</param>
		public void ModifyTokenStackState(Behaviors.Stack stack, Byte index, Byte val)
		{
			if (stack == null) return;
			Manager.Instance.ModifyStackState(this, stack.Identifier, index, val, Manager.Empty);
		}

		/// <summary>
		/// Set a synchronized tag for this entity. Calling this method directly generates
		/// garbage and network traffic. Do not call every frame!
		/// </summary>
		/// <param name="tag">The byte data.</param>
		public void SetTag(Byte[] tag)
		{
			Manager.Instance.SetTag(Identifier, tag, Manager.Empty);
		}

		/// <summary>
		/// Check for reference equality.
		/// </summary>
		/// <param name="o">An instance of an obejct.</param>
		/// <returns>True if the two objects' references are equal, false otherwise.</returns>
		public override bool Equals(object o)
		{
			return System.Object.ReferenceEquals(this, o);
		}

		/// <summary>
		/// Get a hash code for this object.
		/// </summary>
		/// <returns>A hash code.</returns>
		public override int GetHashCode()
		{
			return Identifier;
		}

		/// <summary>
		/// Checks for reference equality between two instance. This overrides the standard
		/// behavior of Unity, which considers an object to be "null" when it is destroyed.
		/// </summary>
		/// <param name="a">The first instance.</param>
		/// <param name="b">The second instance.</param>
		/// <returns></returns>
		public static bool operator ==(Targetable a, Targetable b)
		{
			return System.Object.ReferenceEquals(a, b);
		}

		/// <summary>
		/// Checks for reference inequality between two instance. This overrides the standard
		/// behavior of Unity, which considers an object to be "null" when it is destroyed.
		/// </summary>
		/// <param name="a">The first instance.</param>
		/// <param name="b">The second instance.</param>
		/// <returns></returns>
		public static bool operator !=(Targetable a, Targetable b)
		{
			return !System.Object.ReferenceEquals(a, b);
		}

		/// <summary>
		/// Converts a targetable entity to its identifier.
		/// </summary>
		/// <param name="tar">A targetable entity.</param>
		/// <returns>The entity's identifier.</returns>
		public static implicit operator Int32(Targetable tar)
		{
			if (tar == null) return Int32.MinValue;
			return tar.Identifier;
		}
	}
}