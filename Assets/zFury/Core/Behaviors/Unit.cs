using System;
using System.Collections.Generic;

using UnityEngine;
using Fury.Controllers;

namespace Fury.Behaviors
{
	/// <summary>
	/// The unit class controls the behaviors of commander-controlled entities.
	/// </summary>
	[AddComponentMenu("Fury/Unit")]
	[ExecuteInEditMode]
	public class Unit : Targetable, IUnit
	{
		/// <summary>
		/// The owner of this unit.
		/// </summary>
		public Commander Owner { get { return _Owner; } internal set { _Owner = value; } }
		public KYAgent agent;
		[NonSerialized]
		private Commander _Owner;
		public int level = 0;
		/// <summary>
		/// A helper class used to animate the unit.
		/// </summary>
		public Animator Animator
		{
			get { return _Animator; }
			private set
			{
				_Animator = value;
				if (_Animator != null) _Animator.Parent = this;
			}
		}
		[NonSerialized]
		private Animator _Animator;

		[SerializeField, System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
		internal CommanderIndices DefaultOwner = CommanderIndices.One;

		/// <summary>
		/// The logic entity that holds data about this game object.
		/// </summary>
		public Database.Unit Properties { get { return _Properties; } }
		[SerializeField, System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
        public Database.Unit _Properties;
        public float dist;
		/// <summary>
		/// The radius of the unit.
		/// </summary>
		public override Single Radius { get { return Properties.Radius; } }
		public bool isCheck;
		
		internal void Initialize(Database.Unit entity, Commander commander, Int32 id, Byte[] tag)
		{
			Owner = commander;
			Identifier = id;
			_Properties = entity;

			Controllers = new ControllerCollection();
			Controllers.Add(new TagController(tag));
			Controllers.Add(new TokenController());
			Controllers.Add(new StatusController(this));
			Controllers.Add(new Fury.Controllers.AbilityController(this));
			Controllers.Add(new VitalityController(this));
			Controllers.Add(new MovementController(this));

			if (Properties.Weapon != null)
				Controllers.Add(new WeaponController(this));

			State = UnitStates.Initializing;
		}

		public void setObstacleAvoidance(ObstacleAvoidanceType type)
		{
			gameObject.GetComponent<NavMeshAgent>().obstacleAvoidanceType = type;
		}

		[System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
		private void Start()
		{
			CheckUnique();
		}

		[System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
		private void Update()
		{
			if (State == UnitStates.UnloadedOrEditor)
			{
				// Set the collider for the unit
				if (Properties != null)
					CheckCollider(Properties.Radius, Properties.Height);

				// Clamp the unit to the surface
				CheckClamp();

				return;
			}
			else if (State == UnitStates.Initializing)
			{
				Animator = GetComponent<Animator>();
				CheckCollider(Properties.Radius, Properties.Height);

				foreach (var controller in Controllers)
					controller.Initialize();

				State = UnitStates.Idle;

				Manager.Instance.RelayUnitCreated(this);
			}

			var unit = Properties as Fury.Database.Unit;

			foreach (Controllers.Controller controller in Controllers)
				controller.Update();

			var movCtrl = Controllers.MovementController;
			var abiCtrl = Controllers.AbilityController;
			var wepCtrl = Controllers.WeaponController;
			var staCtrl = Controllers.StatusController;

			if (staCtrl != null)
				if (staCtrl.IsStunned)
					_Order();

			// Calculate the state
			State = UnitStates.Idle;
			if (movCtrl != null)
			{
				if (movCtrl.TargetUnit != null && !movCtrl.TargetUnit.IsDestroyed)
					State = UnitStates.MovingToTarget;
				else if (movCtrl.TargetPosition != null)
					State = UnitStates.MovingToPosition;
			}

			if (wepCtrl != null)
			{
				if(wepCtrl.isAttack)
				{
					if(wepCtrl.Target != null)
					{
						State = UnitStates.AttackingUnit;
					}
					else
					{
						State = UnitStates.FakeAttacking;
					}
								dist = wepCtrl.dist;

				}			
			}

			if (abiCtrl != null)
			{
				if (abiCtrl.State != Fury.Controllers.AbilityController.States.Idle)
					State = UnitStates.CastingAbility;
					
			}

			if (movCtrl != null && Animator != null)
				Animator.ChangeState(AnimationStates.Move, 1, 0);

		}

		[System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
		private void OnTriggerEnter(Collider other)
		{
			if (Properties.RegisterForTriggers)
				Manager.Instance.RelayOnTriggerEnter(this, other);
		}

		/// <summary>
		/// Add a status to this targetable entity.
		/// </summary>
		/// <param name="status">The status to add.</param>
		/// <param name="from">The unit that added the status.</param>
		public void AddStatus(Database.Status status, Behaviors.Unit from)
		{
			Manager.Instance.AddStatus(this, from, status, Manager.Empty);
		}

		/// <summary>
		/// Remove a status from this targetable entity.
		/// </summary>
		/// <param name="status">The status to remove.</param>
		public void RemoveStatus(Database.Status status)
		{
			Manager.Instance.RemoveStatus(this, status, Manager.Empty);
		}

		/// <summary>
		/// Modify this targetable entity's health.
		/// </summary>
		/// <param name="amount">The amount to modify health by.</param>
		/// <param name="from">The unit modifying the health, can be null.</param>
		/// <param name="source">The source of the modification, can be an ability, weapon or status effect.</param>
		public void ModifyHealth(Int32 amount, Behaviors.Unit from, Database.ChangeSource source)
		{
			if (source == null) return;

			Manager.Instance.ModifyHealth(this,
				(from == null) ? Int32.MinValue : from.Identifier,
				amount, source, Manager.Empty);
		}

		/// <summary>
		/// Modify the energy of this entity.
		/// </summary>
		/// <param name="energy">The type of energy to modify.</param>
		/// <param name="amount">The change in energy.</param>
		public void ModifyEnergy(Database.Energy energy, Int32 amount)
		{
			if (energy == null || amount == 0) return;

			Manager.Instance.ModifyEnergy(this, energy, amount, Manager.Empty);
		}

		/// <summary>
		/// Change this unit's owner.
		/// </summary>
		/// <param name="index">The new owner of this unit.</param>
		public void ChangeOwner(CommanderIndices index)
		{
			foreach (var cmdr in Manager.Instance.Commanders)
				if (cmdr.Index == index)
					Manager.Instance.ChangeOwner(this, cmdr, Manager.Empty);
		}

		/// <summary>
		/// Order the unit to a position.
		/// </summary>
		/// <param name="position">The position to go to.</param>
		public void Order(Vector3 position)
		{
			Owner.OrderToPosition(this, position, Manager.Empty);
		}

		/// <summary>
		/// Order the unit to target.
		/// </summary>
		/// <param name="target">The target to attack or move to.</param>
		public void Order(Targetable target)
		{
			if (target == null) return;

			Owner.OrderToTarget(this, target.Identifier, Manager.Empty);
		}

		/// <summary>
		/// Order a unit to cast an ability.
		/// </summary>
		/// <param name="ability">The ability to cast.</param>
		/// <param name="target">The target to cast the ability on.</param>
		/// <param name="pos">The position to cast the ability on.</param>
		public void Order(Database.Ability ability, Targetable target, Vector3 pos)
		{
			if (ability == null) return;

			Owner.OrderToCast(ability, this, target == null ? Int32.MinValue : target.Identifier, pos, Manager.Empty);
		}

		/// <summary>
		/// Call with extreme caution. This is not multiplayer-synchronized!! If you want the unit to stop doing its
		/// current action, call Unit.Order(Unit.position) to stop at its current position.
		/// </summary>
		public void Stop()
		{
			_Order();
		}

		/// <summary>
		/// Remove this unit from the game.
		/// </summary>
		public override void Kill()
		{
			Manager.Instance.KillUnit(this, Manager.Empty);
		}

		/// <summary>
		/// Check whether this unit is a teammate or neutral towards the specified commander.
		/// </summary>
		/// <param name="other">A commander.</param>
		/// <returns>True if the unit belongs to a teammate or is neutral.</returns>
		public override Boolean IsTeamOrNeutral(Commander other)
		{
			return ((this as Unit).Owner.Team == other.Team);
		}

		internal void _Order()
		{
			var abiCtrl = Controllers.AbilityController;
			var wepCtrl = Controllers.WeaponController;
			var movCtrl = Controllers.MovementController;

			if (abiCtrl != null) abiCtrl.Stop();
			if (wepCtrl != null) wepCtrl.Stop();
			if (movCtrl != null) movCtrl.Stop();

			if (Animator != null)
				Animator.ChangeState(AnimationStates.Move, 11, 0.01f);
		}

		internal void _Order(Vector3 position)
		{
			var staCtrl = Controllers.StatusController;
			if (IsDestroyed || (staCtrl != null && !staCtrl.IsOrderable)) return;

			var movCtrl = Controllers.MovementController;

			_Order();

			if (movCtrl != null)
			{
				movCtrl.Start(position, 0);

				if (Animator != null)
					Animator.ChangeState(AnimationStates.Move, 11, 0.01f);
			}
		}

		internal void _Order(Targetable focusUnit)
		{
			var staCtrl = Controllers.StatusController;
			if (IsDestroyed || (staCtrl != null && !staCtrl.IsOrderable)) return;

			var movCtrl = Controllers.MovementController;
			var wepCtrl = Controllers.WeaponController;

			_Order();

			var isSelfOrAlly = focusUnit.IsTeamOrNeutral(Owner);

			if (movCtrl != null)
			{
				if (isSelfOrAlly || wepCtrl == null) movCtrl.Start(focusUnit, 0);
				else
				{
					movCtrl.Start(focusUnit, wepCtrl.Properties.Range * 0.9f);

					if (Animator != null)
						Animator.ChangeState(AnimationStates.Move, 11, 0.01f);
				}
			}

			if (wepCtrl != null && !isSelfOrAlly) wepCtrl.Start(focusUnit);
		}

		internal void _Order(Database.Ability ability, Targetable target, Vector3 position)
		{
			var staCtrl = Controllers.StatusController;
			if (staCtrl != null && !staCtrl.IsOrderable) return;

			_Order();

			var abiCtrl = Controllers.AbilityController;
			if (abiCtrl != null && abiCtrl.QueueCast(ability, target, position))
			{
				var movCtrl = Controllers.MovementController;
				if (movCtrl != null)
				{
					if (target == null) movCtrl.Start(position, ability.CastRange * 0.9f);
					else movCtrl.Start(target, ability.CastRange * 0.9f);
				}
			}
		}
	}
}