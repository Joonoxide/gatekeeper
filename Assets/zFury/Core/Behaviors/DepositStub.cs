﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Fury.Behaviors
{
	/// <summary>
	/// This is a stub created when the CreateDeposit function is called. This stub is necessary because
	/// when the CreateDeposit function is called, the deposit is not immediately created. Methods 
	/// executed on this stub are buffered and executed on the deposit when it becomes available.
	/// </summary>
	public interface IDeposit
	{
		/// <summary>
		/// The identifier of the deposit; this is used for network synchronization.
		/// </summary>
		Int32 Identifier { get; }

		/// <summary>
		/// The properties of the deposit.
		/// </summary>
		Database.Deposit Properties { get; }

		/// <summary>
		/// Create a new stack of tokens.
		/// </summary>
		/// <param name="token">The type of the token.</param>
		/// <param name="state">The initial state of the tokens.</param>
		void CreateTokenStack(Database.Token token, Byte[] state);

		/// <summary>
		/// Remove a stack from this entity.
		/// </summary>
		/// <param name="stack">The stack to remove.</param>
		void RemoveTokenStack(Behaviors.Stack stack);

		/// <summary>
		/// Modify the state of a stack.
		/// </summary>
		/// <param name="stack">The stack to modify.</param>
		/// <param name="index">The index of the state.</param>
		/// <param name="val">The new state.</param>
		void ModifyTokenStackState(Behaviors.Stack stack, Byte index, Byte val);
	}

	internal class DepositStub : IDeposit
	{
		public int Identifier { get; private set; }

		public Database.Deposit Properties { get; private set; }

		internal DepositStub(Int32 identifier, Database.Deposit properties)
		{
			Identifier = identifier;
			Properties = properties;
		}

		void IDeposit.CreateTokenStack(Database.Token token, Byte[] state)
		{
			if (token == null) return;

			Manager.Instance.CreateStack(Manager.Instance.NewBodyID++, Identifier, token, state, Manager.Empty);
		}

		void IDeposit.RemoveTokenStack(Behaviors.Stack stack)
		{
			if (stack == null) return;

			Manager.Instance.RemoveStack(Identifier, stack.Identifier, Manager.Empty);
		}

		void IDeposit.ModifyTokenStackState(Behaviors.Stack stack, Byte index, Byte val)
		{
			if (stack == null) return;

			Manager.Instance.ModifyStackState(Identifier, stack.Identifier, index, val, Manager.Empty);
		}

		public override int GetHashCode()
		{
			return Identifier;
		}
	}
}