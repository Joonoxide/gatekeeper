using System;
using System.Collections.Generic;

using UnityEngine;
using Fury.Behaviors;

namespace Fury.Behaviors
{
	/// <summary>
	/// The commander behavior is the interface into actual gameplay. This behavior implements many 
	/// methods required to control units.
	/// </summary>
	[AddComponentMenu("Fury/Commander (Internal)")]
	[RequireComponent(typeof(NetworkView))]
	public sealed class Commander : MonoBehaviour, IEntity
	{
		/// <summary>
		/// The identifier of the commander.
		/// </summary>
		public Int32 Identifier { get; private set; }

		/// <summary>
		/// The controllers implemented by the commander.
		/// </summary>
		public Controllers.ControllerCollection Controllers { get; private set; }

		/// <summary>
		/// The time spent in the game.
		/// </summary>
		internal Double GameTime = Single.MinValue;

		/// <summary>
		/// True if this commander is the local player, false otherwise.
		/// </summary>
		public Boolean IsLocal { get; internal set; }

		/// <summary>
		/// True if the commander has loaded into the map and is ready for play.
		/// </summary>
		public Boolean IsLoaded { get; private set; }

		/// <summary>
		/// True if the commander is not controlled by a human player. 
		/// </summary>
		public Boolean IsAI { get; private set; }

		/// <summary>
		/// The unique index of the commander.
		/// </summary>
		public CommanderIndices Index { get; private set; }

		/// <summary>
		/// The team this commander belongs to.
		/// </summary>
		public Int32 Team { get; private set; }

		/// <summary>
		/// A dictionary of entities owned by the commander where the key is the 
		/// commander-generated instance identifier.
		/// </summary>
		public General.ReadOnlyDictionary<Int32, Unit> Units { get; private set; }

		internal void Initialize(CommanderIndices idx, Boolean local, Boolean isAI, Int32 team, Byte[] tag)
		{
			Units = new General.ReadOnlyDictionary<Int32, Unit>();
			Index = idx;
			IsLocal = local;
			IsAI = isAI;
			Team = team;

			Identifier = (Int32)(idx);
			Controllers = new Fury.Controllers.ControllerCollection();
			Controllers.Add(new Controllers.TagController(tag));
			Controllers.Add(new Controllers.TokenController());
		}

		/// <summary>
		/// Sets up the commander.
		/// </summary>
		[System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
		private void Awake()
		{
			networkView.observed = this;
			networkView.stateSynchronization = NetworkStateSynchronization.Off;
		}

		/// <summary>
		/// Updates base logic required for the commander behavior to work properly.
		/// </summary>
		[System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
		private void Update()
		{
			GameTime += Time.deltaTime;

			// We should only set the loaded state of commanders we control
			if (GameTime > 0 && !IsLoaded && networkView.viewID.owner == Network.player)
				SetLoaded(Manager.Empty);
		}

		/// <summary>
		/// Called by Unity after a level is done loading.
		/// </summary>
		/// <param name="level">The index of the level that was loaded.</param>
		[System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
		private void OnLevelWasLoaded(Int32 level) { GameTime = 0; }

		/// <summary>
		/// Order a group of units to a position. This method tries to space out the units properly
		/// so they don't try to excessively fight for position in their swarm.
		/// </summary>
		/// <param name="group">A list of units.</param>
		/// <param name="position">The position to travel to.</param>
		public Boolean OrderGroupToPosition(IEnumerable<Unit> group, Vector3 position)
		{
			if (group == null) return false;

			// Calculate the center of mass and sort all the units by the distance to the center
			var sorted = new List<Unit>();
			var center = Vector2.zero;
			foreach (var commandable in group)
				if (commandable.Owner == this && commandable.Properties is Database.Unit)
				{
					sorted.Add(commandable);
					var pos3D = commandable.gameObject.transform.position;
					center += new Vector2(pos3D.x, pos3D.z);
				}

			if (sorted.Count == 0) return false ;

			center = center / sorted.Count;
			sorted.Sort(delegate(Unit a, Unit b)
			{
				if (a == b) return 0;

				var posA = a.gameObject.transform.position;
				var posB = b.gameObject.transform.position;

				Single d0 = (new Vector2(posA.x, posA.z) - center).sqrMagnitude;
				Single d1 = (new Vector2(posB.x, posB.z) - center).sqrMagnitude;

				return Math.Sign(d0 - d1);
			});

			// The closest unit goes in the center
			List<BoundingCircle> arrived = new List<BoundingCircle>();
			arrived.Add(new BoundingCircle(new Vector2(center.x, center.y), (sorted[0].Properties as Database.Unit).Radius, sorted[0]));

			// All additional units use a sweeping sphere algorithm to get as close as possible
			for (Int32 i = 1; i < sorted.Count; i++)
			{
				var current = sorted[i];

				var curPos3D = current.gameObject.transform.position;
				var curPos = center + (new Vector2(curPos3D.x, curPos3D.z) - center).normalized * 1000;
				var newPos = BoundingCircle.SweepSphere(new Vector2(curPos.x, curPos.y),
					new Vector2(center.x, center.y),
					(current.Properties as Database.Unit).Radius, arrived);

				arrived.Add(new BoundingCircle(newPos, (current.Properties as Database.Unit).Radius, current));
			}

			// Give out the orders
			foreach (BoundingCircle arrival in arrived)
			{
				OrderToPosition((arrival.Tag as Unit),
					new Vector3((arrival.Tag as Unit).transform.position.x, position.y, arrival.Position.y) + new Vector3(0, 0, position.z - center.y),
					Manager.Empty);
			}

			return true;
		}

		/// <summary>
		/// Test if a given commander is in the same team as this commander.
		/// </summary>
		/// <param name="cmdr">A commander to test.</param>
		/// <returns>True if the two commanders are in the same team, false otherwise;</returns>
		public Boolean IsTeam(Commander cmdr) { return cmdr.Team == Team; }

		/// <summary>
		/// Find units owned by this commander that are close enough to a specified position.
		/// </summary>
		/// <param name="position">A world position.</param>
		/// <param name="distance">The maximum inclusion radius.</param>
		/// <param name="units">A list which will be populated with units that are within the distance.</param>
		/// <returns>The number of units within the specified radius.</returns>
		public Int32 FindUnits(Vector3 position, Single distance, IList<Unit> units)
		{
			foreach (var kvp in Units.Values)
				if (!kvp.IsDestroyed)
					if ((kvp.transform.position - position).magnitude - kvp.Properties.Radius < distance)
						units.Add(kvp);

			return units.Count;
		}

		/// <summary>
		/// Set a synchronized tag for this entity. Calling this method directly generates
		/// garbage and network traffic. Do not call every frame!
		/// </summary>
		/// <param name="tag">The byte data.</param>
		public void SetTag(Byte[] tag)
		{
			Manager.Instance.SetTag(Identifier, tag, Manager.Empty);
		}

		/// <summary>
		/// Set the commander's state as loaded.
		/// </summary>
		[RPC, System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
		private void SetLoaded(NetworkMessageInfo info)
		{
			QueuedRPC.Queue(networkView, "SetLoaded", info, RPCPermissions.Owner,
				delegate
				{
					if (!IsLoaded)
					{
						IsLoaded = true;

						// Test if all the commanders are loaded
						Boolean allLoaded = Manager.Instance.Commanders.First(c => c.IsLoaded == false) == null;

						// If everyone is loaded, we should signal the scripts
						if (allLoaded) Manager.Instance.Startup();
					}
				});
		}

		/// <summary>
		/// Set the commander's index. The index is only changed if it is available.
		/// </summary>
		/// <param name="newIndex">The new index.</param>
		public void SetIndex(CommanderIndices newIndex)
		{
			_SetIndex((Int32)newIndex, Manager.Empty);
		}

		[RPC, System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
		private void _SetIndex(Int32 index, NetworkMessageInfo info)
		{
			Boolean allowChange = Manager.Instance.Commanders.First(c => c.Index == (CommanderIndices)index) == null;

			if (!allowChange) return;

			QueuedRPC.Queue(networkView, "_SetIndex", info, RPCPermissions.Owner,
				delegate
				{
					Index = (CommanderIndices)index;
					Identifier = index;
				}, index);
		}

		/// <summary>
		/// Set the commander's team.
		/// </summary>
		/// <param name="newTeam">A team.</param>
		public void SetTeam(Int32 newTeam)
		{
			_SetTeam(newTeam, Manager.Empty);
		}

		[RPC, System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
		private void _SetTeam(Int32 team, NetworkMessageInfo info)
		{
			var cmdrProperties = Manager.Instance.Map.Commanders.First(c => c.DefaultIndex == Index);
			if (cmdrProperties.LockedTeams) return;

			QueuedRPC.Queue(networkView, "_SetTeam", info, RPCPermissions.Owner,
				delegate
				{
					Team = team;
				}, team);
		}

		[RPC, System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
		internal void BeginAttack(Int32 id, NetworkMessageInfo info)
		{
			if (Manager.Instance.GameState != GameStates.Playing) return;

			var unit = Units[id];
			if (unit == null) return;

			QueuedRPC.Queue(networkView, "BeginAttack", info, RPCPermissions.Server,
				delegate
				{
					unit.Controllers.WeaponController.BeginAttack();
				}, id);
		}

		/// <summary>
		/// Order one of this commander's units to a position.
		/// </summary>
		/// <param name="unitId">The identifier of the unit given the order. Must belong to this commander.</param>
		/// <param name="position">The position to move out to.</param>
		/// <param name="info">Information about where this call is originating, specify Manager.Empty if local.</param>
		[RPC, System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
		internal void OrderToPosition(Int32 unitId, Vector3 position, NetworkMessageInfo info)
		{
			if (Manager.Instance.GameState != GameStates.Playing) return;

			QueuedRPC.Queue(networkView, "OrderToPosition", info, RPCPermissions.Owner, 
				delegate()
				{
					var src = Manager.Instance.Find<Unit>(unitId);
					if (src != null)
					{
						src._Order(position);
						Manager.Instance.RelayOrderGiven(src, OrderTypes.ToPosition);
					}
				}, unitId, position);
		}

		/// <summary>
		/// Order this commander's unit to another unit.
		/// </summary>
		/// <param name="unitId">The identifier of the unit given the order. Must belong to this commander.</param>
		/// <param name="targetID">The identifier of the target unit.</param>
		/// <param name="info">Information about where this call is originating, specify Manager.Empty if local.</param>
		[RPC, System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
		internal void OrderToTarget(Int32 unitId, Int32 targetID, NetworkMessageInfo info)
		{
			if (Manager.Instance.GameState != GameStates.Playing) return;

			QueuedRPC.Queue(networkView, "OrderToTarget", info, RPCPermissions.Owner,
				delegate
				{
					var src = Manager.Instance.Find<Unit>(unitId);
					var tgt = Manager.Instance.Find<Targetable>(targetID);

					if (src == null || tgt == null) return;

					src._Order(tgt);
					Manager.Instance.RelayOrderGiven(src, OrderTypes.ToUnit);
				}, unitId, targetID);
		}

		/// <summary>
		/// Order this commander's unit to cat an ability.
		/// </summary>
		/// <param name="abilityId">The identifier of the ability.</param>
		/// <param name="casterId">The identifier of the caster.</param>
		/// <param name="targetId">The target unit of the caster.</param>
		/// <param name="position">The target position of the caster.</param>
		/// <param name="info">Information about where this call is originating, specify Manager.Empty if local.</param>
		[RPC, System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
		internal void OrderToCast(Int32 abilityId, Int32 casterId, Int32 targetId, Vector3 position, NetworkMessageInfo info)
		{
			if (Manager.Instance.GameState != GameStates.Playing) return;

			QueuedRPC.Queue(networkView, "OrderToCast", info, RPCPermissions.Owner,
				delegate
				{
					// Validate the action
					var caster = Manager.Instance.Find<Unit>(casterId);
					var target = Manager.Instance.Find<Targetable>(targetId);

					if (caster == null) return;
				
					Fury.Database.Ability ability = null;

					for (Int32 i = 0; i < caster.Properties.Abilities.Count; i++)
						if (caster.Properties.Abilities[i].DefinitionID == abilityId)
							ability = caster.Properties.Abilities[i];

					if (ability == null) return;

					if (ability.RequiresTarget && target == null) return;

					if (!(ability.OnCheckUseOnTarget(caster, target, position) && ability.OnCheckUse(caster))) return;

					// Do the action
					caster._Order(ability, target, position);
					Manager.Instance.RelayOrderGiven(caster, OrderTypes.ToCast);
				}, abilityId, casterId, targetId, position);
		}

		/// <summary>
		/// Implicit operator that converts a Commander into its Int32 identifier.
		/// </summary>
		/// <param name="cmdr">A commander.</param>
		/// <returns>The identifier of the commander.</returns>
		public static implicit operator Int32(Commander cmdr)
		{
			if (cmdr == null) return Int32.MinValue;
			return cmdr.Identifier;
		}
	}
}