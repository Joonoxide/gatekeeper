﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Fury.Behaviors
{
	/// <summary>
	/// The launcher helps testing single player maps much simpler.
	/// </summary>
	[AddComponentMenu("Fury/Launcher")]
	public sealed class Launcher : MonoBehaviour
	{
		/// <summary>
		/// The map name to launch on start up.
		/// </summary>
		public Fury.Database.Map Map;

		/// <summary>
		/// If true, the launcher will only work when the game is run in the Editor.
		/// </summary>
		public Boolean DebugOnly = true;

		[System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
		private void Start()
		{
			if (Manager.Instance.GameState == Fury.GameStates.Menu && (Application.isEditor || !DebugOnly))
			{
				if (Map == null) return;
				
				// We can only auto host maps with human player
				Int32 reqHumans = 0;
				foreach (var cmdr in Map.Commanders)
					if (cmdr.CommanderType == Fury.CommanderTypes.Player && cmdr.IsRequired)
						reqHumans++;
				   
				if (reqHumans > 1) return;
				
				Debug.Log ("before hosting...");
				Manager.Instance.Host(UnityEngine.Random.Range(21000, 21100), 
					Map, Guid.NewGuid().ToString(), Guid.NewGuid().ToString(), false, null);
				Debug.Log ("after hosting...");
			}
		}

		[System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
		private void Update()
		{
			if (Manager.Instance.GameState == Fury.GameStates.Lobby && (Application.isEditor || !DebugOnly))
			{
				// Go through all required computer commanders
				foreach(var mapCmdr in Manager.Instance.Map.Commanders)
					if (mapCmdr.IsRequired && (mapCmdr.CommanderType == CommanderTypes.Computer || mapCmdr.CommanderType == CommanderTypes.Player))
					{
						// Check if the slot is filled with an AI
						var cmdr = Manager.Instance.Commanders.First(c => c.Index == mapCmdr.DefaultIndex);
						if (cmdr == null) Manager.Instance.CreateAICommander(mapCmdr.DefaultIndex, null);
					}

				Manager.Instance._StartGame(true, Manager.Empty);
			}
		}
	}
}