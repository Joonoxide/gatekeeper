﻿using System;
using System.Collections.Generic;

using UnityEngine;

namespace Fury.Behaviors
{
	/// <summary>
	/// The rotator behaviour is required in case the follower has a turreted weapon. The
	/// rotator allows the framework to automatically rotate parts of the follower to
	/// simulate turretted weapons.
	/// </summary>
	[AddComponentMenu("Fury/Rotator")]
	public sealed class Rotator : MonoBehaviour
	{ 		
		/// <summary>
		/// The speed, in radians per second, of the turret.
		/// </summary>
		public Single Speed;
	}
}