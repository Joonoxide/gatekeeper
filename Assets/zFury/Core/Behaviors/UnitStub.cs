﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Fury.Behaviors
{
	/// <summary>
	/// This is a stub created when the CreateUnit function is called. This stub is necessary because
	/// when the CreateUnit function is called, the unit is not immediately created. Methods executed
	/// on this stub are buffered and executed on the unit when it becomes available.
	/// </summary>
	public interface IUnit
	{
		/// <summary>
		/// The identifier of the unit; this is used for network synchronization.
		/// </summary>
		Int32 Identifier { get; }

		/// <summary>
		/// The properties of the unit.
		/// </summary>
		Database.Unit Properties { get; }

		/// <summary>
		/// The owner of the unit.
		/// </summary>
		Behaviors.Commander Owner { get; }

		/// <summary>
		/// Create a new stack of tokens.
		/// </summary>
		/// <param name="token">The type of the token.</param>
		/// <param name="state">The initial state of the tokens.</param>
		void CreateTokenStack(Database.Token token, Byte[] state);

		/// <summary>
		/// Remove a stack from this entity.
		/// </summary>
		/// <param name="stack">The stack to remove.</param>
		void RemoveTokenStack(Behaviors.Stack stack);

		/// <summary>
		/// Modify the state of a stack.
		/// </summary>
		/// <param name="stack">The stack to modify.</param>
		/// <param name="index">The index of the state.</param>
		/// <param name="val">The new state.</param>
		void ModifyTokenStackState(Behaviors.Stack stack, Byte index, Byte val);

		/// <summary>
		/// Add a status to this targetable entity.
		/// </summary>
		/// <param name="status">The status to add.</param>
		/// <param name="from">The unit that added the status.</param>
		void AddStatus(Database.Status status, Behaviors.Unit from);

		/// <summary>
		/// Remove a status from this targetable entity.
		/// </summary>
		/// <param name="status">The status to remove.</param>
		void RemoveStatus(Database.Status status);

		/// <summary>
		/// Modify this targetable entity's health.
		/// </summary>
		/// <param name="amount">The amount to modify health by.</param>
		/// <param name="from">The unit modifying the health, can be null.</param>
		/// <param name="source">The source of the modification, can be an ability, weapon or status effect.</param>
		void ModifyHealth(Int32 amount, Behaviors.Unit from, Database.ChangeSource source);

		/// <summary>
		/// Modify the energy of this entity.
		/// </summary>
		/// <param name="energy">The type of energy to modify.</param>
		/// <param name="amount">The change in energy.</param>
		void ModifyEnergy(Database.Energy energy, Int32 amount);

		/// <summary>
		/// Change this unit's owner.
		/// </summary>
		/// <param name="index">The new owner of this unit.</param>
		void ChangeOwner(CommanderIndices index);

		/// <summary>
		/// Order the unit to a position.
		/// </summary>
		/// <param name="position">The position to go to.</param>
		void Order(Vector3 position);

		/// <summary>
		/// Order the unit to target.
		/// </summary>
		/// <param name="target">The target to attack or move to.</param>
		void Order(Targetable target);

		/// <summary>
		/// Order a unit to cast an ability.
		/// </summary>
		/// <param name="ability">The ability to cast.</param>
		/// <param name="target">The target to cast the ability on.</param>
		/// <param name="pos">The position to cast the ability on.</param>
		void Order(Database.Ability ability, Targetable target, Vector3 pos);
	}

	internal class UnitStub : IUnit
	{
		public int Identifier { get; private set; }

		public Database.Unit Properties { get; private set; }

		public Behaviors.Commander Owner { get; private set; }

		internal UnitStub(Int32 identifier, Database.Unit properties, Behaviors.Commander owner)
		{
			Owner = owner;
			Identifier = identifier;
			Properties = properties;
		}

		void IUnit.CreateTokenStack(Database.Token token, Byte[] state)
		{
			if (token == null) return;

			Manager.Instance.CreateStack(Manager.Instance.NewBodyID++, Identifier, token, state, Manager.Empty);
		}

		void IUnit.RemoveTokenStack(Behaviors.Stack stack)
		{
			if (stack == null) return;

			Manager.Instance.RemoveStack(Identifier, stack.Identifier, Manager.Empty);
		}

		void IUnit.ModifyTokenStackState(Behaviors.Stack stack, Byte index, Byte val)
		{
			if (stack == null) return;

			Manager.Instance.ModifyStackState(Identifier, stack.Identifier, index, val, Manager.Empty);
		}

		void IUnit.AddStatus(Database.Status status, Behaviors.Unit from)
		{
			Manager.Instance.AddStatus(Identifier, from, status, Manager.Empty);
		}

		void IUnit.RemoveStatus(Database.Status status)
		{
			Manager.Instance.RemoveStatus(Identifier, status, Manager.Empty);
		}

		void IUnit.ModifyHealth(Int32 amount, Behaviors.Unit from, Database.ChangeSource source)
		{
			if (source == null) return;

			Manager.Instance.ModifyHealth(Identifier,
				(from == null) ? Int32.MinValue : from.Identifier,
				amount, source, Manager.Empty);
		}

		void IUnit.ModifyEnergy(Database.Energy energy, Int32 amount)
		{
			if (energy == null || amount == 0) return;

			Manager.Instance.ModifyEnergy(Identifier, energy, amount, Manager.Empty);
		}

		void IUnit.ChangeOwner(CommanderIndices index)
		{
			foreach (var cmdr in Manager.Instance.Commanders)
				if (cmdr.Index == index)
					Manager.Instance.ChangeOwner(Identifier, cmdr, Manager.Empty);
		}

		void IUnit.Order(Vector3 position)
		{
			Owner.OrderToPosition(Identifier, position, Manager.Empty);
		}

		void IUnit.Order(Targetable target)
		{
			if (target == null) return;

			Owner.OrderToTarget(Identifier, target.Identifier, Manager.Empty);
		}

		void IUnit.Order(Database.Ability ability, Targetable target, Vector3 pos)
		{
			if (ability == null) return;

			Owner.OrderToCast(ability, Identifier, target == null ? Int32.MinValue : target.Identifier, pos, Manager.Empty);
		}

		public override int GetHashCode()
		{
			return Identifier;
		}
	}
}