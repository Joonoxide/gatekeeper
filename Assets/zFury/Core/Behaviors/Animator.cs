using System;
using System.Collections.Generic;

using UnityEngine;

namespace Fury.Behaviors
{
	/// <summary>
	/// The animator component is used to add extra information to imported animations.
	/// </summary>
	[RequireComponent(typeof(Animation))]
	[AddComponentMenu("Fury/Animator")]
	public class Animator : MonoBehaviour
	{
		private Dictionary<AnimationClip, String> CachedNames;
		
		/// <summary>
		/// All speeds below this limit, the character is considered idle.
		/// </summary>
		public Single LimitIdle = 1f;

		/// <summary>
		/// All speeds below this limit but above LimitIdle, the character is considered walking.
		/// </summary>
		public Single LimitWalk = 2f;

		/// <summary>
		/// All speeds below this limit but above LimitWalk, the character is considered running.
		/// </summary>
		public Single LimitRun = 3.5f;

		/// <summary>
		/// The idle animation.
		/// </summary>
		public AnimationClip Idle;

		/// <summary>
		/// The walk animation.
		/// </summary>
		public AnimationClip Walk;

		/// <summary>
		/// The run animation.
		/// </summary>
		public AnimationClip Run;

		/// <summary>
		/// The sprint animation.
		/// </summary>
		public AnimationClip Sprint;

		/// <summary>
		/// Animations that will be played periodically while the character is in its idle state.
		/// </summary>
		public AnimationClip[] IdleBreaks;

		/// <summary>
		/// Death animations.
		/// </summary>
		public AnimationClip[] Death;

		/// <summary>
		/// Attack animations.
		/// </summary>
		public AnimationClip[] Attack;

		/// <summary>
		/// The parent unit of this animator.
		/// </summary>
		public Fury.Behaviors.Unit Parent { get; internal set;}

		/// <summary>
		/// The current state of the animator.
		/// </summary>
		public AnimationStates CurrentState { get; private set; }

		private Int32 CurrentStatePriority; 
		private Single CurrentStateDuration;
		private Single CurrentStateChangedAt;

		/// <summary>
		/// Pick a random object from the specified array.
		/// </summary>
		/// <typeparam name="T">The type, T, of the elements of the array.</typeparam>
		/// <param name="arr">An array of elements of type T.</param>
		/// <returns>A random element from the array.</returns>
		protected T Random<T>(T[] arr) 
		{
			if (arr == null || arr.Length == 0)
				return default(T);

			return arr[UnityEngine.Random.Range(0, arr.Length)];
		}

		/// <summary>
		/// Play an animation by name.
		/// </summary>
		/// <param name="name">The name of the animation to play.</param>
		/// <param name="priority">The priority of the animation; higher priority animations will override others.</param>
		/// <param name="queueMode">The way the new animation is started.</param>
		/// <param name="duration">The length of time to play the animation.</param>
		/// <param name="mode">The animation play mode.</param>
		public void CrossFade(String name, Int32 priority, Single duration, QueueMode? queueMode, WrapMode mode = WrapMode.Loop)
		{
			var state = animation[name];
			var time = Time.realtimeSinceStartup;

			if (state != null)
				if (priority > CurrentStatePriority || CurrentStateChangedAt + CurrentStateDuration < time)
				{
					CurrentStatePriority = priority;
					CurrentStateDuration = duration;
					CurrentStateChangedAt = time;
					CurrentState = AnimationStates.Custom;

					state.wrapMode = mode;

					if (queueMode == null)
						animation.CrossFade(name, 0.25f);
					else
						animation.CrossFadeQueued(name, 0.25f, queueMode.Value);
				}
		}

		/// <summary>
		/// Play an animation clip.
		/// </summary>
		/// <param name="clip">The animation clip to play.</param>
		/// <param name="mode">The wrap mode of the clip.</param>
		protected void CrossFade(AnimationClip clip, WrapMode mode = WrapMode.Loop)
		{
			if (CachedNames == null)
				CachedNames = new Dictionary<AnimationClip, String>();

			String clipName;
			if (!CachedNames.TryGetValue(clip, out clipName))
			{ 
				clipName = clip.name;
				CachedNames.Add(clip, clipName);
			}

			if (animation.GetClip(clipName) != clip)
				animation.AddClip(clip, clipName);

			animation[clipName].wrapMode = mode;
			animation.CrossFade(clipName, 0.25f);
		}
	
		internal void ChangeState(AnimationStates newState, Int32 priority, Single duration)
		{
			var time = Time.realtimeSinceStartup;

			if (priority > CurrentStatePriority || CurrentStateChangedAt + CurrentStateDuration < time)
			{
				var oldState = CurrentState;

				CurrentStatePriority = priority;
				CurrentStateDuration = duration;
				CurrentStateChangedAt = time;
				CurrentState = newState;

				OnStateChanged(oldState, newState);
			}
		}

		/// <summary>
		/// Called by the framework when an animation state is changed.
		/// </summary>
		/// <param name="oldState">The old state.</param>
		/// <param name="newState">The new state.</param>
		public virtual void OnStateChanged(AnimationStates oldState, AnimationStates newState)
		{
			switch (newState)
			{
				case AnimationStates.Move:
					var spd = (Parent.Controllers.MovementController == null) ?
						0f : Parent.Controllers.MovementController.Velocity.magnitude;
					if (spd < LimitIdle) CrossFade((Idle));
					else if (spd < LimitWalk) CrossFade((Walk));
					else if (spd < LimitRun) CrossFade((Run));
					else CrossFade((Sprint));
					break;

				case AnimationStates.Attack:
					CrossFade(Random(Attack), WrapMode.ClampForever);
					break;

				case AnimationStates.Death:
					CrossFade(Random(Death), WrapMode.ClampForever);
					break;
			}
		}
	}
}