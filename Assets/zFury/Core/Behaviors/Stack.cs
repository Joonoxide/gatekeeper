﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fury.Behaviors
{
	/// <summary>
	/// This class describes a 'stack' of a particular token. This is similar to a stack of
	/// potions, gold or even equipment in a unit's inventory.
	/// </summary>
	[Serializable]
	public class Stack
	{
		/// <summary>
		/// The internal identifier of the stack.
		/// </summary>
		public Int32 Identifier { get; private set; }

		/// <summary>
		/// The type of stack. Each stack may only have one type of token.
		/// </summary>
		public Fury.Database.Token Token { get; private set; }

		/// <summary>
		/// A set of custom defined states that will be synchronized. The states have an index
		/// from 0 to 255 and a value from 0 to 255. States of a stack can represent the
		/// number of objects in a stack (for example, 43 health potions) or the durability
		/// of an item. 
		/// </summary>
		public General.ReadOnlyList<Byte> States { get; private set; }

		/// <summary>
		/// The owner of this stack.
		/// </summary>
		public Fury.Behaviors.IEntity Owner { get; private set; }

		internal Stack(Int32 stackID, Fury.Behaviors.IEntity owner, Fury.Database.Token token, Byte[] state)
		{
			Owner = owner;
			Identifier = stackID;
			Token = token;
			States = new General.ReadOnlyList<Byte>(state, true);
		}
	}
}
