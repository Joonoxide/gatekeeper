﻿using System;
using System.Collections.Generic;
using System.Text;

using Fury.Behaviors;

namespace Fury.General
{
	/// <summary>
	/// Used by the search functions to support lamba-style queries.
	/// </summary>
	/// <typeparam name="T0">The type of the argument.</typeparam>
	/// <param name="item">The argument.</param>
	/// <returns>True if argument matches the predicate, false otherwise.</returns>
	public delegate Boolean DPredicate<T0>(T0 item);
		
	/// <summary>
	/// Generic delegate.
	/// </summary>
	public delegate void Action();

	/// <summary>
	/// Generic delegate.
	/// </summary>
	/// <typeparam name="T">The type of the argument.</typeparam>
	/// <param name="t">An argument.</param>
	public delegate void Action<T>(T t);
	
	/// <summary>
	/// Used by the OnUnitDead event.
	/// </summary>
	/// <param name="deadUnit">The unit that died.</param>
	/// <param name="lastAttacker">The last attacker of the unit.</param>
	public delegate void DUnitDead(Unit deadUnit, Unit lastAttacker);

	/// <summary>
	/// Used by the OnOrderGiven event.
	/// </summary>
	/// <param name="orderedUnit">The unit that was ordered.</param>
	/// <param name="orderType">The type of order.</param>
	public delegate void DOrderGiven(Unit orderedUnit, OrderTypes orderType);

	/// <summary>
	/// Used by the OnStatusAdded event.
	/// </summary>
	/// <param name="target">The unit that had the status added.</param>
	/// <param name="from">The unit that added the status, can be null.</param>
	/// <param name="status">The status that was added.</param>
	public delegate void DStatusAdded(Unit target, Unit from, Database.Status status);

	/// <summary>
	/// Used by the OnStatusRemoved event.
	/// </summary>
	/// <param name="target">The unit from which the status was removed.</param>
	/// <param name="from">The original unit that applied the status, can be null.</param>
	/// <param name="status">The type of status that was removed.</param>
	public delegate void DStatusRemoved(Unit target, Unit from, Database.Status status);

	/// <summary>
	/// Used by the OnEnergyAmountChanged event.
	/// </summary>
	/// <param name="target">The unit that had its energy changed.</param>
	/// <param name="energy">The type of energy that was changed.</param>
	/// <param name="amount">The amount of energy that was changed.</param>
	public delegate void DEnergyAmountChanged (Unit target, Database.Energy energy, Int32 amount);

	/// <summary>
	/// Used by the OnUnitEnterTrigger event.
	/// </summary>
	/// <param name="unit">The unit that entered the trigger.</param>
	/// <param name="other">The collider of the trigger.</param>
	public delegate void DUnitEnterTrigger (Unit unit, UnityEngine.Collider other);

	/// <summary>
	/// Used by the OnTokenStackStateChagned event.
	/// </summary>
	/// <param name="stack">The stack that was changed.</param>
	/// <param name="index">The index of the state.</param>
	/// <param name="oldVal">The old value of the state.</param>
	/// <param name="newVal">The new value of the state.</param>
	public delegate void DTokenStackStateChanged(Behaviors.Stack stack, Byte index, Byte oldVal, Byte newVal);

	/// <summary>
	/// Used by the OnHealthAmountChanged event.
	/// </summary>
	/// <param name="target">The unit that had its health changed.</param>
	/// <param name="changer">The unit that changed the health, can be null.</param>
	/// <param name="src">The source of the change; can be a weapon, ability or status.</param>
	/// <param name="amount">The amount of health that was changed.</param>
	public delegate void DHealthAmountChanged(Unit target, Unit changer, Database.ChangeSource src, Int32 amount);
}