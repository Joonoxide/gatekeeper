﻿using System;
using System.Collections.Generic;

using System.Text;

namespace Fury.General
{
    /// <summary>
    /// A read-only dictionary.
    /// </summary>
    /// <typeparam name="TKey">The type of keys in the dictionary.</typeparam>
    /// <typeparam name="TValue">The type of values in the dictionary.</typeparam>
    [Serializable]
//#if MOBILE
    public class ReadOnlyDictionary<TKey, TValue> : Dictionary<TKey, TValue>, IEnumerable<TValue>
    {
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return this.Values.GetEnumerator();
        }

        IEnumerator<TValue> IEnumerable<TValue>.GetEnumerator()
        {
            return this.Values.GetEnumerator();
        }
    }
/*#else
	public class ReadOnlyDictionary<TKey, TValue> : IEnumerable<TValue>
	{
		/// <summary>
		/// A special enumerator that minimizes allocations. This enumerator uses object pooling
		/// and wraps around the native value-type enumerator that causes boxing in foreach statements.
		/// This enumerator also buffers the dictionary's values which allows for modification of the
		/// collection during enumeration. 
		/// </summary>
		public class ValueEnumerator : IEnumerator<TValue>
		{
			private static List<ValueEnumerator> Pool = new List<ValueEnumerator>();

			internal static ValueEnumerator Create(Dictionary<TKey, TValue> dictionary)
			{
				ValueEnumerator e;
				if (Pool.Count == 0)
				{
					e = new ValueEnumerator();
				}
				else
				{
					e = Pool[Pool.Count - 1];
					Pool.RemoveAt(Pool.Count - 1);
				}

				e.Set(dictionary.Values);
				return e;
			}

			private List<TValue> Values;

			private Int32 Position;

			/// <summary>
			/// Get the element at the current position.
			/// </summary>
			public TValue Current { get { return Values[Position]; } }

			object System.Collections.IEnumerator.Current { get { return Values[Position]; } }

			private ValueEnumerator() { Values = new List<TValue>(); }

			private void Set(IEnumerable<TValue> vals)
			{
				Position = -1;
				Values.Clear();
				Values.AddRange(vals);
			}

			/// <summary>
			/// Dispose of the enumerator.
			/// </summary>
			public void Dispose() { Pool.Add(this); }

			/// <summary>
			/// Move the enumerator to the next position.
			/// </summary>
			/// <returns></returns>
			public bool MoveNext() { return ++Position < Values.Count; }

			/// <summary>
			/// Reset the enumerator.
			/// </summary>
			public void Reset() { Position = -1; }
		}

		private Dictionary<TKey, TValue> Items;

		/// <summary>
		/// Retrieve a value by its key.
		/// </summary>
		/// <param name="key">The key of the value.</param>
		/// <returns>The value associated wit the key. If no value is found, the default value of T is returned.</returns>
		public TValue this[TKey key]
		{
			get
			{
				TValue output;
				if (Items.TryGetValue(key, out output))
					return output;

				return default(TValue);
			}

			internal set
			{
				if (Items.ContainsKey(key)) Items[key] = value;
				else Items.Add(key, value);
			}
		}

		/// <summary>
		/// Get a collection containing the keys of the dictionary.
		/// </summary>
		public Dictionary<TKey, TValue>.KeyCollection Keys { get { return Items.Keys; } }
		
		/// <summary>
		/// The number of keys in the dictionary.
		/// </summary>
		public Int32 Count { get { return Items.Keys.Count; } }

        /// <summary>
        /// The values of this collection.
        /// </summary>
        public IEnumerable<TValue> Values { get { return this; } }

		internal ReadOnlyDictionary(IDictionary<TKey, TValue> items)
			: this()
		{
			if (items == null) return;

			foreach (var kvp in items)
				Items.Add(kvp.Key, kvp.Value);
		}

		internal ReadOnlyDictionary()
		{
			Items = new Dictionary<TKey, TValue>();
		}

		internal void Add(TKey key, TValue val)
		{
			Items.Add(key, val);
		}

		internal void Remove(TKey key)
		{
			Items.Remove(key);
		}

		internal void Clear()
		{
			Items.Clear();
		}

		/// <summary>
		/// Try to get the value associated with the specified key.
		/// </summary>
		/// <param name="key">The key of the value to get.</param>
		/// <param name="val">Contains the value of the key when the method returns.</param>
		/// <returns>True if a value was found, false otherwise.</returns>
		public Boolean TryGetValue(TKey key, out TValue val)
		{
			return Items.TryGetValue(key, out val);
		}

		/// <summary>
		/// Determines whether the dictionary contains the key.
		/// </summary>
		/// <param name="key">The key to locate.</param>
		/// <returns>True if the key exists in the dictionary, false otherwise.</returns>
		public Boolean ContainsKey(TKey key)
		{
			return Items.ContainsKey(key);
		}
		
		System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
		{
			return ValueEnumerator.Create(Items);
		}

		IEnumerator<TValue> IEnumerable<TValue>.GetEnumerator()
		{
			return ValueEnumerator.Create(Items);
		}
	}
#endif*/
}