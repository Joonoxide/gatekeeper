﻿using System;
using System.Collections.Generic;

using System.Text;

using UnityEngine;

namespace Fury.General
{
	/// <summary>
	/// A binary heap, useful for sorting data and priority queues.
	/// </summary>
	/// <typeparam name="T"><![CDATA[IComparable<T> type of item in the heap]]>.</typeparam>
	public class Heap<T> : ICollection<T> where T : class, IComparable<T>
	{
		private const int DEFAULT_SIZE = 1024;

		/// <summary>
		/// An array that holds the heap elements.
		/// </summary>
		private T[] Data = new T[DEFAULT_SIZE];

		private bool Sorted;

		/// <summary>
		/// Gets the number of values in the heap. 
		/// </summary>
		public int Count { get { return _Count; } }
		private int _Count = 0;

		/// <summary>
		/// Gets or sets the capacity of the heap.
		/// </summary>
		public int Capacity
		{
			get { return _Capacity; }
			set
			{
				int previousCapacity = _Capacity;
				_Capacity = Math.Max(value, _Count);
				if (_Capacity != previousCapacity)
				{
					T[] temp = new T[_Capacity];
					Array.Copy(Data, temp, _Count);
					Data = temp;
				}
			}
		}
		private int _Capacity = DEFAULT_SIZE;
				
		/// <summary>
		/// Creates a new binary heap.
		/// </summary>
		public Heap() { }

		private Heap(T[] data, int count)
		{
			Capacity = count;
			_Count = count;
			Array.Copy(data, Data, count);
		}

		/// <summary>
		/// Gets the first value in the heap without removing it.
		/// </summary>
		/// <returns>The lowest value of type TValue.</returns>
		public T Peek()
		{
			return Data[0];
		}

		/// <summary>
		/// Removes all items from the heap.
		/// </summary>
		public void Clear()
		{
			this._Count = 0;
			Data = new T[_Capacity];
		}

		/// <summary>
		/// Adds a key and value to the heap.
		/// </summary>
		/// <param name="item">The item to add to the heap.</param>
		public void Add(T item)
		{
			if (_Count == _Capacity)
			{
				Capacity *= 2;
			}
			Data[_Count] = item;
			UpHeap();
			_Count++;
		}

		/// <summary>
		/// Removes and returns the first item in the heap.
		/// </summary>
		/// <returns>The next value in the heap.</returns>
		public T Remove()
		{
			if (this._Count == 0)
			{
				throw new InvalidOperationException("Cannot remove item, heap is empty.");
			}
			T v = Data[0];
			_Count--;
			Data[0] = Data[_Count];
			Data[_Count] = default(T); //Clears the Last Node
			DownHeap();
			return v;
		}

		//helper function that performs up-heap bubbling
		private void UpHeap()
		{
			Sorted = false;
			int p = _Count;
			T item = Data[p];
			int par = Parent(p);
			while (par > -1 && item.CompareTo(Data[par]) < 0)
			{
				Data[p] = Data[par]; //Swap nodes
				p = par;
				par = Parent(p);
			}
			Data[p] = item;
		}

		//helper function that performs down-heap bubbling
		private void DownHeap()
		{
			Sorted = false;
			int n;
			int p = 0;
			T item = Data[p];
			while (true)
			{
				int ch1 = Child1(p);
				if (ch1 >= _Count) break;
				int ch2 = Child2(p);
				if (ch2 >= _Count)
				{
					n = ch1;
				}
				else
				{
					n = Data[ch1].CompareTo(Data[ch2]) < 0 ? ch1 : ch2;
				}
				if (item.CompareTo(Data[n]) > 0)
				{
					Data[p] = Data[n]; //Swap nodes
					p = n;
				}
				else
				{
					break;
				}
			}
			Data[p] = item;
		}

		private void EnsureSort()
		{
			Sorted = true;
			if (Sorted) return;

			// if (Sorted) return;
			// Array.Sort(Data, 0, _Count);
			// Sorted = true;
		}

		//helper function that calculates the parent of a node
		private static int Parent(int index)
		{
			return (index - 1) >> 1;
		}

		//helper function that calculates the first child of a node
		private static int Child1(int index)
		{
			return (index << 1) + 1;
		}

		//helper function that calculates the second child of a node
		private static int Child2(int index)
		{
			return (index << 1) + 2;
		}

		/// <summary>
		/// Creates a new instance of an identical binary heap.
		/// </summary>
		/// <returns>A BinaryHeap.</returns>
		public Heap<T> Copy()
		{
			return new Heap<T>(Data, _Count);
		}

		/// <summary>
		/// Gets an enumerator for the binary heap.
		/// </summary>
		/// <returns>An IEnumerator of type T.</returns>
		public IEnumerator<T> GetEnumerator()
		{
			EnsureSort();
			for (int i = 0; i < _Count; i++)
			{
				yield return Data[i];
			}
		}

		System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		/// <summary>
		/// Checks to see if the binary heap contains the specified item.
		/// </summary>
		/// <param name="item">The item to search the binary heap for.</param>
		/// <returns>A boolean, true if binary heap contains item.</returns>
		public bool Contains(T item)
		{
			EnsureSort();
			return Array.BinarySearch<T>(Data, 0, _Count, item) >= 0;
		}

		/// <summary>
		/// Copies the binary heap to an array at the specified index.
		/// </summary>
		/// <param name="array">One dimensional array that is the destination of the copied elements.</param>
		/// <param name="arrayIndex">The zero-based index at which copying begins.</param>
		public void CopyTo(T[] array, int arrayIndex)
		{
			EnsureSort();
			Array.Copy(Data, array, _Count);
		}

		/// <summary>
		/// Gets whether or not the binary heap is readonly.
		/// </summary>
		public bool IsReadOnly { get { return false; } }

		/// <summary>
		/// Removes an item from the binary heap. This utilizes the type T's Comparer and will not remove duplicates.
		/// </summary>
		/// <param name="item">The item to be removed.</param>
		/// <returns>Boolean true if the item was removed.</returns>
		public bool Remove(T item)
		{
			EnsureSort();
			int i = Array.BinarySearch<T>(Data, 0, _Count, item);
			if (i < 0) return false;
			Array.Copy(Data, i + 1, Data, i, _Count - i);
			Data[_Count] = default(T);
			_Count--;
			return true;
		}
	}
}