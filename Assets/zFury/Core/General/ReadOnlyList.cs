﻿using System;
using System.Collections.Generic;

using System.Text;

namespace Fury.General
{
    /// <summary>
    /// A read-only list.
    /// </summary>
    /// <typeparam name="T">The type of the elements in the list.</typeparam>
    [Serializable]
//#if MOBILE
    public class ReadOnlyList<T> : List<T>
    {
        internal ReadOnlyList() { }

        internal ReadOnlyList(IEnumerable<T> items, Boolean removeNull)
            : base()
        {
            if (items != null)
            {
                foreach (var item in items)
                    if (item != null)
                        Add(item);
            }
        }

        /// <summary>
        /// Search the list and find the first element using the predicate. This function is equivalent
        /// to the LINQ query FirstOrDefault.
        /// </summary>
        /// <param name="predicate">A predicate, which can be a lambda function.</param>
        /// <returns>An element if one is found, otherwise default(T).</returns>
        public T First(DPredicate<T> predicate)
        {
            for (Int32 i = 0; i < Count; i++)
                if (predicate.Invoke(this[i]))
                    return this[i];

            return default(T);
        }
    }
/*#else
    public class ReadOnlyList<T> : IEnumerable<T>
    {
        /// <summary>
        /// A special enumerator for the read only list that minimizes memory allocations 
        /// during foreach loops.
        /// </summary>
        public class Enumerator : IEnumerator<T>
        {
            private static List<Enumerator> Pool = new List<Enumerator>();

            internal static Enumerator Create(List<T>.Enumerator enumerator)
            {
                Enumerator e;
                if (Pool.Count == 0)
                {
                    e = new Enumerator();
                }
                else
                {
                    e = Pool[Pool.Count - 1];
                    Pool.RemoveAt(Pool.Count - 1);
                }

                e._Enumerator = enumerator;
                return e;
            }

            private List<T>.Enumerator _Enumerator;

            /// <summary>
            /// Gets the element at the current position of the enumerator.
            /// </summary>
            public T Current { get { return _Enumerator.Current; } }

            object System.Collections.IEnumerator.Current { get { return _Enumerator.Current; } }

            /// <summary>
            /// Disposes of the enumerator.
            /// </summary>
            public void Dispose() { Pool.Add(this); }

            /// <summary>
            /// Moves to the next position.
            /// </summary>
            /// <returns>True if the move was successful, false if the collection ended.</returns>
            public bool MoveNext() { return _Enumerator.MoveNext(); }

            /// <summary>
            /// Resets the enumerator, not implemented.
            /// </summary>
            public void Reset() { throw new NotImplementedException(); }
        }

        private List<T> Items;

        /// <summary>
        /// Gets the element at the specified index.
        /// </summary>
        /// <param name="index">A zero-based index.</param>
        /// <returns>The element at the specified index.</returns>
        public T this[Int32 index] { get { return Items[index]; } internal set { Items[index] = value; } }

        /// <summary>
        /// Gets the number of elements in the list.
        /// </summary>
        public Int32 Count { get { return Items.Count; } }

        internal ReadOnlyList()
        {
            Items = new List<T>();
        }

        internal ReadOnlyList(IEnumerable<T> items, Boolean removeNull)
        {
            if (items == null) Items = new List<T>();
            else Items = new List<T>(items);

            if (removeNull)
            {
                for (Int32 i = 0; i < Items.Count; i++)
                    if (Items[i] == null)
                        Items.RemoveAt(i--);
            }
        }

        internal void Insert(Int32 index, T item)
        {
            Items.Insert(index, item);
        }

        internal void Add(T val)
        {
            Items.Add(val);
        }

        internal void Add(IEnumerable<T> vals)
        {
            Items.AddRange(vals);
        }

        internal Boolean Remove(T value)
        {
            return Items.Remove(value);
        }

        internal void RemoveAt(Int32 index)
        {
            Items.RemoveAt(index);
        }

        internal void Clear()
        {
            Items.Clear();
        }

        /// <summary>
        /// Search the list and find the first element using the predicate. This function is equivalent
        /// to the LINQ query FirstOrDefault.
        /// </summary>
        /// <param name="predicate">A predicate, which can be a lambda function.</param>
        /// <returns>An element if one is found, otherwise default(T).</returns>
        public T First(DPredicate<T> predicate)
        {
            for (Int32 i = 0; i < Items.Count; i++)
                if (predicate.Invoke(Items[i]))
                    return Items[i];

            return default(T);
        }

        /// <summary>
        /// Determines whether the list contains the element.
        /// </summary>
        /// <param name="value">The object to locate int the list.</param>
        /// <returns>True if the list contains the value, false otherwise.</returns>
        public Boolean Contains(T value)
        {
            return Items.Contains(value);
        }

        /// <summary>
        /// Returns an enumerater that can be used to inumerate the the list.
        /// </summary>
        /// <returns>An enumerator.</returns>
        public IEnumerator<T> GetEnumerator()
        {
            return Enumerator.Create(Items.GetEnumerator());
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return Enumerator.Create(Items.GetEnumerator());
        }
    }
#endif*/
}