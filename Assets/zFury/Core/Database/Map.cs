using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

using UnityEngine;

namespace Fury.Database
{
	/// <summary>
	/// Holds static data about a map.
	/// </summary>
	public sealed class Map : Definition
	{
		/// <summary>
		/// The default commanders found on the map.
		/// </summary>
		public General.ReadOnlyList<Commander> Commanders { get; private set; }
		[SerializeField, System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
		private Commander[] _Commanders = new Commander[0];

		/// <summary>
		/// The scene to load for the map.
		/// </summary>
		public String Scene { get { return _Scene; } }
		[SerializeField, System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
		private String _Scene = "";

		[System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
		private void OnEnable()
		{
			for (Int32 i = 0; i < _Commanders.Length; i++)
				_Commanders[i].DefaultIndex = (CommanderIndices)i;
			Commanders = new General.ReadOnlyList<Commander>(_Commanders, true);
		}
	}
}