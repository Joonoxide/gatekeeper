﻿using System;

using System.Collections.Generic;
using System.Text;

namespace Fury.Database
{
	/// <summary>
	/// The root class from which all definitions derrive from.
	/// </summary>
	public class Definition : UnityEngine.ScriptableObject
	{
		/// <summary>
		/// The unique identifier of this definition.
		/// </summary>
		public Int32 DefinitionID
		{
			get
			{
				_DefinitionID = (this.GetType().FullName + Name).GetHashCode();
				return _DefinitionID;
			}
		}

		private Int32 _DefinitionID;

		/// <summary>
		/// The name of the definition.
		/// </summary>
		public String Name { get { return name; } }
		
		/// <summary>
		/// Get a unique hashcode for this definition.
		/// </summary>
		/// <returns>A unique hashcode guaranteed to not cause collisions with other definitions.</returns>
		public override Int32 GetHashCode()
		{
			return DefinitionID;
		}

		/// <summary>
		/// A definition can be found by its identifier.
		/// </summary>
		/// <param name="i">The definition.</param>
		/// <returns>The identifier of the definition.</returns>
		public static implicit operator Int32(Definition i)
		{
			return i.DefinitionID;
		}
	}
}