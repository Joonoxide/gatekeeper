﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;

namespace Fury.Database
{
	/// <summary>
	/// Base definition for physical objects such as deposits and units.
	/// </summary>
	public class Targetable : Fury.Database.Definition
	{
		/// <summary>
		/// The prefab used by this unit.
		/// </summary>
		public GameObject Prefab { get { return _Prefab; } }
		[UnityEngine.SerializeField, System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
		private GameObject _Prefab = null;

		public GameObject Prefab2 { get { return _Prefab2; } }
		[UnityEngine.SerializeField, System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
		private GameObject _Prefab2 = null;

		public GameObject Prefab3 { get { return _Prefab3; } }
		[UnityEngine.SerializeField, System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
		private GameObject _Prefab3 = null;
		/// <summary>
		/// The icon name or path of the entity. This field is optional.
		/// </summary>
		public Texture2D Icon { get { return _Icon; } }
		[UnityEngine.SerializeField, System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
		private Texture2D _Icon = null;

		/// <summary>
		/// The radius of the entity.
		/// </summary>
		public Single Radius { get { return _Radius; } }
		[UnityEngine.SerializeField, System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
		private Single _Radius = 0.5f;

		/// <summary>
		/// The height of the entity.
		/// </summary>
		public Single Height { get { return _Height; } }
		[UnityEngine.SerializeField, System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
		private Single _Height = 0.5f;
	}
}