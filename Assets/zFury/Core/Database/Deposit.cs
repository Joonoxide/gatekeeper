﻿using System;
using System.Collections.Generic;

using System.Text;

namespace Fury.Database
{
	/// <summary>
	/// A deposit is an entity that does not belong to any player, cannot cast abilities 
	/// or have health, but can have tokens. This type of entity is ideal for 'resources'
	/// like trees, minerals, gold, etc. It can also be used for chests containing items.
	/// </summary>
	public class Deposit : Targetable
	{
		/// <summary>
		/// Called by the framework when the deposit is removed from the game.
		/// </summary>
		/// <param name="deposit">The deposit that was removed.</param>
		public virtual void OnRemoved(Behaviors.Deposit deposit)
		{
			UnityEngine.GameObject.Destroy(deposit.gameObject);
		}

		/// <summary>
		/// Called by the framework when the deposit is created.
		/// </summary>
		/// <param name="deposit">The deposit that was created.</param>
		public virtual void OnCreated(Behaviors.Deposit deposit) { }
	}
}