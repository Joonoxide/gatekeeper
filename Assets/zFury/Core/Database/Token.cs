﻿using System;
using System.Collections.Generic;

using System.Text;

namespace Fury.Database
{
	/// <summary>
	/// Defines a token, which is somewhat like an item. Tokens can be used for points,
	/// currencies, resources or items.
	/// </summary>
	public class Token : ChangeSource
	{
		/// <summary>
		/// The icon of the token, this field is optional.
		/// </summary>
		public UnityEngine.Texture2D Icon;

		/// <summary>
		/// Fired when a stack containing this type of tokens is changed.
		/// </summary>
		/// <param name="stack">The stack that was changed.</param>
		/// <param name="index">The index of the state.</param>
		/// <param name="oldVal">The old value of the state.</param>
		/// <param name="newVal">The new value of the state.</param>
		public virtual void OnTokenStackStateChanged(Behaviors.Stack stack, Byte index, Int16 oldVal, Int16 newVal) { }

		/// <summary>
		/// Fired when a stack is created.
		/// </summary>
		/// <param name="stack">The stack that was created.</param>
		public virtual void OnTokenStackCreated(Behaviors.Stack stack) { }

		/// <summary>
		/// Fired when a stack is removed.
		/// </summary>
		/// <param name="stack">The stack that was removed.</param>
		public virtual void OnTokenStackRemoved(Behaviors.Stack stack) { }
	}
}