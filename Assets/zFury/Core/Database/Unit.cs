﻿using System;
using System.Collections.Generic;

using System.Text;

using UnityEngine;

namespace Fury.Database
{
	/// <summary>
	/// Defines a unit. Units are entities that can move.
	/// </summary>
	public class Unit : Targetable
	{
		/// <summary>
		/// If set to true, the unit fires Manager.OnUnitEnterRegion events.
		/// </summary>
		public Boolean RegisterForTriggers { get { return _RegisterForTriggers; } }
		[UnityEngine.SerializeField, System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
		private Boolean _RegisterForTriggers = true;
		public int weigh;
		/// <summary>
		/// The turning rate of the unit, defined in degrees per second.
		/// </summary>
		public Single TurnRate { get { return _TurnRate; } }
		[UnityEngine.SerializeField, System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
		private Single _TurnRate = 360f;

		public Single cost { get { return _cost; } }
		[UnityEngine.SerializeField, System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
		public Single _cost = 5f;

		/// <summary>
		/// The move rate of the unit, defined in units per second.
		/// </summary>
		public Single MoveRate { get { return _MoveRate; } }
		[UnityEngine.SerializeField, System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
		public Single _MoveRate = 5f;

		/// <summary>
		/// The acceleration of the unit, defined in units per second per second.
		/// </summary>
		public Single AccelRate { get { return _AccelRate; } }
		[UnityEngine.SerializeField, System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
		private Single _AccelRate = 20f;

		/// <summary>
		/// The health of the entity.
		/// </summary>
		public Int32 Health { get { return _Health; } }
		[UnityEngine.SerializeField, System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
		public Int32 _Health = 150;

		/// <summary>
		/// Defines any energy the entity possesses and regenerates. This is similar to mages having mana in RPG games.
		/// </summary>
		public General.ReadOnlyList<Generator> Generators { get; private set; }
		[UnityEngine.SerializeField, System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
		private Generator[] _Generators = new Generator[0];

		/// <summary>
		/// The abilities this entity possesses.
		/// </summary>
		public General.ReadOnlyList<Ability> Abilities { get; private set; }
		[UnityEngine.SerializeField, System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
		private Ability[] _Abilities = new Ability[0];

		public General.ReadOnlyList<Dictionary<Fury.Database.Unit, float>> arrayWeight { get; private set; }
		[UnityEngine.SerializeField, System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
		private Dictionary<Fury.Database.Unit, float>[] _arrayWeight = new Dictionary<Fury.Database.Unit, float>[0];

		/*
		public General.ReadOnlyList<string> arrayWeigh { get; private set; }
		[UnityEngine.SerializeField, System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
		private string[] _arrayWeigh = new string[0];
		*/

		/// <summary>
		/// The weapon of the entitiy. This value can be null.
		/// </summary>
		public Weapon Weapon { get { return _Weapon; } }
		[UnityEngine.SerializeField, System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
		public Weapon _Weapon = null;

		/// <summary>
		/// Called by the engine when a unit with these properties is created.
		/// </summary>
		/// <param name="created">The created unit.</param>
		public virtual void OnCreated(Fury.Behaviors.Unit created) { }

		/// <summary>
		/// Called by the engine when a unit with these properties dies.
		/// </summary>
		/// <param name="corpse">The unit that died.</param>
		/// <param name="killer">The unit that killed it.</param>
		public virtual void OnDead(Fury.Behaviors.Unit corpse, Fury.Behaviors.Unit killer)
		{
			try { GameObject.Destroy(corpse.collider); }
			catch { }
		}
		public int maximum;
		public int genre;
		/// <summary>
		/// If overriden, please call base.OnEnable() at the begining of the overriding function
		/// or really terrible things will happen.
		/// </summary>
		protected virtual void OnEnable()
		{
			Generators = new General.ReadOnlyList<Generator>(_Generators, true);
			Abilities = new General.ReadOnlyList<Ability>(_Abilities, true);
		}

		public int trigger;
		public int costToUpgrade;
		public float getWeight(List<KYAgent.SpawnWrapper> array)
		{
			float weight = 0;

			foreach(KYAgent.SpawnWrapper spawnWrapper in array)
			{

			}
			return weight;
		}
	}
}