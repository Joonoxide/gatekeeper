﻿using System;
using System.Collections.Generic;

using System.Text;

using UnityEngine;

namespace Fury.Database
{
	/// <summary>
	/// This class describes the base functions necessary to create an ability.
	/// </summary>
	public abstract class Ability : ChangeSource
	{
		/// <summary>
		/// The icon texture of the ability. This field is optional.
		/// </summary>
		public Texture2D Icon { get { return _Icon; } }
		[SerializeField, System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
		private Texture2D _Icon = null;

		/// <summary>
		/// A variable to store the effect. This field is optional.
		/// </summary>
		public GameObject Effect { get { return _Effect; } }
		[SerializeField, System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
		private GameObject _Effect = null;

		/// <summary>
		/// The cast time of the ability in seconds.
		/// </summary>
		public Single CastTime { get { return _CastTime; } }
		[SerializeField, System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
		public Single _CastTime = 1;

		/// <summary>
		/// True if the ability can be cast while the unit is moving.
		/// </summary>
		public Boolean CastMobile { get { return _CastMobile; } }
		[SerializeField, System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
		private Boolean _CastMobile = false;

		/// <summary>
		/// The cast range of the ability.
		/// </summary>
		public Single CastRange { get { return _CastRange; } }
		[SerializeField, System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
        public Single _CastRange = 5f;
		
		/// <summary>
		/// The cooldown of the ability in seconds.
		/// </summary>
		public Single CastCooldown { get { return _CastCooldown; } }
		[SerializeField, System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
        public Single _CastCooldown = 1f;

		/// <summary>
		/// True if the ability requires a target, false otherwise.
		/// </summary>
		public Boolean RequiresTarget { get { return _RequiresTarget; } }
		[SerializeField, System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
		private Boolean _RequiresTarget = false;

		/// <summary>
		/// Called by the engine to check whether an ability is usable at all.
		/// </summary>
		/// <param name="caster">The unit that will cast the ability.</param>
		/// <returns>True if the ability can be used, false otherwise.</returns>
		public virtual bool OnCheckUse(Behaviors.Unit caster) { return true; }

		/// <summary>
		/// Called by the engine to check whether the ability is usable on a target or location.
		/// </summary>
		/// <param name="caster">The unit that will cast the ability.</param>
		/// <param name="target">The target unit of the ability.</param>
		/// <param name="position">The target position of the ability.</param>
		/// <returns>True if the ability can be used on the target, false otherwise.</returns>
		public virtual bool OnCheckUseOnTarget(Behaviors.Unit caster, Behaviors.Targetable target, Vector3 position) { return true; }

		/// <summary>
		/// Called by the engine when the ability has begun to be cast.
		/// </summary>
		/// <param name="caster">The unit that will cast the ability.</param>
		/// <param name="target">The target unit of the ability.</param>
		/// <param name="position">The target position of the ability.</param>
		/// <returns>An optional user-defined tag that is *not* synchronized across clients.</returns>
		public virtual System.Object OnBeginCast(Behaviors.Unit caster, Behaviors.Targetable target, Vector3 position) { return null; }

		/// <summary>
		/// Called by the engine when the ability is being cast or channeled.
		/// </summary>
		/// <param name="tag">An optional user-defined tag that is *not* synchronized across clients.</param>
		/// <param name="time">The time left before the channel is complete.</param>
		/// <param name="caster">The unit that is casting the ability.</param>
		/// <param name="target">The target unit of the ability.</param>
		/// <param name="position">The target position of the ability.</param>
		/// <returns>An optional user-defined tag that is *not* synchronized across clients. The tag will be passed along to methods.</returns>
		public virtual System.Object OnChannel(System.Object tag, Single time, Behaviors.Unit caster, Behaviors.Targetable target, Vector3 position) { return tag; }

		/// <summary>
		/// Called by the engine when the ability channel has been interrupted.
		/// </summary>
		/// <param name="tag">An optional user-defined tag that is *not* synchronized across clients.</param>
		/// <param name="caster">The unit that was casting the ability.</param>
		/// <param name="target">The target unit of the ability.</param>
		/// <param name="position">The target position of the ability.</param>
		public virtual void OnInterruptedCast(System.Object tag, Behaviors.Unit caster, Behaviors.Targetable target, Vector3 position) { }
		
		/// <summary>
		/// Called by the engine when the ability channel has been completed.
		/// </summary>
		/// <param name="tag">An optional user-defined tag that is *not* synchronized across clients.</param>
		/// <param name="caster">The unit that was casting the ability.</param>
		/// <param name="target">The target unit of the ability.</param>
		/// <param name="position">The target position of the ability.</param>
		/// <returns>An optional user-defined tag that is *not* synchronized across clients. The tag will be passed along if the ability launches a projectile.</returns>
		public virtual void OnEndCast(System.Object tag, Behaviors.Unit caster, Behaviors.Targetable target, Vector3 position) { }
	}
}