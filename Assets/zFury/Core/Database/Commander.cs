﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fury.Database
{
	/// <summary>
	/// Defines a potential commander on a map.
	/// </summary>
	[Serializable]
	public sealed class Commander
	{
		/// <summary>
		/// Commanders can be set to human only, computer only or both.
		/// </summary>
		public CommanderTypes CommanderType { get { return _CommanderType; } }
		[UnityEngine.SerializeField, System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
		private CommanderTypes _CommanderType = CommanderTypes.Both;

		/// <summary>
		/// True if this commander is required for the game to launch.
		/// </summary>
		public Boolean IsRequired { get { return _IsRequired; } }
		[UnityEngine.SerializeField, System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
		private Boolean _IsRequired = false;

		/// <summary>
		/// The default team of the commander
		/// </summary>
		public Int32 DefaultTeam { get { return _DefaultTeam; } }
		[UnityEngine.SerializeField, System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
		private Int32 _DefaultTeam = 0;

		/// <summary>
		/// If true, this commander cannot change its teams.
		/// </summary>
		public Boolean LockedTeams { get { return _LockedTeams; } }
		[UnityEngine.SerializeField, System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
		private Boolean _LockedTeams = false;

		/// <summary>
		/// The index of this commander, assigned automatically.
		/// </summary>
		public CommanderIndices DefaultIndex { get; internal set; }
	}
}