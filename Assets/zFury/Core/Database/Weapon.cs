﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fury.Database
{
	/// <summary>
	/// Defines a weapon that hits instantly after the delay period. There is no projectile involved.
	/// </summary>
	public class Weapon : ChangeSource
	{
		/// <summary>
		/// Can the weapon be fired when the unit is moving.
		/// </summary>
		public Boolean Mobile { get { return _Mobile; } }
		[UnityEngine.SerializeField, System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
		internal Boolean _Mobile = false;

		/// <summary>
		/// The range of the weapon.
		/// </summary>
		public Single Range { get { return _Range; } }
		[UnityEngine.SerializeField, System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
		public Single _Range = 0.5f;

		/// <summary>
		/// The time, in seconds before the weapon is used. If the weapon is not mobile, the 
		/// entity will not move during this time.
		/// </summary>
		public Single PreDelay { get { return _PreDelay; } }
		[UnityEngine.SerializeField, System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
        public Single _PreDelay = 0.25f;

		/// <summary>
		/// The time, in seconds before the weapon is put away. If the weapon is not mobile,
		/// the entity will not move during this time.
		/// </summary>
		public Single PostDelay { get { return _PostDelay; } }
		[UnityEngine.SerializeField, System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
        public Single _PostDelay = 0.25f;

		/// <summary>
		/// The time before the weapon can fire again. The entity can move during this time.
		/// </summary>
		public Single Cooldown { get { return _Cooldown; } }
		[UnityEngine.SerializeField, System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
        public Single _Cooldown = 1f;

		/// <summary>
		/// The damage dealt by the weapon.
		/// </summary>
		public Int32 Damage { get { return _Damage; } }
		[UnityEngine.SerializeField, System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
        public Int32 _Damage = 10;

		/// <summary>
		/// Called by the framework when a unit begins attacking another unit. The default implementation
		/// of this method does nothing.
		/// </summary>
		/// <param name="attacker">The unit that is the attacker.</param>
		/// <param name="target">The unit that is the target.</param>
		public virtual void OnAttackBegin(Behaviors.Unit attacker, Behaviors.Unit target) { }

		/// <summary>
		/// Called by the framework when a unit has finished attacking another unit. The default 
		/// implementation of this method will damage the target according to the attacker's weapon.
		/// </summary>
		/// <param name="attacker">The unit that was the attacker.</param>
		/// <param name="target">The unit that was the target.</param>
		public virtual void OnAttackComplete(Behaviors.Unit attacker, Behaviors.Unit target)
		{
			target.ModifyHealth(-Damage, attacker, this);
		}
	}
}