﻿using System;
using System.Collections.Generic;

using System.Text;

namespace Fury.Database
{
	/// <summary>
	/// A status is an effect that is applied to units, similar to 'buffs' and 'debuffs' in RPG style games.
	/// </summary>
	public abstract class Status : ChangeSource
	{
		/// <summary>
		/// The engine will prevent the unit from being ordered around. This is useful
		/// for 'confuse' or 'berserk' type effects where the unit becomes unresponsive to orders.
		/// </summary>
		public interface IOrderable
		{
			/// <summary>
			/// Called by the engine to determine if the unit can be ordered by its commander.
			/// </summary>
			/// <param name="from">The unit that applied the effect.</param>
			/// <param name="target">The unit the status effect is applied to.</param>
			/// <returns>True if the unit can be controlled, false otherwise.</returns>
			Boolean IsOrderable(Behaviors.Unit from, Behaviors.Unit target);
		}

		/// <summary>
		/// The engine will modify the maximum health of the character by the 
		/// given amount. The maximum health will not fall below one.
		/// </summary>
		public interface IHealth
		{
			/// <summary>
			/// Called by the engine to determine the maximum health of the target.
			/// </summary>
			/// <param name="from">The unit that applied the effect.</param>
			/// <param name="target">The unit the status effect is applied to.</param>
			/// <returns>The amount of health that should be added to the target's maximum health.</returns>
			Int32 GetModifier(Behaviors.Unit from, Behaviors.Unit target);
		}

		/// <summary>
		/// The engine will modify the maximum speed of the character. A value of
		/// zero will freeze the character, while a value of two would make the
		/// character twice as fast.
		/// </summary>
		public interface ISpeed
		{
			/// <summary>
			/// Called by the engine to calculate the speed modifier of the target.
			/// </summary>
			/// <param name="from">The unit that applied the effect.</param>
			/// <param name="target">The unit the status effect is applied to.</param>
			/// <returns>The percent modification to be applied to the speed of the target.</returns>
			Single GetModifier(Behaviors.Unit from, Behaviors.Unit target);
		}

		public interface IASpeed
		{
			/// <summary>
			/// Called by the engine to calculate the speed modifier of the target.
			/// </summary>
			/// <param name="from">The unit that applied the effect.</param>
			/// <param name="target">The unit the status effect is applied to.</param>
			/// <returns>The percent modification to be applied to the speed of the target.</returns>
			Single GetModifier(Behaviors.Unit from, Behaviors.Unit target);
		}

		/// <summary>
		/// This interface can be used to amplify the change in health (healing or damage) to this unit.
		/// </summary>
		public interface IHealthChange
		{
			/// <summary>
			/// Called by the engine when a unit's health is being changed.
			/// </summary>
			/// <param name="target">The entity whose health will be changed.</param>
			/// <param name="changer">The unit that will change the health.</param>
			/// <param name="source">The source that will change the health; can be a weapon, ability or status.</param>
			/// <param name="baseAmount">The base amount of change.</param>
			/// <returns>The amount to amplify the change by.</returns>
			Single GetModifier(Behaviors.Unit target, Behaviors.Unit changer, Database.ChangeSource source, Int32 baseAmount);
		}

		/// <summary>
		/// This interface can be used to amplify the change in health (healing or damage) caused by this unit.
		/// </summary>
		public interface IHealthChangeCaused
		{
			/// <summary>
			/// Called by the engine when a unit changes another unit's health.
			/// </summary>
			/// <param name="changer">The unit that is changing the health.</param>
			/// <param name="target">The unit whose health will be changed.</param>
			/// <param name="source">The source of the change; can be a weapon, ability or status.</param>
			/// <param name="baseAmount">The base amount of change.</param>
			/// <returns>The amount to amplify the change by.</returns>
			Single GetModifier(Behaviors.Unit changer, Behaviors.Unit target, Database.ChangeSource source, Int32 baseAmount);
		}
		
		/// <summary>
		/// The engine will stun the unit, preventing any action and cancelling all casts and orders.
		/// </summary>
		public interface IStun
		{
			/// <summary>
			/// Called by the engine to determine if this status effect stuns the target.
			/// </summary>
			/// <param name="from">The unit that applied the effect.</param>
			/// <param name="target">The unit the status effect is applied to.</param>
			/// <returns>True if the target should be stunned, false otherwise.</returns>
			Boolean IsStunned(Behaviors.Unit from, Behaviors.Unit target);
		}

		/// <summary>
		/// The engine periodically allows the status to run code.
		/// </summary>
		public interface IPeriodic
		{
			/// <summary>
			/// The period of time between the update function calls.
			/// </summary>
			Single Period { get; }

			/// <summary>
			/// Called by the engine periodically.
			/// </summary>
			/// <param name="tag">An optional user-defined tag. The tag is *not* synchronized between clients.</param>
			/// <param name="unit">The unit afflicted by the status effect.</param>
			/// <param name="from">The unit that originally applied the status effect.</param>
			/// <returns>An optional, user-defined tag that will be passed onto subsequent update calls.</returns>
			System.Object OnUpdate(System.Object tag, Behaviors.Unit unit, Behaviors.Unit from);
		}

		/// <summary>
		/// The graphical effect associated with this status. This is an optional field.
		/// </summary>
		public UnityEngine.GameObject Effect { get { return _Effect; } }
		[UnityEngine.SerializeField, System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
		private UnityEngine.GameObject _Effect = null;

		/// <summary>
		/// The duration of the status.
		/// </summary>
		public Single Duration { get { return _Duration; } }
		[UnityEngine.SerializeField, System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
		public Single _Duration = 4f;

		/// <summary>
		/// Called by the engine when this status effect is applied to a unit.
		/// </summary>
		/// <param name="target">The unit onto which the status effect was applied.</param>
		/// <param name="from">The unit that applied the status effect.</param>
		/// <returns>An optional application defined tag, that is *not* synchronized between clients.</returns>
		public virtual System.Object OnStatusAdded(Behaviors.Unit target, Behaviors.Unit from) { return null; }

		/// <summary>
		/// Called by the engine when this status effect is removed from a unit.
		/// </summary>
		/// <param name="tag">An optional application defined tag, that is *not* synchronized between clients.</param>
		/// <param name="target">The unit from which the status effect was removed.</param>
		/// <param name="from">The unit that originally applied the status effect.</param>
		public virtual void OnStatusRemoved(System.Object tag, Behaviors.Unit target, Behaviors.Unit from) { }
	}
}