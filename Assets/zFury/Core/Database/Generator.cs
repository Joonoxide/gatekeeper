﻿using System;
using System.Collections.Generic;

using System.Text;

namespace Fury.Database
{
	/// <summary>
	/// A generator defines an entity's "energy bar." This is similar to common energy types
	/// like mana, stamina, etc. A unit can have multiple energy types.
	/// </summary>
	public class Generator : Definition
	{
		/// <summary>
		/// The energy this generator uses.
		/// </summary>
		public Energy Energy { get { return _Energy; } }
		[UnityEngine.SerializeField, System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
		private Energy _Energy = null;

		/// <summary>
		/// The total capacity the entity can pool up.
		/// </summary>
		public Int32 Capacity { get { return _Capacity; } }
		[UnityEngine.SerializeField, System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
		private Int32 _Capacity = 100;

		/// <summary>
		/// The duration between regenerations.
		/// </summary>
		public Single RegenPeriod { get { return _RegenPeriod; } }
		[UnityEngine.SerializeField, System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
		private Single _RegenPeriod = 1f;

		/// <summary>
		/// The amount to be regenerated per period, can be negative.
		/// </summary>
		public Int32 RegenAmount { get { return _RegenAmount; } }
		[UnityEngine.SerializeField, System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
		private Int32 _RegenAmount = 1;
	}
}