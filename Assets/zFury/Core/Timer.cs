﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fury
{
	/// <summary>
	/// The timer is a helper class used to execute timed events.
	/// </summary>
	public class Timer
	{
		private Single Delay, Duration;
		private Delegate Callback;
		private System.Object[] State;

		/// <summary>
		/// Create a timed event.
		/// </summary>
		/// <param name="delay">The time before the event is executed.</param>
		/// <param name="duration">The time, in seconds, during which the callback will be continuously called. Pass
		/// in a value of zero or negative to execute the callback just once.</param>
		/// <param name="callback">The callback that will be called.</param>
		/// <param name="state">A user-defined state to pass back to the callback function.</param>
		internal Timer(Single delay, Single duration, Delegate callback, params System.Object[] state)
		{
			Delay = delay;
			Duration = duration;
			Callback = callback;
			State = state;
		}

		private static List<Timer> Timers;

		static Timer()
		{
			Timers = new List<Timer>();
		}
		
		/// <summary>
		/// Create a timed event.
		/// </summary>
		/// <param name="delay">The time before the event is executed.</param>
		/// <param name="duration">The time, in seconds, during which the callback will be continuously called. Pass
		/// in a value of zero or negative to execute the callback just once.</param>
		/// <param name="callback">The callback method that will be executed after the delay.</param>
		/// <param name="state">A user-defined state to pass back to the callback function.</param>
		public static Timer Start(Single delay, Single duration, Delegate callback, params System.Object[] state)
		{
			var t = new Timer(delay, duration, callback, state);
			Timers.Add(t);
			return t;
		}

		/// <summary>
		/// Clear all timed events.
		/// </summary>
		public static void Clear()
		{
			Timers.Clear();
		}

		/// <summary>
		/// Remove a specific timed event.
		/// </summary>
		/// <param name="t">The timed event to remove.</param>
		/// <returns>True if the event was successfully removed, false otherwise.</returns>
		public static Boolean Remove(Timer t)
		{
			return (t == null) ? false : Timers.Remove(t);
		}

		/// <summary>
		/// Update the timed events, and execute them as necessary.
		/// </summary>
		internal static void Update()
		{
			for (Int32 t = 0; t < Timers.Count; t++)
			{
				var e = Timers[t];

				e.Delay -= UnityEngine.Time.deltaTime;

				if (e.Delay < 0)
				{
					try
					{
						e.Callback.DynamicInvoke(e.State);
					}
					catch (Exception ex)
					{
						UnityEngine.Debug.LogError("Timer could not execute callback method.");
						UnityEngine.Debug.LogError(ex);
					}

					e.Duration -= UnityEngine.Time.deltaTime;
					Timers[t] = e;

					if (e.Duration <= 0)
						Timers.RemoveAt(t--);
				}
			}
		}
	}
}