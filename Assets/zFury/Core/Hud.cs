﻿using System;
using System.Collections.Generic;
using UnityEngine;

using Fury.Behaviors;

namespace Fury
{
	/// <summary>
	/// Helper class to make user-interface tasks a little bit easier.
	/// </summary>
	public static class Hud
	{
		/// <summary>
		/// True if shift was down in the last frame, false otherwise.
		/// </summary>
		public static Boolean Shift { get; private set; }

		/// <summary>
		/// True if the left mouse button was clicked. If this trigger is used, call ConsumeLMB()
		/// to insure that one click isn't processed multiple times by overlapping components.
		/// </summary>
		public static Boolean TriggerLMB { get; private set; }

		/// <summary>
		/// True if the right mouse button was clicked. If this trigger is used, call ConsumeRMB()
		/// to insure that one click isn't processed multiple times by overlapping components.
		/// </summary>
		public static Boolean TriggerRMB { get; private set; }

		/// <summary>
		/// An abilit that was queued up with QueueAbility().
		/// </summary>
		public static Database.Ability QueuedAbility { get; private set; }

		/// <summary>
		/// A list of units that can cast the QueuedAbility.
		/// </summary>
		public static General.ReadOnlyList<Behaviors.Unit> QueuedAbilityCasters { get; private set; }

		/// <summary>
		/// The event is fired when the user completes a drag action on the screen.
		/// </summary>
		public static event Fury.General.Action<Rect> OnDragComplete;

		/// <summary>
		/// The position of the mouse pointer that is constrained to the screen.
		/// </summary>
		public static Vector2 Position { get; private set; }

		/// <summary>
		/// The rate at which the mouse moves.
		/// </summary>
		public static Single SensitivityMouse { get; set; }

		/// <summary>
		/// The selection box, in screen space. If the user isn't in the process of drag selecting units,
		/// this value is set to null.
		/// </summary>
		public static Rect? SelectionBox { get; private set; }

		/// <summary>
		/// The position that the user's mouse is focusing on, in world space.
		/// </summary>
		public static Vector3 FocusPoint { get; set; }

		/// <summary>
		/// The target that the user's mouse is focusing on.
		/// </summary>
		public static Targetable FocusTarget { get; private set; }

		/// <summary>
		/// The layer mask that units use.
		/// </summary>
		public static LayerMask UnitMask { get; set; }

		private static Plane[] PlaneBuffer = new Plane[4];
		private static Vector2? MouseDownAt;
		private static Boolean LMBDownLastFrame, RMBDownLastFrame;

		static Hud()
		{
			SensitivityMouse = 40f;
			Position = new Vector2(Screen.width, Screen.height) * 0.5f;
			QueuedAbilityCasters = new General.ReadOnlyList<Unit>();
		}

		/// <summary>
		/// Consume the left mouse button click so multiple scripts aren't run off the same trigger.
		/// </summary>
		public static void ConsumeLMB() { TriggerLMB = false; }

		/// <summary>
		/// Consume the right mouse button click so multiple scripts aren't run off the same trigger.
		/// </summary>
		public static void ConsumeRMB() { TriggerRMB = false; }

		/// <summary>
		/// Focus the camera onto the specified world point.
		/// </summary>
		/// <param name="pos">A point in world space.</param>
		/// <param name="distance">The distance from the position.</param>
		public static void PanCameraTo(Vector3 pos, Single distance = 10)
		{
			var xform = Camera.main.transform;

			var dir = -xform.forward;
			var newPos = dir * distance + pos;

			xform.position = newPos;
		}

		/// <summary>
		/// Pan the main camera around. The camera's true forward vector is projected onto the XZ plane
		/// to simulate typical RTS/Overhead movement styles.
		/// </summary>
		/// <param name="dForward">The units to go forward, can be negative to go backwards. </param>
		/// <param name="dRight">The units to go right, can be negative to go left.</param>
		public static void PanCamera(Single dForward, Single dRight)
		{
			var xform = Camera.main.transform;

			var right = Vector3.Cross(Vector3.up, xform.forward).normalized;
			var forward = Vector3.Cross(right, Vector3.up);
			var newPos = xform.position + right * dRight + forward * dForward;;

			xform.position = newPos;
		}

		/// <summary>
		/// Calculate the the focus of the ray.
		/// </summary>
		/// <param name="ray">A ray, should usually be from the camera center, straight outwards.</param>
		public static void CalculateFocus(Ray ray)
		{
			RaycastHit hit;

			// Reset the things the user is focusing on
			FocusTarget = null; FocusPoint = Vector3.zero; 

			// Calculate the point we're hovering the mouse above
			var ignoreMask = 0x00000004;
			if (Physics.Raycast(ray, out hit, Single.MaxValue, ~(UnitMask | ignoreMask)))
				FocusPoint = hit.point;
			else
			{
				Plane p = new Plane(Vector3.up, 0); Single d;
				if (p.Raycast(ray, out d))
					FocusPoint = ray.origin + ray.direction * d;
			}

			var minDistance = Single.MaxValue;

			// Check if we're mousing over a targetable unit
			if (Physics.Raycast(ray, out hit, Single.MaxValue))
			{
				var focusObj = hit.collider.gameObject;
				FocusTarget = focusObj.GetComponent<Targetable>();

				// Check the parents for the components as well
				if (FocusTarget == null)
				{
					var focusParent = focusObj.transform.parent;
					if (focusParent != null)
						FocusTarget = focusParent.gameObject.GetComponent<Targetable>();
				}

				minDistance = hit.distance;
			}
		}

		/// <summary>
		/// Calculate all the units that are inside the specified screen-space rectangle.
		/// </summary>
		/// <param name="r">The rectangle to test.</param>
		/// <param name="buffer">The list that will be populated with all units that are inside the rectangle.</param>
		public static void CalculateUnitsInRect(Rect r, IList<Unit> buffer)
		{
			if (buffer == null) return;

			buffer.Clear();

			if (r.width < 3 || r.height < 3)
			{
				// Very small box, treat as point
				var ray = Camera.main.ScreenPointToRay(new Vector3((r.xMin + r.xMax) * 0.5f, (r.yMin + r.yMax) * 0.5f));
				RaycastHit hit;

				if (Physics.Raycast(ray, out hit, Single.MaxValue))
				{
					var f = hit.transform.gameObject.GetComponent<Unit>();
					if (hit.transform.parent != null && f == null)
						f = hit.transform.gameObject.GetComponent<Unit>();

					if (f != null) buffer.Add(f);
				}
			}
			else
			{
				var c = Camera.mainCamera;

				// Project the rectangle into world space
				var c0 = c.ScreenToWorldPoint(new Vector3(r.xMin, r.yMin, c.nearClipPlane));
				var c1 = c.ScreenToWorldPoint(new Vector3(r.xMin, r.yMax, c.nearClipPlane));
				var c2 = c.ScreenToWorldPoint(new Vector3(r.xMax, r.yMin, c.nearClipPlane));
				var c3 = c.ScreenToWorldPoint(new Vector3(r.xMax, r.yMax, c.nearClipPlane));

				var c4 = c.ScreenToWorldPoint(new Vector3(r.xMin, r.yMin, c.farClipPlane));
				var c5 = c.ScreenToWorldPoint(new Vector3(r.xMax, r.yMax, c.farClipPlane));

				// Define the planes of the rectangle projected into the world
				PlaneBuffer[0] = new Plane(c0, c4, c2);
				PlaneBuffer[1] = new Plane(c2, c5, c3);
				PlaneBuffer[2] = new Plane(c3, c5, c1);
				PlaneBuffer[3] = new Plane(c1, c4, c0);

				for (Int32 i = 0; i < Manager.Instance.Commanders.Count; i++)
				{
					var cmdr = Manager.Instance.Commanders[i];
					foreach (var kvp in cmdr.Units.Values) 
						if (kvp != null && kvp.collider != null) 
							if (GeometryUtility.TestPlanesAABB(PlaneBuffer, kvp.collider.bounds))
								buffer.Add(kvp);
				}
			}
		}

		/// <summary>
		/// Get a caster that is the closest to the specified point and is ready to cast the 
		/// queued ability. Casters will only be picked from those supplied in the QueueAbility call.
		/// </summary>
		/// <param name="point">A point, in world space. Should usually be the target of the queued ability.</param>
		/// <returns>A caster if one is ready to cast the ability.</returns>
		public static Fury.Behaviors.Unit QueueGetCaster(Vector3 point)
		{
			if (QueuedAbility == null || QueuedAbilityCasters == null) return null;

			var minDistance = Single.MaxValue;
			Fury.Behaviors.Unit closestCaster = null;

			foreach (var caster in QueuedAbilityCasters)
			{
				var abiCtrl = caster.Controllers.AbilityController;

				if (!caster.IsDestroyed && abiCtrl != null && abiCtrl.CanCast(QueuedAbility))
				{
					var dSquared = Vector3.Distance(caster.transform.position, point);
					if (dSquared < minDistance)
					{
						closestCaster = caster;
						minDistance = dSquared;
					}
				}
			}

			return closestCaster;
		}

		/// <summary>
		/// Queue an ability and a list of casters that could potentially cast that ability.
		/// </summary>
		/// <param name="ability">The ability to queue.</param>
		/// <param name="casters">The potential list of casters.</param>
		public static void QueueAbility(Database.Ability ability, IEnumerable<Behaviors.Unit> casters)
		{
			if (ability == null || casters == null)
			{
				QueuedAbility = null;
				QueuedAbilityCasters.Clear();
				return;
			}

			QueuedAbility = ability;

			QueuedAbilityCasters.Clear();
			foreach (var unit in casters)
				if (unit.Properties.Abilities.Contains(ability))
					QueuedAbilityCasters.Add(unit);
		}
		 
		internal static void Update()
		{
			if (Screen.lockCursor)
			{
				var delta = new Vector2(Input.GetAxis("Mouse X"), -Input.GetAxis("Mouse Y")) * SensitivityMouse;
				Position = new Vector2(Mathf.Clamp(delta.x + Position.x, 0, Screen.width),
					Mathf.Clamp(-delta.y + Position.y, 0, Screen.height));
			}
			else
			{
				Position = Input.mousePosition;
			}

			TriggerLMB = false;
			Shift = Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift);

			if (Input.GetMouseButton(0))
			{
				SelectionBox = null;
				if (MouseDownAt == null)
				{
					MouseDownAt = Position;
				}
				else
				{
					Single dist = (MouseDownAt.Value - Position).magnitude;

					if (dist > 2)
					{
						SelectionBox = new Rect(Math.Min(MouseDownAt.Value.x, Position.x),
							Math.Min(MouseDownAt.Value.y, Position.y),
							Math.Abs(MouseDownAt.Value.x - Position.x),
							Math.Abs(MouseDownAt.Value.y - Position.y));
					}
				}
			}
			else
			{
				if (MouseDownAt != null)
				{
					if (SelectionBox == null)
					{
						TriggerLMB = true;
					}
					else
					{
						try
						{
							if (OnDragComplete != null)
								OnDragComplete(SelectionBox.Value);
						}
						catch
						{
						}
					}
				}

				SelectionBox = null;
				MouseDownAt = null;
			}

			TriggerRMB = Input.GetMouseButton(1) && !RMBDownLastFrame;

			LMBDownLastFrame = Input.GetMouseButton(0);
			RMBDownLastFrame = Input.GetMouseButton(1);
		}
	}
}