﻿using System;
using System.Collections.Generic;

using System.Text;

#pragma warning disable 1591

namespace Fury
{
	/// <summary>
	/// The states the game can be in.
	/// </summary>
	public enum GameStates
	{
		/// <summary>
		/// The user is in the game menus.
		/// </summary>
		Menu,

		/// <summary>
		/// The user is in the lobby of a game.
		/// </summary>
		Lobby,

		/// <summary>
		/// The user is loading the map.
		/// </summary>
		Loading,

		/// <summary>
		/// The user is playing the game.
		/// </summary>
		Playing
	}

	/// <summary>
	/// The states the unit can be in.
	/// </summary>
	public enum UnitStates
	{
		/// <summary>
		/// This is the state of the entity during design (editor) time, or if it is 
		/// sitting unloaded in the scene.
		/// </summary>
		UnloadedOrEditor,

		/// <summary>
		/// The entity is currently initializing itself.
		/// </summary>
		Initializing,

		/// <summary>
		/// The unit is idle. It will fire at hostile entities but not move.
		/// </summary>
		Idle,

		/// <summary>
		/// The unit is attacking a unit.
		/// </summary>
		AttackingUnit,
		FakeAttacking,
		/// <summary>
		/// The unit is moving to a position.
		/// </summary>
		MovingToPosition,

		/// <summary>
		/// The unit is moving to a unit.
		/// </summary>
		MovingToTarget,

		/// <summary>
		/// The unit is traveling to cast or casting an ability.
		/// </summary>
		CastingAbility,

		Dead
	}

	/// <summary>
	/// The RPC permission levels.
	/// </summary>
	public enum RPCPermissions : int { Owner = 1, Server = 2 }

	/// <summary>
	/// The indices available to commanders, up to thirty two.
	/// </summary>
	public enum CommanderIndices : int
	{
		One, Two, Three, Four, Five, Six, Seven, Eight, Nine, Ten,
		Eleven, Twelve, Thirteen, Fourteen, Fifteen, Sixteen, Seventeen, Eighteen, Nineteen, Twenty,
		TwentyOne, TwentyTwo, TwentyThree, TwentyFour, TwentyFive, TwentySix, TwentySeven, TwentyEight, TwentyNine, Thirty,
		ThirtyOne, ThirtyTwo
	}
	
	/// <summary>
	/// The different animation states of an Animator.
	/// </summary>
	public enum AnimationStates : int { Custom, Idle, Move, Death, Attack }

	/// <summary>
	/// The different orders that can be given to a unit.
	/// </summary>
	public enum OrderTypes : int { ToUnit, ToPosition, ToCast }

	/// <summary>
	/// The type of commander.
	/// </summary>
	public enum CommanderTypes : int { Computer, Player, Both }
}