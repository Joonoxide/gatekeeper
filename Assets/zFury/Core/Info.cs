namespace Fury
{
	/// <summary>
	/// Provides versioning and licensing information for the product.
	/// </summary>
	public class Info
	{
		/// <summary>
		/// True if the product is running in trial mode, false otherwise.
		/// </summary>
#if TRIAL
		public const bool IsTrial = true;
#else
		public const bool IsTrial = false;
#endif

		/// <summary>
		/// The current revision of the project.
		/// </summary>
		public static int Revision
		{
			get
			{
				return 290;
			}
		}
	}
}