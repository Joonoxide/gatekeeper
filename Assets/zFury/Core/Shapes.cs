﻿using System;
using System.Collections.Generic;

using UnityEngine;

using System.Runtime.Serialization;

namespace Fury
{
	/// <summary>
	/// Defines a bounding circle.
	/// </summary>
	[Serializable]
	internal class BoundingCircle
	{
		/// <summary>
		/// The position of the circle.
		/// </summary>
		public Vector2 Position;

		/// <summary>
		/// The radius of the circle.
		/// </summary>
		public Single Radius;

		/// <summary>
		/// An application defined tag. Often bounding circles may need to be associated with the object they represent.
		/// </summary>
		[NonSerialized]
		public System.Object Tag;

		/// <summary>
		/// Create a bounding circle.
		/// </summary>
		/// <param name="position">The position of the circle.</param>
		/// <param name="radius">The radius of the circle.</param>
		/// <param name="tag">An application defined tag associated with the circle.</param>
		public BoundingCircle(Vector2 position, Single radius, System.Object tag)
		{
			Position = position;
			Tag = tag;
			Radius = radius;
		}

		/// <summary>
		/// Sweep a circle against a segment.
		/// </summary>
		/// <param name="start">The starting point of the circle.</param>
		/// <param name="delta">The movement vector of the circle.</param>
		/// <param name="radius">The radius of the circle.</param>
		/// <param name="f0">The starting point of the segment.</param>
		/// <param name="f1">The ending point of the segment.</param>
		/// <param name="x">The contact point, if a collision occurs.</param>
		/// <param name="t">The time at which a collision occured.</param>
		/// <returns>True if a collision occured, false otherwise.</returns>
		public static Boolean SweepSphere(Vector2 start, Vector2 delta, Single radius, Vector2 f0, Vector2 f1, out Vector2 x, out Single t)
		{
			// The equations assume unit radius, so we have to tweak some of the input variables
			Single inverseRadius = 1f / radius;
			var p0 = f0 * inverseRadius;
			var p1 = f1 * inverseRadius;
			var basepoint = start * inverseRadius;
			var velocity = delta * inverseRadius;
			var velocitySquaredLength = velocity.sqrMagnitude;

			Boolean collision = false;
			Single a, b, c;
			x = Vector2.zero;
			t = 1f;

			a = velocitySquaredLength;

			// Collide against the first end point
			b = 2f * (Vector2.Dot(velocity, basepoint - p0));
			c = (p0 - basepoint).sqrMagnitude - 1f;
			if (GetLowestPositiveRoot(a, b, c, ref t))
			{
				collision = true;
				x = f0;
			}

			// Collide against the second end point
			b = 2f * (Vector2.Dot(velocity, basepoint - p1));
			c = (p1 - basepoint).sqrMagnitude - 1f;
			if (GetLowestPositiveRoot(a, b, c, ref t))
			{
				collision = true;
				x = f1;
			}

			var edge = p0 - p1;
			var baseToVertex = p1 - basepoint;
			var edgeSquaredLength = edge.sqrMagnitude;
			var edgeDotVelocity = Vector2.Dot(edge, velocity);
			var edgeDotBaseToVertex = Vector2.Dot(edge, baseToVertex);

			a = edgeSquaredLength * -velocitySquaredLength + edgeDotVelocity * edgeDotVelocity;
			b = edgeSquaredLength * (2 * Vector2.Dot(velocity, baseToVertex)) - 2.0f * edgeDotVelocity * edgeDotBaseToVertex;
			c = edgeSquaredLength * (1 - baseToVertex.sqrMagnitude) + edgeDotBaseToVertex * edgeDotBaseToVertex;

			if (GetLowestPositiveRoot(a, b, c, ref t))
			{
				float f = (edgeDotVelocity * t - edgeDotBaseToVertex) / edgeSquaredLength;

				if (f >= 0.0 && f <= 1.0)
				{
					collision = true;
					x = (p1 + f * edge) * radius;
				}
			}

			return collision;
		}

		/// <summary>
		/// Sweep a sphere against a point.
		/// </summary>
		/// <returns>The time fraction, t between start and end where the circle collides with the point.</returns>
		public static Boolean SweepSphere(Vector2 start, Vector2 end, Single radius, Vector2 point, out Single t)
		{
			Single ra = radius;
			Vector2 ab = point - start;
			Vector2 vab = start - end;

			Single a = Vector2.Dot(vab, vab);
			Single b = 2 * Vector2.Dot(vab, ab);
			Single c = Vector2.Dot(ab, ab) - ra * ra;

			t = 1f;

			if (Vector2.Dot(ab, ab) > ra * ra)
				if (GetLowestPositiveRoot(a, b, c, ref t))
					return true;

			return false;
		}

		/// <summary>
		/// Sweep the circle against other bounding circles. The return value is a position along the 
		/// circle's desired path that does not collide with any other circles.
		/// </summary>
		/// <param name="start">The starting position of the circle.</param>
		/// <param name="end">The desired ending position of the circle.</param>
		/// <param name="radius">The radius of the circle.</param>
		/// <param name="circles">Other circle obstacles.</param>
		/// <returns>The position of the circle, somewhere between <paramref name="start"/> and <paramref name="end"/>.</returns>
		public static Vector2 SweepSphere(Vector2 start, Vector2 end, Single radius, IEnumerable<BoundingCircle> circles)
		{
			Vector2 va = (end - start);
			Single ra = radius;

			Single minR = 1;

			foreach (var arrived in circles)
			{
				Vector2 b0 = arrived.Position;
				Vector2 b1 = arrived.Position;
				Vector2 vb = b1 - b0;
				Single rb = arrived.Radius;

				Vector2 ab = b0 - start;
				Vector2 vab = vb - va;
				Single rab = ra + rb;

				Single a = Vector2.Dot(vab, vab);
				Single b = 2 * Vector2.Dot(vab, ab);
				Single c = Vector2.Dot(ab, ab) - rab * rab;

				if (Vector2.Dot(ab, ab) <= rab * rab)
				{
					// Already overlaps
				}
				else
				{
					Single q = b * b - 4 * a * c;
					if (q >= 0)
					{
						Single sq = (Single)Math.Sqrt(q);
						Single d = 1 / (2 * a);
						Single r1 = (-b + sq) * d;
						Single r2 = (-b - sq) * d;

						if (r1 > r2)
						{
							Single t = r1;
							r1 = r2;
							r2 = t;
						}

						if (r1 < minR)
							minR = r1;
					}
					else
					{
						// Complex roots
					}
				}
			}

			return start + va * minR;
		}

		private static Boolean GetLowestPositiveRoot(Single a, Single b, Single c, ref Single t)
		{
			// Check if a solution exists
			float determinant = b * b - 4.0f * a * c;

			// If determinant is negative it means no solutions.
			if (determinant < 0.0f) return false;

			// calculate the two possible roots
			float sqrtD = (Single)Math.Sqrt(determinant);
			float r1 = (-b - sqrtD) / (2 * a);
			float r2 = (-b + sqrtD) / (2 * a);

			// Sort so x1 <= x2
			if (r1 > r2)
			{
				float temp = r2;
				r2 = r1;
				r1 = temp;
			}

			// Get lowest root:
			if (r1 > 0 && r1 < t)
			{
				t = r1;
				return true;
			}

			// It is possible that we want x2 - this can happen if x1 < 0
			if (r2 > 0 && r2 < t)
			{
				t = r2;
				return true;
			}

			return false;
		}
	}
}