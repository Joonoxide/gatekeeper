﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;

namespace Fury
{
	/// <summary>
	/// The INavMeshAgent can be used to implement custom pathfinding solutions if the
	/// default Unity solution is not desireable, or not available.
	/// </summary>
	public interface INavMeshAgent
	{
		/// <summary>
		/// The velocity of the unit.
		/// </summary>
		Vector3 velocity { get; }

		/// <summary>
		/// The radius of the unit.
		/// </summary>
		Single radius { get; set; }

		/// <summary>
		/// The acceleration of the unit.
		/// </summary>
		Single acceleration { get; set; }

		/// <summary>
		/// The maximum angular speed of the unit.
		/// </summary>
		Single angularSpeed { get; set; }

		/// <summary>
		/// The maximum translational speed of the unit.
		/// </summary>
		Single speed { get; set; }

		/// <summary>
		/// The position of the unit to be set in the next frame, you should use a bufferd 
		/// approach to setting positions, as demonstrated in the tutorials.
		/// </summary>
		Vector3 nextPosition { get; set; }

		/// <summary>
		/// The destination of the unit.
		/// </summary>
		Vector3 destination { get; }

		/// <summary>
		/// If true, the script should set the position of the unit.
		/// </summary>
		Boolean updatePosition { get; set; }

		/// <summary>
		/// If true, the script should set the rotation of the unit.
		/// </summary>
		Boolean updateRotation { get; set; }

		/// <summary>
		/// If true, the nav agent has been destroyed for some reason.
		/// </summary>
		Boolean isDestroyed { get; }

		/// <summary>
		/// Stop the nav agent from moving.
		/// </summary>
		void Stop();

		/// <summary>
		/// Destroy the nav agent.
		/// </summary>
		void Destroy();

		/// <summary>
		/// Set the nav agent destination.
		/// </summary>
		/// <param name="destination"></param>
		void SetDestination(Vector3 destination);
	}

	/// <summary>
	/// This behaviour acts as a relay between NavMeshAgent and the INavMeshAgent relay.
	/// </summary>
	[System.Reflection.Obfuscation(Feature = "renaming", Exclude = true)]
	class UnityNavMeshAgent : MonoBehaviour, INavMeshAgent
	{
		Vector3 INavMeshAgent.velocity { get { return Agent.velocity; } }
		Single INavMeshAgent.radius { get { return Agent.radius; } set { Agent.radius = value; } }
		Single INavMeshAgent.acceleration { get { return Agent.acceleration; } set { Agent.acceleration = value; } }
		Single INavMeshAgent.angularSpeed { get { return Agent.angularSpeed; } set { Agent.angularSpeed = value; } }
		Single INavMeshAgent.speed { get { return Agent.speed; } set { Agent.speed = value; } }
		Vector3 INavMeshAgent.nextPosition { get { return Agent.nextPosition; } set { Agent.nextPosition = value; } }
		Vector3 INavMeshAgent.destination { get { return Agent.destination; } }

		Boolean INavMeshAgent.updatePosition { get { return Agent.updatePosition; } set { Agent.updatePosition = value; } }
		Boolean INavMeshAgent.updateRotation { get { return Agent.updateRotation; } set { Agent.updateRotation = value; } }

		Boolean INavMeshAgent.isDestroyed { get { return this == null || Agent == null; } }
		public NavMeshAgent Agent;

		private void Awake()
		{
			Agent = gameObject.AddComponent<NavMeshAgent>();
			Agent.baseOffset = 0;
		}

		private void OnDestroy()
		{
			GameObject.Destroy(Agent);
		}

		void INavMeshAgent.Stop()
		{
			Agent.Stop();
		}

		void INavMeshAgent.Destroy()
		{
			GameObject.Destroy(Agent);
			GameObject.Destroy(this);
		}

		void INavMeshAgent.SetDestination(Vector3 d)
		{
			if(GameController.instance.isEnd != 10)
			{
				if(d.x < GameController.instance.door[1].transform.position.x)
				{
					d.x = GameController.instance.door[1].transform.position.x;
				}
				else if(d.x > GameController.instance.door[0].transform.position.x)
				{
					d.x = GameController.instance.door[0].transform.position.x;
				}
				d.z = Mathf.Clamp(d.z, -25, 25);
			}
			Vector3 sss = Agent.destination;
			sss.y = 0;
			Vector3 dd = d;
			dd.y = 0;
			if((sss-dd).magnitude > 0.1f || (Agent.velocity).magnitude == 0)
			{
				Agent.SetDestination(d);
			}

		}

		public bool isMoving()
		{
			if(Agent.velocity.magnitude == 0)
			{
				return true;
			}
			else
			{
				return false;
			}			
		}
	}
}