﻿using System;
using System.Collections.Generic;

using UnityEngine;
using Fury.Behaviors;

namespace Fury.Controllers
{
	/// <summary>
	/// The movement controller manages the movement of the entity.
	/// </summary>
	public sealed class MovementController : Controller
	{
		/// <summary>
		/// The controllers desired target position, null if no position is set.
		/// </summary>
		public Vector3? TargetPosition { get; private set; }

		/// <summary>
		/// The controller's desired target unit, null if no unit is set.
		/// </summary>
		public Targetable TargetUnit { get; private set; }

		/// <summary>
		/// The velocity of the unit.
		/// </summary>
		public Vector3 Velocity { get { return (Agent != null)? Agent.velocity : new Vector3(0, 0, 0); } }

		private StatusController ParentStatuses;

		private Unit Parent;

		public INavMeshAgent Agent;

		private Single StoppingDistanceSquared;
		public float targetZ;

		internal MovementController(Unit parent)
		{
			Parent = parent;
		}

		public bool isMoving()
		{
			if(Agent.speed < 0.0001f)
			{
				return false;
			}
			else
			{
				return true;
			}
		}

		internal override void Initialize()
		{
			ParentStatuses = Parent.Controllers.StatusController;
            if(Parent.name.Contains("Resource"))
            {
            }
            else
            {   
            	Agent = Parent.gameObject.AddComponent(Manager.Instance.NavMeshAgentType) as INavMeshAgent;
           		NavMeshAgent agent = Parent.gameObject.GetComponent<NavMeshAgent>();
            	agent.obstacleAvoidanceType = ObstacleAvoidanceType.NoObstacleAvoidance;
				int mask = 1 << 0;
				if(Parent.name.Contains ("Griffin"))
				{
					mask = mask | (1 << 3);
				}
				//mask = -1;

				agent.walkableMask = mask;
            }
		}

		public Targetable prevv;
		internal override void Update()
		{
			if(Parent.Properties.genre == 3) 
			{
				return;
			}
			if (Agent.isDestroyed)
			{
				Debug.LogWarning("Navigation agent unexpectedly destroyed! Re-initializing!");
				Agent.Destroy();
				Initialize();
			}

			var canRotate = !Parent.Controllers.StatusController.IsStunned && !(Parent.State == Fury.UnitStates.CastingAbility);
			canRotate = false;
			var canMove = Parent.Properties.MoveRate > 0;

			if (Parent.Controllers.WeaponController != null &&
				Parent.Controllers.WeaponController.InUse == true &&
				Parent.Controllers.WeaponController.Properties.Mobile == false)
			{
				canMove = false;
			}

			if (Parent.Controllers.AbilityController != null &&
				Parent.Controllers.AbilityController.State == AbilityController.States.Casting &&
				Parent.Controllers.AbilityController.TargetAbility.CastMobile == false)
			{
				canMove = false;
			}

			Agent.radius = Parent.Radius;
			Agent.speed = Parent.Properties.MoveRate;
			Agent.acceleration = Parent.Properties.AccelRate;
			Agent.angularSpeed = Parent.Properties.TurnRate * (canRotate ? 1f : 0f);
			Agent.speed = Parent.Properties.MoveRate * ParentStatuses.ModifierSpeed * (canMove ? 1f : 0f);

			Vector3? actualTarget = null;
			if (TargetUnit != null && !TargetUnit.IsDestroyed)
			{
				Vector3 pos = TargetUnit.transform.position;
				if(Parent.transform.position.z < pos.z+(TargetUnit as Fury.Behaviors.Unit).Properties.Radius && Parent.transform.position.z > pos.z-(TargetUnit as Fury.Behaviors.Unit).Properties.Radius)
				{
					pos.z = Parent.transform.position.z;
				}
				else if(prevv == null)
				{
					pos.z = TargetUnit.transform.position.z+UnityEngine.Random.Range(-(TargetUnit as Fury.Behaviors.Unit).Properties.Radius, (TargetUnit as Fury.Behaviors.Unit).Properties.Radius);				
				}

				targetZ = pos.z;
				actualTarget = pos;
				prevv = TargetUnit;
			}
			else
			{
				prevv = null;
			}
			if (TargetPosition != null)
				actualTarget = TargetPosition.Value;

			if (actualTarget != null)
			{
				var vecToTarget = actualTarget.Value - Parent.transform.position;
				vecToTarget.y = 0;
				var distToTargetSqred = vecToTarget.sqrMagnitude;

				if (distToTargetSqred < StoppingDistanceSquared)
				{
					Agent.nextPosition = Parent.transform.position;
					Agent.SetDestination(Parent.transform.position);
					Agent.updateRotation = false;
					Agent.updatePosition = false;

					if (distToTargetSqred > 0.01)
					{
						var angleToTarget = Vector3.Angle(Parent.transform.forward, vecToTarget);
						var sgn = Math.Sign(Vector3.Dot(Parent.transform.right, vecToTarget));

						angleToTarget = Mathf.Clamp(angleToTarget, 0, Parent.Properties.TurnRate * Time.deltaTime);

						if (canRotate)
						{
							Parent.transform.Rotate(Vector3.up, angleToTarget * sgn);

							if (angleToTarget < 0.01f)
							{
								TargetPosition = null;
							}
						}
						else
						{
							TargetPosition = null;
						}
					}
					else
					{
						TargetPosition = null;
					}
				}
				else
				{			
					Agent.SetDestination(actualTarget.Value);
					Agent.updateRotation = true;
					Agent.updatePosition = true;
				}
			}
			else
			{
				Agent.updatePosition = true;
				Agent.updateRotation = true;
			}
		}

		internal override void Dispose()
		{
			if(Parent.Properties.genre != 3)
			{
				Agent.Destroy();			
			} 
		}

		internal void Stop()
		{
			TargetPosition = null;
			TargetUnit = null;

			if (Agent != null)
				Agent.Stop();
		}

		internal void Start(Vector3 position, Single stoppingDistance)
		{
			stoppingDistance += Parent.Radius * 1.01f;

			TargetUnit = null;
			TargetPosition = position;
			StoppingDistanceSquared = stoppingDistance * stoppingDistance;
		}

		internal void Start(Targetable focusUnit, Single stoppingDistance)
		{
			stoppingDistance += (focusUnit.Radius + Parent.Radius)*0.9f;
			//stoppingDistance = 0;
			TargetPosition = null;
			TargetUnit = focusUnit;
			StoppingDistanceSquared = stoppingDistance * stoppingDistance;
		}
	}
}