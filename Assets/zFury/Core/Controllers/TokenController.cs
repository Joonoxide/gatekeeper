﻿using System;
using System.Collections.Generic;

using UnityEngine;
using Fury.Behaviors;

namespace Fury.Controllers
{
	/// <summary>
	/// The token controller manages the 'tokens,' which are like items, that an 
	/// entity can posses.
	/// </summary>
	public sealed class TokenController : Controller
	{
		/// <summary>
		/// The list of stacks this unit has.
		/// </summary>
		public General.ReadOnlyList<Stack> Stacks { get; private set; }

		internal TokenController()
		{
			Stacks = new General.ReadOnlyList<Stack>();
		}

		internal Byte SetStackState(Int32 stackID, Byte index, Byte val)
		{
			foreach(var stack in Stacks)
				if (stack.Identifier == stackID)
				{
					while (index >= stack.States.Count)
						stack.States.Add(0);

					var oldVal = stack.States[index];
					stack.States[index] = val;
					return oldVal;
				}

			return 0;
		}

		internal override System.Object GetState() { return Stacks; }

		internal override  void SetState(System.Object state)
		{
			Stacks = state as General.ReadOnlyList<Stack>;
		}
	}
}