﻿using System;
using System.Collections.Generic;

using UnityEngine;
using Fury.Behaviors;

using Unit = global::Fury.Behaviors.Unit;
using Commander = global::Fury.Behaviors.Commander;

namespace Fury.Controllers
{
	/// <summary>
	/// A helper collection that is designed for units currently selected by the player.
	/// </summary>
	public sealed class SelectionController : Controller
	{
		/// <summary>
		/// The table of units currently selected.
		/// </summary>
		public General.ReadOnlyList<Unit> Units { get; private set; }

		/// <summary>
		/// Called when a unit is selected.
		/// </summary>
		public event Action<Unit> OnUnitSelected;

		/// <summary>
		/// Called when a unit is deselected.
		/// </summary>
		public event Action<Unit> OnUnitDeselected;

		/// <summary>
		/// Create an empty unit collection.
		/// </summary>
		public SelectionController()
		{
			Units = new General.ReadOnlyList<Unit>();
		}

		/// <summary>
		/// Add a unit to the collection.
		/// </summary>
		/// <param name="unit">The unit to add.</param>
		public void Add(Unit unit)
		{
			if (unit.IsDestroyed) return;

			if (!Units.Contains(unit))
			{
				if (OnUnitSelected != null)
					OnUnitSelected(unit);

				Units.Add(unit);
			}
		}

		/// <summary>
		/// Remove a unit from the collection.
		/// </summary>
		/// <param name="unit">The unit to remove.</param>
		public void Remove(Unit unit)
		{
			if (Units.Contains(unit))
			{
				if (OnUnitDeselected != null)
					OnUnitDeselected(unit);

				Units.Remove(unit);
			}
		}

		/// <summary>
		/// Clear the selection of all units.
		/// </summary>
		public void Clear()
		{
			foreach (var kvp in Units)
				OnUnitDeselected(kvp);

			Units.Clear();
		}

		/// <summary>
		/// Check if a unit is contained in the selection.
		/// </summary>
		/// <param name="unit">The unit to find.</param>
		/// <returns>True if the unit is in the selected collection, false otherwise.</returns>
		public Boolean Contains(Unit unit)
		{
			return Units.Contains(unit);
		}

		/// <summary>
		/// Returns an enumerator that can be used to iterate through the dictionary.
		/// </summary>
		/// <returns>An enumerator.</returns>
		public IEnumerator<Unit> GetEnumerator()
		{
			return Units.GetEnumerator();
		}

		/// <summary>
		/// Prune the selected collection of dead units.
		/// </summary>
		public void Prune()
		{
			Update();
		}

		internal override void Update()
		{
			for (Int32 i = 0; i < Units.Count; i++)
				if (Units[i].IsDestroyed)
					Units.RemoveAt(i--);
		}

		internal override object GetState() { return Units; }

		internal override void SetState(System.Object state)
		{
			Units = state as Fury.General.ReadOnlyList<Unit>;
		}
	}
}