﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fury.Controllers
{
	/// <summary>
	/// Controllers are behaviour-like scripts that are attached to different entities that 
	/// implement specific functionality such as inventory, health, etc.
	/// </summary>
	public abstract class Controller
	{
		internal virtual void Initialize() { }
		internal virtual void Update() { }
		internal virtual void Dispose() { }

		internal virtual System.Object GetState() { return null; }
		internal virtual void SetState(System.Object state) { }
	}

	/// <summary>
	/// A specialized collection used to store controllers. The storage container caches
	/// all available controllers for fast and easy access.
	/// </summary>
	public class ControllerCollection : IEnumerable<Controller>
	{
		private General.ReadOnlyList<Controller> Table;

		/// <summary>
		/// Cached ability controller, may be null.
		/// </summary>
		public AbilityController AbilityController { get; private set; }

		/// <summary>
		/// Cached movement controller, may be null.
		/// </summary>
		public MovementController MovementController { get; private set; }

		/// <summary>
		/// Cached status controller, may be null.
		/// </summary>
		public StatusController StatusController { get; private set; }

		/// <summary>
		/// Cached tag controller, may be null.
		/// </summary>
		public TagController TagController { get; private set; }

		/// <summary>
		/// Cached token controller, may be null.
		/// </summary>
		public TokenController TokenController { get; private set; }

		/// <summary>
		/// Cached vitality controller, may be null.
		/// </summary>
		public VitalityController VitalityController { get; private set; }

		/// <summary>
		/// Cached weapon controller, may be null.
		/// </summary>
		public WeaponController WeaponController { get; private set; }

		internal ControllerCollection()
		{
			Table = new General.ReadOnlyList<Controller>();
		}

		internal void Add(Controller ctrl)
		{
			if (ctrl is AbilityController)
				AbilityController = ctrl as AbilityController;
			else if (ctrl is MovementController)
				MovementController = ctrl as MovementController;
			else if (ctrl is StatusController)
				StatusController = ctrl as StatusController;
			else if (ctrl is TagController)
				TagController = ctrl as TagController;
			else if (ctrl is TokenController)
				TokenController = ctrl as TokenController;
			else if (ctrl is VitalityController)
				VitalityController = ctrl as VitalityController;
			else if (ctrl is WeaponController)
				WeaponController = ctrl as WeaponController;

			Table.Add(ctrl);
		}

		/// <summary>
		/// Returns an enumerator that can be used to inumerate the list.
		/// </summary>
		/// <returns>Returns an enumerator that can be used to inumerate the list.</returns>
		public IEnumerator<Controller> GetEnumerator()
		{
			return Table.GetEnumerator();
		}

		/// <summary>
		/// Returns an enumerator that can be used to inumerate the list.
		/// </summary>
		/// <returns>Returns an enumerator that can be used to inumerate the list.</returns>
		System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
		{
			return Table.GetEnumerator();
		}
	}
}