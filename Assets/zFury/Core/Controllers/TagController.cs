using System;
using System.Collections.Generic;

using UnityEngine;
using Fury.Behaviors;

namespace Fury.Controllers
{
	/// <summary>
	/// The tag controller manages user-defined data, associated with a given entity, that is 
	/// serialized by the engine and synchronized amongst all the players.
	/// </summary>
	public sealed class TagController : Controller
	{
		/// <summary>
		/// The user defined data, as a byte array.
		/// </summary>
		public Byte[] Tag { get; internal set; }

		internal TagController(Byte[] tag)
		{
			Tag = tag;
		}

		internal override System.Object GetState() { return Tag; }

		internal override void SetState(System.Object state) { Tag = state as Byte[]; }
	}
}