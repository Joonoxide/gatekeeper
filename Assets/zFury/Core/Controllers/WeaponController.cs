using System;
using System.Collections.Generic;

using UnityEngine;
using Fury.Behaviors;

namespace Fury.Controllers
{
	/// <summary>
	/// The weapon controller allows entities to use a weapon.
	/// </summary>
	public sealed class WeaponController : Controller
	{
		/// <summary>
		/// The states the weapon can be in.
		/// </summary>
		public enum States
		{
			/// <summary>
			/// The attack has been queued by the server, and the command will soon be 
			/// broadcasted to connected clients.
			/// </summary>
			Queued,

			/// <summary>
			/// The delay before the attack is considered 'complete.' This time is similar
			/// to a character preparing his weapon for an attack.
			/// </summary>
			PreDelay,

			/// <summary>
			/// The delay after the attack before the cooldown period begins. This time is
			/// similar to a character resetting his weapon after an attack.
			/// </summary>
			PostDelay,

			/// <summary>
			/// The time after which the weapon can be used again. This time is similar
			/// to a reload period of some kind.
			/// </summary>
			Cooldown
		}

		[Serializable]
		internal class WeaponControllerState
		{
			internal States State;
			internal Single TimeInState;
			internal Int32 ParentID;
			internal Int32 DefinitionID;
		}

		/// <summary>
		/// The current state of the weapon controller.
		/// </summary>
		public States State { get; private set; }

		/// <summary>
		/// The time spent in the state.
		/// </summary>
		public Single TimeInState { get; private set; }

		/// <summary>
		/// The unit the weapon controller is attached to.
		/// </summary>
		public Unit Parent { get; private set; }

		/// <summary>
		/// True if the weapon can be used, false otherwise.
		/// </summary>
		public Boolean IsUsable { get { return State == States.Cooldown && TimeInState > Properties.Cooldown; } }

		/// <summary>
		/// True if the weapon is in use, false otherwise. A weapon is considered 'in use' if it is
		/// during its PreDelay or PostDelay states.
		/// </summary>
		public Boolean InUse { get { return State == States.PostDelay || State == States.PreDelay; } }

		/// <summary>
		/// True if the target of the weapon controller is out of its range, false otherwise.
		/// </summary>
		public Boolean IsTargetOutOfRange
		{
			get { return Target == null || (Target.transform.position - Parent.transform.position).sqrMagnitude > Properties.Range * Properties.Range; }
		}

		/// <summary>
		/// The properties of the weapon, as defined at startup.
		/// </summary>
		public Database.Weapon Properties { get; private set; }

		/// <summary>
		/// The target of the weapon controller.
		/// </summary>
		public Behaviors.Targetable Target;

		private Behaviors.Rotator Rotator;
		public bool isAttack;
		public bool notMove;
		public float dist;

		internal WeaponController(Unit parent)
		{
			State = States.Cooldown;
			Parent = parent;
			Properties = parent.Properties.Weapon;
		}

		internal override void Initialize()
		{
			var t = Parent.gameObject.GetComponentsInChildren<Behaviors.Rotator>();
			if (t != null && t.Length > 0)
			{
				if (t.Length > 1)
					Debug.LogWarning("More than two rotating joint found in one unit, only using the first.");

				Rotator = t[0];
			}
		}

		internal void Start(Behaviors.Targetable tgt)
		{
			Target = tgt;
			isAttack = true;
		}

		internal void Stop()
		{
			Target = null;
			isAttack = false;
			switch (State)
			{
				case States.PreDelay:
					State = States.Cooldown;
					TimeInState = Properties.Cooldown;
					break;

				case States.PostDelay:
					State = States.Cooldown;
					TimeInState = -(Properties.PostDelay/Parent.Controllers.StatusController.ModifierASpeed - TimeInState);
					break;
			}
		}

		internal override void Update()
		{
			//Debug.Log(Parent.name+":"+Properties.PostDelay/Parent.Controllers.StatusController.ModifierASpeed);
			TimeInState += Time.deltaTime;

			var abiController = Parent.Controllers.AbilityController;
			notMove = false;
			if (Target != null && !Target.IsDestroyed)
			{
				if (abiController == null || abiController.State == AbilityController.States.Idle)
				{
					dist = Vector3.Distance(Parent.transform.position, new Vector3(Target.transform.position.x, 0, Parent.Controllers.MovementController.targetZ));
					dist -= Parent.Properties.Radius + Target.Radius;
					
					if(dist < Properties.Range)
					{
						notMove = true;
					}
					else
					{
						notMove = false;
					}
					// Calculate the angle between the entity and target
					var p1 = Target.transform.position;
					var p2 = Parent.transform.position;
					p1.y = 0; p2.y = 0;
					var dirToTarget = (p1 - p2).normalized;

					var diff = Vector3.Angle(Parent.transform.forward, dirToTarget);

					// If the entity has a turret, the angle is measured from the turret angle
					if (Rotator != null)
					{
						diff = Vector3.Angle(Rotator.transform.forward, dirToTarget);
						var sgn = Math.Sign(Vector3.Dot(Rotator.transform.right, dirToTarget));

						Rotator.transform.RotateAroundLocal(Vector3.up, sgn * Math.Min(diff * Mathf.PI / 180f, Rotator.Speed * Time.deltaTime));
					}

					if (Network.isServer)
					{
						if(Parent.isCheck)
						Debug.Log(Parent+":"+dist+":"+diff+":"+Target+":"+notMove+":"+Target.transform.position);
						if (dist < Properties.Range && IsUsable && diff < 180)
						{
							State = States.Queued;
							Parent.Owner.BeginAttack(Parent, Manager.Empty);
						}
					}
				}
			}
			else
			{
				Target = null;
			}

			switch (State)
			{
				case States.PreDelay:
					if (TimeInState > Properties.PreDelay)
					{
						State = States.PostDelay;
						TimeInState = 0;
						if(Target == null)
						{
							isAttack = false;
						}
						else if (Target != null && (abiController == null || abiController.State == AbilityController.States.Idle))
						{
							Properties.OnAttackComplete(Parent, Target as Unit);
						}
					}
					break;

				case States.PostDelay:
					if (TimeInState > Properties.PostDelay/Parent.Controllers.StatusController.ModifierASpeed)
					{
						State = States.Cooldown;
						TimeInState = 0;
					}
					if(Target == null)
					{
						isAttack = false;
					}
					break;

				case States.Cooldown:
					if(Target == null)
					{
						isAttack = false;
					}
					break;
			}
		}

		internal void BeginAttack()
		{
			if (Parent.Animator != null)
				Parent.Animator.ChangeState(AnimationStates.Attack, 10, Properties.PreDelay + Properties.PostDelay/Parent.Controllers.StatusController.ModifierASpeed);

			State = States.PreDelay;
			TimeInState = 0;

			Properties.OnAttackBegin(Parent, Target as Unit);
		}

		internal override System.Object GetState()
		{
			var s = new WeaponControllerState();
			s.DefinitionID = Properties;
			s.ParentID = Parent;
			s.State = State;
			s.TimeInState = TimeInState;
			return s;
		}

		internal override void SetState(System.Object state)
		{ 
			var s = state as WeaponControllerState;
			// Properties = Database.Definition.Find<Database.Weapon>(s.DefinitionID);
			Parent = Manager.Instance.Find<Unit>(s.ParentID);
			State = s.State;
			TimeInState = s.TimeInState;
		}
	}
}