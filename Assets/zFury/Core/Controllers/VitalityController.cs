﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fury.Controllers
{
	/// <summary>
	/// The vitality controller allows entities to have health or energy.
	/// </summary>
	public class VitalityController : Controller
	{
		/// <summary>
		/// Contains information about a type of energy this unit may possess.
		/// </summary>
		public class Generator
		{
			/// <summary>
			/// The amount of time before the generator replenishes some of its energy.
			/// </summary>
			public Single Cooldown { get; internal set; }

			/// <summary>
			/// The amount of energy currently in the generator.
			/// </summary>
			public Int32 Amount
			{
				get { return _Amount; }
				internal set
				{
					_Amount = value;
					_Amount = Math.Max(Math.Min(_Amount, Properties.Capacity), 0);
				}
			}
			private Int32 _Amount;

			/// <summary>
			/// The properties defined for the generator.
			/// </summary>
			public Database.Generator Properties { get; private set; }

			/// <summary>
			/// The percentage of energy in the generator.
			/// </summary>
			public Single AmountPercentage { get { return _Amount / (Single)Properties.Capacity; } }

			internal Generator(Database.Generator properties)
			{
				Properties = properties;
				Amount = Properties.Capacity;
			}
		}

		private Behaviors.Unit Parent;

		/// <summary>
		/// The energy generators associated with this unit.
		/// </summary>
		public General.ReadOnlyDictionary<Database.Energy, Generator> Generators { get; private set; }

		/// <summary>
		/// The health of the entity.
		/// </summary>
		public Int32 Health
		{
			get { return UnityEngine.Mathf.Clamp(_Health, 0, MaxHealth); }
			set { _Health = UnityEngine.Mathf.Clamp(value, 0, MaxHealth); }
		}
		private Int32 _Health;

		/// <summary>
		/// The current health of the entity as a percentage.
		/// </summary>
		public Single HealthPercentage { get { return (Single)_Health / (Single)MaxHealth; } }

		/// <summary>
		/// The maximum health of the entity, including the base health defined under properties and
		/// any bonuses applied through statuses.
		/// </summary>
		public Int32 MaxHealth
		{
			get
			{
				if(Parent.Owner.Identifier == 1 && Parent.agent is KYTowerAgent)
				{
					if(Parent.agent is TowerAgent)
					{
						return LevelManager.instance.tower1HP;
					}
					else if(Parent.agent is TowerAgent2)
					{
						return LevelManager.instance.tower2HP;
					}
					else
					{
						return LevelManager.instance.tower3HP;
					}
				}
				else
				{
					var staCtrl = Parent.Controllers.StatusController;
					var baseHealth = Parent.Properties.Health;
					if (staCtrl != null)
						baseHealth += staCtrl.ModifierHealth;

					return Math.Max(1, baseHealth);
				}
			}
		}

		/// <summary>
		/// The last unit that damaged this unit.
		/// </summary>
		public Fury.Behaviors.Unit LastAttacker { get; set; }

		/// <summary>
		/// The last unit that healed this unit.
		/// </summary>
        public Fury.Behaviors.Unit LastHealer { get; set; }

		internal VitalityController(Behaviors.Unit unit)
		{
			Parent = unit;
			Health = MaxHealth;

			Generators = new General.ReadOnlyDictionary<Database.Energy, Generator>();
			foreach (var generator in Parent.Properties.Generators)
				if (!Generators.ContainsKey(generator.Energy))
					Generators.Add(generator.Energy, new Generator(generator));
		}

		/// <summary>
		/// Check if the unit possesses the amount of the specified energy.
		/// </summary>
		/// <param name="energy">The energy type the unit must have.</param>
		/// <param name="amt">The amount of energy the unit must have.</param>
		/// <returns></returns>
		public Boolean HasEnergy(Database.Energy energy, Int32 amt)
		{
			Generator output;
			if (Generators.TryGetValue(energy, out output))
				if (output.Amount >= amt)
					return true;

			return false;
		}

		internal override void Update()
		{
			foreach (var kvp in Generators.Values)
			{
				kvp.Cooldown -= UnityEngine.Time.deltaTime;

				if (kvp.Cooldown < 0)
				{
					kvp.Amount += kvp.Properties.RegenAmount;
					kvp.Cooldown = kvp.Properties.RegenPeriod;
				}
			}
		}
	}
}