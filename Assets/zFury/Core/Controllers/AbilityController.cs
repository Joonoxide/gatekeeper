﻿using System;
using System.Collections.Generic;

using UnityEngine;
using Fury.Behaviors;

namespace Fury.Controllers
{
	/// <summary>
	/// The ability controller controls the way abilities are cast by units.
	/// </summary>
	public sealed class AbilityController : Controller
	{
		/// <summary>
		/// The various states of the ability controller.
		/// </summary>
		public enum States
		{
			/// <summary>
			/// No ability is being cast.
			/// </summary>
			Idle,

			/// <summary>
			/// An ability is queued, the controller is waiting for the unit to get near its target.
			/// </summary>
			Waiting,

			/// <summary>
			/// The unit is casting an ability.
			/// </summary>
			Casting
		}

		/// <summary>
		/// The state of the ability controller.
		/// </summary>
		public States State { get; private set; }

		/// <summary>
		/// The cooldowns of the abilities.
		/// </summary>
		public General.ReadOnlyDictionary<Database.Ability, Single> Cooldowns { get; private set; }

		/// <summary>
		/// The time remaining on the cast.
		/// </summary>
		public Single CastTimeRemaining { get; private set; }

		/// <summary>
		/// The ability that is currently being cast by the unit. If null, no ability is being cast.
		/// </summary>
		public Database.Ability TargetAbility { get; private set; }

		/// <summary>
		/// The target onto which the ability is being cast.
		/// </summary>
		public Targetable Target { get; private set; }

		/// <summary>
		/// The position onto which the ability is being cast.
		/// </summary>
		public Vector3 TargetPosition { get; private set; }

		internal System.Object AbilityTag { get; private set; }

		internal Unit Parent { get; private set; }

		internal AbilityController(Unit parent)
		{
			State = States.Idle;
			Parent = parent;

			Cooldowns = new General.ReadOnlyDictionary<Database.Ability, Single>();
			for (Int32 i = 0; i < Parent.Properties.Abilities.Count; i++)
				Cooldowns.Add(Parent.Properties.Abilities[i], -1);
		}

		/// <summary>
		/// Check if the specified ability can be immediately casted by the unit.
		/// </summary>
		/// <param name="ability">The ability to locate.</param>
		/// <returns>True if the ability can be cast, false otherwise.</returns>
		public Boolean CanCast(Database.Ability ability)
		{
			Single cooldown;
			if (!Cooldowns.TryGetValue(ability, out cooldown)) return false;
			if (cooldown > 0) return false;

			return true;
		}

		internal Boolean QueueCast(Database.Ability ability, Targetable target, Vector3 position)
		{
			if (State == States.Casting || Parent.IsDestroyed || (ability.RequiresTarget && (target == null || target.IsDestroyed)) || !CanCast(ability))
				return false;

			State = States.Waiting;
			Target = target;
			TargetPosition = position;

			TargetAbility = ability;
			CastTimeRemaining = TargetAbility.CastTime;

			return true;
		}

		internal void Stop()
		{
			State = States.Idle;

			if (TargetAbility != null)
			{
				TargetAbility.OnInterruptedCast(AbilityTag, Parent, Target, TargetPosition);
				TargetAbility = null;
			}
		}

		internal override void Update()
		{
			for (Int32 i = 0; i < Parent.Properties.Abilities.Count; i++)
				Cooldowns[Parent.Properties.Abilities[i]] -= Time.deltaTime;
			
			if (State == States.Waiting)
			{
				Single dist;
				if (Target != null && !Target.IsDestroyed)
					dist = Vector3.Distance(Target.transform.position, Parent.transform.position) -
						Target.Radius - Parent.Properties.Radius;
				else dist = Vector3.Distance(Parent.transform.position, TargetPosition);

				if (dist < TargetAbility.CastRange)
				{
					State = States.Casting;
					AbilityTag = TargetAbility.OnBeginCast(Parent, Target, TargetPosition);
				}
				
			}
			else if (State == States.Casting)
			{
				CastTimeRemaining -= Time.deltaTime;

				AbilityTag = TargetAbility.OnChannel(AbilityTag, CastTimeRemaining, Parent, Target, TargetPosition);

				if (TargetAbility.RequiresTarget && Target.IsDestroyed)
				{
					var temp = TargetAbility;

					TargetAbility = null;
					State = States.Idle;

					Cooldowns[temp] = temp.CastCooldown;
					temp.OnInterruptedCast(AbilityTag, Parent, Target, TargetPosition);
				}

				if (CastTimeRemaining < 0)
				{
					var temp = TargetAbility;
					
					if (temp == null)
					{
						return;
					}

					TargetAbility = null;
					State = States.Idle;

					if (Parent.Controllers.MovementController != null)
						Parent.Controllers.MovementController.Stop();

					Cooldowns[temp] = temp.CastCooldown;
					temp.OnEndCast(AbilityTag, Parent, Target, TargetPosition);
				}
			}
		}

		internal override object GetState()
		{
			UnityEngine.Debug.Log(this.GetType().Name + " does not implement GetState()");
			return null;
		}

		internal override void SetState(object state)
		{
			UnityEngine.Debug.Log(this.GetType().Name + " does not implement SetState()");
		}
	}
}