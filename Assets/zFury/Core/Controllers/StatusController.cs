﻿using System;
using System.Collections.Generic;

using UnityEngine;
using Fury.Behaviors;

namespace Fury.Controllers
{
	/// <summary>
	/// The status controller controlls the application, maintainence and removal of status effects from a unit.
	/// </summary>
	public sealed class StatusController : Controller
	{
		/// <summary>
		/// Data structure containing information about a single, applied status effect.
		/// </summary>
		public class StatusInfo
		{
			/// <summary>
			/// The base properties of the applied status effect.
			/// </summary>
			public Database.Status Properties { get; private set; }

			/// <summary>
			/// The unit that applied the effect.
			/// </summary>
			public Behaviors.Unit From { get; private set; }

			/// <summary>
			/// The time remaining on the status effect.
			/// </summary>
			public Single Remaining { get; internal set; }

			/// <summary>
			/// An optional, user-defined that is *not* synchronized across the network.
			/// </summary>
			public System.Object Tag { get; set; }

			/// <summary>
			/// Time remaining before a periodic status effect is updated. This is only valid for status effects that 
			/// implement the IPeriodic interface.
			/// </summary>
			public Single PeriodRemaining { get; internal set; }

			internal StatusInfo(Database.Status properties, Behaviors.Unit from, System.Object tag)
			{
				Properties = properties;
				Remaining = properties.Duration;
				From = from;
				Tag = tag;

				if (Properties is Fury.Database.Status.IPeriodic)
					PeriodRemaining = (Properties as Database.Status.IPeriodic).Period;
			}
		}

		/// <summary>
		/// The parent this status controller belongs to.
		/// </summary>
		public Unit Parent { get; private set; }

		/// <summary>
		/// A list of status effects applied to the parent.
		/// </summary>
		public General.ReadOnlyList<StatusInfo> Statuses { get; private set; }

		/// <summary>
		/// The total speed modifier from all status effects.
		/// </summary>
		public Single ModifierSpeed { get; private set; }
		public Single ModifierASpeed { get; private set; }
		/// <summary>
		/// The total health modifier from all status effects.
		/// </summary>
		public Int32 ModifierHealth { get; private set; }

		/// <summary>
		/// True if any status effect has stunned the parent, false otherwise.
		/// </summary>
		public Boolean IsStunned { get; private set; }

		/// <summary>
		/// True if none of the status effects prevent order compliance.
		/// </summary>
		public Boolean IsOrderable { get; private set; }

		internal StatusController(Unit parent)
		{
			Parent = parent;
			Statuses = new General.ReadOnlyList<StatusInfo>();
			IsOrderable = true;
			IsStunned = false;
			ModifierHealth = 0;
			ModifierSpeed = 1f;
			ModifierASpeed = 1f;
		}

		internal void Add(Database.Status status, Behaviors.Unit from)
		{
			Remove(status);

			var tag = status.OnStatusAdded(Parent, from);
			Statuses.Add(new StatusInfo(status, from, tag));
		}

		internal Boolean Remove(Database.Status status)
		{
			for (Int32 i = 0; i < Statuses.Count; i++)
				if (Statuses[i].Properties == status)
				{
					var temp = Statuses[i];
					Statuses.RemoveAt(i--);

					Manager.Instance.RelayStatusRemoved(temp.Properties,  Parent, temp.From, temp.Tag);

					return true;
				}

			return false;
		}

		internal override void Update()
		{
			Update(true);
		}

		internal void Update(Boolean full)
		{
			IsStunned = false;
			IsOrderable = true;
			ModifierSpeed = 1f;
			ModifierASpeed = 1f;
			ModifierHealth = 0;

			for (Int32 i = 0; i < Statuses.Count; i++)
			{
				var statusInfo = Statuses[i];
				var status = Statuses[i].Properties;

				if (status is Database.Status.IStun)
					if ((status as Database.Status.IStun).IsStunned(statusInfo.From, Parent))
					{
						IsStunned = true;
						IsOrderable = false;
						ModifierSpeed = 0f;
						ModifierASpeed = 1f;
					}

				if (status is Database.Status.IOrderable)
					if (!(status as Database.Status.IOrderable).IsOrderable(statusInfo.From, Parent))
						IsOrderable = false;

				if (status is Database.Status.ISpeed)
					ModifierSpeed *= (status as Database.Status.ISpeed).GetModifier(statusInfo.From, Parent);

				if (status is Database.Status.IASpeed)
				{
					ModifierASpeed *= (status as Database.Status.IASpeed).GetModifier(statusInfo.From, Parent);
				}

				if (status is Database.Status.IHealth)
					ModifierHealth += (status as Database.Status.IHealth).GetModifier(statusInfo.From, Parent);

				if (full)
				{
					if (status is Database.Status.IPeriodic)
					{
						statusInfo.PeriodRemaining -= Time.deltaTime;
						if (statusInfo.PeriodRemaining < 0)
						{
							statusInfo.PeriodRemaining += (status as Database.Status.IPeriodic).Period;
							statusInfo.Tag = (status as Database.Status.IPeriodic).OnUpdate(statusInfo.Tag, Parent, statusInfo.From);
						}
					}

					statusInfo.Remaining -= Time.deltaTime;

					if (statusInfo.Remaining < 0)
					{
						Statuses.RemoveAt(i--);

						Manager.Instance.RelayStatusRemoved(statusInfo.Properties, Parent, statusInfo.From, statusInfo.Tag);
					}
				}
			}
		}

		internal override object GetState()
		{
			UnityEngine.Debug.Log(this.GetType().Name + " does not implement GetState()");
			return null;
		}

		internal override void SetState(object state)
		{
			UnityEngine.Debug.Log(this.GetType().Name + " does not implement SetState()");
		}
	}
}