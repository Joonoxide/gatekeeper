﻿using UnityEngine;
using System.Collections;
using Fury.Behaviors;

public class TestInitializer : MonoBehaviour {
	
	// Use this for initialization
	void Start () 
	{	
		//Fury.Behaviors.Manager.Instance.OnMapLoaded += OnMapLoaded;
		Debug.Log (Fury.Behaviors.Manager.Instance.Commanders.Count + " " + Fury.Behaviors.Manager.Instance.Commanders[1]);
		MakeAnotherCube();
	}
	
	void OnLevelWasLoaded()
	{
		Debug.Log (Fury.Behaviors.Manager.Instance.Commanders.Count + " " + Fury.Behaviors.Manager.Instance.Commanders[1]);
		GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
		
		cube.transform.position = Vector3.zero + new Vector3(-2, 0, 0);
	}
	
	void MakeAnotherCube()
	{
		Debug.Log (Fury.Behaviors.Manager.Instance.Commanders.Count + " " + Fury.Behaviors.Manager.Instance.Commanders[1]);
		
		GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
		
		cube.transform.position = Vector3.zero + new Vector3(2, 0, 0);
	}
}
