﻿using UnityEngine;
using System.Collections;

public class GameLoader : MonoBehaviour {
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnGUI()
	{
		Debug.Log (Fury.Behaviors.Manager.Instance);
		if (Fury.Behaviors.Manager.Instance.GameState == Fury.GameStates.Menu)
		{
			if (GUI.Button(new Rect((Screen.width - 100) * 0.5f, Screen.height * 0.2f, 100f, 40f), "Host Game"))
			{
				Fury.Behaviors.Manager.Instance.Host(7000, Fury.Behaviors.Manager.Instance.AvailableMaps[0], "FuryTest.MapHosting", "", false, null);
				Fury.Behaviors.Manager.Instance.CreateAICommander(Fury.CommanderIndices.Two, null);
			}
		}
		
		else if (Fury.Behaviors.Manager.Instance.GameState == Fury.GameStates.Lobby)
		{
			if (GUI.Button(new Rect((Screen.width - 100) * 0.5f, Screen.height * 0.2f, 100f, 40f), "Start Game"))
			{
				Fury.Behaviors.Manager.Instance.StartGame();
			}
		}
		
	}
}
