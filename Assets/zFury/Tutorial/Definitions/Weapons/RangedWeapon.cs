﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

class RangedWeapon : Fury.Database.Weapon
{
	public GameObject ArrowPrefab = null;

	public Single Speed = 20f;
	
	public UnityEngine.GameObject ShootEffect = null;
	
	public UnityEngine.GameObject HitEffect = null;
	
	private TowerController tCtrl;
	
	public override void OnAttackComplete(Fury.Behaviors.Unit attacker, Fury.Behaviors.Unit target)
	{
		/*
		tCtrl = attacker.gameObject.GetComponent("TowerController") as TowerController;
		
		var arrow = (GameObject)GameObject.Instantiate(ArrowPrefab, new Vector3(0, 0, 100), Quaternion.identity);
		var projectile = arrow.AddComponent<Projectile>();
		
		// Create the effect
		var effect = (GameObject)
			GameObject.Instantiate(ShootEffect, new Vector3(1000, 0, 0), Quaternion.identity);
		
		// Position the effect
		var position = attacker.transform.FindChild("-turret").position;
		effect.transform.position = position;
		effect.transform.parent = target.transform;
		
		projectile.FlightDuration = Vector3.Distance(attacker.transform.position, target.transform.position) / Speed;
		projectile.StartPoint = attacker.transform.FindChild("-turret").position;
		projectile.WeaponSource = this;
		projectile.AttackingUnit = attacker;
		projectile.TargetUnit = target;
		projectile.multiplier = tCtrl.tLevel;
		*/
	}
}