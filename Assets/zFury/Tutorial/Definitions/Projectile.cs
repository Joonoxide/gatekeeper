﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

class Projectile : MonoBehaviour
{
	public Vector3 StartPoint;
	public Fury.Behaviors.Unit TargetUnit, AttackingUnit;
	public Fury.Database.Weapon WeaponSource;
	public Single FlightDuration;
	public int multiplier;
	public Vector3 dest;
	private Single TimeElapsed;
	
	public UnityEngine.GameObject HitEffect = Resources.Load("rifle_hit") as UnityEngine.GameObject;
	
	public void Update()
	{	
		TimeElapsed += Time.deltaTime;

		if (TargetUnit.IsDestroyed)
			GameObject.Destroy(gameObject);

		var age = Mathf.Clamp01(TimeElapsed / FlightDuration);
		var pos = Vector3.Lerp(StartPoint, TargetUnit.collider.bounds.center, age);

		gameObject.transform.rotation = Quaternion.LookRotation(pos - transform.position);
		gameObject.transform.position = pos;

		if (age > 0.99f)
		{
			TargetUnit.ModifyHealth(-(WeaponSource.Damage), AttackingUnit, WeaponSource);
			
			// Create and position on-hit effect
			var rifleHit = (GameObject)
				GameObject.Instantiate(HitEffect);
			var hitPosition = dest;
			rifleHit.transform.position = hitPosition;
			//rifleHit.transform.parent = TargetUnit.transform;
			
			GameObject.Destroy(gameObject);
		}
	}
}