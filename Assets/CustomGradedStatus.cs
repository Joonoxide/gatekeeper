﻿using UnityEngine;
using System.Collections;

public class CustomGradedStatus : Fury.Database.Status {
	
	[SerializeField]
	protected int effectIndex = 0;
	
	[SerializeField]
	protected float [] effectMagnitudeByRank;
	
	public int effectiveRank {protected set; get;}
}
