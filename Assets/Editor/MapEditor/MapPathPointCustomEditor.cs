﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(MapPathPoints))]
public class MapPathPointCustomEditor : Editor {

	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();
		
		EditorGUILayout.BeginHorizontal();
		
		if (GUILayout.Button("Add Path Point"))
		{
			GameObject newPt = new GameObject();
			
			GameObject container = (target as MapPathPoints).gameObject;
			
			newPt.transform.position = container.transform.position;
			newPt.transform.parent = container.transform;
			newPt.name = "pathPoint" + container.GetComponent<MapPathPoints>().getNodeCount().ToString();
			
			container.GetComponent<MapPathPoints>().appendPoint(newPt.transform);
		}
		
		EditorGUILayout.EndHorizontal();
	}
}
