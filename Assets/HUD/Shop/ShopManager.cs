using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using LitJson;
using KYData;

public class ShopManager : MonoBehaviour {

	List<SpellWrapper> arraySpellData = new List<SpellWrapper>();
	public SpellWrapper currSpellData = null;
	public UIStateToggleBtn iconSpell;
	public SpriteText textDescription;
	public SpriteText textName;
	public SpriteText textCost;
	public UIButton buttonBuy;
	public KYScroll scrollItem;
	public Transform transformStocks;
	public UIPanel panelMoreRuby;
	public List<UIStateToggleBtn> arraySpellIcon;
	public ParticleSystem ps;
	public UIButton buttonSell;
	public static bool isBlocking;

	void Start () 
	{
		GA.API.Design.NewEvent(CustomDesignEvent.getNavigationWindow(NavigationType.Magic));
		
		TextAsset data = Resources.Load("spell_json") as TextAsset;
		string strSpellData = data.text;
		arraySpellData = JsonMapper.ToObject<List<SpellWrapper>>(strSpellData);
		GameObject iconSpellGO = Resources.Load("icon_spellinfo") as GameObject;
		GameObject newIconSpell = null;
		UIStateToggleBtn buttonSpell;
		int k = 0;
		/*
		foreach(Transform child in transformStocks)
		{
			newIconSpell = Instantiate(iconSpellGO, Vector3.zero, Quaternion.identity) as GameObject;
			newIconSpell.transform.position = child.position;		
			newIconSpell.transform.rotation = child.rotation;
			newIconSpell.transform.localScale = child.localScale;
			buttonSpell = newIconSpell.GetComponent<UIStateToggleBtn>();	
			buttonSpell.SetToggleState("State "+arraySpellData[k].index);
			buttonSpell.scriptWithMethodToInvoke = this;
			buttonSpell.methodToInvoke = "onClickStock";
			buttonSpell.delay = k+1;
			++k;
		}
		*/
		GameObject textGO = Resources.Load("KYText") as GameObject;
		GameObject newTextGO = null;
		Vector3 textPos;
		SpriteText spriteText = null;
		KYDataManager manager = KYDataManager.getInstance();

		/*
		foreach(SpellWrapper spellWrapper in KYDataManager.getInstance().arraySpellWrapper)
		{
			newIconSpell = Instantiate(iconSpellGO, Vector3.zero, Quaternion.identity) as GameObject;
			newIconSpell.GetComponent<UIStateToggleBtn>().width = 12f;
			newIconSpell.GetComponent<UIStateToggleBtn>().height = 12f;
			buttonSpell = newIconSpell.GetComponent<UIStateToggleBtn>();	
			buttonSpell.SetToggleState("State "+spellWrapper.index);
			scrollItem.addChild(newIconSpell);		
			newTextGO = Instantiate(textGO, Vector3.zero, Quaternion.identity) as GameObject;
			textPos = newIconSpell.transform.position;
			textPos.x += 7;
			textPos.z -= 1;
			newTextGO.transform.position = textPos;
			spriteText = newTextGO.GetComponent<SpriteText>();
			spriteText.Text = spellWrapper.count+"";
			spriteText.SetCharacterSize(10f);
		}
		*/

		Transform stocks = GameObject.Find("stock").transform;
		arraySpellIcon = new List<UIStateToggleBtn>();
		foreach(Transform child in stocks)
		{
			arraySpellIcon.Add(child.gameObject.GetComponent<UIStateToggleBtn>());
		}
		reload();

		if(KYDataManager.getInstance().arraySpellWrapper.Count < 3)
		{
			onClickStock(1);
		}
		else
		{
			onClickStock(arraySpellIcon[0]);
		}

		g1.SetActive(false);
	}

	public GameObject g1;
	public GameObject g2;

	public void onClickStock(UIButton button)
	{
		onClickStock((int)button.delay);
	}

	public void onClickStock(UIStateToggleBtn button)
	{
		onClickStock((int)button.delay);
	}

	bool sss = false;
	public bool isXP;
	public void onClickStock(int index)
	{
		if(isBlocking)
		{
			return;
		}
		if(index > 7)
		{
			index -= 7;
			sss = false;
			buttonBuy.controlIsEnabled = true;
			buttonBuy.transform.Find("text_ruby").gameObject.GetComponent<SpriteText>().Text = "SELL SCROLL";
		}
		else
		{
			sss = true;
			if(KYDataManager.getInstance().arraySpellWrapper.Count < 3)
			{
				buttonBuy.controlIsEnabled = true;
			}
			else
			{
				buttonBuy.controlIsEnabled = false;
			}
			buttonBuy.transform.Find("text_ruby").gameObject.GetComponent<SpriteText>().Text = "BUY SCROLL";
		}
		currSpellData = arraySpellData[index-1];
		if(currSpellData.cost >= 100)
		{
			isXP = true;
		}
		else
		{
			isXP = false;
		}

		if(isXP)
		{
			g1.SetActive(false);
			g2.SetActive(true);
		}
		else
		{
			g1.SetActive(true);
			g2.SetActive(false);
		}
		iconSpell.gameObject.SetActive(true);
		iconSpell.SetToggleState("State "+index);
		textDescription.Text = currSpellData.description;
		textName.Text = currSpellData.name;
		textCost.Text = currSpellData.cost+"";

	}

	public void reload()
	{
		if(currSpellData == null)
		{
			iconSpell.gameObject.SetActive(false);
			textCost.Text = "";
			textName.Text = "";
			textDescription.Text = "";
		}
		else
		{
			onClickStock(currSpellData.index);
		}

		int i = 0;
		List<KYData.SpellWrapper> arraySpellWrapper = KYDataManager.getInstance().arraySpellWrapper;
		foreach(UIStateToggleBtn icon in arraySpellIcon)
		{
			if(i < arraySpellWrapper.Count)
			{
				icon.SetToggleState("State "+arraySpellWrapper[i].index);
				icon.gameObject.SetActive(true);
				icon.delay = arraySpellWrapper[i].index+7;
			}
			else
			{
				icon.gameObject.SetActive(false);
			}
			++i;
		}
	}

	public void onClickNo()
	{
		panelMoreRuby.StartTransition("Dismiss Forward");
	}

	public void onClickPurchase()
	{
		if(isBlocking)
		{
			return;
		}
		if(currSpellData != null)
		{
			if(sss)
			{
				if(((isXP)? KYDataManager.getInstance().dataWrapper.xpCount : KYDataManager.getInstance().dataWrapper.rubyCount) >= currSpellData.cost)
				{
					if(KYDataManager.getInstance().arraySpellWrapper.Count < 3)
					{
						Vector3 pos = arraySpellIcon[KYDataManager.getInstance().arraySpellWrapper.Count].transform.position;
						GameObject newGO = GameObject.Instantiate(ps.gameObject) as GameObject;
						newGO.AddComponent<CFX_AutoDestructShuriken>();
						newGO.transform.position = pos;
						newGO.GetComponent<ParticleSystem>().Play();
						KYDataManager.getInstance().addSpell(currSpellData);
						if(isXP)
						{
							KYDataManager.getInstance().dataWrapper.xpCount -= currSpellData.cost;
						}
						else
						{
							KYDataManager.getInstance().dataWrapper.rubyCount -= currSpellData.cost;					
						}						
						StartCoroutine(GameController.execute2(1, delegate()
						{
							reload();
						}));
						if(KYDataManager.getInstance().arraySpellWrapper.Count >= 3)
						{	
							buttonBuy.controlIsEnabled = false;
						}
					}
				}
				else
				{
					panelMoreRuby.StartTransition("Bring in Forward");
				}
			}
			else
			{
				KYDataManager.getInstance().removeSpell(currSpellData);
				if(isXP)
				{
					KYDataManager.getInstance().dataWrapper.xpCount += currSpellData.cost;
				}
				else
				{
					KYDataManager.getInstance().dataWrapper.rubyCount += currSpellData.cost;					
				}
				reload();			
				if(KYDataManager.getInstance().arraySpellWrapper.Count > 0)
				{	
					onClickStock(arraySpellIcon[0]);
				}
				else
				{
					onClickStock(1);
				}
			}
		}
	}

	public void onClickCancel()
	{
		onClickGem();
		panelMoreRuby.StartTransition("Dismiss Forward");		
	}

	public void onClickIAP()
	{
		panelMoreRuby.StartTransition("Dismiss Forward");
	}

	public void onClickSellScroll()
	{
		if(isBlocking)
		{
			return;
		}
		GameController.LoadLevel("fragment", TransitionType.NoChange);		
	}

	public void onClickBack()
	{
		if(isBlocking)
		{
			return;
		}
		GameController.LoadLevel("title", TransitionType.Open);
		currSpellData = null;
	}

	public void onClickGem()
	{
		isBlocking = true;
		FragmentManager.instance.comein();	
		maincam.instance.overlaycam_above.enabled = true;
	}

}
