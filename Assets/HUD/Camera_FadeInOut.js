// FadeInOut
//
//--------------------------------------------------------------------
//                        Public parameters
//--------------------------------------------------------------------
 
public var fadeOutTexture : Texture2D;
public var fadeSpeed = 0.3;
private var draw : boolean = true;
 
var drawDepth = -1000;
 
//--------------------------------------------------------------------
//                       Private variables
//--------------------------------------------------------------------
 
private var alpha = 1.0; 
 
private var fadeDir = -1;
 
//--------------------------------------------------------------------
//                       Runtime functions
//--------------------------------------------------------------------
 
//--------------------------------------------------------------------
 
function OnGUI()
{
	if(draw)
	{
		if(fadeDir == -1 && alpha >= 0 || fadeDir == 1 && alpha <= 1)
		{
			alpha += fadeDir * fadeSpeed * Time.deltaTime;	
			alpha = Mathf.Clamp01(alpha);	
		 
			GUI.color.a = alpha;
		 
			GUI.depth = drawDepth;
		 
			GUI.DrawTexture(Rect(0, 0, Screen.width, Screen.height), fadeOutTexture);
			
			
		}
	}
}
 
//--------------------------------------------------------------------
 
function fadeIn(){
	fadeDir = -1;	
}
 
//--------------------------------------------------------------------
 
function fadeOut(){
	fadeDir = 1;	
}

function ClearScreenFade()
{
	draw = false;
}
 
function Start(){
	//alpha=1;
	fadeIn();
	fadeOutTexture = new Texture2D(1, 1);
    fadeOutTexture.SetPixel(1, 1, Color.black);
    fadeOutTexture.Apply();
}