﻿using UnityEngine;
using System.Collections;

public class TextHarvester : MonoBehaviour 
{
	SpriteText text = null;
	int prev;

	void Start () 
	{
		text = GetComponent<SpriteText>();
		int xpCount = KYDataManager.getInstance().upgradeNameForLevel("Harvester Count")+2;
		text.Text = xpCount+"/7";
		prev = xpCount;
	}
	
	// Update is called once per frame
	void Update () 
	{
		int xpCount = KYDataManager.getInstance().upgradeNameForLevel("Harvester Count")+2;
		if(prev != xpCount)
		{
			text.Text = xpCount+"/7";
			prev = xpCount;
		}
	}
}
