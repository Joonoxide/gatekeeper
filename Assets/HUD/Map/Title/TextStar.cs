using UnityEngine;
using System.Collections;

public class TextStar : MonoBehaviour {

	SpriteText text = null;
	int prev;

	void Start () 
	{
		text = GetComponent<SpriteText>();
		int starCount = KYDataManager.getInstance().getRemainingStarCount();
		text.Text = starCount+"/"+(TitleController.maxLevel-1)*3;
		prev = starCount;
	}
	
	// Update is called once per frame
	void Update () 
	{
		int starCount = KYDataManager.getInstance().getStarCount(); //getRemainingStarCount();
		if(prev != starCount)
		{
			text.Text = starCount+"/"+(TitleController.maxLevel-1)*3;
			prev = starCount;
		}
	}
}
