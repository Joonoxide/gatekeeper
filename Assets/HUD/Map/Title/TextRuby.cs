using UnityEngine;
using System.Collections;

public class TextRuby : MonoBehaviour {

	SpriteText text = null;
	int prev;

	void Start () 
	{
		text = GetComponent<SpriteText>();
		int rubyCount = KYDataManager.getInstance().dataWrapper.rubyCount;
		text.Text = rubyCount+"";
		prev = rubyCount;
	}
	
	// Update is called once per frame
	void Update () 
	{
		int rubyCount = KYDataManager.getInstance().dataWrapper.rubyCount;
		if(prev != rubyCount)
		{
			text.Text = rubyCount+"";
			prev = rubyCount;
		}
	}
}
