﻿using UnityEngine;
using System.Collections;

public class TextStarUpgrade : MonoBehaviour {
	
	SpriteText text = null;
	int prev;
	
	// Use this for initialization
	void Start () {
		text = GetComponent<SpriteText>();
		int starCount = KYDataManager.getInstance().getRemainingStarCount();
		text.Text = starCount+"/"+ KYDataManager.getInstance().getStarCount().ToString();
		prev = starCount;
	}
	
	// Update is called once per frame
	void Update () {
		int starCount = KYDataManager.getInstance().getRemainingStarCount();
		if(prev != starCount)
		{
			text.Text = starCount+"/"+ KYDataManager.getInstance().getStarCount().ToString();
			prev = starCount;
		}
	
	}
}
