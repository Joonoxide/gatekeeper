using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using UnityEngine;
using LitJson;
using KYData;

namespace KYData
{
	public class TrooperWrapper
	{
		public TrooperWrapper() { }
		public TrooperWrapper(TrooperWrapper trooperWrapper) 
		{
			this.typeIndex = trooperWrapper.typeIndex;
			this.name = trooperWrapper.name;
			this.genre = trooperWrapper.genre;
			this.portraitIndices = trooperWrapper.portraitIndices;
		}
		public int indexIcon
		{
			get
			{
				if (this.portraitIndices != null)
				{
					return this.portraitIndices[(level-1)/10];
				}
				
				else
				{
					return typeIndex;
				}
			}
		}
		
		public int typeIndex;
		public List<int> portraitIndices;
		public string name;
		public int identifier;
		public int level = 0;
		public int genre;
		public int cost;
		public bool unlock;
		public int xpCount = 0;
		public int totalxpCount = 0;
		
		public bool isBookmarked = false;
	}
	
	public class SlotWrapper
	{
		public int slotCount;
		public List<int> arrayTrooperIdentifier;
	}

	public class DataWrapper
	{
		public int rubyCount;
		public int starCount;
		public int xpCount;
		public int usedStar;
		public DateTime lastSummon;
	}

	public class SpellWrapper
	{
		public SpellWrapper() { }
		public SpellWrapper(SpellWrapper spellWrapper) 
		{
			this.index = spellWrapper.index;
			this.name = spellWrapper.name;
		}
		public int count = 0;
		public string name;
		public int index;
		public string description;
		public int cost;
	}

	public class UpgradeWrapper
	{
		public UpgradeWrapper() { }
		public UpgradeWrapper(UpgradeWrapper upgradeWrapper)
		{
			this.index = upgradeWrapper.index;
			this.name = upgradeWrapper.name; 
		}
		public int index;
		public string name;
		public int level;
		public string description;
		public int cost;
		
		public UpgradeLanguageField titleLanguageField
		{
			get
			{
				switch (index)
				{
				case 1:
					return UpgradeLanguageField.UPGRADE_HARVESTER_TITLE;
					break;
					
				case 2:
					return UpgradeLanguageField.UPGRADE_TOWER_MASTERY_TITLE;
					break;
					
				case 3:
					return UpgradeLanguageField.UPGRADE_DAMAGE_AURA_TITLE;
					break;
					
				case 4:
					return UpgradeLanguageField.UPGRADE_DEFEND_AURA_TITLE;
					break;
				default:
					return UpgradeLanguageField.UPGRADE_HARVESTER_TITLE;
					break;
				}
			}
		}
		
		public UpgradeLanguageField descriptionLanguageField
		{
			get
			{
				switch (index)
				{
				case 1:
					return UpgradeLanguageField.UPGRADE_HARVESTER_DESCRIPTION;
					break;
					
				case 2:
					return UpgradeLanguageField.UPGRADE_TOWER_MASTERY_DESCRIPTION;
					break;
					
				case 3:
					return UpgradeLanguageField.UPGRADE_DAMAGE_AURA_DESCRIPTION;
					break;
					
				case 4:
					return UpgradeLanguageField.UPGRADE_DEFEND_AURA_DESCRIPTION;
					break;
				default:
					return UpgradeLanguageField.UPGRADE_HARVESTER_DESCRIPTION;
					break;
				}
			}
		}
	}

	public class LevelWrapper
	{
		public LevelWrapper() { }
		public LevelWrapper(int level)
		{
			this.level = level;
		}
		public LevelWrapper(LevelWrapper levelWrapper)
		{
			this.level = levelWrapper.level;
			this.star = levelWrapper.star;
		}
		public int level;
		public int star;
		public int count;
	}
	
	public class DescriptionWrapper
	{
		public string name;
		public string unitDescription, humourDescription;
		public int index;
		
		public List<string> abilityDescriptions;
		
		public string getAbilityDescription(int level)
		{
			return abilityDescriptions[(level-1)/10];
		}
	}
	
	public class LevelRewardWrapper
	{
		public int level;
		public List<int> fragments;
		public List<string> unlockableCharacters;
	}
	
	public class HintWrapper
	{
		public List<string> hintText;
		
		public string getRandomHint()
		{
			if (hintText != null)
			{
				return hintText[UnityEngine.Random.Range(0, hintText.Count)];
			}
			
			return "I'd give you a hint, IF I HAD ONE!";
		}
	}
	
	public class UnitVariableAbilityWrapper
	{
		public string name;
		public int index;
		public List<int> rank_0, rank_1, rank_2;
		
		public List<int> getVariableByRank(int rank)
		{
			List<int> result = rank_0;
			
			switch (rank)
			{
			case 0:
				result = rank_0;
				break;
				
			case 1:
				result = rank_1;
				break;
				
			case 2:
				result = rank_2;
				break;
			}
			
			return result;
		}
	}
}

public class KYDataManager 
{
	public static KYDataManager instance;
	public List<TrooperWrapper> arrayTrooperWrapper = new List<TrooperWrapper>();
	public SlotWrapper slotWrapper = new SlotWrapper();
	public DataWrapper dataWrapper = new DataWrapper();
	public List<SpellWrapper> arraySpellWrapper = new List<SpellWrapper>();
	public List<UpgradeWrapper> arrayUpgradeWrapper = new List<UpgradeWrapper>();
	public List<LevelWrapper> arrayLevelWrapper = new List<LevelWrapper>();
	public List<EquipmentManager.XPWrapper> arrayXPWrapper = new List<EquipmentManager.XPWrapper>();
	public static List<EquipmentManager.XPWrapper> aaa = new List<EquipmentManager.XPWrapper>();
	
	public static List<DescriptionWrapper> arrayDescriptionWrapper = new List<DescriptionWrapper>();
	public static List<LevelRewardWrapper> arrayLevelRewardWrapper = new List<LevelRewardWrapper>();
	public static List<UnitVariableAbilityWrapper> arrayUnitVariableAbilityWrapper = new List<UnitVariableAbilityWrapper>();

	public static KYDataManager getInstance()
	{
		if(instance == null)
		{
			GameObject data = Resources.Load("data") as GameObject;
			GameObject newData = GameObject.Instantiate(data, Vector3.zero, Quaternion.identity) as GameObject;
			TextAsset dat = Resources.Load("lotto_json") as TextAsset;
			string strXPData = dat.text;
			aaa = JsonMapper.ToObject<List<EquipmentManager.XPWrapper>>(strXPData);
			
			aaa = aaa.OrderBy(o=>o.index).ToList();
			
			dat = Resources.Load ("unitDescriptions_json") as TextAsset;
			
			arrayDescriptionWrapper = JsonMapper.ToObject<List<DescriptionWrapper>>(dat.text);
			
			dat = Resources.Load ("levelreward_json") as TextAsset;
			
			arrayLevelRewardWrapper = JsonMapper.ToObject<List<LevelRewardWrapper>>(dat.text);
			
			dat = Resources.Load ("unitAbilityVariables") as TextAsset;
			
			arrayUnitVariableAbilityWrapper = JsonMapper.ToObject<List<UnitVariableAbilityWrapper>>(dat.text);
			
			instance = new KYDataManager();	
		}
		
		return instance;
	}

	public KYDataManager() 
	{
		arrayXPWrapper = aaa;
		bool init = (PlayerPrefs.HasKey("IsInit") & PlayerPrefs.GetInt("IsInit") == 1);
		if(!init)
		{
			initData();
		}
		bool isFetched = fetchData();
		int loopCount = 0;
		while(!isFetched)
		{
			initData();
			isFetched = fetchData();
			++loopCount;
			if(loopCount > 10)
			{
				break;
			}
		}
	}

	public void initialize()
	{
		initData();
		fetchData();
	}

	public void initData()
	{
		arrayTrooperWrapper = new List<TrooperWrapper>();

		int swordmanIdentifier = addTrooper("Call_Swordman", true, 1);
		/*addTrooper("Call_Swordman", true, 11);
		addTrooper("Call_Swordman", true, 21);*/
				
		int archerIdentifier = addTrooper("Call_Archer", true, 1);
		/*addTrooper("Call_Archer", true, 11);
		addTrooper("Call_Archer", true, 21);*/
		
		/*int crusaderIdentifier = addTrooper("Call_Crusader", true, 1);
		addTrooper("Call_Crusader", true, 11);
		addTrooper("Call_Crusader", true, 21);
		
		int griffinIdentifier = addTrooper("Call_Griffin", true, 1);
		addTrooper("Call_Griffin", true, 11);
		addTrooper("Call_Griffin", true, 21);
		
		int priestIdentifier = addTrooper("Call_Priest", true, 1);
		addTrooper("Call_Priest", true, 11);
		addTrooper("Call_Priest", true, 21);
		
		int cyclopsidentifier = addTrooper("Call_Cyclops", true, 1);
		addTrooper("Call_Cyclops", true, 11);
		addTrooper("Call_Cyclops", true, 21);
		
		int paladinIdentifier = addTrooper("Call_Paladin", true, 1);
		addTrooper("Call_Paladin", true, 11);
		addTrooper("Call_Paladin", true, 21);
		
		int medusaIdentifier = addTrooper("Call_Medusa", true, 1);
		addTrooper("Call_Medusa", true, 11);
		addTrooper("Call_Medusa", true, 21);
		
		int necromancerIdentifier = addTrooper("Call_Necromancer", true, 1);
		addTrooper("Call_Necromancer", true, 11);
		addTrooper("Call_Necromancer", true, 21);
		
		int ghoulIdentifier = addTrooper("Call_Ghoul", true, 1);
		addTrooper("Call_Ghoul", true, 11);
		addTrooper("Call_Ghoul", true, 21);
		
		int bombIdentifier = addTrooper("Call_Golem", true, 1);
		addTrooper("Call_Golem", true, 11);
		addTrooper("Call_Golem", true, 21);
		
		int elementalistIdentifier = addTrooper("Call_Elementalist", true, 1);
		addTrooper("Call_Elementalist", true, 11);
		addTrooper("Call_Elementalist", true, 21);*/
		
		int harvesterIdentifier = addTrooper("Call_Harvester", true, 1);

		string initString = JsonMapper.ToJson(arrayTrooperWrapper);
		PlayerPrefs.SetString("HasUnits", initString);
		
		//Debug.Log ("from initData... " + initString);
		arrayTrooperWrapper = null;

		SlotWrapper slot = new SlotWrapper();
		slot.slotCount = 12;
		slot.arrayTrooperIdentifier = new List<int>();
		for(int i = 0; i < slot.slotCount; ++i)
		{
			slot.arrayTrooperIdentifier.Add(0);
		}
		slot.arrayTrooperIdentifier[9] = harvesterIdentifier;
		slot.arrayTrooperIdentifier[0] = swordmanIdentifier;
		
		//slot.arrayTrooperIdentifier[1] = crusaderIdentifier;
		//slot.arrayTrooperIdentifier[2] = archerIdentifier;
		//slot.arrayTrooperIdentifier[3] = priestIdentifier;
		//slot.arrayTrooperIdentifier[4] = griffinIdentifier;
		//slot.arrayTrooperIdentifier[5] = cyclopsidentifier;
		
		initString = JsonMapper.ToJson(slot);	
		PlayerPrefs.SetString("Slots", initString);	
		
		//Debug.Log ("slots..." + initString);

		DataWrapper data = new DataWrapper();
		data.rubyCount = 10;
		data.starCount = 0;
		data.usedStar = 0;
		data.lastSummon = DateTime.Now+new TimeSpan(0, 0, -86400);

		initString = JsonMapper.ToJson(data);	
		PlayerPrefs.SetString("Data", initString);	

		List<SpellWrapper> fakeArraySpellWrapper  = new List<SpellWrapper>();
		TextAsset spelldata = Resources.Load("spell_json") as TextAsset;
		string strSpellData = spelldata.text;
		fakeArraySpellWrapper = JsonMapper.ToObject<List<SpellWrapper>>(strSpellData);
		fakeArraySpellWrapper.Clear();
		initString = JsonMapper.ToJson(fakeArraySpellWrapper);	
		//initString = "";
		PlayerPrefs.SetString("HasSpells", initString);					

		List<UpgradeWrapper> fakeUpgradeWrapper  = new List<UpgradeWrapper>();
		initString = JsonMapper.ToJson(fakeUpgradeWrapper);	
		PlayerPrefs.SetString("HasUpgrades", initString);	

		List<LevelWrapper> fakeLevelWrapper = new List<LevelWrapper>();
		initString = JsonMapper.ToJson(fakeLevelWrapper);
		PlayerPrefs.SetString("HasLevels", initString);

		PlayerPrefs.SetInt("IsInit", 1);
	}

	public bool fetchData()
	{
		try
		{
			string strHasUnits = PlayerPrefs.GetString("HasUnits");
			
			//Debug.Log (strHasUnits);
			
			arrayTrooperWrapper = new List<TrooperWrapper>();
			arrayTrooperWrapper = JsonMapper.ToObject<List<TrooperWrapper>>(strHasUnits);
			if(arrayTrooperWrapper == null)
			{
				return false;
			}
		}
		catch
		{
			return false;
		}

		try
		{
			string strSlots = PlayerPrefs.GetString("Slots");
			slotWrapper = new SlotWrapper();
			slotWrapper = JsonMapper.ToObject<SlotWrapper>(strSlots);
			if(slotWrapper == null)
			{
				return false;
			}

			string strData = PlayerPrefs.GetString("Data");
			dataWrapper = new DataWrapper();
			dataWrapper = JsonMapper.ToObject<DataWrapper>(strData);
			if(dataWrapper == null)
			{
				return false;
			}
		}
		catch
		{
			return false;
		}

		try
		{
			string strHasSpells = PlayerPrefs.GetString("HasSpells");
			arraySpellWrapper = new List<SpellWrapper>();
			arraySpellWrapper = JsonMapper.ToObject<List<SpellWrapper>>(strHasSpells);
			if(arraySpellWrapper == null)
			{
				return false;
			}
		}
		catch
		{
			return false;
		}

		try
		{
			string strHasUpgrades = PlayerPrefs.GetString("HasUpgrades");
			arrayUpgradeWrapper = new List<UpgradeWrapper>();
			arrayUpgradeWrapper = JsonMapper.ToObject<List<UpgradeWrapper>>(strHasUpgrades);
			if(arrayUpgradeWrapper == null)
			{
				return false;
			}
		}
		catch
		{
			return false;
		}

		try
		{
			string strHasLevels = PlayerPrefs.GetString("HasLevels");
			arrayLevelWrapper = new List<LevelWrapper>();
			arrayLevelWrapper = JsonMapper.ToObject<List<LevelWrapper>>(strHasLevels);
			if(arrayLevelWrapper == null)
			{
				return false;
			}
		}
		catch
		{
			return false;
		}

		return true;
	}

	public void addSpell(SpellWrapper spellWrapper)
	{
		/*
		foreach(SpellWrapper selfSpell in arraySpellWrapper)
		{
			if(selfSpell.index == spellWrapper.index)
			{
				++selfSpell.count;
				return;
			}
		}
		*/
		spellWrapper = new SpellWrapper(spellWrapper);
		spellWrapper.count = 1;
		arraySpellWrapper.Add(spellWrapper);
	}

	public void removeSpell(SpellWrapper spellWrapper)
	{
		for(int i = 0; i < arraySpellWrapper.Count; ++i)
		{
			if(spellWrapper.name == arraySpellWrapper[i].name)
			{
				arraySpellWrapper.RemoveAt(i);
				break;
			}
		}
	}

	public int getRemainingStarCount()
	{
		return KYDataManager.getInstance().dataWrapper.starCount-KYDataManager.getInstance().dataWrapper.usedStar;
	}
	
	public int getStarCount()
	{
		return KYDataManager.getInstance().dataWrapper.starCount;
	}
	
	public int getUsedStarCount()
	{
		return KYDataManager.getInstance().dataWrapper.usedStar;
	}

	public int addUpgrade(UpgradeWrapper upgradeWrapper)
	{
		int level;
		foreach(UpgradeWrapper selfUpgrade in arrayUpgradeWrapper)
		{
			if(selfUpgrade.index == upgradeWrapper.index)
			{
				level = ++selfUpgrade.level;
				return level;
			}
		}

		upgradeWrapper = new UpgradeWrapper(upgradeWrapper);
		upgradeWrapper.level = 1;
		arrayUpgradeWrapper.Add(upgradeWrapper);
		level = 1;
		return level;
	}

	public int addUpgrade(int id)
	{
		int level;
		foreach(UpgradeWrapper selfUpgrade in arrayUpgradeWrapper)
		{
			if(selfUpgrade.index == id)
			{
				level = ++selfUpgrade.level;
				return level;
			}
		}

		TextAsset data = Resources.Load("upgrade_json") as TextAsset;
		string strUpgradeData = data.text;
		List<UpgradeWrapper> arrayUpgradeData = new List<UpgradeWrapper>();
		arrayUpgradeData = JsonMapper.ToObject<List<UpgradeWrapper>>(strUpgradeData);
		UpgradeWrapper upgradeWrapper;
		upgradeWrapper = new UpgradeWrapper(arrayUpgradeData[0]);
		upgradeWrapper.level = 1;
		upgradeWrapper.name = "Harvester Count";
		upgradeWrapper.index = id;
		arrayUpgradeWrapper.Add(upgradeWrapper);
		level = 1;
		return level;
	}

	public int addUpgrade(int id, int lll)
	{
		int level;
		foreach(UpgradeWrapper selfUpgrade in arrayUpgradeWrapper)
		{
			if(selfUpgrade.index == id)
			{
				selfUpgrade.level = lll;
				level = lll;
				return level;
			}
		}

		TextAsset data = Resources.Load("upgrade_json") as TextAsset;
		string strUpgradeData = data.text;
		List<UpgradeWrapper> arrayUpgradeData = new List<UpgradeWrapper>();
		arrayUpgradeData = JsonMapper.ToObject<List<UpgradeWrapper>>(strUpgradeData);
		UpgradeWrapper upgradeWrapper;
		upgradeWrapper = new UpgradeWrapper(arrayUpgradeData[0]);
		upgradeWrapper.level = lll;
		upgradeWrapper.name = "Harvester Count";
		upgradeWrapper.index = id;
		arrayUpgradeWrapper.Add(upgradeWrapper);
		Debug.Log(upgradeWrapper.name+"::"+Network.time);
		level = lll;
		return level;
	}

	public void addTrooperXP(TrooperWrapper trooperWrapper, int xps)
	{
		if(trooperWrapper.level%10 == 0)
		{
			++trooperWrapper.level;
			trooperWrapper.xpCount = 0;
			return;
		}
		trooperWrapper.totalxpCount += xps;
		trooperWrapper.xpCount += xps;
		int sss = xpForTrooper(trooperWrapper);
		int coe = trooperWrapper.level/10+1;
		while(trooperWrapper.xpCount >= sss && trooperWrapper.level <= coe*10)
		{
			trooperWrapper.xpCount -= sss;
			++trooperWrapper.level;
			sss = xpForTrooper(trooperWrapper);
		}
		if(trooperWrapper.level >= coe*10)
		{
			trooperWrapper.level = coe*10;
			trooperWrapper.xpCount = xpForTrooper(trooperWrapper);
		}
	}

	public int fakeaddTrooperXP(TrooperWrapper trooperWrapper, int xps)
	{
		int l = trooperWrapper.level;
		int totalxpCount = xps;
		int xpCount = xps+trooperWrapper.xpCount;
		int sss = xpForTrooper(trooperWrapper, l);
		int coe = l/10+1;

		while(xpCount >= sss && l <= coe*10)
		{
			xpCount -= sss;
			++l;
			sss = xpForTrooper(trooperWrapper, l);
		}
		if(l >= coe*10)
		{
			l = coe*10;
		}
		return CollectionManager.hulala(l);
	}

	public int xpForTrooper(TrooperWrapper trooperWrapper)
	{
		EquipmentManager.XPWrapper xp = null;
		foreach(EquipmentManager.XPWrapper xpp in arrayXPWrapper)
		{
			xp = xpp;
			if(xpp.index == trooperWrapper.typeIndex)
			{
				break;
			}
		}
		//return xp.xps[(trooperWrapper.level-1)%xp.xps.Count];
		//return xp.xps[(trooperWrapper.level-1)%10];
		return xp.xps[0]*trooperWrapper.level;
	}

	public int xpForTrooper(TrooperWrapper trooperWrapper, int l)
	{
		EquipmentManager.XPWrapper xp = null;
		foreach(EquipmentManager.XPWrapper xpp in arrayXPWrapper)
		{
			xp = xpp;
			if(xpp.index == trooperWrapper.typeIndex)
			{
				break;
			}
		}
		//return xp.xps[(trooperWrapper.level-1)%xp.xps.Count];
		//return xp.xps[(l-1)%10];
		return xp.xps[0]*l;
	}

	public int addTrooper(string name, bool unlock = false, int level = 0)
	{
		TrooperWrapper wrapper = new TrooperWrapper();
		EquipmentManager.XPWrapper xpWrapper = null;
		foreach(var w in arrayXPWrapper)
		{
			if(w.name == name)
			{
				xpWrapper = w;
				break;
			}
		}
		if(xpWrapper == null)
		{
			return -1;
		}
		wrapper.typeIndex = xpWrapper.index;
		wrapper.portraitIndices = xpWrapper.portraitIndices;
		
		wrapper.name = name;
		int identifier = UnityEngine.Random.Range(1, Int32.MaxValue);
		while(identifierForTrooperWrapper(identifier) != null)
		{
			identifier = UnityEngine.Random.Range(1, Int32.MaxValue);
		}		
		wrapper.identifier = identifier;
		wrapper.genre = 0;
		wrapper.genre = xpWrapper.genre;
		wrapper.unlock = unlock;
		wrapper.level = level;
		for(int i = 1; i < wrapper.level; ++i)
		{
			wrapper.totalxpCount += xpForTrooper(wrapper, i);
		}
		wrapper.totalxpCount += xpForTrooper(wrapper, 1);
		arrayTrooperWrapper.Add(wrapper);
		TrooperWrapper trooperWrapper = wrapper;
		
		string unitName = (wrapper.name.Split('_'))[1];
		Fury.Database.Unit unit = Resources.Load(unitName) as Fury.Database.Unit;
		
		wrapper.cost = Mathf.RoundToInt(unit.cost * (1 + (0.5f * (int)((level - 1)/10))));

		return wrapper.identifier;
	}

	public TrooperWrapper identifierForTrooperWrapper(int identifier)
	{
		foreach(TrooperWrapper trooperWrapper in arrayTrooperWrapper)
		{
			if(trooperWrapper.identifier == identifier)
			{
				return trooperWrapper;
			}
		}
		return null;
	}

	public int upgradeNameForLevel(string upgradeName)
	{
		foreach(UpgradeWrapper upgradeWrapper in arrayUpgradeWrapper)
		{
			if(upgradeWrapper.name == upgradeName)
			{
				return upgradeWrapper.level;
			}
		}
		return 0;
	}

	public void save()
	{
		bool init = (PlayerPrefs.HasKey("IsInit") & PlayerPrefs.GetInt("IsInit") == 1);
		if(!init)
		{
			return;
		}
		string initString = JsonMapper.ToJson(arrayTrooperWrapper);
		PlayerPrefs.SetString("HasUnits", initString);

		initString = JsonMapper.ToJson(slotWrapper);	
		PlayerPrefs.SetString("Slots", initString);

		initString = JsonMapper.ToJson(dataWrapper);	
		PlayerPrefs.SetString("Data", initString);

		initString = JsonMapper.ToJson(arraySpellWrapper);	
		PlayerPrefs.SetString("HasSpells", initString);

		initString = JsonMapper.ToJson(arrayUpgradeWrapper);	
		PlayerPrefs.SetString("HasUpgrades", initString);

		initString = JsonMapper.ToJson(arrayLevelWrapper);	
		PlayerPrefs.SetString("HasLevels", initString);

		PlayerPrefs.Save();
	}
	
	public void saves()
	{
		bool init = (PlayerPrefs.HasKey("IsInit") & PlayerPrefs.GetInt("IsInit") == 1);
		if(!init)
		{
			return;
		}
		string initString = JsonMapper.ToJson(arrayTrooperWrapper);
		PlayerPrefs.SetString("HasUnits", initString);

		initString = JsonMapper.ToJson(slotWrapper);	
		PlayerPrefs.SetString("Slots", initString);

		initString = JsonMapper.ToJson(dataWrapper);	
		PlayerPrefs.SetString("Data", initString);

		initString = JsonMapper.ToJson(arraySpellWrapper);	
		PlayerPrefs.SetString("HasSpells", initString);

		initString = JsonMapper.ToJson(arrayUpgradeWrapper);	
		PlayerPrefs.SetString("HasUpgrades", initString);

		initString = JsonMapper.ToJson(arrayLevelWrapper);	
		PlayerPrefs.SetString("HasLevels", initString);

		PlayerPrefs.Save();
	}

	public LevelWrapper levelNumForWrapper(int level)
	{
		KYDataManager instance = KYDataManager.getInstance();
		foreach(KYData.LevelWrapper levelWrapper in instance.arrayLevelWrapper)
		{
			if(levelWrapper.level == level)
			{
				return levelWrapper;
			}
		}	
		return null;	
	}

	public LevelWrapper addLevel(int level, int star)
	{
		LevelWrapper wrapper = levelNumForWrapper(level);
		if(wrapper != null && wrapper.star >= star )
		{
			return wrapper;
		}
		if(wrapper == null)
		{
			wrapper = new LevelWrapper();
			arrayLevelWrapper.Add(wrapper);
		}
		wrapper.level = level;
		int addStar = star-wrapper.star;
		wrapper.star = star;
		dataWrapper.starCount += addStar;
		return wrapper;
	}
	
	public UnitVariableAbilityWrapper getVariableAbilityWrapper(TrooperWrapper trooperWrapper)
	{
		for (int i = 0; i < arrayUnitVariableAbilityWrapper.Count; i++)
		{
			if (arrayUnitVariableAbilityWrapper[i].index == trooperWrapper.typeIndex)
			{
				return arrayUnitVariableAbilityWrapper[i];
			}
		}
		
		return null;
	}
}
