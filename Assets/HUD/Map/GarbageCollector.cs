﻿using UnityEngine;
using System.Collections;

public class GarbageCollector : MonoBehaviour {
	
	private const float COLLECTION_INTERVAL = 10f;
	private float _currTime = 0f;
	
	// Use this for initialization
	void Start () {
	
		DontDestroyOnLoad(this);
	}
	
	// Update is called once per frame
	void Update () {
		
		_currTime += Time.deltaTime;
		
		if (_currTime >= COLLECTION_INTERVAL)
		{
			_currTime = 0f;
			System.GC.Collect();
		}
	
	}
}
