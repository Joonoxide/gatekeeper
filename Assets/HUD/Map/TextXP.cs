﻿using UnityEngine;
using System.Collections;

public class TextXP : MonoBehaviour {

	SpriteText text = null;
	int prev;

	void Start () 
	{
		text = GetComponent<SpriteText>();
		int xpCount = KYDataManager.getInstance().dataWrapper.xpCount;
		text.Text = xpCount+"";
		prev = xpCount;
	}
	
	// Update is called once per frame
	void Update () 
	{
		int xpCount = KYDataManager.getInstance().dataWrapper.xpCount;
		if(prev != xpCount)
		{
			text.Text = xpCount+"";
			prev = xpCount;
		}
	}
}
