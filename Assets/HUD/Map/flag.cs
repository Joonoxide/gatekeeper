using UnityEngine;
using System.Collections;

public class flag : MonoBehaviour 
{
	public int _star;
	public int star
	{
		set
		{
			_star = value;
			if(_star == 0)
			{
				sprite.SetCurFrame(0);		
			}
			else
			{
				sprite.SetCurFrame(_star);					
			}
		}
		get
		{
			return _star;
		}
	}
	PackedSprite sprite;
	void Awake () 
	{
		if (!Application.loadedLevelName.Contains("title"))
		{
			return;
		}
		
		/*
		sprite = gameObject.GetComponent<PackedSprite>();
		sprite.DoAnim(0);
		sprite.PauseAnim();
		sprite.SetCurFrame(2);
		*/
		GameObject effect = GameObject.Instantiate(Resources.Load("effect") as GameObject) as GameObject;
		effect.transform.parent = transform;
		effect.transform.localPosition = new Vector3(0, 0f, 1f);
		sprite = effect.GetComponent<PackedSprite>();
		
		transform.FindChild("button").GetComponent<UIButton>().scriptWithMethodToInvoke = TitleController.instance;
		
		//sprite.DoAnim(0);
		//sprite.PauseAnim();
		if(star == 0)
		{
			sprite.PlayAnim(0, 0);		
		}
		else
		{
			sprite.PlayAnim(0, star);					
		}
	}
	
	void OnDrawGizmos()
	{
		Gizmos.color = Color.cyan;
		Gizmos.DrawWireSphere(transform.position, 1);
	}
	
	void OnLevelWasLoaded()
	{
		if (!Application.loadedLevelName.Contains("title"))
		{
			gameObject.SetActive(false);
		}
	}
}
