﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class MapData : MonoBehaviour {
	
	public Transform [] levelPositions;
	public List<MapPathPoints> mapPathPoints;
	
	public static MapData instance;
	
	void Awake()
	{
		instance = this;
	}
	
	public Vector3 getStandingPositionForLevel(int level)
	{
		int index = Mathf.Max(0, level - 1); //offset by -1 as array starts at index 0
		
		return mapPathPoints[index].pathPoints[0].transform.position;
	}
	
	public List<Vector3> getPathToNextLevel(int level)
	{
		int index = Mathf.Max(0, level - 1); //offset by -1 as array starts at index 0
		
		List <Vector3> pts;
		
		pts = mapPathPoints[index].pathPoints.Select(p => p.transform.position).ToList();
		
		if (level < TitleController.maxLevel - 2)
		{
			pts.Add(mapPathPoints[index + 1].pathPoints[0].transform.position);
		}
		
		return pts;
	}
	
	public float getTotalDistance(Vector3 [] pts)
	{
		float dist = 0;
		
		for (int i = 1; i < pts.Length; i++)
		{
			dist += (pts[i] - pts[i-1]).magnitude;
		}
		
		return dist;
	}
	
	public float getTotalDistance(List<Vector3> pts)
	{
		float dist = 0;
		
		for (int i = 1; i < pts.Count; i++)
		{
			dist += (pts[i] - pts[i-1]).magnitude;
		}
		
		return dist;
	}
	
	void OnDrawGizmos()
	{
		List <Vector3> pts = new List<Vector3>();
		
		for (int i = 0; i < TitleController.maxLevel - 1; i++)
		{
			pts.AddRange(mapPathPoints[i].pathPoints.Select(p => p.transform.position).ToList());
		}
		
		iTween.DrawPath(pts.ToArray(), Color.red);
		
	}
}
