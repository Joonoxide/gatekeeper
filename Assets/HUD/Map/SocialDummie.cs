﻿using UnityEngine;
using System.Collections;

public class SocialDummie : MonoBehaviour {

	// Use this for initialization
	void Start () {

		Screen.autorotateToPortrait = false;
		Screen.autorotateToPortraitUpsideDown = false;
		Screen.autorotateToLandscapeRight = true;
		Screen.autorotateToLandscapeLeft = true;
		Screen.orientation = ScreenOrientation.AutoRotation;
		DontDestroyOnLoad(gameObject);
		Screen.sleepTimeout = (int)SleepTimeout.NeverSleep;	

		(GameObject.Instantiate(Resources.Load("sprites") as GameObject) as GameObject).SetActive(false);
		StartCoroutine(lateStart());
	}
	
	IEnumerator lateStart()
	{
		yield return null;
	}
	// Update is called once per frame
	void Update () {
	
	}

	public void OnLevelWasLoaded(int level)
	{
		Screen.autorotateToPortrait = false;
		Screen.autorotateToPortraitUpsideDown = false;
		Screen.autorotateToLandscapeRight = true;
		Screen.autorotateToLandscapeLeft = true;
		Screen.orientation = ScreenOrientation.AutoRotation;
	}
}
