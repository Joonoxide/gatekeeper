﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MapPathPoints : MonoBehaviour {

	public List<Transform> pathPoints;
	
	void OnDrawGizmos()
	{
		Gizmos.color = Color.white;
		
		cleanUpList();
		
		foreach(Transform pt in pathPoints)
		{
			if (pt)
			{
				Gizmos.DrawWireSphere(pt.position, 0.5f);
			}
			
		}
	}
	
	void cleanUpList()
	{
		for (int i = 0; i < pathPoints.Count - 1; i++)
		{
			if (pathPoints[i] == null)
			{
				pathPoints[i] = pathPoints[i + 1];
				pathPoints[i].gameObject.name = "pathPoint" + i.ToString();
				pathPoints.RemoveAt(i + 1);
			}
		}
	}
	
	public void appendPoint(Transform point)
	{
		pathPoints.Add(point);
	}
	
	public int getNodeCount()
	{
		return pathPoints.Count;
	}
}
