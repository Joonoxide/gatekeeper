﻿using UnityEngine;
using System.Collections;

public class birdfly : MonoBehaviour {

	public float speed = 0.05f;
	public float radius = 2f;
	public float visibleDuration = 5f;
	public float invisibleDuration = 5f;
	
	private float _time = 0f;
	private float _visibilityTime = 0f;
	private Vector3 _origin;
	private bool isVisible = true;
	
	void Start () 
	{
		_origin = transform.position;
	}
	
	void Update () 
	{	
		_visibilityTime += Time.deltaTime;
		
		if (_visibilityTime >= (isVisible? visibleDuration:invisibleDuration))
		{
			_visibilityTime = 0;
			
			//fade out
			if (isVisible)
			{
				iTween.FadeTo(this.gameObject, 0f, 1f);
				isVisible = false;
			}
			
			//fade in
			else
			{
				iTween.FadeTo(this.gameObject, 0.7f, 1f);
				isVisible = true;
			}
		}
		
		transform.Rotate(0, 0, (speed / Mathf.PI) * 180);
		transform.position = _origin + new Vector3(Mathf.Cos(_time) * radius, Mathf.Sin(_time) * radius, 0);
		_time += speed;
	}
}
