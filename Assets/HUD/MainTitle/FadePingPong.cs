﻿using UnityEngine;
using System.Collections;

public class FadePingPong : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
		iTween.FadeFrom(gameObject, iTween.Hash("alpha", 0, "time", 1.8f, "looptype", iTween.LoopType.pingPong));
	
	}
}
