﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using KYData;

public class UnitInfo : MonoBehaviour {
	
	[SerializeField]
	private TextMesh name, description, ability;
	
	[SerializeField]
	private SpriteText atk, hp, cost;
	
	[SerializeField]
	private GameObject promptSelectOccluder, sprites;
	
	private List<SkeletonAnimation> skeletonList = new List<SkeletonAnimation>();
	
	private bool isRevealed = false;
	
	// Use this for initialization
	void Start () 
	{
		promptSelectUnit(false);
		
		//collect all sprites into a list
		if (sprites != null)
		{
			foreach(Transform child in sprites.transform)
			{
				SkeletonAnimation s = child.gameObject.GetComponent<SkeletonAnimation>();
				s.state.SetAnimation("standby", true); 
				skeletonList.Add(s);
				s.renderer.enabled = false;
			}
		}
	}
	
	public void updateInfo(TrooperWrapper trooperWrapper)
	{	
		string name_temp = trooperWrapper.name.Split('_')[1].ToUpper();
		
		name.text = Language.Get("UNIT_NAME_" + name_temp.ToUpper());
		
		////////////////////
		///Update unit description
		
		float maxWidth = 2.8f;
		float offsetY = 0.26f;
		float size = 0.09f;
		
#if LANGUAGE_JA
		maxWidth = 1.43f;
		offsetY = 0.33f;
		size = 0.11f;
#endif
		
		string description_temp = Language.Get("UNIT_DESCRIPTION_" + name_temp.ToUpper());
		
		description.characterSize = size;
		description.text = TextUtils.ResolveTextBySize(description_temp, description.characterSize, maxWidth);
		
		///////////////////
		///Update unit ability and offset position based on length of description (above)
		
		string ability_temp = Language.Get ("UNIT_ABILITY_" + name_temp.ToUpper());
		
		//replace format placeholders with appropriate values
		UnitVariableAbilityWrapper variableAbilityData = KYDataManager.instance.getVariableAbilityWrapper(trooperWrapper);
		List<int> variables = variableAbilityData.getVariableByRank((int)(trooperWrapper.level - 1) / 10);
		
		List<object> vars = new List<object>();
		
		for (int i = 0; i < variables.Count; i++)
		{
			vars.Add(variables[i]);
		}
		
		ability_temp = string.Format(ability_temp, vars.ToArray());
		
		ability.characterSize = size;
		ability.text = TextUtils.ResolveTextBySize(ability_temp, ability.characterSize, maxWidth);
		
		Vector3 abilityPos = ability.transform.localPosition;
		abilityPos.y = -TextUtils.GetDisplayLineCount(description.text) * offsetY;
		
		ability.transform.localPosition = abilityPos;
		
		//sprite
		setSprite(trooperWrapper.name.Split('_')[1].ToLower(), ((trooperWrapper.level - 1) / 10) + 1);
		
		//stats
		Fury.Database.Unit unit = Resources.Load(trooperWrapper.name.Split('_')[1].ToLower()) as Fury.Database.Unit;
		
		atk.Text = unit.Weapon == null? "-": (int)(unit.Weapon.Damage*(1f+(trooperWrapper.level-1)*GameSettings.DMG_INCREMENT_PER_LEVEL))+"";
		hp.Text = (int)(unit.Health*(1f+(trooperWrapper.level-1)*GameSettings.HP_INCREMENT_PER_LEVEL))+"";
		cost.Text = trooperWrapper.cost.ToString();
		
		if (!isRevealed)
		{
			hideSelectUnit(true);
		}
	}
	
	public void promptSelectUnit(bool isAnimated)
	{
		if (isAnimated)
		{
			promptSelectOccluder.transform.localScale = new Vector3(1, 0, 1);
			iTween.ScaleTo(promptSelectOccluder, iTween.Hash("y", 1, "time", 0.4f));
		}
		
		else
		{
			promptSelectOccluder.transform.localScale = new Vector3(1, 1, 1);
		}
		
		isRevealed = false;
	}
	
	public void hideSelectUnit(bool isAnimated)
	{
		if (isAnimated)
		{
			promptSelectOccluder.transform.localScale = new Vector3(1, 1, 1);
			iTween.ScaleTo(promptSelectOccluder, iTween.Hash("y", 0, "time", 0.4f));
		}
		
		else
		{
			promptSelectOccluder.transform.localScale = new Vector3(1, 0, 1);
		}
		
		isRevealed = true;
		
	}
	
	void setSprite(string name, int rank)
	{
		foreach(SkeletonAnimation s in skeletonList)
		{
			if(s.gameObject.name.Contains(name.ToLower()))
			{
				s.renderer.enabled = true;
				string n = s.skeletonDataAsset.name;
				int currrank = System.Int32.Parse((n.Split('_'))[1]);
				if(rank != currrank)
				{
					s.skeletonDataAsset = Resources.Load((n.Split('_'))[0]+"_"+rank) as SkeletonDataAsset;
					s.Clear();
					s.Initialize();
				}
				s.state.SetAnimation("standby", true); 
			}
			else
			{
				s.renderer.enabled = false;
			}
		}
	}
	
	KYData.DescriptionWrapper getDescriptionWrapper(int index)
	{
		foreach(KYData.DescriptionWrapper desc in KYDataManager.arrayDescriptionWrapper)
		{
			if (desc.index == index)
			{
				return desc;
			}
		}
		
		return null;
	}
}
