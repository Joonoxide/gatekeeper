using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using UnityEngine;
using LitJson;
using KYData;

public class EquipmentManager : MonoBehaviour 
{
	public class XPWrapper
	{
		public string name;
		public List<int> xps;
		public int index;
		public List<int> portraitIndices;
		public int genre;
		public int unlock;
	}

	public GameObject equipmentButton, equipmentButtonListHolder;
	public List<UIButton> arrayButton = new List<UIButton>();
	public static EquipmentManager instance;
	public KYButton draggingButton;
	public List<TrooperWrapper> arrayTrooperWrapper = new List<TrooperWrapper>();
	public SlotWrapper slotWrapper = new SlotWrapper();
	public SpriteText infoText;
	public KYScroll scrollSelection;
	public KYScroll scrollSlot;
	public List<SkeletonAnimation> sprites;
	public SpriteText textName;
	public SpriteText textLevel;
	public SpriteText textCost;
	public SpriteText textHP;
	public SpriteText textDamage;
	public SpriteText textRuby;
	public SpriteText textDescription;
	public SpriteText textAbility;
	
	public List<GameObject> stars;
	
	public UIScrollList scrollList;
	public List <UIListItemContainer> arrayScrollItems;
	public GameObject huge;
	
	public UIButton buttonBuy;
	public List<XPWrapper> arrayXPWrapper;
	public GameObject frame;
	public ParticleSystem ps;
	
	public ButtonLock btnBookmark;
	public KeyValuePair<TrooperWrapper, ButtonLockMini> activeUnit;

	void Awake()
	{
		instance = this;
	}

    void Start () 
	{
		GA.API.Design.NewEvent(CustomDesignEvent.getNavigationWindow(NavigationType.Barracks));
		
		huge = GameObject.Find("hugepanel");
		arrayXPWrapper = KYDataManager.getInstance().arrayXPWrapper;

		KYDataManager dataManager = KYDataManager.getInstance();
		arrayTrooperWrapper = new List<TrooperWrapper>();

		GameObject newEquipmentButton = null;
		PackedSprite sprite;
		int ii = 0;

		foreach(TrooperWrapper trooperWrapper in dataManager.arrayTrooperWrapper)
		{
			if(trooperWrapper.name.Contains("Harvester"))
			{
				continue;
			}
			arrayTrooperWrapper.Add(trooperWrapper);			
		}
		
		//sort troops
		arrayTrooperWrapper = SortingHelper.sortBy(arrayTrooperWrapper, SortType.Level);
			
		GameObject holder = null;
		GameObject unitButton = null;
		arrayScrollItems = new List<UIListItemContainer>();
		
		foreach(TrooperWrapper trooperWrapper in arrayTrooperWrapper)
		{	
			//newEquipmentButton = Instantiate(equipmentButton, new Vector3(2.3f+ii%2*1.8f, 1.8f+(ii%8)/2*-1.3f, 0), Quaternion.identity) as GameObject;
			//newEquipmentButton.transform.localScale = new Vector3(0.95f, 0.95f, 1);
			
			//newEquipmentButton.transform.parent = huge.transform;
			
			//new row
			if(ii%2 == 0)
			{
				holder = Instantiate(equipmentButtonListHolder) as GameObject;
				holder.name = (ii/2).ToString();
			}
			
			unitButton = Instantiate(equipmentButton) as GameObject;
			unitButton.transform.parent = holder.transform;
			
			//new row
			if(ii%2 == 0)
			{
				unitButton.transform.localPosition = new Vector3(-1.5f, 0, 0);
			}
			else
			{
				unitButton.transform.localPosition = new Vector3(0.7f, 0, 0);
			}
			
			newEquipmentButton = unitButton;
			sprite = newEquipmentButton.GetComponent<PackedSprite>();
			sprite.DoAnim(0);
			sprite.PauseAnim();
			sprite.SetCurFrame(trooperWrapper.indexIcon);
			
			newEquipmentButton.name = "trooper"+(arrayButton.Count+1);
			
			//check if it's bookmarked, then update lock icon accordingly
			ButtonLockMini miniLockIcon = newEquipmentButton.GetComponentInChildren<ButtonLockMini>();
			
			if (trooperWrapper.isBookmarked)
			{
				miniLockIcon.doLock(false);
			}
			
			else
			{
				miniLockIcon.doUnlock(false);
			}
			
			sprite.transform.Find("but").gameObject.GetComponent<UIButton>().scriptWithMethodToInvoke = this;
			sprite.transform.Find("but").gameObject.GetComponent<UIButton>().delay = arrayButton.Count+1;
			sprite.transform.Find("gray").gameObject.GetComponent<UIButton>().scriptWithMethodToInvoke = this;
			sprite.transform.Find("gray").gameObject.GetComponent<UIButton>().delay = arrayButton.Count+1;
			string name = (trooperWrapper.name.Split('_'))[1];
			Fury.Database.Unit unit = Resources.Load(name) as Fury.Database.Unit;
			
			string level = "";
			
			if (CollectionManager.hulala(trooperWrapper.level) < 10)
			{
				level += "0";
			}
			
			level += CollectionManager.hulala(trooperWrapper.level);
			
			newEquipmentButton.transform.Find("cost").gameObject.GetComponent<SpriteText>().Text = level;

			for(int i = (trooperWrapper.level-1)/10+2; i <= 3; ++i)
			{
				sprite.transform.Find("star"+i).gameObject.transform.localScale = Vector3.zero;
			}
			
			holder.GetComponent<UIListItemContainer>().ScanChildren();
			
			if(trooperWrapper.genre == 2)
			{
				newEquipmentButton.gameObject.SetActive(false);
			}
			else
			{
				if(ii%2 == 1)
				{
					scrollList.AddItem(holder);
				}
				
				++ii;
			}
			
			arrayScrollItems.Add(holder.GetComponent<UIListItemContainer>());
			addButton(sprite.transform.Find("but").gameObject.GetComponent<UIButton>());
		}
		
		if(ii%2 == 1)
		{
			scrollList.AddItem(holder);
		}
		
		foreach(Transform child in GameObject.Find("sprites").transform)
		{
			SkeletonAnimation s = child.gameObject.GetComponent<SkeletonAnimation>();
			s.state.SetAnimation("standby", true); 
			sprites.Add(s);
			s.gameObject.renderer.material.shader = Shader.Find("Sprite/Vertex Colored");
			s.gameObject.renderer.material.color = new Color(0.5f, 0.5f, 0.5f, 1f);
			s.renderer.enabled = false;
		}

		if(arrayTrooperWrapper.Count > 0)
		{
			onClickTroop(1);	
		}
		//huge.transform.position = huge.transform.position+new Vector3(0, 15, 0);
		//huge.GetComponent<UIPanel>().StartTransition("Bring in Forward");
		
		slideIn();
		
		reload();
	}
	
	public void slideIn()
	{
		iTween.MoveFrom(huge, iTween.Hash("y", 10, "time", 2f, "easetype", iTween.EaseType.spring));
	}
	
	public void slideOut()
	{
		iTween.MoveTo(huge, iTween.Hash("y", 15, "time", 1f, "easetype", iTween.EaseType.linear));
	}
	
	public int cost;
	public TrooperWrapper currTrooperWrapper;

	public void fast()
	{
		++page;
		reload();
	}

	public void slow()
	{
		--page;
		reload();
	}

	public void onClickTroop(UIButton but)
	{
		onClickTroop((int)but.delay);
	}

	public GameObject pppp;
	public GameObject nnnn;
	int page = 0;
	public void reload()
	{
		return;
		for(int i = 0; i < arrayButton.Count; ++i)
		{
			var but = arrayButton[i];
			if(i/8 == page)
			{
				but.transform.parent.gameObject.SetActive(true);
			}
			else
			{
				but.transform.parent.gameObject.SetActive(false);
			}
		}

		if(page == 0)
		{
			pppp.SetActive(false);
		}
		else
		{
			pppp.SetActive(true);
		}

		if((arrayButton.Count-1)/8 == page)
		{
			nnnn.SetActive(false);
		}
		else
		{
			nnnn.SetActive(true);
		}	
	}

	int iid = -1;

	public void onClickTroop(int identifier)
	{	
		Vector3 slotPos;
		
		//if first time
		if (iid < 0)
		{
			iid = identifier;
			onClickTroop(arrayTrooperWrapper[identifier-1]);
			
			highlightUnit(identifier);
			
			updateActiveUnitDataFromID(identifier);
		}
		
		//do nothing if the same unit was selected
		if (iid == identifier)
			return;

		
		onClickTroop(arrayTrooperWrapper[identifier-1]);
		
		unhighlightUnit(iid);
		highlightUnit(identifier);
		
		updateActiveUnitDataFromID(identifier);
		
		iid = identifier;
	}
	
	void updateActiveUnitDataFromID(int id)
	{
		activeUnit = new KeyValuePair<TrooperWrapper, ButtonLockMini>(arrayTrooperWrapper[id-1], 
				arrayButton[id-1].transform.parent.GetComponentInChildren<ButtonLockMini>());
	}
	
	void highlightUnit(int id)
	{
		PackedSprite sprite = arrayButton[id-1].transform.parent.transform.FindChild("box").GetComponent<PackedSprite>();
		sprite.DoAnim(0);
		sprite.PauseAnim();
		sprite.SetCurFrame(1);
	}
	
	void unhighlightUnit(int id)
	{
		PackedSprite sprite = arrayButton[id-1].transform.parent.transform.FindChild("box").GetComponent<PackedSprite>();
		sprite.DoAnim(0);
		sprite.PauseAnim();
		sprite.SetCurFrame(0);
	}

	public int xpForTrooper(TrooperWrapper trooperWrapper)
	{
		XPWrapper xp = null;
		foreach(XPWrapper xpp in arrayXPWrapper)
		{
			xp = xpp;
			if(xpp.index == trooperWrapper.typeIndex)
			{
				break;
			}
		}
		int totalXP = xp.xps[(trooperWrapper.level-1)%10];
		for(int i = 0; i < (trooperWrapper.level-1)/10; ++i)
		{
			totalXP += xp.xps[9];
		}
		//return xp.xps[(trooperWrapper.level-1)%xp.xps.Count];
		return totalXP;
	}	

	public void onClickTroop(TrooperWrapper trooperWrapper)
	{
		BarracksLanguageManager languageManager = GetComponent<BarracksLanguageManager>();
		languageManager.updateFields(trooperWrapper);
		
		updateLockState(trooperWrapper);
		
		cost = xpForTrooper(trooperWrapper);

		currTrooperWrapper = trooperWrapper;
		foreach(SkeletonAnimation s in sprites)
		{
			if(s.gameObject.name.Contains((trooperWrapper.name.Split('_'))[1].ToLower()))
			{
				s.renderer.enabled = true;
				int rank = (currTrooperWrapper.level-1)/10;
				string n = s.skeletonDataAsset.name;
				++rank;
				s.skeletonDataAsset = Resources.Load((n.Split('_'))[0]+"_"+rank) as SkeletonDataAsset;
				s.Clear();
				s.Initialize();
				s.state.SetAnimation("standby", true); 
				s.gameObject.renderer.material.shader = Shader.Find("Sprite/Vertex Colored");
				s.gameObject.renderer.material.color = new Color(0.5f, 0.5f, 0.5f, 1f);
			}
			else
			{
				s.renderer.enabled = false;
			}
		}

		string name = (trooperWrapper.name.Split('_'))[1];
		Fury.Database.Unit unit = Resources.Load(name) as Fury.Database.Unit;
		/*textName.Text = name.ToUpper();
		
		//scale the background behind the name to fit the length of the name
		Transform bg_name = textName.transform.FindChild("bg_name");
		
		if (bg_name)
		{
			Vector3 bgScale = bg_name.localScale;
			bgScale.x = Mathf.Max(2.5f, textName.TotalWidth + 0.3f); //cap the narrowest at 2.5f (tested in scene)
			
			bg_name.localScale = bgScale;
		}*/
		
		
		textLevel.Text = "Level "+CollectionManager.hulala(trooperWrapper.level);
		//textHP.Text = (int)(unit.Health*(1f+(trooperWrapper.level-1)*0.2f))+"";
		textHP.Text = (int)(unit.Health*(1f+(trooperWrapper.level-1)*GameSettings.HP_INCREMENT_PER_LEVEL))+"";
		
		if (unit.Weapon != null)
		{
			//textDamage.Text = (int)(unit.Weapon.Damage*(1f+(trooperWrapper.level-1)*0.2f))+"";	
			textDamage.Text = (int)(unit.Weapon.Damage*(1f+(trooperWrapper.level-1)*GameSettings.DMG_INCREMENT_PER_LEVEL))+"";	
		}
		
		else
		{
			textDamage.Text = "-";	
		}
		
		//textCost.Text = trooperWrapper.xpCount+"/"+xpForTrooper(trooperWrapper);
		
		textCost.Text = trooperWrapper.cost.ToString();
		textRuby.Text = cost+"";
		
		/*
		//description
		DescriptionWrapper desc = getDescriptionWrapper(trooperWrapper.typeIndex);
		textDescription.Text = desc.unitDescription;
		
		Vector3 abilityPos = textAbility.transform.localPosition;
		abilityPos.y = -textDescription.GetDisplayLineCount() * 0.45f;
		
		textAbility.transform.localPosition = abilityPos;
		textAbility.Text = desc.getAbilityDescription(trooperWrapper.level);*/
		
		//display stars
		int starCount = (trooperWrapper.level-1)/10;
		
		for(int i = 0; i < 3; i++)
		{
			if (i <= starCount)
			{
				stars[i].GetComponent<MeshRenderer>().enabled = true;
			}
			
			else
			{
				stars[i].GetComponent<MeshRenderer>().enabled = false;
			}
		}
		
		/*
		if(trooperWrapper.level == 30)
		{
			buttonBuy.gameObject.SetActive(false);
		}
		else
		{
			buttonBuy.gameObject.SetActive(true);
		}*/
	}

	public TrooperWrapper identifierForTrooperWrapper(int identifier)
	{
		foreach(TrooperWrapper trooperWrapper in arrayTrooperWrapper)
		{
			if(trooperWrapper.identifier == identifier)
			{
				return trooperWrapper;
			}
		}
		return null;
	}
	
	public DescriptionWrapper getDescriptionWrapper(int index)
	{
		foreach(DescriptionWrapper desc in KYDataManager.arrayDescriptionWrapper)
		{
			if (desc.index == index)
			{
				return desc;
			}
		}
		
		return null;
	}

	public void addButton(UIButton button)
	{
		arrayButton.Add(button);
	}

	public void setFrame(GameObject first, GameObject second)
	{
		first.GetComponent<PackedSprite>().SetCurFrame(second.GetComponent<PackedSprite>().GetCurAnim().GetCurPosition());
	}

	public void onClickBack()
	{
		slideOut();
		GameController.LoadLevel("title", TransitionType.Open);
	}

	void OnDestroy()
	{
		KYDataManager dataManager = KYDataManager.getInstance();
		//dataManager.arrayTrooperWrapper = arrayTrooperWrapper;
		//dataManager.slotWrapper = slotWrapper;
	}

	public void onClickPurchase()
	{
		if(currTrooperWrapper == null) return;
		if(KYDataManager.getInstance().dataWrapper.xpCount < cost)
		{
			spawn();
			return;
		}
		if(KYDataManager.getInstance().dataWrapper.xpCount >= cost)
		{
			ps.gameObject.SetActive(true);
			ps.Play();
			KYDataManager.getInstance().dataWrapper.xpCount -= cost;
			++currTrooperWrapper.level;
			onClickTroop(currTrooperWrapper);
			GameObject go = GameObject.Find("trooper"+iid);
			go.transform.Find("cost").gameObject.GetComponent<SpriteText>().Text = "LV "+CollectionManager.hulala(currTrooperWrapper.level);
			for(int i = 1; i <= 3; ++i)
			{
				go.transform.Find("star"+i).gameObject.transform.localScale = new Vector3(0.36f, 0.36f, 1);
			}
			for(int i = (currTrooperWrapper.level-1)/10+2; i <= 3; ++i)
			{
				go.transform.Find("star"+i).gameObject.transform.localScale = Vector3.zero;
			}
		}
	}

	public UIPanel panelMore;
	public void onClickCancel()
	{
		FragmentManager.instance.comein();	
		maincam.instance.overlaycam_above.enabled = true;	
		panelMore.StartTransition("Dismiss Forward");		
	}

	public void spawn()
	{
		panelMore.StartTransition("Bring in Forward");
	}

	public void onClickIAP()
	{
		panelMore.StartTransition("Dismiss Forward");
	}
	
	public void onClickGoToFusion()
	{
		slideOut();
		GameController.LoadLevel("collection", TransitionType.NoChange);
	}
	
	public void onToggleLock()
	{
		btnBookmark.toggle();
		
		activeUnit.Key.isBookmarked = btnBookmark.isLocked;
		
		if (activeUnit.Key.isBookmarked)
		{
			activeUnit.Value.doLock(true);
		}
		
		else
		{
			activeUnit.Value.doUnlock(true);
		}
		
		StartCoroutine(updateScrollList());
	}
	
	IEnumerator updateScrollList()
	{
		yield return new WaitForSeconds(0.3f);
		
		scrollList.RepositionItems();
		
		yield break;
	}
	
	void updateLockState(TrooperWrapper unit)
	{
		if (unit.isBookmarked)
		{
			btnBookmark.doLock();
		}
		
		else
		{
			btnBookmark.doUnlock();
		}
	}
}
