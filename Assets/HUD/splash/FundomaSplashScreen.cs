using UnityEngine;
using System.Collections;

public class FundomaSplashScreen : MonoBehaviour {

	public GameObject fading;
		
	// Use this for initialization
	void Start () 
	{	
#if LANGUAGE_EN
		GA.SettingsGA.SetKeys("a118303c4b811d958a79a98a970d2bdc", "8bd58ad7a0f3fbe537874afe1104105fc78cd074");
#elif LANGUAGE_JA
		GA.SettingsGA.SetKeys("2a13f66b8869715abad5569813059e71", "4edb202eb72d22c2c1f64d163aa3dfe66ec6d852");
#endif
		
		fading = Camera.main.gameObject;
		StartCoroutine(GameController.execute2(1, delegate()
		{
			fading.SendMessage ("fadeOut");
			StartCoroutine(GameController.execute2(1, delegate()
			{
				Application.LoadLevel ("title2");
			}));
		}));
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
