using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using UnityEngine;
using LitJson;
using KYData;

public class UpgradeManager : MonoBehaviour 
{
	List<UpgradeWrapper> arrayUpgradeData = new List<UpgradeWrapper>();
	UpgradeWrapper currUpgradeData = null;
	public UIStateToggleBtn iconUpgrade;
	public SpriteText textDescription;
	public SpriteText textName;
	public SpriteText textCost;
	public UIButton buttonBuy;
	int[] arrayBah = new int[]{ 1, 3, 2, 0 };
	public GameObject pan;
	public List<UIStateToggleBtn> arrayButton;
	public GameObject frame;
	public ParticleSystem ps;
	
	public GameObject infoTable, backWindow;
	public scissor clipper;
	
	// Use this for initialization
	void Start () 
	{
		GA.API.Design.NewEvent(CustomDesignEvent.getNavigationWindow(NavigationType.Upgrade));
		
		arrayButton = new List<UIStateToggleBtn>();
		TextAsset data = Resources.Load("upgrade_json") as TextAsset;
		string strUpgradeData = data.text;
		arrayUpgradeData = JsonMapper.ToObject<List<UpgradeWrapper>>(strUpgradeData);
		
		slideIn();
	}
	
	public void slideIn()
	{
		frame.SetActive(false);
		iTween.MoveFrom(backWindow, iTween.Hash("y", 10, "time", 2f, "easetype", iTween.EaseType.spring, "oncompletetarget", gameObject, "oncomplete", "populateWindow"));
	}
	
	public void slideOut()
	{
		frame.SetActive(false);
		clipper.h = 1f;
		iTween.MoveTo(backWindow, iTween.Hash("y", 10, "time", 0.7f, "easetype", iTween.EaseType.linear));
	}
	
	void populateWindow()
	{
		GameObject iconUpgradeGO = Resources.Load("icon_upgradeinfo") as GameObject;
		GameObject newIconUpgrade = null;
		UIStateToggleBtn buttonUpgrade;
		Vector3 pos;
		//KYDataManager dataManager = KYDataManager.getInstance();
		pan = Resources.Load("bigpanel") as GameObject;
		GameObject newpan;
		
		for(int i = 0; i < arrayUpgradeData.Count; ++i)
		{
			newpan = GameObject.Instantiate(pan) as GameObject;
			pos.x = -2.36f+(arrayUpgradeData[i].index-1)*1.6f;
			pos.y = 0;
			pos.z = -1;
			newpan.transform.position = pos;
			newpan.transform.parent = backWindow.transform.FindChild("container_skills");
			for(int j = 0; j < 5; ++j)
			{
				newIconUpgrade = Instantiate(iconUpgradeGO, Vector3.zero, Quaternion.identity) as GameObject;
				newIconUpgrade.transform.parent = newpan.transform.Find("panel1");
				pos.x = 0;
				pos.y = -3f+j*1.2f;
				pos.z = -2;
				newIconUpgrade.transform.localPosition = pos;
				buttonUpgrade = newIconUpgrade.GetComponent<UIStateToggleBtn>();	
				buttonUpgrade.scriptWithMethodToInvoke = this;
				buttonUpgrade.methodToInvoke = "onClickUpgrade";
				buttonUpgrade.delay = (i*5+j+1);
				buttonUpgrade.isPass = true;
				//buttonUpgrade.transform.GetChild(0).gameObject.GetComponent<BoxCollider>().enabled = false;

				arrayButton.Add(buttonUpgrade);
				//newIconUpgrade.transform.Find("KYText_level").gameObject.GetComponent<SpriteText>().Text = j+"";
			}
			GameObject panpan = newpan;
			
			panpan.SetActive(false);
			StartCoroutine(GameController.execute2((arrayUpgradeData[i].index-1)*0.5f, delegate()
			{
				panpan.SetActive(true);
				panpan.transform.Find("panel1").GetComponent<UIPanel>().StartTransition("Bring in Forward");
			}));
				
		}
		
		iTween.MoveTo(infoTable, iTween.Hash("x", 3.5, "time", 1, "delay", 4.1f));
		iTween.MoveTo(backWindow, iTween.Hash("x", -2, "time", 1, "delay", 3.5f));

		buttonBuy.controlIsEnabled = false;	
		refresh(true);		
	}
	
	public void refresh(bool bugoi = false, bool sugoi = false)
	{
		int k = 0;
		UIStateToggleBtn buttonUpgrade;
		for(int i = 0; i < arrayUpgradeData.Count; ++i)
		{
			int level = KYDataManager.getInstance().upgradeNameForLevel(arrayUpgradeData[i].name);
			for(int j = 0; j < 5; ++j)
			{
				buttonUpgrade = arrayButton[k];
				if(j <= level)
				{
					buttonUpgrade.SetToggleState("State "+(arrayBah[i]*5+j+1));	
					if(j == level)
					{
						buttonUpgrade.transform.Find("frame").gameObject.GetComponent<UIStateToggleBtn>().SetToggleState("State 1");
						buttonUpgrade.transform.Find("frame").gameObject.GetComponent<UIStateToggleBtn>().transform.localScale = new Vector3(1, 1, 1);
						if((bugoi || sugoi) && arrayUpgradeData[i].index == 1)
						{
							UIStateToggleBtn btn = buttonUpgrade;
							if(bugoi)
							{
								StartCoroutine(GameController.execute2(arrayUpgradeData.Count*0.5f+2.3f, delegate()
								{
									onClickUpgrade(btn);
								}));
							}
							else
							{
								onClickUpgrade(btn);
							}
							bugoi = false;
							sugoi = false;
						}
					}
					else 
					{
						buttonUpgrade.transform.Find("frame").gameObject.GetComponent<UIStateToggleBtn>().SetToggleState("State 3");
						buttonUpgrade.transform.Find("frame").gameObject.GetComponent<UIStateToggleBtn>().transform.localScale = new Vector3(1, 1, 1);
					}
				}
				else
				{
					buttonUpgrade.transform.Find("frame").gameObject.GetComponent<UIStateToggleBtn>().SetToggleState("State 2");
					buttonUpgrade.transform.Find("frame").gameObject.GetComponent<UIStateToggleBtn>().transform.localScale = new Vector3(1, 1, 1);
					buttonUpgrade.SetToggleState("State "+(arrayBah[i]*5+j+1+20));	
				}
				++k;
			}			
		}

	}
	Vector3 pos = Vector3.zero;


	public void onClickUpgrade(UIStateToggleBtn btn)
	{
		onClickUpgrade((int)btn.delay);
		frame.SetActive(true);
		pos = btn.transform.position;
		pos.z = -10;
		frame.transform.position = pos;
	}

	public void onClickUpgrade(int index)
	{
		UpgradeLanguageManager languageManager = GetComponent<UpgradeLanguageManager>();
		
		int level = ((index-1)%5)+1;
		int i = (index-1)/5;
		UpgradeWrapper wrapper = arrayUpgradeData[i];
		if(KYDataManager.getInstance().upgradeNameForLevel(wrapper.name)+1 == level)
		{
			buttonBuy.controlIsEnabled = true;
			if(KYDataManager.getInstance().getRemainingStarCount() >= wrapper.cost*level)
			{
				buttonBuy.controlIsEnabled = true;
			}
			else
			{
				buttonBuy.controlIsEnabled = false;
			}
			textCost.transform.parent.gameObject.SetActive(true);
		}
		else
		{
			buttonBuy.controlIsEnabled = false;		
			textCost.transform.parent.gameObject.SetActive(false);			
		}
		iconUpgrade.gameObject.SetActive(true);
		iconUpgrade.SetToggleState("State "+(arrayBah[i]*5+level));
		currUpgradeData = wrapper;
		textDescription.Text = currUpgradeData.description;
		textName.Text = currUpgradeData.name+" (Lv "+level+")";
		textCost.Text = (currUpgradeData.cost*level)+"";
		
		languageManager.updateFields(wrapper, level);

		//int level = KYDataManager.getInstance().upgradeNameForLevel(currUpgradeData.name);
		//	iconUpgrade.transform.Find("KYText_level").gameObject.GetComponent<SpriteText>().Text = level+"";
	}

	public void onClickPurchase()
	{
		if(currUpgradeData != null)
		{
			int level = KYDataManager.getInstance().upgradeNameForLevel(currUpgradeData.name)+1;
			if(KYDataManager.getInstance().getRemainingStarCount() >= currUpgradeData.cost*level)
			{
				KYDataManager.getInstance().addUpgrade(currUpgradeData);
				KYDataManager.getInstance().dataWrapper.usedStar += currUpgradeData.cost*level;
				buttonBuy.controlIsEnabled = false;
				textCost.transform.parent.gameObject.SetActive(false);	
				GameObject newGO = GameObject.Instantiate(ps.gameObject) as GameObject;
				newGO.transform.position = pos+new Vector3(0, 1.2f, -20);
				newGO.AddComponent<CFX_AutoDestructShuriken>();
				newGO.GetComponent<ParticleSystem>().Play();
				refresh();
				//GameController.LoadLevel("upgrade");			
			}
			else
			{
				//panelMoreStar.StartTransition("Bring in Forward");
			}
		}
	}

	public void onClickReset()
	{
		KYDataManager.getInstance().arrayUpgradeWrapper = new List<UpgradeWrapper>();
		KYDataManager.getInstance().dataWrapper.usedStar = 0;
		refresh(false, true);
	}

	void onClickBack()
	{
		slideOut();
		GameController.LoadLevel("title", TransitionType.Open);
		currUpgradeData = null;
	}
}
