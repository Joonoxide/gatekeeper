﻿using UnityEngine;
using System.Collections;

public class ButtonLockMini : MonoBehaviour {

	public void doLock(bool isAnimated)
	{
		if (isAnimated)
		{
			iTween.ScaleTo(gameObject, iTween.Hash("x", 1, "y", 1, "time", 0.4f));
		}
		
		else
		{
			transform.localScale = new Vector3(1, 1, 1);
		}
	}
	
	public void doUnlock(bool isAnimated)
	{
		if (isAnimated)
		{
			iTween.ScaleTo(gameObject, iTween.Hash("x", 0, "y", 0, "time", 0.4f));
		}
		
		else
		{
			transform.localScale = new Vector3(0, 0, 0);
		}
	}
}
