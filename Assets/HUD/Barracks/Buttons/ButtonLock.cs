﻿using UnityEngine;
using System.Collections;

public class ButtonLock : MonoBehaviour {

	public GameObject state_on, state_off;
	private bool _isLocked = false;
	
	public void doLock()
	{
		state_on.SetActive(true);
		state_off.SetActive(false);
		
		_isLocked = true;
	}
	
	public void doUnlock()
	{
		state_on.SetActive(false);
		state_off.SetActive(true);
		
		_isLocked = false;
	}
	
	public void toggle()
	{
		if (_isLocked)
		{
			doUnlock();	
		}
		
		else
		{
			doLock();
		}
	}
	
	public bool isLocked
	{
		get
		{
			return _isLocked;
		}
	}
}
