﻿using UnityEngine;
using System.Collections;

public class RunningProgressBar : MonoBehaviour {
	
	private UIProgressBar progressBar;
	
	private float _oVal = 0f, _tVal = 0f;
	private int _cLvl = 0, _tLvl = 0;
	private int _iteration = 0;
	
	private float stepSize = 0.01f;
	
	public System.Action onFinish;
	public System.Action<int> onLevelUp;
	public System.Action<float, int> update;
	
	// Use this for initialization
	void Start () 
	{
		progressBar = GetComponentInChildren<UIProgressBar>();
	}
	
	public float Value
	{
		get
		{
			return progressBar.Value;
		}
		
		set
		{
			progressBar.Value = value;
		}
	}
	
	public void doFillFromTo(float fromVal, float toVal, int fromLevel, int toLevel)
	{
		//store the values internally
		_oVal = fromVal;
		_tVal = toVal;
		_cLvl = fromLevel;
		_tLvl = toLevel;
		
		//cap at minimum of 0
		_iteration = Mathf.Max(0, _tLvl - _cLvl);
		
		//set the val to the beginning
		progressBar.Value = _oVal;
		
		//begin fill animation
		InvokeRepeating("fill", 0, 1.0f / 30.0f);
	}
	
	public void skipFill()
	{
		//stop fill immediately
		StopFill();
		
		//set the progress bar to the final value
		progressBar.Value = _tVal;
		
		//fire onLevelUp event for target level
		onLevelUp(_tLvl);
		
		//fire onFinish event to end level up sequence
		onFinish();
	}
	
	void fill()
	{
		//still ends to be filled to the end
		if (_iteration > 0)
		{
			//cap the maximum value at 1
			progressBar.Value = Mathf.Min(1, progressBar.Value + stepSize);
			
			//if it has filled up completely
			if (progressBar.Value >= 1)
			{
				//reset value to 0
				progressBar.Value = 0;
				
				//reduce iteration count
				_iteration--;
				
				//fire onLevelUp event for next level
				onLevelUp(++_cLvl);
			}
		}
		
		//if it is the final iteration (i.e. does not fill up all the way to the end)
		else
		{
			//cap the maximum value at target value
			progressBar.Value = Mathf.Min(_tVal, progressBar.Value + stepSize);
			
			//if it has reached the target value
			if (progressBar.Value >= _tVal)
			{
				//fire onFinish event to end level up sequence
				onFinish();
				StopFill();
			}
		}
	}
	
	private void StopFill()
	{
		CancelInvoke("fill");
	}
}
