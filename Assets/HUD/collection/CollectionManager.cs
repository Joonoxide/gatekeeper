using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using UnityEngine;
using LitJson;
using KYData;
using Hud = Fury.Hud;

public class CollectionManager : MonoBehaviour 
{
	public GameObject equipmentButton;
	public GameObject equipmentButton2;
	public List<UIButton> arrayButton = new List<UIButton>();
	public List<TrooperWrapper> arrayTrooperWrapper = new List<TrooperWrapper>();
	public UIPanel panel;
	public GameObject frame;
	public UIButton buttonUpgrade;
	public UIPanel panelDone;
	private int page = 0;
	public GameObject prev;
	public GameObject next;
	public SpriteText txt;
	public InputWrapper inputWrapper;
	public Vector3 ssize;
	public SkeletonAnimation sprite;
	public static CollectionManager instance;
	public expbar1 exp;
	
	public RunningProgressBar progressBar;
	
	public TextMesh text_type, text_cost, text_result;
	public GameObject expBar;
	
	public GameObject window;
	
	private bool isInteracting = false;

	void Start () 
	{
		GA.API.Design.NewEvent(CustomDesignEvent.getNavigationWindow(NavigationType.Fusion));
		
		instance = this;
		inputWrapper = new InputWrapper();

		s2();
		panel.transform.localScale = Vector3.zero;
		frame.SetActive(false);
		panelDone.transform.localScale = Vector3.zero;
		reload();
		ssize = slots[0].gameObject.GetComponent<Collider>().bounds.size;
		scroll.ss = ss;
		scroll.dd = dd;
		scroll.ee = ee;

		exp = expBar.GetComponent<expbar1>();
		five = GameObject.Find("five");
		unclickfive();
		
		slideIn();
	}
	
	public void slideIn()
	{
		iTween.MoveFrom(window, iTween.Hash("y", 10, "time", 2f, "easetype", iTween.EaseType.spring));
	}
	
	public void slideOut()
	{
		clearAllSlots();
		iTween.MoveTo(window, iTween.Hash("y", 15, "time", 1f, "easetype", iTween.EaseType.linear));
	}
	
	void clearAllSlots()
	{
		unclickfive();
		
		for(int i = 0; i < 6; ++i)
		{
			if(slotted[i] != null)
			{
				Destroy(slotted[i].transform.parent.gameObject);
			}
		}
	}

	GameObject five;

	public UIButton ss()
	{
		return (currIndex == -1)? null : arrayButton[currIndex-1];
	}

	public void s2()
	{
		GameObject newEquipmentButton = null;
		PackedSprite sprite;
		int ii = 0;

		KYDataManager dataManager = KYDataManager.getInstance();

		arrayTrooperWrapper = new List<TrooperWrapper>();

		foreach(TrooperWrapper trooperWrapper in dataManager.arrayTrooperWrapper)
		{
			if(!trooperWrapper.name.Contains("vester"))
			{
				arrayTrooperWrapper.Add(trooperWrapper);
			}
		}
		GameObject newg = null;
		GameObject newg2 = null;
		
		//sort troops
		arrayTrooperWrapper = SortingHelper.sortBy(arrayTrooperWrapper, SortType.Type);
		
		foreach(TrooperWrapper trooperWrapper in arrayTrooperWrapper)
		{
			//GameObject newg = Instantiate(equipmentButton, new Vector3(-5f+ii%4*1.8f, 2f+ii/4*-1.3f, -5), Quaternion.identity) as GameObject;
			if(ii%2 == 0)
			{
				newg = Instantiate(equipmentButton) as GameObject;
				newg.name = ii/2+"";
			}
			newg2 = Instantiate(equipmentButton2) as GameObject;
			newg2.transform.parent = newg.transform;

			if(ii%2 == 0)
			{
				newg2.name = "EquipmentButton3";
				newg2.transform.localPosition = new Vector3(-1.5f, 0, 0);
			}
			else
			{
				newg2.name = "EquipmentButton4";
				newg2.transform.localPosition = new Vector3(0.7f, 0, 0);
			}

			newEquipmentButton = newg2;
			
			//check if it's bookmarked, then update lock icon accordingly
			ButtonLockMini miniLockIcon = newEquipmentButton.GetComponentInChildren<ButtonLockMini>();
			
			if (trooperWrapper.isBookmarked)
			{
				miniLockIcon.doLock(false);
			}
			
			else
			{
				miniLockIcon.doUnlock(false);
			}
			
			sprite = newEquipmentButton.GetComponent<PackedSprite>();
			sprite.DoAnim(0);
			sprite.PauseAnim();
			sprite.SetCurFrame(trooperWrapper.indexIcon);
			sprite.transform.Find("but").gameObject.GetComponent<UIButton>().scriptWithMethodToInvoke = this;
			sprite.transform.Find("but").gameObject.GetComponent<UIButton>().delay = arrayButton.Count+1;
			sprite.transform.Find("gray").gameObject.GetComponent<UIButton>().scriptWithMethodToInvoke = this;
			sprite.transform.Find("gray").gameObject.GetComponent<UIButton>().delay = arrayButton.Count+1;
			if(slotted[5] != null)
			{
				if(ct.identifier == trooperWrapper.identifier)
				{
					sprite.gameObject.renderer.material.color = new Vector4(0.1f, 0.1f, 0.1f, 1f);
					slotted[5].delay = arrayButton.Count+1;
				}
			}
			newEquipmentButton.name = trooperWrapper.name;
			string name = (trooperWrapper.name.Split('_'))[1];
			Fury.Database.Unit unit = Resources.Load(name) as Fury.Database.Unit;
			
			string level = "";
			
			if (CollectionManager.hulala(trooperWrapper.level) < 10)
			{
				level += "0";
			}
			
			level += CollectionManager.hulala(trooperWrapper.level);
			
			newEquipmentButton.transform.Find("cost").gameObject.GetComponent<SpriteText>().Text = level;
			
			/*newEquipmentButton.transform.Find("cost").gameObject.GetComponent<SpriteText>().Text = "LV "+((trooperWrapper.level)%10);
			if(trooperWrapper.level%10 == 0)
			{
				newEquipmentButton.transform.Find("cost").gameObject.GetComponent<SpriteText>().Text = "LV 10";
			}
			*/
			for(int i = (trooperWrapper.level-1)/10+2; i <= 3; ++i)
			{
				sprite.transform.Find("star"+i).gameObject.transform.localScale = Vector3.zero;
			}
			newg.GetComponent<UIListItemContainer>().ScanChildren();

			if(trooperWrapper.genre == 2)
			{
				newEquipmentButton.gameObject.SetActive(false);
			}
			else
			{
				if(ii%2 == 1)
				{
					scroll.AddItem(newg);
				}

				++ii;
			}

			addButton(sprite.transform.Find("but").gameObject.GetComponent<UIButton>());
		}	
		if(ii%2 == 1)
		{
			scroll.AddItem(newg);
		}
	}

	public void dd()
	{
		alo = false;
		unloop(zombie.transform.parent.gameObject);
	}

	public void ee()
	{
		Destroy(zombie.transform.parent.gameObject);
		currIndex = -1;
		alo = false;
	}

	public UIScrollList scroll;
	bool dropping = false;

	public void onClickTroop(UIButton but)
	{
		if(isBlock)
		{
			return;
		}
		if(!(Mathf.Abs(but.transform.parent.renderer.material.color.r-0.5f) < 0.1f))
		{
			return;
		}
		onClickTroop((int)but.delay);
	}

	Vector3 startdelta = Vector3.zero;
	public void onClickTroop(int index)
	{
		if(currIndex != -1)
		{
			return;
		}
		Vector3 pos;

		//scroll.touchScroll = true;
		Ray ray;
		for(int i = 0; i < slotted.Length; ++i)
		{
			if(slotted[i] != null && slotted[i].delay == index)
			{
				UIButton nz = slotted[i];
				//frame.SetActive(true);
				pos = nz.transform.parent.position;
				pos.z -= 10;
				pos.x += 0.25f;
				frame.transform.position = pos;	
				zombie = nz;
				dropping = true;
				inputWrapper.update();
				ray = Camera.main.ScreenPointToRay(inputWrapper.screenPos);
				Hud.CalculateFocus(ray);
				//Hud.FocusPoint -= new Vector3(0, 0, Hud.FocusPoint.y);
				os = Hud.FocusPoint-nz.transform.parent.position;
				os.z = 0;
				currIndex = index;

				loop(zombie.transform.parent.gameObject);
				unloop(zombie.transform.parent.gameObject);
				//Debug.Log(zombie.transform.gameObject.GetComponent<MeshRenderer>().enabled+":"+Network.time);
				return;
			}
		}
		alo = true;
		dropping = false;
		currIndex = index;
		//frame.SetActive(true);
		pos = arrayButton[index-1].transform.parent.position;
		pos.z = -10;
		pos.x += 0.25f;
		frame.transform.position = pos;	
		origin = arrayButton[index-1].transform.parent.position;

		inputWrapper.update();
		ray = Camera.main.ScreenPointToRay(inputWrapper.screenPos);
		Hud.CalculateFocus(ray);
		//Hud.FocusPoint -= new Vector3(0, 0, Hud.FocusPoint.y);
		os = Hud.FocusPoint-arrayButton[index-1].transform.parent.position;
		os.z = 0;

		GameObject newGO = Instantiate(equipmentButton2) as GameObject;
		newGO.transform.position = arrayButton[index-1].transform.parent.position;
		
		TrooperWrapper trooperWrapper = arrayTrooperWrapper[index-1];
		PackedSprite sprite = newGO.GetComponent<PackedSprite>();
		sprite.DoAnim(0);
		sprite.PauseAnim();
		sprite.SetCurFrame(trooperWrapper.indexIcon);
		sprite.transform.Find("but").gameObject.GetComponent<UIButton>().scriptWithMethodToInvoke = this;
		sprite.transform.Find("but").gameObject.GetComponent<UIButton>().delay = index;
		sprite.transform.Find("gray").gameObject.GetComponent<UIButton>().scriptWithMethodToInvoke = this;
		sprite.transform.Find("gray").gameObject.GetComponent<UIButton>().delay = index;
		newGO.name = trooperWrapper.name;
		
		//check if it's bookmarked, then update lock icon accordingly
		ButtonLockMini miniLockIcon = newGO.GetComponentInChildren<ButtonLockMini>();
		
		if (trooperWrapper.isBookmarked)
		{
			miniLockIcon.doLock(false);
		}
		
		else
		{
			miniLockIcon.doUnlock(false);
		}
		
		/*
		string name = (trooperWrapper.name.Split('_'))[1];
		Fury.Database.Unit unit = Resources.Load(name) as Fury.Database.Unit;
		*/
		
		string level = "";
			
		if (CollectionManager.hulala(trooperWrapper.level) < 10)
		{
			level += "0";
		}
		
		level += CollectionManager.hulala(trooperWrapper.level);
		
		newGO.transform.Find("cost").gameObject.GetComponent<SpriteText>().Text = level;
		
		/*newGO.transform.Find("cost").gameObject.GetComponent<SpriteText>().Text = "LV "+((trooperWrapper.level)%10);
		if(trooperWrapper.level%10 == 0)
		{
			newGO.transform.Find("cost").gameObject.GetComponent<SpriteText>().Text = "LV 10";			
		}*/
		for(int i = (trooperWrapper.level-1)/10+2; i <= 3; ++i)
		{
			sprite.transform.Find("star"+i).gameObject.transform.localScale = Vector3.zero;
		}

		UIButton temp = arrayButton[index-1];
		zombie = temp;
		arrayButton[index-1] = newGO.transform.Find("but").gameObject.GetComponent<UIButton>();
		
		zombie.transform.parent.gameObject.name = "zombie";
		sprite = zombie.transform.parent.Find("box").gameObject.GetComponent<PackedSprite>();
		sprite.RevertToStatic();

		int y = 0;
		for(y = 0; y < scroll.items.Count; ++y)
		{
			bool isBreak = false;
			foreach(var child in scroll.items[y].transform)
			{
				if(child == zombie.transform.parent)
				{
					isBreak = true;
					break;
				}
			}
			if(isBreak)
			{
				break;
			}
		}
		scroll.items[y].gameObject.GetComponent<UIListItemContainer>().RemoveChild(zombie.transform.parent.gameObject);
		zombie.transform.parent.parent = null;
		scroll.items[y].gameObject.GetComponent<UIListItemContainer>().MakeChild(newGO);
		scroll.items[y].gameObject.GetComponent<UIListItemContainer>().ScanChildren();
		loop3(zombie.transform.parent.gameObject);
		getSprite((int)zombie.delay-1);
		/*
		scroll.RemoveItem(y, false);
		scroll.InsertItem(newg.GetComponent<UIListItemContainer>(), y);
		GameObject t2 = zombie.transform.parent.parent.gameObject;
		zombie.transform.parent.parent = null;
		Destroy(t2);
		*/
	}

	UIButton zombie;
	public Vector3 origin;
	public GameObject[] slots;
	public UIButton[] slotted = new UIButton[6];
	public Vector3 os;
	public Vector3 prevv;
	public bool alo;
	Vector3 ppp;
	public void Update()
	{
		string str = "";
		for(int i = 0; i < 6; ++i)
		{
			if(slotted[i] != null)
			{
				str += slotted[i]+":";
			}
		}
		if(zombie != null)
		{
			inputWrapper.update();
			UIButton but = zombie;

			if(alo)
			{
				if(but.m_ctrlState == UIButton.CONTROL_STATE.OVER || but.m_ctrlState == UIButton.CONTROL_STATE.NORMAL)
				{
					ee();
				}
				else
				{
					loop(but.transform.parent.gameObject);
					MeshRenderer renderer = but.transform.parent.gameObject.GetComponent<MeshRenderer>();
					if(renderer != null)
					{
						renderer.enabled = false;
					}
	   				Ray ray = Camera.main.ScreenPointToRay(inputWrapper.screenPos);
					Hud.CalculateFocus(ray);
					//Hud.FocusPoint -= new Vector3(0, 0, Hud.FocusPoint.y);
					Hud.FocusPoint = new Vector3(Hud.FocusPoint.x-os.x, Hud.FocusPoint.y-os.y, origin.z-25);
					but.transform.parent.position = Hud.FocusPoint;		
				}		
			}
			else if(but.m_ctrlState == UIButton.CONTROL_STATE.ACTIVE && inputWrapper.isPress)
			{
   				Ray ray = Camera.main.ScreenPointToRay(inputWrapper.screenPos);
				Hud.CalculateFocus(ray);
				//Hud.FocusPoint -= new Vector3(0, 0, Hud.FocusPoint.y);
				if(Hud.FocusPoint == Vector3.zero)
				{
					Hud.FocusPoint = ppp;
				}
				else
				{
					Hud.FocusPoint = new Vector3(Hud.FocusPoint.x-os.x, Hud.FocusPoint.y-os.y, origin.z-25);
					ppp = Hud.FocusPoint;
				}
				but.transform.parent.position = Hud.FocusPoint;
				Vector3 pos = but.transform.parent.position;
				pos.z = -35;
				pos.x += 0.25f;
				frame.transform.position = pos;

				for(int i = 0; i < slots.Length; ++i)
				{
					Bounds b = slots[i].GetComponent<Collider>().bounds;
					b.size += new Vector3(0, 0, 100);
					if(slotted[i] == null && b.Intersects(but.gameObject.GetComponent<Collider>().bounds))
					{
						but.transform.parent.transform.localScale = new Vector3(1f, 1f, 1f);
						but.gameObject.GetComponent<BoxCollider>().size = ssize*1.2f;
						frame.transform.localScale = new Vector3(1f, 1, 1f);
						break;
					}
					else
					{
						but.transform.parent.transform.localScale = new Vector3(1.2f, 1.2f, 1f);
						but.gameObject.GetComponent<BoxCollider>().size = ssize;
						frame.transform.localScale = new Vector3(1.2f, 1.2f, 1f);
					}
				}

			}
			else if(but.m_ctrlState == UIButton.CONTROL_STATE.NORMAL || but.m_ctrlState == UIButton.CONTROL_STATE.OVER || !inputWrapper.isPress)
			{
				if(true)
				{
					bool isBreak = false;
					int i;
					bool blek = false;
					bool ssd = false;
					bool ss = true;

					if(!slots[0].activeSelf && arrayTrooperWrapper[currIndex-1].level != arrayTrooperWrapper[(int)slotted[5].delay-1].level)
					{
						ssd = true;
						ss = true;
					}
					string nn = "none";
					for(int yy = 0; yy < slotted.Length; ++yy)
					{
						if(slotted[yy] != null)
						{
							nn = arrayTrooperWrapper[(int)slotted[yy].delay-1].name;
							
							break;
						}
					}
 					blek = arrayTrooperWrapper[currIndex-1].name == nn | nn == "none";
					for(i = 0; i < slots.Length; ++i)
					{
						Bounds b = slots[i].GetComponent<Collider>().bounds;
						b.size += new Vector3(0, 0, 100);

						int kkk = -1;
						if(dropping)
						{
							for(int lkk = 0; lkk < 6; ++lkk)
							{
								if(slotted[lkk] == but)
								{
									kkk = lkk;
									break;
								}
							}							
						}



						if((slotted[i] == null || i == kkk) && b.Intersects(but.gameObject.GetComponent<Collider>().bounds) && slots[i].activeSelf && ss)
						{
							if(!dropping)
							{
								but.transform.parent.transform.localScale = new Vector3(1.2f, 1.2f, 1.2f);
								but.gameObject.GetComponent<BoxCollider>().size = but.gameObject.GetComponent<BoxCollider>().bounds.size*0.8f;
								frame.transform.localScale = new Vector3(1.2f, 1.2f, 1.2f);
								slotted[i] = but;
							}
							else
							{

							}
							isBreak = true;

							break;
						}
					}
					if(isBreak && !dropping)
					{
						if(blek && !ssd)
						{
							if(i == 5)
							{
								unloop(zombie.transform.parent.gameObject, true);
								clickfive();
							}
							else
							{
								but.transform.parent.transform.position = slots[i].transform.position+new Vector3(-0.3f, 0, -5);
								loop2(but.transform.parent.gameObject);
								unloop(zombie.transform.parent.gameObject, false);
							}
							arrayButton[currIndex-1].transform.parent.gameObject.renderer.material.color = new Vector4(0.1f, 0.1f, 0.1f, 1f);
							but.Container = null;
						}
						else
						{
							float maxWidth = 2f;
									
						#if LANGUAGE_JA
							maxWidth = 1f;
						#endif
							
							TextMesh tf_error = panelDone.transform.Find("text_error").gameObject.GetComponent<TextMesh>();
							
							if(!blek)
							{
								tf_error.text = Language.Get(ErrorLanguageField.ERROR_WRONG_UNIT_TYPE.ToString());//"Select another unit of the same type!";
								panelDone.StartTransition("Bring in Forward");	
							}	
							else
							{
								tf_error.text = Language.Get(ErrorLanguageField.ERROR_WRONG_UNIT_LEVEL.ToString());//"Select another Level 10 unit!";
								panelDone.StartTransition("Bring in Forward");									
							}
							
							
							tf_error.text = TextUtils.ResolveTextBySize(tf_error.text, tf_error.characterSize, maxWidth);
							
							Destroy(but.transform.parent.gameObject);					
						}
					}
					else if(!dropping)
					{
						Destroy(but.transform.parent.gameObject);
					}
					else if(isBreak && dropping)
					{
						int k = 0;
						for(k = 0; k < 6; ++k)
						{
							if(slotted[k] == but)
							{
								break;
							}
						}
						if(k == 5 && i != 5)
						{
							slotted[i] = but;
							slotted[k] = null;
							MeshRenderer renderer = slotted[i].transform.parent.gameObject.GetComponent<MeshRenderer>();
							if(renderer != null)
							{
								renderer.enabled = true;
							}		
							loop2(but.transform.parent.gameObject);	
							unloop(zombie.transform.parent.gameObject, false);	
							unclickfive();
						}
						else if(k != 5)
						{
							if(i == 5)
							{
								slotted[i] = but;
								slotted[k] = null;
								clickfive();
							}
							else
							{
								unloop(zombie.transform.parent.gameObject, false);
								loop2(but.transform.parent.gameObject);	
							}
						}
						but.Container = null;
					}
					else if(dropping)
					{
						int k = 0;
						for(k = 0; k < 6; ++k)
						{
							if(slotted[k] == but)
							{
								break;
							}
						}
						if(k == 5)
						{
							unclickfive();
						}
						DestroyImmediate(but.transform.parent.gameObject);
						arrayButton[currIndex-1].transform.parent.gameObject.renderer.material.color = new Vector4(0.5f, 0.5f, 0.5f, 1f);
					}
					rearrange();
					currIndex = -1;
					frame.SetActive(false);
					zombie = null;
					//scroll.touchScroll = true;
				}
			}
		}
	}

	public void getSprite(int iiii)
	{
		string na = arrayTrooperWrapper[iiii].name;
		string n = "sprite_"+((na.Split('_'))[1].ToLower());
		GameObject newGO = GameObject.Instantiate(Resources.Load(n) as GameObject) as GameObject;
		newGO.transform.localScale = newGO.transform.localScale*0.2f;
		newGO.transform.position = new Vector3(-4.2f, 0.3f, -36);
		int rank = (arrayTrooperWrapper[currIndex-1].level-1)/10+1;
		sprite = newGO.GetComponent<SkeletonAnimation>();
		n = sprite.skeletonDataAsset.name;
		sprite.skeletonDataAsset = Resources.Load((n.Split('_'))[0]+"_"+rank) as SkeletonDataAsset;
		sprite.Clear();
		sprite.Initialize();
		sprite.state.SetAnimation("standby", true); 
		sprite.transform.parent = zombie.transform.parent;
		
		
		if (na.Contains("Elementalist"))
		{
			sprite.transform.localPosition = new Vector3(0.4f, -0.4f, -1);
		}
		
		else
		{
			sprite.transform.localPosition = new Vector3(0.3f, -0.5f, -1);
		}
	}

	public void loop(GameObject go)
	{
		MeshRenderer renderer = go.GetComponent<MeshRenderer>();
		if(renderer != null)
		{
			renderer.enabled = false;
		}
		foreach(Transform child in go.transform)
		{
			loop(child.gameObject);
		}
	}

	public void unloop(GameObject go, bool ss = true)
	{
		if(go.name.Contains("sprite"))
		{
			go.GetComponent<MeshRenderer>().enabled = ss;
		}
		foreach(Transform child in go.transform)
		{
			unloop(child.gameObject, ss);
		}
	}

	public GameObject getSprite2(GameObject go)
	{
		if(go.name.Contains("sprite"))
		{
			return go;
		}
		foreach(Transform child in go.transform)
		{
			if(child.gameObject.name.Contains("sprite"))
			{
				return child.gameObject;
			}
		}
		return null;
	}

	public void loop2(GameObject go)
	{
		if(go.name.Contains("sprite"))
		{
		}
		else
		{
			MeshRenderer renderer = go.GetComponent<MeshRenderer>();
			if(renderer != null)
			{
				renderer.enabled = true;
			}
		}
		foreach(Transform child in go.transform)
		{
			loop2(child.gameObject);
		}
	}

	public void loop3(GameObject go)
	{
		if(go.GetComponent<AutoSpriteControlBase>())
		{
			go.GetComponent<AutoSpriteControlBase>().Unclip();
		}
		else if(go.GetComponent<AutoSpriteBase>())
		{
			go.GetComponent<AutoSpriteBase>().Unclip();
		}
		else if(go.GetComponent<SpriteText>())
		{
			go.GetComponent<SpriteText>().Unclip();
		}
		foreach(Transform child in go.transform)
		{
			loop3(child.gameObject);
		}
	}

	public static int hulala(int lev)
	{
		if(lev%10 == 0)
		{
			return 10;
		}
		else return lev%10;
	}

	public void clickfive(bool aaa = false)
	{	
		FusionLanguageManager languageManager = GetComponent<FusionLanguageManager>();
		
		TrooperWrapper trooperWrapper = arrayTrooperWrapper[(int)slotted[5].delay-1];
		
		GameObject textName = five.transform.Find("text_name").gameObject;
		
		textName.SetActive(true);
		
		languageManager.updateFields(trooperWrapper);
		
		five.transform.Find("text_level").gameObject.GetComponent<SpriteText>().Text = "Level "+(hulala(trooperWrapper.level))+"";
		
		for(int i = 1; i < (trooperWrapper.level-1)/10+2; ++i)
		{
			five.transform.Find("star"+i).gameObject.GetComponent<MeshRenderer>().enabled = true;
		}
		float ratio = (float)trooperWrapper.xpCount/(float)KYDataManager.getInstance().xpForTrooper(trooperWrapper);
		progressBar.Value = ratio;
		//expBar.renderer.material.SetFloat("_Cutoff", Mathf.Clamp(1-ratio, 0.02f, 1f));
		//text_exp.Text = trooperWrapper.xpCount+"/"+KYDataManager.getInstance().xpForTrooper(trooperWrapper);

		int type = 0;
		if(trooperWrapper.level == 30)
		{
			type = 2;
		}
		if(trooperWrapper.level%10 == 0)
		{
			type = 1;
		}
		if(type == 2)
		{
			slots[0].SetActive(false);
			slots[1].SetActive(false);
			slots[2].SetActive(false);
			slots[3].SetActive(false);
			slots[4].SetActive(false);
		}
		else if(type == 1)
		{
			//text_type.Text = "EVOLUTION";
			UIButton but;
			if(!aaa)
			{
				for(int i = 0; i < 5; ++i)
				{
					but = slotted[i];
					if(but != null)
					{
						DestroyImmediate(but.transform.parent.gameObject);
						arrayButton[(int)but.delay-1].transform.parent.gameObject.renderer.material.color = new Vector4(0.5f, 0.5f, 0.5f, 1f);
					}
				}
			}
			slots[0].SetActive(false);
			slots[2].SetActive(false);
			slots[3].SetActive(false);
			slots[4].SetActive(false);
		}
		else
		{
			//text_type.Text = "FUSION";		
			for(int i = 0; i < 5; ++i)
			{
				slots[i].SetActive(true);
			}	
		}
	}

	public void unclickfive()
	{
		FusionLanguageManager languageManager = GetComponent<FusionLanguageManager>();
		
		languageManager.clearFields();
		
		//five.transform.Find("text_name").gameObject.GetComponent<SpriteText>().Text = "";
		five.transform.Find("text_level").gameObject.GetComponent<SpriteText>().Text = "";
		//five.transform.Find("text_name").gameObject.SetActive(false);
		
		for(int i = 1; i < 4; ++i)
		{
			five.transform.Find("star"+i).gameObject.GetComponent<MeshRenderer>().enabled = false;
		}
		//expBar.renderer.material.SetFloat("_Cutoff", 1);
		progressBar.Value = 0;
		
		//text_exp.text = "";
		text_result.text = "";
		text_cost.text = "";
		text_type.text = "";			
		for(int i = 0; i < 5; ++i)
		{
			slots[i].SetActive(true);
		}
		//Debug.Log(Network.time);
		buttonUpgrade.controlIsEnabled = false;
	}

	bool isBlock = false;
	public void fuse()
	{
		if(KYDataManager.getInstance().dataWrapper.xpCount < System.Int32.Parse(text_cost.text))
		{
			spawn();
		}
		else
		{
			GA.API.Design.NewEvent(CustomDesignEvent.getFeatureUsage(FeatureUsageType.Fusion));
			
			KYDataManager.getInstance().dataWrapper.xpCount -= System.Int32.Parse(text_cost.text);
			isBlock = true;
			buttonUpgrade.controlIsEnabled = false;
			StartCoroutine(animate());
		}
	}

	public IEnumerator animate(bool aaa = false)
	{
		Vector3[] dest = new Vector3[5];
		for(int i = 0; i < 5; ++i)
		{
			if(slotted[i] != null)
			{
				dest[i] = slotted[5].transform.parent.position;
				dest[i].z += 5;
				loop(slotted[i].transform.parent.gameObject);
				unloop(slotted[i].transform.parent.gameObject);
			}
		}

		while(true)
		{
			bool isBreak = true;
			for(int i = 0; i < 5; ++i)
			{
				if(slotted[i] != null)
				{
					GameObject go = slotted[i].transform.parent.gameObject;
					Vector3 pos = go.transform.position+(dest[i]-go.transform.position)*Time.deltaTime*2;
					go.transform.position = pos;
					if((go.transform.position-dest[i]).magnitude > 1f)
					{
						isBreak = false;
					}
				}
			}			


			if(!isBreak)
			{
				yield return null;
			}
			else
			{
				break;
			}		

		}

		int count = 0;
		for(int i = 0; i < 5; ++i)
		{
			if(slotted[i] != null)
			{
				GameObject go = slotted[i].transform.parent.gameObject;
				go.transform.position = dest[i];
				count += arrayTrooperWrapper[(int)slotted[i].delay-1].totalxpCount;
			}
		}
		yield return null;

		TrooperWrapper trooperWrapper = arrayTrooperWrapper[(int)slotted[5].delay-1];
		float ratio_before = 0f, ratio_after = 0;
		int level_before = 0, level_after = 0;
			
		ratio_before =(float)trooperWrapper.xpCount/(float)KYDataManager.getInstance().xpForTrooper(trooperWrapper);
		level_before = trooperWrapper.level;
		
		KYDataManager.getInstance().addTrooperXP(trooperWrapper, count);
		
		ratio_after =(float)trooperWrapper.xpCount/(float)KYDataManager.getInstance().xpForTrooper(trooperWrapper);
		level_after = trooperWrapper.level;
		
		progressBar.onLevelUp = onLevelup;
		progressBar.onFinish = onFinish;
		
		progressBar.doFillFromTo
			(
				ratio_before,
				ratio_after,
				level_before,
				level_after
			);
			
		
		/*exp.curr = (float)trooperWrapper.xpCount/(float)KYDataManager.getInstance().xpForTrooper(trooperWrapper);
		exp.cl = trooperWrapper.level;
		KYDataManager.getInstance().addTrooperXP(trooperWrapper, count);
		exp.target = (float)trooperWrapper.xpCount/(float)KYDataManager.getInstance().xpForTrooper(trooperWrapper);
		exp.tl = trooperWrapper.level;
		exp.onLevelup = onLevelup;
		exp.update = update;
		exp.onFinish = onFinish;
		exp.start();*/

		for(int i = 0; i < 5; ++i)
		{
			if(slotted[i] != null)
			{
				slotted[i].transform.parent.gameObject.SetActive(false);
			}
		}
	}

	public ParticleSystem ps;
	public void onLevelup(int l)
	{
		TrooperWrapper trooperWrapper = arrayTrooperWrapper[(int)slotted[5].delay-1];
		ps.Play();
		five.transform.Find("text_level").gameObject.GetComponent<SpriteText>().Text = "Level "+hulala(l)+"";
		//text_exp.text = 0+"/"+KYDataManager.getInstance().xpForTrooper(trooperWrapper, l);
		slotted[5].transform.parent.Find("cost").gameObject.GetComponent<SpriteText>().Text = "LV "+((trooperWrapper.level-1)%10);
		if(trooperWrapper.level%10 == 0)
		{
			slotted[5].transform.parent.Find("cost").gameObject.GetComponent<SpriteText>().Text = "LV 10";
		}
		for(int i = 1; i <= (trooperWrapper.level-1)/10+1; ++i)
		{
			slotted[5].transform.parent.Find("star"+i).gameObject.transform.localScale = new Vector3(0.36f, 0.36f, 1);
		}
		for(int i = 1; i < 4; ++i)
		{
			five.transform.Find("star"+i).gameObject.GetComponent<MeshRenderer>().enabled = false;
		}
	}

	public void update(float x, int l)
	{
		TrooperWrapper trooperWrapper = arrayTrooperWrapper[(int)slotted[5].delay-1];
		int deno = (int)(KYDataManager.getInstance().xpForTrooper(trooperWrapper, l)*x);
		if(l == trooperWrapper.level)
		{
			if(deno >= trooperWrapper.xpCount)
			{
				deno = trooperWrapper.xpCount;
			}
		}
		//text_exp.text = (int)(deno)+"/"+KYDataManager.getInstance().xpForTrooper(trooperWrapper, l);
	}

	TrooperWrapper ct;
	public void onFinish()
	{
		TrooperWrapper trooperWrapper = arrayTrooperWrapper[(int)slotted[5].delay-1];
		//text_exp.text = trooperWrapper.xpCount+"/"+KYDataManager.getInstance().xpForTrooper(trooperWrapper);
		text_result.text = "Level "+hulala(trooperWrapper.level);
		text_cost.text = "";

		TrooperWrapper[] troopers = new TrooperWrapper[6];
		for(int i = 0; i < 5; ++i)
		{
			if(slotted[i] != null)
			{
				troopers[i] = arrayTrooperWrapper[(int)slotted[i].delay-1];
				Destroy(slotted[i].transform.parent.gameObject);
			}
		}
		for(int i = 0; i < 5; ++i)
		{
			KYDataManager.getInstance().arrayTrooperWrapper.Remove(troopers[i]);
		}

		arrayButton.Clear();
		scroll.ClearList(true);
		ct = arrayTrooperWrapper[(int)slotted[5].delay-1];
		s2();

		isBlock = false;
		
		clickfive(true);

		int rank = (trooperWrapper.level-1)/10+1;
		GameObject ggg = slotted[5].transform.parent.gameObject;
		foreach(Transform child in ggg.transform)
		{
			if(child.gameObject.GetComponent<SkeletonAnimation>())
			{
				sprite = child.gameObject.GetComponent<SkeletonAnimation>();
			}
		}
		string n = sprite.skeletonDataAsset.name;
		sprite.skeletonDataAsset = Resources.Load((n.Split('_'))[0]+"_"+rank) as SkeletonDataAsset;
		sprite.Clear();
		sprite.Initialize();
		sprite.state.SetAnimation("standby", true); 
		
		string unitName = (trooperWrapper.name.Split('_'))[1];
		Fury.Database.Unit unit = Resources.Load(unitName) as Fury.Database.Unit;
		
		trooperWrapper.cost = Mathf.RoundToInt(unit.cost * (1 + (0.5f * (int)((trooperWrapper.level - 1)/10))));
	}

	public void rearrange()
	{
		for(int i = 0; i < slotted.Length-1; ++i)
		{
			if(slotted[i] == null && slots[i].activeSelf)
			{
				for(int j = i+1; j < slotted.Length-1; ++j)
				{
					if(slotted[j] != null && slots[j].activeSelf)
					{
						slotted[i] = slotted[j];
						slotted[j] = null;
						break;
					}
				}
			}
		}

		for(int i = 0; i < slotted.Length; ++i)
		{
			if(slotted[i] != null && slots[i].activeSelf)
			{
				if(i != 5)
				{
					slotted[i].transform.parent.localScale = new Vector3(1, 1, 1);
				}
				else
				{
					slotted[i].transform.parent.localScale = new Vector3(1.5f, 1.5f, 1);					
				}
				slotted[i].gameObject.GetComponent<BoxCollider>().size = ssize;
				if(i == 5)
				{
					slotted[i].transform.parent.transform.position = slots[i].transform.position+new Vector3(-0.4f, 0, -5);
				}
				else
				{
					slotted[i].transform.parent.transform.position = slots[i].transform.position+new Vector3(-0.3f, 0, -5);
				}
			}
		}

		bool b = false;
		if(slotted[5] != null)
		{
			int count = 0;
			for(int i = 0; i < 5; ++i)
			{
				if(slotted[i] != null && (arrayTrooperWrapper[(int)slotted[5].delay - 1].name == arrayTrooperWrapper[(int)slotted[i].delay - 1].name))
				{
					count += arrayTrooperWrapper[(int)slotted[i].delay-1].totalxpCount;
					b = true;
				} 
			}
			if(arrayTrooperWrapper[(int)slotted[5].delay-1].level%10 != 0)
			{
				text_result.text = "Level "+KYDataManager.getInstance().fakeaddTrooperXP(arrayTrooperWrapper[(int)slotted[5].delay-1], count)+"";
			}
			else if(slotted[1] != null)
			{
				text_result.text = Language.Get(FusionLanguageField.POWERUP_TYPE_EVOLUTION.ToString());				
			}
			else
			{
				text_result.text = "Level "+hulala(arrayTrooperWrapper[(int)slotted[5].delay-1].level);							
			}
			text_cost.text = count/10+"";
		}
		if(b)
		{
			Debug.Log ("fusion enabled!");
			buttonUpgrade.controlIsEnabled = true;
		}
		else
		{
			buttonUpgrade.controlIsEnabled = false;
		}
	}

	int pagemax = 12;
	public void reload()
	{
		/*
		for(int i = 0; i < arrayButton.Count; ++i)
		{
			UIButton but = arrayButton[i];
			//but.transform.parent.position = new Vector3(-5f+(i%pagemax)%4*1.8f, -0.4f+(i%pagemax)/4*-1.3f, -5);
			but.delay = i+1;
			if(i/pagemax == page)
			{
				but.transform.parent.gameObject.SetActive(true);
			}
			else
			{
				but.transform.parent.gameObject.SetActive(false);
			}
		}

		if(page == 0)
		{
			prev.SetActive(false);
		}
		else
		{
			prev.SetActive(true);
		}

		if((arrayButton.Count-1)/pagemax == page)
		{
			next.SetActive(false);
		}
		else
		{
			next.SetActive(true);
		}
		*/
	}

	public void sacrifice(int i)
	{
		Destroy(arrayButton[i].transform.parent.gameObject);
		arrayButton.Remove(arrayButton[i]);
		KYDataManager.getInstance().arrayTrooperWrapper.Remove(arrayTrooperWrapper[i]);
		arrayTrooperWrapper.Remove(arrayTrooperWrapper[i]);
	}

	public UIPanel panelMore;

	public void onClickSacrificeAll()
	{
		int len = arrayTrooperWrapper.Count;
		for(int i = 0; i < len; ++i)
		{
			sacrifice(0);
		}
		KYDataManager.getInstance().dataWrapper.xpCount += len*100;
		string l = (len*100)+"";
		panelDone.transform.Find("text").gameObject.GetComponent<SpriteText>().Text = "You have gained "+l+" golds";
		panelDone.StartTransition("Bring in Forward");
		reload();
	}

	public void onClickCancel3()
	{
		FragmentManager.instance.comein();	
		maincam.instance.overlaycam_above.enabled = true;	
		panelMore.StartTransition("Dismiss Forward");		
	}

	public void spawn()
	{
		panelMore.StartTransition("Bring in Forward");
	}

	public void onClickIAP()
	{
		panelMore.StartTransition("Dismiss Forward");
	}

	public void onClickSacrifice()
	{
		sacrifice(currIndex-1);
		KYDataManager.getInstance().dataWrapper.xpCount += 100;
		onClickCancel();
		panelDone.transform.Find("text").gameObject.GetComponent<SpriteText>().Text = "You have gained 100 golds";
		panelDone.StartTransition("Bring in Forward");
		reload();
	}

	public void onClickUpgrade()
	{
		int l = currTrooperWrapper.level;
		string n = (currTrooperWrapper.name.Split('_'))[1];

		if(l == 20)
		{
			panelDone.transform.Find("text").gameObject.GetComponent<SpriteText>().Text = n+" has reached the maximum level";	
			panelDone.StartTransition("Bring in Forward");		
			onClickCancel();
			return;		
		}

		if(l == 0)
		{
			panelDone.transform.Find("text").gameObject.GetComponent<SpriteText>().Text = "You have unlocked "+n;
		}
		else
		{
			panelDone.transform.Find("text").gameObject.GetComponent<SpriteText>().Text = n+" level has been increased from "+l+" to "+(l+1);
		}
		++currTrooperWrapper.level;
		panelDone.StartTransition("Bring in Forward");		
		sacrifice(currIndex-1);
		onClickCancel();
		reload();
	}

	public int currIndex = -1;
	public TrooperWrapper currTrooperWrapper;

	/*
	public void onClickTroop(int index)
	{
		if(currIndex == -1)
		{
			panel.StartTransition("Bring in Forward");
		}
		currIndex = index;
		frame.SetActive(true);
		Vector3 pos = arrayButton[index-1].transform.parent.position;
		pos.z = -10;
		pos.x += 0.3f;
		frame.transform.position = pos;

		foreach(TrooperWrapper t in KYDataManager.getInstance().arrayTrooperWrapper)
		{
			if(t.name == arrayTrooperWrapper[index-1].name)
			{
				currTrooperWrapper = t;
				break;
			}
		}
		if(currTrooperWrapper.level == 0)
		{
			buttonUpgrade.transform.Find("text1").gameObject.GetComponent<SpriteText>().Text = "UNLOCK";
			txt.Text = "Unlock this character or sacrifice it to golds?";
		}
		else
		{
			buttonUpgrade.transform.Find("text1").gameObject.GetComponent<SpriteText>().Text = "UPGRADE";	
			txt.Text = "Upgrade this character or sacrifice it to golds?";
		}
	}
	*/

	public void onClickCancel()
	{
		frame.SetActive(false);
		currIndex = -1;
		currTrooperWrapper = null;
		panel.StartTransition("Dismiss Forward");
	}

	public void addButton(UIButton button)
	{
		arrayButton.Add(button);
	}

	public void onClickBack()
	{
		if (isBlock)
		{
			return;
		}
		
		slideOut();
		//GameController.LoadLevel("gotcha", 1);
		GameController.LoadLevel("title", TransitionType.Open);
	}

	public void onClickGotcha()
	{
		GameController.LoadLevel("gotcha", TransitionType.NoChange);
	}

	public void onClickPrev()
	{
		--page;
		reload();
	}

	public void onClickNext()
	{
		++page;
		reload();
	}

	public void onClickCancel2()
	{
		panelDone.StartTransition("Dismiss Forward");
	}

	public void onClickGem()
	{
		FragmentManager.instance.comein();	
		maincam.instance.overlaycam_above.enabled = true;
	}
}
