// FadeInOut
//
//--------------------------------------------------------------------
//                        Public parameters
//--------------------------------------------------------------------
 
public var fadeInColour: Color;
public var fadeOutColour: Color;
public var fadeSpeed = 0.3;
private var draw : boolean = true;
 
var drawDepth = -1000;
 
//--------------------------------------------------------------------
//                       Private variables
//--------------------------------------------------------------------
 
private var alpha = 1.0; 
 
private var fadeDir = -1;

private var fadeInTexture: Texture2D;
private var fadeOutTexture: Texture2D;
 
//--------------------------------------------------------------------
//                       Runtime functions
//--------------------------------------------------------------------
 
//--------------------------------------------------------------------
 
function OnGUI()
{
	if(draw)
	{
		if(fadeDir == -1 && alpha >= 0 || fadeDir == 1 && alpha <= 1)
		{
			alpha += fadeDir * fadeSpeed * Time.deltaTime;	
			alpha = Mathf.Clamp01(alpha);	
		 
			GUI.color.a = alpha;
		 
			GUI.depth = drawDepth;
		 
			GUI.DrawTexture(Rect(0, 0, Screen.width, Screen.height), fadeDir < 0? fadeInTexture: fadeOutTexture);
			
			
		}
	}
}
 
//--------------------------------------------------------------------
 
function fadeIn(){
	fadeDir = -1;	
}
 
//--------------------------------------------------------------------
 
function fadeOut(){
	fadeDir = 1;	
}

function ClearScreenFade()
{
	draw = false;
}
 
function Start(){
	//alpha=1;
	
	fadeInTexture = new Texture2D(1, 1);
	fadeInTexture.SetPixel(1, 1, fadeInColour);
	fadeInTexture.Apply();
	fadeIn();
	
	fadeOutTexture = new Texture2D(1, 1);
    fadeOutTexture.SetPixel(1, 1, fadeOutColour);
    fadeOutTexture.Apply();
}