﻿using UnityEngine;
using System.Collections;
using KYData;
using LitJson;

public class HintManager : MonoBehaviour {
	
	public UIPanel character, backPanel;
	
	//HintWrapper hintContainer;
	bool isHidden = true;
	
	void Awake()
	{
		DontDestroyOnLoad(gameObject);
		
		//TextAsset dat = Resources.Load("hints_json") as TextAsset;
		
		//hintContainer = JsonMapper.ToObject<HintWrapper>(dat.text);
	}
	
	void generateRandomHint()
	{
		//textHint.Text = hintContainer.getRandomHint();
		GetComponent<HintLanguageManager>().generateRandomHint();
	}
	
	public void show(float delay)
	{
		if (!isHidden)
		{
			return;
		}
		
		isHidden = false;
		
		StartCoroutine(beginFlyIn(delay));
	}
	
	public void hide(float delay)
	{
		if (isHidden)
		{
			return;
		}
		
		isHidden = true;
		
		StartCoroutine(beginFlyOut(delay));
	}
	
	IEnumerator beginFlyIn(float delay)
	{
		yield return new WaitForSeconds(delay);
		
		flyIn();
		
		yield break;
	}
	
	IEnumerator beginFlyOut(float delay)
	{
		yield return new WaitForSeconds(delay);
		
		flyOut();
		
		yield break;
	}
	
	void flyIn()
	{
		generateRandomHint();
		
		character.StartTransition(UIPanelManager.SHOW_MODE.BringInForward);
		backPanel.StartTransition(UIPanelManager.SHOW_MODE.BringInForward);
		
		/*
		//move character in from the right side
		iTween.MoveFrom(character, iTween.Hash("x", 500, "time", 0.7f, "easetype", iTween.EaseType.easeInOutBack));
		iTween.PunchRotation(character, iTween.Hash ("z", 7, "time", 1.2f, "delay", 0.5f));
		
		//move the back panel in from the left
		iTween.FadeFrom(backPanel, 0f, 1f);
		iTween.MoveFrom(backPanel, iTween.Hash("x", -500, "time", 0.7f, "easetype", iTween.EaseType.easeInOutBack));*/
	}
	
	void flyOut()
	{		
		character.StartTransition(UIPanelManager.SHOW_MODE.DismissForward);
		backPanel.StartTransition(UIPanelManager.SHOW_MODE.DismissForward);
		
		/*
		//move character in from the right side
		iTween.MoveTo(character, iTween.Hash("x", 500, "time", 0.7f, "easetype", iTween.EaseType.easeInOutBack));
		
		//move the back panel in from the left
		iTween.FadeTo(backPanel, 0f, 1f);
		iTween.MoveTo(backPanel, iTween.Hash("x", -500, "time", 0.7f, "easetype", iTween.EaseType.easeInOutBack));*/
	}
	
	IEnumerator testTransition()
	{
		flyIn();
		
		yield return new WaitForSeconds(4f);
		
		flyOut();
		
		yield break;
	}
}
