using UnityEngine;
using System.Collections;

public class fakedata : MonoBehaviour {

	void Awake()
	{
		DontDestroyOnLoad(gameObject);
		if(MapResource.instance == null 
			&& !Application.loadedLevelName.Contains("debug")
			&& !Application.loadedLevelName.Contains("Result"))
		{
			GameObject.Instantiate(Resources.Load("mapresource") as GameObject);
		}
	}

	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
	}

	void OnApplicationQuit()
	{
		KYDataManager.getInstance().save();
		//FBScoreSystem.FBScoreSystem.UnInit();
	}
	
	void OnApplicationPause()
	{
		KYDataManager.getInstance().saves();
		//FBScoreSystem.FBScoreSystem.UnInit();
	}

	void OnLevelWasLoaded()
	{	
		if(MapResource.instance == null 
			&& !Application.loadedLevelName.Contains("debug")
			&& !Application.loadedLevelName.Contains("Result"))
		{
			GameObject.Instantiate(Resources.Load("mapresource") as GameObject);
		}	
	}
}
