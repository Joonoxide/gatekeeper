using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FusingManager : MonoBehaviour 
{
	public static List<int> arrayIndex;
	public static int initLevel;
	public static int afterLevel;
	public List<Transform> mover;
	public int bah = 0;
	public float elapsed;

	void Start () 
	{
		elapsed = 5;
		GameObject equipmentButton = Resources.Load("EquipmentButton") as GameObject;
		Vector3[] pos = { Vector3.zero, new Vector3(0, 80, 0), new Vector3(-65, -40, 0), new Vector3(-72, 40, 0), new Vector3(65, -40, 0), new Vector3(72, 40, 0) };

		if(arrayIndex == null)
		{
			arrayIndex = new List<int>();
			return;
		}

		mover = new List<Transform>();
		for(int i = 0; i < arrayIndex.Count; ++i)
		{
			GameObject newEquipmentButton = GameObject.Instantiate(equipmentButton, pos[i], Quaternion.identity) as GameObject;
			newEquipmentButton.transform.Find("frame").gameObject.SetActive(false);
			if(i == 0)
			{
				newEquipmentButton.transform.Find("LevelText").gameObject.GetComponent<SpriteText>().Text = "Level "+initLevel;
			}
			else
			{
				newEquipmentButton.transform.Find("LevelText").gameObject.SetActive(false);
			}
			PackedSprite sprite = newEquipmentButton.GetComponent<PackedSprite>();
			sprite.DoAnim(0);
			sprite.PauseAnim();
			sprite.SetCurFrame(arrayIndex[i]);
			mover.Add(newEquipmentButton.transform);
		}
		mover[0].localScale = new Vector3(2, 2, 2);
	}
	
	void Update () 
	{
		if(bah == 0)
		{
			for(int i = 1; i < mover.Count; ++i)
			{
				Vector3 dist = mover[i].position-mover[0].position;
				if(dist.magnitude < 1)
				{
					bah = 1;
				}
				Vector3 pos = mover[i].position;
				pos -= dist.normalized*0.5f;
				mover[i].position = pos;
			}
		}
		else if(bah == 1)
		{
			for(int i = 1; i < mover.Count; ++i)
			{
				mover[i].gameObject.SetActive(false);
			}
			mover[0].Find("LevelText").gameObject.GetComponent<SpriteText>().Text = "Level "+afterLevel;
			++bah;
		}
	}

	public void onClickBack()
	{
		GameController.LoadLevel("fusion", TransitionType.NoChange);	
	}
}
