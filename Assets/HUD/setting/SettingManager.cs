﻿using UnityEngine;
using System.Collections;

public class SettingManager : MonoBehaviour 
{
	UIStateToggleBtn btnBGM;
	UIStateToggleBtn btnSFX;
	
	public GameObject panel;

	public void onClickBack()
	{
		slideOut();
		GameController.LoadLevel("title", TransitionType.Open);
	}

	public void Start()
	{
		KYDataManager.getInstance();
		btnBGM = GameObject.Find("toggle_bgm").GetComponent<UIStateToggleBtn>();
		btnBGM.scriptWithMethodToInvoke = this;
		btnSFX = GameObject.Find("toggle_sfx").GetComponent<UIStateToggleBtn>();
		btnSFX.scriptWithMethodToInvoke = this;
		string music = PlayerPrefs.GetString("IsMusic", "null");
		if(music == "no")
		{
			btnBGM.SetToggleState(1);
		}
		else
		{
			btnBGM.SetToggleState(0);
		}
		string sound = PlayerPrefs.GetString("IsSound", "null");
		if(sound == "no")
		{
			btnSFX.SetToggleState(1);
		}
		else
		{
			btnSFX.SetToggleState(0);
		}

		sfx.getInstance();
		
		slideIn();
	}
	
	public void slideIn()
	{
		iTween.MoveFrom(panel, iTween.Hash("y", 10, "time", 2f, "easetype", iTween.EaseType.spring));
	}
	
	public void slideOut()
	{
		iTween.MoveTo(panel, iTween.Hash("y", 15, "time", 1f, "easetype", iTween.EaseType.linear));
	}

	public void ToggleBGM()
	{
		if(btnBGM.StateNum == 1)
		{
			PlayerPrefs.SetString("IsMusic", "yes");
			btnBGM.SetToggleState(0);
			sfx.instance.onMusic(1);
		}
		else
		{
			PlayerPrefs.SetString("IsMusic", "no");
			btnBGM.SetToggleState(1);
			sfx.instance.onMusic(0f);
		}
	}

	public void ToggleSFX()
	{
		if(btnSFX.StateNum == 1)
		{
			PlayerPrefs.SetString("IsSound", "yes");
			btnSFX.SetToggleState(0);
			sfx.instance.onSound(1f);
		}
		else
		{
			PlayerPrefs.SetString("IsSound", "no");
			btnSFX.SetToggleState(1);
			sfx.instance.onSound(0f);
		}	
	}

	public void onClickDelete()
	{
		PlayerPrefs.DeleteAll();
		
		KYDataManager.getInstance().initialize();
		
		slideOut();
		GameController.LoadLevel("title", TransitionType.Open);
	}

	public void onClickOpen()
	{
		for(int i = 1; i < TitleController.maxLevel; ++i)
		{
			Debug.Log (i);
			KYData.LevelWrapper levelWrapper = KYDataManager.getInstance().levelNumForWrapper(i);
			if(levelWrapper == null || levelWrapper.star == 0)
			{
				KYDataManager.getInstance().addLevel(i, 1);
				TitleController.isFlag = true;
				//StartCoroutine(lateStart());
				break;
			}
		}	
	}

}
