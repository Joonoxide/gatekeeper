﻿using UnityEngine;
using System.Collections;
using KYData;

public class RecruitManager : MonoBehaviour {
	
	private const int NECROMANCER = 1;
	
	// Use this for initialization
	void Start () {
	
		KYDataManager dataManager = KYDataManager.getInstance();
		
		foreach(TrooperWrapper trooperWrapper in dataManager.arrayTrooperWrapper)
		{
			Debug.Log (trooperWrapper.name);		
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void onClickPurchase(int index)
	{
		string identifier;
		
		if (index == NECROMANCER)
		{
			identifier = "Call_Necromancer";
			
		}
		
		else
		{
			//do nothing atm
			return;
		}
		
		KYDataManager.getInstance().addTrooper(identifier, false, 1);
	}
}
