﻿using UnityEngine;
using System.Collections;

public enum ShopPurchaseType
{
	FragmentXP,
	Harvester
}

public class ShopHeaderIconContainer : MonoBehaviour {

	public GameObject icon_fragment, icon_harvester;
	
	public void updateIcon(ShopPurchaseType type)
	{
		if (type.Equals(ShopPurchaseType.FragmentXP))
		{
			icon_fragment.SetActive(true);
			icon_harvester.SetActive(false);
		}
		
		else
		{
			icon_fragment.SetActive(false);
			icon_harvester.SetActive(true);
		}
	}
}
