using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FragmentManager : MonoBehaviour 
{
	public static FragmentManager instance;
	public UIPanel panel;
	public GameObject ccc;
	public List<UIListItemContainer> children = new List<UIListItemContainer>();
	public UIScrollList scroll;
	public Camera fragmentCam;
	
	public ShopHeaderIconContainer shopHeaderContainer;
	
	[SerializeField]
	private TextMesh spend_5_frag, spend_10_frag;
	
	private string frag_plural, get_frag;
	
	private ShopPurchaseType activePurchaseType;

	public void Start()
	{
		instance = this;
		
		frag_plural = Language.Get (ShopLanguageField.SHOP_FRAGMENT_PLURAL.ToString());
		get_frag = Language.Get(ShopLanguageField.SHOP_FRAGMENT_GET.ToString());
		
		panel = gameObject.GetComponent<UIPanel>();
		ccc = transform.Find("GameObject").gameObject;

		UIListItemContainer child;
		for(int i = 1; i <= 8; ++i)
		{
			child = transform.Find("iap"+i).gameObject.GetComponent<UIListItemContainer>();
			children.Add(child);
		}
		
		/*for(int i = 1; i <= 6; ++i)
		{
			child = transform.Find("harvester"+i).gameObject.GetComponent<UIListItemContainer>();
			children.Add(child);
		}*/
		
		child = transform.Find("harvesterBundle").gameObject.GetComponent<UIListItemContainer>();
		children.Add(child);
		
		child = transform.Find("harvester6").gameObject.GetComponent<UIListItemContainer>();
		children.Add(child);
		
		foreach(var c in children)
		{
			c.gameObject.SetActive(false);
		}
		scroll = transform.Find("scroll").gameObject.GetComponent<UIScrollList>();
		fragmentCam = transform.parent.gameObject.GetComponent<Camera>();
		
		addIAP();
		UIManager.instance.AddCamera(Camera.main, -1, Mathf.Infinity, 0);
		
		OnLevelWasLoaded();
		
		EventManager.HarvesterChanged += populateHarvesterList;
		EventManager.FragmentChanged += checkFragment;
	}

	public void addIAP()
	{
		foreach(var c in children)
		{
			if(c.gameObject.name.Contains("iap"))
			{
				//place buttons to purchase XP on top in ascending order
				//7 - XP 1000
				//8 - XP 3000
				if (c.gameObject.name.Contains("7"))
				{
					scroll.InsertItem(c, 0);
				}
				
				else if (c.gameObject.name.Contains("8"))
				{
					scroll.InsertItem(c, 1);
				}
				
				else
				{
					scroll.AddItem(c);
				}
			}
		}
		checkFragment();
		scroll.ScrollToItem(0, 0);
	}
	
	public void populateHarvesterList()
	{
		if (activePurchaseType != ShopPurchaseType.Harvester)
		{
			return;
		}
		
		scroll.ClearList(false);
		int count = KYDataManager.getInstance().upgradeNameForLevel("Harvester Count");
		UIButton but;
		
		foreach(var c in children)
		{
			string cName = c.gameObject.name;
			
			//to omit fragment options
			if(cName.Contains("harvester"))
			{
				
				scroll.AddItem(c);
				
				//need to iterate through the children of the bundle
				if (cName.Contains("Bundle"))
				{
					
					foreach (Transform sub in c.transform)
					{
						but = sub.Find ("but").gameObject.GetComponent<UIButton>();
						
						if (count < 5)
						{
							//+1 because we want to check the price of the next item
							if (sub.name.Contains((count + 1).ToString()))
							{
								sub.gameObject.SetActive(true);
								
								but.isActiveOver = true;
								but.controlIsEnabled = true;
							}
							
							else
							{
								but.isActiveOver = false;
								but.controlIsEnabled = false;
								sub.gameObject.SetActive(false);
								
							}
						}
						
						else
						{
							//disable the purchase button when max count has been reached
							if (sub.name.Contains((count).ToString()))
							{
								but.isActiveOver = false;
								but.controlIsEnabled = false;
							}
							
							else
							{
								but.isActiveOver = false;
								but.controlIsEnabled = false;
								sub.gameObject.SetActive(false);
							}
						}
					}
					
				}
				
				//always show (quick fill option)
				else
				{
					but = c.transform.Find("but").gameObject.GetComponent<UIButton>();
					
					if (count < 5)
					{
						but.isActiveOver = true;
						but.controlIsEnabled = true;
					}
					
					else
					{
						but.isActiveOver = false;
						but.controlIsEnabled = false;
					}
					
				}
			}
		}
		
	}
	
	//legacy code for extended buy list
	/*
	public void addHarvester(int i = 0)
	{
		scroll.ClearList(false);
		int count = KYDataManager.getInstance().upgradeNameForLevel("Harvester Count");
		int z = 0;
		UIButton but;
		foreach(var c in children)
		{
			if(c.gameObject.name.Contains("harvester"))
			{
				scroll.AddItem(c);
				but = c.transform.Find("but").gameObject.GetComponent<UIButton>();
				if(((z > count || z < count) && (z != 5)) || (z == 5 && count == 5))
				{
					but.isActiveOver = false;
					but.controlIsEnabled = false;
				}
				else
				{
					but.isActiveOver = true;
					but.controlIsEnabled = true;
				}
				++z;
			}
		}
		scroll.ScrollToItem(i, 0);
	}*/

	public void checkFragment()
	{	
		if (activePurchaseType != ShopPurchaseType.FragmentXP)
		{
			return;
		}
		
		foreach(var c in children)
		{
			if(c.gameObject.name.Contains("7"))
			{
				if(KYDataManager.getInstance().dataWrapper.rubyCount >= 5)
				{
					UIButton but = c.transform.Find("but").gameObject.GetComponent<UIButton>();
					but.isActiveOver = true;
					but.controlIsEnabled = true;
					//SpriteText text = c.transform.Find("desc").gameObject.GetComponent<TextMesh>();
					spend_5_frag.text = string.Format(frag_plural, 5);	
				}
				else
				{
					UIButton but = c.transform.Find("but").gameObject.GetComponent<UIButton>();
					but.isActiveOver = false;
					but.controlIsEnabled = false;
					//SpriteText text = c.transform.Find("desc").gameObject.GetComponent<SpriteText>();
					spend_5_frag.text = get_frag;
				}
			}
			else if(c.gameObject.name.Contains("8"))
			{
				if(KYDataManager.getInstance().dataWrapper.rubyCount >= 10)
				{
					UIButton but = c.transform.Find("but").gameObject.GetComponent<UIButton>();
					but.isActiveOver = true;
					but.controlIsEnabled = true;
					//SpriteText text = c.transform.Find("desc").gameObject.GetComponent<SpriteText>();
					spend_10_frag.text = string.Format(frag_plural, 10);					
				}
				else
				{
					UIButton but = c.transform.Find("but").gameObject.GetComponent<UIButton>();
					but.isActiveOver = false;
					but.controlIsEnabled = false;
					//SpriteText text = c.transform.Find("desc").gameObject.GetComponent<SpriteText>();
					spend_10_frag.text = get_frag;				
				}
			}
		}
		
		scroll.RepositionItems ();

	}

	public void onClickBack()
	{
		panel.StartTransition("Dismiss Forward");
		maincam.instance.overlaycam_above.enabled = false;
		maincam.instance.overlaycam_below.enabled = false;
		if(TitleController.instance != null)
		{
			TitleController.instance.overlay.enabled = false;
			TitleController.instance.isBlocking = false;
		}
		ccc.SetActive(false);
		ShopManager.isBlocking = false;
		UIManager.instance.RemoveCamera(0);
		UIManager.instance.AddCamera(Camera.main, -1, Mathf.Infinity, 0);
		fragmentCam.enabled = false;
	}

	public void comein(int i = 0)
	{	
		//harvester
		if  (i == 1)
		{
			shopHeaderContainer.updateIcon(ShopPurchaseType.Harvester);
			GA.API.Design.NewEvent(CustomDesignEvent.getNavigationWindow(NavigationType.Harvester));
			
			activePurchaseType = ShopPurchaseType.Harvester;
		}
		
		else
		{
			shopHeaderContainer.updateIcon(ShopPurchaseType.FragmentXP);
			GA.API.Design.NewEvent(CustomDesignEvent.getNavigationWindow(NavigationType.Shop));
			
			activePurchaseType = ShopPurchaseType.FragmentXP;
		}
		
		scroll.ClearList(false, transform);
		for(int k = 0; i < children.Count; ++k)
		{
			if(k == children.Count)
			{
				break;
			}
			var c = children[k];
			if(c != null)
			{
				c.gameObject.SetActive(false);
			}
			else
			{
				children.Remove(c);
				--k;
			}
		}
		if(i == 0)
		{
			addIAP();
		}
		else
		{
			populateHarvesterList();
			//addHarvester();
		}
		panel.StartTransition("Bring in Forward");
		ccc.SetActive(true);
		fragmentCam.enabled = true;
		UIManager.instance.RemoveCamera(0);
		UIManager.instance.AddCamera(fragmentCam, -1, Mathf.Infinity, 0);
	}

	public void onClickBuyy(int index)
	{
		string productID;
		
		if(index == 1)
		{
			productID = SKUData.ID_fragment1;
			//KYDataManager.getInstance().dataWrapper.rubyCount += 1;
		}
		else if(index == 2)
		{
			productID = SKUData.ID_fragment6;
			//KYDataManager.getInstance().dataWrapper.rubyCount += 6;
		}
		else if(index == 3)
		{
			productID = SKUData.ID_fragment12;
			//KYDataManager.getInstance().dataWrapper.rubyCount += 12;
		}
		else if(index == 4)
		{
			productID = SKUData.ID_fragment30;
			//KYDataManager.getInstance().dataWrapper.rubyCount += 30;
		}
		else if(index == 5)
		{
			productID = SKUData.ID_fragment60;
			//KYDataManager.getInstance().dataWrapper.rubyCount += 60;			
		}
		else
		{
			productID = SKUData.ID_fragment85;
			//KYDataManager.getInstance().dataWrapper.rubyCount += 85;						
		}
		
#if UNITY_ANDROID
		GoogleIAB.purchaseProduct(SKUData.getSKU (productID), SKUData.createTransactionToken());
#endif
		
#if UNITY_IPHONE
		StoreKitBinding.purchaseProduct(SKUData.getSKU (productID), 1);
#endif
		
		checkFragment();
	}

	public void onClickBuyXP(int index)
	{
		//xp 1000
		if(index == 7)
		{	
			if (KYDataManager.getInstance().dataWrapper.rubyCount >= 5)
			{
				KYDataManager.getInstance().dataWrapper.rubyCount -= 5;
				KYDataManager.getInstance().dataWrapper.xpCount += 1000;
				
				GA.API.Design.NewEvent(CustomDesignEvent.getPurchaseXP(QuantityXP._1000));
			}
		}
		
		//xp 3000
		else if(index == 8)
		{
			if (KYDataManager.getInstance().dataWrapper.rubyCount >= 10)
			{
				KYDataManager.getInstance().dataWrapper.rubyCount -= 10;
				KYDataManager.getInstance().dataWrapper.xpCount += 3000;
				
				GA.API.Design.NewEvent(CustomDesignEvent.getPurchaseXP(QuantityXP._3000));
			}
			
		}
		
		/* legacy code - no idea what these are for
		else if(index == 3)
		{
			Debug.Log ("3");
			KYDataManager.getInstance().dataWrapper.rubyCount += 3000;
		}
		else if(index == 4)
		{
			Debug.Log ("4");
			KYDataManager.getInstance().dataWrapper.rubyCount += 180;
		}
		*/
		checkFragment();
	}

	public void onClickHarvester(int index)
	{
		string productID;
		
		if(index == 1)
		{
			productID = SKUData.ID_harvester1;
		}
		else if(index == 2)
		{
			productID = SKUData.ID_harvester2;
		}
		else if(index == 3)
		{
			productID = SKUData.ID_harvester3;
		}
		else if(index == 4)
		{
			productID = SKUData.ID_harvester4;
		}
		else if(index == 5)
		{
			productID = SKUData.ID_harvester5;			
		}
		
		else
		{
			productID = SKUData.ID_harvesterAll;					
		}
		
#if UNITY_ANDROID
		GoogleIAB.purchaseProduct(SKUData.getSKU (productID), SKUData.createTransactionToken());
#endif
		
#if UNITY_IPHONE
		StoreKitBinding.purchaseProduct(SKUData.getSKU (productID), 1);
#endif
		
		populateHarvesterList();
		
		//legacy code without iap
		/*
		//if player buys one harvester only
		if(index != 6)
		{
			//KYDataManager.getInstance().addUpgrade(5);
		}
		
		//if player purchases 5
		else
		{
			productID = SKUData.ID_harvesterAll;
			//KYDataManager.getInstance().addUpgrade(5, 5);
		}
		
		
		//addHarvester(index-1);
		
		*/
		
	}
	
	public void onClickRestore()
	{
		StoreKitBinding.restoreCompletedTransactions();
	}

	public void OnLevelWasLoaded()
	{
		if(Application.loadedLevelName == "title")
		{
			transform.localPosition = new Vector3(-2, 29, 3);
		}
		else
		{
			fragmentCam.enabled = false;
			transform.localPosition = new Vector3(-4, 29, 3);
		}
	}
	
	void OnDestroy()
	{
		EventManager.HarvesterChanged -= populateHarvesterList;
		EventManager.FragmentChanged -= checkFragment;
	}
}

