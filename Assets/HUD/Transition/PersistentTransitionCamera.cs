﻿using UnityEngine;
using System.Collections;
using System;

public enum TransitionType
{
	NoChange,
	Close,
	Open,
	CloseOpen
}

public class PersistentTransitionCamera : MonoBehaviour {
	
	private static PersistentTransitionCamera __instance;
	
	public GameObject transitionElement;
	private UIPanel leftDoor, rightDoor;
	
	public HintManager hintManager;
	
	delegate IEnumerator TransitionMethod(string name);
	private float delay = 2f;
	
	private bool isPlaying = false;
	private bool isClosed = false;
	
	void Awake()
	{
		transitionElement = gameObject.transform.FindChild("doorTransition").gameObject;
		leftDoor = transitionElement.transform.FindChild("leftDoor").gameObject.GetComponent<UIPanel>();
		rightDoor = transitionElement.transform.FindChild("rightDoor").gameObject.GetComponent<UIPanel>();
	}
	
	IEnumerator testTransition()
	{
		close();
		
		yield return new WaitForSeconds(4f);
		
		open();
		
		yield break;
	}
	
	public static PersistentTransitionCamera instance
	{
		get
		{
			if (__instance == null)
			{
				GameObject go = Instantiate(Resources.Load("camTransition")) as GameObject;
				DontDestroyOnLoad(go);
				__instance = go.GetComponent<PersistentTransitionCamera>();
			}
			
			return __instance;
		}
	}
	
	IEnumerator delayedLoadClosedScene(string sceneName)
	{
		close ();
		yield return new WaitForSeconds(delay);
		
		//moving into result screen
		//unload the map
		if (sceneName.Contains("Result"))
		{
			MapLoader.instance.UnloadMap();
		}
		
		//Application.LoadLevel(sceneName);
		
		yield return Application.LoadLevelAsync(sceneName);
		
		yield break;
	}
	
	IEnumerator delayedLoadOpenScene(string sceneName)
	{
		//moving back to title from level or result scenes
		//show hint & disconnect game
		if (sceneName.Contains("title") && Application.loadedLevelName.Contains("Result"))
		{
			hintManager.show(0.5f);	
			yield return new WaitForSeconds(1f);
		}
		
		yield return new WaitForSeconds(1f);
		
		yield return Application.LoadLevelAsync(sceneName);
		
		//Application.LoadLevel(sceneName);
		
		//going back into level
		if(sceneName.Contains("level"))
		{
			if(MapResource.instance != null)
			{
				Destroy(MapResource.instance.gameObject);
			}
		}
		
		yield return new WaitForSeconds(0.5f);
		open ();
		hintManager.hide(0);
		yield break;
	}
	
	IEnumerator delayedLoadNoChangeScene(string sceneName)
	{
		yield return new WaitForSeconds(delay);
		//Application.LoadLevel(sceneName);
		yield return Application.LoadLevelAsync(sceneName);
		yield break;
	}
	
	IEnumerator delayedLoadCloseOpenScene(string sceneName)
	{
		if (!isClosed)
		{
			close ();
		}
		
		hintManager.show (0.5f);
		
		yield return new WaitForSeconds(delay);
		
		//leaving to title screen from a level (abort)
		if (sceneName.Contains("title") && Application.loadedLevelName.Contains("level"))
		{
			MapLoader.instance.UnloadMap();
		}
		
		yield return Application.LoadLevelAsync(sceneName);
		//Application.LoadLevel(sceneName);
		
		yield return new WaitForSeconds(0.5f);
		
		open ();
		hintManager.hide(0);
		
		yield break;
	}
	
	public void close()
	{
		leftDoor.StartTransition(UIPanelManager.SHOW_MODE.BringInBack);
		rightDoor.StartTransition(UIPanelManager.SHOW_MODE.BringInBack);
		
		isClosed = true;
	}
	
	public void open()
	{
		leftDoor.StartTransition(UIPanelManager.SHOW_MODE.DismissForward);
		rightDoor.StartTransition(UIPanelManager.SHOW_MODE.DismissForward);
		
		isClosed = false;
	}
	
	IEnumerator delayedLoadIntoLevel(string sceneName)
	{
		if (!isClosed)
		{
			close ();
		}
		
		hintManager.show (0.5f);
		
		yield return new WaitForSeconds(delay);
		
		Fury.Behaviors.Manager.Instance.StartGame();
		
		yield return new WaitForSeconds(0.5f);
		
		open ();
		hintManager.hide(0);
		
		yield break;
	}
	
	public void transitionIntoLevel(Fury.Database.Map map)
	{
		if (isPlaying)
		{
			return;
		}
		
		isPlaying = true;
		
		StartCoroutine(beginTransition(delayedLoadIntoLevel, map.Scene));
	}
	
	public void transitionToScene(string name, TransitionType type)
	{	
		if (isPlaying)
		{
			return;
		}
		
		isPlaying = true;
		
		TransitionMethod transitionMethod;
		
		switch (type)
		{
			case TransitionType.NoChange:
				transitionMethod = delayedLoadNoChangeScene;
				break;
				
			case TransitionType.Open:
				transitionMethod = delayedLoadOpenScene;
				break;
				
			case TransitionType.Close:
				transitionMethod = delayedLoadClosedScene;
				break;
				
			case TransitionType.CloseOpen:
				transitionMethod = delayedLoadCloseOpenScene;
				break;
				
			default:
				transitionMethod = delayedLoadCloseOpenScene;
				break;
		}
		
		StartCoroutine(beginTransition(transitionMethod, name));
		
		/*string currentScene = Application.loadedLevelName;
		
		if (currentScene == "title")
		{
			if (name.Contains("level"))
			{
				StartCoroutine(delayedLoadCloseOpenScene(name));
			}
			
			else
			{
				StartCoroutine(delayedLoadClosedScene(name));
			}
			
		}
		
		else
		{
			if (name == "title")
			{
				if (name.Contains("level"))
				{
					
				}
				StartCoroutine(delayedLoadOpenScene(name));
			}
			
			else if (name.Contains("level"))
			{
				StartCoroutine(delayedLoadCloseOpenScene(name));
			}
			
			else if (currentScene == "barracks"
				|| currentScene == "gotcha")
			{
				StartCoroutine(delayedLoadNoChangeScene(name));
			}		
		}*/
	}
	
	IEnumerator beginTransition(TransitionMethod transitionMethod, string name)
	{
		StartCoroutine(transitionMethod(name));
		
		yield return new WaitForSeconds(delay);
		
		isPlaying = false;
		
		Resources.UnloadUnusedAssets();
		
		yield break;
	}
}
