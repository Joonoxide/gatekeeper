﻿using UnityEngine;
using System.Collections;

public class MissionRewardContainer : MonoBehaviour {
	
	[SerializeField]
	private GameObject handle;
	
	[SerializeField]
	private TextMesh variableText;
	
	public void updateVariableText(string text)
	{
		float defaultVariableWidth = 1.3f;
		float spacing = 0f;
		
#if LANGUAGE_JA
		defaultVariableWidth = 1.5f;
		spacing = 0.5f;
#endif
		variableText.text = text;
		
		if (handle.transform.childCount > 1)
		{	
			float diff = Mathf.Abs(defaultVariableWidth - variableText.renderer.bounds.size.x) * 0.5f;
		
			handle.transform.position = handle.transform.position + new Vector3(-diff, 0, 0);
			
			variableText.transform.localPosition += new Vector3(spacing, 0, 0);
		}
		
	}
}
