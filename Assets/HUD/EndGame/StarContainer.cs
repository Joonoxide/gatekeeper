﻿using UnityEngine;
using System.Collections;

public class StarContainer : MonoBehaviour {
	
	[SerializeField]
	private GameObject state_on, state_off;
	
	public void enable(bool isNewStar)
	{
		state_on.SetActive(true);
		state_off.SetActive(false);
		
		if (isNewStar)
		{
			iTween.ScaleFrom(state_on, iTween.Hash("time", 0.5f, "x", 3, "y", 3));
		}
	}
	
	public void disable()
	{
		state_on.SetActive(false);
		state_off.SetActive(true);
	}
}
