﻿using UnityEngine;
using System.Collections;

public class FrameContainer : MonoBehaviour {

	[SerializeField]
	private GameObject state_on, state_off, state_new;
	
	public void enable(bool isNew)
	{
		state_off.SetActive(false);
		
		if (isNew)
		{
			state_new.SetActive(true);
			state_on.SetActive(false);
		}
		
		else
		{
			state_new.SetActive(false);
			state_on.SetActive(true);
		}
	}
	
	public void disable()
	{
		state_on.SetActive(false);
		state_off.SetActive(true);
		state_new.SetActive(false);
	}
}
