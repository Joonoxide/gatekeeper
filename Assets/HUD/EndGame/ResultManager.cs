﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public enum ResultFieldIndex
{
	RemainingGold = 0,
	GoldSpent,
	TroopsKilled,
	TroopsLost,
	HarvestersKilled,
	HarvestersLost,
	TowersDestroyed,
	TowersLost
}

public enum RewardsTypeIndex
{
	XP = 0,
	Fragment,
	Character
}

public class ResultManager : MonoBehaviour {
	
	public GameObject panel, panelReward;
	public UIScrollList scrollList_stat, scrollList_reward;
	public List<UIListItemContainer> scrollStatsItems;
	public CharacterCardContainer characterCard;
	public GameObject occluder;
	
	public SkeletonAnimation winEmblem, loseEmblem;
	public GameObject icon_win, icon_lose;
	public List<StarContainer> stars;
	public List<MissionShieldContainer> missionShields;
	public List<MissionRewardContainer> missionRewards;
	
	ResultData resultData;
	string format_frag_plural, format_frag_singular;
	
	void searchForResultData()
	{
		//get the result data from the scene (should be available once a battle has ended)
		//destroy extras (if any) in the scene
		GameObject [] potentialDatasFound = GameObject.FindGameObjectsWithTag("resultData");
		
		if (potentialDatasFound != null && potentialDatasFound.Length > 0)
		{
			for (int i = 0; i < potentialDatasFound.Length; i++)
			{
				//only take the first one in the list
				if (i == 0)
				{
					resultData = potentialDatasFound[i].GetComponent<ResultData>();
				}
				
				else
				{
					DestroyImmediate(potentialDatasFound[i]);
				}
				
			}
		}
		
		else
		{
			//none found, create dummy
			resultData = new ResultData();
		}
	}
	
	void Start () {
		
		searchForResultData();
		applyData();
		populateFields();
		
		slideIn();
	}
	
	void populateFields()
	{
		format_frag_plural = Language.Get(ShopLanguageField.SHOP_FRAGMENT_PLURAL.ToString());
		format_frag_singular = Language.Get (ShopLanguageField.SHOP_FRAGMENT_SINGULAR.ToString());
		
		//add scroll items to the list, at the same time populate its fields
		for (int i = 0; i < scrollStatsItems.Count; i++)
		{
			scrollList_stat.AddItem(scrollStatsItems[i]);
			scrollStatsItems[i].gameObject.
				transform.FindChild("text_value").
					GetComponent<SpriteText>().Text = resultData.getValueFromIndex(i).ToString();
		}
		
		//disable/enable correct icons and emblems according to win/lose
		if (resultData.isWin)
		{	
			icon_win.SetActive(true);
			icon_lose.SetActive(false);
			
			winEmblem.gameObject.SetActive(true);
			loseEmblem.gameObject.SetActive(false);
		}
		
		else
		{
			//force the lose emblem to completion at start since it does not have an idle animation
			loseEmblem.state.Update(10);
			
			icon_win.SetActive(false);
			icon_lose.SetActive(true);
			
			winEmblem.gameObject.SetActive(false);
			loseEmblem.gameObject.SetActive(true);
		}
		
		//update star count
		//TO DO: animation for virgin star
		List<StarContainer> starsToSpawn = new List<StarContainer>();
		bool isChallengePassed = resultData.passedChallenge;
		bool isAttemptingNewStar = resultData.attemptedStarCount > resultData.starCount;
		
		for(int k = 0; k < stars.Count; k++)
		{
			stars[k].disable();	
			
			if (k < resultData.starCount)
			{
				stars[k].enable(false);
			}
		}
		
		//update mission shields
		MissionShieldContainer missionUnlocked = null;
		
		for(int m = 0; m < missionShields.Count; m++)
		{
			//unlock all up to the current star count
			if (m <= resultData.starCount)
			{
				missionShields[m].unlock(false);
			}
			
			else
			{
				missionShields[m].doLock();
			}
		}
		
		//add new star if it is a victory round and 
		//player is attempting a new star and has passed the challenge
		List<MissionRewardContainer> rewardsToAward = new List<MissionRewardContainer>();
		panelReward.SetActive(false); //hide reward panel at the beginning
		characterCard.hideSnap(); //hide character card at the beginning
		
		Debug.Log ("challenge passed: " + isChallengePassed);
		Debug.Log ("attemping new star: " + isAttemptingNewStar);
		if (resultData.isWin)
		{
			if (isAttemptingNewStar && isChallengePassed)
			{
			
				starsToSpawn.Add(stars[resultData.attemptedStarCount - 1]);
				
				if (resultData.attemptedStarCount <= stars.Count - 1)
				{
					missionUnlocked = missionShields[Mathf.Min(resultData.attemptedStarCount, stars.Count - 1)];
				}
				
			}
			
			//update available rewards
			for (int n = 0; n < Enum.GetValues(typeof(RewardsTypeIndex)).Length; n++)
			{
				MissionRewardContainer rewardContainer = missionRewards[n];
				
				//XP is guaranteed, no need to check
				if (n.Equals((int)RewardsTypeIndex.XP))
				{
					rewardsToAward.Add(rewardContainer);
					rewardContainer.updateVariableText("+ " + resultData.getRewardXP().ToString() + " XP");
				}
				
				//check if new star (attempted star > starCount && isPassedChallenge)
				else if (n.Equals((int)RewardsTypeIndex.Fragment))
				{
					if (isAttemptingNewStar && isChallengePassed)
					{
						int fragCount = resultData.getRewardFragments();
						
						rewardsToAward.Add(rewardContainer);
						//rewardContainer.updateVariableText(fragCount.ToString() + " Fragment" + (fragCount > 1? "s":""));
						
						string format = fragCount > 1? format_frag_plural: format_frag_singular;
						
						rewardContainer.updateVariableText(string.Format(format, fragCount));
					}
				}
				
				//check if virgin level (starCount == 0)
				else
				{
					if (resultData.isVirginLevel && isChallengePassed)
					{
						string [] unlockedCharacters = resultData.getUnlockedCharacters().ToArray();
						
						//only process if there are at least one unlocked characters
						if (unlockedCharacters.Length > 0)
						{
							rewardsToAward.Add(rewardContainer);
							//rewardContainer.updateVariableText(string.Join(", ", unlockedCharacters));
							characterCard.swapCharacters(unlockedCharacters[0]);
							
							///process character names and put them in readable strings with correct punctuation (, &)
							string buffer = "";
			
							for (int i = 0; i < unlockedCharacters.Length; i++)
							{	
								if (unlockedCharacters[i] == "")
								{
									continue;
								}
								
								//not last unit
								if (i < unlockedCharacters.Length - 1)
								{
									buffer += Language.Get("UNIT_NAME_" + unlockedCharacters[i].ToUpper());
									
									if (unlockedCharacters.Length > 2)
									{
										buffer += ", ";
									}
								}
								
								else
								{
									if (unlockedCharacters.Length == 1)
									{
										buffer += Language.Get("UNIT_NAME_" + unlockedCharacters[i].ToUpper());
									}
									
									else
									{
										buffer += " & " + Language.Get("UNIT_NAME_" + unlockedCharacters[i].ToUpper());
									}
									
								}
							}
							
							rewardContainer.updateVariableText(buffer);
						}
						
					}
				}
			}
			
			//populate reward scroller
			for (int p = 0; p < rewardsToAward.Count; p++)
			{
				scrollList_reward.AddItem(rewardsToAward[p].gameObject.transform.GetComponent<UIListItemContainer>());
			}
		}
		
		StartCoroutine(doQueuedEvents(starsToSpawn, missionUnlocked, rewardsToAward));
	}
	
	IEnumerator doQueuedEvents(List<StarContainer> stars, MissionShieldContainer unlockedMission, List<MissionRewardContainer> awardsToReward)
	{
		//wait for window drop animation to settle
		yield return new WaitForSeconds(1f);
		
		//spawn stars
		StartCoroutine(spawnStar(stars));
		
		//wait until all stars have been spawned
		yield return new WaitForSeconds(1f * (stars.Count + 1)); //+1 for extra delay after final star
		
		if (unlockedMission)
		{
			StartCoroutine(unlockNewMission(unlockedMission));
		}
		
		//wait until mission unlock animations have settled
		yield return new WaitForSeconds(2f);
		
		if (awardsToReward.Count > 0)
		{
			StartCoroutine(awardRewards(awardsToReward));
		}
		
		yield return new WaitForSeconds(4f * awardsToReward.Count + 1); //+1 for extra delay to fade out
		
		allowInteraction();
		
		yield break;
	}
	
	IEnumerator spawnStar(List<StarContainer> stars)
	{
		foreach(StarContainer star in stars)
		{
			yield return new WaitForSeconds(1f);
			star.enable(true);
		}
		
		yield break;
	}
	
	IEnumerator unlockNewMission(MissionShieldContainer unlockedMission)
	{
		yield return new WaitForSeconds(1f);
		
		unlockedMission.unlock(true);
		
		yield break;
	}
	
	IEnumerator awardRewards(List<MissionRewardContainer> awards)
	{
		iTween.FadeFrom(panelReward, 0, 1f);
		panelReward.SetActive(true);
		
		for (int i = 0; i < awards.Count; i++)
		{
			scrollList_reward.ScrollToItem(i, 0.5f);
			
			if (missionRewards.IndexOf(awards[i]) == (int)RewardsTypeIndex.Character)
			{
				characterCard.showFlip();
			}
			
			yield return new WaitForSeconds(4f);
		}
		
		if (!characterCard.isHidden)
		{
			characterCard.hideFlip();
		}
		
		yield return new WaitForSeconds(1f);
		
		iTween.FadeTo(panelReward, 0, 1f);
	
		yield return new WaitForSeconds(1f);
		
		panelReward.SetActive(false);
		
		yield break;
	}
	
	public void onClickRematch()
	{
		occludeInteraction();
		
		Debug.Log ("rematch");
		
		string levelName = "level"+resultData.level;
		
		destroyData();
		
		slideOut();
		
		//GameController.LoadLevel(levelName, TransitionType.Open);
		MapLoader.instance.ReloadMap();
	}
	
	public void onClickContinue()
	{
		occludeInteraction();
		
		Debug.Log ("continue");
		destroyData();
		
		slideOut();
		
		GameController.LoadLevel("title", TransitionType.Open);
	}
	
	public void destroyData()
	{
		try
		{	
			DestroyImmediate(resultData.gameObject);
		}
		
		catch (NullReferenceException e)
		{
			Destroy(resultData);
		}
	}
	
	public void applyData()
	{
		KYDataManager data = KYDataManager.getInstance();
		
		bool isChallengePassed = resultData.passedChallenge;
		bool isAttemptingNewStar = resultData.attemptedStarCount > resultData.starCount;
		
		if (resultData.isWin)
		{
			if (isAttemptingNewStar && isChallengePassed)
			{
				data.addLevel(resultData.level, resultData.attemptedStarCount);
				
				data.dataWrapper.rubyCount += resultData.getRewardFragments();
			}
			
			data.addLevel(resultData.level, resultData.starCount);
			data.dataWrapper.xpCount += resultData.getRewardXP();
		}
		
	}
	
	public void occludeInteraction()
	{
		occluder.SetActive(true);
	}
	
	public void allowInteraction()
	{
		occluder.SetActive(false);
	}
	
	public void slideIn()
	{
		iTween.MoveFrom(panel, iTween.Hash("y", 10, "time", 2f, "easetype", iTween.EaseType.easeOutCubic));
	}
	
	public void slideOut()
	{
		iTween.MoveTo(panel, iTween.Hash("y", 15, "time", 1f, "easetype", iTween.EaseType.linear));
	}
}
