
skeleton-1.png
format: RGBA8888
filter: Linear,Linear
repeat: none
lose_defeat
  rotate: true
  xy: 616, 117
  size: 410, 147
  orig: 410, 147
  offset: 0, 0
  index: -1
lose_robe
  rotate: false
  xy: 310, 529
  size: 642, 319
  orig: 642, 319
  offset: 0, 0
  index: -1
lose_skull
  rotate: false
  xy: 310, 139
  size: 304, 388
  orig: 304, 388
  offset: 0, 0
  index: -1
lose_sword
  rotate: false
  xy: 2, 2
  size: 306, 846
  orig: 306, 846
  offset: 0, 0
  index: -1
