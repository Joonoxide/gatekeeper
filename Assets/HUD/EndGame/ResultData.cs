﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using KYData;

public class ResultData : MonoBehaviour {
	
	#region ESSENTIAL_RESULT_DATA
	
	public bool isWin = true;
	public int level = 4;
	public int remainingGold = 0;
	public int starCount = 0;
	public int attemptedStarCount = 1;
	public bool isVirginLevel = true;
	public int enemyTowerLevel = 1;
	#endregion
	
	#region SPECIAL_LEVEL_CONDITIONS
	
	public int spentGold = 0;
	public int towersLost = 0;
	public int towersDestroyed = 0;
	public int harvestersLost = 0;
	public int harvestersKilled = 0;
	public int troopsLost = 0;
	public int troopsKilled = 0;
	
	#endregion
	
	void Awake()
	{
		DontDestroyOnLoad(gameObject);
	}
	
	public int getValueFromIndex(int index)
	{
		int val = 0;
		
		switch (index)
		{
			case (int)ResultFieldIndex.RemainingGold:
				val = remainingGold;
				break;
			
			case (int)ResultFieldIndex.GoldSpent:
				val = spentGold;
				break;
			
			case (int)ResultFieldIndex.TroopsKilled:
				val = troopsKilled;
				break;
			
			case (int)ResultFieldIndex.TroopsLost:
				val = troopsLost;
				break;
			
			case (int)ResultFieldIndex.HarvestersKilled:
				val = harvestersKilled;
				break;
			
			case (int)ResultFieldIndex.HarvestersLost:
				val = harvestersLost;
				break;
			
			case (int)ResultFieldIndex.TowersDestroyed:
				val = towersDestroyed;
				break;
			
			case (int)ResultFieldIndex.TowersLost:
				val = towersLost;
				break;
			
			default:
				break;
			}
		
		return val;
	}
	
	public int getRewardXP()
	{
		bool isNewStar = false;
		
		if (attemptedStarCount > starCount && passedChallenge)
		{
			isNewStar = true;
		}
		
		return (int)(UnityEngine.Random.Range(GameSettings.XP_JAGGED_LOW_MOD, GameSettings.XP_JAGGED_HIGH_MOD) * 
			Mathf.Max((int)GameSettings.XP_MIN_BY_STAR[attemptedStarCount - 1], (int)(
			(remainingGold *  enemyTowerLevel) + (GameSettings.XP_BASE_STAR_MOD * enemyTowerLevel * (isNewStar? attemptedStarCount: 0))))
			);
	}
	
	public int getRewardFragments()
	{
		LevelRewardWrapper reward = getRewardWrapper(level);
		
		if (reward != null)
		{
			return reward.fragments[attemptedStarCount - 1];
		}
		
		return 0;
	}
	
	public List<string> getUnlockedCharacters()
	{
		LevelRewardWrapper reward = getRewardWrapper(level);
					
		return reward.unlockableCharacters;
	}
	
	public bool passedChallenge
	{
		get
		{
			//Need to spend a minimum amount of gold in battle
			if (spentGold < TitleController.useGold)
			{
				Debug.Log("fail at gold");
				return false;
			}
			
			//Not allowed to lose any tower (towersLost == 0)
			if (TitleController.loseAnyTower)
			{
				if (towersLost > 0)
				{
					Debug.Log("fail at lost tower");
					return false;
				}
			}
			
			//Need to destroy all enemy towers (towersDestroyed == 2)
			if (TitleController.isDestroyTower)
			{
				if (towersDestroyed < 2)
				{
					Debug.Log("fail at destroyed tower");
					
					return false;
				}
			}
			
			//Need to kill at LEAST a certain number of enemy harvesters
			if (TitleController.killHarvester > 0 && harvestersKilled < TitleController.killHarvester)
			{
				Debug.Log("fail at kill harvester");
				
				return false;
			}
			
			//Cannot lose more than a certain number of friendly harvesters
			if (TitleController.deadHarvester > 0 && harvestersLost > TitleController.deadHarvester)
			{
				Debug.Log("fail at lost harvester");
				
				return false;
			}
			
			
			return true;
		}
	}
	
	LevelRewardWrapper getRewardWrapper(int level)
	{
		if (KYDataManager.instance == null)
		{
			KYDataManager.getInstance();
		}
		
		foreach (LevelRewardWrapper reward in KYDataManager.arrayLevelRewardWrapper)
		{
			if (reward.level == level)
			{
				return reward;
			}
		}
		
		return null;
	}
}
