﻿using UnityEngine;
using System.Collections;

public class MissionShieldContainer : MonoBehaviour {

	[SerializeField]
	private GameObject state_locked, state_unlocked;
	
	[SerializeField]
	private FrameContainer frameContainer;
	
	public void unlock(bool isNew)
	{
		state_locked.SetActive(false);
		state_unlocked.SetActive(true);
		
		frameContainer.enable(isNew);
		
		iTween.ScaleFrom(state_unlocked, iTween.Hash("time", 0.5f, "x", 3, "y", 3));
	}
	
	public void doLock()
	{
		state_locked.SetActive(true);
		state_unlocked.SetActive(false);
		
		frameContainer.disable();
	}
}
