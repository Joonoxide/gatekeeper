﻿using UnityEngine;
using System.Collections;

public class CharacterCardContainer : MonoBehaviour {
	
	[SerializeField]
	private GameObject container;
	
	[SerializeField]
	private SkeletonAnimation skeleton;
	
	public bool isHidden {private set; get;}
	
	public void swapCharacters(string name)
	{
		string assetName = name.ToLower() + "skeleton_1";
		
		skeleton.skeletonDataAsset = Resources.Load(assetName) as SkeletonDataAsset;
		skeleton.Clear();
		skeleton.Initialize();
		skeleton.state.SetAnimation("standby", true);
	}
	
	public void showFlip()
	{
		//iTween.RotateTo(container, new Vector3(0, 450, 0), 2f);
		
		iTween.RotateBy(container, iTween.Hash ("amount", new Vector3(0, -2.25f, 0), "easetype", iTween.EaseType.easeInCubic, "time", 0.75f, "oncomplete", "correctRotationToFront"));
		
		isHidden = false;
	}
	
	public void hideFlip()
	{
		//iTween.RotateTo(container, new Vector3(0, 90, 0), 1f);
		
		iTween.RotateBy(container, iTween.Hash ("amount", new Vector3(0, 2.25f, 0), "easetype", iTween.EaseType.easeOutCubic, "time", 0.75f, "oncomplete", "correctRotationToNothing"));
		
		isHidden = true;
	}
	
	public void hideSnap()
	{
		container.transform.rotation = Quaternion.Euler(0, 90, 0);
		
		isHidden = true;
	}
	
	void correctRotationToFront()
	{
		container.transform.rotation = Quaternion.Euler(0, 0, 0);
	}

	void correctRotationToNothing()
	{
		container.transform.rotation = Quaternion.Euler(0, 90, 0);
	}
}
