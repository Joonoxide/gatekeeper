﻿using UnityEngine;
using System.Collections;

public class rayanimation : MonoBehaviour 
{
	public Vector3 originalScale;
	public Vector3 targetScale;
	public bool direction;
	public float elapsed;
	public float h;
	public float w;
	public Vector3 originalPos;

	void Start () 
	{
		originalScale = transform.localScale;
		targetScale = originalScale;
		targetScale = targetScale*0.3f;
		h = GetComponent<PackedSprite>().height;
		w = GetComponent<PackedSprite>().width;
		originalPos = transform.position;
	}
	
	void Update () 
	{
		elapsed += Time.deltaTime;
		float time = elapsed/1f;

		if(direction)
		{
			transform.localScale = Vector3.Lerp( originalScale, targetScale, time);
		}
		else
		{
			transform.localScale = Vector3.Lerp( targetScale, originalScale, time);
		}

		Vector3 pos = originalPos;
		pos = originalPos;
		pos.y = pos.y-(1f-transform.localScale.y)*h/2;
		transform.position = pos;

		if(time > 1)
		{
			direction = !direction;
			elapsed = 0;
		}
	}
}
