using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using UnityEngine;
using LitJson;
using KYData;

public class GotchaManager : MonoBehaviour 
{
	public class GotcaWrapper
	{
		public int index;	
		public List<string> items;	
		public double prob;
	}

	public class GachaLevelWrapper
	{
		public int lowerBound;
		public int upperCound;
		public int prob;
	}
	
	public ParticleSystem particleXP;
	public KYAAgent[] fragment;
	public KYAAgent pot;
	public SkeletonAnimation pandoraBoxSkeleton_normal, pandoraBoxSkeleton_premium;
	public UIButton buttonFinish;
	public UIPanel panel1;
	public UIPanel panel2;
	public List<GotcaWrapper> arrayGotchaWrapper = new List<GotcaWrapper>();
	public List<GachaLevelWrapper> arrayGachaLevelWrapper = new List<GachaLevelWrapper>();
	public List<KYAAgent> sprites = new List<KYAAgent>();
	public static GotchaManager instance;
	public GameObject light;
	public UIPanel panelMore;
	public GameObject chosen1;
	public GameObject chosen2;
	public int maximum;
	
	public GameObject window;
	
	public UIButton btn_mode_XP, btn_mode_fragment;
	public GameObject container_XP, container_fragment, container_textXP, container_textFragment;
	private GachaType defaultGachaMode = GachaType.Fragment;
	private GachaType currentGachaMode;
	
	public List<string> lockedUnits = new List<string>();
	
	bool isAnimationActive = false;

	public Int64 getDifference()
	{
		TimeSpan diff = DateTime.Now-KYDataManager.getInstance().dataWrapper.lastSummon;
		return (Int64)diff.TotalSeconds;
	}

	public void Start()
	{	
		GA.API.Design.NewEvent(CustomDesignEvent.getNavigationWindow(NavigationType.Gacha));
		currentGachaMode = defaultGachaMode;
		
		updateGachaModeVisuals();
		updateLockedUnitsList();
		
		TextAsset data = Resources.Load("xp_json") as TextAsset;
		string strData = data.text;
		arrayGotchaWrapper = JsonMapper.ToObject<List<GotcaWrapper>>(strData);
		arrayGotchaWrapper = arrayGotchaWrapper.OrderBy(o=>o.index).ToList();
		instance = this;

		foreach(KYAAgent f in fragment)
		{
			f.gameObject.SetActive(false);
		}

		poss = new Vector3[]{ new Vector3(1.2f, 0, -14), new Vector3(-0.1f, 0, -14), new Vector3(-1.2f, 0, -14), new Vector3(-2.5f, 0, -14), new Vector3(-3.8f, 0, -14), new Vector3(-5.1f, 0, -14)};
		
		data = Resources.Load("gacha_level_json") as TextAsset;
		strData = data.text;

		maximum = 1;
		foreach(TrooperWrapper t in KYDataManager.getInstance().arrayTrooperWrapper)
		{
			if(t.level > maximum)
			{
				maximum = t.level;
			}
		}
		
		if (currentGachaMode == GachaType.Fragment)
		{
			pandoraBoxSkeleton_premium.state.SetAnimation("normal", true);
		}
		
		else
		{
			pandoraBoxSkeleton_normal.state.SetAnimation("normal", true);
		}
		
		buttonFinish.transform.Find("text1").gameObject.GetComponent<SpriteText>().Text = Language.Get (SummonLanguageField.SUMMON_SKIP.ToString()).ToUpper();
		
		slideIn();
	}
	
	public void slideIn()
	{
		iTween.MoveFrom(window, iTween.Hash("y", 10, "time", 2f, "easetype", iTween.EaseType.spring));
	}
	
	public void slideOut()
	{
		iTween.MoveTo(window, iTween.Hash("y", 15, "time", 1f, "easetype", iTween.EaseType.linear));
	}
	
	private void updateLockedUnitsList()
	{
		lockedUnits.Clear();
		
		List<LevelWrapper> arrayLevelWrapper = KYDataManager.instance.arrayLevelWrapper;
		List<LevelRewardWrapper> arrayLevelRewardWrapper = KYDataManager.arrayLevelRewardWrapper;
		
		Debug.Log ("no. of unlocked levels: " + arrayLevelWrapper.Count);
		for (int i = arrayLevelWrapper.Count; i < TitleController.maxLevel - 1; i++)
		{	
			foreach (string unit in arrayLevelRewardWrapper[i].unlockableCharacters)
			{
				if (!lockedUnits.Contains(unit))
				{
					lockedUnits.Add(unit);
				}
				
			}
		}
		
		Debug.Log ("locked units..." + string.Join (",", lockedUnits.ToArray()));
	}

	public SpriteText[] textLevels;

	public KYAAgent getSprite(string na)
	{
		//string n = "sprite_"+((na.Split('_'))[1].ToLower());
		string n = "sprite_" + na.ToLower();
		
		Debug.Log (n);
		
		GameObject newGO = GameObject.Instantiate(Resources.Load(n) as GameObject) as GameObject;
		GameObject newStar = GameObject.Instantiate(Resources.Load("bigstar") as GameObject) as GameObject;
		newGO.transform.localScale = newGO.transform.localScale*0.25f;
		newGO.transform.position = new Vector3(-2, 0, -10);
		newStar.transform.parent = newGO.transform;
		newStar.transform.localPosition = new Vector3(0, 8, 0);
		sprites.Add(newGO.AddComponent<KYAAgent>());
		newGO.GetComponent<SkeletonAnimation>().state.SetAnimation("standby", true);
		for(int i = 0; i < textLevels.Length; ++i)
		{
			if(textLevels[i].transform.parent == null)
			{
				textLevels[i].transform.parent = newGO.transform;
				textLevels[i].transform.localPosition = new Vector3(0, -1, 0);
				textLevels[i].Text = "L"+levels[i]%10;
				for(int j = (levels[i]-1)/10+2; j <= 3; ++j)
				{
					newStar.transform.Find("star"+j).gameObject.transform.localScale = Vector3.zero;
				}
				break;
			}
		}
		return sprites[sprites.Count-1];
	}

	public string randGotcha()
	{
		double randy = UnityEngine.Random.Range(0, 100);
		double p = 0;
		GotcaWrapper g = null;
		foreach(GotcaWrapper gotcha in arrayGotchaWrapper)
		{
			p += gotcha.prob;
			if(randy < p)
			{
				g = gotcha;
				break;
			}
		}

		if(g == null)
		{
			g = arrayGotchaWrapper[0];
		}

		int randy2 = UnityEngine.Random.Range(0, g.items.Count);
		return g.items[randy2];
	}
	
	public string generateRandomUnit(GachaType drawType)
	{
		return GachaGenerator.instance.generateUnit(drawType);
	}
	
	public int generateRandomLevel(GachaType drawType)
	{
		return GachaGenerator.instance.generateLevel(drawType);
	}
	
	public void onClickOneTime()
	{
		StartCoroutine(gotcha());
	}


	public void onClickFiveTime()
	{
		StartCoroutine(gotcha(6));
	}

	List<string> name = new List<string>();
	List<int> levels = new List<int>();

	public IEnumerator gotcha(int time = 1)
	{	
		name.Clear();
		levels.Clear();
		sprites.Clear();
		int cost = time;
		if(time == 6)
		{
			cost = 5;
		}
		
		//legacy code with assumption of only fragment summons
		/*
		if(KYDataManager.getInstance().dataWrapper.rubyCount < cost && !(getDifference() > 86400 && time == 1))
		{
			spawn();
			yield break;
		}
		else
		{
			if(!(getDifference() > 86400 && time == 1))
			{
				if (currentGachaMode == GachaType.Fragment)
				{
					KYDataManager.getInstance().dataWrapper.rubyCount -= cost;
					GA.API.Design.NewEvent(CustomDesignEvent.getGacha(GachaType.Fragment, cost == 5? QuantityGacha._5: QuantityGacha._1));
				}
				
				else
				{
					KYDataManager.getInstance().dataWrapper.xpCount -= (cost * GameSettings.GACHA_XP_COST_PER_TIME);
					GA.API.Design.NewEvent(CustomDesignEvent.getGacha(GachaType.XP, cost == 5? QuantityGacha._5: QuantityGacha._1));
				}
					
			}
			else
			{
				chosen1.SetActive(false);
				chosen2.SetActive(true);
				KYDataManager.getInstance().dataWrapper.lastSummon = DateTime.Now;
			}
		}*/
		
		if (currentGachaMode == GachaType.Fragment)
		{
			if(KYDataManager.getInstance().dataWrapper.rubyCount < cost && !(getDifference() > 86400 && time == 1))
			{
				spawn();
				yield break;
			}
			
			else
			{
				if(!(getDifference() > 86400 && time == 1))
				{
					KYDataManager.getInstance().dataWrapper.rubyCount -= cost;
					GA.API.Design.NewEvent(CustomDesignEvent.getGacha(GachaType.Fragment, cost == 5? QuantityGacha._5: QuantityGacha._1));
				}
				
				else
				{
					chosen1.SetActive(false);
					chosen2.SetActive(true);
					KYDataManager.getInstance().dataWrapper.lastSummon = DateTime.Now;
				}
			}
		}
		
		else
		{
			if(KYDataManager.getInstance().dataWrapper.xpCount < (cost * GameSettings.GACHA_XP_COST_PER_TIME))
			{
				spawn();
				yield break;
			}
			
			else
			{
				KYDataManager.getInstance().dataWrapper.xpCount -= (cost * GameSettings.GACHA_XP_COST_PER_TIME);
				GA.API.Design.NewEvent(CustomDesignEvent.getGacha(GachaType.XP, cost == 5? QuantityGacha._5: QuantityGacha._1));
			}
		}
		
		disableGachaModeChange();
		isAnimationActive = true;
		
		for(int i = 0; i < time; ++i)
		{
			//name.Add(randGotcha());
			name.Add(generateRandomUnit(currentGachaMode));
			
			/*float randyLevel = UnityEngine.Random.Range(0, 100f);
			int ll = 0;
			if(randyLevel < 96)
			{
				ll = 1;
			}
			else if(randyLevel < 99.5)
			{
				ll = 11;
			}
			else
			{
				ll = 21;
			}
			randyLevel = UnityEngine.Random.Range(0, 100f);

			if(randyLevel < 80)
			{
				ll += 0;
			}
			else if(randyLevel < 95)
			{
				ll += 1;
			}
			else
			{
				ll += 2;
			}*/
			
			levels.Add(generateRandomLevel(currentGachaMode));
		}
		int kkk = 0;
		foreach(string n in name)
		{
			getSprite(n);
			KYDataManager.getInstance().addTrooper("Call_" + n, false, levels[kkk]);
			++kkk;
		}
		int lb = 0;
		foreach(KYAAgent s in sprites)
		{
			int rank = (levels[lb]-1)/10+1;
			s.Start();
			s.gameObject.SetActive(false);
			SkeletonAnimation ss = s.gameObject.GetComponent<SkeletonAnimation>();
			string n = ss.skeletonDataAsset.name;
			ss.skeletonDataAsset = Resources.Load((n.Split('_'))[0]+"_"+rank) as SkeletonDataAsset;
			ss.Clear();
			ss.Initialize();
			ss.state.SetAnimation("standby", true);
			++lb;
		}
		
		pot.gameObject.SetActive(true);
		foreach(KYAAgent f in fragment)
		{
			f.gameObject.SetActive(true);
		}		
		pot.aname = "idel";

		panel1.gameObject.SetActive(false);
		yield return new WaitForSeconds(0.5f);
		panel2.StartTransition("Bring in Forward");
		
		if (currentGachaMode == GachaType.Fragment)
		{
			for(int i = 0; i < 1; ++i)
			{
				if(time == 1)
				{
					i = 0;
				}
				else
				{
					i = 1;
				}
				yield return new WaitForSeconds(0.5f);
				if(i > 0)
				{
					//pot.isLoop = true;
					//pot.Animate(0, true);	
				}
				fragment[i].transform.position = new Vector3(-2, 2, -10);
				fragment[i].transform.localScale = Vector3.zero;
				fragment[i].Animate();
				yield return new WaitForSeconds(1f);
				pot.isLoop = false;
				
				pandoraBoxSkeleton_premium.state.SetAnimation("summon", false);
				
				yield return new WaitForSeconds(1.7f);
				break;
			}
		}
		
		else
		{
			yield return new WaitForSeconds(1f);
			particleXP.Play();
			
			pandoraBoxSkeleton_normal.state.SetAnimation("summon", false);
			
			yield return new WaitForSeconds(2.0f);
		}
		
		yield return new WaitForSeconds(0.5f);
		pot.Animate(0, true);	
		yield return new WaitForSeconds(1f);
		
		if (currentGachaMode == GachaType.Fragment)
		{
			pandoraBoxSkeleton_premium.state.SetAnimation("normal", true);
		}
		
		else
		{
			pandoraBoxSkeleton_normal.state.SetAnimation("normal", true);
		}
		
		foreach(KYAAgent f in fragment)
		{
			f.transform.position = new Vector3(50, 2, 5);
		}
		pot.isLoop = false;
		pot.aname = "ladi";
		pot.Animate();
		yield return new WaitForSeconds(0.5f);
		int ii = 1;
		foreach(KYAAgent s in sprites)
		{
			s.gameObject.SetActive(true);
			s.gameObject.GetComponent<SkeletonAnimation>().state.SetAnimation("standby", true); 
			s.aname = "momo"+ii;
			if(sprites.Count == 1)
			{
				s.aname = "momo0";
			}
			s.Animate();
			++ii;
		}
		yield return new WaitForSeconds(1f);
		ii = 0;
		foreach(KYAAgent s in sprites)
		{
			textLevels[ii].gameObject.SetActive(true);
			++ii;
		}
		pot.aname = "madi";
		pot.Animate();
		yield return new WaitForSeconds(2f);
		onClickFinal();
	}

	public Vector3[] poss;
	public void onClickFinal()
	{
		int iii = 0;
		foreach(KYAAgent s in sprites)
		{
			textLevels[iii].gameObject.SetActive(true);
			++iii;
			for(int i = 1; i <= 3; ++i)
			{
				s.transform.Find("bigstar(Clone)").Find("star"+i).gameObject.GetComponent<MeshRenderer>().enabled = true;
			}
		}
		StopAllCoroutines();
		particleXP.Stop();
		
		pot.isLoop = false;
		pot.gameObject.SetActive(false);
		pot.transform.localScale = Vector3.zero;
		foreach(KYAAgent f in fragment)
		{
			iTween.Stop(f.gameObject);
			f.gameObject.SetActive(false);
			if(f.anime.anim != null)
			{
				f.anime.anim.End();
			}
			f.transform.position = new Vector3(50, 2, 5);
			f.gameObject.SetActive(false);
			f.anime.name = "";		
		}
		buttonFinish.transform.Find("text1").gameObject.GetComponent<SpriteText>().Text = Language.Get(SummonLanguageField.SUMMON_CLOSE.ToString()).ToUpper();
		buttonFinish.methodToInvoke = "onClickFinal2";
		light.SetActive(true);

		int ii = 0;
		foreach(KYAAgent s in sprites)
		{
			s.gameObject.SetActive(true);
			s.gameObject.GetComponent<SkeletonAnimation>().state.SetAnimation("standby", true); 
			iTween.Stop(s.gameObject);
			if(s.anime.anim != null)
			{
				s.anime.anim.End();
			}
			s.anime.name = "";
			s.transform.position = poss[ii];
			if(sprites.Count == 1)
			{
				s.transform.position = new Vector3(-1.85f, 0, -14f);
			}
			++ii;
		}
	}

	public void onClickFinal2()
	{
		light.SetActive(false);
		pot.aname = "wudi";
		pot.gameObject.SetActive(true);
		pot.Animate(0);
		StartCoroutine(finish());
		buttonFinish.transform.Find("text1").gameObject.GetComponent<SpriteText>().Text = Language.Get (SummonLanguageField.SUMMON_SKIP.ToString()).ToUpper();
		buttonFinish.methodToInvoke = "onClickFinal";
		foreach(KYAAgent s in sprites)
		{
			for(int i = 1; i <= 3; ++i)
			{
				s.transform.Find("bigstar(Clone)").Find("star"+i).gameObject.GetComponent<MeshRenderer>().enabled = false;
			}
		}
		
		if (currentGachaMode == GachaType.Fragment)
		{
			pandoraBoxSkeleton_premium.state.SetAnimation("normal", true);
		}
		
		else
		{
			pandoraBoxSkeleton_normal.state.SetAnimation("normal", true);
		}
		
		enableGachaModeChange();
	}

	public IEnumerator finish()
	{
		panel2.StartTransition("Dismiss Forward");
		int ii = 0;
		foreach(KYAAgent s in sprites)
		{
			s.aname = "momoku";
			s.Animate();
			textLevels[ii].transform.gameObject.SetActive(false);
			textLevels[ii].transform.parent = null;
			++ii;
		}
		yield return new WaitForSeconds(2);
		panel1.gameObject.SetActive(true);
		panel1.StartTransition("Bring in Forward");	
		
		isAnimationActive = false;
	}

	public void onClickCancel()
	{
		if (isAnimationActive)
		{
			return;
		}
		
		FragmentManager.instance.comein();	
		maincam.instance.overlaycam_above.enabled = true;	
		panelMore.StartTransition("Dismiss Forward");		
	}

	public void onClickCollection()
	{
		if (isAnimationActive)
		{
			return;
		}
		
		slideOut();
		GameController.LoadLevel("collection", TransitionType.NoChange);
	}

	public void spawn()
	{
		panelMore.StartTransition("Bring in Forward");
	}

	public void onClickIAP()
	{
		panelMore.StartTransition("Dismiss Forward");
	}

	public void onClickBack()
	{
		if (isAnimationActive)
		{
			return;
		}
		
		slideOut();
		GameController.LoadLevel("title", TransitionType.Open);
	}

	public void onClickGem()
	{
		if (isAnimationActive)
		{
			return;
		}
		
		FragmentManager.instance.comein();	
		maincam.instance.overlaycam_above.enabled = true;
	}
	
	void onSwapGacha(int type)
	{
		//1 - XP
		//2 - Fragment
		GachaType mode = type == 1? GachaType.XP: GachaType.Fragment;
		
		if (mode == currentGachaMode)
		{
			updateGachaModeVisuals();
			return;
		}
		
		currentGachaMode = mode;
		updateGachaModeVisuals();
	}
	
	void disableGachaModeChange()
	{
		btn_mode_fragment.controlIsEnabled = false;
		btn_mode_XP.controlIsEnabled = false;
	}
	
	void enableGachaModeChange()
	{
		btn_mode_fragment.controlIsEnabled = true;
		btn_mode_XP.controlIsEnabled = true;
	}
	
	void updateGachaModeVisuals()
	{
		if (currentGachaMode == GachaType.Fragment)
		{	
			//btn_mode_XP.SetColor(Color.grey);
			//btn_mode_fragment.SetColor(Color.white);
			
			//free daily summons
			if(getDifference() > 86400)
			{
				chosen1.SetActive(true);
				chosen2.SetActive(false);
			}
			else
			{
				chosen2.SetActive(true);
				chosen1.SetActive(false);			
			}
			
			container_fragment.SetActive(true);
			container_XP.SetActive(false);
			
			container_textFragment.SetActive(true);
			container_textXP.SetActive(false);
			
			pandoraBoxSkeleton_normal.gameObject.SetActive(false);
			pandoraBoxSkeleton_premium.gameObject.SetActive(true);
			
			pandoraBoxSkeleton_premium.state.SetAnimation("normal", true);
		}
		
		else
		{
			//btn_mode_XP.SetColor(Color.white);
			//btn_mode_fragment.SetColor(Color.grey);
			
			container_fragment.SetActive(false);
			container_XP.SetActive(true);
			
			container_textFragment.SetActive(false);
			container_textXP.SetActive(true);
			
			pandoraBoxSkeleton_normal.gameObject.SetActive(true);
			pandoraBoxSkeleton_premium.gameObject.SetActive(false);
			
			pandoraBoxSkeleton_normal.state.SetAnimation("normal", true);
		}
		
		
	}
}
