﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class KYAnimator 
{
	public Dictionary<string, System.Action<KYAAgent>> dictionaryAnimation;
	public static KYAnimator instance
	{
		get
		{
			if(_instance == null)
			{
				_instance = new KYAnimator();
				_instance.Awake();
			}
			return _instance;
		}
	}
	public static KYAnimator _instance;
	public delegate void handler(GameObject go);

	public void Awake () 
	{
		dictionaryAnimation = new Dictionary<string, System.Action<KYAAgent>>();
		
		dictionaryAnimation.Add("idel", delegate(KYAAgent agent)
		{
			agent.anime.name = "idel";
			agent.anime.anim = Crash.Do(agent.gameObject, new Vector3(0.3f, 0.2f, 0), 0.2f, 0, null, delegate(EZAnimation anim)
			{
				if(anim.subject != null)
				{
					anim.subject.transform.gameObject.GetComponent<KYAAgent>().anime.name = "";
				}
			});
		});		

		dictionaryAnimation.Add("ladi", delegate(KYAAgent agent)
		{
			agent.anime.name = "ladi";
			PunchPosition.Do(agent.gameObject, new Vector3(0, 5f, 0), 1.5f, 0, null, null);
			agent.anime.anim =	PunchScale.Do(agent.gameObject, new Vector3(0, 1f, 0), 1.5f, 0f, null, delegate(EZAnimation anim)
			{
				if(anim.subject != null)
				{
					anim.subject.transform.gameObject.GetComponent<KYAAgent>().anime.name = "";
				}
			});
		});	

		dictionaryAnimation.Add("ledi", delegate(KYAAgent agent)
		{
			Vector3 start = new Vector3(-2, 2.5f, -10);
			agent.transform.position = start;
			agent.anime.name = "ledi";
			AnimateScale.Do(agent.gameObject, EZAnimation.ANIM_MODE.FromTo, new Vector3(0.0f, 0.0f, 1), new Vector3(1, 1, 1), EZAnimation.GetInterpolator(EZAnimation.EASING_TYPE.Linear), 0.3f, 0, null, null);
			agent.anime.anim =	PunchScale.Do(agent.gameObject, new Vector3(0, -0.3f, 0), 0.2f, 0.3f, null, delegate(EZAnimation anim)
			{
				if(anim.subject != null  && anim.subject.gameObject.activeSelf)
				{
					Vector3[] points = new Vector3[] { start, new Vector3(-2, 3.5f, -10), new Vector3(-2f, 0, -10) };
       				iTween.MoveTo(anim.subject.gameObject,iTween.Hash("path",points,"time", 2 , "onComplete", "finish","easetype", "linear"));
				}			
			});
		});	
		
		dictionaryAnimation.Add("updown", delegate(KYAAgent agent)
		{
			agent.anime.name = "updown";
			//AnimateScale.Do(agent.gameObject, EZAnimation.ANIM_MODE.FromTo, new Vector3(1f, 1f, 1), new Vector3(0.9f, 0.9f, 1), EZAnimation.GetInterpolator(EZAnimation.EASING_TYPE.Linear), 0.5f, 0, null, null);
			
			//agent.anime.anim =	AnimatePosition.Do(agent.gameObject, EZAnimation.ANIM_MODE.FromTo, new Vector3(0.9f, 0.9f, 1), new Vector3(1, 1, 1), EZAnimation.GetInterpolator(EZAnimation.EASING_TYPE.Linear), 0.5f, 0.5f, null, delegate(EZAnimation anim)
	
			AnimatePosition.Do(agent.gameObject, EZAnimation.ANIM_MODE.By, new Vector3(0, 0.5f, 0), EZAnimation.GetInterpolator(EZAnimation.EASING_TYPE.Linear), 0.5f, 0, null, null);
			
			agent.anime.anim =	AnimatePosition.Do(agent.gameObject, EZAnimation.ANIM_MODE.By, new Vector3(0, -0.5f, 0), EZAnimation.GetInterpolator(EZAnimation.EASING_TYPE.Linear), 0.5f, 0.5f, null, delegate(EZAnimation anim)
			{
				if(anim.subject != null  && anim.subject.gameObject.activeSelf)
				{
					anim.subject.transform.gameObject.GetComponent<KYAAgent>().anime.name = "";
				}			
			});
		});	

		dictionaryAnimation.Add("bigsmall", delegate(KYAAgent agent)
		{
			agent.anime.name = "bigsmall";
			AnimateScale.Do(agent.gameObject, EZAnimation.ANIM_MODE.FromTo, new Vector3(1f, 1f, 1), new Vector3(0.9f, 0.9f, 1), EZAnimation.GetInterpolator(EZAnimation.EASING_TYPE.Linear), 0.5f, 0, null, null);
			agent.anime.anim =	AnimateScale.Do(agent.gameObject, EZAnimation.ANIM_MODE.FromTo, new Vector3(0.9f, 0.9f, 1), new Vector3(1, 1, 1), EZAnimation.GetInterpolator(EZAnimation.EASING_TYPE.Linear), 0.5f, 0.5f, null, delegate(EZAnimation anim)
			{
				if(anim.subject != null  && anim.subject.gameObject.activeSelf)
				{
					anim.subject.transform.gameObject.GetComponent<KYAAgent>().anime.name = "";
				}			
			});
		});	

		dictionaryAnimation.Add("smallbig", delegate(KYAAgent agent)
		{
			agent.anime.name = "smallbig";
			agent.anime.anim =	AnimateScale.Do(agent.gameObject, EZAnimation.ANIM_MODE.FromTo, new Vector3(3f, 3f, 1), new Vector3(1, 1, 1), EZAnimation.GetInterpolator(EZAnimation.EASING_TYPE.Linear), 0.5f, 0f, null, delegate(EZAnimation anim)
			{
				if(anim.subject != null  && anim.subject.gameObject.activeSelf)
				{
					anim.subject.transform.gameObject.GetComponent<KYAAgent>().anime.name = "";
				}			
			});
		});	

		dictionaryAnimation.Add("wudi", delegate(KYAAgent agent)
		{
			agent.anime.name = "wudi";
			AnimateScale.Do(agent.gameObject, EZAnimation.ANIM_MODE.FromTo, new Vector3(0.0f, 0.0f, 1), new Vector3(1, 1, 1), EZAnimation.GetInterpolator(EZAnimation.EASING_TYPE.Linear), 0.5f, 0, null, null);
			agent.anime.anim =	PunchScale.Do(agent.gameObject, new Vector3(-0.5f, 0, 0), 0, 0.5f, null, delegate(EZAnimation anim)
			{
				if(anim.subject != null)
				{
					anim.subject.transform.gameObject.GetComponent<KYAAgent>().anime.name = "";
				}			
			});
		});

		dictionaryAnimation.Add("madi", delegate(KYAAgent agent)
		{
			agent.anime.name = "madi";
			AnimateScale.Do(agent.gameObject, EZAnimation.ANIM_MODE.FromTo, new Vector3(1f, 1f, 1), new Vector3(0, 0, 1), EZAnimation.GetInterpolator(EZAnimation.EASING_TYPE.Linear), 0.5f, 0.5f, null, delegate(EZAnimation anim)
			{
				if(anim.subject != null)
				{
					anim.subject.transform.gameObject.GetComponent<KYAAgent>().anime.name = "";
				}			
			});
		});

		dictionaryAnimation.Add("momo0", delegate(KYAAgent agent)
		{
			agent.anime.name = "momo0";
			Vector3 start1 = new Vector3(-2, 0, -10);
			Vector3 start = new Vector3(-1.85f, 0, -14f);
			agent.transform.position = start1;
			Vector3[] points = new Vector3[] { start1, new Vector3(-2, 3.5f, -10), start };
			agent.gameObject.renderer.material.shader = Shader.Find("Sprite/Vertex Colored");
			FadeMaterial.Do(agent.gameObject, agent.gameObject.renderer.material, EZAnimation.ANIM_MODE.FromTo, new Color(1f, 1f, 1f, 1f), new Color(0.5f, 0.5f, 0.5f, 1f), EZAnimation.GetInterpolator(EZAnimation.EASING_TYPE.Linear), 1.5f, 0, null, null);
       		iTween.MoveTo(agent.gameObject,iTween.Hash("path",points,"time", 2 , "onComplete", "finish","easetype", "linear"));
		});	

		dictionaryAnimation.Add("momo1", delegate(KYAAgent agent)
		{
			agent.anime.name = "momo1";
			Vector3 start1 = new Vector3(-2, 0, -10);
			Vector3 start = GotchaManager.instance.poss[0];
			agent.transform.position = start1;
			Vector3[] points = new Vector3[] { start1, new Vector3(-2, 3.5f, -10), start };
			agent.gameObject.renderer.material.shader = Shader.Find("Sprite/Vertex Colored");
			FadeMaterial.Do(agent.gameObject, agent.gameObject.renderer.material, EZAnimation.ANIM_MODE.FromTo, new Color(1f, 1f, 1f, 1f), new Color(0.5f, 0.5f, 0.5f, 1f), EZAnimation.GetInterpolator(EZAnimation.EASING_TYPE.Linear), 1.5f, 0, null, null);
       		iTween.MoveTo(agent.gameObject,iTween.Hash("path",points,"time", 2 , "onComplete", "finish","easetype", "linear"));
		});	

		dictionaryAnimation.Add("momo2", delegate(KYAAgent agent)
		{
			agent.anime.name = "momo2";
			Vector3 start1 = new Vector3(-2, 0, -10);
			Vector3 start = GotchaManager.instance.poss[1];
			agent.transform.position = start1;
			Vector3[] points = new Vector3[] { start1, new Vector3(-2, 3.5f, -10), start };
			agent.gameObject.renderer.material.shader = Shader.Find("Sprite/Vertex Colored");
			FadeMaterial.Do(agent.gameObject, agent.gameObject.renderer.material, EZAnimation.ANIM_MODE.FromTo, new Color(1f, 1f, 1f, 1f), new Color(0.5f, 0.5f, 0.5f, 1f), EZAnimation.GetInterpolator(EZAnimation.EASING_TYPE.Linear), 1.5f, 0, null, null);
       		iTween.MoveTo(agent.gameObject,iTween.Hash("path",points,"time", 2 , "onComplete", "finish","easetype", "linear"));
		});	

		dictionaryAnimation.Add("momo3", delegate(KYAAgent agent)
		{
			agent.anime.name = "momo3";
			Vector3 start1 = new Vector3(-2, 0, -10);
			Vector3 start = GotchaManager.instance.poss[2];
			agent.transform.position = start1;
			Vector3[] points = new Vector3[] { start1, new Vector3(-2, 3.5f, -10), start };
			agent.gameObject.renderer.material.shader = Shader.Find("Sprite/Vertex Colored");
			FadeMaterial.Do(agent.gameObject, agent.gameObject.renderer.material, EZAnimation.ANIM_MODE.FromTo, new Color(1f, 1f, 1f, 1f), new Color(0.5f, 0.5f, 0.5f, 1f), EZAnimation.GetInterpolator(EZAnimation.EASING_TYPE.Linear), 1.5f, 0, null, null);
       		iTween.MoveTo(agent.gameObject,iTween.Hash("path",points,"time", 2 , "onComplete", "finish","easetype", "linear"));
		});	

		dictionaryAnimation.Add("momo4", delegate(KYAAgent agent)
		{
			agent.anime.name = "momo4";
			Vector3 start1 = new Vector3(-2, 0, -10);
			Vector3 start = GotchaManager.instance.poss[3];
			agent.transform.position = start1;
			Vector3[] points = new Vector3[] { start1, new Vector3(-2, 3.5f, -10), start };
			agent.gameObject.renderer.material.shader = Shader.Find("Sprite/Vertex Colored");
			FadeMaterial.Do(agent.gameObject, agent.gameObject.renderer.material, EZAnimation.ANIM_MODE.FromTo, new Color(1f, 1f, 1f, 1f), new Color(0.5f, 0.5f, 0.5f, 1f), EZAnimation.GetInterpolator(EZAnimation.EASING_TYPE.Linear), 1.5f, 0, null, null);
       		iTween.MoveTo(agent.gameObject,iTween.Hash("path",points,"time", 2 , "onComplete", "finish","easetype", "linear"));
		});	

		dictionaryAnimation.Add("momo5", delegate(KYAAgent agent)
		{
			agent.anime.name = "momo5";
			Vector3 start1 = new Vector3(-2, 0, -10);
			Vector3 start = GotchaManager.instance.poss[4];
			agent.transform.position = start1;
			Vector3[] points = new Vector3[] { start1, new Vector3(-2, 3.5f, -10), start };
			agent.gameObject.renderer.material.shader = Shader.Find("Sprite/Vertex Colored");
			FadeMaterial.Do(agent.gameObject, agent.gameObject.renderer.material, EZAnimation.ANIM_MODE.FromTo, new Color(1f, 1f, 1f, 1f), new Color(0.5f, 0.5f, 0.5f, 1f), EZAnimation.GetInterpolator(EZAnimation.EASING_TYPE.Linear), 1.5f, 0, null, null);
       		iTween.MoveTo(agent.gameObject,iTween.Hash("path",points,"time", 2 , "onComplete", "finish","easetype", "linear"));
		});	

		dictionaryAnimation.Add("momo6", delegate(KYAAgent agent)
		{
			agent.anime.name = "momo6";
			Vector3 start1 = new Vector3(-2, 0, -10);
			Vector3 start = GotchaManager.instance.poss[5];
			agent.transform.position = start1;
			Vector3[] points = new Vector3[] { start1, new Vector3(-2, 3.5f, -10), start };
			agent.gameObject.renderer.material.shader = Shader.Find("Sprite/Vertex Colored");
			FadeMaterial.Do(agent.gameObject, agent.gameObject.renderer.material, EZAnimation.ANIM_MODE.FromTo, new Color(1f, 1f, 1f, 1f), new Color(0.5f, 0.5f, 0.5f, 1f), EZAnimation.GetInterpolator(EZAnimation.EASING_TYPE.Linear), 1.5f, 0, null, null);
       		iTween.MoveTo(agent.gameObject,iTween.Hash("path",points,"time", 2 , "onComplete", "finish","easetype", "linear"));
		});	

		dictionaryAnimation.Add("momoku", delegate(KYAAgent agent)
		{
			agent.anime.name = "momoku";
			Vector3 start = agent.transform.position;
			agent.transform.position = start;
			Vector3[] points = new Vector3[] { start, new Vector3(2, 2f, -14), new Vector3(4f, -3, -14) };
       		iTween.MoveTo(agent.gameObject,iTween.Hash("path",points,"time", 2 ,"easetype", "linear"));
       		AnimateScale.Do(agent.gameObject, EZAnimation.ANIM_MODE.FromTo, agent.transform.localScale, new Vector3(0, 0, 1), EZAnimation.GetInterpolator(EZAnimation.EASING_TYPE.Linear), 0.5f, 1f, null, delegate(EZAnimation anim)
			{
				if(anim.subject != null)
				{
					GotchaManager.instance.sprites.Remove(agent);
					GameObject.Destroy(agent.gameObject);
				}			
			});
		});	

		dictionaryAnimation.Add("curve", delegate(KYAAgent agent)
		{
			agent.anime.name = "momoku";
			Vector3 start = agent.transform.position;
			agent.transform.position = start;
			Vector3[] points = new Vector3[] { start, start+new Vector3(2, 2f, 0), start+new Vector3(4, 0f, 0) };
       		iTween.MoveTo(agent.gameObject,iTween.Hash("path",points,"time", 2 ,"easetype", "linear", "onComplete", new handler(delegate(GameObject go)
       		{
       			go.GetComponent<KYAAgent>().finish();
       		}
       		)));
		});
	}
	
	public bool doAnim(KYAAgent agent, string name)
	{
		if(agent.oriScale != Vector3.zero && agent.transform.localScale != agent.oriScale)
		{
			//agent.transform.localScale = agent.oriScale;
		}
		if(agent.anime.name != "")
		{
			if(agent.anime.name == name)
			{
				return false;
			}
			else if(agent.anime.anim != null)
			{
				agent.anime.anim.End();
				agent.anime.name = name;
				dictionaryAnimation[name](agent);	
			}	
		}
		else
		{
			dictionaryAnimation[name](agent);	
		}
		return true;
	}

	public bool forceAnim(KYAAgent agent, string name)
	{
		agent.anime.anim.End();
		dictionaryAnimation[name](agent);	
		return true;
	}
}
