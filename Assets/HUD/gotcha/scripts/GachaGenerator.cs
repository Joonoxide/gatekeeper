﻿using UnityEngine;
using System.Collections;
using System.Xml;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System;

public enum GachaType
{
	XP,
	Fragment
}

public enum RankType
{
	A = 0,
	B,
	C
}

public enum RankBounds
{
	Low,
	High
}

public class GachaGenerator {
	
	public static readonly Dictionary<string, GachaWrapper> gachaOddsLookup = new Dictionary<string, GachaWrapper>();
	public static readonly Dictionary<string, GachaSpawnWrapper> gachaSpawnsLookup = new Dictionary<string, GachaSpawnWrapper>();
	
	private static GachaGenerator __instance;
	
	private static System.Random random = new System.Random();
	
	public class GachaWrapper
	{
		public string category {private set; get;}
		
		private List<KeyValuePair<string, float>> __rarity = new List<KeyValuePair<string, float>>();
		public ReadOnlyCollection<KeyValuePair<string, float>> rarity {get {return __rarity.AsReadOnly();}}
		
		private List<KeyValuePair<string, GachaRankWrapper>> __oddsByRank = new List<KeyValuePair<string, GachaRankWrapper>>();
		public ReadOnlyCollection<KeyValuePair<string, GachaRankWrapper>> oddsByRank {get {return __oddsByRank.AsReadOnly();}}
		
		public GachaWrapper()
		{
			
		}
		
		public GachaWrapper (XmlNode gachaCategoryNode)
		{
			category = gachaCategoryNode.Attributes.GetNamedItem("name").Value;
			
			foreach (XmlNode rarityLevel in gachaCategoryNode.SelectNodes("descendant::Rarity"))
			{
				float result;
				
				if (float.TryParse(rarityLevel.InnerText, out result))
				{
					__rarity.Add(new KeyValuePair<string, float> (rarityLevel.Attributes.GetNamedItem("type").Value, result));
				}
				
				else
				{
					Debug.LogError("Error parsing rarity value for + " + rarityLevel.Attributes.GetNamedItem("type").Value + " in " + category + " category!");
				}
			}
			
			foreach (XmlNode rank in gachaCategoryNode.SelectNodes("descendant::Rank"))
			{
				__oddsByRank.Add(new KeyValuePair<string, GachaRankWrapper> (rank.Attributes.GetNamedItem("type").Value, new GachaRankWrapper(rank)));
			}
		}
		
		public List<KeyValuePair<string, float>> sortedByRarity()
		{
			List<KeyValuePair<string, float>> result = new List<KeyValuePair<string, float>>(__rarity);
			
			result.Sort(ascendingFloat);
			
			return result;
		}
		
		public List<KeyValuePair<string, GachaRankWrapper>> sortedByRankOdds()
		{
			List<KeyValuePair<string, GachaRankWrapper>> result = new List<KeyValuePair<string, GachaRankWrapper>>(__oddsByRank);
			
			result.Sort(ascendingRankWrapper);
			
			return result;
		}
		
		public string ToString ()
		{
			string output = "[GachaWrapper] Category: " + category;
			
			foreach(KeyValuePair<string, float> pair in __rarity)
			{
				output += " " + pair.Key + ": " + pair.Value + "%";
			}
			
			return output;
		}
		
		private int ascendingFloat(KeyValuePair<string, float> a, KeyValuePair<string, float> b)
		{
			return (a.Value.CompareTo(b.Value));
		}
		
		private int ascendingRankWrapper(KeyValuePair<string, GachaRankWrapper> a, KeyValuePair<string, GachaRankWrapper> b)
		{
			return ((a.Value as GachaRankWrapper).rollChance.CompareTo((b.Value as GachaRankWrapper).rollChance));
		}
		
	}
	
	public class GachaSpawnWrapper
	{
		public string category {private set; get;}
		
		private List<string> __units = new List<string>();
		public ReadOnlyCollection<string> units {get {return __units.AsReadOnly();}}
		
		public GachaSpawnWrapper()
		{
			
		}
		
		public GachaSpawnWrapper(XmlNode rarityCategoryNode)
		{
			category = rarityCategoryNode.Attributes.GetNamedItem("type").Value;
			
			foreach (XmlNode unit in rarityCategoryNode.SelectNodes("descendant::Unit"))
			{
				__units.Add(unit.InnerText);
			}
		}
		
		public override string ToString ()
		{
			string output = "[GachaSpawnWrapper] Category: " + category + " {";
				
			output += string.Join(", ", __units.ToArray()) + "}";
			
			return output;
		}
	}
	
	public class GachaRankWrapper
	{
		public string category {private set; get;}
		public float rollChance {private set; get;}
		public float odds_L {private set; get;}
		public float odds_H {private set; get;}
		
		public GachaRankWrapper(XmlNode rankNode)
		{
			category = rankNode.Attributes.GetNamedItem("type").Value;
			
			XmlNode baseRollNode = rankNode.SelectSingleNode("descendant::RollChance");
			float buffer;
				
			if (float.TryParse(baseRollNode.InnerText, out buffer))
			{
				rollChance = buffer;
			}
			
			else
			{
				Debug.LogError("[GachaRankWrapper] Error parsing base roll chance for " + rankNode.Attributes.GetNamedItem("type").Value);
			}
			
			foreach (XmlNode bounds in rankNode.SelectNodes("descendant::Bounds"))
			{
				string boundsType = bounds.Attributes.GetNamedItem("type").Value;
				
				if (boundsType.Equals("L"))
				{
					if (float.TryParse(bounds.InnerText, out buffer))
					{
						odds_L = buffer;
					}
				}
				
				else if (boundsType.Equals("H"))
				{
					if (float.TryParse(bounds.InnerText, out buffer))
					{
						odds_H = buffer;
					}
				}
				
				else
				{
					Debug.LogError("[GachaRankWrapper] Invalid bounds type of " + boundsType);
				}
			}
		}
	}
	
	// Use this for initialization
	public static void Initialize () {
		
		if (__instance != null)
		{
			Debug.LogError("Attempt to re-initialize GachaGenerator when it is already initialized!");
			return;
		}
		
		__instance = new GachaGenerator();
		
		XmlDocument XMLGachaOdds = new XmlDocument();
	
		XMLGachaOdds.LoadXml((Resources.Load("gachaOdds") as TextAsset).text);
		
		XmlNodeList groupList = XMLGachaOdds.GetElementsByTagName("Group");
		
		foreach(XmlNode node in groupList)
		{
			gachaOddsLookup.Add(node.Attributes.GetNamedItem("name").Value, new GachaWrapper(node));
		}

		XmlDocument XMLUnitByRarity = new XmlDocument();
	
		XMLUnitByRarity.LoadXml((Resources.Load("unitRarity") as TextAsset).text);
		
		XmlNodeList rarityList = XMLUnitByRarity.GetElementsByTagName("Rarity");
		
		foreach (XmlNode node in rarityList)
		{
			gachaSpawnsLookup.Add(node.Attributes.GetNamedItem("type").Value, new GachaSpawnWrapper(node));
		}
	}
	
	public static GachaGenerator instance
	{
		get
		{
			if (__instance == null)
			{
				Initialize();
			}
			
			return __instance;
		}
	}

	public string generateUnit(GachaType type)
	{
		
		//obtain right table for the correct gacha type i.e. XP or Fragment		
		GachaWrapper data;
		
		if (!gachaOddsLookup.TryGetValue(type.ToString(), out data))
		{
			Debug.LogError ("No data found for gacha! Make sure the XML is referenced and the class is initialised!");
			return "null";
		}
		
		string rarityType;
		double rand ;
		double runningProb;
		List <KeyValuePair<string, float>> sortedList;
		List <string> validCandidates = new List<string>();
		List<string> lockedUnits = GotchaManager.instance.lockedUnits;
		
		sortedList = data.sortedByRarity();
		
		do
		{
			validCandidates.Clear();
			rarityType = "";
			//rand = UnityEngine.Random * 100;
			rand = random.NextDouble() * 100f;
			runningProb = 0f;
			
			for (int i = 0; i < sortedList.Count; i++)
			{
				runningProb += sortedList[i].Value;
				
				if (rand <= runningProb)
				{
					rarityType = sortedList[i].Key;
					break;
				}
			}
			
			Debug.Log (rarityType.ToString() + " at " + rand.ToString());
			
			ReadOnlyCollection <string> candidates = gachaSpawnsLookup[rarityType].units;
			
			foreach(string unit in candidates)
			{
				if (!lockedUnits.Contains(unit))
				{
					validCandidates.Add(unit);
				}
			}
			
		} while(validCandidates.Count == 0);
		
		
		int randUnit = UnityEngine.Random.Range(0, validCandidates.Count); //Random.Range is exclusive of the max, so -1 is not necessary
		
		return validCandidates[randUnit];
	}
	
	public int generateLevel(GachaType type)
	{
		//obtain right table for the correct gacha type i.e. XP or Fragment		
		GachaWrapper data;
		
		if (!gachaOddsLookup.TryGetValue(type.ToString(), out data))
		{
			Debug.LogError ("No data found for gacha! Make sure the XML is referenced and the class is initialised!");
			return 1;
		}
		
		List <KeyValuePair<string, GachaRankWrapper>> sortedList = data.sortedByRankOdds();
		
		GachaRankWrapper rankWrapper = null;
		double rand;
		double runningProb;
		
		//rand = UnityEngine.Random.value * 100;
		rand = random.NextDouble() * 100;
		runningProb = 0f;
		
		Debug.Log ("Rank roll: " + rand);
		
		//obtain the wrapper for the rank first
		for (int i = 0; i < sortedList.Count; i++)
		{
			runningProb += sortedList[i].Value.rollChance;
			
			if (rand <= runningProb)
			{
				rankWrapper = sortedList[i].Value;
				break;
			}
		}
		
		// get rank (A, B, C)
		RankType rank = getRankType(rankWrapper.category);
		
		//compute the base level depending on the rank rolled
		int level = 10 * (int)rank;
		
		//obtain bounds (low or high end level range)
		//default at 1
		int minLevel, maxLevel;
		RankBounds bounds = getBounds(rankWrapper);
		
		if (bounds.Equals(RankBounds.Low))
		{
			minLevel = 1;
			maxLevel = 5;
		}
		
		else
		{
			minLevel = 6;
			maxLevel = 10;
		}
		
		//+1 to max because random.range for int is exclusive of max
		int randLevel = UnityEngine.Random.Range(minLevel, maxLevel + 1); 
		
		level += randLevel;
		
		Debug.Log ("[GachaGenerator] Rolled a(n) " 
			+ rank.ToString() 
			+ " with " + bounds.ToString());
		
		return Mathf.Min(level, 30); //cap max level at 30
	}
	
	RankType getRankType(string type)
	{
		RankType result = RankType.A;
		
		if (type.Equals("A"))
		{
			result = RankType.A;
		}
		
		else if (type.Equals("B"))
		{
			result = RankType.B;
		}
		
		else if (type.Equals("C"))
		{
			result = RankType.C;
		}
		
		return result;
	}
	
	RankBounds getBounds(GachaRankWrapper rankWrapper)
	{
		RankBounds result;
		double rand = random.NextDouble() * 100;
		
		Debug.Log ("Bounds roll: " + rand);
		
		if (rand <= rankWrapper.odds_L)
		{
			result = RankBounds.Low;
		}
		
		else 
		{
			result = RankBounds.High;
		}
		
		return result;
	}
}
