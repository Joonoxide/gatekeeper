pandoraBox@4.atlas.png
format: RGBA4444
filter: Linear,Linear
repeat: none
eye_top
  rotate: false
  xy: 8, 760
  size: 140, 144
  orig: 139, 141
  offset: 0, -3
  index: -1
premium2
  rotate: false
  xy: 516, 8
  size: 500, 368
  orig: 499, 365
  offset: 0, -3
  index: -1
premium1
  rotate: false
  xy: 8, 384
  size: 500, 368
  orig: 499, 365
  offset: 0, -3
  index: -1
premium3
  rotate: false
  xy: 8, 8
  size: 500, 368
  orig: 499, 365
  offset: 0, -3
  index: -1
eye_surprise
  rotate: false
  xy: 156, 760
  size: 140, 144
  orig: 139, 141
  offset: 0, -3
  index: -1
_dummy
  rotate: false
  xy: 8, 936
  size: 12, 12
  orig: 12, 12
  offset: 0, 0
  index: -1
eye_centel
  rotate: false
  xy: 748, 760
  size: 140, 144
  orig: 139, 141
  offset: 0, -3
  index: -1
eye_left
  rotate: false
  xy: 452, 760
  size: 140, 144
  orig: 139, 141
  offset: 0, -3
  index: -1
eye_right
  rotate: false
  xy: 304, 760
  size: 140, 144
  orig: 139, 141
  offset: 0, -3
  index: -1
pandora_box
  rotate: false
  xy: 516, 384
  size: 500, 368
  orig: 499, 365
  offset: 0, -3
  index: -1
_dummy2.jpeg
  rotate: false
  xy: 8, 912
  size: 16, 16
  orig: 16, 16
  offset: 0, 0
  index: -1
eye_down
  rotate: false
  xy: 600, 760
  size: 140, 144
  orig: 139, 141
  offset: 0, -3
  index: -1
