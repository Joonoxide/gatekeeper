pandoraBox@2.atlas.png
format: RGBA4444
filter: Linear,Linear
repeat: none
eye_top
  rotate: false
  xy: 4, 380
  size: 70, 72
  orig: 70, 71
  offset: 0, -1
  index: -1
premium2
  rotate: false
  xy: 258, 4
  size: 250, 184
  orig: 250, 183
  offset: 0, -1
  index: -1
premium1
  rotate: false
  xy: 4, 192
  size: 250, 184
  orig: 250, 183
  offset: 0, -1
  index: -1
premium3
  rotate: false
  xy: 4, 4
  size: 250, 184
  orig: 250, 183
  offset: 0, -1
  index: -1
eye_surprise
  rotate: false
  xy: 78, 380
  size: 70, 72
  orig: 70, 71
  offset: 0, -1
  index: -1
_dummy
  rotate: false
  xy: 4, 468
  size: 6, 6
  orig: 6, 6
  offset: 0, 0
  index: -1
eye_centel
  rotate: false
  xy: 374, 380
  size: 70, 72
  orig: 70, 71
  offset: 0, -1
  index: -1
eye_left
  rotate: false
  xy: 226, 380
  size: 70, 72
  orig: 70, 71
  offset: 0, -1
  index: -1
eye_right
  rotate: false
  xy: 152, 380
  size: 70, 72
  orig: 70, 71
  offset: 0, -1
  index: -1
pandora_box
  rotate: false
  xy: 258, 192
  size: 250, 184
  orig: 250, 183
  offset: 0, -1
  index: -1
_dummy2.jpeg
  rotate: false
  xy: 4, 456
  size: 8, 8
  orig: 8, 8
  offset: 0, 0
  index: -1
eye_down
  rotate: false
  xy: 300, 380
  size: 70, 72
  orig: 70, 71
  offset: 0, -1
  index: -1
