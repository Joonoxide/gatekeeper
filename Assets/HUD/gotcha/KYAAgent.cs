﻿using UnityEngine;
using System.Collections;

public class KYAAgent : MonoBehaviour 
{
	public class AnimationWrapper
	{
		public EZAnimation anim;
		public string name;
	}

	public AnimationWrapper anime;
	public Vector3 oriScale;
	public string aname = "";
	public bool runOnStart;
	public bool isLoop;
	public float del;
	public float t;

	public virtual void Start () 
	{
		anime = new AnimationWrapper();
		anime.name = "";
		oriScale = transform.localScale;
		if(runOnStart)
		{
			Animate(del, true);
		}
	}

	public void Animate(float d = 0, bool loop = false)
	{
		isLoop = loop;
		StartCoroutine(animate(d));
	}

	public IEnumerator animate(float d)
	{
		yield return new WaitForSeconds(d);
		if(KYAnimator.instance.dictionaryAnimation.ContainsKey(aname))
		{
			if(isLoop)
			{
				StartCoroutine(loopAnimate(true));
			}
			else
			{
				KYAnimator.instance.doAnim(this, aname);
			}
		}
		yield return null;
	}

	public void finish()
	{
		//Debug.Log("WHEEERERE!!");
		anime.name = "";
	}

	public IEnumerator loopAnimate(bool isFirst)
	{
		if(!isLoop)
		{
			yield break;
		}
		if(!isFirst)
		{
			yield return new WaitForSeconds(t);
		}
		StartCoroutine(loopAnimate(false));
		KYAnimator.instance.doAnim(this, aname);
		yield return null;
	}
}
