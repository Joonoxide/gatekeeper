﻿using UnityEngine;
using System.Collections;

public class maincam : MonoBehaviour 
{
	Vector3 pos;
	public static maincam instance;
	public Camera cam;
	public Camera overlaycam_above, overlaycam_below;
	public GameObject gui;
	public GameObject level;
	public GameObject bg_top, bg_bottom, bg_container;

	void Awake () 
	{
		//Debug.Log (cam.orthographicSize);
		pos = transform.position;
		instance = this;
		cam = GetComponent<Camera>();
		overlaycam_above = transform.Find("overlaycamera_above").gameObject.GetComponent<Camera>();
		overlaycam_below = transform.Find("overlaycamera_below").gameObject.GetComponent<Camera>();
		gui = transform.Find("gui").gameObject;
		
		//bg_top = transform.parent.transform.Find ("Cube1").gameObject;
		//bg_bottom = transform.parent.transform.Find ("Cube2").gameObject;
		
		//bg_container = transform.parent.transform.Find("MapContainer").gameObject;
		
		level = transform.Find("levelcamera").gameObject;
		OnLevelWasLoaded();
	}
	
	IEnumerator bbb()
	{
		yield return null;
		GameObject go = transform.Find("gui").transform.Find("panel").gameObject;
		loop(go);
		go = transform.Find("levelcamera").transform.Find("panel_options").gameObject;
		loop(go);

	}

	public void loop(GameObject go)
	{
		foreach(Transform child in go.transform)
		{
			var but = child.gameObject.GetComponent<UIButton>();
			if(but != null)
			{
				but.scriptWithMethodToInvoke = TitleController.instance;
			}
			var but2 = child.gameObject.GetComponent<UIStateToggleBtn>();
			if(but2 != null)
			{
				but2.scriptWithMethodToInvoke = TitleController.instance;
			}
			loop(child.gameObject);
		}
	}

	public void OnLevelWasLoaded()
	{
		if(Application.loadedLevelName != "title")
		{
			transform.position = new Vector3(0, 0, -10);
			gui.SetActive(false);
			level.SetActive(false);
			cam.enabled = false;
			
			//bg_bottom.SetActive(false);
			//bg_top.SetActive(false);
			
			//bg_container.SetActive(false);
		}
		else
		{
			gui.SetActive(true);
			level.SetActive(true);
			transform.position = pos;
			cam.enabled = true;
			
			//bg_bottom.SetActive(true);
			//bg_top.SetActive(true);
			
			//bg_container.SetActive(true);
			
			UIManager.instance.AddCamera(gameObject.GetComponent<Camera>(), -1, Mathf.Infinity, 1);
			
		}
		StartCoroutine(bbb());
	}
}
