﻿using UnityEngine;
using System.Collections;

public enum NavigationType
{
	StartGame,
	Harvester,
	Gacha,
	Shop,
	Magic,
	Fusion,
	Upgrade,
	Barracks
}

public enum PurchaseType
{
	Harvester,
	XP,
	Fragment
}

public enum FeatureUsageType
{
	Fusion
}

public enum QuantityXP
{
	_1000,
	_3000
}

public enum QuantityHarvester
{
	_3rd,
	_4th,
	_5th,
	_6th,
	_7th,
	_Max,
	Null
}

public enum QuantityFragment
{
	_01,
	_06,
	_12,
	_30,
	_60,
	_85,
	Null
}

public enum QuantityGacha
{
	_1,
	_5
}

public static class CustomDesignEvent {
	
	private const string PROGRESS_LEVEL_COMPLETE = "Progress:End:Complete";
	private const string PROGRESS_LEVEL_FAIL = "Progress:End:Fail";
	private const string PROGRESS_LEVEL_ABORT = "Progress:End:Abort";
	private const string PROGRESS_LEVEL_RETRY = "Progress:Retry";
	private const string PROGRESS_LEVEL_START = "Progress:Start";
	
	private const string NAVIGATION_TITLE = "Navigation:StartGame";
	private const string NAVIGATION_WINDOW_HARVESTER = "Navigation:HarvesterWindow";
	private const string NAVIGATION_WINDOW_GACHA = "Navigation:GachaWindow";
	private const string NAVIGATION_WINDOW_SHOP = "Navigation:ShopWindow";
	private const string NAVIGATION_WINDOW_MAGIC = "Navigation:MagicWindow";
	private const string NAVIGATION_WINDOW_FUSION = "Navigation:FusionWindow";
	private const string NAVIGATION_WINDOW_UPGRADE = "Navigation:UpgradeWindow";
	private const string NAVIGATION_WINDOW_BARRACKS = "Navigation:BarracksWindow";
	
	private const string FEATURE_USAGE_FUSION = "FeatureUsage:Fusion";
	
	private const string PURCHASE_HARVESTER = "Purchase:Harvester";
	private const string PURCHASE_XP = "Purchase:XP";
	private const string PURCHASE_FRAGMENT = "Purchase:Fragment";
	
	private const string GACHA_XP = "Gacha:XP";
	private const string GACHA_FRAGMENT = "Gacha:Fragment";
	
	private const string DEBUG_MODE = "Debug";
	
	public static string getGacha(GachaType type, QuantityGacha quantity)
	{
		string output = getDebugPrefix();
		
		if (type.Equals(GachaType.XP))
		{
			output += GACHA_XP;
		}
		
		else
		{
			output += GACHA_FRAGMENT;
		}
		
		output += ":" + quantity.ToString().Split('_')[1];
		
		return output;
	}
	
	public static QuantityHarvester getHarvesterQuantityFromID(string productID)
	{
		QuantityHarvester quantity = QuantityHarvester.Null;
		
		int count;
		string identifier = "harvester";
				
		if (int.TryParse(productID.Substring(productID.LastIndexOf(identifier) + identifier.Length), out count))
		{
			switch (count)
			{
				case 1:
					quantity = QuantityHarvester._3rd;
					break;
				
				case 2:
					quantity = QuantityHarvester._4th;
					break;
				
				case 3:
					quantity = QuantityHarvester._5th;
					break;
				
				case 4:
					quantity = QuantityHarvester._6th;
					break;
				
				case 5:
					quantity = QuantityHarvester._7th;
					break;
				
				case 6:
					quantity = QuantityHarvester._Max;
					break;
			}
		}
		
		else
		{
			Debug.LogError("Invalid product ID!");
		}
		
		return quantity;
	}
	
	public static QuantityFragment getFragmentQuantityFromID(string productID)
	{
		QuantityFragment quantity = QuantityFragment.Null;
		
		int count;
		string identifier = "fragment";
				
		if (int.TryParse(productID.Substring(productID.LastIndexOf(identifier) + identifier.Length), out count))
		{
			switch (count)
			{
				case 1:
					quantity = QuantityFragment._01;
					break;
				
				case 6:
					quantity = QuantityFragment._06;
					break;
				
				case 12:
					quantity = QuantityFragment._12;
					break;
				
				case 30:
					quantity = QuantityFragment._30;
					break;
				
				case 60:
					quantity = QuantityFragment._60;
					break;
				
				case 85:
					quantity = QuantityFragment._85;
					break;
			}
		}
		
		else
		{
			Debug.LogError("Invalid product ID!");
		}
		
		return quantity;
	}
	
	public static string getPurchaseHarvester(QuantityHarvester quantity)
	{
		return getDebugPrefix() + PURCHASE_HARVESTER + ":" + quantity.ToString().Split('_')[1];
	}
	
	public static string getPurchaseXP(QuantityXP quantity)
	{
		return getDebugPrefix() + PURCHASE_XP + ":" + quantity.ToString().Split('_')[1];
	}
		
	public static string getPurchaseFragment(QuantityFragment quantity)
	{
		return getDebugPrefix() + PURCHASE_FRAGMENT + ":" + quantity.ToString().Split('_')[1];
	}
	
	public static string getNavigationWindow(NavigationType type)
	{
		string output = getDebugPrefix();
		
		switch (type)
		{
			case NavigationType.StartGame:
				output +=NAVIGATION_TITLE;
				break;
			
			case NavigationType.Harvester:
				output += NAVIGATION_WINDOW_HARVESTER;
				break;
			
			case NavigationType.Gacha:
				output += NAVIGATION_WINDOW_GACHA;
				break;
			
			case NavigationType.Shop:
				output += NAVIGATION_WINDOW_SHOP;
				break;
			
			case NavigationType.Magic:
				output += NAVIGATION_WINDOW_MAGIC;
				break;
			
			case NavigationType.Fusion:
				output += NAVIGATION_WINDOW_FUSION;
				break;
			
			case NavigationType.Upgrade:
				output += NAVIGATION_WINDOW_UPGRADE;
				break;
			
			case NavigationType.Barracks:
				output += NAVIGATION_WINDOW_BARRACKS;
				break;
		}
		return output;
	}
	
	public static string getFeatureUsage(FeatureUsageType type)
	{
		string output = getDebugPrefix();
		
		switch (type)
		{
			case FeatureUsageType.Fusion:
				output += FEATURE_USAGE_FUSION;
				break;
		}
		
		return output;
	}
	
	public static string getLevelComplete(int level)
	{
		string output = "";
		
		if (level < 10)
		{
			output += "0";
		}
		
		output += level.ToString();
		
		return getDebugPrefix() + PROGRESS_LEVEL_COMPLETE + ":" + output.ToString();
	}
	
	public static string getLevelFail(int level)
	{
		string output = "";
		
		if (level < 10)
		{
			output += "0";
		}
		
		output += level.ToString();
		
		return getDebugPrefix() + PROGRESS_LEVEL_FAIL + ":" + output.ToString();
	}
	
	public static string getLevelAbort(int level)
	{
		string output = "";
		
		if (level < 10)
		{
			output += "0";
		}
		
		output += level.ToString();
		
		return getDebugPrefix() + PROGRESS_LEVEL_ABORT + ":" + output.ToString();
	}
	
	public static string getLevelRetry(int level)
	{
		string output = "";
		
		if (level < 10)
		{
			output += "0";
		}
		
		output += level.ToString();
		
		return getDebugPrefix() + PROGRESS_LEVEL_RETRY + ":" + output.ToString();
	}
	
	public static string getLevelStart(int level)
	{
		string output = "";
		
		if (level < 10)
		{
			output += "0";
		}
		
		output += level.ToString();
		
		return getDebugPrefix() + PROGRESS_LEVEL_START + ":" + output.ToString();
	}
	
	private static string getDebugPrefix()
	{
		return (GA.SettingsGA.DebugMode? DEBUG_MODE + ":": "");
	}
}
