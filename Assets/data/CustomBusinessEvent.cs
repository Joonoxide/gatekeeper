﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class CustomBusinessEvent {
	
	private readonly static Dictionary<QuantityHarvester, int> harvesterPrice = new Dictionary<QuantityHarvester, int>
	{
		{QuantityHarvester._3rd, 99},
		{QuantityHarvester._4th, 199},
		{QuantityHarvester._5th, 299},
		{QuantityHarvester._6th, 399},
		{QuantityHarvester._7th, 499},
		{QuantityHarvester._Max, 999},
	};
	
	private readonly static Dictionary<QuantityFragment, int> fragmentPrice = new Dictionary<QuantityFragment, int>
	{
		{QuantityFragment._01, 99},
		{QuantityFragment._06, 499},
		{QuantityFragment._12, 899},
		{QuantityFragment._30, 1999},
		{QuantityFragment._60, 3799},
		{QuantityFragment._85, 4999},
	};
	
	private const string PURCHASE_HARVESTER = "Purchase:Harvester";
	private const string PURCHASE_FRAGMENT = "Purchase:Fragment";
	
	private const string DEBUG_MODE = "Debug";
	
	public const string CURRENCY = "USD";
	
	public static int fetchPriceHarvester(QuantityHarvester quantity)
	{
		return harvesterPrice[quantity];
	}
	
	public static int fetchPriceFragment(QuantityFragment quantity)
	{
		return fragmentPrice[quantity];
	}
	
	public static string getPurchaseHarvester(QuantityHarvester quantity)
	{
		return getDebugPrefix() + PURCHASE_HARVESTER + ":" + quantity.ToString().Split('_')[1];
	}
	
	public static string getPurchaseFragment(QuantityFragment quantity)
	{
		return getDebugPrefix() + PURCHASE_FRAGMENT + ":" + quantity.ToString().Split('_')[1];
	}
	
	private static string getDebugPrefix()
	{
		return (GA.SettingsGA.DebugMode? DEBUG_MODE + ":": "");
	}
}
