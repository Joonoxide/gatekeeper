﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class GameSettings {
	
	//tower HP
	public static int TOWER_HP_BASE_SUPPORT = 50; //archer towers
	public static int TOWER_HP_BASE_MAIN = 500; //main gate
	
	//xp reward
	public static List<float> XP_MIN_BY_STAR = new List<float>(new float[3] {300f, 400f, 500f});
	public static float XP_BASE_STAR_MOD = 150f;
	public static float XP_RESOURCE_MOD = 1f;
	
	//jaggedness here is for the purpose of introducing an illusion of variation and organic-ness to the calculation
	//i.e. does not look too structured
	public static float XP_JAGGED_LOW_MOD = 0.95f; 
	public static float XP_JAGGED_HIGH_MOD = 1.1f;
	
	//gacha
	public static int GACHA_XP_COST_PER_TIME = 100;
	
	//resources (trees)
	public static int RESOURCES_MAX_SPAWN = 5;
	
	//unit level up mod
	public static float HP_INCREMENT_PER_LEVEL = 0.1f;
	public static float DMG_INCREMENT_PER_LEVEL = 0.05f;
	
	//unit vision
	public static float VISION_RANGE_MELEE = 18;
	public static float VISION_RANGE_RANGED = 35;
}
